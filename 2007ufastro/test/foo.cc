#include "foo.h"

int main(int argc, char**argv) {
  try {
    clog<<"hello dave; please don't dump core or coredump..."<<endl;
    clog<<"ok, if i got this far, then no libstdc++ exceptions have occured."<<endl;
  }
  catch( std::exception& e ) {
    clog<<"ufgmotord> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgmotord> unknown exception..."<<endl;
    return -2;
  }
  return 0;

}
