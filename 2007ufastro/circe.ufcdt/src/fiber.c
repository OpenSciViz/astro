/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Wide-Field Infrared Camera (CIRCE)
 |      Cornell University, Ithaca, NY
 |
 | FILE: fiber.c -- ufCDT fiber management routines
 |
 | DESCRIPTION:
 |	This source code file contains the routines for
 |      managing fiber-optic interface (FOI) communications.
 |      Data from FOI is managed with edt_read() statements.
 |
 | REVISION HISTORY:
 |   Date            By         Description
 | 2001-Sep-24   Tom Hayward   Adapted from xpharo fiber.c
 | 
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <stropts.h>
#include <pthread.h>
#include <sys/mman.h>   /* for MAP_..., etc. */

#include "main.h"
#include "fiber.h"
#include "files.h"
#include "cmdcodes.h"

#ifdef INCLUDE_EDT
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sdv.h>
#include <camera.h>
#include <sdv_reg.h>
#include <libedt.h>
EdtDev *EDT_fd;               /* file descriptor for /dev/sdvN */
#endif

/* Special Characters */
#define CMD   1
#define PROG  2
#define ABORT 3

/******* SET THESE CONSTANTS DEPENDING ON COMPILER CONFIGURATION *******/
#define DELAY 120       /*  120 w/optimizer,  20 in debug mode */
#define PROGDELAY 3200  /* 1200 w/optimizer, 200 in debug mode */

unsigned char *FIFO_Base;     /* Points to sdvfoi registers */
foi_packet FOI_LastCmd, FOI_ContAcqCmd;
int SentZero = 0;
extern int Fake_Pipe_Out[];
extern int Fake_Pipe_In[];
pthread_mutex_t VLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t ZLock = PTHREAD_MUTEX_INITIALIZER;


/* Open SDVFOI device */

int sdvfoi_open(int unit)        /* N to select "/dev/sdvN" */
{

#ifdef INCLUDE_EDT

   unsigned short dma_cmd;

   /* Open the sdv driver for the scdfoi in specified unit */
   EDT_fd = edt_open("sdv", unit);
   if (EDT_fd == NULL) return(-1);

   {
      ushort dograbs = 0;                       /* Added 99May27 */
      u_int chunksize = 512*1024;
      ioctl(EDT_fd->fd, SDV_QUEUE_GRAB, &dograbs);
      ioctl(EDT_fd->fd, SDVS_MAXDMASIZE, &chunksize);
   } 

   /* Tell edt_read() in driver to write a 0x13 to scdfoi Config register  */
   /* at address 0x82 when it is ready to kick off the DMA.            */
   /* This sets the DMA_ENAB bit (along with DMA_IN and RAWOUT)	*/
   dma_cmd = 0x8213 ;
   ioctl(EDT_fd->fd, SDVS_SFOI_DMA_REG, &dma_cmd);

   /* Get the base address for scdfoi hardware registers */
   FIFO_Base = (u_char *) mmap((caddr_t)0, SDV_REG_SIZ, 
                    PROT_READ|PROT_WRITE, MAP_SHARED, EDT_fd->fd, 0) ;
   *(FIFO_Base + 0x80) = 0x03;      /* Command reg:  Reset the scdfoi  */
   *(FIFO_Base + 0x82) = 0x11;      /* Config  reg:  RAWOUT, DMAIN set */

   /* Must reprogram the almost_empty/almost_full fifo flags after reset */
   /* The values they are loaded with is function of SBus burst size,    */
   /* which varies over the different machines we might be running on.   */
   ioctl(EDT_fd->fd, SDV_PROG_FIFOS);

   return(0);

#else
   return(-1);
#endif

}


/* Close the SDVFOI device */

int sdvfoi_close(void)
{

#ifdef INCLUDE_EDT

   if (EDT_fd != NULL) {
      edt_close(EDT_fd);
      EDT_fd = NULL;
   }
   IC.data_mode = -1;

#endif

   return(0);
}


/* Insert a delay */

int foi_delay(int delay)
{
   int i, delaycount = 0;
   for (i=0; i<delay; i++) delaycount++;
   return(delaycount);
}


/* Reset global variables */

int foi_resetvars(void)
{
   int i;

   pthread_mutex_lock(&VLock);   /* Protect FOI_LastCmd */

   IC.fpga_executing = 0;
   IC.macro_executing = 0;
   IC.remote_executing = 0;
   for (i=0; i<FOI_NWORDS; i++) FOI_LastCmd.i[i] = 0;
   IC.int_mode = STDINT;
   newframe(-1);       /* Reset counters */

   pthread_mutex_unlock(&VLock);
   
   return(0);
}


/* Send 1 byte to FOI to test echoing. */

int foi_send1byte(void)
{
   if (IC.data_mode == REALDATA) {

      if (IC.debug) 
         fprintf(stderr, "foi_send1byte    : Sending SC%1d 0x00\n", CMD);

      *(FIFO_Base + 0xc5) = CMD;
      foi_delay(DELAY);
      *(FIFO_Base + 0xc6) = 0;

      pthread_mutex_lock(&ZLock);   /* Protect SentZero */
      SentZero = 1;
      pthread_mutex_unlock(&ZLock);
   }
   return(0);
}


/* Send up to FOI_NBYTES bytes to prompt FPGA to echo */

int foi_nullcmd(void)
{
   int i, lastcmd;
   
#ifdef SYSV
   useconds_t udelay = 10000;  /*  microseconds */
#else
   unsigned long udelay = 10000;
#endif

   if (IC.data_mode != REALDATA) return(0);

   pthread_mutex_lock(&VLock);   /* Protect FOI_LastCmd */
   if (FOI_LastCmd.i[1] == 0) FOI_LastCmd.i[1] = FOI_NULL_CMD;
   pthread_mutex_unlock(&VLock);

   i = 0;

   do {
      foi_send1byte();
      usleep(udelay);
      i++;
      pthread_mutex_lock(&VLock);   /* Protect FOI_LastCmd */
      lastcmd = FOI_LastCmd.i[1];   /* LastCmd will be cleared in foi_monitor */
                                    /* if there's a response */
      pthread_mutex_unlock(&VLock);
   } while (lastcmd && i < FOI_NBYTES);

   if (FOI_LastCmd.i[1] == 0) {
      if (IC.debug) fprintf(stderr, "foi_nullcmd      : Sent %d bytes.\n", i);
      msgprintf("Null command echo OK");
      return(0);
   }
   else {
      if (IC.debug) fprintf(stderr, "foi_nullcmd      : Sent %d bytes, no response.\n",i);
      msgprintf("Null command echo not received: try resetting FOI.");
      return(-1);
   }
   return(0);
}


/* Send DMA init chars for loopback initialization 
 * (Only used for testing code w/ loopback cable.)
 */

int foi_dmainit_loopback(void)
{
   if (IC.data_mode == REALDATA) {
      *(FIFO_Base + 0xc5) = 0x01;   /* Sp 0x01 */
      foi_delay(DELAY);
      *(FIFO_Base + 0xc6) = 0x0d;   /* Normal 0x0d */
      foi_delay(DELAY);
      *(FIFO_Base + 0xc5) = 0;      /* Sp 0x00 */
      foi_delay(DELAY);
   }
   return(0);
}


/* Send special command packet, including checksum, to prompt the
 * FPGA to send the proper bits to put FOI into DMA input mode.
 */

int foi_dmainit(void)
{
   int i;
   union _packet {
      unsigned char c[FOI_NBYTES+sizeof(int)];
      unsigned int i[FOI_NWORDS+1];
   } p;

   if (IC.data_mode != REALDATA) return(0);

   /* Construct packet */
   for (i=0; i<(FOI_NWORDS+1); i++) p.i[i] = 0;
   p.i[1] = INIT_DMA;
   for (i=0; i<FOI_NBYTES; i++) p.i[FOI_NWORDS] += (unsigned int)p.c[i];
   p.i[FOI_NWORDS] = p.i[FOI_NWORDS] << 16;

   if (IC.debug) {
      fprintf(stderr, "foi_dmainit      : Sending  SC%1d", CMD);
      for (i=0; i<FOI_NWORDS; i++) fprintf(stderr, " %08x", p.i[i]);
      fprintf(stderr, "\n");
   }

   /* Send characters */
   *(FIFO_Base + 0xc5) = CMD;     /* Special character */
   for (i=0; i<(FOI_NBYTES+2); i++) {
      foi_delay(DELAY);
      *(FIFO_Base + 0xc6) = p.c[i];
   }

   return(0);
}


/* Reset the FOI FIFO */

int foi_fiforeset(void)
{
  if (IC.data_mode == REALDATA) {
     *(FIFO_Base + 0x80) = 0x03;     /* Command reg: Reset the scdfoi */
     *(FIFO_Base + 0x82) = 0x11;     /* Config reg:  RAWOUT, DMAIN set */
#ifdef INCLUDE_EDT
     ioctl(EDT_fd->fd, SDV_PROG_FIFOS);
#endif
     if (IC.debug) fprintf(stderr, "foi_fiforeset    : FOI FIFO has been reset\n");
  }
  return(0);
}


/* Write a Command - Argument group to FOI or Fake Data pipe. */

int foi_cmd(unsigned int cmd, unsigned int arg1, unsigned int arg2, 
            unsigned int arg3)
{
   int i;
   foi_packet fcmd;

  /* Write raw data including special characters to remote device.
     0xc5 is an odd  address: data sent with "special character" bit on.
     0xc6 is an even address: data sent with "special character" bit off.*/

   fcmd.i[0] = FOI_VERIFY;
   fcmd.i[1] = cmd;
   fcmd.i[2] = arg1;
   fcmd.i[3] = arg2;
   if (FOI_NWORDS > 4) fcmd.i[4] = arg3;

   /* Debug messages */
   if (IC.debug) { 
      fprintf(stderr, "foi_cmd          : Sending  SC%1d", CMD);
      for (i=0; i<FOI_NWORDS; i++) fprintf (stderr, " %08x", fcmd.i[i]);
      fprintf(stderr, "\n");
   }

   /* Save the command to be sent */
   for (i=0; i<FOI_NWORDS; i++) FOI_LastCmd.i[i] = fcmd.i[i];
   if (!(IC.fpga_executing & fcmd.i[1])) IC.fpga_executing += fcmd.i[1];

   /* Send the command */
   if (IC.data_mode == REALDATA) {
      *(FIFO_Base + 0xc5) = CMD;     /* Special character */
      for (i=0; i<FOI_NBYTES; i++) {
         foi_delay(DELAY);
         *(FIFO_Base + 0xc6) = fcmd.c[i];
      }
   }
   else {
      write(Fake_Pipe_Out[1], &fcmd.c[0], FOI_NBYTES);
   }

   return(0);
}


/***** Monitor FOI input for incoming data ******/
/***** Runs in second thread.              ******/

void *foi_monitor(void *arg)
{
   int i, rdbytes, nframebytes, nscanbytes, nbytestoread;
   int ncycles, nframes, offset, bufindex, ntries, maxtries;
   int delaycount, match, lastcmd0, lastcmd1;
   foi_packet foi_buf;
   union _checksum {
      unsigned char c[4];
      unsigned short i[2];
   } checksum1, checksum2;

   for (;;) {

      /* Read the FOI device or Fake Data pipe */
      if (IC.data_mode == REALDATA)
#ifdef INCLUDE_EDT
         rdbytes = edt_read(EDT_fd, &foi_buf.c[0], FOI_NBYTES);
#else
         rdbytes = 0;
#endif
      else
         rdbytes = read(Fake_Pipe_In[0], &foi_buf.c[0], FOI_NBYTES);

      /* Were the expected number of bytes received? */
      if (rdbytes != FOI_NBYTES) {
         fprintf(stderr, "foi_monitor Error: Read returned %d bytes, was expecting %d\n",
            rdbytes, FOI_NBYTES);
         foi_resetvars();
         break;
      }
      else if (IC.debug) {
         fprintf(stderr, "foi_monitor      : Received    ");
         for (i=0; i<FOI_NWORDS; i++) fprintf(stderr, " %08x", foi_buf.i[i]);
         fprintf(stderr, " (%d bytes)\n", rdbytes);
      }

      /* Process a "verify command" packet */
      if (foi_buf.i[0] == FOI_VERIFY) {

         pthread_mutex_lock(&VLock);   /* Protect FOI_LastCmd */

         /* Compare sent and received packets */
         match = 1;
         for (i=0; i<FOI_NWORDS; i++) 
             if (foi_buf.i[i] != FOI_LastCmd.i[i]) match = 0;
         if (FOI_LastCmd.i[0] != FOI_VERIFY  )  match = 0;

         lastcmd0 = FOI_LastCmd.i[0];
         lastcmd1 = FOI_LastCmd.i[1];
         for (i=0; i<FOI_NWORDS; i++) FOI_LastCmd.i[i] = 0;

         pthread_mutex_unlock(&VLock);

         if (match == 0) {
            if (lastcmd0 == FOI_VERIFY) 
               fprintf(stderr, "foi_monitor Error: Transmitted and Echoed packets do not match\n");
            else
               fprintf(stderr, "foi_monitor Error: VERIFY packet received unexpectedly\n");
            foi_resetvars();
            *(FIFO_Base + 0xc5) = ABORT;
         }
  	   
         /* If received matches sent, calculate and send checksum */
         else {
            checksum1.i[0] = 0;
            for (i=0; i<FOI_NBYTES; i++) 
               checksum1.i[0] += (unsigned short)foi_buf.c[i];
        
            if (IC.debug) fprintf(stderr, 
               "foi_monitor      : Sending checksum %04x\n",
               checksum1.i[0]);
         
            delaycount = 0;
            if (IC.data_mode == REALDATA) {
               *(FIFO_Base + 0xc6) = checksum1.c[0];
               foi_delay(DELAY); 
               *(FIFO_Base + 0xc6) = checksum1.c[1];

            /***** Send extra integer for loopback fiber ***
             *  *(FIFO_Base + 0xc6) = checksum1.c[0];
             *  foi_delay(DELAY); 
             *  *(FIFO_Base + 0xc6) = checksum1.c[1];
             **** End loopback ****/

#ifdef INCLUDE_EDT
               rdbytes = edt_read(EDT_fd, &checksum2, 4);
#endif
            }
            else {
               write(Fake_Pipe_Out[1], (char *)&checksum1.c[0], 2);
               rdbytes = read(Fake_Pipe_In[0], &checksum2, 4);
            }

            if (IC.debug)
               fprintf(stderr, 
                  "foi_monitor      : Echoed, FPGA checksums = %04x, %04x\n",
                  checksum2.i[0], checksum2.i[1]);

            if (checksum1.i[0] != checksum2.i[0] ||
                checksum1.i[0] != checksum2.i[1]) {
	       if (IC.debug) fprintf(stderr,
                  "foi_monitor Error: Checksums don't match\n");
               foi_resetvars();
               *(FIFO_Base + 0xc5) = ABORT;
            }
         }
      
         /* Process a Take_Data command */
         if (foi_buf.i[1] == TAKE_DATA) {
            nframebytes = LastImage->ncols * LastImage->nrows * 4 * BYTESPERPIX;
            nscanbytes = 0;
            i = 0;
            foi_buf.i[0] = NEWFRAME;
            foi_buf.i[2] = 0;

            if (IC.int_mode == STDINT) ncycles = ArrayClock.ncycles;
            else ncycles = 1;
            nframes = ncycles * 2 * LastImage->nendptframes;

            /* Loop to read desired number of frames */
            for (i=0; i < nframes; i++) {
               bufindex = i%2;
               if (IC.debug) fprintf(stderr, 
                     "foi_monitor      : Awaiting %d bytes (frame %d/%d)\n", 
                     nframebytes, i+1, nframes);

               /* Read the frame.  The EDT_fd read should only be required once;
		* all 2 MB of a full frame will be acquired with 1 read.  The
                * fakedata read needs to be executed multiple times because only
		* so many bytes (e.g. 10240) can be held by the pipe at one time.
                */
               nbytestoread = nframebytes;
               offset = 0;
               ntries = 0;
               maxtries = 512;

               while (nbytestoread > 0 && ntries < maxtries) {
                  if (IC.data_mode == REALDATA) {
#ifdef INCLUDE_EDT
                     rdbytes = edt_read(EDT_fd, Raw_Buf[bufindex]+offset, nbytestoread);
#endif
                     if (rdbytes != nbytestoread) {
                        fprintf (stderr, "foi_monitor Error: Read %d bytes out of %d\n", 
                           rdbytes, nbytestoread);
                        if (rdbytes < 0) {               /* The FOI device is probably closed */
                           perror("foi_monitor Error");
                           break;
                        }
                     }
                  }
                  else {                     
                     rdbytes = read(Fake_Pipe_In[0], Raw_Buf[bufindex]+offset, nbytestoread);
                  }
                  nbytestoread = nbytestoread - MAX(rdbytes, 0);
                  offset = offset + MAX(rdbytes, 0);
                  ntries++;
               }

               nscanbytes += offset;
               if (IC.debug) fprintf(stderr, "foi_monitor      : %d bytes received.\n", offset);
	       if (offset != nframebytes)
                  msgprintf("FOI Error: Image data not of expected size");

	    /* Signal main thread that a new frame has arrived */
               foi_buf.i[1] = bufindex;
               write(FOI_Pipe[1], &foi_buf.i[0], 3*sizeof(int));

            } /* End of nframes loop */

         /* Signal main thread that the integration is complete */
            foi_buf.i[0] = INTEG_COMPLETE;
            foi_buf.i[1] = 0; 
            write(FOI_Pipe[1], &foi_buf.i[0], 3*sizeof(int));
            foi_buf.i[0] = 0;
            if (IC.debug) fprintf(stderr, "foi_monitor      : End of scan\n\n"); 

         }  /* End Take Data */

         /* Process FOI_NULL_CMD */
         else if (foi_buf.i[1] == FOI_NULL_CMD) {
            foi_resetvars();
	 }

      }  /* End Process Verify */


      /* Process INIT_DMA which may or may not have been echoed */

      else if (foi_buf.i[1] == INIT_DMA) {
	 if (IC.data_mode == REALDATA)
#ifdef INCLUDE_EDT
            rdbytes = edt_read(EDT_fd, &checksum2, 4);
#else
            rdbytes = 0;
#endif
         else 
            rdbytes = read(Fake_Pipe_In[0], &checksum2, 4);

         if (IC.debug) fprintf(stderr, 
               "foi_monitor      : Echoed, FPGA checksums = %04x, %04x\n",
               checksum2.i[0], checksum2.i[1]);
      }
      
      /* Process a null command */
      else if (SentZero == 1) {
         foi_resetvars();
      }

      /* Otherwise, write received packet to the pipe monitored by
         the main thread.  This is how MOTOR_STOP packets are handled. */

      else if (foi_buf.i[0] > 0 && foi_buf.i[0] < 10) {
         if (IC.debug) fprintf(stderr, 
            "foi_monitor      : Writing  %08x %08x %08x to FOI_Pipe\n", 
            foi_buf.i[0], foi_buf.i[1], foi_buf.i[2]);

         write(FOI_Pipe[1], &foi_buf.i[0], 3*sizeof(int));

         pthread_mutex_lock(&VLock);   /* Protect FOI_LastCmd */
         for (i=0; i<FOI_NWORDS; i++) FOI_LastCmd.i[i] = 0;
         pthread_mutex_unlock(&VLock);
      }

      pthread_mutex_lock(&ZLock);   /* Protect SentZero */
      SentZero = 0;
      pthread_mutex_unlock(&ZLock);

   }  /* End main loop */

   return((void *)NULL);
}


/* Create the thread containing the foi_monitor routine */

int foi_monitor_create(void)
{
   int status;
   char *ptr=NULL;

   status = pthread_create(&FOI_Monitor_Thread, NULL, foi_monitor, (void *)ptr);
   if (IC.debug) fprintf(stderr, "foi_monitor_creat: thread created\n");

   return(0);
}


/* Cancel the FOI Monitor thread */

int foi_monitor_cancel(void)
{
   int err;

   /* Cancel thread */
   err = pthread_cancel(FOI_Monitor_Thread); 
   if (err != 0) {
      fprintf(stderr, "foi_monitor_cancel: thread cancellation error: %d\n", err);
      return(-1);
   }

   /* Wait for thread to actually exit */
   err = pthread_join(FOI_Monitor_Thread, NULL);
   if (err != 0) {
      fprintf(stderr, "foi_monitor_cancel: thread join error = %d\n", err);
      return(-1);
   }

   if (IC.debug) fprintf(stderr, "foi_monitor_cancel: thread canceled\n");
   return(0);
}


int datamode_toggle(void)
{
   int status;
   char *ptr=NULL;
   char fderrmsg[] = "Error: Fake Data initialization failed\n";

   /* Cancel Monitor thread to interrupt blocked read */
   if (foi_monitor_cancel() < 0) {
      msgprintf("Error exiting FOI_Monitor thread");
      return(-1);
   }

   if (IC.data_mode == FAKEDATA) {
      if (fakedata_cancel() < 0) {
         msgprintf("Error exiting Fake Data Mode");
         return(-1);
      }
      if (sdvfoi_open(0) < 0) {
         printf("EDT FOI board not found, returning to fake data mode\n");
         if (fakedata_init() < 0) {
            fprintf(stderr, fderrmsg);
            exit(0);
         }
      }
   }
   else {
       sdvfoi_close();
       if (fakedata_init() < 0) {
          fprintf(stderr, fderrmsg);
          exit(0);
       }
   }

  /* Restart Monitor thread */
   sleep(1);

/* Start fiber monitor routine in another thread */
   status = pthread_create(&FOI_Monitor_Thread, NULL, foi_monitor, (void *)ptr);
   
   return(0);
}



void flipbits(int *inplace)  /* flip low 8 bits D7<==>D0, D6<==>D1, etc. */
{
  int i;
  unsigned char src = *inplace & 0xff;
  unsigned char dst = 0;

  for (i=0; i<8; i++)
    if (src & (1<<i)) dst |= 1 << (7-i);

  *inplace = dst;
}


/* Program the Pirger FPGA board over the FOI */

int fpga_program(char *ttfname)
{
   int count, val, delaycount;
   char *ptr;
   FILE *inf;
   char buf[512];

   if (IC.data_mode != REALDATA) return(0);

   inf = fopen(ttfname, "r");
   if (!inf) {
      msgprintf("Error: Can't open ttf file %s for reading\n", ttfname);
      return(-1);
   }

   count = 0;
   delaycount = 0;

   if (IC.debug) fprintf(stderr, "fpga_program     : Sending data from %s\n", ttfname);

   /* Send special character to initialize FPGA board
      for programming. */
   *(FIFO_Base + 0xc5) = PROG;
   foi_delay(PROGDELAY);

   /* Loop through lines of file */
   while (fgets(buf, sizeof buf, inf)) {

      /* Parse first token from current line */
      ptr = (char *)strtok(buf, " ,\r\n");
      if (!ptr) continue;

      /* Loop until no more tokens */
      do {
         count++;
         val = atoi(ptr);
         /* flipbits(&val); This was needed for 1st-generation FPGA board */

         /* Send character to FOI */
         *(FIFO_Base + 0xc6) = (char)(val&0xff);
         foi_delay(PROGDELAY);
      } while ((ptr = (char *)strtok(0, " ,\r\n")));
   }

   if (IC.debug) fprintf(stderr, "fpga_program     : Sent %d bytes\n", count);

   fclose(inf);
   return(0);
}


/* Check status of FPGA board */

int fpga_status(void)
{

  /* Still waiting for return of previous command by FPGA? */

   if (FOI_LastCmd.i[1] != 0) {
      msgprintf("Warning: Previous command %d not yet verified",
         FOI_LastCmd.i[1]);
      pthread_mutex_unlock(&VLock);
      return(0);
   }

  /* Check for commands currently executing in FPGA */
  /* For now, this limits us to one-command-at-at-time operation */

   if (IC.fpga_executing) {
      msgprintf("Warning: FPGA still executing previous command");
      pthread_mutex_unlock(&VLock);
      return(0);
   }

   return(1);
}


/* Send special char ABORT to halt an executing FPGA command and reset FPGA */

int fpga_abort(void)
{
   if (IC.data_mode == REALDATA) {
      if (IC.debug) fprintf(stderr, "fpga_abort       : Sending SC%1d\n", ABORT);
      *(FIFO_Base + 0xc5) = ABORT;
   }
   
   return(0);
}
