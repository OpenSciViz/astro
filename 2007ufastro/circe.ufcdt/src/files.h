#ifndef _files_h 
#define _files_h 

#include <stdio.h>

#define ROOTDIR "/.ufcdt/"
#define SETTINGSFILE "settings.dat"
#define FITSDIR "/scratch/home/amarin/data/circe"

typedef struct file_struct {
   char dir[80], prefix[16], suffix[8];
   int index, ndigits, max_index, nbytes;
   FILE *fd;
} file_struct;

/* functions */
extern int  openlog(void);
extern int  closelog(void);
extern void logprintf(char *fmt, ...);
extern void msgprintf(char *fmt, ...);

#endif
