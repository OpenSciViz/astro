/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: main.c -- ufCDT Main routine
 | 
 | DESCRIPTION
 |	This source code file contains the main ufCDT routine.
 |	Initialize many global variables and structures.
 |      Initialize GTK displays, shells, etc.
 |      Initialize I/O Hardware.
 |      Call main GTK app loop.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc main.c
 |
 *------------------------------------------------------------*/ 


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>

#include "main.h"
#include "interface.h"
#include "support.h"
#include "support2.h"
#include "fiber.h"
#include "files.h"
#include "cmdcodes.h"
#include "display.h"
#include "plot.h"

/* Instrument Status Structure */
INST_CONTROL IC = {
   FALSE, TRUE, FALSE,            /* tryfake, trynet, remote */
   FALSE, FALSE,                  /* debug, net_debug */
   FAKEDATA, STDINT, WRITE_DIFF,  /* data_mode, int_mode, write_mode */
   FALSE, FALSE, FALSE,           /* fpga, macro, remote executing */
   FALSE, AF_OFF,                 /* interval_active, auto_focus */
   FALSE, FALSE,                  /* net_connected, net_warn */
   FALSE,
   " "
};

/* GUI structure defined in support2.h */
INST_GUI GUI;

/* GDK graphics context */
GdkGC *GC_Main;

/* Remaining Integration time display */
gint IntervalID=0; 

/* Data buffers */
char *Raw_Buf[2];
int *Sort_Buf;
image SrcImage, BgdImage, FlatImage, *LastImage;

char Data_Dir[NAMESTRLEN]      = "/scratch/home/amarin/data/circe";      /* Current value saved in settings.dat */
char Object_Name[NAMESTRLEN]   = "";
char Observer_Name[NAMESTRLEN] = "amarin";
char Macro_Name[NAMESTRLEN]    = "";

float Pixelscale = 0.25/3600.;

/* Array clock structure */
arrayclock ArrayClock = 
   {512, 512,             /* ncols, nrows */
      0, 100,             /* t_frame, t_framegap (msec) */
      0,   0,   0,        /* scan, int, effective times (msec) */
      0,   0,   0,        /* quick scan, int, effective times (msec) */
      1,   0,   0,   1};  /* # endpt, pause, quick pause frames, #cycles */


/* Fiber Optic Interface pipes and thread */
int FOI_Pipe[2];
pthread_t FOI_Monitor_Thread;
gint FOI_Pipe_ID;

/* Fake Data pipes and thread */
int Fake_Pipe_Out[2];
int Fake_Pipe_In[2];
pthread_t Fake_Data_Thread;


int main (int argc, char **argv)
{
   int i, dumarg=0;
   int fulldisp_decor=FALSE, width;
   GtkWidget *wid;
   
   /* GTK initialization */
   gtk_init(&argc, &argv);

   /* GDK_RGB initialization */
   gdk_rgb_init();
   gtk_widget_set_default_visual (gdk_rgb_get_visual());
   gtk_widget_set_default_colormap (gdk_rgb_get_cmap());
    
   /* Process Command line args not stripped by gtk_init */
   if (argc > 1) {
      for (i=1; i<argc; i++) {
         if      (!strcmp(argv[i], "-fake" )) IC.tryfake = TRUE;
         else if (!strcmp(argv[i], "-nonet")) IC.trynet = FALSE;
         else if (!strcmp(argv[i], "-remote")) IC.remote = TRUE;
         else if (!strcmp(argv[i], "-fulldecor")) fulldisp_decor = TRUE;
         else if (!strcmp(argv[i], "-ncolors") && argc > (i+1)) {
            NColors_arg = atoi(argv[i+1]);
         }
      }
   }

   /* Check for Threads Compatibility */
   if (sysconf(_SC_THREADS) == -1) {
      printf("Error: Threads not supported!\n");
      exit(0);
   }

   /* Create the Windows */
   GUI.Message_window    = create_Message_window   ( );
   GUI.FullDisp_window   = create_FullDisp_window  ( );
   GUI.SubDisp_window    = create_SubDisp_window   ( );
   GUI.FITSSelect_window = create_FITSSelect_window( );
   GUI.Plot_window       = create_Plot_window      ( );
   
/* create_about_window    ( );
*/

   /* Initialize the remaining elements in the GUI structure */
   init_gui_struct();

   /* GDK graphics context */
   GC_Main = gdk_gc_new((GdkWindow*)GUI.SubDisp_window);
   
   /* Initialize the Main window */
   init_main();

   /* Display the SubDisp window */
   gtk_widget_show (GUI.SubDisp_window);

   /* Display the Plot window */
   gtk_widget_show (GUI.Plot_window);
   init_plot();
   
   /* Display the FullDisp window with or without decorations */
   wid = GUI.FullDisp_window;
   gtk_widget_realize (wid);

   if (fulldisp_decor) 
      gtk_widget_set_uposition(wid, 0, 0);
   else {
      width = gdk_screen_width();
      gtk_widget_set_uposition(wid, width-1280, 0);
   }

   gdk_window_set_decorations((GdkWindow *)wid->window, fulldisp_decor);
   gtk_widget_show (wid);
   init_display();
   

   /* Initialize fiber-monitoring Pipe.  Set X to call "foi_pipe" when 
    * there's activity on the pipe.
    */
   if (pipe(FOI_Pipe) < 0) {
      fprintf(stderr, "Error: Monitor Pipe not created.\n");
      exit(0);
   }
   FOI_Pipe_ID = gdk_input_add(FOI_Pipe[0], GDK_INPUT_READ, 
                     (GdkInputFunction)foi_pipe, (gpointer)dumarg);
    

   init_datadir();
   init_final();
   
   /* GTK main loop */
   gtk_main();

   return(0);
}

