/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Palomar High Angular Resolution Observer (PHARO)
 |      Cornell University, Ithaca, NY
 |
 | FILE: fits.c -- XPHARO FITS file management routines. 
 |
 | DESCRIPTION:
 |	This source code file contains the routines for
 |      managing creation of FITS files and reading and writing
 |      of headers and data.
 |
 | REVISION HISTORY:
 |   Date            By         Description
 | 1999-Jul-07       TLH 
 | 
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>

#include "fits.h"
#include "files.h"

#define KEYSTART    0    /* keyword */
#define KEYWIDTH    8      
#define VINDSTART   8    /* value indicator  */
#define VINDWIDTH   2
#define VINDICATOR "= "
#define VALUESTART 10    /* value */
#define VALUEWIDTH 20
#define VALUEEND   29
#define MINSTRVALWIDTH 8
#define COMMENTPOS 31

#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )

/* FITS file name structures */
fitsfile MainFits = {"", "circe", "fits", 0, -1, 4, 9999, 0};
fitsfile SubFits  = {"", "circesub", "fits", 0, -1, 4, 9999, 0};

static char blanks[] = "                                                                                ";

int createFITS(fitsfile *fits, int debug)
{
   char filename[120], format[32];
   int fp = 0;
   int index;
   unsigned int mbfree, mbthreshold=50;
   struct statvfs vfsbuf;
   unsigned int mbyte = 1048576;

   sprintf(format, "%%s/%%s%%0%dd.%%s", fits->ndigits);
   index = fits->index+1;

   /* Search for unique file name */
   do {
      if (fp) close(fp);
      sprintf(filename, format, fits->dir, fits->prefix,  
         index++, fits->suffix);
      fp = open(filename, O_RDONLY);
   } while (fp != -1 && (index < fits->max_index));

   if ((index == fits->max_index) && fp) {
      fprintf(stderr, "createFITS Error: Can't create unique file name");
      close(fp);
      return(-1);
   }

   /* Open file for writing */
   if (fp) close(fp);
   if ((fp = open(filename, O_CREAT|O_WRONLY)) == -1) {
      fprintf(stderr, "createFITS Error: can't open file %s for writing", filename);
      return(-1);
   }

   /* Check Disk Space */
   fstatvfs (fp, &vfsbuf);
   mbfree = ((unsigned long long)vfsbuf.f_frsize*(unsigned long long)vfsbuf.f_bavail)/mbyte;
   if (mbfree < mbthreshold) 
      msgprintf("WARNING: %u MBytes free space on %s", 
         mbfree, fits->dir);

   fits->index = index-1;
   fits->fp = fp;
   if (debug) fprintf(stderr, "createFITS: Opened %s\n", filename);

   if (fchmod(fits->fp, 0644)) {
      fprintf(stderr, "createFITS Error: can't modify data file permissions");
      return(-1);
   }

   return(0);
}



/* Close FITS file indicated by fs. */
/* nbytes = no. of data bytes in file before padding */
int closeoutFITS(fitsfile *fits, int nbytes, int debug)
{
   char zbuf[2880], format[32];
   int nwritten, nfillbytes;

   /* Make sure file is open */
   if (!fits->fp) {
      fprintf(stderr, "closeoutFITS: data file already closed\n");
      return(-1);
   }

   /* pad fits file to record boundary */
   nfillbytes = 2880 - (nbytes % 2880);
   bzero(zbuf, nfillbytes);
   nwritten = write(fits->fp, zbuf, nfillbytes);  

   close(fits->fp);
   fits->fp = 0;

   if (nwritten != nfillbytes) {
      fprintf(stderr, "closeoutFITS: Error padding FITS file\n");
      return(-1);
   }

   if (debug) {
      sprintf(format, "closeoutFITS: Closed %%s%%0%dd.%%s\n", fits->ndigits);
      fprintf(stderr, format, fits->prefix, fits->index, fits->suffix);
   }
   return(0);
}

          /* ********** blankCard printCard ************** */

void blankCard(char *str)
{
   bcopy(blanks, str, CARDWIDTH); 
}

          /* ********** .....CardImage's ************* */
void logicalCardImage(char *memdest, char *keyword, int val)
{ 
    char *d, *s;

    blankCard(memdest);

    d = &memdest[KEYSTART];
    s = keyword;
    while (*s)  *d++ = *s++;

    strncpy(&memdest[VINDSTART], VINDICATOR, 2);
    memdest[VALUEEND] = val ? 'T' : 'F';
    memdest[COMMENTPOS] = '/';
}

void integerCardImage(char *memdest, char *keyword, int val)  
{   
    char abuf[VALUEWIDTH], *d, *s;

    blankCard(memdest);

    d = &memdest[KEYSTART];
    s = keyword;
    while (*s)  *d++ = *s++;

    strncpy(&memdest[VINDSTART], VINDICATOR, 2);

    sprintf(abuf, "%20d", val);
    d = &memdest[VALUESTART];
    s = abuf;
    while (*s)  *d++ = *s++;

    memdest[COMMENTPOS] = '/';
}

void floatCardImage(char *memdest, char *keyword, float val, int dec)  
{ 
    char abuf[VALUEWIDTH+1], *d, *s;
    char format[32];

    blankCard(memdest);

    d = &memdest[KEYSTART];
    s = keyword;
    while (*s)  *d++ = *s++;

    strncpy(&memdest[VINDSTART], VINDICATOR, 2);

    sprintf(format, "%%20.%df", dec);
    sprintf(abuf, format, val);
    d = &memdest[VALUESTART];
    s = abuf;
    while (*s)  *d++ = *s++;

    memdest[COMMENTPOS] = '/';
}

void expCardImage(char *memdest, char *keyword, float val, int dec)  
{ 
    char abuf[VALUEWIDTH+1], *d, *s;
    char format[32];

    blankCard(memdest);

    d = &memdest[KEYSTART];
    s = keyword;
    while (*s)  *d++ = *s++;

    strncpy(&memdest[VINDSTART], VINDICATOR, 2);

    sprintf(format, "%%20.%de", dec);
    sprintf(abuf, format, val);
    d = &memdest[VALUESTART];
    s = abuf;
    while (*s)  *d++ = *s++;

    memdest[COMMENTPOS] = '/';
}

void stringCardImage(char *memdest, char *keyword, char *val)
{ 
   int i, slen;
   char *d, *s;

   blankCard(memdest);

   /* Keyword */    
   d = &memdest[KEYSTART];
   s = keyword;
   while (*s)  *d++ = *s++;

   /* = sign */
   strncpy(&memdest[VINDSTART], VINDICATOR, 2);

   /* Value surrounded by " ' " */
   d = &memdest[VALUESTART];
   s = val;
   slen = strlen(val);
   *d++ = '\'';
   while(*s) *d++ = *s++;
   for (i=slen; i<MINSTRVALWIDTH; i++) *d++ = ' '; 
   *d = '\'';

   /* "/" at end */
   memdest[MAX(VALUESTART+slen+3,COMMENTPOS)] = '/';
}

void annotateCardImage(char *card, char *comment)
{  
   card += COMMENTPOS;
   *card++ = '/';
   *card++ = ' ';
   while (*comment)  *card++ = *comment++;
}


/* Write FITS header data to open file */

int writeFITSheader(fitsfile *fits, char hdrbuf[][BUFWIDTH], int nlines)
{
   int i, retval, ncardimages = 0;
   char buf[82];

   if (!fits->fp) {
      fprintf(stderr, "writeFITSheader Error: Data file not open!\n");
      return(-1);
   }

   for (i=0; i<nlines; i++) {
      retval = write(fits->fp, hdrbuf[i], CARDWIDTH);
      if (retval != CARDWIDTH) return(-1);
      ncardimages++;
   }

   /* pad FITS logical record */
   blankCard(buf); 
   while (ncardimages % 36) {
      retval = write(fits->fp, buf, CARDWIDTH);
      if (retval != CARDWIDTH) return(-1);
      ncardimages++;
   }

   return(0);
}


/* Write FITS data to open file */

int writeFITSdata(fitsfile *fits, int *imdata, int nbytes, int debug)
{  
   int nwritten = 0;

   if (!fits->fp) {  
      fprintf(stderr, "writeFITSdata Error: FITS file not open!");
      return(-1);
   }

   nwritten = write(fits->fp, imdata, nbytes);

   if (nwritten < nbytes) {
      fprintf(stderr, "writeFITSdata Error: Requested %d, wrote %d bytes",
         nbytes, nwritten);
      return(-1);
   }

   if (debug)
      fprintf(stderr, "writeFITSdata: wrote %d bytes\n", nbytes);

   return(0);
}


/* Open existing FITS file for reading */

int openFITS(char *filename)
{
   int fp;

   if ( (fp = open(filename, O_RDONLY)) == -1) {
      fprintf(stderr, "openFITS error: Can't open %s\n", filename);
      return(RDFITS_NOOPN);
   }
   MainFits.fp = fp;
   return(0);
}


/* Close an open FITS file */

int closeFITS(void)
{
   /* Make sure file is open */
   if (!MainFits.fp) {
      fputs("closeFITS: data file already closed\n", stderr);
      return(-1);
   }
   close(MainFits.fp);
   MainFits.fp = 0;
   return(0);
}


/* Read a FITS header */

int readFITShdr(char hdrbuf[][BUFWIDTH], int maxlines, int *nlines)
{
   int i, nread;

   /* Make sure file is open */
   if (!MainFits.fp) {
      fputs("readFITShdr: data file is closed.\n", stderr);
      return(-1);
   }

   for (i=0; i<maxlines; i++) {
      if ((nread = read(MainFits.fp, hdrbuf[i], 80)) != 80) {
         fprintf(stderr, "readFITShdr error: bad header at line %d.\n", i);
         return(RDFITS_BADHDR);
      }
      hdrbuf[i][BUFWIDTH-1] = '\0';
      if (!strncmp(hdrbuf[i], "END ", 4)) break;
   }

   if (i == maxlines) {
      fputs("readFITShdr error: Cannot find END in header.\n", stderr);
      return(RDFITS_BADHDR);
   }

   *nlines = i;

   return(0);
}


/* Read FITS data */

int readFITSdata(int nhdrlines, int nbytes, char *databuf)
{
   int hrecords, offset;

   hrecords = nhdrlines/36 + 1;
   offset = 2880*hrecords;

   /* Set pointer to first data record */
   if (lseek(MainFits.fp, offset, SEEK_SET) < 0) {
      fputs("readFITSdata error seeking image data.\n", stderr);
      return(RDFITS_BADDATA);
   }

   /* Read the data */
   if (read(MainFits.fp, databuf, nbytes) < 0) { 
      fputs("readFITSdata error reading data.\n", stderr);
      return(RDFITS_BADDATA);
   }
   return(0);
}


/* Read an integer value from FITS header */

int getFITSint (char hdrbuf[][BUFWIDTH], char *keyword, int *value)
{
   int i, nchars;
   char *ptr;

   i = 0;
   nchars = strlen(keyword);

   while (1) {
      ptr = (char *)strtok(hdrbuf[i], " ");
      if (ptr && !strncmp(ptr, "END ", 4)) {
         fputs("getFITSint error: keyword not present\n", stderr);
         return(-1);
      }
      if (ptr && !strncmp(ptr, keyword, nchars)) {
          *value = atoi(strtok(hdrbuf[i]+10, " /"));
          break;
      }
      i++;
   }

   return(0);
}


/* Read an exponential floating point value from FITS header */
int getFITSexp (char hdrbuf[][BUFWIDTH], char *keyword, float *value)
{
   int i, nchars;
   char *ptr, *strptr;

   i = 0;
   nchars = strlen(keyword);

   while (1) {
      ptr = (char *)strtok(hdrbuf[i], " ");
      if (ptr && !strncmp(ptr, "END ", 4)) {
         fputs("getFITSint error: keyword not present\n", stderr);
         return(-1);
      }
      if (ptr && !strncmp(ptr, keyword, nchars)) {
          strptr  = strtok(hdrbuf[i]+10, " /");
          sscanf(strptr, "%e", value);
          break;
      }
      i++;
   }

   return(0);
}
