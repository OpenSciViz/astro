#ifndef _fits_h
#define _fits_h

#define CARDWIDTH  80
#define BUFWIDTH   81

/* Error codes */
#define RDFITS_NOOPN     1
#define RDFITS_BADHDR    2
#define RDFITS_BADBITPIX 3
#define RDFITS_BADNAXIS  4
#define RDFITS_BADDIM    5
#define RDFITS_BADDATA   6

typedef struct _fitsfile {
   char dir[80], prefix[16], suffix[8];
   int fp, index, ndigits, max_index, nbytes;
} fitsfile;

extern fitsfile MainFits, SubFits;

/* functions */
extern int   createFITS(fitsfile *, int);
extern int   closeoutFITS(fitsfile *, int, int);
extern void  blankCard(char *);
extern void  logicalCardImage(char *, char *, int);
extern void  integerCardImage(char *, char *, int);
extern void  floatCardImage(char *, char *, float, int);
extern void  expCardImage(char *, char *, float, int);
extern void  stringCardImage(char *, char *, char *);
extern void  annotateCardImage(char *, char *);
extern int   writeFITSheader(fitsfile *, char hdrstr[][BUFWIDTH], int);
extern int   writeFITSdata(fitsfile *, int *, int, int);
extern int   openFITS(char *);
extern int   closeFITS(void);
extern int   readFITShdr(char hdrbuf[][BUFWIDTH], int, int *);
extern int   readFITSdata(int, int, char*);
extern int   getFITSint(char hdrbuf[][BUFWIDTH], char *, int *);
extern int   getFITSexp(char hdrbuf[][BUFWIDTH], char *, float *);

#endif
