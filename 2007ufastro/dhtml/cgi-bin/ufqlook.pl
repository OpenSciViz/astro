#!/usr/local/bin/perl -w

BEGIN { require 'uffrm.plib'; }

my $rcsId = '$Name:  $ $Id: ufqlook.pl 14 2008-06-11 01:49:45Z hon $';
#my $htdpath = $cgi->param('data');
my $htdpath = "/trecs/data/test/";
my $dpath = $ENV{DOCUMENT_ROOT};
if( ! $dpath ) { 
  $dpath = "/data/test/";
}
else {
  $dpath = $dpath . $htdpath;
}

uffrm::display($dpath, $htdpath, 2);

