#!/usr/local/bin/perl -w
use strict;
use CGI;
use CGI::Carp qw( fatalsToBrowser );

my $rcsId = '$Name:  $ $Id: ufdatalist.pl 14 2008-06-11 01:49:45Z hon $';
my $htdpath = shift;
my $cgi = new CGI;
# note that the data file directory root is the htdocs root:
if( ! $htdpath ) { $htdpath = "/trecs/data/test/"; }
my $dpath = $ENV{DOCUMENT_ROOT} . $htdpath;

# get all entrees in the directory:
opendir DIRH, $dpath;
my @dir = readdir DIRH;                                                    
closedir DIRH;
# reverse sort:
my @jpg = sort { $b cmp $a } grep /.jpg/, @dir;
my $jc = @jpg;
my $doctitle = "UF Instrument Frames:";
print $cgi->header( -type => "text/html", -expires => "now");
print $cgi->start_html( -title=> "$doctitle", -bgcolor=> "#ffffff");
my $j;
foreach $j ( @jpg ) {
  my $jmg = $htdpath . $j;
  print $cgi->p("");
  print $cgi->a( { -href => "$jmg", -target => "output" }, "$jmg" );
} 
print $cgi->end_html;
 
