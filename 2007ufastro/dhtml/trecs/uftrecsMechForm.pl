#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufmech;

$ufmech::rcsId = q($Name:  $ $Id: uftrecsMechForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufmech::cgi = new CGI;
$ufmech::organization = "University of Florida";
$ufmech::department = "Department of Astronomy";
$ufmech::refresh = 10;
$ufmech::doctitle = "TReCS Cryostat Environment";

print $ufmech::cgi->header( -type => "text/html", -expires => "now");
print $ufmech::cgi->start_html( -title=> "$ufmech::doctitle", -bgcolor=> "#ffffff" );
($ufmech::s,$ufmech::m,$ufmech::h,$ufmech::md,$ufmech::mo,$ufmech::y,$ufmech::wd,$ufmech::yd,$ufmech::isd) = localtime(time());
$ufmech::y = $ufmech::y + 1900; $ufmech::yd = $ufmech::yd + 1;
if( $ufmech::s < 10 ) { $ufmech::s = "0$ufmech::s"; }
if( $ufmech::m < 10 ) { $ufmech::m = "0$ufmech::m"; }
if( $ufmech::h < 10 ) { $ufmech::h = "0$ufmech::h"; }
$ufmech::output = "$ufmech::y:$ufmech::yd:$ufmech::h:$ufmech::m:$ufmech::s TReCS Environment (10sec. Updates):\n";
print $ufmech::cgi->p("$ufmech::output");
$ufmech::output = "==========================================\n";
print $ufmech::cgi->p("$ufmech::output");
$ufmech::cgi->end_html();
