#!/share/local/bin/perl
#
$rcsId = '$Name:  $ $Id: Flam2EnvStatAgents.cgi,v 0.0 2004/03/05 15:16:38 hon Exp $';
#
use lib "$UFINSTALL/perl";
use CGI;
use CGI::Carp qw(fatalsToBrowser);

#package flam2;

$cgi = new CGI;
$organization = "University of Florida";
$department = "Department of Astronomy";
$refresh = 30;
$doctitle = "UF Flamingos-2 Status";
$instrum = "flam";

print $cgi->header( -type => "text/html", -expires => "now", -refresh => "$refresh" );
print $cgi->start_html( -title=> "$doctitle", -bgcolor=> "#ffffff" );
($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime(time());
$y = $y + 1900; $yd = $yd + 1;
if( $s < 10 ) { $s = "0$s"; }
if( $m < 10 ) { $m = "0$m"; }
if( $h < 10 ) { $h = "0$h"; }
$output = "$y:$yd:$h:$m:$s $doctitle ($refresh sec. Updates):\n";
print $cgi->p("$output");
$output = "Software version: $rcsId\n";
print $cgi->p("$output");
$separate = "==============================================================\n";
print $cgi->p("$separate");

agentstat();

print $cgi->p("$separate");
$output = "Flamingos-2 SubSystem Agents Heartbeat from $flam2a";
print $cgi->p("$output");
$cgi->end_html();

############################################# sub agentstat() #########################################
sub agentstat {
$run = "env LD_LIBRARY_PATH=/share/local/uf/lib:/share/local/lib:/gemini/epics/lib/solaris-sparc-gnu:/opt/EDTpdv";
$pfvac = `$run/ufvac -port 52004 -host $flam2a -q -raw PRX`;
chomp $pfvac;
@vac = split / /,$pfvac;
$output = "MOS and Detector Cryostat Vacuum (Torr):     $vac[0], $vac[1]";
print $cgi->p("$output");
#
$ls342a = `$run/uflsc -port 52003 -host $flam2a -q -raw 'krdg?a'`;
$ls342b = `$run/uflsc -port 52003 -host $flam2a -q -raw 'krdg?b'`;
chomp $ls332a; chomp $ls332a;
chomp $ls332b; chomp $ls332b;
@cryotemp2 = [$ls332a, $ls332b];
$ls332a = `$run/uflsc -port 52003 -host $flam2a -q -raw 'srdg?a'`;
$ls332a = `$run/uflsc -port 52003 -host $flam2a -q -raw 'srdg?b'`;
chomp $ls332a; chomp $ls332a;
chomp $ls332b; chomp $ls332b;
@cryosens2 = [$ls332a, $ls332b];
$output = "Detector Array (Kelvin):           $cryotemp2[0] ($cryosens2[0] mV)\n";
print $cgi->p("$output");
$output = "Cold Plate (Kelvin):         $cryotemp2[1] ($cryosens2[1] mV)\n";
print $cgi->p("$output");
#
$ls218 = `$run/uflsc -port 52002 -host $flam2a -q -raw 'krdg?0'`;
chomp $ls218;
@cryotemp8 = split /,/,$ls218;
$output = "CryoCooler Stage1 (Kelvin):       $cryotemp8[0]\n";
print $cgi->p("$output");
$output = " Cold Plate (Kelvin):       $cryotemp8[1]\n";
print $cgi->p("$output");
$output = "Grism Turret (Kelvin):       $cryotemp8[2]\n";
print $cgi->p("$output");
#$output = "Passive Shield (Kelvin):      $cryotemp8[3]\n";
#print $cgi->p("$output");
#$output = "Slit Wheel Motor (Kelvin):       $cryotemp8[4]\n";
#print $cgi->p("$output");
#$output = "MOS Wheel Motor (Kelvin):  $cryotemp8[5]\n";
#print $cgi->p("$output");
#$output = "Cold Surface Edge (Kelvin):   $cryotemp8[6]\n";
#print $cgi->p("$output");
#$output = "Cold Surface Center (Kelvin): $cryotemp8[7]";
#print $cgi->p("$output");

}
