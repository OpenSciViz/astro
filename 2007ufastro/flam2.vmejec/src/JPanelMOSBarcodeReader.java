package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class JPanelMOSBarcodeReader extends JPanel{

    String epicsName;
    JCheckBox useSteps;
    JLabel mosStateLabel;
    EPICSTextField stepField;
    BRFlowState [] brFlowStates;

    public static final int RESTING = 0;
    public static final int MOVING = 1;
    public static final int WAITING = 2;
    public static final int ERROR = 3;
    public static final int DATUMING = 4;

    public static final int APPLYC = 1;
    public static final int DATUMC = 2;
    public static final int SETUPC = 4;
    public static final int SETUPMOSC = 8;

    private int STATEFLAG;

    private /*inner*/ class BRFlowState {
	int steps;
	String name;
	JCheckBox nameBox;
	JButton moveButton;
	JLabel brLabel;
	EPICSLabel barcodeReading;
	JLabel boLabel;
	JTextField barcodeOverride;
	JLabel observationProgramIDLabel;
	JTextField observationProgramID;
	JButton acceptButton;
	JPanel myPanel;
	
	int state;
  
	public BRFlowState [] brFSptr;

	public BRFlowState(String name, int steps) {
	    this.steps = steps;
	    this.name = name;
	    state = RESTING;
	    nameBox = new JCheckBox(name,false);
	    nameBox.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
			moveButton.setVisible(nameBox.isSelected());
			brLabel.setVisible(nameBox.isSelected());
			barcodeReading.setVisible(nameBox.isSelected());
			boLabel.setVisible(nameBox.isSelected());
			barcodeOverride.setVisible(nameBox.isSelected());
			observationProgramIDLabel.setVisible(nameBox.isSelected());
			observationProgramID.setVisible(nameBox.isSelected());
			acceptButton.setVisible(nameBox.isSelected());
		    }
		});
	    
	    moveButton = new JButton("Move");
	    moveButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
			moveButtonActionPerformed();
		    }
		});
	    brLabel = new JLabel("Barcode Reading:");
	    barcodeReading = new EPICSLabel(EPICS.prefix+"sad:"+name,"");
	    boLabel = new JLabel("Barcode Override:");
	    barcodeOverride = new JTextField("",10);
	    observationProgramIDLabel = new JLabel("ObsProgID:");
	    observationProgramID = new JTextField();
	    acceptButton = new JButton("Accept");
	    acceptButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent ae) {
			acceptButtonActionPerformed();
		    }
		});
	    myPanel= new JPanel();
	    myPanel.setLayout(new GridLayout(1,0));
	    myPanel.add(nameBox);
	    myPanel.add(moveButton);
	    myPanel.add(brLabel);
	    myPanel.add(barcodeReading);
	    myPanel.add(boLabel);
	    myPanel.add(barcodeOverride);
	    myPanel.add(observationProgramIDLabel);
	    myPanel.add(observationProgramID);
	    myPanel.add(acceptButton);
	    nameBox.setSelected(false);
	    visibleelbisiv(nameBox.isSelected());
	}
	
	private void visibleelbisiv(boolean seeme) {
	    moveButton.setVisible(seeme);
	    brLabel.setVisible(seeme);
	    barcodeReading.setVisible(seeme);
	    boLabel.setVisible(seeme);
	    barcodeOverride.setVisible(seeme);
	    observationProgramIDLabel.setVisible(seeme);
	    observationProgramID.setVisible(seeme);
	    acceptButton.setVisible(seeme);
	}

	private void moveButtonActionPerformed() {
	    state = MOVING;
	    nameBox.setEnabled(false);
	    moveButton.setEnabled(false);
	    //barcodeReading.setText("Moving...");
	    boLabel.setEnabled(false);
	    barcodeOverride.setEnabled(false);
	    acceptButton.setEnabled(false);
	    for (int i=0; i<brFSptr.length; i++) {
		if (!brFSptr[i].name.equals(name)) {
		    brFSptr[i].disableAll();
		}
	    }
	    // set movement direction to positive
	    EPICS.put(EPICS.prefix+"cc:setup.MOSBarCdDirection","+");
	    // put position name into epics rec for movement
	    if (!useSteps.isSelected())
		EPICS.put(EPICS.prefix+"cc:setup.MOSBarCd",name);
	    EPICS.put(EPICS.applyPrefix+"apply.dir",EPICS.START);
	}

	private void acceptButtonActionPerformed() {
	    if (!barcodeOverride.getText().trim().equals( "")) {
		String rd = barcodeReading.getText();
		String nd = barcodeOverride.getText();
		if (JOptionPane.showConfirmDialog(null,"Override reading: "+rd+" with this: "+nd+" ?") == JOptionPane.YES_OPTION) {
		    EPICS.put(EPICS.prefix+"sad:"+name,nd);
		}
	    }
	    nameBox.setEnabled(true);
	    for (int i=0; i<brFSptr.length; i++) {
		if (!brFSptr[i].name.equals(name)) {
		    brFSptr[i].enableAll();
		}
	    }
	    nameBox.setSelected(false);
	    visibleelbisiv(nameBox.isSelected());
	}

	public JPanel getPanel() {
	    return myPanel;
	}
	
	public int getState() {
	    return state;
	}

	public void resetStateOnAbort() {
	    //barcodeReading.setText("Aborted");
	    state = RESTING;
	}

	public void enableAccept() {
	    acceptButton.setEnabled(true);
	}

	public void disableAll() {
	    nameBox.setEnabled(false);
	    moveButton.setEnabled(false);
	    brLabel.setEnabled(false);
	    barcodeReading.setEnabled(false);
	    boLabel.setEnabled(false);
	    barcodeOverride.setEnabled(false);
	    observationProgramIDLabel.setEnabled(false);
	    observationProgramID.setEnabled(false);
	    acceptButton.setEnabled(false);
	}

	public void enableAll() {
	    nameBox.setEnabled(true);
	    moveButton.setEnabled(true);
	    brLabel.setEnabled(true);
	    barcodeReading.setEnabled(true);
	    boLabel.setEnabled(true);
	    barcodeOverride.setEnabled(true);
	    observationProgramIDLabel.setEnabled(true);
	    observationProgramID.setEnabled(true);
	    acceptButton.setEnabled(true);
	}

    }

    public JPanelMOSBarcodeReader(String filename) {
	STATEFLAG=0;
	setLayout(new RatioLayout());
	readFile(filename);
	JPanel flowPanel = new JPanel();
	//JButton triggerButton = new JButton("Trigger");
	JButton abortButton = new JButton("Abort");
	JButton datumButton = new JButton("Datum MOS");
	JButton exitSessionButton =new JButton("Exit Session");
	useSteps = new JCheckBox("Use steps: ",false);
	stepField = new EPICSTextField(EPICS.prefix+"cc:setup.MOSBarCdStep","");
	stepField.setEnabled(useSteps.isSelected());
	useSteps.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    useStepsActionPerformed();
		}
	    });
	datumButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:datum.MOS",EPICS.PRESET);
		    //EPICS.put(EPICS.prefix+"cc:datum.MOSBacklash",0);
		    //EPICS.put(EPICS.prefix+"cc:datum.MOSDirection",0);
		    //EPICS.put(EPICS.prefix+"cc:datum.MOSVel",100);
		    EPICS.put(EPICS.applyPrefix+"apply.dir",EPICS.START);
		}
	    });
	abortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    for (int i=0; i<brFlowStates.length; i++) {
			if (brFlowStates[i].getState() != RESTING)
			    brFlowStates[i].resetStateOnAbort();
			brFlowStates[i].enableAll();
		    }
		}
	    });
	exitSessionButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    for (int i=0; i<brFlowStates.length; i++)
			brFlowStates[i].enableAll();
		    //must datum mos wheel, then send it home
		    EPICS.put(EPICS.prefix+"cc:datum.MOS",EPICS.PRESET);
		    EPICS.put(EPICS.applyPrefix+"apply.dir",EPICS.START);
		    //wait for motion completion?
		    //home?
		    
		}
	    });
	new EPICSCarCallback(EPICS.prefix+"cc:setupC",new EPICSCarListener() {
		public void carTransition(int x) {
		    if (x == EPICS.IDLE) {
			STATEFLAG &= ~SETUPC;
			updateMosStateLabel(RESTING);
			if (STATEFLAG == 0) {
			    setAllEnabled();
			}
		    } else if (x == EPICS.BUSY) {
			STATEFLAG |= SETUPC;
			setAllDisabled();
			updateMosStateLabel(MOVING);
		    } else if (x == EPICS.ERROR) {
			setAllEnabled();
			updateMosStateLabel(ERROR);
			setErrorMsg(EPICS.prefix+"cc:setupC");
		    } else {
			System.err.println("JPanelMOSBarcodeReader> Unknown CAR value for cc:setupC --> "+x);
		    }
		}
	    });
	new EPICSCarCallback(EPICS.applyPrefix+"applyC",new EPICSCarListener() {
		public void carTransition(int x) {
		    if (x == EPICS.IDLE) {
			STATEFLAG &= ~APPLYC;
			updateMosStateLabel(RESTING);
			if (STATEFLAG == 0) {
			    setAllEnabled();
			}
		    } else if (x == EPICS.BUSY) {
			STATEFLAG |= APPLYC;
			setAllDisabled();
			updateMosStateLabel(MOVING);
		    } else if (x == EPICS.ERROR) {
			setAllEnabled();
			updateMosStateLabel(ERROR);
			setErrorMsg(EPICS.applyPrefix+"applyC");
		    } else {
			System.err.println("JPanelMOSBarcodeReader> Unknown CAR value for"+EPICS.applyPrefix+"applyC --> "+x);			
		    }
		}
	    });
	new EPICSCarCallback(EPICS.prefix+"cc:setupMOSC",new EPICSCarListener() {
		public void carTransition(int x) {
		    if (x == EPICS.IDLE) {
			STATEFLAG &= ~SETUPMOSC;
			updateMosStateLabel(RESTING);
			if (STATEFLAG == 0) {
			    //we've transitioned from busy to idle, and this is the barcode car, so
			    //presumably we want to press the 'accept' button, after possibly 
			    //editing the manual override field
			    setAcceptEnabled();
			}
		    } else if (x == EPICS.BUSY) {
			STATEFLAG |= SETUPMOSC;
			setAllDisabled();
			updateMosStateLabel(WAITING);
		    } else if (x == EPICS.ERROR) {
			setAllEnabled();
			updateMosStateLabel(ERROR);
			setErrorMsg(EPICS.prefix+"cc:setupMOSC");
		    } else {
			System.err.println("JPanelMOSBarcodeReader> Unknown CAR value for"+EPICS.prefix+"cc:setupMOSC --> "+x);
		    }
		}
	    });
	new EPICSCarCallback(EPICS.prefix+"cc:datumC",new EPICSCarListener() {
		public void carTransition(int x) {
		    if (x == EPICS.IDLE) {
			STATEFLAG &= ~DATUMC;
			updateMosStateLabel(RESTING);
			if (STATEFLAG ==0) {
			    setAllEnabled();
			}
		    } else if (x == EPICS.BUSY) {
			STATEFLAG |= DATUMC;
			setAllDisabled();
			updateMosStateLabel(DATUMING);
		    } else if (x == EPICS.ERROR) {
			setAllEnabled();
			updateMosStateLabel(ERROR);
			setErrorMsg(EPICS.prefix+"cc:datumC");
		    } else {
			System.err.println("JPanelMOSBarcodeReader> Unknown CAR value for"+EPICS.prefix+"cc:datumC --> "+x);
		    }
		}
	    });
	JButton exitButton = new JButton("Exit Session");
	exitButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    
		}
	    });
	mosStateLabel = new JLabel("Resting");
	flowPanel.setLayout(new GridLayout(0,1));
	for (int i=0; i<brFlowStates.length; i++)
	    flowPanel.add(brFlowStates[i].getPanel());
	add("0.01,0.01;0.20,0.08",new JLabel("Current Wheel Position:"));
	add("0.21,0.01;0.20,0.08",new EPICSLabel(EPICS.prefix+"sad:MOSSteps",""));
	
	//add("0.41,0.01;0.14,0.08",triggerButton);
        add("0.56,0.01;0.14,0.08",datumButton);
        add("0.71,0.01;0.14,0.08",abortButton);
	add("0.86,0.01;0.14,0.08",exitButton);
	add("0.01,0.11;0.20,0.08",new JLabel("Select positions to (re)set plates"));
	add("0.41,0.11;0.10,0.08",new JLabel("Current State:"));
	add("0.51,0.11;0.20,0.08",mosStateLabel);
	add("0.71,0.11;0.10,0.08",useSteps);
	add("0.81,0.11;0.20,0.08",stepField);
	add("0.01,0.22;0.99,0.70",flowPanel);
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("cc:"));
    }

    public void setErrorMsg(String carName) {
	String s = EPICS.get(carName+".OMESS");
	mosStateLabel.setText("Error: "+s);
    }

    public void updateMosStateLabel(int state) {
	if (STATEFLAG != 0) {
	    if ((STATEFLAG & APPLYC) != 0) mosStateLabel.setText("System is busy");
	    if ((STATEFLAG & DATUMC) != 0) mosStateLabel.setText("Datum-ing");
	    if ((STATEFLAG & SETUPMOSC)!=0)mosStateLabel.setText("Waiting for barcode reading...");
	    if ((STATEFLAG & SETUPC) != 0) mosStateLabel.setText("Moving to position...");
	} 
	else if (state == RESTING) mosStateLabel.setText("Resting");
	else if (state == MOVING) mosStateLabel.setText("Moving to position...");
	else if (state == WAITING)mosStateLabel.setText("Waiting for barcode reading...");
	else if (state == ERROR) mosStateLabel.setText("Error");
	else if (state == DATUMING) mosStateLabel.setText("Datum-ing");
    }

    public void useStepsActionPerformed() {
	stepField.setEnabled(useSteps.isSelected());
    }

    public void setAllEnabled(boolean enable) {
	for (int i=0; i<brFlowStates.length; i++) {
	    if (enable) brFlowStates[i].enableAll();
	    else brFlowStates[i].disableAll();
	}
    }

    public void setAcceptEnabled() {
	for (int i=0; i<brFlowStates.length; i++) {
	    brFlowStates[i].enableAccept();
	}
    }

    public void setAllEnabled() { setAllEnabled(true);}
    public void setAllDisabled(){ setAllEnabled(false);}

    public void readFile(String filename) {
	TextFile tf = new TextFile(filename,TextFile.IN);
	epicsName = EPICS.prefix + tf.nextUncommentedLine();
	int num_pos = Integer.parseInt(tf.nextUncommentedLine());
	brFlowStates = new BRFlowState[num_pos];
	for (int i=0; i<num_pos; i++) {
	    StringTokenizer st = new StringTokenizer(tf.nextUncommentedLine());
	    int steps  = Integer.parseInt(st.nextToken());
	    //st.nextToken();
	    String positionName = "";
	    while (st.hasMoreTokens()) 
		positionName += st.nextToken();
	    brFlowStates[i] = new BRFlowState(positionName,steps);
	    brFlowStates[i].brFSptr = brFlowStates;
	}
    }

}
