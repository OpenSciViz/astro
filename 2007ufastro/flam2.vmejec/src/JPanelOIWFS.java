package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class JPanelOIWFS extends JPanel {

    public JPanelOIWFS() {
	
	String [] tmp = {"NONE","MIN","FULL"};
	EPICSComboBox debugLevelBox = new EPICSComboBox(EPICS.OIWFSprefix+"debug.A",tmp,EPICSComboBox.ITEM);

	EPICSTextField moveXField = new EPICSTextField(EPICS.OIWFSprefix+"move.A","");
	EPICSTextField moveYField = new EPICSTextField(EPICS.OIWFSprefix+"move.B","");
	EPICSTextField tolXYField = new EPICSTextField(EPICS.OIWFSprefix+"tolerance.A","");
	EPICSTextField tolZField  = new EPICSTextField(EPICS.OIWFSprefix+"tolerance.B","");

	JPanel debugPanel = new JPanel();
	debugPanel.setLayout(new GridLayout(0,1));
	debugPanel.add(new JLabel("Debug Level",JLabel.CENTER));
	debugPanel.add(debugLevelBox);
	debugPanel.setBorder(new EtchedBorder(0));

	JPanel moveXPanel = new JPanel();
	moveXPanel.setLayout(new RatioLayout());
	moveXPanel.add("0.01,0.01;0.50,0.99",new JLabel("X-axis position:"));
	moveXPanel.add("0.51,0.01;0.30,0.99",moveXField);
	moveXPanel.add("0.81,0.01;0.20,0.99",new JLabel("(mm)"));
	JPanel moveYPanel = new JPanel();
	moveYPanel.setLayout(new RatioLayout());
	moveYPanel.add("0.01,0.01;0.50,0.99",new JLabel("Y-axis position:"));
	moveYPanel.add("0.51,0.01;0.30,0.99",moveYField);
	moveYPanel.add("0.81,0.01;0.20,0.99",new JLabel("(mm)"));
	JPanel movePanel = new JPanel();
	movePanel.setLayout(new GridLayout(0,1));
	movePanel.add(new JLabel("Probe Movement",JLabel.CENTER));
	movePanel.add(moveXPanel);
	movePanel.add(moveYPanel);
	movePanel.setBorder(new EtchedBorder(0));

	JPanel tolXYPanel = new JPanel();
	tolXYPanel.setLayout(new RatioLayout());
	tolXYPanel.add("0.01,0.01;0.50,0.99",new JLabel("Tolerance in X&Y"));
	tolXYPanel.add("0.51,0.01;0.30,0.99",tolXYField);
	tolXYPanel.add("0.81,0.01;0.20,0.99",new JLabel("(mm)"));	
	JPanel tolZPanel = new JPanel();
	tolZPanel.setLayout(new RatioLayout());
	tolZPanel.add("0.01,0.01;0.50,0.99",new JLabel("Tolerance in Z"));
	tolZPanel.add("0.51,0.01;0.30,0.99",tolZField);
	tolZPanel.add("0.81,0.01;0.20,0.99",new JLabel("(mm)"));	
	JPanel tolPanel = new JPanel();
	tolPanel.setLayout(new GridLayout(0,1));
	tolPanel.add(new JLabel("Tolerance",JLabel.CENTER));
	tolPanel.add(tolXYPanel);
	tolPanel.add(tolZPanel);
	tolPanel.setBorder(new EtchedBorder(0));

	JButton datumButton = new JButton("Datum");
	datumButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"datum.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"datum.DIR",EPICS.START);
		}
	    });
	JButton initButton = new JButton("Init");
	initButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"init.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"init.DIR",EPICS.START);
		}
	    });
	JButton parkButton = new JButton("Park");
	parkButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"park.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"park.DIR",EPICS.START);
		}
	    });
	JButton testButton = new JButton("Test");
	testButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"test.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"test.DIR",EPICS.START);
		}
	    });
	JButton stopButton = new JButton("Stop");
	stopButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"stop.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"stop.DIR",EPICS.START);
		}
	    });
	JButton rebootButton = new JButton("Reboot");
	rebootButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.OIWFSprefix+"reboot.MARK",EPICS.MARK);
		    EPICS.put(EPICS.OIWFSprefix+"reboot.DIR",EPICS.START);
		}
	    });
	JButton followButton = new JButton("Follow");
	followButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    
		}
	    });
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(2,3));
	buttonPanel.add(initButton);	
	buttonPanel.add(datumButton);
	buttonPanel.add(parkButton);
	buttonPanel.add(followButton);
	buttonPanel.add(stopButton);
	buttonPanel.add(testButton);
	buttonPanel.add(rebootButton);

	JPanel statusPanel = new JPanel();
	statusPanel.setLayout(new RatioLayout());
	//statusPanel.add("0.49,0.05;0.02,0.99",new JSeparator(JSeparator.VERTICAL));
	
	statusPanel.add("0.01,0.01;0.99,0.04",new JLabel("Status",JLabel.CENTER));
	statusPanel.add("0.01,0.10;0.49,0.04",new JLabel("activeC:",JLabel.LEFT));
	statusPanel.add("0.50,0.10;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"activeC",""));
	statusPanel.add("0.01,0.15;0.49,0.04",new JLabel("arrayS:",JLabel.LEFT));
	statusPanel.add("0.50,0.15;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"arrayS",""));
	statusPanel.add("0.01,0.20;0.49,0.04",new JLabel("followS:",JLabel.LEFT));
	statusPanel.add("0.50,0.20;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"followS",""));
	
	statusPanel.add("0.01,0.25;0.49,0.04",new JLabel("health:",JLabel.LEFT));
	statusPanel.add("0.50,0.25;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"health",""));
	/*statusPanel.add("0.01,0.30;0.49,0.04",new JLabel("historylog:",JLabel.LEFT));
	statusPanel.add("0.50,0.30;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"historylog",""));
	*/
	statusPanel.add("0.01,0.35;0.49,0.04",new JLabel("inPosition:",JLabel.LEFT));
	statusPanel.add("0.50,0.35;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"inPosition",""));
	
	statusPanel.add("0.01,0.40;0.49,0.04",new JLabel("name:",JLabel.LEFT));
	statusPanel.add("0.50,0.40;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"name",""));
	statusPanel.add("0.01,0.45;0.49,0.04",new JLabel("present:",JLabel.LEFT));
	statusPanel.add("0.50,0.45;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"present",""));
	statusPanel.add("0.01,0.50;0.49,0.04",new JLabel("state:",JLabel.LEFT));
	statusPanel.add("0.50,0.50;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"state",""));
	
	statusPanel.add("0.01,0.55;0.49,0.04",new JLabel("probeDebug:",JLabel.LEFT));
	statusPanel.add("0.50,0.55;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeDebug",""));
	statusPanel.add("0.01,0.60;0.49,0.04",new JLabel("probeInit:",JLabel.LEFT));
	statusPanel.add("0.50,0.60;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeInit",""));
	statusPanel.add("0.01,0.65;0.49,0.04",new JLabel("probeIndex:",JLabel.LEFT));
	statusPanel.add("0.50,0.65;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeIndex",""));
	statusPanel.add("0.01,0.70;0.49,0.04",new JLabel("probePark:",JLabel.LEFT));
	statusPanel.add("0.50,0.70;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probePark",""));
	statusPanel.add("0.01,0.75;0.49,0.04",new JLabel("probeC:",JLabel.LEFT));
	statusPanel.add("0.50,0.75;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeC",""));
	statusPanel.add("0.01,0.80;0.49,0.04",new JLabel("probeSimulate:",JLabel.LEFT));
	statusPanel.add("0.50,0.80;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeSimulate",""));
	//statusPanel.add("0.01,0.85;0.49,0.04",new JLabel("probeX:",JLabel.LEFT));
	//statusPanel.add("0.50,0.85;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeX",""));
	//statusPanel.add("0.01,0.90;0.49,0.04",new JLabel("probeY:",JLabel.LEFT));
	//statusPanel.add("0.50,0.90;0.49,0.04",new EPICSLabel(EPICS.OIWFSprefix+"probeY",""));
	
	statusPanel.setBorder(new EtchedBorder(0));

	JPanel probeOffsetPanel = new JPanel();
	probeOffsetPanel.setLayout(new RatioLayout());
	/*
	probeOffsetPanel.add("0.01,0.01;0.99,0.05",new JLabel("Project Vulcan: Probe Offset",JLabel.CENTER));
	probeOffsetPanel.add("0.01,0.10;0.29,0.05",new JLabel("Timestamp"));
	probeOffsetPanel.add("0.35,0.10;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.01,0.20;0.29,0.09",new JLabel("TrackID"));
	probeOffsetPanel.add("0.35,0.20;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.01,0.30;0.29,0.09",new JLabel("Angle"));
	probeOffsetPanel.add("0.35,0.30;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.30;0.09,0.09",new JLabel("rad"));
	probeOffsetPanel.add("0.01,0.40;0.29,0.09",new JLabel("X"));
	probeOffsetPanel.add("0.35,0.40;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.40;0.09,0.09",new JLabel("mm"));
	probeOffsetPanel.add("0.01,0.50;0.29,0.09",new JLabel("XErr"));
	probeOffsetPanel.add("0.35,0.50;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.50;0.09,0.09",new JLabel("mm"));
	probeOffsetPanel.add("0.01,0.60;0.29,0.09",new JLabel("Y"));
	probeOffsetPanel.add("0.35,0.60;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.60;0.09,0.09",new JLabel("mm"));
	probeOffsetPanel.add("0.01,0.70;0.29,0.09",new JLabel("YErr"));
	probeOffsetPanel.add("0.35,0.70;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.70;0.09,0.09",new JLabel("mm"));
	probeOffsetPanel.add("0.01,0.80;0.29,0.09",new JLabel("Z"));
	probeOffsetPanel.add("0.35,0.80;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.80;0.09,0.09",new JLabel("mm"));
	probeOffsetPanel.add("0.01,0.90;0.29,0.09",new JLabel("ZErr"));
	probeOffsetPanel.add("0.35,0.90;0.44,0.09",new EPICSTextField(EPICS.OIWFSprefix+"",""));
	probeOffsetPanel.add("0.90,0.90;0.09,0.09",new JLabel("mm"));
	*/
	probeOffsetPanel.setBorder(new EtchedBorder(0));

	setLayout(new RatioLayout());
	add("0.05,0.30;0.25,0.20",movePanel);
	add("0.05,0.50;0.25,0.20",tolPanel);
	add("0.05,0.10;0.10,0.10",debugPanel);
	add("0.35,0.10;0.35,0.60",probeOffsetPanel);
	add("0.75,0.10;0.24,0.75",statusPanel);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add("0.25,0.85;0.25,0.14",buttonPanel);
    }

}
