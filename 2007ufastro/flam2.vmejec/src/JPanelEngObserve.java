package uffjec;

import ufjca.*;
import javaUFProtocol.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.border.*;

public class JPanelEngObserve extends JPanel {
	public JPanelEngObserve() {
		setLayout(new RatioLayout());
		add("0.7068527918781726,0.5;-0.10786802030456853,0.0989399293286219",new uffjec.EPICSTextField(EPICS.prefix+"test"));
		add("0.0850253807106599,0.12190812720848057;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("DHSLabel"));
		add("0.0850253807106599,0.20141342756183744;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("DS9 panels"));
		add("0.0850253807106599,0.2773851590106007;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("EDTInit"));
		add("0.0850253807106599,0.3586572438162544;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("Exposure Time"));
		add("0.0850253807106599,0.4363957597173145;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("Frame Mode"));
		add("0.0850253807106599,0.04063604240282685;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("DHSInit"));
		add("0.2512690355329949,0.04063604240282685;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.DHSInit"));
		add("0.2512690355329949,0.12190812720848057;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.DHSLabel"));
		add("0.2512690355329949,0.20141342756183744;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.DS9"));
		add("0.2512690355329949,0.2773851590106007;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.EDTInit"));
		add("0.2512690355329949,0.3586572438162544;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.ExpTime"));
		add("0.2512690355329949,0.4363957597173145;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.FrameMode"));
		{
		    Box.Filler bf = new Box.Filler(null,null,null);
		    bf.setBorder(new EtchedBorder(0));
		    add("0.04568527918781726,0.014134275618374558;0.4073604060913706,0.5265017667844523", bf);
		}
		add("0.5215736040609137,0.04063604240282685;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("Index"));
		add("0.5215736040609137,0.20141342756183744;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("PNG filename"));
		add("0.7106598984771574,0.04063604240282685;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.Index"));
		add("0.5228426395939086,0.12367491166077739;0.1218274111675127,0.05123674911660778",new javax.swing.JLabel("FITS filename"));
		add("0.7106598984771574,0.12367491166077739;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.FITSFile"));
		add("0.7106598984771574,0.20141342756183744;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.PNGFile"));
		add("0.5215736040609137,0.3586572438162544;0.15736040609137056,0.05123674911660778",new javax.swing.JLabel("Look-up Table"));
		add("0.5215736040609137,0.4363957597173145;0.15862944162436549,0.05123674911660778",new javax.swing.JLabel("Simulation Mode"));
		add("0.7106598984771574,0.3586572438162544;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.LUT"));
		add("0.7106598984771574,0.4363957597173145;0.17893401015228427,0.06007067137809187",new uffjec.EPICSTextField(EPICS.prefix+"eng:observe.SimMode"));
		{
		    Box.Filler bf = new Box.Filler(null,null,null);
		    bf.setBorder(new EtchedBorder(0));
		    add("0.5,0.012367491166077738;0.42258883248730966,0.2791519434628975", bf);
		}
		{
		    Box.Filler bf = new Box.Filler(null,null,null);
		    bf.setBorder(new EtchedBorder(0));
		    add("0.5025380710659898,0.3127208480565371;0.4200507614213198,0.22968197879858657", bf);
		}
		add("0.04568527918781726,0.7137809187279152;0.12690355329949238,0.054770318021201414",new javax.swing.JLabel("EDT Action"));
		add("0.6510152284263959,0.7137809187279152;0.17258883248730963,0.054770318021201414",new javax.swing.JLabel("EDT Total Frames"));
		add("0.8451776649746193,0.7137809187279152;0.07487309644670051,0.053003533568904596",new uffjec.EPICSLabel(EPICS.prefix+"sad:EDTTOTAL"));
		add("0.5469543147208121,0.7137809187279152;0.07487309644670051,0.053003533568904596",new uffjec.EPICSLabel(EPICS.prefix+"sad:EDTFRAME"));
		add("0.37817258883248733,0.7137809187279152;0.15228426395939088,0.054770318021201414",new javax.swing.JLabel("EDT Frame Number"));
		add("0.18781725888324874,0.7137809187279152;0.15862944162436549,0.053003533568904596",new uffjec.EPICSLabel(EPICS.prefix+"sad:EDTACTN"));
		{
		    Box.Filler bf = new Box.Filler(null,null,null);
		    bf.setBorder(new EtchedBorder(0));
		    add("0.030456852791878174,0.6802120141342756;0.9137055837563451,0.13427561837455831", bf);
		}
		//EPICSApplyButton k = new EPICSApplyButton("Apply");
                //add("0.16370558375634517,0.5918727915194346;0.16624365482233502,0.054770318021201414",k);
		add(EPICSApplyButton.consistantLayout,new EPICSApplyButton()); 
	       
		//EDTTestButton j = new EDTTestButton("Grab Frame");
		//add("0.6421319796954315,0.5918727915194346;0.16624365482233502,0.054770318021201414",j);
		add("0.030456852791878174,0.59;0.911,0.063",new javax.swing.JProgressBar());
	}

		//Unit Test
	public static void main(String [] args){
		EPICS.prefix = "foo:";
		if (args != null && args.length > 0)
			EPICS.prefix = args[0];
		JFrame x = new JFrame();
		x.setContentPane(new JPanelEngObserve());
		x.setSize(400,400);
		x.setVisible(true);
		x.setDefaultCloseOperation(3);
		UFCAToolkit.startMonitorLoop();
	}
}
