package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.lang.Integer;
import java.io.* ;
import java.util.* ;
import java.net.*;
import java.text.* ;
import java.beans.*;
import javax.swing.border.*;
import java.lang.reflect.*;


import javaUFProtocol.*;
import java.net.Socket;
import ufjca.*;

/**
 *Title:     Flamingos 2 Java Engineering Interface
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     David Rashkin
 *Company:    University of Florida
 *Description:
 */

//===============================================================================
/**
 * The main frame
 */
public class fjecFrame extends JFrame {
    public static final String rcsID = "$Name:  $ $Id: fjecFrame.java,v 1.38 2005/09/16 19:15:25 drashkin Exp $";
    public static final double TIMEOUT = 0.05 ;
    //public static fjecMotorParameters fjecMotorParameters ;
    //public static fjecOpath Opathframe = null;

    JMenuBar menuBar1 ;
    JMenu menuFile ;
    JMenuItem menuFileExit  ;
    JMenuItem menuFileEdit ;
    JMenuItem menuFileUpdateRecList ;
    JMenu menuHelp  ;
    JMenuItem menuHelpAbout  ;
    JMenuItem menuHelpEdit  ;
  
    JMenuItem menuConnectEPICSdb;

    public static JLabel statusBar  ;
    BorderLayout borderLayout1 ;
    public static JTabbedPane jTabbedPane  ;
    public static JPanelMotorParameters jPanelParameters  ;
    public static JPanelMotorLowLevel jPanelLowLevel  ;
    public static JPanelMotorHighLevel jPanelHighLevel  ;
    public static JPanelTemperature jPanelTemperature;
    public static JPanelMOSBarcodeReader jPanelMOSBarcodeReader;
    public static JPanelLVDT jPanelLVDT;
    //public static JPanelEPICSRaw jPanelRawCmd;
    
    public static JPanelEPICSRecs jPanelEPICSRecs;
    public static JPanelEngObserve jPanelEngObserve;
    public static JPanelDetectorHighLevel jPanelDetectorHighLevel;
    public static JPanelDetectorLowLevel jPanelDetectorLowLevel;
    public static JPanelScripts jPanelScripts;
    public static JPanelMaster jPanelMaster;
    public static JPanelIS jPanelIS;
    public static JPanelGIS jPanelGIS;
    public static JPanelOIWFS jPanelOIWFS;
    //  public static JPanelEPICSStatus jPanelEPICSStatus;
    //public static JPanelMaster jPanelMaster;
    //public static JPanelSystem jPanelSystem;
    //public static JPanelBias jPanelBias;  
    public static String motorFileName;
    public static String temperatureFileName;
    public static String detectorFileName;
    public static String epicsrecsFileName;
    public static String mosBarcodeFilename;
    public static JCheckBox jCheckBoxPanelLock = new JCheckBox("Panel Lock",false);
    public static int panelLockFlags = 0;
    public static JPanel southPanel = new JPanel();        


    UFRashSkinner ufSkinner;

    String xinetdHost;

    //static EPICSHeartBeat hb = new EPICSHeartBeat();
    //static EPICSMotorStatus epicsMotorStatus;
    //EPICSLabel heartbeatLabel;
    //EPICSLabel EDTcamTypeLabel;

    /**
     *Construct the frame
     *@param inSimulatinMode boolean: in simulatin mode (yes or no)
     *@param prefix system prefix??
     */
    public fjecFrame(boolean splash) {
	this(splash,null,true);
    } //end of fjecFrame
    
    static fjecFrame me = null;

    public fjecFrame(boolean splash, String theme, boolean wfs) {
	System.out.println("Initializing.");
	motorFileName = fjec.data_path + "motor_param_V2.txt";
	detectorFileName = fjec.data_path + "detector_params.txt";
	temperatureFileName = fjec.data_path + "temperature_params.txt";
	epicsrecsFileName = fjec.data_path + "epics_rec_names.txt";
	mosBarcodeFilename = fjec.data_path + "mos_barcode.txt";
	xinetdHost = "192.168.111.222";
	//epicsSim.in_simulation_mode = inSimulationMode;
	//if (epicsSim.in_simulation_mode) {
	//  System.out.println("Running local epics simulation mode.");
	//  epicsSim.load();
	//}
	enableEvents(AWTEvent.WINDOW_EVENT_MASK);
	try  {
	    jbInit(wfs);
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
	if (theme != null && theme != "")
	    ufSkinner.loadTheme(theme);
	me = this;
	if (splash) {
	    new Thread(){
		public void run() {
		    Window w = showSplash();
		    if (w != null){
			while (!ufjca.UFCAToolkit.keepGoing) try{Thread.sleep(100);}catch(Exception e){}
			w.setVisible(false);
			setVisible(true);
			me.createBufferStrategy(2);
		    }
		}
	    }.start();
	} else {
	    setVisible(true);
	    me.createBufferStrategy(2);
	}
    }

    public static int getTheHeight(){
	if (me != null)
	    return me.getHeight();
	else return 0;
    }

    public static int getTheWidth() {
	if (me != null)
	    return me.getWidth();
	else return 0;
    }

    public static Graphics getDrawBuffer() {
	if (me != null)
	    return me.getBufferStrategy().getDrawGraphics();
	else return null;
    }

    public static void repaintMe() {
	if (me != null)
	    me.getBufferStrategy().show();
    }

    public static void repaintMe(Graphics g) {
	if (me != null)
	    me.paint(g);
    }

    public static void repaintPane(Graphics g){
	jTabbedPane.paint(g);
    }
    public static void repaintPane(){
	jTabbedPane.repaint();
    }
    public static Image getPaneImage(){
	return jTabbedPane.createImage(jTabbedPane.getWidth(),jTabbedPane.getHeight());
    }

    public static Window showSplash() {
	ImageIcon ii = null;
	if ((new File("fjec_splash.jpg")).exists()) 
	    ii = new ImageIcon("fjec_splash.jpg");
	else if((new File("images/fjec_splash.jpg")).exists())
	    ii = new ImageIcon("images/fjec_splash.jpg");
	else if((new File(fjec.data_path+"fjec_splash.jpg")).exists())
	    ii = new ImageIcon(fjec.data_path+"fjec_splash.jpg");
	else {
	    System.err.println("fjec::showSplash> Cannot find splash image.");
	    return null;
	}
	Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
	int w = ii.getIconWidth();
	int h = ii.getIconHeight();
	final Window splashWin= new Window(new JFrame());
	splashWin.add(new JLabel(ii));
	splashWin.setSize(w,h);
	splashWin.setLocation((dim.width-w)/2,(dim.height-h)/2);
	splashWin.setVisible(true);
	splashWin.addMouseListener(new MouseListener() {
		public void mouseClicked(MouseEvent me) {
		    splashWin.setVisible(false);
		}
		public void mousePressed(MouseEvent me){}
		public void mouseReleased(MouseEvent me) {}
		public void mouseExited(MouseEvent me) {}
		public void mouseEntered(MouseEvent me) {}
	    });
	//try { Thread.sleep(3000);}
	//catch (Exception e) {}
	//splashWin.setVisible(false);
	return splashWin;
    }



    //-------------------------------------------------------------------------------
    /**
     *Component initialization from parameter file
     *Component initialization from EPICS or UFlib
     */
    private void jbInit(boolean wfs) throws Exception  {
	/*UIDefaults dd = UIManager.getDefaults();
	  Enumeration enu = dd.keys();
	  while (enu.hasMoreElements())
	  System.out.println(enu.nextElement());
	*/
	//UIManager.put("Label.font",new Font("SansSerif",Font.PLAIN,10));
	//UIManager.put("TextField.font",new Font("SansSerif",Font.PLAIN,10));
	//UIManager.put("ComboBox.font",new Font("SansSerif",Font.PLAIN,10));
	//UIManager.put("Button.font",new Font("SansSerif",Font.PLAIN,10));
	//fjecMotorParameters = new fjecMotorParameters(motorFileName);

	menuBar1 = new JMenuBar();
	menuFile = new JMenu();
	menuFileExit = new JMenuItem();
	menuFileEdit = new JMenuItem();
	menuFileUpdateRecList = new JMenuItem();
	menuHelp = new JMenu();
	menuHelpAbout = new JMenuItem();
	menuHelpEdit = new JMenuItem();
	statusBar = new JLabel();
	borderLayout1 = new BorderLayout();
	jTabbedPane = new JTabbedPane();

	ufSkinner = new UFRashSkinner(this,jTabbedPane);

	JMenu connectOption = new JMenu("EPICS db");
	menuConnectEPICSdb = new JMenuItem("Connect to ...");
	menuConnectEPICSdb.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    String s = JOptionPane.showInputDialog("EPICS db name",EPICS.prefix);
		    if (s != null && !s.trim().equals("")) {
			s = s.trim();
			if (!s.endsWith(":")) s = s+":";
			EPICS.prefix = s;
			EPICS.applyPrefix = s;
			EPICS.nullDB = false;
			connectDB();
			setStatusBarText();
		    }		    
		}
	    });
	connectOption.add(menuConnectEPICSdb);

	JMenu rebootOption = new JMenu("Shutdown/Boot");
	JMenuItem rebootOption1 = new JMenuItem("Change host...");
	JMenuItem rebootOption2 = new JMenuItem("Shutdown");
	JMenuItem rebootOption3 = new JMenuItem("Boot");
	final JCheckBoxMenuItem rebootOption4 = new JCheckBoxMenuItem("Sim",true);
	rebootOption.add(rebootOption1);
	rebootOption.add(rebootOption4);
	rebootOption.add(new JSeparator());
	rebootOption.add(rebootOption2);
	rebootOption.add(rebootOption3);
	rebootOption1.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    String s = JOptionPane.showInputDialog("Hostname for xinetd",xinetdHost);
		    if (s != null && !s.trim().equals("")) xinetdHost = s;
		    setStatusBarText();
		}
	    });
	rebootOption2.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			Socket soc = new Socket(xinetdHost,3720);
			UFProtocol.sendString("UFFLAMshutdown",soc);
			String s = UFProtocol.recvString(soc);
			if (s == null || s.length() == 0) {
			    System.err.println("fjecFrame.rebootOption2.actionPerformed> Empty response string!");
			}
			soc.close();
		    } catch (Exception e) {
			System.err.println("fjecFrame.rebootOpion2.actionPerformed> "+e.toString());
		    }
		}
	    });
	rebootOption3.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			String s = "UFFLAMboot";
			if (rebootOption4.isSelected()) s += "sim";
			Socket soc = new Socket(xinetdHost,3720);
			UFProtocol.sendString(s,soc);
			String recvs = UFProtocol.recvString(soc);
			if (recvs == null || recvs.length() == 0) {
			    System.err.println("fjecFrame.rebootOption2.actionPerformed> Empty response string!");
			}
			soc.close();
		    } catch (Exception e) {
			System.err.println("fjecFrame.rebootOption3.actionPerformed> "+e.toString());
		    }
		}
	    });

	JMenu menuOption = new JMenu("Look & Feel");
	JMenuItem menuOption1 = new JMenuItem("Motif Look");
	JMenuItem menuOption2 = new JMenuItem("Metal Look");
	JMenuItem menuOption3 = new JMenuItem("Windows Look");
	JMenuItem menuOption4 = new JMenuItem("Load Theme...");
	JMenuItem menuOption5 = new JMenuItem("Edit Theme...");
	JMenuItem menuOption6 = new JMenuItem("Save Theme...");
	menuOption.add(menuOption1);
	menuOption.add(menuOption2);
	menuOption.add(menuOption3);
	menuOption.add(menuOption4);
	menuOption.add(menuOption5);
	menuOption.add(menuOption6);

	menuOption1.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent ae) {
		    setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		}
	    });

	menuOption2.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent ae) {
		    setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
		}
	    });

	menuOption3.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent ae) {
		    setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}
	    });
	menuOption4.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    ufSkinner.showLoadDialog();
		}
	    });
	menuOption5.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    ufSkinner.showThemeWindow();
		}
	    });
	menuOption6.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    ufSkinner.showSaveDialog();
		}
	    });
	
	JMenu soundMenu = new JMenu("Sounds");
	final JMenuItem soundsOnOff = new JMenuItem("Sounds On");
	soundsOnOff.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    FJECSounds.playSounds = !FJECSounds.playSounds;
		    if (FJECSounds.playSounds) 
			soundsOnOff.setText("Sounds Off");
		    else
			soundsOnOff.setText("Sounds On");
		}
	    });
	soundMenu.add(soundsOnOff);

	//heartbeatLabel = new EPICSLabel(EPICS.prefix + "heartbeat", EPICS.prefix + "heartbeat:");
	//heartbeatLabel.setHorizontalAlignment(JLabel.CENTER);
	//EDTcamTypeLabel = new EPICSLabel(EPICS.prefix + "sad:dcState"," Det.Control State:");

	southPanel.setLayout(new BorderLayout());
	southPanel.add(jCheckBoxPanelLock, BorderLayout.EAST);
	southPanel.add(statusBar,BorderLayout.WEST);
	//southPanel.add(heartbeatLabel, BorderLayout.CENTER);
	//southPanel.add(EDTcamTypeLabel, BorderLayout.WEST);

	FJECMotor [] fjecmotors = createMotors(motorFileName);

	//jPanelEngObserve = new JPanelEngObserve();
	jPanelParameters = new JPanelMotorParameters(fjecmotors);
	jPanelHighLevel = new JPanelMotorHighLevel(fjecmotors);
	jPanelLowLevel = new JPanelMotorLowLevel(fjecmotors);
	//jPanelLVDT = new JPanelLVDT(fjecmotors);
	//jPanelDetectorHighLevel = new JPanelDetectorHighLevel();
	//jPanelDetectorLowLevel  = new JPanelDetectorLowLevel();
	jPanelEPICSRecs = new JPanelEPICSRecs(epicsrecsFileName);
	//jPanelMOSBarcodeReader = new JPanelMOSBarcodeReader(mosBarcodeFilename);
	jPanelScripts = new JPanelScripts();
	//jPanelMaster = new JPanelMaster();
	jPanelIS = new JPanelIS();
	jPanelTemperature = new JPanelTemperature(temperatureFileName);
	//jPanelGIS = new JPanelGIS();
	//if (wfs) jPanelOIWFS = new JPanelOIWFS();
	//    jPanelRawCmd = new JPanelEPICSRaw();
	//    jPanelEPICSStatus = new JPanelEPICSStatus();
	//    jPanelMaster = new JPanelMaster(fjecmotors);
	//jPanelSystem = new JPanelSystem();
	//jPanelBias= new JPanelBias();
    
	this.getContentPane().setLayout(borderLayout1);
	this.setLocation(fjec.screen_loc[0]);
	this.setSize(fjec.screen_size[0]);

	this.addComponentListener(new java.awt.event.ComponentAdapter() {
		public void componentResized(ComponentEvent e) {
		    this_componentResized(e);
		}
	    });
	this.addComponentListener(new java.awt.event.ComponentAdapter() {
		public void componentMoved(ComponentEvent e) {
		    this_componentMoved(e);
		}
	    });

	this.setTitle("Flamingos 2 Java Engineering Interface (ver " + version.version + ")");
	statusBar.setText(" ");
	menuFile.setText("File");
	menuFileEdit.setText("Edit a file");



	menuFileEdit.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    fileEdit_action(e);
		}
	    });
	menuFileExit.setText("Exit");
	menuFileExit.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent e) {
		    fileExit_action(e);
		}
	    });
	menuHelp.setText("Help");
	menuHelpAbout.setText("About");
	menuHelpAbout.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent e) {
		    helpAbout_action(e);
		}
	    });
	menuHelpEdit.setText("Edit fjec_doc.txt");
	menuHelpEdit.addActionListener(new ActionListener()  {
		public void actionPerformed(ActionEvent e) {
		    helpEdit_action(e);
		}
	    });
	jCheckBoxPanelLock.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    panelLock_action(e);
		}
	    });
	jTabbedPane.addChangeListener(new ChangeListener() {
		public void stateChanged(ChangeEvent e) {
		    jTabbedPane_stateChanged(e);
		}
	    });
	menuFile.add(menuFileEdit);
	menuFile.add(menuFileUpdateRecList);
	menuFile.add(menuFileExit);
	menuHelp.add(menuHelpAbout);
	menuHelp.add(menuHelpEdit);
	menuBar1.add(menuFile);
	menuBar1.add(rebootOption);
	menuBar1.add(connectOption);
	menuBar1.add(menuOption);
	menuBar1.add(soundMenu);
       
	menuBar1.add(menuHelp);
	//menuBar1.add(statusBar);
	

	this.setJMenuBar(menuBar1);
	this.getContentPane().add(southPanel, BorderLayout.SOUTH);

	// do scripts last so autoexec sees loaded data
	//jPanelScripts = new JPanelScripts();

	jTabbedPane.add(jPanelMaster,"Master");
	jTabbedPane.add(jPanelIS, "Instrument Sequencer");
	jTabbedPane.add(jPanelEngObserve,"Engineering Observe");
	JTabbedPane detectorSubPane = new JTabbedPane();
	detectorSubPane.add(jPanelDetectorHighLevel,"High-Level");
	detectorSubPane.add(jPanelDetectorLowLevel,"Low-Level");
	jTabbedPane.add(detectorSubPane,"Detector");
	jTabbedPane.add(jPanelLVDT,"LVDT");
	//jTabbedPane.add(jPanelBias,"Bias");
	jTabbedPane.add(jPanelTemperature,"Temperature And Pressure");
	//jTabbedPane.add(jPanelEPICSStatus,"Status");
	JTabbedPane motorSubPane = new JTabbedPane();
	motorSubPane.add(jPanelHighLevel,"High-Level Movement");
	motorSubPane.add(jPanelLowLevel,"Low-Level Movement");
	motorSubPane.add(jPanelParameters,"Low-Level Parameters");
	jTabbedPane.add(motorSubPane,"Motor Indexors");
	jTabbedPane.add(jPanelMOSBarcodeReader,"MOS Barcode Reader");
	if (wfs) jTabbedPane.add(jPanelOIWFS,"OIWFS");
	jTabbedPane.add(jPanelGIS,"GIS");
	jTabbedPane.add(jPanelScripts,"Scripts");
	jTabbedPane.add(jPanelEPICSRecs,"EPICS Recs");
	//jTabbedPane.add(jPanelRawCmd,"Raw");
	//jTabbedPane.add(jPanelSystem,"System");
	jTabbedPane.add(new JPanelMindReader(),"Mind Reader");
	jTabbedPane.add(new EPICSCadTester(),"EPICS CAD Tester");
	this.getContentPane().add(jTabbedPane, BorderLayout.CENTER);
	setStatusBarText();
    } //end of jbInit

    public FJECMotor [] createMotors(String filename) {
	/*
	  FJECMotor [] j = new FJECMotor[7];
	  String [] names = { "Decker","Filter1","Filter2","Focus",
	  "Grism","Lyot","MOS"};
	  for (int i=0; i<7; i++) 
	  j[i] = new FJECMotor(names[i]);
	  return j;
	*/
	try {
	    TextFile in_file = new TextFile(filename,TextFile.IN);
	    int num_motors =  Integer.parseInt(in_file.nextUncommentedLine().trim());
	    FJECMotor [] retVals = new FJECMotor[num_motors];
	    for (int i=0; i<retVals.length; i++) {
		String line = in_file.nextUncommentedLine();
		if (line.toLowerCase().indexOf("begin_rec") == -1) throw new Exception();
		line = in_file.nextUncommentedLine();
		StringTokenizer st = new StringTokenizer(line);
		//for (int k=0; k<11; k++) st.nextToken();
		String iname = st.nextToken();
		String hname = st.nextToken();
		while (st.hasMoreTokens()) hname += " "+st.nextToken();
		String ename = hname; //st.nextToken();
		line = in_file.nextUncommentedLine();
		if (line.toLowerCase().indexOf("positions") == -1) throw new Exception();
		line = in_file.nextUncommentedLine();
		int num_pos = Integer.parseInt(line.trim());
		String [] posNames = new String[num_pos];
		float [] posValues = new float[num_pos];
		for (int h=0; h<num_pos; h++) {
		    st = new StringTokenizer(in_file.nextUncommentedLine());
		    st.nextToken();
		    posValues[h] = Float.parseFloat(st.nextToken());
		    posNames[h] = "";
		    while(st.hasMoreTokens()) posNames[h] += st.nextToken()+" ";
		} 
		retVals[i] = new FJECMotor(ename,hname,posNames,posValues);
		line = in_file.nextUncommentedLine();
		if (line.toLowerCase().indexOf("end_rec") == -1) throw new Exception();
	    }
	    return retVals;
	} catch (Exception e) {
	    System.err.println("fjecFrame::createMotors> "+e.toString());
	    return null;
	}

    }    


    public void setStatusBarText() {
	statusBar.setText("EPICS db: "+EPICS.prefix+"                       xinetd host: "+xinetdHost);
    }

    //-------------------------------------------------------------------------------
    /**
     * Check flags for panel lock. Check/uncheck panel Lock checkbox based on current
     *  state of flags and current tab selected
     *@param e TBD
     */
    public void jTabbedPane_stateChanged(ChangeEvent e)
    {
	int pindex = jTabbedPane.getSelectedIndex();

	if(( panelLockFlags & (int)Math.pow(2,pindex) ) == 0)
	    jCheckBoxPanelLock.setSelected(false);
	else
	    jCheckBoxPanelLock.setSelected(true);

	/*if(jTabbedPane.getTitleAt(pindex)=="Detector")
	  jPanelDetector.checkConnection(true);

	  if(jTabbedPane.getTitleAt(pindex)=="Temperature")
	  jPanelTemperature.checkConnection(true);

	  if(jTabbedPane.getTitleAt(pindex)=="Bias") {
	  jPanelBias.checkConnection(true);
	  //Always Lock the Bias Panel:
	  recurseLock( jPanelBias, false );
	  jCheckBoxPanelLock.setSelected(true);
	  }

	  if(jTabbedPane.getTitleAt(pindex)=="PreAmp") {
	  jPanelPreamp.checkConnection(true);
	  //Always Lock the PreAmp Panel:
	  recurseLock( jPanelPreamp, false );
	  jCheckBoxPanelLock.setSelected(true);
	  }*/
    } //end of jTabbedPane_stateChanged



    public void recurseConnect(Container jp, String dbPrefix) {
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    try {
		if (cmp[i] instanceof UFCAToolkit.MonitorListener) {
		    ((UFCAToolkit.MonitorListener)cmp[i]).reconnect(dbPrefix);
		}
		if (cmp[i] instanceof Container) 
		    recurseConnect((Container)cmp[i],dbPrefix);
	    } catch (Exception e) {
		System.err.println("fjecFrame.recurseConnect> "+e.toString());
	    }
	}
    }

    public void connectDB () {
	recurseConnect(jPanelMaster,EPICS.prefix);
	recurseConnect(jPanelIS,EPICS.prefix);
	recurseConnect(jPanelDetectorHighLevel,EPICS.prefix);
	recurseConnect(jPanelDetectorLowLevel,EPICS.prefix);
	recurseConnect(jPanelLVDT,EPICS.prefix);
	recurseConnect(jPanelTemperature,EPICS.prefix);
	recurseConnect(jPanelHighLevel,EPICS.prefix);
	recurseConnect(jPanelLowLevel,EPICS.prefix);
	recurseConnect(jPanelParameters,EPICS.prefix);
	recurseConnect(jPanelMOSBarcodeReader,EPICS.prefix);
	recurseConnect(jPanelGIS,EPICS.prefix);
	recurseConnect(jPanelScripts,EPICS.prefix);
	recurseConnect(jPanelEPICSRecs,EPICS.prefix);

	if (jPanelOIWFS != null)
	    recurseConnect(jPanelOIWFS,EPICS.OIWFSprefix);

    }

    //-------------------------------------------------------------------------------
    /**
     * Locks components of a Panel
     *@param jp JPanel to be locked
     *@param enable boolean: lock(yes or no)
     */
    public void recurse_update(Container jp) {
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    try {
		if (!cmp[i].getClass().getName().equals("javax.swing.JButton"))
		    {
			JButton _jb = (JButton) cmp[i];
			_jb.setDefaultCapable(false);
		    }
		Container _jp = (Container) cmp[i];
		recurse_update(_jp);
	    }
	    catch(ClassCastException cce) {
	    }
	    catch(Exception e) {
		fjecError.show(e.toString());
	    }
	}
    } //end of recurseLock

    //-------------------------------------------------------------------------------
    /**
     * Locks components of a Panel
     *@param jp JPanel to be locked
     *@param enable boolean: lock(yes or no)
     */
    public void recurseLock(Container jp, boolean enable) {
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    try {
		if (!cmp[i].getClass().getName().equals("javax.swing.JLabel") &&
		    cmp[i].getName()==null)  cmp[i].setEnabled(enable);
		Container _jp = (Container) cmp[i];
		recurseLock(_jp,enable);
	    }
	    catch(ClassCastException cce) {
	    }
	    catch(Exception e) {
		fjecError.show(e.toString());
	    }
	}
    } //end of recurseLock


    //-------------------------------------------------------------------------------
    /**
     * Action performed for the lock checkbox
     *@param e TBD
     */
    public void panelLock_action(ActionEvent e) {
	JPanel cm = (JPanel) jTabbedPane.getSelectedComponent();
	int s = jTabbedPane.getSelectedIndex();
	panelLockFlags ^= (int)Math.pow(2,s);
	if( jCheckBoxPanelLock.isSelected() )
	    recurseLock(cm,false);
	else
	    recurseLock(cm,true);
	cm.repaint();
    } //end of panelLock_action


    //-------------------------------------------------------------------------------
    /**
     *File | Exit action performed
     * Simply exits the application
     *@param e not used
     */
    public void fileExit_action(ActionEvent e) {
	/* close socket if != 0
	   try {
	   if (SocketComm.in != null)
	   SocketComm.in.close();          // close the input stream
	   if (SocketComm.out != null)
	   SocketComm.out.close();         // close the output stream
	   if (SocketComm.mysocket != null)
	   SocketComm.mysocket.close();    // close the socket
	   }
	   catch (IOException ee) {
	   System.out.println("SocketComm Error 7 : " + ee);
	   fjecError.show("SocketComm Error 7 : " + ee);
	   }*/
	fjec.save_screen_locs();
	System.exit(0);
    } //end of fileExit_action

    //-------------------------------------------------------------------------------
    /**
     *Help | About action performed
     *Brings up the about dialog window
     *@param e not used
     */
    public void helpAbout_action(ActionEvent e) {
	fjecFrameAboutBox dlg = new fjecFrameAboutBox(this);
	Dimension dlgSize = dlg.getSize();
	Dimension frmSize = getSize();
	dlg.setLocation(100,100);
	dlg.setModal(true);
	dlg.setVisible(true);
    } //end of helpAbout_action


    //-------------------------------------------------------------------------------
    /**
     *Help | Edit action performed
     *Brings up the fjec_doc.txt document for viewing and editing
     *@param e not used
     */
    public void helpEdit_action(ActionEvent e) {
	new fjecEditorFrame(fjec.data_path + "fjec_doc.txt");
    } //end of helpEdit_action


    //-------------------------------------------------------------------------------
    /**
     *File | Edit action performed
     *brings up the fjec_doc.txt document for viewing and editing
     *@param e not used
     */
    public void fileEdit_action(ActionEvent e) {
	new fjecEditorFrame(fjec.data_path);
    } //end of helpEdit_action


    //-------------------------------------------------------------------------------
    /**
     *Overridden so we can exit on System Close
     *@param e TBD
     */
    protected void processWindowEvent(WindowEvent e) {
	super.processWindowEvent(e);
	if(e.getID() == WindowEvent.WINDOW_CLOSING) {
	    // stop psh scripting process if started
	    jPanelScripts.destroyPsh();
	    fileExit_action(null);
	}
    } //end of processWindowEvent

    //-------------------------------------------------------------------------------
    /**
     * Component Event Handler??
     *@param e not used
     */
    void this_componentMoved(ComponentEvent e) {
	fjec.screen_loc[0] = this.getLocation();
	// System.out.println("screen_loc = " + fjec.screen_loc[0].toString());
    } //end of this_componentMoved

    //-------------------------------------------------------------------------------
    /**
     * Component Event Handler
     *@param e not used
     */
    void this_componentResized(ComponentEvent e) {
	fjec.screen_size[0] = this.getSize();
	// System.out.println("size = " + fjec.screen_size[0].toString()); // java.awt.Dimension[width=859,height=534]
    } // end of this_componentResized

    //---------------------------------------------------------------------------------------


    public void setLookAndFeel(String lnf) {
	try {
	    UIManager.setLookAndFeel(lnf);
	    SwingUtilities.updateComponentTreeUI(this);
	} catch (Exception e) {
	    System.err.println("fjecFrame.setLookAndFeel> "+e.toString());
	}
    }


} //end of class fjecFrame




