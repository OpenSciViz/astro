package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;

public class JPanelMonitor extends JPanel {

    JScrollPane monitorScrollPane;
    JPanel monitorPanel;
    JButton saveListButton;
    JButton loadListButton;
    JButton clearListButton;

    public JPanelMonitor(JPanel monitorPanelToCopy) {
	monitorPanel = new JPanel();
	monitorScrollPane = new JScrollPane(monitorPanel);
	saveListButton = new JButton("Save List");
	loadListButton = new JButton("Load List");
	clearListButton = new JButton("Clear List");
    	saveListButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    JFileChooser jfc = new JFileChooser();
		    if (jfc.showSaveDialog(null) != jfc.APPROVE_OPTION) return;
		    try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(jfc.getSelectedFile()));
			for (int i=0; i<monitorPanel.getComponentCount(); i++) {
			    try {
				JPanel pan = (JPanel)monitorPanel.getComponent(i);
				String s = ((JLabel)pan.getComponent(0)).getText().trim();
				if (s.endsWith(":")) s = (s.substring(0,s.length()-1)).trim();
				//remove epics db prefix
				s = s.substring(s.indexOf(":")+1,s.length());
				bw.write(s,0,s.length());
				bw.newLine();
			    } catch (ClassCastException cce) {
				System.err.println("JPanelMonitor::saveListButton.addActionListener> Oops, "+
						   "not a JPanel. How'd that happen? "+cce.toString());
			    }
			}
			bw.flush();
			bw.close();
		    } catch (IOException ioe) {
			System.err.println("JPanelMonitor::saveListButton.addActionListener> "+
					   ioe.toString());
			
		    }
		}
	    });
	loadListButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    //monitorScrollPane.remove(monitorPanel);
		    monitorPanel.removeAll();
		    JFileChooser jfc = new JFileChooser();
		    if (jfc.showOpenDialog(null) != jfc.APPROVE_OPTION) return;
		    try {
			BufferedReader br = new BufferedReader(new FileReader(jfc.getSelectedFile()));
			while (br.ready()) {
			    String s = br.readLine();
			    //prepend epics db prefix
			    addMonitor(EPICS.prefix+s);
			}
			//monitorScrollPane.add(monitorPanel);
			monitorPanel.revalidate();
			revalidate();
		    } catch (IOException ioe) {
			System.err.println("JPanelMonitor::loadListButton.addActionListener> "+
					   ioe.toString());
		    }
		}
	    });
	clearListButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    monitorPanel.removeAll();
		    monitorPanel.revalidate();
		    revalidate();
		}
	    });
	monitorPanel.setLayout(new GridLayout(0,1));
	monitorPanel.setBorder(new EtchedBorder(0));
	setLayout(new RatioLayout());
	add("0.01,0.01;0.99,0.90",monitorScrollPane);
	add("0.01,0.91;0.30,0.09",saveListButton);
	add("0.31,0.91;0.30,0.09",loadListButton);
	add("0.61,0.91;0.30,0.09",clearListButton);
	for (int i=0; i<monitorPanelToCopy.getComponentCount(); i++) {
	    try {
		JPanel pan = (JPanel)monitorPanelToCopy.getComponent(i);
		String s = ((JLabel)pan.getComponent(0)).getText().trim();
		if (s.endsWith(":")) s = (s.substring(0,s.length()-1)).trim();
		addMonitor(s);
	    } catch (ClassCastException cce) {
		System.err.println("JPanelMonitor::JPanelMonitor> Oops, "+
				   "not a JPanel. How'd that happen? "+cce.toString());
	    }
	}	
    }

    public JPanel getMonitorPanel() { return monitorPanel; }

    public void addMonitor(String recname) {
	final JPanel newPan = new JPanel();
	newPan.add(new JLabel(recname+": "));
	EPICSTextField etf = new EPICSTextField(recname,"");
	etf.setColumns(10);
	newPan.add(etf);
	JButton j = new JButton("Remove");
	j.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    monitorPanel.remove(newPan);
		    monitorPanel.revalidate();
		}
	    });
	JButton up = new JButton("Up");
	JButton down = new JButton("Down");
	up.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    Component [] cmps = monitorPanel.getComponents();
		    int i = 0;
		    for (i=0; i<cmps.length; i++) {
			if (cmps[i] == newPan){
			    if (i > 0) {
				monitorPanel.removeAll();
				for (int j=0; j<i-1; j++)
				    monitorPanel.add(cmps[j]);
				monitorPanel.add(cmps[i]);
				monitorPanel.add(cmps[i-1]);
				for (int j=i+1; j<cmps.length; j++)
				    monitorPanel.add(cmps[j]);
				monitorPanel.revalidate();
			    }
			    break;
			}
		    }		
		    if (i == cmps.length) 
			System.err.println("JPanelMonitor.upActionPerformed> Cannot find myself in the component");			    
		}
	    });
	down.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    Component [] cmps = monitorPanel.getComponents();
		    int i = 0;
		    for (i=0; i<cmps.length; i++) {
			if (cmps[i] == newPan){
			    if (i < cmps.length-1) {
				monitorPanel.removeAll();
				for (int j=0; j<i; j++)
				    monitorPanel.add(cmps[j]);
				monitorPanel.add(cmps[i+1]);
				monitorPanel.add(cmps[i]);
				for (int j=i+2; j<cmps.length; j++)
				    monitorPanel.add(cmps[j]);
				monitorPanel.revalidate();
			    }
			    break;
			}
		    }		
		    if (i == cmps.length) 
			System.err.println("JPanelMonitor.downActionPerformed> Cannot find myself in the component"); 		    
		}
	    });
	newPan.add(j);
	newPan.add(up);
	newPan.add(down);
	monitorPanel.add(newPan);
    }

}
