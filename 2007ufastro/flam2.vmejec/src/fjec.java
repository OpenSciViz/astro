package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.* ;
import java.util.StringTokenizer ;
import java.util.Properties;
import java.util.Vector;

import ufjca.*;
//import com.borland.jb.io.* ;


/**
*   Title:      CanariCam Java Engineering Interface
*   Version:
*   Copyright:  Copyright (c) 2003
*   Author:     Frank Varosi, David Rashkin
*   Company:    University of Florida
*   Description:
*/


//===============================================================================
/**
 * main class
 */
public class fjec {
  public static final String rcsID = "$Name:  $ $Id: fjec.java,v 1.17 2005/08/24 20:49:35 drashkin Exp $";
  public static String data_path = "./";
    public static String dbPath;// = "$UFINSTALL/bin/uf"+EPICS.prefix+"epicsd";
  public static final int MAX_SCREEN_LOCS = 100;
  public static int n_screen_locs;
  public static String screen_name[] = new String[MAX_SCREEN_LOCS];
  public static Point screen_loc[] = new Point[MAX_SCREEN_LOCS];
  public static Dimension screen_size[] = new Dimension[MAX_SCREEN_LOCS];

  boolean packFrame = false;

//-------------------------------------------------------------------------------
  /**
   *default constructor for the application
   *@param sim boolean simulation mode (true or false)
   */
  public fjec(boolean splash, String theme, boolean wfs) {
    EPICS.setupStaticVars();
    if (dbPath == null || dbPath == "" || dbPath.trim() == "")
	dbPath = "$UFINSTALL/bin/uf"+EPICS.prefix.substring(0,EPICS.prefix.length()-1)+"epicsd" ;
    n_screen_locs = 1;
    screen_name[0] = "fjecFrame";
    screen_loc[0] = new Point(50,50);
    screen_size[0] = new Dimension(400,400);


    load_screen_locs();
 
    fjecFrame frame = new fjecFrame(splash,theme,wfs);
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame)
	frame.pack();
    else
	frame.validate();
    //try to redirect System.err.println's to fjecError window
    PrintStream origErr = System.err;
    System.setErr(new PrintStream(origErr) {
	    //override System.err.println method
	    public void println(String x) {
		fjecError.show(x);
	    }
    });
  } //end of fjec


    public static void printOptions() {
	System.out.println("Usage: fjec [options] [epics dbname]");
	System.out.println("Command line arguments:");
	System.out.println();
	System.out.println("\t-nosplash\tDon't show splash screen on startup");
	System.out.println("\t-theme <path-to-theme>\tTheme to automatically load on startup");
	System.out.println("\t-dbpath <path-to-epics-db>\tLocation of epics db for raw epics panel");
    }

//-------------------------------------------------------------------------------
  /**
   *Main method
   *@param args: -tsport: motor agent port; -tshost: motor agent host; default reads values out of mot_param.txt
   */
  public static void main(String[] args) {
    boolean sim = false;
    boolean splash = true;
    boolean wfs = false;
    String theme = "";
    load_ini();

    for (int i = 0; i < args.length; i++) {
	if (args[i].toLowerCase().indexOf("-help")!=-1){
	    printOptions();
	    System.exit(0);
	}
	else if (args[i].toLowerCase().equals("-nosplash"))
	  //	  (new Thread() {
	  //  public void run() {
	  //      showSplash();
	  //  }
	  //  }).start();
	  splash = false;
	else if (args[i].toLowerCase().equals("-nowfs")) {
	    wfs = false;
	} else if (args[i].toLowerCase().equals("-wfs")) {
	    wfs = true;
	}else if (args[i].toLowerCase().equals("-dbpath")) {
	  try {
	      if (i+1 < args.length)
		  dbPath = args[++i];
	      else
		  System.err.println("fjec.main> -dbpath must be followed by an argument");
	  } catch (Exception e) {
	      System.err.println("fjec.main> "+e.toString());
	  }
      } else if (args[i].toLowerCase().equals("-theme")) {
	  try {
	      if (i+1 < args.length)
		  theme = args[++i];
	      else
		  System.err.println("fjec.main> -theme must be followed by an argument");
	  } catch (Exception e) {
	      System.err.println("fjec.main> "+e.toString());	      
	  }
      }else if (args[i].indexOf(":") != -1) 
	  EPICS.prefix = args[i];
      else {
	  // assume for now that any cmd-line-arg is an epics prefix
	  // and that the user simply forgot to append the ':'
	  EPICS.prefix = args[i]+":";
      }
      /*      if (args[i].equals("-motport"))
	      try {
	      motport = Integer.parseInt(args[i+1]);
	      } catch (Exception e) {
	      System.err.println("-motport must be followed by a valid integer");
	      System.exit(-1);
	      }
	      if (args[i].equals("-mothost"))
	      if (i == args.length-1) {
	      System.err.println("-tshost must be followed by a valid hostname");
	      } else mothost = new String(args[i+1]);
      */
    }
    fjec fj = new fjec(splash,theme,wfs);
    System.out.println("Finished Initializing, starting UFJCA monitor loop");
    UFCAToolkit.startMonitorLoop();
    System.err.println("fjec.main> Looks like the UFJCA monitor loop died.");
  }


//-------------------------------------------------------------------------------
  /**
   *void load_init()
   * sets the data_path field from system property "fjec.data_path" (default = "./data/")
   */
  static void load_ini() {
      Properties props = new Properties(System.getProperties()) ;
      System.setProperties(props) ;
      data_path = props.getProperty("fjec.data_path") ;
      if (data_path == null) data_path = "./data/" ;
      System.out.println("fjec.data_path = " + data_path) ;
  } // end of load_ini


//-------------------------------------------------------------------------------
  /**
   * Sets the screen locations for the window
   *@param name String: name in the title bar
   *@param loc Point: location of window
   *@param size Dimension: size of window
   */
  public static void set_screen_loc(String name, Point loc, Dimension size) {
    int i;
    for (i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        screen_loc[i] = loc;
        screen_size[i] = size;
        return;
      }
    }
    // name was not found, add it
    if (i < MAX_SCREEN_LOCS) {
      n_screen_locs++;
      screen_name[i] = name;
      screen_loc[i] = loc;
      screen_size[i] = size;
    }
    else {
      fjecError.show("Too many screen_locs. Remove some old ones from screen_locs.txt");
    }
  } // end of set_screen_loc


//-------------------------------------------------------------------------------
  /**
   * Returns the point location of the window with a given name
   *@param name String: window name
   */
  public static Point get_screen_loc(String name) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_loc[i];
      }
    }
    // name was not found
    return new Point(50, 50);
  } // end of set_screen_size


//-------------------------------------------------------------------------------
  /**
   * Returns the size fo the window with a given name
   * If window with given name isn't found, Returns Dimension(400, 600)
   *@param name String: window name
   */
  public static Dimension get_screen_size(String name) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_size[i];
      }
    }
    // name was not found
    return new Dimension(400, 600);
  } // end of get_screen_size


//-------------------------------------------------------------------------------
  /**
   * Returns the size fo the window with a given name
   * If window with given name isn't found, Returns init_size parameter
   *@param name String: window name
   *@param init_size Dimension: size to be set if name not found
   */
  public static Dimension get_screen_size(String name, Dimension init_size) {
    for (int i = 0; i < n_screen_locs; i++) {
      if (name.equals(screen_name[i])) {
        return screen_size[i];
      }
    }
    // name was not found
    return init_size;
  } // end of get_screen_size


//-------------------------------------------------------------------------------
  /**
   * Saves the screen locations to a text file: "screen_locs.txt"
   */
  static void save_screen_locs() {
    TextFile out_file = new TextFile(fjec.data_path + "screen_locs.txt", TextFile.OUT);
    if (!out_file.ok()) {
      System.out.println("Error saving screen_locs.txt");
      return;
    }
    for (int i = 0; i < n_screen_locs; i++) {
      out_file.println(screen_name[i]);
      out_file.println(screen_loc[i].toString());
      out_file.println(screen_size[i].toString());
    }
    out_file.close();
  }

//-------------------------------------------------------------------------------
  /**
   * Loads the screen locations from the file: "screen_locs.txt"
   */
  static void load_screen_locs() {
    String line;
    TextFile in_file = new TextFile(fjec.data_path + "screen_locs.txt", TextFile.IN);
    if (!in_file.ok()) {
      System.out.println("Cannot open screen_locs.txt");
      int response = JOptionPane.showConfirmDialog(null,
          "Cannot open screen_locs.txt. Do you want to try to continue running FJEC?",
          "DATA FILE MISSING", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
      if (response != JOptionPane.YES_OPTION) {
        System.exit(1);
      }
    }
    // read the file
    line = in_file.readLine();
    int i = 0;
    while (line != null) {
      screen_name[i] = line;
      line = in_file.readLine();
      screen_loc[i] = new Point(nth_int_in_String(line, 1), nth_int_in_String(line, 2));
      line = in_file.readLine();
      screen_size[i] = new Dimension(nth_int_in_String(line, 1), nth_int_in_String(line, 2));
      line = in_file.readLine();
      i++;
      if (i == MAX_SCREEN_LOCS) line = null;
    }
    n_screen_locs = i;
    in_file.close();
  } // end of load_screen_locs


//-------------------------------------------------------------------------------
  /**
   *returns the n'th integer in s. e.g. when n = 2 and s = "[100,200]", returns 200
   *@param s string, line of integers delimited by ','
   *@param n int, number of numbers to move over
   */
  static int nth_int_in_String(String s, int n) {
    // returns the n'th integer in s. e.g. when n = 2 and s = "[100,200]", returns 200
    int p1 = 0;
    int p2 = 0;
    for (int i = 0; i < n; i++) {
      p1 = p2;
      while (!Character.isDigit(s.charAt(p1))) p1++;
      p2 = p1 + 1;
      while (Character.isDigit(s.charAt(p2))) p2++;
    }
    int answer = Integer.parseInt(s.substring(p1,p2));
    return answer;
  } // end of nth_int_in_String

} //end of class fjec

