package uffjec;


/**
 *Handles the current version of the package
 *Date is hard coded in the version field
 *Date is six digits starting with the year then month then day
 *This string is used for the name in the title bar
 */
  public class version {
  public static final String rcsID = "$Name:  $ $Id: version.java,v 0.1 2003/11/19 22:42:55 drashkin beta $";
  public static final String version = "2003/06/10";
}
