
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Modified:     by Frank Varosi, 2001 (to make it work right with epics).
//Company:      University of Florida
//Description:

package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JComboBox;
//===============================================================================
/**
 * Combo box that deals with EPICS Record Fields
 * Can talk to two separate record fields one for the input and another for the ouput
 */

public class EPICSComboBox extends JComboBox implements UFCAToolkit.MonitorListener, ItemListener, ActionListener {
  public static final String rcsID = "$Name:  $ $Id: EPICSComboBox.java,v 0.18 2005/08/05 19:05:00 drashkin Exp $";
  public static final int INDEX = 0;
  public static final int ITEM = 1;

    private boolean MONITOR_RECEIVED = false;

  int item_OR_index;
  String putRec;
  String monitorRec;
  String setVal = "";
  String putVal = "";
  String monVal = "";
  boolean dual; // true if putRec and monitorRec are two different recs
  boolean doPuts = true;
  long timeStamp = 0;

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public EPICSComboBox() {
    try  {
      jbInit("test", "test", null, ITEM);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating a default EPICSComboBox constructor: " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param rec String: Record field
   */
  public EPICSComboBox(String rec) {
    try  {
      jbInit(rec, rec, null, ITEM);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSComboBox " + rec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   */
  public EPICSComboBox(String putRec, String monitorRec) {
    try  {
      jbInit(putRec, monitorRec, null, ITEM);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor
   * Set whether you want to communicate with
   * Combobox items or combobox indexes to EPICS
   *@param rec String: record field
   *@param items Array of Strings: list of values to be passed to the record field
   *@param _index_OR_item int: INDEX or ITEM orientation
   */
  public EPICSComboBox(String rec, String [] items, int _index_OR_item) {
    try  {
      jbInit(rec, rec, items, _index_OR_item);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSComboBox " + rec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor for communicating with input AND output EPICS records
   * Set whether you want to communicate with Combobox items or combobox indexes to EPICS
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   *@param items Array of Strings: list of items in the combo box
   *@param _index_OR_item int: INDEX or ITEM orientation
   */
  public EPICSComboBox(String putRec, String monitorRec, String [] items, int _index_OR_item) {
    try  {
      jbInit(putRec, monitorRec, items, _index_OR_item);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor for communicating with input AND output EPICS records
   * Set whether you want to communicate with Combobox items or combobox indexes to EPICS
   *@param putRec  String: Input Record Field to be monitored
   *@param monitorRec  String: Output Record Field to be monitored
   *@param items  Array of Strings: list of items in the combo box
   *@param _index_OR_item  int: INDEX or ITEM orientation
   *@param doPuts  boolean: false means do NOT automatically put to EPICS (default is true)
   */
  public EPICSComboBox(String putRec, String monitorRec, String [] items, int _index_OR_item, boolean doPuts)
    {
	try {
	    this.doPuts = doPuts;
	    jbInit(putRec, monitorRec, items, _index_OR_item);
	}
	catch(Exception ex) {
	    fjecError.show("Error creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
	}
    } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Component initialization -- returns no arguments
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   *@param items Array of Strings: list of items in the combo box
   *@param item_OR_index int: INDEX or ITEM orientation
   */
  private void jbInit(String putRec, String monitorRec, String[] items, int item_OR_index) throws Exception
    {
	dual = !putRec.equals(monitorRec);
	this.item_OR_index = item_OR_index;
	this.setMaximumRowCount(14);
	this.timeStamp = 0;
	if (items != null) {
	    for( int i=0; i<items.length; i++ ) this.addItem( items[i].trim() );
	}

	// setup monitor(s)
	this.putRec = putRec;
	this.monitorRec = monitorRec;
	
	EPICS.addMonitor(monitorRec,this);
	/*
	int mask = 7;
		
	try {
	    gov.aps.jca.Channel theChannel = EPICS.theContext.createChannel(monitorRec);
	    EPICS.theContext.pendIO(EPICS.TIMEOUT);
	    outMon = theChannel.addMonitor(mask,this);
	    EPICS.theContext.flushIO();
	    //EPICS.monitorCheat(outMon,this).start();
	}
	catch (Exception ex) { this.setBackground(Color.red); }
	*/
	//monVal = EPICS.get( monitorRec );
	//setVal = monVal;
	setToolTipText();
       	//addItemListener(this);
	addActionListener(this);
	MONITOR_RECEIVED = false;
    } //end of jbInit

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
	putRec = dbname + putRec.substring(putRec.indexOf(":")+1);
	monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
	EPICS.addMonitor(monitorRec,this);
    }



    public long getTimeStamp () { return timeStamp; }
    
//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(String value) {
    try {
	
 
	//      if( event.pv().name().equals(this.monitorRec) )
	monVal = value.trim();
	this.setToolTipText();
	if (!setVal.trim().equals(monVal.trim()))
	    MONITOR_RECEIVED=true;
	if (item_OR_index == ITEM) {
	    setSelectedItem(monVal);
	} else {
	    setSelectedIndex(Integer.parseInt(monVal.trim()));
	}
	
	if( setVal.equals( monVal ) )
	    this.setBackground(Color.white);
	else if( ! setVal.trim().equals("") )
	    this.setBackground(Color.yellow);
	
	timeStamp = System.currentTimeMillis();
    }
    catch (Exception ex) {
      fjecError.show("Error in monitor changed for EPICSComboBox: " + monitorRec + " " + ex.toString());
    }
  } //end of monitorChanged

//-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ie ItemEvent: TBD
     */
    public void itemStateChanged(ItemEvent ie)
    {
      if( ie.getStateChange() == ItemEvent.SELECTED )
	  {
	      if( item_OR_index == ITEM )
		  setVal = this._getSelectedItem();
	      else
		  setVal = this._getSelectedIndex();

	      if( setVal.equals( monVal ) )
		  this.setBackground(Color.white);
	      else
		  this.setBackground(Color.yellow);
	      
	      if (doPuts){
		   putcmd( setVal );
		   putVal = setVal;
	      }
	      MONITOR_RECEIVED = false;
	  }
    } //end of itemStateChanged

    public void actionPerformed(ActionEvent ae) {
	if( item_OR_index == ITEM )
	    setVal = this._getSelectedItem();
	else
	    setVal = this._getSelectedIndex();
	
	if( setVal.equals( monVal ) )
	    this.setBackground(Color.white);
	else
	    this.setBackground(Color.yellow);
	
	if (doPuts)
	    putcmd(setVal);
	    //if (!MONITOR_RECEIVED) putcmd( setVal );
	    //else { putVal = setVal;}
	//putcmd(setVal);
	MONITOR_RECEIVED = false;
	
    }

//-------------------------------------------------------------------------------
  /**
   * Returns the String name of the selected item in the combo box,
   * with comments after blank removed.
   */
  public String _getSelectedItem() {
    String item = (String)this.getSelectedItem();
    if (item == null) item = "";
    if( item.indexOf(" ") > 0 ) //anything after a blank is a comment so eliminate it:
	item = item.substring( 0, item.indexOf(" ") );
    return item.trim();
  } //end of _getSelectedItem

//-------------------------------------------------------------------------------
  /**
   * Returns the integer index of the selected item in the combo box, as a string.
   */
  public String _getSelectedIndex() {
    int item = this.getSelectedIndex();
    return String.valueOf( item );
  } //end of _getSelectedIndex

//-------------------------------------------------------------------------------
  /**
   * Sets the text to go in the tool tip -- returns no arguments
   */
  void setToolTipText() {
      this.setToolTipText( putRec + " = " + putVal + " -> " + monitorRec + " = " + monVal );
  }

//-------------------------------------------------------------------------------
  /**
   * Puts the currently selected item/index into the 
   * putRec (Input Record Field)
   *@param val String: value to be put
   */
    void forcePut() {
	putVal = "";
	if (item_OR_index == ITEM) 
	    putcmd( this._getSelectedItem() );
	else 
	    putcmd( this._getSelectedIndex() );
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val (value) into the putRec (Input Record Field)
   *@param val String: value to be put
   */
  void putcmd(String val) {
      if( ! val.trim().equals("") ) {
	  if( val.indexOf(" ") > 0 ) //anything after a blank is a comment so eliminate it:
	      val = val.substring( 0, val.indexOf(" ") );
	  if( ! val.trim().equals( putVal ) ) {
	      //jeiCmd command = new jeiCmd();
	      //command.execute(cmd);
	      EPICS.put(putRec,val.trim());
	      putVal = val;
	  }
      }
  } //end of putcmd

//-------------------------------------------------------------------------------
  /**
   * Sets the combo box to requested value and then puts value to EPICS.
   *@param val String: value to be put
   */
  public void set_and_putcmd(String val) {
      putVal = "";
      this.putcmd(val);
      this.setSelectedItem(val);
  } //end of set_and_putcmd


//-------------------------------------------------------------------------------
  /**
   * TBD??
   *@param e WindowEvent: TBD
   */
    protected void processWindowEvent(WindowEvent e) {
	if(e.getID() == WindowEvent.WINDOW_CLOSING) {
	    try {
		EPICS.removeMonitor(monitorRec,this);
	    }
	    catch (Exception ee) { fjecError.show(e.toString()); }
	}
    } //end of processWindowEvent

} //end of class EPICSComboBox
