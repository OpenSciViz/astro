package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FJECSubSystemPanel extends JPanel {
    
    JButton clearSystemButton;
    JButton initSystemButton;
    JButton datumSystemButton;
    JButton parkSystemButton;

    String ssprefix;

    public static final String consistantLayout = "0.21,0.92;0.50,0.08";

    public FJECSubSystemPanel(String subsystemPrefix) {
	ssprefix = subsystemPrefix.trim();
	if (ssprefix.endsWith(":")) ssprefix = ssprefix.substring(0,ssprefix.length()-1);
	clearSystemButton = new JButton("Clear "+ssprefix.toUpperCase()+" Subsystem");
	//clearSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	initSystemButton = new JButton("Init "+ssprefix.toUpperCase()+" Subsystem");
	//initSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	datumSystemButton = new JButton("Datum "+ssprefix.toUpperCase()+" Subsystem");
	//datumSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	parkSystemButton = new JButton("Park "+ssprefix.toUpperCase()+" Subsystem");
	//parkSystemButton.setFont(new Font("serif",Font.PLAIN,10));
	ssprefix += ":";
	setLayout(new GridLayout(1,0,10,10));
	add(clearSystemButton);
	add(initSystemButton);
	add(datumSystemButton);
	add(parkSystemButton);
	clearSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    clearSystem();
		}
	    });
	initSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    initSystem();
		}
	    });
	datumSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    datumSystem();
		}
	    });
	parkSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    parkSystem();
		}
	    });
    }

    public void datumSystem() {
	EPICS.put(EPICS.prefix+ssprefix+"datum.All",EPICS.PRESET);
	EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
    }

    public void initSystem() {
// 	if (ssprefix.equals("cc:")) {
// 	    if (JOptionPane.showConfirmDialog(this,"Origin all indexors after connecting?") == JOptionPane.OK_OPTION)
// 		EPICS.put(EPICS.prefix+"cc:init.Origin","All");
// 	    if (JOptionPane.showConfirmDialog(this,"Force status retrieval after connecting?") == JOptionPane.OK_OPTION)
// 		EPICS.put(EPICS.prefix+"cc:init.Status","All");
	    
// 	}
	EPICS.put(EPICS.prefix+ssprefix+"init.DIR",EPICS.PRESET);
	EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
    }

    public void clearSystem() {
	//EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.CLEAR);
	//EPICS.put(EPICS.prefix+ssprefix+"initC",EPICS.IDLE);
	//EPICS.put(EPICS.prefix+ssprefix+"datumC",EPICS.IDLE);
	//EPICS.put(EPICS.prefix+ssprefix+"datum.DIR",EPICS.CLEAR);
	//EPICS.put(EPICS.applyPrefix+"applyC",EPICS.IDLE);
	//EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.CLEAR);
	EPICS.put(EPICS.prefix+ssprefix+"setup.DIR",EPICS.CLEAR);
	EPICS.put(EPICS.prefix+ssprefix+"datum.DIR",EPICS.CLEAR);
    }
    public void parkSystem() {
	System.err.println("FJECSubsystemPanel.parkSystem> Not Yet Implemented");
    }
    
}
