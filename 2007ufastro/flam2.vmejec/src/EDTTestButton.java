package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFProtocol.*;
import java.net.*;
import java.awt.image.*;

public class EDTTestButton extends JButton implements ActionListener{

    public String host ;
    public int port;

    public EDTTestButton(String title, String host, int port){
	this(title);
	this.host = new String(host);
	this.port = port;
    }
    

    public EDTTestButton(String title) {
	super(title);
	addActionListener(this);
    	host = "localhost";
	port = 52001;
    }


    public void getHostAndPort() {
	final JDialog jd = new JDialog();
	JPanel diagPan = new JPanel();
	final JTextField hostField = new JTextField(host,10);
	final JTextField portField = new JTextField(port+"",5);
	JButton okButton = new JButton("OK");
	okButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {host = hostField.getText().trim();}
		    catch (Exception e) { System.err.println("EDTTestButton.getHostAndPort> "+e.toString());}
		    try {port = Integer.parseInt(portField.getText().trim());}
		    catch (Exception e) { System.err.println("EDTTestButton.getHostAndPort> "+e.toString());}
		    jd.dispose();
		}
	    });
	diagPan.add(new JLabel("Host:"));
	diagPan.add(hostField);
	diagPan.add(new JLabel("Port:"));
	diagPan.add(portField);
	diagPan.add(okButton);
	jd.setContentPane(diagPan);
	jd.setSize(300,80);
	jd.setLocation(this.getLocationOnScreen());
	jd.setModal(true);
	jd.setVisible(true);
    }

    public void actionPerformed(ActionEvent ae) {
	try {
	    getHostAndPort();
	    UFObsConfig oc;
	    UFFrameConfig fc;
	    Socket soc = new Socket(host,port);
	    UFTimeStamp uft = new UFTimeStamp("DummyJavaApp");
	    uft.sendTo(soc);
	    UFProtocol p = UFProtocol.createFrom(soc);
	    System.out.println(p.name());
	    String [] strs = {"-frame","flam:512Image"};
	    UFStrings ufs = new UFStrings("DummyJavaApp",strs);
	    ufs.sendTo(soc);
	    UFProtocol ufp = UFProtocol.createFrom(soc);
	    System.out.println(ufp.name());



	    ufp = UFProtocol.createFrom(soc);
	    if (ufp == null) {
		System.err.println("JPanelEngObserve.j_action_Performed> Got null UFProtocol object from edt daemon.");
		return;
	    }
	    /*
	    if (ufp.typeId() == UFProtocol.MsgTyp._TimeStamp_) {
		ufp = UFProtocol.createFrom(soc);
	    } 
	    if (ufp.typeId() == UFProtocol.MsgTyp._ObsConfig_) {
		oc = (UFObsConfig)UFProtocol.createFrom(soc);
		ufp = UFProtocol.createFrom(soc);
	    }
	    if (ufp.typeId() == UFProtocol.MsgTyp._FrameConfig_) {
		fc = (UFFrameConfig)UFProtocol.createFrom(soc);
		ufp = UFProtocol.createFrom(soc);
	    }
	    */
	    if (ufp instanceof UFInts) {
		UFInts q = (UFInts)ufp;
		int [] vals = q.values();
		if (vals == null) {
		    System.err.println("JPanelEngObserve.j_action_Performed> Got null int array");
		    System.err.println("JPanelEngObserve.j_action_Performed> "+q.toString());
		    return;
		}
		System.out.println("Val len: "+vals.length);
		final BufferedImage bim = new BufferedImage(512,512,BufferedImage.TYPE_INT_RGB);
		for (int j=0; j<512; j++)
		    for (int i=0; i<512; i++)
			bim.setRGB(i,j,vals[j*512+i]);
		JPanel pan = new JPanel(){
			public void paint(Graphics g) {
			    g.drawImage(bim,0,0,null);
			}
		    };
		JFrame j = new JFrame();
		j.setSize(512,512);
		j.setContentPane(pan);
		j.setVisible(true);
	    } else {
		System.err.println("JPanelEngObserve.j_action_Performed> Expected UFInts. Got: "+ufp.description());
	    }
	} catch (Exception e) {
	    System.err.println("JPanelEngObserve.j_action_Performed> "+e.toString());
	    System.err.println("JPanelEngObserve.j_action_Performed> Trying to connect to edt daemon on host: "+host+", port: "+port);
	}
    }


    public static void main(String [] args) {
	JFrame j = new JFrame();
	JPanel p = new JPanel();
	p.add(new EDTTestButton("Test"));
	j.setContentPane(p);
	j.setSize(400,400);
	j.setDefaultCloseOperation(3);
	j.setVisible(true);
    }

}
