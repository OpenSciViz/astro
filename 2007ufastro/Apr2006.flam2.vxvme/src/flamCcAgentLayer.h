
/*
 *
 *  Header for flamCcAgentLayer.h
 *
 */




/*
 *
 *  Public function prototypes
 *
 */

long ccCadInit (cadRecord * pcr);
long ccCadSend (cadRecord * pcr);
long ccCadNoSend (cadRecord * pcr);

long ccGensubInit (genSubRecord * pgs);
long ccGensubSend (genSubRecord * pgs);
