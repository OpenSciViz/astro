/* RCS */
#if !defined( __UFBANCOM_C__ )
#define __UFBANCOM_C__

static const char _rcsId[] = "$Id: ufBancom.c,v 0.0 2004/10/04 18:08:50 hon Developmental $";
static const char _rcsDate[] = "$Date: 2004/10/04 18:08:50 $";

/* this gets picked up in time.h via vxANSI.h...
#define _EXTENSION_POSIX_REENTRANT 1 
*/

/* vxworks */
#include "vxWorks.h"
#include "timexLib.h"
#include "tickLib.h"
#include "memLib.h"
#include "ellLib.h"
#include "taskLib.h"
#include "semLib.h"
#include "ioLib.h"
#include "fioLib.h"
#include "stdioLib.h"
#include "ellLib.h"
/*
#include "clockLib.h"
#include "ansiTime.h"
*/
/* epics */
#include "dbDefs.h"
#include "dbAccess.h"
#include "dbBase.h"
/* #include "dbRecType.h"
#include "dbRecords.h" */
#include "dbCommon.h"
#include "recSup.h"
#include "devSup.h"
#include "drvSup.h"
/* #include "choice.h" */
#include "special.h"
/* #include "dbRecDes.h" */
#include "dbStaticLib.h"
#include "dbEvent.h"
#include "fast_lock.h"
#include "callback.h"
#include "cad.h"
#include "car.h"
/* #include "genSub.h" */
#include "genSubRecord.h"
#include "cadRecord.h"
#include "sirRecord.h"
#include "dbFldTypes.h"

/* posix */
#include "time.h"
#include "math.h"
#include "stdlib.h"
#include "errno.h"

/* this introduces a conflict with another include (memcmp & memcpy)?
#include "string.h"
*/
extern char *rindex(const char *s, int c);
extern size_t strlen (const char *s);
extern char *strcpy (char *s1, const char *s2);
extern char *strncpy (char *s1, const char *s2, size_t n);
extern void *memset (void *s, int c, size_t n);
extern char *index (const char *s,  /* string in which to find character */
                    int c           /* character to find in string       */);

/* uf */
#include "ufLog.h"

/* Assuming once/sec. periodic processing... */
#define __UFMAXPERIOD__ 3600

/*************************** private addresses/variables and private functions *********/

static int _adjperiod = 1; /* should ramp up from 1 sec  3600 for 1 hour */

/* bancom event (bit) field */
#define __UFBCEFIELDS__ 5 
/* page 3-4 Datum Inc. - bc635vme/bc350vxi manual */
static unsigned short _efield[__UFBCEFIELDS__]; 

#if CPU_FAMILY == MC680X0
static unsigned char* _bc635vme_base = (unsigned char*) 0xffff4000;
#else /* assume ppc */
static unsigned char* _bc635vme_base = (unsigned char*) 0xfbff4000;
/*
static unsigned char* _bc635vme_base = (unsigned char*) 0x00004000;
static unsigned char* _bc635vme_base = (unsigned char*) 0x08004000;
*/
#endif

static const unsigned char _bc635vme_soh = 0x01;
static const unsigned char _bc635vme_etb = 0x17;
static const unsigned char _bc635vme_tfp_bit3 = 0x08;
static const unsigned char _bc635vme_tfp_exec = 0x80;
static const unsigned char _bc635vme_tfp_clearbit = 0x01;
static const unsigned char _bc635vme_tfp_clear = 0x81; /* OR the above two bits */
/*
// initialize these in functions:
// although example in manual uses 0x0a offset (location of time request latch/strobe?)
// tables on page 3-4 indicate 0x0c.
// Note these global intilizations do not seem to get performed by c/c++ compiler?
*/
static unsigned short* _bc635vme_read= 0; /* = (unsigned short*)(_bc635vme_base+0x0c); */
static unsigned short* _bc635vme_reqlatch= 0; /* = (unsigned short*)(_bc635vme_base+0x0a); */
static unsigned short* _bc635vme_fifo= 0; /* = (unsigned short*)(_bc635vme_base+0x26); */
static unsigned short* _bc635vme_ack= 0; /* (unsigned short*)(_bc635vme_base+0x22); */
static unsigned short* _bc635vme_instat= 0; /* (unsigned short*)(_bc635vme_base+0x2a); */

/* static unsigned short* _bc635vme_irqmask= 0; (unsigned short*)(_bc635vme_base+0x28); */
/* static unsigned short* _bc635vme_reset= (unsigned short*)(_bc635vme_base+0x04); */
/* static unsigned short* _bc635vme_unlock= (unsigned short*)(_bc635vme_base+0x20); */
/* static unsigned short* _bc635vme_cmd= (unsigned short*)(_bc635vme_base+0x24); */

static unsigned short* _rp = 0;
static unsigned short _latch = 0;

static struct timespec _bctimespec;
static struct timespec _systimespec;
static char _bctimestr[] = "ddd:hh:mm:ss.uu";
static int _setsysclock = -1; /* every (3600 sec./iterations) hour */

/* SEM_ID is 'struct semaphore*' (pointer) */
static SEM_ID _semMutex= 0;

static void _ufGetBancomTime() {
  struct tm t; /* t.tm_year, t.tm_yday, t.tm_hour, t.tm_min, t.tm_sec */
  time_t bcsec, bcsubsec, yrsec, mksec;
  int rcsyr = 102, nowyr = 2002, maxdays = 364; /* days from 0 - 364 non leap years, 365 leap year */
  char rcsYr[] = "yyyy";
  char* pdate= 0;

  unsigned short status= 0;
  unsigned short days= 0;
  unsigned short hours= 0;
  unsigned short min= 0;
  unsigned short sec= 0;
  unsigned short hun= 0;
  int i;

  if( _semMutex == 0 )
    _semMutex = semMCreate(SEM_Q_PRIORITY);
  semTake(_semMutex, WAIT_FOREVER);
  
  /* current system time, only interested in current year */
  memset(&_systimespec, 0, sizeof(_systimespec));
  if( clock_gettime(CLOCK_REALTIME, &_systimespec) != OK )
    fprintf( stderr, "_ufGetBancomTime> clock_gettime failed...\n");

  memset(&t, 0, sizeof(t));
  if( gmtime_r(&_systimespec.tv_sec, &t) != OK )
    fprintf( stderr, "_ufGetBancomTime> gmtime_r failed...\n"); /* assume that current sys. time is at least accurate to this year! */
  fprintf( stderr, "_ufGetBancomTime> sysclock: %03d:%02d:%02d:%02d.00\n", t.tm_yday, t.tm_hour, t.tm_min, t.tm_sec );
  yrsec = t.tm_sec + 60*t.tm_min + 60*60*t.tm_hour + 24*60*60*t.tm_yday;

  /* current sys. time may be totally wrong if cpu battery is dead and there was a power glitch, so check rcsDate */
  pdate = index(_rcsDate, ' ');
  if( pdate++ ) {
    strncpy(rcsYr, pdate, sizeof(rcsYr));
    rcsyr = atoi(rcsYr) - 1900;
  }
  t.tm_year = rcsyr; 
  mksec = mktime(&t); /* this returns -1 if it fails to convert! */
  if( mksec > 0 && _systimespec.tv_sec < mksec )
    _bctimespec.tv_sec = mksec;
  else if( _systimespec.tv_sec > yrsec ) /* just set to systime, and subtract seconds to start of year */
    _bctimespec.tv_sec = _systimespec.tv_sec - yrsec;
  else
    _bctimespec.tv_sec = 0;

  if( nowyr < (1900+t.tm_year) ) nowyr = 1900+t.tm_year;
  /* sanity check on days value ala leap year */
  if( (nowyr%4==0 && nowyr%100 != 0) || nowyr%400 == 0 ) ++maxdays;
  
  /* read bancom timespec */
  _bc635vme_base = (unsigned char*) 0xffff4000;
  _bc635vme_read = (unsigned short*)(_bc635vme_base+0x0c);
  _rp = _bc635vme_read;
  memset(_efield, 0, sizeof(_efield));
  for(i = 0; i < __UFBCEFIELDS__; i++, _rp++) { 
    /* evidently need to re-read/strobe the latch to cause update of "event fields" */
    _bc635vme_reqlatch = (unsigned short*)(_bc635vme_base+0x0a);
    _latch = *_bc635vme_reqlatch;
    _efield[i] = *_rp;
  }
  status = _efield[0] & 0x00f0;

  /* this is presumably the time specification since Jan. 1 of current year */
  /* parse bc timespec, days starts with 0? */
  days = (_efield[0] & 0x000f) * 100;
  days += ((_efield[1] & 0xf000) >> 12) * 10;
  days += ((_efield[1] & 0x0f00) >> 8);
  if( days > maxdays ) {
    fprintf( stderr, "_ufGetBancomTime> days read reset from %d to 0\n", days);
    days = 0;
  }
  hours = ((_efield[1] & 0x00f0) >> 4) * 10;
  hours += _efield[1] & 0x000f;
  if( hours > 23 ) {
    fprintf( stderr, "_ufGetBancomTime> hours read reset from %d to 0\n", hours);
    hours = 0;
  }
  min = ((_efield[2] & 0xf000) >> 12) * 10;
  min += ((_efield[2] & 0x0f00) >> 8);
  if( min > 59 ) {
    fprintf( stderr, "_ufGetBancomTime> min read reset from %d to 0\n", min);
    min = 0;
  }
  sec = ((_efield[2] & 0x00f0) >> 4) * 10;
  sec += _efield[2] & 0x000f;
  if( sec > 59 ) {
    fprintf( stderr, "_ufGetBancomTime> sec read reset from %d to 0\n", sec);
    sec = 0;
  }
  hun = ((_efield[3] & 0xf000) >> 12) * 10;
  hun += ((_efield[3] & 0x0f00) >> 8);

  bcsec = sec + 60*min + 60*60*hours + 24*60*60*days; /* seconds into this year, if days >= 0? */
  bcsubsec = hun * 1000*1000*10; /* only interested in hundredth of a sec. resolution */
 
  /* set text val. */
  sprintf( _bctimestr, "%03d:%02d:%02d:%02d.%02d", days, hours, min, sec, hun );
  fprintf( stderr, "_ufGetBancomTime> bcread:   %03d:%02d:%02d:%02d.%02d\n", days, hours, min, sec, hun );
  
  /* set bctimespec to match bancom readout (which is time since start of year), adjusted for currebt year (2002 or greater!) */
  _bctimespec.tv_sec += bcsec;
  _bctimespec.tv_nsec = bcsubsec;

  semGive(_semMutex);
  return;
}
static void init_record() {
}

static void _ufSendBancom(char* cmd) {
  char* cmd0 = cmd;
  if( _semMutex == 0 )
    _semMutex = semMCreate(SEM_Q_PRIORITY);
  semTake(_semMutex, WAIT_FOREVER);

  if( strlen(cmd) < 2 ) {
    fprintf(stderr, "_ufSendBancom> cmd must be >= 2 char! (%s)\n", cmd); 
    return;
  }
  else if( cmd[0] != 'A' && cmd[0] != 'B' && cmd[0] != 'C' && cmd[0] != 'L' ) {
    fprintf(stderr, "_ufSendBancom> cmd not supported: %s\n", cmd); 
    return;
  } 

  _bc635vme_fifo = (unsigned short*)(_bc635vme_base+0x26);
  *_bc635vme_fifo = _bc635vme_soh;
  fprintf(stderr, "_ufSendBancom> _bc635vme_fifo= %x, cmd= %s\n", (int)_bc635vme_fifo, cmd);
  do {
    *_bc635vme_fifo = *cmd;
    /* fprintf(stderr, "_ufSendBancomm> _bc635vme_fifo= %x, cmd=  %s\n", (int)_bc635vme_fifo, cmd); */
  } while( *cmd++ != 0 );

  *_bc635vme_fifo = _bc635vme_etb;

  /* according to manual -- busy wait for tfp acknowledge */
  fprintf(stderr, "_ufSendBancom> waiting for fifo acknowledge\n");
  /* clear ack bit */
  _bc635vme_ack = (unsigned short*)(_bc635vme_base+0x22);
  *_bc635vme_ack = _bc635vme_tfp_clear; 
  while( !(*_bc635vme_ack & 0x01 ) ) { ; }

  fprintf(stderr, "_ufSendBancom> acknowledged sent cmd= %s\n", cmd0);

  semGive(_semMutex);
  return;
}


static void _ufsynctime() {
  struct tm st; /* t.tm_year, t.tm_yday, t.tm_hour, t.tm_min, t.tm_sec */
  struct tm bt; /* t.tm_year, t.tm_yday, t.tm_hour, t.tm_min, t.tm_sec */
  int sdrift = abs(_systimespec.tv_sec - _bctimespec.tv_sec);
  int nsdrift = abs(_systimespec.tv_nsec - _bctimespec.tv_nsec);

  if( _semMutex == 0 )
    _semMutex = semMCreate(SEM_Q_PRIORITY);
  semTake(_semMutex, WAIT_FOREVER);

  /* the mv167 (68k) seems to drift relative to the bancom (or vice versa) on the
     order of 0.01 sec every 200 seconds! */
  if( sdrift > 0 || nsdrift > 10000000 ) { /* 10 million nano-sec. is 0.01 sec  */
    /* set sys. clock! */
    if( clock_settime(CLOCK_REALTIME, &_bctimespec) != OK )
      fprintf( stderr, "_ufGetBancomTime> clock_settime failed...\n");
    if( clock_gettime(CLOCK_REALTIME, &_systimespec) != OK )
      fprintf( stderr, "_ufGetBancomTime> clock_gettime failed...\n");
    fprintf(stderr,"ufsynctime> Drift detected, after %d sec.\n", _adjperiod);
    fprintf(stderr,"ufsynctime> Reset Sys clock due to Drift: sdrift= %d, nsdrift= %d\n", sdrift, nsdrift);
    /*
    fprintf(stderr,"ufsynctime> _systimespec.tv_sec = %d, _bctimespec.tv_sec= %d, _systimespec.tv_nsec = %d, _bctimespec.tv_nsec= %d\n",
	    (int)_systimespec.tv_sec, (int)_bctimespec.tv_sec, (int)_systimespec.tv_nsec, (int)_bctimespec.tv_nsec);
    */

    gmtime_r(&_systimespec.tv_sec, &st);
    fprintf(stderr,"ufsynctime> systime:    %04d:%03d:%02d:%02d:%02d.%02d\n",
            1900+st.tm_year, st.tm_yday, st.tm_hour, st.tm_min, st.tm_sec,(int) (_systimespec.tv_nsec/1000));
    gmtime_r(&_bctimespec.tv_sec, &bt);
    fprintf(stderr,"ufsynctime> bancomtime: %04d:%03d:%02d:%02d:%02d.%02d\n",
            1900+bt.tm_year, bt.tm_yday, bt.tm_hour, bt.tm_min, bt.tm_sec,(int) (_systimespec.tv_nsec/1000));
  }
  else {
    if( _adjperiod < __UFMAXPERIOD__ )
      _adjperiod += 100;
    fprintf(stderr,"ufsynctime> No appreciable Drift, next check in %d sec.; systime compares to bancom:\n", _adjperiod);
    gmtime_r(&_systimespec.tv_sec, &st);
    fprintf(stderr,"ufsynctime> systime:    %04d:%03d:%02d:%02d:%02d.%02d\n",
            1900+st.tm_year, st.tm_yday, st.tm_hour, st.tm_min, st.tm_sec,(int) (_systimespec.tv_nsec/1000));
    gmtime_r(&_bctimespec.tv_sec, &bt);
    fprintf(stderr,"ufsynctime> bancomtime: %04d:%03d:%02d:%02d:%02d.%02d\n",
            1900+bt.tm_year, bt.tm_yday, bt.tm_hour, bt.tm_min, bt.tm_sec,(int) (_systimespec.tv_nsec/1000));
  }

  semGive(_semMutex);
  return;
}

static void _ufclearAndWaitBancom() {
  /* clear interrupts ? */
  _bc635vme_instat = (unsigned short*)(_bc635vme_base+0x2a);
  *_bc635vme_instat = 0x00; /* wait for 1pps ? */
  fprintf(stderr, "_ufClearAndWaitBancom> waiting for 1pps bit (whatever that is)\n");

  while( !(*_bc635vme_instat & 0x08) ) { ; } 

  fprintf(stderr, "_ufClearAndWaitBancom> got 1pps bit\n");
  return;
}

/***************************** public functions **********************************/

void ufSetBancom(char* ddd_hh_mm_ss) {
  /* this expects "000_00_00_00" thru "364_23_59_59" or "365_23_59_59" (if leap year) */
  char major_time[] = "Bdddhhmmss";
  /* char* rtclock[] = "Lyymmddhhmmss"; */

  if( strlen(ddd_hh_mm_ss) != 12 || rindex(ddd_hh_mm_ss, '_') == 0 ) {
    fprintf(stderr,"ufSetBancom> badly formated datetime = %s\n",ddd_hh_mm_ss);
    return;
  }
  strncpy(major_time+1, ddd_hh_mm_ss, 3);
  strncpy(major_time+4, ddd_hh_mm_ss+4, 2);
  strncpy(major_time+6, ddd_hh_mm_ss+7, 2);
  strncpy(major_time+8, ddd_hh_mm_ss+10, 2);

  strncpy(major_time+8, ddd_hh_mm_ss+10, 2);
  fprintf(stderr,"ufSetBancom> set datetime = %s, cmd= %s\n",ddd_hh_mm_ss, major_time);
  /* this follows the example in bancom manula page 5-3 */
  /* select mode A1 (free running mode) */
  _ufSendBancom("A1"); 
  _ufclearAndWaitBancom(); 
  _ufSendBancom(major_time);
}

void ufSetTimeFrmBancom( const sirRecord* sir ) {
  if( 0 == sir ) {
    sprintf( _UFerrmsg, __HERE__ "> NULL sir Ptr" );
    ufLog( _UFerrmsg );
      return;
  }
  if( 0 == sir->val ) {
    sprintf( _UFerrmsg, __HERE__ "> NULL sir Ptr->val" );
    ufLog( _UFerrmsg );
    return;
  }

  _ufGetBancomTime(); /* sets _bctimespec & _bctimestr */

  if( _setsysclock < 0 ) { 
    fprintf(stderr,"ufSetTimeFrmBancom> Initial check of Bancom & System times in rec: %s\n", sir->name);
    _setsysclock = 0;
    _ufsynctime();
  }

  if( ++_setsysclock > _adjperiod ) { 
    fprintf(stderr,"ufSetTimeFrmBancom> checking Bancom & System times in rec: %s\n", sir->name);
    _setsysclock = 0;
    _ufsynctime();
  }

  strcpy((char*) sir->val, _bctimestr);

  return;
}

/* why does this print a very different result than the vx shell date? */
/* perhaps the date function is not vx shell, actually epics, and its clock starts ? */ 
void ufsystime() {
  struct tm st; /* t.tm_year, t.tm_yday, t.tm_hour, t.tm_min, t.tm_sec */

  _ufGetBancomTime(); /* sets _bctimespec & _bctimestr */
  _ufsynctime();

  gmtime_r(&_systimespec.tv_sec, &st);
  fprintf(stderr,"ufsystime> %04d:%03d:%02d:%02d:%02d.%02d\n",
          1900+st.tm_year, st.tm_yday, st.tm_hour, st.tm_min, st.tm_sec,(int) (_systimespec.tv_nsec/1000));
  return;
}

#endif /*  __UFBANCOM_C__ */
