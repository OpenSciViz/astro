#if !defined(__UFLog_h__)
#define __UFLog_h__ "$Name:  $ $Id: ufLog.h,v 0.0 2004/10/04 18:08:50 hon Developmental $"
#define __UFLog_H__(arg) const char arg##Log_h__rcsId[] = __UFLog_h__;
#include "sys/types.h"

/* log interface */
#if !defined(_IDL_)

extern void ufLog (const char *msg);

#endif /* _IDL_ */

#ifndef __HERE__
#define __HERE__ __FUNCTION__
#endif

char _UFerrmsg[2048];

#endif /* __UFLog_h__ */
