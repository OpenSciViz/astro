[schematic2]
uniq 54
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos EC Status
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamsSadEcStatus.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2001/01/25
[cell use]
use esirs 1856 1671 100 0 esirs#53
xform 0 2064 1824
p 1792 1376 100 0 0 FTVL:LONG
p 2048 1472 100 0 0 SNAM:ufSetDatumCnt
p 1968 1664 100 768 1 name:$(top)ecDatumCnt
use esirs 544 423 100 0 esirs#40
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)mosCryosPressureOK
use esirs 1248 1671 100 0 esirs#38
xform 0 1456 1824
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)tSMosSpareOK
use esirs 544 839 100 0 esirs#34
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)tSDetFanoutOK
use esirs 544 1255 100 0 esirs#33
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 752 1248 100 1024 -1 name:$(top)tSCamWSurface1OK
use esirs 544 1671 100 0 esirs#32
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 752 1664 100 1024 -1 name:$(top)tSMosWSurfaceOK
use esirs -192 7 100 0 esirs#31
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)camCryosPressureOK
use esirs -192 423 100 0 esirs#30
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -16 416 100 1024 -1 name:$(top)tSCamSpareOK
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -16 832 100 1024 -1 name:$(top)tSCamWSurface2OK
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -16 1248 100 1024 -1 name:$(top)tSCamColdhead1OK
use esirs -192 1671 100 0 esirs#27
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p 0 1664 100 1024 -1 name:$(top)tSMosColdheadOK
use esirs 1248 1287 100 0 esirs#7
xform 0 1456 1440
p 1312 1248 100 0 1 SCAN:Passive
p 1408 1280 100 1024 -1 name:$(top)ecHeartbeat
use esirs 1248 519 100 0 esirs#47
xform 0 1456 672
p 1312 480 100 0 1 SCAN:Passive
p 1408 512 100 1024 -1 name:$(top)ecState
use esirs 1248 903 100 0 esirs#48
xform 0 1456 1056
p 1312 864 100 0 1 SCAN:Passive
p 1408 896 100 1024 -1 name:$(top)ecHealth
use esirs 1248 7 100 0 esirs#49
xform 0 1456 160
p 1312 -32 100 0 1 SCAN:Passive
p 1408 0 100 1024 -1 name:$(top)ecName
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadEcStatus.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
