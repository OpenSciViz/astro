[schematic2]
uniq 8
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 Flamingos-2
s 2432 -192 100 256 Flamingos-2 Top Level Schematic
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2004/05/11
s 2096 -272 100 1792 Author: RRO
s 2512 -240 100 1792 flam2Top.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/11/03
[cell use]
use flamMain 864 775 100 0 flamMain#7
xform 0 1072 1088
p 1152 512 100 0 1 setBusy:CAR_BUSY 2
p 800 512 100 0 1 setClear:CAD_CLEAR 1
p 1152 448 100 0 1 setError:CAR_ERROR 3
p 1152 384 100 0 1 setHeartbeat:HB_TIMEOUT 50
p 1152 576 100 0 1 setIdle:CAR_IDLE 0
p 800 576 100 0 1 setMark:CAD_MARK 0
p 800 448 100 0 1 setPreset:CAD_PRESET 2
p 864 704 100 0 1 setSad:sad flam:sad:
p 800 384 100 0 1 setStart:CAD_START 3
p 864 736 100 0 1 setTop:top flam:
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamTop.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
