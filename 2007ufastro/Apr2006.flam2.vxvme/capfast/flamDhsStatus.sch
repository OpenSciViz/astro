[schematic2]
uniq 662
[tools]
[detail]
w -222 1547 100 0 n#589 elongins.$(cc)heartbeat.FLNK -288 1600 -256 1600 -256 1536 -128 1536 elongouts.elongouts#587.SLNK
w -238 1579 100 0 n#588 elongins.$(cc)heartbeat.VAL -288 1568 -128 1568 elongouts.elongouts#587.DOL
w -644 1523 100 0 n#570 elongouts.elongouts#568.OUT -704 1472 -640 1472 -640 1584 -544 1584 elongins.$(cc)heartbeat.SLNK
w -1060 1579 100 0 n#569 ecalcs.ecalcs#566.FLNK -1184 1664 -1056 1664 -1056 1504 -960 1504 elongouts.elongouts#568.SLNK
w -1374 1891 100 0 n#567 ecalcs.ecalcs#566.VAL -1184 1632 -1120 1632 -1120 1888 -1568 1888 -1568 1824 -1472 1824 ecalcs.ecalcs#566.INPA
w -1054 1539 100 0 n#567 junction -1120 1632 -1088 1632 -1088 1536 -960 1536 elongouts.elongouts#568.DOL
s -1152 960 100 0 Status
s 192 960 100 0 Status
s -480 736 100 0 Array Input of Counters
s 192 1024 100 0 Qlk Data Set Name
s -1152 928 100 0 Frame Count
s -1152 992 100 0 Total Frames
s -1152 1024 100 0 Data Set Name
s 192 384 100 0 flam:dif2 Count
s 192 416 100 0 flam:dif1 Count
s 192 448 100 0 flam:accum(dif2) Count
s 192 480 100 0 flam:accum(dif1) Count
s 192 512 100 0 flam:accum(ref2) Count
s 192 544 100 0 flam:accum(src2) Count
s 192 576 100 0 flam:accum(ref1) Count
s 192 608 100 0 flam:accum(src1) Count
s 192 640 100 0 flam:sig Count
s 192 672 100 0 flam:accum(sig) Count
s 192 704 100 0 flam:ref2 Count
s 192 736 100 0 flam:src2 Count
s 192 768 100 0 flam:ref1 Count
s 192 800 100 0 flam:src1 Count
s -384 1024 100 0 Qlk Data Set Name
s -1728 736 100 0 Frame Count
s -1728 992 100 0 Total Frames
s -1728 1024 100 0 Data Set Name
s -1815 1388 100 0 0 (SIMM FAST)
s -1858 1419 100 0 1 (SIMM NONE, SIMM FULL)
s 736 -192 120 0 dhs01.sch 2/25/2001 11:56 AM EST
s -384 992 100 0 Total Frames
s 192 992 100 0 Total Frames
[cell use]
use estringins -992 103 100 0 estringins#660
xform 0 -864 176
p -880 96 100 1024 -1 name:$(dhs)qlkLog
use estringins -992 -121 100 0 estringins#661
xform 0 -864 -48
p -880 -128 100 1024 -1 name:$(dhs)archiveLog
use ecars -1024 487 100 0 ecars#658
xform 0 -864 656
p -912 480 100 1024 -1 name:$(dhs)expStatusC
use ecars 128 -89 100 0 ecars#659
xform 0 288 80
p 240 -96 100 1024 -1 name:$(dhs)qlkStatusC
use egenSubB -160 231 100 0 egenSubB#657
xform 0 -16 656
p -383 5 100 0 0 FTA:STRING
p -383 5 100 0 0 FTB:STRING
p -383 -27 100 0 0 FTC:STRING
p -383 -59 100 0 0 FTD:STRING
p -383 -91 100 0 0 FTE:STRING
p -383 -155 100 0 0 FTF:STRING
p -383 -155 100 0 0 FTG:STRING
p -383 -187 100 0 0 FTH:STRING
p -383 -219 100 0 0 FTI:STRING
p -383 -251 100 0 0 FTJ:STRING
p -383 -251 100 0 0 FTK:STRING
p -383 -251 100 0 0 FTL:STRING
p -383 -251 100 0 0 FTM:STRING
p -383 -251 100 0 0 FTN:STRING
p -383 -251 100 0 0 FTO:STRING
p -383 -251 100 0 0 FTP:STRING
p -383 5 100 0 0 FTVA:STRING
p -383 5 100 0 0 FTVB:STRING
p -383 -27 100 0 0 FTVC:STRING
p -383 -59 100 0 0 FTVD:STRING
p -383 -91 100 0 0 FTVE:STRING
p -383 -155 100 0 0 FTVF:STRING
p -383 -155 100 0 0 FTVG:STRING
p -383 -187 100 0 0 FTVH:STRING
p -383 -219 100 0 0 FTVI:STRING
p -383 -251 100 0 0 FTVJ:STRING
p -383 -251 100 0 0 FTVK:STRING
p -383 -251 100 0 0 FTVL:STRING
p -383 -251 100 0 0 FTVM:STRING
p -383 -251 100 0 0 FTVN:STRING
p -383 -251 100 0 0 FTVO:STRING
p -448 638 100 0 0 INAM:ufqlkStatusGinit
p -383 -635 100 0 0 NOJ:14
p -448 606 100 0 0 SNAM:ufqlkStatusGproc
p -48 224 100 1024 -1 name:$(dhs)qlkStatusG
use egenSubB -1504 231 100 0 egenSubB#656
xform 0 -1360 656
p -1727 5 100 0 0 FTA:STRING
p -1727 5 100 0 0 FTB:STRING
p -1727 -27 100 0 0 FTC:STRING
p -1727 -251 100 0 0 FTJ:STRING
p -1727 5 100 0 0 FTVA:STRING
p -1727 5 100 0 0 FTVB:STRING
p -1727 -27 100 0 0 FTVC:STRING
p -1727 -59 100 0 0 FTVD:STRING
p -1727 -91 100 0 0 FTVE:STRING
p -1727 -155 100 0 0 FTVF:STRING
p -1727 -155 100 0 0 FTVG:STRING
p -1727 -187 100 0 0 FTVH:STRING
p -1727 -219 100 0 0 FTVI:STRING
p -1727 -251 100 0 0 FTVJ:STRING
p -1727 -251 100 0 0 FTVK:STRING
p -1727 -251 100 0 0 FTVL:STRING
p -1727 -251 100 0 0 FTVM:STRING
p -1727 -251 100 0 0 FTVN:STRING
p -1727 -251 100 0 0 FTVO:STRING
p -1727 -251 100 0 0 FTVP:STRING
p -1727 -251 100 0 0 FTVQ:STRING
p -1727 -251 100 0 0 FTVR:STRING
p -1727 -251 100 0 0 FTVS:STRING
p -1727 -251 100 0 0 FTVT:STRING
p -1727 -251 100 0 0 FTVU:STRING
p -1792 638 100 0 0 INAM:ufexpStatusGinit
p -1792 606 100 0 0 SNAM:ufexpStatusGproc
p -1392 224 100 1024 -1 name:$(dhs)expStatusG
use bc200tr -2064 -472 -100 0 frame
xform 0 -384 832
use elongouts -960 1415 100 0 elongouts#568
xform 0 -832 1504
p -1075 1849 100 0 0 DESC:Heartbeat in SIM FAST Mode
p -944 1376 100 0 1 OMSL:closed_loop
p -848 1408 100 1024 -1 name:$(dhs)simHeartbeat
p -704 1472 75 768 -1 pproc(OUT):PP
use elongouts -128 1447 100 0 elongouts#587
xform 0 0 1536
p -243 1881 100 0 0 DESC:CC Heartbeat Long out
p -112 1408 100 0 1 OMSL:closed_loop
p -16 1440 100 1024 -1 name:$(dhs)heartbeatLO
p 128 1504 75 768 -1 pproc(OUT):PP
use ecalcs -1472 1351 100 0 ecalcs#566
xform 0 -1328 1616
p -1472 1312 100 0 1 CALC:A+1
p -1679 1782 100 0 0 DESC:Starts the hearbeat in SIM FAST Mode
p -1760 1598 100 0 0 DISV:0
p -1760 1726 100 0 0 SCAN:1 second
p -1360 1344 100 1024 -1 name:$(dhs)HeartbeatCALC
use hwout 192 1463 100 0 hwout#590
xform 0 288 1504
p 392 1496 100 0 -1 val(outp):$(sad)dhsHeartbeat.VAL PP NMS
use elongins -544 1511 -100 0 $(cc)heartbeat
xform 0 -416 1584
p -755 1785 100 0 0 DESC:Heartbeat from CC Agent
p -432 1504 100 1024 -1 name:$(dhs)heartbeat
[comments]
RCS: "$Name:  $ $Id: flamDhsStatus.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $"
