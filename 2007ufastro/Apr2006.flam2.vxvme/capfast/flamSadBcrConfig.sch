[schematic2]
uniq 72
[tools]
[detail]
s 2624 2064 100 1792 2001/01/25
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2528 -240 100 1792 flamSadEcConfigure.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos EC Configure
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs -192 1671 100 0 esirs#40
xform 0 16 1824
p -256 1376 100 0 0 FTVL:LONG
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)mos1BarCode
use esirs 352 1671 100 0 esirs#51
xform 0 560 1824
p 288 1376 100 0 0 FTVL:LONG
p 416 1632 100 0 1 SCAN:Passive
p 512 1664 100 1024 -1 name:$(top)mos2BarCode
use esirs 896 1671 100 0 esirs#52
xform 0 1104 1824
p 832 1376 100 0 0 FTVL:LONG
p 960 1632 100 0 1 SCAN:Passive
p 1056 1664 100 1024 -1 name:$(top)mos3BarCode
use esirs 1440 1671 100 0 esirs#53
xform 0 1648 1824
p 1376 1376 100 0 0 FTVL:LONG
p 1504 1632 100 0 1 SCAN:Passive
p 1600 1664 100 1024 -1 name:$(top)mos4BarCode
use esirs 1984 1671 100 0 esirs#54
xform 0 2192 1824
p 1920 1376 100 0 0 FTVL:LONG
p 2048 1632 100 0 1 SCAN:Passive
p 2144 1664 100 1024 -1 name:$(top)mos5BarCode
use esirs -192 1255 100 0 esirs#55
xform 0 16 1408
p -256 960 100 0 0 FTVL:LONG
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)mos6BarCode
use esirs 352 1255 100 0 esirs#56
xform 0 560 1408
p 288 960 100 0 0 FTVL:LONG
p 416 1216 100 0 1 SCAN:Passive
p 512 1248 100 1024 -1 name:$(top)mos7BarCode
use esirs 896 1255 100 0 esirs#57
xform 0 1104 1408
p 832 960 100 0 0 FTVL:LONG
p 960 1216 100 0 1 SCAN:Passive
p 1056 1248 100 1024 -1 name:$(top)mos8BarCode
use esirs 1440 1255 100 0 esirs#58
xform 0 1648 1408
p 1376 960 100 0 0 FTVL:LONG
p 1504 1216 100 0 1 SCAN:Passive
p 1600 1248 100 1024 -1 name:$(top)mos9BarCode
use esirs 1984 1255 100 0 esirs#59
xform 0 2192 1408
p 1920 960 100 0 0 FTVL:LONG
p 2048 1216 100 0 1 SCAN:Passive
p 2144 1248 100 1024 -1 name:$(top)Circ1BarCode
use esirs 1984 839 100 0 esirs#60
xform 0 2192 992
p 1920 544 100 0 0 FTVL:LONG
p 2048 800 100 0 1 SCAN:Passive
p 2144 832 100 1024 -1 name:$(top)mos4MDFString
use esirs 1440 839 100 0 esirs#61
xform 0 1648 992
p 1376 544 100 0 0 FTVL:LONG
p 1504 800 100 0 1 SCAN:Passive
p 1600 832 100 1024 -1 name:$(top)mos3MDFString
use esirs 896 839 100 0 esirs#62
xform 0 1104 992
p 832 544 100 0 0 FTVL:LONG
p 960 800 100 0 1 SCAN:Passive
p 1056 832 100 1024 -1 name:$(top)mos2MDFString
use esirs 352 839 100 0 esirs#63
xform 0 560 992
p 288 544 100 0 0 FTVL:LONG
p 416 800 100 0 1 SCAN:Passive
p 512 832 100 1024 -1 name:$(top)mos1MDFString
use esirs -192 839 100 0 esirs#64
xform 0 16 992
p -256 544 100 0 0 FTVL:LONG
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)Circ2BarCode
use esirs -192 423 100 0 esirs#65
xform 0 16 576
p -256 128 100 0 0 FTVL:LONG
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)mos5MDFString
use esirs 352 423 100 0 esirs#66
xform 0 560 576
p 288 128 100 0 0 FTVL:LONG
p 416 384 100 0 1 SCAN:Passive
p 512 416 100 1024 -1 name:$(top)mos6MDFString
use esirs 896 423 100 0 esirs#67
xform 0 1104 576
p 832 128 100 0 0 FTVL:LONG
p 960 384 100 0 1 SCAN:Passive
p 1056 416 100 1024 -1 name:$(top)mos7MDFString
use esirs 1440 423 100 0 esirs#68
xform 0 1648 576
p 1376 128 100 0 0 FTVL:LONG
p 1504 384 100 0 1 SCAN:Passive
p 1600 416 100 1024 -1 name:$(top)mos8MDFString
use esirs 1984 423 100 0 esirs#69
xform 0 2192 576
p 1920 128 100 0 0 FTVL:LONG
p 2048 384 100 0 1 SCAN:Passive
p 2144 416 100 1024 -1 name:$(top)mos9MDFString
use esirs 352 7 100 0 esirs#70
xform 0 560 160
p 288 -288 100 0 0 FTVL:LONG
p 416 -32 100 0 1 SCAN:Passive
p 512 0 100 1024 -1 name:$(top)Circ2MDFString
use esirs -192 7 100 0 esirs#71
xform 0 16 160
p -256 -288 100 0 0 FTVL:LONG
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)Circ1MDFString
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadBcrConfig.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $
