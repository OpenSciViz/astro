[schematic2]
uniq 110
[tools]
[detail]
w 1346 1739 100 0 n#109 estringouts.estringouts#68.FLNK 1248 1520 1312 1520 1312 1728 1440 1728 elongouts.elongouts#104.SLNK
w 1346 1771 100 0 n#107 elongouts.elongouts#104.DOL 1440 1760 1312 1760 1312 1808 1248 1808 hwin.hwin#108.in
w 1752 1707 100 0 n#105 elongouts.elongouts#104.OUT 1696 1696 1856 1696 hwout.hwout#106.outp
w 1608 1099 100 0 n#78 eseqs.eseqs#98.LNK1 1440 1088 1824 1088 ecars.ecars#53.IVAL
w 1480 1067 100 0 n#78 eseqs.eseqs#98.LNK2 1440 1056 1568 1056 1568 1088 junction
w 1026 1099 100 0 n#103 eseqs.eseqs#98.DOL1 1120 1088 992 1088 992 1136 928 1136 hwin.hwin#100.in
w 1026 1067 100 0 n#102 eseqs.eseqs#98.DOL2 1120 1056 992 1056 992 1008 928 1008 hwin.hwin#101.in
w 664 779 100 0 n#49 ecad2.ecad2#48.STLK 256 768 1120 768 eseqs.eseqs#98.SLNK
w -48 1387 -100 0 c#89 ecad2.ecad2#48.DIR -64 1248 -128 1248 -128 1376 80 1376 80 1696 -32 1696 inhier.DIR.P
w -80 1419 -100 0 c#90 ecad2.ecad2#48.ICID -64 1216 -160 1216 -160 1408 48 1408 48 1568 -32 1568 inhier.ICID.P
w 192 1387 -100 0 c#91 ecad2.ecad2#48.VAL 256 1248 320 1248 320 1376 112 1376 112 1696 256 1696 outhier.VAL.p
w 224 1419 -100 0 c#92 ecad2.ecad2#48.MESS 256 1216 352 1216 352 1408 144 1408 144 1568 256 1568 outhier.MESS.p
w 696 1547 100 0 n#73 ecad2.ecad2#48.VALA 256 1056 448 1056 448 1536 992 1536 estringouts.estringouts#68.DOL
w 728 1515 100 0 n#71 ecad2.ecad2#48.PLNK 256 800 512 800 512 1504 992 1504 estringouts.estringouts#68.SLNK
w 1304 1499 100 0 n#59 estringouts.estringouts#68.OUT 1248 1488 1408 1488 hwout.hwout#60.outp
s 2624 2032 100 1792 2000/12/05
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 flamSetDhsInfo.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/05
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 FLAMINGOS setDhsInfo Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use hwin 736 967 100 0 hwin#101
xform 0 832 1008
p 576 1008 100 0 -1 val(in):$(CAR_IDLE)
use hwin 736 1095 100 0 hwin#100
xform 0 832 1136
p 576 1136 100 0 -1 val(in):$(CAR_BUSY)
use hwin 1056 1767 100 0 hwin#108
xform 0 1152 1808
p 896 1808 100 0 -1 val(in):$(CAD_CLEAR)
use hwout 1408 1447 100 0 hwout#60
xform 0 1504 1488
p 1632 1488 100 0 -1 val(outp):$(top)dc:acqControl.D
use hwout 1856 1655 100 0 hwout#106
xform 0 1952 1696
p 2080 1696 100 0 -1 val(outp):$(top)dc:acqControl.DIR PP NMS
use elongouts 1440 1639 100 0 elongouts#104
xform 0 1568 1728
p 1280 1998 100 0 0 SCAN:Passive
p 1552 1632 100 1024 1 name:$(top)setDhsInfoClear
p 1696 1696 75 768 -1 pproc(OUT):PP
use eseqs 1120 679 100 0 eseqs#98
xform 0 1280 928
p 1184 640 100 0 1 DLY1:0.0e+00
p 1184 608 100 0 1 DLY2:0.5e+00
p 1328 672 100 1024 1 name:$(top)setDhsInfoBusy
p 1456 1088 75 1024 -1 pproc(LNK1):PP
p 1456 1056 75 1024 -1 pproc(LNK2):PP
use outhier 272 1696 100 0 VAL
xform 0 240 1696
use outhier 272 1568 100 0 MESS
xform 0 240 1568
use inhier -96 1696 100 0 DIR
xform 0 -32 1696
use inhier -112 1568 100 0 ICID
xform 0 -32 1568
use estringouts 992 1431 100 0 estringouts#68
xform 0 1120 1504
p 1072 1392 100 0 1 OMSL:closed_loop
p 1184 1424 100 1024 1 name:$(top)setDhsInfoDcWrite
use ecars 1824 807 100 0 ecars#53
xform 0 1984 976
p 1984 800 100 1024 1 name:$(top)setDhsInfoC
use ecad2 -64 679 100 0 ecad2#48
xform 0 96 992
p 0 640 100 0 1 INAM:flamIsNullInit
p 0 592 100 0 1 SNAM:flamIsSetDhsInfoProcess
p 96 672 100 1024 1 name:$(top)setDhsInfo
p 272 768 75 1024 -1 pproc(STLK):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSetDhsInfo.sch,v 1.1 2006/09/14 22:28:02 gemvx Exp $
