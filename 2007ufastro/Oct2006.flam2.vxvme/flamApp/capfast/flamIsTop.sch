[schematic2]
uniq 92
[tools]
[detail]
w 1064 1867 100 0 n#35 flamIsObserveCmds.flamIsObserveCmds#66.MESS 2368 1376 2496 1376 2496 1856 -320 1856 -320 1248 0 1248 eapply.eapply#23.INMC
w 1064 1835 100 0 n#34 flamIsObserveCmds.flamIsObserveCmds#66.VAL 2368 1408 2464 1408 2464 1824 -288 1824 -288 1280 0 1280 eapply.eapply#23.INPC
w 792 1803 100 0 n#33 flamIsSetupCmds.flamIsSetupCmds#65.MESS 1760 1376 1888 1376 1888 1792 -256 1792 -256 1312 0 1312 eapply.eapply#23.INMB
w 792 1771 100 0 n#32 flamIsSetupCmds.flamIsSetupCmds#65.VAL 1760 1408 1856 1408 1856 1760 -224 1760 -224 1344 0 1344 eapply.eapply#23.INPB
w 1304 651 100 0 n#31 eapply.eapply#23.OCLC 384 1248 640 1248 640 640 2016 640 2016 1376 2112 1376 flamIsObserveCmds.flamIsObserveCmds#66.CLID
w 1304 683 100 0 n#30 eapply.eapply#23.OUTC 384 1280 672 1280 672 672 1984 672 1984 1408 2112 1408 flamIsObserveCmds.flamIsObserveCmds#66.DIR
w 1032 715 100 0 n#29 eapply.eapply#23.OCLB 384 1312 704 1312 704 704 1408 704 1408 1376 1504 1376 flamIsSetupCmds.flamIsSetupCmds#65.CLID
w 1032 747 100 0 n#28 eapply.eapply#23.OUTB 384 1344 736 1344 736 736 1376 736 1376 1408 1504 1408 flamIsSetupCmds.flamIsSetupCmds#65.DIR
w 520 1739 100 0 n#27 flamIsSystemCmds.flamIsSystemCmds#64.MESS 1152 1376 1280 1376 1280 1728 -192 1728 -192 1376 0 1376 eapply.eapply#23.INMA
w 520 1707 100 0 n#26 flamIsSystemCmds.flamIsSystemCmds#64.VAL 1152 1408 1248 1408 1248 1696 -160 1696 -160 1408 0 1408 eapply.eapply#23.INPA
w 616 1387 100 0 n#25 eapply.eapply#23.OCLA 384 1376 896 1376 flamIsSystemCmds.flamIsSystemCmds#64.CLID
w 616 1419 100 0 n#24 eapply.eapply#23.OUTA 384 1408 896 1408 flamIsSystemCmds.flamIsSystemCmds#64.DIR
s 2624 2064 100 1792 2000/12/08
s 2480 2064 100 1792 RRO
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamIsTop.sch
s 2096 -272 100 1792 Author: RRO
s 2096 -240 100 1792 2000/12/08
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos IS Top
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use changeBar 1984 2023 100 0 changeBar#91
xform 0 2336 2064
use flamIsObserveCmds 2112 807 100 0 flamIsObserveCmds#66
xform 0 2240 1168
use flamIsSetupCmds 1504 807 100 0 flamIsSetupCmds#65
xform 0 1632 1168
use flamIsSystemCmds 896 807 100 0 flamIsSystemCmds#64
xform 0 1024 1168
use eapply 0 871 100 0 eapply#23
xform 0 192 1232
p 176 848 100 1024 1 name:$(top)is:apply
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamIsTop.sch,v 1.1 2006/09/14 22:27:52 gemvx Exp $
