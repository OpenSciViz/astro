[schematic2]
uniq 434
[tools]
[detail]
w 818 331 100 0 n#431 ecad20.ecad20#93.VALR 448 320 1248 320 1248 576 1552 576 egenSub.egenSub#289.INPH
w 786 395 100 0 n#430 ecad20.ecad20#93.VALQ 448 384 1184 384 1184 640 1552 640 egenSub.egenSub#289.INPG
w 1234 1899 100 0 n#359 egenSub.egenSub#319.INPB 1536 1888 992 1888 992 1344 448 1344 ecad20.ecad20#93.VALB
w 1242 1035 100 0 n#359 junction 992 1344 992 1024 1552 1024 egenSub.egenSub#289.INPA
w 1922 1451 100 0 n#422 egenSub.egenSub#319.OUTI 1824 1440 2080 1440 hwout.hwout#423.outp
w 1922 1899 100 0 n#421 egenSub.egenSub#319.OUTB 1824 1888 2080 1888 hwout.hwout#420.outp
w 1170 1771 100 0 n#364 junction 864 1216 864 1760 1536 1760 egenSub.egenSub#319.INPD
w 1178 907 100 0 n#364 egenSub.egenSub#289.INPC 1552 896 864 896 864 1216 448 1216 ecad20.ecad20#93.VALD
w 1394 1451 100 0 n#413 junction 1312 512 1312 1440 1536 1440 egenSub.egenSub#319.INPI
w 850 267 100 0 n#413 ecad20.ecad20#93.VALS 448 256 1312 256 1312 512 1552 512 egenSub.egenSub#289.INPI
w 1930 523 100 0 n#416 egenSub.egenSub#289.OUTI 1840 512 2080 512 hwout.hwout#417.outp
w 1930 1035 100 0 n#415 egenSub.egenSub#289.OUTA 1840 1024 2080 1024 hwout.hwout#414.outp
w 1082 715 100 0 n#407 ecad20.ecad20#93.VALH 448 960 672 960 672 704 1552 704 egenSub.egenSub#289.INPF
w 1362 1515 100 0 n#407 egenSub.egenSub#319.INPH 1536 1504 1248 1504 1248 704 junction
w 1210 971 100 0 n#409 ecad20.ecad20#93.VALC 448 1280 928 1280 928 960 1552 960 egenSub.egenSub#289.INPB
w 1202 1835 100 0 n#409 junction 928 1280 928 1824 1536 1824 egenSub.egenSub#319.INPC
w 1114 779 100 0 n#429 egenSub.egenSub#289.INPE 1552 768 736 768 736 1024 448 1024 ecad20.ecad20#93.VALG
w 1330 1579 100 0 n#429 junction 1184 768 1184 1568 1536 1568 egenSub.egenSub#319.INPG
w 58 171 100 0 n#406 hwin.hwin#405.in 48 160 128 160 ecad20.ecad20#93.INPT
w 1930 907 100 0 n#402 egenSub.egenSub#289.OUTC 1840 896 2080 896 hwout.hwout#403.outp
w 1922 1963 100 0 n#398 egenSub.egenSub#319.OUTA 1824 1952 2080 1952 hwout.hwout#399.outp
w 1106 1963 100 0 n#397 egenSub.egenSub#319.INPA 1536 1952 736 1952 736 1408 448 1408 ecad20.ecad20#93.VALA
w 930 235 100 0 n#394 ecad20.ecad20#93.PLNK 448 0 576 0 576 224 1344 224 1344 352 1552 352 egenSub.egenSub#289.SLNK
w 1170 203 100 0 n#354 efanouts.efanouts#96.LNK1 912 0 1024 0 1024 192 1376 192 1376 1376 1536 1376 egenSub.egenSub#319.INPJ
w 1922 1707 100 0 n#340 egenSub.egenSub#319.OUTE 1824 1696 2080 1696 hwout.hwout#347.outp
w 1922 1835 100 0 n#338 egenSub.egenSub#319.OUTC 1824 1824 2080 1824 hwout.hwout#348.outp
w 1922 1771 100 0 n#337 egenSub.egenSub#319.OUTD 1824 1760 2080 1760 hwout.hwout#349.outp
w 1922 1643 100 0 n#336 egenSub.egenSub#319.OUTF 1824 1632 2080 1632 hwout.hwout#350.outp
w 1922 1579 100 0 n#335 egenSub.egenSub#319.OUTG 1824 1568 2080 1568 hwout.hwout#351.outp
w 1922 1515 100 0 n#334 egenSub.egenSub#319.OUTH 1824 1504 2080 1504 hwout.hwout#352.outp
w 1298 1643 100 0 n#367 junction 1120 832 1120 1632 1536 1632 egenSub.egenSub#319.INPF
w 1146 843 100 0 n#367 egenSub.egenSub#289.INPD 1552 832 800 832 800 1088 448 1088 ecad20.ecad20#93.VALF
w 722 1163 100 0 n#365 egenSub.egenSub#319.INPE 1536 1696 1056 1696 1056 1152 448 1152 ecad20.ecad20#93.VALE
w 1930 715 100 0 n#305 egenSub.egenSub#289.OUTF 1840 704 2080 704 hwout.hwout#137.outp
w 1930 779 100 0 n#304 egenSub.egenSub#289.OUTE 1840 768 2080 768 hwout.hwout#136.outp
w 1930 843 100 0 n#303 egenSub.egenSub#289.OUTD 1840 832 2080 832 hwout.hwout#135.outp
w 1930 587 100 0 n#302 egenSub.egenSub#289.OUTH 1840 576 2080 576 hwout.hwout#134.outp
w 1930 651 100 0 n#301 egenSub.egenSub#289.OUTG 1840 640 2080 640 hwout.hwout#133.outp
w 1930 971 100 0 n#299 egenSub.egenSub#289.OUTB 1840 960 2080 960 hwout.hwout#131.outp
w 1474 75 100 0 n#385 flamSubSysCommand.flamSubSysCommand#56.CAR_IVAL 1408 64 1600 64 ecars.ecars#53.IVAL
w 1474 11 100 0 n#386 flamSubSysCommand.flamSubSysCommand#56.CAR_IMSS 1408 0 1600 0 ecars.ecars#53.IMSS
w 986 -21 100 0 n#384 efanouts.efanouts#96.LNK2 912 -32 1120 -32 flamSubSysCommand.flamSubSysCommand#56.START
w 482 -21 100 0 n#356 ecad20.ecad20#93.STLK 448 -32 576 -32 576 -80 672 -80 efanouts.efanouts#96.SLNK
w 144 1739 -100 0 c#89 ecad20.ecad20#93.DIR 128 1600 64 1600 64 1728 272 1728 272 1984 160 1984 inhier.DIR.P
w 112 1771 -100 0 c#90 ecad20.ecad20#93.ICID 128 1568 32 1568 32 1760 240 1760 240 1856 160 1856 inhier.ICID.P
w 384 1739 -100 0 c#91 ecad20.ecad20#93.VAL 448 1600 512 1600 512 1728 304 1728 304 1984 448 1984 outhier.VAL.p
w 416 1771 -100 0 c#92 ecad20.ecad20#93.MESS 448 1568 544 1568 544 1760 336 1760 336 1856 448 1856 outhier.MESS.p
s 1920 1456 100 0 MOS Bar Code mode
s 1920 1904 100 0 window mode
s 1952 528 100 0 Mos Bar pos
s 1952 1040 100 0 Window pos
s 576 224 100 0 MOS Set name
s 576 1360 100 0 Window name
s -128 1328 100 0 Window
s -128 816 100 0 MOS override
s 2560 -48 100 1536 2003/02/17
s 2464 -48 100 1536 WNR
s 2224 -48 100 1792 Changed SAD outputs
s 2000 -48 100 1536 E
s 2560 -16 100 1536 2002/09/10
s 2464 -16 100 1536 WNR
s 2224 -16 100 1792 Correct CAD input labels
s 2000 -16 100 1536 D
s 2560 48 100 1536 2001/02/03
s 2560 80 100 1536 2000/12/05
s 2464 48 100 1536 WNR
s 2464 80 100 1536 WNR
s 2128 48 100 1536 Rework CAD inputs
s 2144 80 100 1536 Initial Layout
s 2000 48 100 1536 B
s 2000 80 100 1536 A
s -144 624 100 0 Grism override
s -208 560 100 0 det. position override
s 576 1232 100 0 MOS slit name
s 576 352 100 0 filter 1 name
s 576 976 100 0 focus mode
s 576 1040 100 0 Grism name
s 576 1104 100 0 Lyot name
s 576 1424 100 0 beam mode
s 1952 720 100 0 focus pos
s 1952 784 100 0 Grism pos
s 1952 848 100 0 Lyot pos
s 1952 592 100 0 filter 2 pos
s 1952 656 100 0 filter 1 pos
s 1952 976 100 0 Decker pos
s 2512 -240 100 1792 flamInsSetup.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2003/09/06
s 2320 -240 100 1792 Rev: F
s 2432 -192 100 256 Flamingos instrumentSetup Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s -128 1408 100 0 beam
s -128 1264 100 0 Decker
s -128 1200 100 0 MOS slit
s -128 1136 100 0 filter
s -128 1072 100 0 Lyot
s -128 1008 100 0 Grism
s -128 944 100 0 detector position
s -144 752 100 0 Decker override
s -144 688 100 0 Lyot override
s 2560 16 100 1536 2002/07/14
s 2464 16 100 1536 WNR
s 2224 16 100 1792 Mark obsSetup on preset
s 2000 16 100 1536 C
s 1952 912 100 0 MOS slit pos
s 2560 -80 100 1536 2003/09/06
s 2464 -80 100 1536 WNR
s 2224 -80 100 1792 Added wavelength SAD update
s 2000 -80 100 1536 F
s 576 1296 100 0 Decker name
s 1920 1968 100 0 beam mode
s 1920 1840 100 0 Decker name
s 1920 1776 100 0 MOS slit name
s 576 288 100 0 filter 2 name
s 576 1168 100 0 filter name
s 1920 1648 100 0 Lyot name
s 1920 1584 100 0 Grism name
s 1920 1520 100 0 focus mode
s 1920 1712 100 0 filter name
s -144 880 100 0 Window override
[cell use]
use hwout 2080 1399 100 0 hwout#423
xform 0 2176 1440
p 2304 1440 100 0 -1 val(outp):$(sad)MOSBarCdMode.VAL PP NMS
use hwout 2080 1847 100 0 hwout#420
xform 0 2176 1888
p 2304 1888 100 0 -1 val(outp):$(sad)WindowMode.VAL PP NMS
use hwout 2080 471 100 0 hwout#417
xform 0 2176 512
p 2304 512 100 0 -1 val(outp):$(top)cc:MOSBarCd:namedPos.A
use hwout 2080 983 100 0 hwout#414
xform 0 2176 1024
p 2304 1024 100 0 -1 val(outp):$(top)cc:Window:namedPos.A
use hwout 2080 1911 100 0 hwout#399
xform 0 2176 1952
p 2304 1952 100 0 -1 val(outp):$(sad)beamMode.VAL PP NMS
use hwout 2080 1463 100 0 hwout#352
xform 0 2176 1504
p 2304 1504 100 0 -1 val(outp):$(sad)FocusMode.VAL PP NMS
use hwout 2080 1527 100 0 hwout#351
xform 0 2176 1568
p 2304 1568 100 0 -1 val(outp):$(sad)GrismName.VAL PP NMS
use hwout 2080 1591 100 0 hwout#350
xform 0 2176 1632
p 2304 1632 100 0 -1 val(outp):$(sad)LyotName.VAL PP NMS
use hwout 2080 1719 100 0 hwout#349
xform 0 2176 1760
p 2304 1760 100 0 -1 val(outp):$(sad)MOSName.VAL PP NMS
use hwout 2080 1783 100 0 hwout#348
xform 0 2176 1824
p 2304 1824 100 0 -1 val(outp):$(sad)DeckerName.VAL PP NMS
use hwout 2080 1655 100 0 hwout#347
xform 0 2176 1696
p 2304 1696 100 0 -1 val(outp):$(sad)filter1Name.VAL PP NMS
use hwout 2080 663 100 0 hwout#137
xform 0 2176 704
p 2304 704 100 0 -1 val(outp):$(top)cc:Focus:namedPos.A
use hwout 2080 727 100 0 hwout#136
xform 0 2176 768
p 2304 768 100 0 -1 val(outp):$(top)cc:Grism:namedPos.A
use hwout 2080 791 100 0 hwout#135
xform 0 2176 832
p 2304 832 100 0 -1 val(outp):$(top)cc:Lyot:namedPos.A
use hwout 2080 535 100 0 hwout#134
xform 0 2176 576
p 2304 576 100 0 -1 val(outp):$(top)cc:Filter2:namedPos.A
use hwout 2080 599 100 0 hwout#133
xform 0 2176 640
p 2304 640 100 0 -1 val(outp):$(top)cc:Filter1:namedPos.A
use hwout 2080 919 100 0 hwout#131
xform 0 2176 960
p 2304 960 100 0 -1 val(outp):$(top)cc:Decker:namedPos.A
use hwout 2080 855 100 0 hwout#403
xform 0 2176 896
p 2304 896 100 0 -1 val(outp):$(top)cc:MOS:namedPos.A
use hwin -144 119 100 0 hwin#405
xform 0 -48 160
p -96 160 100 512 -1 val(in):$(top)state
use changeBar 1984 -89 100 0 changeBar#400
xform 0 2336 -48
use changeBar 1984 -57 100 0 changeBar#396
xform 0 2336 -16
use changeBar 1984 7 100 0 changeBar#383
xform 0 2336 48
use changeBar 1984 39 100 0 changeBar#382
xform 0 2336 80
use changeBar 1984 -25 100 0 changeBar#395
xform 0 2336 16
use changeBar 1984 -121 100 0 changeBar#404
xform 0 2336 -80
use egenSub 1536 1191 100 0 egenSub#319
xform 0 1680 1616
p 1313 965 100 0 0 FTA:STRING
p 1313 965 100 0 0 FTB:STRING
p 1313 933 100 0 0 FTC:STRING
p 1313 901 100 0 0 FTD:STRING
p 1313 869 100 0 0 FTE:STRING
p 1313 805 100 0 0 FTF:STRING
p 1313 805 100 0 0 FTG:STRING
p 1313 773 100 0 0 FTH:STRING
p 1313 741 100 0 0 FTI:STRING
p 1313 709 100 0 0 FTJ:STRING
p 1313 965 100 0 0 FTVA:STRING
p 1313 965 100 0 0 FTVB:STRING
p 1313 933 100 0 0 FTVC:STRING
p 1313 901 100 0 0 FTVD:STRING
p 1313 869 100 0 0 FTVE:STRING
p 1313 805 100 0 0 FTVF:STRING
p 1313 805 100 0 0 FTVG:STRING
p 1313 773 100 0 0 FTVH:STRING
p 1313 741 100 0 0 FTVI:STRING
p 1313 709 100 0 0 FTVJ:STRING
p 1600 1152 100 0 1 INAM:flamIsNullGInit
p 1600 1120 100 0 1 SNAM:flamIsCopyGProcess
p 1744 1184 100 1024 1 name:$(top)insSetupCopyG
p 1824 1962 75 0 -1 pproc(OUTA):PP
p 1824 1898 75 0 -1 pproc(OUTB):PP
p 1824 1834 75 0 -1 pproc(OUTC):PP
p 1824 1770 75 0 -1 pproc(OUTD):PP
p 1824 1706 75 0 -1 pproc(OUTE):PP
p 1824 1642 75 0 -1 pproc(OUTF):PP
p 1824 1578 75 0 -1 pproc(OUTG):PP
p 1824 1514 75 0 -1 pproc(OUTH):PP
p 1824 1450 75 0 -1 pproc(OUTI):PP
p 1824 1386 75 0 -1 pproc(OUTJ):PP
use egenSub 1552 263 100 0 egenSub#289
xform 0 1696 688
p 1329 37 100 0 0 FTA:STRING
p 1329 37 100 0 0 FTB:STRING
p 1329 5 100 0 0 FTC:STRING
p 1329 -27 100 0 0 FTD:STRING
p 1329 -59 100 0 0 FTE:STRING
p 1329 -123 100 0 0 FTF:STRING
p 1329 -123 100 0 0 FTG:STRING
p 1329 -155 100 0 0 FTH:STRING
p 1329 -187 100 0 0 FTI:STRING
p 1329 -219 100 0 0 FTJ:STRING
p 1329 37 100 0 0 FTVA:STRING
p 1329 37 100 0 0 FTVB:STRING
p 1329 5 100 0 0 FTVC:STRING
p 1329 -27 100 0 0 FTVD:STRING
p 1329 -59 100 0 0 FTVE:STRING
p 1329 -123 100 0 0 FTVF:STRING
p 1329 -123 100 0 0 FTVG:STRING
p 1329 -155 100 0 0 FTVH:STRING
p 1329 -187 100 0 0 FTVI:STRING
p 1329 -219 100 0 0 FTVJ:STRING
p 1616 224 100 0 1 INAM:flamIsNullGInit
p 1616 192 100 0 1 SNAM:flamIsTranslateGProcess
p 1792 256 100 1024 1 name:$(top)insSetupTranslateG
p 1840 970 75 0 -1 pproc(OUTB):PP
use efanouts 672 -217 100 0 efanouts#96
xform 0 792 -64
p 816 -224 100 1024 1 name:$(top)insSetupFanout
use ecad20 128 -121 100 0 ecad20#93
xform 0 288 768
p 192 -160 100 0 1 INAM:flamIsNullInit
p 192 -192 100 0 1 SNAM:flamIsInsSetupProcess
p 352 -128 100 1024 1 name:$(top)instrumentSetup
p 448 10 75 0 -1 pproc(PLNK):PP
p 448 -22 75 0 -1 pproc(STLK):PP
use outhier 464 1984 100 0 VAL
xform 0 432 1984
use outhier 464 1856 100 0 MESS
xform 0 432 1856
use inhier 96 1984 100 0 DIR
xform 0 160 1984
use inhier 80 1856 100 0 ICID
xform 0 160 1856
use flamSubSysCommand 1120 -217 100 0 flamSubSysCommand#56
xform 0 1264 -32
p 1120 -256 100 0 1 setCommand:cmd insSetup
p 1120 -224 100 0 1 setSystem:sys cc
use ecars 1600 -217 100 0 ecars#53
xform 0 1760 -48
p 1760 -224 100 1024 1 name:$(top)instrumentSetupC
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamInsSetup.sch,v 1.1 2006/09/14 22:27:50 gemvx Exp $
