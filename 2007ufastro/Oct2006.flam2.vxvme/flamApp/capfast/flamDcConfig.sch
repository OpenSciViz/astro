[schematic2]
uniq 1461
[tools]
[detail]
w 884 2139 100 0 n#1460 ecad20.$(top)$(dev)obsControl.VALE 272 2128 1568 2128 estringouts.estringouts#1454.DOL
w 1636 2187 100 0 n#1458 estringouts.estringouts#1454.FLNK 1824 2112 1888 2112 1888 2176 1456 2176 1456 2240 1568 2240 estringouts.estringouts#1456.SLNK
w 772 2203 100 0 n#1457 ecad20.$(top)$(dev)obsControl.VALD 272 2192 1344 2192 1344 2272 1568 2272 estringouts.estringouts#1456.DOL
w 2236 1931 100 0 n#1451 estringouts.estringouts#1453.OUT 1808 2368 2112 2368 2112 1920 2432 1920 egenSub.$(top)$(dev)obsControlG.C
w 1972 2539 100 0 n#1450 estringouts.estringouts#1452.OUT 1840 2528 2176 2528 2176 1984 2432 1984 egenSub.$(top)$(dev)obsControlG.B
w 1628 2315 100 0 n#1449 estringouts.estringouts#1456.FLNK 1824 2256 1872 2256 1872 2304 1456 2304 1456 2384 1552 2384 estringouts.estringouts#1453.SLNK
w 1628 2475 100 0 n#1448 estringouts.estringouts#1453.FLNK 1808 2400 1872 2400 1872 2464 1456 2464 1456 2544 1584 2544 estringouts.estringouts#1452.SLNK
w 740 2267 100 0 n#1441 estringouts.estringouts#1453.DOL 1552 2416 1280 2416 1280 2256 272 2256 ecad20.$(top)$(dev)obsControl.VALC
w 1380 955 100 0 n#1440 ecad20.$(top)$(dev)obsControl.STLK 272 944 2560 944 efanouts.efanouts#691.SLNK
w 3996 587 100 0 n#1370 ecars.$(top)$(dev)obsControlC.FLNK 4224 672 4256 672 4256 576 3808 576 junction
w 4140 363 100 0 n#1370 ecars.$(top)$(dev)acqControlC.FLNK 4224 1056 4256 1056 4256 960 3808 960 3808 352 4544 352 egenSubD.egenSubD#1210.SLNK
w 4380 995 100 0 n#1178 ecars.$(top)$(dev)obsControlC.VAL 4224 896 4288 896 4288 992 4544 992 egenSubD.egenSubD#1210.INPC
w 4396 963 100 0 n#1177 ecars.$(top)$(dev)obsControlC.OMSS 4224 832 4320 832 4320 960 4544 960 egenSubD.egenSubD#1210.INPD
w 4380 1115 100 0 n#1176 ecars.$(top)$(dev)acqControlC.OMSS 4224 1216 4384 1216 4384 1024 4544 1024 egenSubD.egenSubD#1210.INPB
w 4412 1163 100 0 n#1175 ecars.$(top)$(dev)acqControlC.VAL 4224 1280 4416 1280 4416 1056 4544 1056 egenSubD.egenSubD#1210.INPA
w 2156 1163 100 0 n#1442 efanouts.efanouts#691.LNK1 2800 1024 2880 1024 2880 1152 1504 1152 1504 2096 1568 2096 estringouts.estringouts#1454.SLNK
w 2946 1211 100 0 n#837 flamLinkEnable.flamLinkEnable#664.FLINK 3472 1024 3616 1024 3616 1200 2336 1200 2336 1344 2432 1344 egenSub.$(top)$(dev)obsControlG.SLNK
w 3362 2059 100 0 n#836 elongouts.elongouts#703.FLNK 2592 2624 3104 2624 3104 2048 3680 2048 3680 1152 3104 1152 3104 1056 3152 1056 flamLinkEnable.flamLinkEnable#664.ENABLE
w 2434 2139 100 0 n#816 elongouts.elongouts#703.OUT 2592 2560 2720 2560 2720 2128 2208 2128 2208 2048 2432 2048 egenSub.$(top)$(dev)obsControlG.A
w 1634 2635 100 0 n#712 ecad20.$(top)$(dev)obsControl.VALA 272 2384 992 2384 992 2624 2336 2624 elongouts.elongouts#703.DOL
w 698 2331 100 0 n#711 ecad20.$(top)$(dev)obsControl.VALB 272 2320 1184 2320 1184 2576 1584 2576 estringouts.estringouts#1452.DOL
w 2226 1867 100 0 n#709 estringouts.estringouts#1456.OUT 1824 2224 2080 2224 2080 1856 2432 1856 egenSub.$(top)$(dev)obsControlG.D
w 2210 1803 100 0 n#708 estringouts.estringouts#1454.OUT 1824 2080 2048 2080 2048 1792 2432 1792 egenSub.$(top)$(dev)obsControlG.E
w 2946 971 100 0 n#676 efanouts.efanouts#691.LNK3 2800 960 3152 960 3152 992 flamLinkEnable.flamLinkEnable#664.SLINK
w 2090 2603 100 0 n#671 estringouts.estringouts#1452.FLNK 1840 2560 1904 2560 1904 2592 2336 2592 elongouts.elongouts#703.SLNK
w 4860 835 100 0 n#1007 egenSubD.egenSubD#1210.OUTD 4832 832 4960 832 4960 704 4976 704 hwout.hwout#308.outp
w 4924 699 100 0 n#1008 egenSubD.egenSubD#1210.OUTE 4832 768 4928 768 4928 640 4976 640 hwout.hwout#309.outp
w 4948 1059 100 0 n#1006 egenSubD.egenSubD#1210.OUTB 4832 960 4928 960 4928 1056 5040 1056 ecars.$(top)$(dev)applyC.IVAL
w 4860 1027 100 0 n#1005 egenSubD.egenSubD#1210.OUTA 4832 1024 4960 1024 4960 992 5040 992 ecars.$(top)$(dev)applyC.IMSS
s 2208 1872 100 0 Readout mode
s 2208 1936 100 0 number of readout
s 2208 1808 100 0 Obs. mode bias
s 2224 2000 100 0 exposure time
s 2272 2058 100 0 command mode
s -384 2192 120 0 Readout mode
s -384 2256 120 0 number of readout
s -384 2384 120 0 command mode
s 5029 152 225 0 flam2DcConfig.sch 1/31/2002 12:13 PM EST
s -384 2128 120 0 Bias Mode
s 2783 2057 100 0 sim mode
s 2783 2025 100 0 socket number
s 2783 1961 100 0 number of ND readout
s 2783 1993 100 0 exposure time
s -384 2320 120 0 exposure time
s 2800 1920 100 0 Readout mode
s 2800 1872 100 0 Obs. mode bias
[cell use]
use estringouts 1568 2167 100 0 estringouts#1456
xform 0 1696 2240
p 1504 2046 100 0 0 OMSL:closed_loop
p 1680 2160 100 1024 -1 name:$(dc)mCreadoutmodeW
use estringouts 1568 2023 100 0 estringouts#1454
xform 0 1696 2096
p 1504 1902 100 0 0 OMSL:closed_loop
p 1680 2016 100 1024 -1 name:$(dc)mCbiasmodeW
use estringouts 1552 2311 100 0 estringouts#1453
xform 0 1680 2384
p 1488 2190 100 0 0 OMSL:closed_loop
p 1664 2304 100 1024 -1 name:$(dc)nCreadoutW
use estringouts 1584 2471 100 0 estringouts#1452
xform 0 1712 2544
p 1520 2350 100 0 0 OMSL:closed_loop
p 1696 2464 100 1024 -1 name:$(dc)mCexpTimeW
use elongouts 2336 2503 100 0 elongouts#703
xform 0 2464 2592
p 2368 2464 100 0 1 OMSL:closed_loop
p 2368 2496 100 768 -1 name:$(dc)mCmodeW
use egenSub 2432 1255 -100 0 $(top)$(dev)obsControlG
xform 0 2576 1680
p 2275 1995 100 0 0 DESC:obs control gensub
p 2209 1029 100 0 0 FTA:LONG
p 2209 1029 100 0 0 FTB:STRING
p 2209 997 100 0 0 FTC:STRING
p 2209 965 100 0 0 FTD:STRING
p 2209 933 100 0 0 FTE:STRING
p 2209 869 100 0 0 FTF:STRING
p 2209 869 100 0 0 FTG:STRING
p 2209 837 100 0 0 FTH:STRING
p 2209 805 100 0 0 FTI:STRING
p 2209 773 100 0 0 FTJ:STRING
p 2209 1029 100 0 0 FTVA:LONG
p 2209 1029 100 0 0 FTVB:LONG
p 2209 997 100 0 0 FTVC:STRING
p 2209 965 100 0 0 FTVD:STRING
p 2209 933 100 0 0 FTVE:STRING
p 2209 869 100 0 0 FTVF:STRING
p 2209 869 100 0 0 FTVG:STRING
p 2209 837 100 0 0 FTVH:STRING
p 2209 805 100 0 0 FTVI:STRING
p 2209 773 100 0 0 FTVJ:STRING
p 2144 1662 100 0 0 INAM:ufobsControlGinit
p 2144 1950 100 0 0 SCAN:5 second
p 2144 1630 100 0 0 SNAM:ufobsControlGproc
p 2544 1248 100 1024 -1 name:$(dc)obsControlG
p 2720 2026 75 0 -1 pproc(OUTA):PP
use hwout 4976 663 100 0 hwout#308
xform 0 5072 704
p 5178 696 100 0 -1 val(outp):$(sad)dcState.VAL PP NMS
use hwout 4976 599 100 0 hwout#309
xform 0 5072 640
p 5178 632 100 0 -1 val(outp):$(sad)dcHealth.VAL PP NMS
use egenSubD 4544 263 100 0 egenSubD#1210
xform 0 4688 688
p 4321 37 100 0 0 FTA:LONG
p 4321 37 100 0 0 FTB:STRING
p 4321 5 100 0 0 FTC:LONG
p 4321 -27 100 0 0 FTD:STRING
p 4321 -59 100 0 0 FTE:LONG
p 4321 -123 100 0 0 FTF:STRING
p 4321 -123 100 0 0 FTG:LONG
p 4321 -155 100 0 0 FTH:STRING
p 4321 37 100 0 0 FTVA:STRING
p 4321 37 100 0 0 FTVB:LONG
p 4321 5 100 0 0 FTVC:LONG
p 4321 -27 100 0 0 FTVD:STRING
p 4321 -59 100 0 0 FTVE:STRING
p 4256 670 100 0 0 INAM:ufflamNullGInit
p 4256 638 100 0 0 SNAM:ufflamCmdCombineGProcess
p 4656 256 100 1024 -1 name:$(dc)MrgCarG
p 4496 778 75 0 -1 pproc(INPJ):NPP
p 4832 970 75 0 -1 pproc(OUTB):PP
p 4832 906 75 0 -1 pproc(OUTC):NPP
p 4832 842 75 0 -1 pproc(OUTD):PP
p 4832 778 75 0 -1 pproc(OUTE):PP
use efanouts 2560 807 100 0 efanouts#691
xform 0 2680 960
p 2672 800 100 1024 -1 name:$(dc)mFanout
use flamLinkEnable 3152 903 100 0 flamLinkEnable#664
xform 0 3312 1024
p 3152 896 100 0 1 setSystem:system $(dc)m
use ecars 3904 999 -100 0 $(top)$(dev)acqControlC
xform 0 4064 1168
p 4000 1216 100 0 0 DESC:acq Control CAR
p 4016 992 100 1024 -1 name:$(dc)acqControlC
use ecars 3904 615 -100 0 $(top)$(dev)obsControlC
xform 0 4064 784
p 4000 832 100 0 0 DESC:Observation Control CAR
p 4016 608 100 1024 -1 name:$(dc)obsControlC
use ecars 5040 771 -120 0 $(top)$(dev)applyC
xform 0 5200 944
p 5136 992 100 0 0 DESC:Detector Controller CAR
p 5152 768 100 1024 -1 name:$(dc)applyC
use ecad20 -48 855 -100 0 $(top)$(dev)obsControl
xform 0 112 1744
p 48 2480 100 0 0 DESC:Observation Control CAD
p 48 2320 100 0 0 FTVA:LONG
p 48 2288 100 0 0 FTVB:STRING
p 48 2256 100 0 0 FTVC:STRING
p 48 2224 100 0 0 FTVD:STRING
p 48 2192 100 0 0 FTVE:STRING
p 48 2128 100 0 0 FTVG:LONG
p 48 2096 100 0 0 FTVH:LONG
p 48 2064 100 0 0 FTVI:DOUBLE
p 48 2032 100 0 0 FTVJ:DOUBLE
p 48 1456 100 0 0 SNAM:dcObsCommand
p 64 848 100 1024 -1 name:$(dc)obsControl
p -80 1136 75 1280 -1 pproc(INPT):PP
p 272 954 75 0 -1 pproc(STLK):PP
[comments]
RCS: "$Name:  $ $Id: flamDcConfig.sch,v 1.1 2006/09/14 22:27:45 gemvx Exp $"
