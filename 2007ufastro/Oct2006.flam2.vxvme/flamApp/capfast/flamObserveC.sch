[schematic2]
uniq 259
[tools]
[detail]
w 1298 715 100 0 n#257 hwin.hwin#256.in 1248 704 1408 704 elongouts.elongouts#240.DOL
w 1122 651 100 0 n#255 elongouts.elongouts#240.SDIS 1408 640 896 640 egenSubE.egenSubE#78.VALS
w 1026 1195 100 0 n#252 egenSubE.egenSubE#78.VALB 896 1184 1216 1184 1216 960 1408 960 estringouts.estringouts#124.DOL
w 354 1035 100 0 n#247 hwin.hwin#225.in 288 1024 480 1024 480 1088 608 1088 egenSubE.egenSubE#78.INPE
w 338 1099 100 0 n#246 hwin.hwin#215.in 288 1088 448 1088 448 1120 608 1120 egenSubE.egenSubE#78.INPD
w 354 1291 100 0 n#245 hwin.hwin#81.in 288 1280 480 1280 480 1216 608 1216 egenSubE.egenSubE#78.INPA
w 338 1227 100 0 n#244 hwin.hwin#82.in 288 1216 448 1216 448 1184 608 1184 egenSubE.egenSubE#78.INPB
w 418 1163 100 0 n#243 hwin.hwin#216.in 288 1152 608 1152 egenSubE.egenSubE#78.INPC
w 1458 779 100 0 n#242 elongouts.elongouts#240.FLNK 1664 704 1696 704 1696 768 1280 768 1280 928 1408 928 estringouts.estringouts#124.SLNK
w 1058 491 100 0 n#241 egenSubE.egenSubE#78.FLNK 896 480 1280 480 1280 672 1408 672 elongouts.elongouts#240.SLNK
w 1730 651 100 0 n#251 elongouts.elongouts#240.OUT 1664 640 1856 640 hwout.hwout#250.outp
w 1122 1227 100 0 n#211 egenSubE.egenSubE#78.VALA 896 1216 1408 1216 elongouts.elongouts#123.DOL
w 1682 923 100 0 n#145 estringouts.estringouts#124.OUT 1664 912 1760 912 1760 1088 1856 1088 ecars.ecars#34.IMSS
w 1730 1163 100 0 n#144 elongouts.elongouts#123.OUT 1664 1152 1856 1152 ecars.ecars#34.IVAL
w 1458 1035 100 0 n#129 estringouts.estringouts#124.FLNK 1664 944 1696 944 1696 1024 1280 1024 1280 1184 1408 1184 elongouts.elongouts#123.SLNK
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos ObserveC Generator
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2003/05/28
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamObserveC.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2002/11/11
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Corrected nod complete link
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2003/05/28
[cell use]
use changeBar 1984 2023 100 0 changeBar#235
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#258
xform 0 2336 2032
use hwin 96 983 100 0 hwin#225
xform 0 192 1024
p 64 1024 100 512 -1 val(in):$(top)rdout.VAL
use hwin 96 1239 100 0 hwin#81
xform 0 192 1280
p 64 1280 100 512 -1 val(in):$(top)dc:observeC.VAL
use hwin 96 1175 100 0 hwin#82
xform 0 192 1216
p 64 1216 100 512 -1 val(in):$(top)dc:observeC.OMSS
use hwin 96 1047 100 0 hwin#215
xform 0 192 1088
p 64 1088 100 512 -1 val(in):$(top)prep.VAL
use hwin 96 1111 100 0 hwin#216
xform 0 192 1152
p 64 1152 100 512 -1 val(in):$(top)acq.VAL
use hwin 1056 663 100 0 hwin#256
xform 0 1152 704
p 1024 704 100 512 -1 val(in):0
use hwout 1856 599 100 0 hwout#250
xform 0 1952 640
p 2080 640 100 0 -1 val(outp):$(top)dc:TCSISnodComplG.J
use elongouts 1408 1095 100 0 elongouts#123
xform 0 1536 1184
p 1472 1056 100 0 1 OMSL:closed_loop
p 1584 1088 100 1024 1 name:$(top)obsCState
p 1664 1152 75 768 -1 pproc(OUT):PP
use elongouts 1408 583 100 0 elongouts#240
xform 0 1536 672
p 1472 544 100 0 1 OMSL:closed_loop
p 1584 576 100 1024 1 name:$(top)nodClear
p 1664 640 75 768 -1 pproc(OUT):PP
use estringouts 1408 855 100 0 estringouts#124
xform 0 1536 928
p 1472 816 100 0 1 OMSL:closed_loop
p 1600 848 100 1024 1 name:$(top)obsCMessage
use egenSubE 608 423 100 0 egenSubE#78
xform 0 752 848
p 385 197 100 0 0 FTA:LONG
p 385 197 100 0 0 FTB:STRING
p 385 165 100 0 0 FTC:LONG
p 385 133 100 0 0 FTD:LONG
p 385 101 100 0 0 FTE:LONG
p 385 37 100 0 0 FTF:DOUBLE
p 385 37 100 0 0 FTG:DOUBLE
p 385 5 100 0 0 FTH:DOUBLE
p 385 -27 100 0 0 FTI:DOUBLE
p 385 -59 100 0 0 FTJ:DOUBLE
p 385 197 100 0 0 FTK:DOUBLE
p 385 197 100 0 0 FTL:DOUBLE
p 385 165 100 0 0 FTM:DOUBLE
p 385 133 100 0 0 FTN:DOUBLE
p 385 101 100 0 0 FTO:DOUBLE
p 385 37 100 0 0 FTP:DOUBLE
p 385 37 100 0 0 FTQ:DOUBLE
p 385 5 100 0 0 FTR:DOUBLE
p 385 -27 100 0 0 FTS:DOUBLE
p 385 -59 100 0 0 FTT:DOUBLE
p 385 197 100 0 0 FTVA:LONG
p 385 197 100 0 0 FTVB:STRING
p 385 165 100 0 0 FTVC:DOUBLE
p 385 -27 100 0 0 FTVI:DOUBLE
p 385 -27 100 0 0 FTVK:DOUBLE
p 385 -27 100 0 0 FTVQ:DOUBLE
p 385 -27 100 0 0 FTVS:LONG
p 576 384 100 0 1 INAM:flamIsNullGInit
p 576 320 100 0 1 SCAN:.1 second
p 576 352 100 0 1 SNAM:flamObsCombineGProcess
p 720 416 100 1024 1 name:$(top)obsCombineG
use ecars 1856 871 100 0 ecars#34
xform 0 2016 1040
p 2032 864 100 1024 1 name:$(top)observeC
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamObserveC.sch,v 1.1 2006/09/14 22:27:55 gemvx Exp $
