[schematic2]
uniq 78
[tools]
[detail]
s 2624 2064 100 1792 2000/11/12
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamSadCcConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Flamingos CC Configure
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s 2624 2032 100 1792 2001/01/25
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Removed EC records
s 2016 2032 100 1792 B
[cell use]
use esirs 1856 7 100 0 esirs#77
xform 0 2064 160
p 1920 -32 100 0 1 SCAN:Passive
p 2016 0 100 1024 -1 name:$(top)Filter1RawPos
use esirs 544 -441 100 0 esirs#76
xform 0 752 -288
p 608 -480 100 0 1 SCAN:Passive
p 704 -448 100 1024 -1 name:$(top)LyotRawPos
use esirs -192 -9 100 0 esirs#75
xform 0 16 144
p -128 -48 100 0 1 SCAN:Passive
p -32 -16 100 1024 -1 name:$(top)DeckerRawPos
use esirs -192 -425 100 0 esirs#74
xform 0 16 -272
p -128 -464 100 0 1 SCAN:Passive
p -32 -432 100 1024 -1 name:$(top)Filter2RawPos
use esirs 1120 23 100 0 esirs#73
xform 0 1328 176
p 1184 -16 100 0 1 SCAN:Passive
p 1280 16 100 1024 -1 name:$(top)GrismRawPos
use esirs 544 -9 100 0 esirs#72
xform 0 752 144
p 608 -48 100 0 1 SCAN:Passive
p 704 -16 100 1024 -1 name:$(top)MOSRawPos
use esirs 1120 -393 100 0 esirs#69
xform 0 1328 -240
p 1184 -432 100 0 1 SCAN:Passive
p 1280 -400 100 1024 -1 name:$(top)FocusRawPos
use esirs 1856 -425 100 0 esirs#68
xform 0 2064 -272
p 1920 -464 100 0 1 SCAN:Passive
p 2016 -144 100 1024 -1 name:$(top)WindowRawPos
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)DeckerSteps
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)Filter2Steps
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)GrismSteps
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)MOSSteps
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)Filter1Steps
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)LyotSteps
use esirs -192 423 100 0 esirs#34
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)FocusSteps
use esirs 1984 423 100 0 esirs#48
xform 0 2192 576
p 2048 384 100 0 1 SCAN:Passive
p 2192 704 100 1024 -1 name:$(top)Filter2Status
use esirs 1264 1671 100 0 esirs#49
xform 0 1472 1824
p 1328 1632 100 0 1 SCAN:Passive
p 1424 1664 100 1024 -1 name:$(top)DeckerStatus
use esirs 1264 1255 100 0 esirs#50
xform 0 1472 1408
p 1328 1216 100 0 1 SCAN:Passive
p 1424 1248 100 1024 -1 name:$(top)FocusStatus
use esirs 1264 839 100 0 esirs#51
xform 0 1472 992
p 1328 800 100 0 1 SCAN:Passive
p 1424 832 100 1024 -1 name:$(top)GrismStatus
use esirs 2000 1671 100 0 esirs#52
xform 0 2208 1824
p 2064 1632 100 0 1 SCAN:Passive
p 2160 1664 100 1024 -1 name:$(top)MOSStatus
use esirs 2000 1255 100 0 esirs#53
xform 0 2208 1408
p 2064 1216 100 0 1 SCAN:Passive
p 2160 1248 100 1024 -1 name:$(top)Filter1Status
use esirs 1984 823 100 0 esirs#54
xform 0 2192 976
p 2048 784 100 0 1 SCAN:Passive
p 1296 624 100 768 1 name:$(top)LyotStatus
use esirs 544 391 100 0 esirs#55
xform 0 752 544
p 608 352 100 0 1 SCAN:Passive
p 704 672 100 1024 -1 name:$(top)WindowSteps
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#47
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadCcConfig.sch,v 1.1 2006/09/14 22:27:57 gemvx Exp $
