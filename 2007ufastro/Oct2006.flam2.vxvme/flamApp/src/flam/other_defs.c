#if !defined(__OTHERDEFS_C__)
#define __OTHERDEFS_C__ "RCS: $Name:  $ $Id: other_defs.c,v 1.1 2006/09/14 22:28:15 gemvx Exp $"
static const char rcsIdufOTHERDEFSC[] = __OTHERDEFS_C__;

#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "other_defs.h"
#include "fcntl.h"
#include "ioLib.h"
#include "fioLib.h"
#include "in.h"
#include "ioctl.h"
#include "stdio.h"
#include "time.h"
#include "hostLib.h"
#include "math.h"
#include "inetLib.h"
#include "ctype.h"
#include "sockLib.h"
#include "u_Lib.h"


/*#include "netdb.h"*/

struct hostent *
v_gethostbyname (const char *hostname)
{
  static struct hostent retval;
  static char *host;
  /* u_gethostbyname (host); */
  retval.h_name = host;
  /* retval.h_aliases = ; */
  /* retval.h_addrtype = ; */
  retval.h_length = sizeof (&host);
  /* retval.h_addr_list = ; */
  /* retval.h_tt = ; */
  return &retval;
}

struct tm *
v_localtime_r (const time_t * time_tvalue, struct tm *tmvalue)
{
  static struct tm *retval;
  localtime_r (time_tvalue, tmvalue);
  retval = tmvalue;
  return retval;
}

struct tm *
v_gmtime_r (const time_t * time_tvalue, struct tm *tmvalue)
{
  static struct tm *retval;
  gmtime_r (time_tvalue, tmvalue);
  retval = tmvalue;
  return retval;
}

#endif
