#if !defined(__UFGEMCADCOMMEC_C__)
#define __UFGEMCADCOMMEC_C__ "RCS: $Name:  $ $Id: ufGemCadCommEC.c,v 1.1 2006/09/14 22:28:18 gemvx Exp $"
static const char rcsIdufGEMCADCOMMECC[] = __UFGEMCADCOMMEC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <registryFunction.h>
#include <epicsExport.h>

#include <cad.h>
#include <cadRecord.h>

#include "flam.h"
#include "ufGemComm.h"

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/**********************************************************/

/************   Environment Controller CAD's   ************/


/**********************************************************/

/**********************************************************/

/**********************************************************/

/***********   Temperature Set Command  **********/
long
ectempSetCadProc (cadRecord * pcr)
{

  double out_vala;
  long out_valb;
  char temp_str[40];
  char *endptr;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      out_vala = strtod (pcr->a, &endptr);
      if (*endptr != '\0')
	{
	  strncpy (pcr->mess, "Can't convert set point", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      /* out_valb = (long)strtod(pcr->b,&endptr) ; */

      out_valb = atol (pcr->b);
      /* if (*endptr != '\0'){ */
      if (out_valb < 0)
	{
	  strncpy (pcr->mess, "Can't convert heater range", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      if (UFcheck_double (out_vala, set_point_lo, set_point_hi) == -1)
	{
	  printf
	    ("################## I am rejecting the temp because it is %8.3f\n",
	     out_vala);
	  strncpy (pcr->mess, "invalid Temp", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      if (UFcheck_long_r (out_valb, heater_range_lo, heater_range_hi) == -1)
	{
	  strncpy (pcr->mess, "invalid heater range", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      *(double *) pcr->vala = out_vala;
      sprintf (temp_str, "%8.3f", out_vala);
      strcpy (pcr->valg, temp_str);

      *(long *) pcr->valb = out_valb;
      sprintf (temp_str, "%ld", out_valb);
      strcpy (pcr->valh, temp_str);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }



  return CAD_ACCEPT;
}

/**********************************************************/

/**********************************************************/

/**********************************************************/

/*********** INIT Command  ***********/
long
ecinitCadInit (cadRecord * pcr)
{
  /* 
   *(double *)pcr->vala = 0.0 ; *//* Start in Real Mode */

  return CAD_ACCEPT;
}

/*********************************************************/

long
ecinitCadProc (cadRecord * pcr)
{

  long out_valb;
  char *endptr;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      out_valb = (long) strtod (pcr->a, &endptr);
      if (*endptr != '\0')
	{
	  strncpy (pcr->mess, "Can't convert init com", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}


      if (UFcheck_long_r (out_valb, cmd_mode_lo, cmd_mode_hi) == -1)
	{
	  strncpy (pcr->mess, "invalid SIM Mode", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      *(long *) pcr->valb = out_valb;
      *(double *) pcr->vala = (double) out_valb;	/* send 
							   the 
							   SIM 
							   mode 
							   to 
							   the 
							   CALc 
							   record 
							 */
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
static long ecVacChInitCommand (cadRecord * pcr)
{

  long out_vala;
  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a)
	     ; */
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else
	{
	  *(long *) pcr->vala = -1;
	  /* strncpy (pcr->mess, "Bad init command",
	     MAX_STRING_SIZE); return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/

/**********************************************************/

/**********************************************************/
long
ecVacChPowerCommand (cadRecord * pcr)
{
  char out_vala[40];
  /* char *endptr ; */
  long temp_long;

  strcpy (out_vala, "");

  temp_long = 0;

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;

      /* 
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      if (strcmp (pcr->a, "") != 0)
	{
	  strcpy (out_vala, pcr->a);
	  if ((strcmp (out_vala, "ON") != 0) &&
	      (strcmp (out_vala, "OFF") != 0))
	    {
	      strncpy (pcr->mess, "invalid Command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  else
	    temp_long = 1;
	}

      if ((temp_long) && (pcr->dir == menuDirectiveSTART))
	{
	  /* if (temp_long) { */
	  strcpy (pcr->vala, out_vala);
	  strcpy (pcr->a, "");
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


#endif /* __UFGEMCADCOMMEC_C__ */

epicsRegisterFunction(ecVacChInitCommand);
