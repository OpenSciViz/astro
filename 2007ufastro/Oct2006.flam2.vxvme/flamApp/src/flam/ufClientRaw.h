#if !defined(__UFClientRaw_h__)
#define __UFClientRaw_h__ "$Name:  $ $Id: ufClientRaw.h,v 1.1 2006/09/14 22:28:17 gemvx Exp $"
#define __UFClientRaw_H__(arg) const char arg##ClientRaw_h__rcsId[] = __UFClientRaw_h__;

#include "sys/types.h"
#include "arpa/inet.h"

/* basic os functions */

extern void ufSleep (float time); /** posix nanosleep */

extern const char *ufHostName ();
extern const char *ufHostTime (char *tz);

extern int ufAvailable (int fd); /** sockets or fifos */

extern int ufFifoAvailable (const char *fifoname);/** named pipe/fifos */

/* basic socket functions */
extern int ufIsIPAddress (const char *host);
extern int ufSetSocket (int socFd, struct sockaddr_in *addr, int portNo);
extern int ufConnect (const char *host, int portNo);
extern int ufClose (int socFd);

/** raw byte array output */
extern int ufSend (int socFd, const unsigned char *sndbuf, int nb);

extern int ufSendShort (int socFd, short val);	/* htons */
extern int ufSendShorts (int socFd, short *vals, int cnt);	/* htons 
								 */
extern int ufSendInt (int socFd, int val);	/* htonl */
extern int ufSendInts (int socFd, int *vals, int cnt);	/* htonl 
							 */
extern int ufSendFloat (int socFd, float val);	/* htonl */
extern int ufSendFloats (int socFd, float *vals, int cnt);	/* htonl 
								 */
extern int ufSendCstr (int socFd, const char *s);

/** raw byte array input */
extern int ufRecv (int socFd, unsigned char *rbuf, int len);

extern unsigned short ufRecvShort (int socFd);	/* ntohs */
extern int ufRecvShorts (int socFd, short *vals, int cnt);	/* ntohs 
								 */
extern int ufRecvInt (int socFd);	/* ntohl */
extern int ufRecvInts (int socFd, int *vals, int cnt);	/* ntohl 
							 */
extern float ufRecvFloat (int socFd);	/* ntohl */
extern int ufRecvFloats (int socFd, float *vals, int cnt);	/* ntohl 
								 */

/** reset null terminated string */
extern int ufRecvCstr (int socFd, char *rbuf, int len);

/** allocate new null terminated string */
extern int ufRecvNewCstr (int socFd, char **rbuf);

/** send agent startup sim-mode request to inetd on host */
extern int ufInetStartSim(const char* host);

/** send agent startup request to inetd on host */
extern int ufInetStartup(const char* host);

/** send agent shutdown request to inetd on host */
extern int ufInetShutdown(const char* host);

#endif /* __UFClientRaw_h__ */
