#include "vxWorks.h"
#include "stdio.h"
#include "string.h"


/* Gemini Records required */
#include "alarm.h"
#include "cadRecord.h"
#include "carRecord.h"
#include "genSubRecord.h"

#include <registryFunction.h>
#include <epicsExport.h>


#include "cad.h"
#include "menuCarstates.h"
#include "slalib.h"
#include "astLib.h"
#include "gem_wcs.h"


/*****************************************************************************
 * gemProcConfWcs
 *
 * load a wcs config file
 *
 *****************************************************************************/
static long gemProcConfWcs ( struct cadRecord *pCad )
{
   long status;               /* return status */

   /* Initialise CAD status */
   status = CAD_ACCEPT;

   /* Switch according to the CAD directive in DIR field */
   switch (pCad->dir)
   {
      case menuDirectiveMARK:
         break;
      case menuDirectivePRESET:
         break;
      case menuDirectiveCLEAR:
         break;
      case menuDirectiveSTART:
         /* put the file name on the output link */
         strcpy (pCad->vala,pCad->a);
         semGive(semInitWcs);
         break;
      case menuDirectiveSTOP:
         strcpy( pCad->mess, "gemProcConfWcs> Cannot be stopped");
         status = CAD_REJECT;
         break;
      default:
         strcpy( pCad->mess, "gemProcConfWcs> Unrecognized CAD directive" );
         status = CAD_REJECT;
         break;
   }
   return status;
}

/*****************************************************************************
 * gem_updateAstCtx
 *
 * computes the WCS
 *
 *****************************************************************************/
long gem_updateAstCtx(struct genSubRecord *pgsub)
{
   /* printf ("gem_updateAstCtx->sevr is %d stat %d\n",pgsub->sevr,pgsub->stat); */

   if(pgsub->sevr != INVALID_ALARM) { 
      if (pgsub->noa >= AST_CTXA_SIZE) {
         if (gem_TCSconnection!=1) {
            printf (">> updateAstCtx  [TCS connected]\n");
            gem_TCSconnection=1;
            }

         strcpy(pgsub->valb,pgsub->b);                 
         strcpy(pgsub->valc,pgsub->c);                  
         *(double*)pgsub->vald = *(double*)pgsub->d;    

         strcpy (trackFrameCurrent,(char*)pgsub->b);
         trackWavelengthCurrent = *(double*)pgsub->d;
         strcpy (trackEquinoxCurrent,(char*)pgsub->c);
         astSetctx(pgsub->a);
         }
      else {
         printf ("missing array elt, only got %d vs. %d\n",pgsub->noa,AST_CTXA_SIZE);
         }
   }
   else { 
      strcpy(pgsub->valb,"INDEF");
      if (gem_TCSconnection!=0) {
         printf (">> ERROR updateAstCtx [TCS disconnected] got %d vs. AST_CTXA_SIZE=%d\n",pgsub->noa,AST_CTXA_SIZE);
         gem_TCSconnection=0;
         }
      } 

   return OK;
   }
epicsRegisterFunction(gemProcConfWcs); 
