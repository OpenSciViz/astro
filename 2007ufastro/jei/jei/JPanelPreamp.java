package ufjei ;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import jca.event.MonitorEvent;
import jca.dbr.DBR_DBString;
import jca.dbr.DBR_DBDouble;
import jca.*;
import jca.Ca;
import jca.event.*;
import javax.swing.SwingConstants;
import javax.swing.border.*;
import java.text.DecimalFormat;

class JPanelTitlePa extends JPanel {
    public JPanelTitlePa() {
	     
	RatioLayout gridbag = new RatioLayout();
	setLayout(gridbag);
	JLabel D_Label = new JLabel("Default",JLabel.CENTER);
	JLabel C_Label = new JLabel("Last Read",JLabel.CENTER);
	JLabel N_Label = new JLabel("Name",JLabel.CENTER);	   
	     
	this.add("0.01,0.01;0.20,0.98",D_Label);
	this.add("0.23,0.01;0.22,0.98",C_Label);
	this.add("0.45,0.01;0.50,0.98",N_Label);
    }
}

class JPanelOffsetV extends JPanel
{  
    public  String P_Name;
    public  int P_Register ;
    public  int P_LatchOrder;
    public  double P_OffsetVoltage;
    public  double P_DacScaleFactor ;
    public  int P_DacOffset;
    public  JTextField P_Default;
    private JTextField P_Current ;
    private JButton P_OffsetVoltageName ;

    public JPanelOffsetV( String name, String register, String latchorder,
		      String offsetVoltage, String dacscalefactor, String dacoffset)
    {
	try {
	    P_Name = name;
	    P_Register = Integer.parseInt(register);
	    P_LatchOrder= Integer.parseInt(latchorder);
	    P_OffsetVoltage = Double.valueOf(offsetVoltage.trim()).doubleValue() ;
	    P_DacScaleFactor = Double.valueOf(dacscalefactor.trim()).doubleValue();
	    P_DacOffset = Integer.parseInt(dacoffset);      
	    setLayout(new RatioLayout());
	    P_Default = new JTextField(offsetVoltage,JTextField.CENTER);
	    P_Default .setToolTipText(offsetVoltage);
	    P_Default .setEditable(false);
	    P_Default .setBackground(Color.white);
	    P_Current= new JTextField();
	    P_Current.setEditable(false);
	    P_Current.setBackground(Color.white);
	    P_OffsetVoltageName = new JButton(P_Name);
	    P_OffsetVoltageName.setDefaultCapable(false);
	    P_OffsetVoltageName.setToolTipText(P_Name);
	    this.add("0.01,0.01;0.2,0.98",P_Default );
	    this.add("0.23,0.01;0.2,0.98",P_Current);
	    this.add("0.45,0.01;0.5,0.98",P_OffsetVoltageName);
	}
	catch(Exception e)
	    { JOptionPane.showMessageDialog(null,e,"Error in Offset Def?",JOptionPane.ERROR_MESSAGE); }    
    }

    public String getVolts() { return P_Current.getText(); }

    public void showVolts( double Voltage ) 
    {
	String volts = Double.toString( Voltage );
	int ndigits = volts.indexOf(".") + 4;
	if( ndigits < 4 ) ndigits = 4;
	if( ndigits > volts.length() ) ndigits = volts.length();
	P_Current.setText( volts.substring(0,ndigits) );
    }

    public JButton P_get_Button() { return P_OffsetVoltageName; }

    public String P_get_Name() { return P_Name; }
  	   
} //end of Class JPanelOffsetV.

public class JPanelPreamp  extends JPanel /* implements MonitorListener */
{
    public static final String rcsID = "$Name:  $ $Id: JPanelPreamp.java,v 0.17 2003/06/13 22:28:21 varosi beta $";

    public static final int NvOffsets=16; //must be divisible by 2, for equal Left & Right panels.
    final private JPanelOffsetV jPanelOffsets[];
    private JPanel P_LeftPanel, P_MidPanel, P_RightPanel;
    private JButton readAllButton, resetButton;
    public  JPanel P_jPanelVoltageSet;
    private JButton jButton_Connect = new JButton("Connect");
    private JButton jButtonCLEAR = new JButton("CLEAR  EPICS");
    JTextField P_GlobVolts = new JTextField("9.0");
    JTextField P_AdjVolts = new JTextField("1.0");
    public jeiCmd jeiCommand = new jeiCmd();

    // read_only EPICS info labels ( added at bottom of frame ) 
    EPICSLabel CAR_PreAmp = new EPICSLabel(EPICS.prefix + "dc:PreAmpC.VAL","CAR =");
    EPICSLabel errMessCAR = new EPICSLabel(EPICS.prefix + "dc:PreAmpC.OMSS","Message: ",jButton_Connect);
    EPICSLabel CAD_PreAmp = new EPICSLabel(EPICS.prefix + "dc:paApply.VAL","CAD =");
    EPICSLabel errMessCAD = new EPICSLabel(EPICS.prefix + "dc:paApply.MESS","Message: ");

//	jButton_Connect.setDefaultCapable(false);

    public JPanelPreamp()
    {
	JButton readAllButton = new JButton("Read ALL voltages");
	readAllButton.setDefaultCapable(false);
	JButton resetButton = new JButton("Reset MCE defaults");
	resetButton.setDefaultCapable(false);
	JButton P_Global = new JButton("Set ALL");
	JButton P_Adjust = new JButton("Adjust ALL");
	P_Global.setDefaultCapable(false);
	P_Adjust.setDefaultCapable(false);
	final JLabel PreAmpName = new JLabel("Offset Name",JLabel.CENTER);
	final JTextField PreAmpValue = new JTextField(" ");
	final JTextField PreAmpIndex  = new JTextField("-1");
	JButton P_Apply = new JButton("APPLY");
	P_Apply.setDefaultCapable(false);
	JPanel P_jPanelVoltageSet = new JPanel();
	P_jPanelVoltageSet.setBorder(new EtchedBorder(0));
	P_jPanelVoltageSet.setLayout(new GridLayout(1,3));
	P_jPanelVoltageSet.add(PreAmpName);
	P_jPanelVoltageSet.add(PreAmpValue);
	P_jPanelVoltageSet.add(P_Apply);
	jPanelOffsets = new JPanelOffsetV[NvOffsets];

	setLayout(new RatioLayout());
	int jc = 0;
	String record = null ;

	//read file of parameters and create buttons for each preamp channel voltage offset:
	try {
	    FileReader fin = new FileReader(jei.data_path + "dc_preamp_param.txt");
	    BufferedReader bin = new BufferedReader(fin);
	    P_LeftPanel = new JPanel();
	    P_LeftPanel.setBorder(new EtchedBorder(0));
	    P_LeftPanel.setLayout(new GridLayout(9,1));
	    P_LeftPanel.add(new JPanelTitlePa());
	    record = bin.readLine();         //read until first offset voltage:	 
	    while( record.indexOf("Offset_") < 0 ) record = bin.readLine();

	    for( int i=0; i < NvOffsets/2; i++ ) {
		StringTokenizer tokens = new StringTokenizer(record);
		jPanelOffsets[jc] = new JPanelOffsetV(tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken());
		final int ix = jc;
		jPanelOffsets[jc].P_get_Button().addActionListener( new ActionListener() {
			public void actionPerformed( ActionEvent e ) {
			    PreAmpName.setText( jPanelOffsets[ix].P_Name ); 
			    PreAmpIndex.setText( Integer.toString( jPanelOffsets[ix].P_Register ) );
			    PreAmpValue.setText( jPanelOffsets[ix].getVolts() );
			}
		    } );
		P_LeftPanel.add( jPanelOffsets[jc++] );
		record = bin.readLine();
	    }

	    P_RightPanel = new JPanel();
	    P_RightPanel.setBorder(new EtchedBorder(0));
	    P_RightPanel.setLayout(new GridLayout(9,1));
	    P_RightPanel.add(new JPanelTitlePa());

	    for( int i=0; i < NvOffsets/2; i++ ) {
		StringTokenizer tokens = new StringTokenizer(record);
		jPanelOffsets[jc] = new JPanelOffsetV(tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken(),
						      tokens.nextToken(),tokens.nextToken());
		final int ix = jc;
		jPanelOffsets[jc].P_get_Button().addActionListener( new ActionListener() {
			public void actionPerformed( ActionEvent e ) {
			    PreAmpName.setText( jPanelOffsets[ix].P_Name ); 
			    PreAmpIndex.setText( Integer.toString( jPanelOffsets[ix].P_Register ) );
			    PreAmpValue.setText( jPanelOffsets[ix].getVolts() );
			}
		    } );
		P_RightPanel.add( jPanelOffsets[jc++] );
		record = bin.readLine();
	    }
	    bin.close();  //done reading file and creating buttons for each channel voltage offset.
	}
	catch(Exception e)
	    { JOptionPane.showMessageDialog(null,e,"Error in reading file?"+jc,JOptionPane.ERROR_MESSAGE); }    

	P_Apply.addActionListener ( new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    String put = "PUT " + EPICS.prefix;
		    jeiCommand.execute( put + "dc:paSet.A  " + Integer.parseInt(PreAmpIndex.getText()) );
		    jeiCommand.execute( put + "dc:paSet.B  " + PreAmpValue.getText() );
		    jeiCommand.execute( put + "dc:paApply.DIR START"); 
		}
	    } );
	    
	resetButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { resetMCE_action(); } } );
	    
	readAllButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { readAll_action(0); } } );

	P_Global.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { globalVolts_action(); } } );

	P_Adjust.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { adjustVolts_action(); } } );

	jButton_Connect.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { jButtonConnect_action(e); } } );

	jButtonCLEAR.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR CLEAR"); 
		}
	    } );

	this.add("0.05,0.035;0.15,0.07",jButton_Connect) ;
	this.add("0.25,0.035;0.15,0.07",jButtonCLEAR) ;
	this.add("0.05,0.14;0.4,0.55",P_LeftPanel);
	this.add("0.55,0.14;0.4,0.55",P_RightPanel);
	this.add("0.05,0.75;0.4,0.08",P_jPanelVoltageSet);
	this.add("0.55,0.75;0.15,0.08",readAllButton);
	this.add("0.55,0.85;0.15,0.08",resetButton);
	this.add("0.71,0.75;0.15,0.08",P_Global);
	this.add("0.71,0.85;0.15,0.08",P_Adjust);
	this.add("0.86,0.75;0.08,0.08",P_GlobVolts);
	this.add("0.86,0.85;0.08,0.08",P_AdjVolts);
	this.add("0.05,0.87;0.10,0.06",CAD_PreAmp);
	this.add("0.15,0.87;0.40,0.06",errMessCAD);
	this.add("0.05,0.93;0.10,0.06",CAR_PreAmp);
	this.add("0.15,0.93;0.40,0.06",errMessCAR);

	checkConnection(true);

    } //end of method JPanelPreamp().
	    
//-------------------------------------------------------------------------------

    void readAll_action( int sleepFirst )  
    {
	if( sleepFirst > 0 )
	    try { Thread.sleep(sleepFirst);} catch ( Exception _e) {}

	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paReadAll.A 1" );
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR START");

	try { Thread.sleep(2500);} catch ( Exception _e) {}
	PV pv = new PV(EPICS.prefix + "dc:PreAmpG.VALE"); 

	try { Ca.pendIO(10.0); } catch (jca.TimeOutException x) { System.out.println(x); } ;
	DBR_DBDouble pv_value = new  DBR_DBDouble( pv.elementCount() );

	try {
	    try {
		try {
		    pv.get( pv_value );
		} catch (jca.GetFailException gf) { System.out.println(gf); } ;
	    } catch (jca.BadCountException bc) { System.out.println(bc); } ;
	} catch (jca.BadTypeException c) { System.out.println(c); } ;

	try { Ca.pendIO(10.0); } catch (jca.TimeOutException x) { System.out.println(x); };

	for( int i = 0; i < NvOffsets; i++ )
	    jPanelOffsets[i].showVolts( pv_value.valueAt(i) );
    }
//-------------------------------------------------------------------------------

    void resetMCE_action()  
    { 
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paDatum.A " + 1);
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR START");
    }
//-------------------------------------------------------------------------------

    void globalVolts_action()  
    { 
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paGlobSet.A  " + P_GlobVolts.getText() );
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR START");
    }
//-------------------------------------------------------------------------------

    void adjustVolts_action()  
    { 
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paAdjust.A  " + P_AdjVolts.getText() );
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR START"); 
    }
//-------------------------------------------------------------------------------

    /**
     * JPanelPreamp#Connect button action performed
     * Sends commands to EPICS dc:PreAmp records
     *  directing EPICS gensubs to connect via sockets to the Detector Control Agent,
     *  allowing direct engineering access to detector control bias voltage parameters.
     *@param e not used
     */
    void jButtonConnect_action(ActionEvent e)
    {
	jButton_Connect.setBackground(Color.yellow);
	jButton_Connect.setForeground(Color.black);
	jButton_Connect.setText("Connecting");
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paInit.A 0" );
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:paApply.DIR START");
	
	try { Thread.sleep(1000);} catch ( Exception _e) {}
	checkConnection(true);
    }
//-------------------------------------------------------------------------------

    void checkConnection( boolean indicateOK )
    {
	String socPreamp = EPICS.get(EPICS.prefix + "dc:PreAmpG.VALB"); 

	if( socPreamp.equals("-1") || 
	    checkforBadSocket( errMessCAR.getText() ) )
	    { 
		jButton_Connect.setBackground(Color.red);
		jButton_Connect.setForeground(Color.white);
		jButton_Connect.setText("Connect");
	    }
	else if( indicateOK ) {
	    jButton_Connect.setBackground(Color.green);
	    jButton_Connect.setForeground(Color.black);
	    jButton_Connect.setText("Connected");
	}
    }
//-------------------------------------------------------------------------------

    boolean checkforBadSocket( String EPICSmessage )
    {
	if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	    EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	    EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
	    return true;
	else
	    return false;
    }
} //end of Class JPanelPreamp.



