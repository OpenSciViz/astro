
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Modified:     by Frank Varosi, 2001 (to make it work right with epics).
//Company:      University of Florida
//Description:

package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JComboBox;
import jca.*;
import jca.dbr.*;
import jca.event.*;

//===============================================================================
/**
 * Combo box that deals with EPICS Record Fields
 * Can talk to two separate record fields one for the input and another for the ouput
 */

public class EPICSComboBox extends JComboBox implements MonitorListener, ItemListener {
  public static final String rcsID = "$Name:  $ $Id: EPICSComboBox.java,v 0.6 2002/10/09 22:41:02 varosi beta $";
  public static final int INDEX = 0;
  public static final int ITEM = 1;

  int item_OR_index;
  String putRec;
  String monitorRec;
  String setVal = "";
  String putVal = "";
  String monVal = "";
  Monitor outMon = null;
  boolean dual; // true if putRec and monitorRec are two different recs
  boolean doPuts = true;

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public EPICSComboBox() {
    try  {
      jbInit("test", "test", null, ITEM);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating a default EPICSComboBox constructor: " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param rec String: Record field
   */
  public EPICSComboBox(String rec) {
    try  {
      jbInit(rec, rec, null, ITEM);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSComboBox " + rec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   */
  public EPICSComboBox(String putRec, String monitorRec) {
    try  {
      jbInit(putRec, monitorRec, null, ITEM);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor
   * Set whether you want to communicate with
   * Combobox items or combobox indexes to EPICS
   *@param rec String: record field
   *@param items Array of Strings: list of values to be passed to the record field
   *@param _index_OR_item int: INDEX or ITEM orientation
   */
  public EPICSComboBox(String rec, String [] items, int _index_OR_item) {
    try  {
      jbInit(rec, rec, items, _index_OR_item);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSComboBox " + rec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor for communicating with input AND output EPICS records
   * Set whether you want to communicate with Combobox items or combobox indexes to EPICS
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   *@param items Array of Strings: list of items in the combo box
   *@param _index_OR_item int: INDEX or ITEM orientation
   */
  public EPICSComboBox(String putRec, String monitorRec, String [] items, int _index_OR_item) {
    try  {
      jbInit(putRec, monitorRec, items, _index_OR_item);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   * Constructor for communicating with input AND output EPICS records
   * Set whether you want to communicate with Combobox items or combobox indexes to EPICS
   *@param putRec  String: Input Record Field to be monitored
   *@param monitorRec  String: Output Record Field to be monitored
   *@param items  Array of Strings: list of items in the combo box
   *@param _index_OR_item  int: INDEX or ITEM orientation
   *@param doPuts  boolean: false means do NOT automatically put to EPICS (default is true)
   */
  public EPICSComboBox(String putRec, String monitorRec, String [] items, int _index_OR_item, boolean doPuts)
    {
	try {
	    this.doPuts = doPuts;
	    jbInit(putRec, monitorRec, items, _index_OR_item);
	}
	catch(Exception ex) {
	    jeiError.show("Error creating EPICSComboBox " + putRec + "/" + monitorRec + ": " + ex.toString());
	}
    } //end of EPICSComboBox

//-------------------------------------------------------------------------------
  /**
   *Component initialization -- returns no arguments
   *@param putRec String: Input Record Field to be monitored
   *@param monitorRec String: Output Record Field to be monitored
   *@param items Array of Strings: list of items in the combo box
   *@param item_OR_index int: INDEX or ITEM orientation
   */
  private void jbInit(String putRec, String monitorRec, String[] items, int item_OR_index) throws Exception
    {
	dual = !putRec.equals(monitorRec);
	this.item_OR_index = item_OR_index;
	this.setMaximumRowCount(14);

	if (items != null) {
	    for( int i=0; i<items.length; i++ ) this.addItem( items[i] );
	}

	// setup monitor(s)
	this.putRec = putRec;
	this.monitorRec = monitorRec;
	int mask = Monitor.VALUE;

	try {
	    PV pv = new PV(monitorRec);
	    Ca.pendIO(jeiFrame.TIMEOUT);
	    outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
	    Ca.pendIO(jeiFrame.TIMEOUT);
	}
	catch (Exception ex) { this.setBackground(Color.red); }

	monVal = EPICS.get( monitorRec ).trim();
	setVal = monVal;
	setToolTipText();
       	addItemListener(this);
    } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(MonitorEvent event) {
    try {
      DBR_DBString dbstr = new DBR_DBString();
      event.pv().get(dbstr);
      Ca.pendIO(jeiFrame.TIMEOUT);

      if( event.pv().name().equals(this.monitorRec) )
	  {
	      monVal = dbstr.valueAt(0).trim();
	      this.setToolTipText();

	      if( setVal.equals( monVal ) )
		  this.setBackground(Color.white);
	      else if( ! setVal.trim().equals("") )
		  this.setBackground(Color.yellow);
	  }
    }
    catch (Exception ex) {
      jeiError.show("Error in monitor changed for EPICSComboBox: " + monitorRec + " " + ex.toString());
    }
  } //end of monitorChanged

//-------------------------------------------------------------------------------
    /**
     * Event Handling Method
     *@param ie ItemEvent: TBD
     */
    public void itemStateChanged(ItemEvent ie)
    {
      if( ie.getStateChange() == ItemEvent.SELECTED )
	  {
	      if( item_OR_index == ITEM )
		  setVal = this._getSelectedItem();
	      else
		  setVal = this._getSelectedIndex();

	      if( setVal.equals( monVal ) )
		  this.setBackground(Color.white);
	      else
		  this.setBackground(Color.yellow);

	      if (doPuts) putcmd( setVal );
	  }
    } //end of itemStateChanged

//-------------------------------------------------------------------------------
  /**
   * Returns the String name of the selected item in the combo box,
   * with comments after blank removed.
   */
  public String _getSelectedItem() {
    String item = (String)this.getSelectedItem();
    if (item == null) item = "";
    if( item.indexOf(" ") > 0 ) //anything after a blank is a comment so eliminate it:
	item = item.substring( 0, item.indexOf(" ") );
    return item.trim();
  } //end of _getSelectedItem

//-------------------------------------------------------------------------------
  /**
   * Returns the integer index of the selected item in the combo box, as a string.
   */
  public String _getSelectedIndex() {
    int item = this.getSelectedIndex();
    return String.valueOf( item );
  } //end of _getSelectedIndex

//-------------------------------------------------------------------------------
  /**
   * Sets the text to go in the tool tip -- returns no arguments
   */
  void setToolTipText() {
      this.setToolTipText( putRec + " = " + putVal + " -> " + monitorRec + " = " + monVal );
  }

//-------------------------------------------------------------------------------
  /**
   * Puts the currently selected item/index into the 
   * putRec (Input Record Field)
   *@param val String: value to be put
   */
    void forcePut() {
	putVal = "";
	if (item_OR_index == ITEM) 
	    putcmd( this._getSelectedItem() );
	else 
	    putcmd( this._getSelectedIndex() );
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val (value) into the putRec (Input Record Field)
   *@param val String: value to be put
   */
  void putcmd(String val) {
      if( ! val.trim().equals("") ) {
	  if( val.indexOf(" ") > 0 ) //anything after a blank is a comment so eliminate it:
	      val = val.substring( 0, val.indexOf(" ") );
	  if( ! val.trim().equals( putVal ) ) {
	      String cmd = new String("PUT " + putRec + "  \"" + val + "\"") ;
	      jeiCmd command = new jeiCmd();
	      command.execute(cmd);
	      putVal = val;
	  }
      }
  } //end of putcmd

//-------------------------------------------------------------------------------
  /**
   * Sets the combo box to requested value and then puts value to EPICS.
   *@param val String: value to be put
   */
  public void set_and_putcmd(String val) {
      putVal = "";
      this.putcmd(val);
      this.setSelectedItem(val);
  } //end of set_and_putcmd


//-------------------------------------------------------------------------------
  /**
   * TBD??
   *@param e WindowEvent: TBD
   */
    protected void processWindowEvent(WindowEvent e) {
	if(e.getID() == WindowEvent.WINDOW_CLOSING) {
	    try {
		outMon.clear();
	    }
	    catch (Exception ee) { jeiError.show(e.toString()); }
	}
    } //end of processWindowEvent

} //end of class EPICSComboBox
