
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Modified:     by Frank Varosi, 2001 (to make it work right with epics).
//Company:      University of Florida
//Description:

package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JTextField;
import javax.swing.*;
import jca.*;
import jca.dbr.*;
import jca.event.*;

//===============================================================================
/**
 * Handles text fields related to EPICS
 */
public class EPICSTextField extends JTextField implements MonitorListener,FocusListener, KeyListener {
    public static final String rcsID = "$Name:  $ $Id: EPICSTextField.java,v 0.2 2003/09/04 22:52:54 varosi beta $";
    String putRec;
    String monitorRec;
    String prev_text = "";
    String new_text = "";
    String desc;
    String outVal = "";
    Monitor outMon = null;
    Monitor inMon = null;
    boolean dual = true; // true if putRec and monitorRec are two different recs
    boolean doPuts = false;
    boolean userEvent = false;
    jeiCmd command = new jeiCmd();

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public EPICSTextField() {
    try  {
      jbInit("", "", "");
    }
    catch(Exception ex) {
      jeiError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param rec String: record field
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String rec, String description) {
    try  {
      jbInit(rec, rec, description);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param rec String: record field
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String rec, String description, boolean doPuts) {
    try  {
      this.doPuts = doPuts;
      jbInit(rec, rec, description);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String putRec, String monitorRec, String description, boolean doPuts) {
    try  {
      this.doPuts = doPuts;
      jbInit(putRec, monitorRec, description);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String putRec, String monitorRec, String description) {
    try  {
      jbInit(putRec, monitorRec, description);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  private void jbInit(String putRec, String monitorRec, String description) throws Exception {
    this.dual = !putRec.equals(monitorRec);
    this.putRec = putRec;
    this.monitorRec = monitorRec;
    this.desc = description;
    this.setToolTipText();

    if( dual || doPuts ) {
	this.addFocusListener(this);
	this.addKeyListener(this);
    }

    if( putRec.trim().equals("") || monitorRec.trim().equals("") ) return;

    // setup monitor(s)
    int mask = Monitor.VALUE;

    try {
      PV pv = new PV(monitorRec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
      Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception ex) { this.setBackground( Color.red ); }
  } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   * Sets the text for the tool tip -- returns no arguments
   */
  void setToolTipText() {
    if( dual )
	this.setToolTipText( putRec + " -> " + monitorRec + " = " + outVal );
    //this.setToolTipText( monitorRec + " = " + outVal
    //		     + "  : old=" + prev_text
    //		     + "  : new=" + new_text );
    else
	this.setToolTipText( monitorRec + " = " + outVal );
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method: overloads the virtual method in abstract MonitorListener class (jca)
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(MonitorEvent event) {
    try {
      DBR_DBString dbstr = new DBR_DBString();
      event.pv().get(dbstr);
      Ca.pendIO(jeiFrame.TIMEOUT);

      if( event.pv().name().equals(this.monitorRec) ) {
	  outVal = dbstr.valueAt(0);
	  setText( outVal.trim() );
	  if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
	  if( this.getText().indexOf("ERR") >= 0 ) {
	      Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	  }
      }

      setToolTipText();

      if( dual && doPuts && userEvent )
	  {
	      userEvent = false;
	      if( new_text.equals( getText() ) )
		  this.setBackground( Color.white );
	      else
		  this.setBackground( Color.yellow );
	  }
      else this.setBackground( Color.white );

    }
    catch (Exception ex) {
      jeiError.show("Error in monitor changed for EPICSTextField: " + monitorRec + " " + ex.toString());
    }
  } //end of monitorChanged

//-------------------------------------------------------------------------------

    public void focusGained(FocusEvent e) 
    { 
	if( prev_text.trim().equals("") )
	    prev_text = this.getText().trim();
    }

//-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	new_text = getText().trim();

	if( ! new_text.equals( prev_text ) ) {
	    setBackground( Color.yellow );
	    if( doPuts ) putcmd( new_text );
	}
	else setBackground( Color.white );
    }
//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
  public void keyTyped (KeyEvent ke) {
      userEvent = true;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
  public void keyReleased (KeyEvent ke) {
      userEvent = true;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
    public void keyPressed (KeyEvent ke)
    {
	userEvent = true;

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		new_text = getText().trim();

		if( ! new_text.equals( prev_text ) ) {
		    setBackground( Color.yellow );
		    if( doPuts ) putcmd( new_text );
		}
		else setBackground( Color.white );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prev_text );
	    }
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the current contents of the textfield to the EPICS record field
   *@param val String: Value to be put
   */
    void forcePut() {
      putcmd( this.getText() );
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val to the EPICS record field
   *@param val String: Value to be put
   */
  void putcmd(String val) {
    if( val.trim().equals("") || putRec.trim().equals("") ) return;
    String cmd = new String("PUT " + putRec + " " + "\""+val+ "\"" + " ;" + desc) ;
    command.execute(cmd);
    prev_text = val.trim();
  } //end of putcmd

//-------------------------------------------------------------------------------
  /**
   * Sets the text and puts the value in EPICS
   *@param val String: value to be set and put
   */
  public void set_and_putcmd(String val) {
    setText( val );
    putcmd( val );
  } //end of set_and_putcmd

//-------------------------------------------------------------------------------
  /**
   * Window Event Handling Method
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      try {
        if (inMon != null)
            inMon.clear();
        if (outMon != null)
            outMon.clear();
      }
      catch (Exception ee) {
        jeiError.show(e.toString());
      }
      ///dispose();
    }
    ///super.processWindowEvent(e);
  } //end of processWindowEvent

//-------------------------------------------------------------------------------
  /**
   *  This method allows the user to reset the
   *  EPICS record names and re-establish
   *  monitors to the new record names.
   *@param putRec String: Input record field for monitoring
   *@param monRec String: Output record field for monitoring
   *@param desc String: description of the record field
   */
  public void setEPICSRecords(String putRec, String monRec, String desc) {
    this.setText("");
    this.setBackground(Color.white);
    this.putRec = putRec;
    this.monitorRec = monRec;
    this.desc = desc;
    this.prev_text = this.getText();
    this.doPuts = true;

    try {
      if (this.inMon != null) this.inMon.clear();
      if (this.outMon != null) this.outMon.clear();
      // setup monitor
      PV pv = new PV(this.putRec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      int mask = Monitor.VALUE;
      this.inMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
      Ca.pendIO(jeiFrame.TIMEOUT);
      pv = new PV(this.monitorRec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      mask = Monitor.VALUE;
      this.outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
      Ca.pendIO(jeiFrame.TIMEOUT);
      this.setToolTipText();
    }
    catch (Exception ee) {
      this.setBackground(Color.red);
      inMon = null;
      outMon = null;
    }

  } // end of setEPICSRecords

//-------------------------------------------------------------------------------
  /**
   *  This method allows the user to reset the
   *  EPICS record names and re-establish
   *  monitors to the new record names.
   *@param rec String: Record field
   *@param desc String: Record description
   */
  public void setEPICSRecords(String rec, String desc) {
      this.setEPICSRecords(rec,rec,desc);
  } // end of setEPICSRecords

} //end of class EPICSTextField

