package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;


//===============================================================================
/**
 * TBD
 */
public class JPanelEPICSStatus extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelEPICSStatus.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  Vector sections;
  BorderLayout borderLayout1 = new BorderLayout();
  JList jListSections;
  JButton jButton1 = new JButton();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JScrollPane jScrollPane1 = new JScrollPane();

//-------------------------------------------------------------------------------
  /**
   *Construct the frame
   */
  public JPanelEPICSStatus() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of JPanelEPICSStatus


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    status_recs section;
    status_rec stat;
    load_status_recs_txt_file();
    Iterator it = sections.iterator();
    while (it.hasNext()) {
      section = (status_recs)it.next();
//      System.out.println (section.title_line);
    }
    jListSections = new JList(sections);
    jListSections.setSelectedIndex(0);
    jListSections.setVisibleRowCount(23);
    jButton1.setText("View status information for the selected section");
    jButton1.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButton1_actionPerformed();
      }
    });
    jListSections.addMouseListener(new MouseListener() {
      public void mouseClicked( MouseEvent me) {
        if (me.getClickCount() == 2) jButton1_actionPerformed();
      }
      public void mouseReleased(MouseEvent e) {}
      public void mousePressed(MouseEvent e) {}
      public void mouseExited(MouseEvent e) {}
      public void mouseEntered(MouseEvent e) {}
    });
    this.setLayout(new BorderLayout());
    jPanel1.setLayout(new GridLayout(1,1));
    jPanel2.setLayout(new FlowLayout());
    add(jPanel1, BorderLayout.CENTER);
    jPanel1.add(jScrollPane1, null);
    jScrollPane1.getViewport().add(jListSections, null);
    add(jPanel2, BorderLayout.SOUTH);
    jPanel2.add(jButton1, null);

  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *processes the status recs file
   */
  void load_status_recs_txt_file() {
    // to be implemented - error checking
    status_recs section;
    status_rec stat;
    try {
      sections = new Vector();
      String file_name = "status_recs.txt";
      String line;
      int i = 0;
      BufferedReader in_file ;
      in_file = new  BufferedReader (new FileReader(jei.data_path + file_name));
///TextFile out = new TextFile("sir.txt",TextFile.OUT);
      while (!(line = in_file.readLine()).equals("eod")) {
///out.println("");
///out.println("2 " + line.substring(5));
///out.println("");
        section = new status_recs();
        section.title_line = line;
        section.status_recs = new Vector();
        sections.add(section);
        line = in_file.readLine(); // should be a blank line
        while ((line = in_file.readLine()).trim().length() > 0) {   // read recs for this section
          stat = new status_rec();
          stat.name  = line;
          stat.type  = in_file.readLine();
          stat.units = in_file.readLine();
          stat.fits  = in_file.readLine();
          stat.added = in_file.readLine();
          stat.info  = in_file.readLine();
///out.println("SIR sad:" + stat.name + ", " + stat.type + ", " + stat.units + ", " + stat.fits+ ", " + stat.added + ", " + stat.info);
          section.status_recs.add(stat);
          line = in_file.readLine(); // should be a blank line
        }
      }
      in_file.close();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of load_status_recs_txt_file


//-------------------------------------------------------------------------------
  /**
   *JPanelEPICSStatus#jbutton1 action performed
   */
  void jButton1_actionPerformed() {
    status_recs section = (status_recs)jListSections.getSelectedValue();
//    System.out.println(section);  // should be a section title line
    statusFrame frame = new statusFrame(section);
    frame.validate();
    frame.setVisible(true);
  } //end of jButton1_actionPerformed

} //end of class JPanelEPICSStatus


