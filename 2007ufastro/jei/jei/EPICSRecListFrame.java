
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Company:      University of Florida
//Description:

package ufjei;

import java.awt.*;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;

//===============================================================================
/**
 * This Class handles the window that displays lists of records for monitoring
 */
public class EPICSRecListFrame extends JFrame {
  int n = 0;
  JPanel contentPane;
  BorderLayout borderLayout1 = new BorderLayout();
  JSplitPane jSplitPane1 = new JSplitPane();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  GridLayout gridLayout1 = new GridLayout();
  GridLayout gridLayout2 = new GridLayout();


//-------------------------------------------------------------------------------
  /**
   * Default constructor
   *@param win_name String: Window name(on the title bar)
   */
  public EPICSRecListFrame(String win_name) {
    try  {
      jbInit(win_name);
    }
    catch(Exception e) {
      jeiError.show("jbInit of EPICSRecListFame - " + e.toString());
    }
  } //end of EPICSRecListFrame


//-------------------------------------------------------------------------------
  /**
   *Component Initialization
   *@param win_name String: Name of the window
   */
  private void jbInit(String win_name) throws Exception {
    contentPane = (JPanel) this.getContentPane();
    contentPane.setLayout(borderLayout1);
    this.setSize(new Dimension(400, 105));
    this.setTitle(win_name);
    jPanel1.setLayout(gridLayout1);
    jPanel2.setLayout(gridLayout2);
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    gridLayout2.setColumns(1);
    gridLayout2.setRows(0);

    contentPane.add(jSplitPane1, BorderLayout.CENTER);
    jSplitPane1.add(jPanel1, JSplitPane.LEFT);
    jSplitPane1.add(jPanel2, JSplitPane.RIGHT);
    jSplitPane1.setDividerLocation(100);

    this.setLocation(jei.get_screen_loc(win_name));
    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        this_componentMoved(e);
      }
    });
    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        doWindowCleanup();
      }
    });
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *  This method properly destroys the window and removes the
   *  window and it's name from the vectors in the JPanelEPICSRecs
   *  class. -- returns no arguments
   */
  public void doWindowCleanup() {
    int i = 0;
    int n = JPanelEPICSRecs.EPICSRecListFrame_names.size();
    boolean found = false;
    String win_name = this.getTitle();
    while (!found && i < n) {
      if (win_name.equals((String)JPanelEPICSRecs.EPICSRecListFrame_names.elementAt(i))) {
        found = true;
        JPanelEPICSRecs.EPICSRecListFrames.removeElementAt(i);
        JPanelEPICSRecs.EPICSRecListFrame_names.removeElementAt(i);
      }
      else i++;
    }
    this.dispose();
  } //end of doWindowCleanup


//-------------------------------------------------------------------------------
  /**
   * Add the record to the window for monitoring
   *@param rec String: Record field to be monitored/added
   *@param desc String: Text to be displayed next to the text field as a description
   */
  public void add_rec(String rec, String desc) {
    n++;
    EPICSTextField field = new EPICSTextField(rec, rec, desc, true);
    JTextField label = new JTextField();
    label.setBackground(new Color(216, 208, 200));
    label.setBorder(null);
    label.setText(rec + " - " + desc);
    ///label.setHorizontalAlignment(SwingConstants.LEADING);
    jPanel1.add(field, null);
    jPanel2.add(label, null);
    this.setSize(600,10 + n * 25 + 25);
    jPanel1.invalidate();
    this.validate();
  } //end of add_rec


//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param e ComponentEvent: TBD
   */
  void this_componentMoved(ComponentEvent e) {
    jei.set_screen_loc(this.getTitle(), this.getLocation(), this.getSize());
  } //end of this_componentMoved

} //end of class EPICSRecListFrame
