package ufjei;

import java.awt.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;


//===============================================================================
/**
 *TBD
 */
public class statusFrame extends JFrame {
  public static final String rcsID = "$Name:  $ $Id: statusFrame.java,v 0.0 2002/06/03 17:44:36 hon beta $";
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JLabel jLabel2 = new JLabel();
  JLabel jLabel3 = new JLabel();
  JLabel jLabel4 = new JLabel();
  JLabel jLabel5 = new JLabel();
  JButton jButton1 = new JButton();
  EPICSTextField [] jTextFieldValue;
  JLabel [] jLabelName;
  JLabel [] jLabelType;
  JLabel [] jLabelUnits;
///  JLabel [] jLabelInfo;
///  JScrollPane [] jScrollPanes;
  GridLayout gridLayout1 = new GridLayout();
  int n;


//-------------------------------------------------------------------------------
  /**
   *Constructor method
   *@param section status_recs: status records object
   */
  public statusFrame(status_recs section) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit(section);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit(status_recs section) throws Exception {
    n = section.status_recs.size();
    status_rec stat;

    jButton1.setText("Close");
    jLabel1.setText("Value");
    jLabel2.setText("Name");
    jLabel3.setText("Type");
    jLabel4.setText("Units");
    jTextFieldValue = new EPICSTextField [n];
    jLabelName = new JLabel [n];
    //jLabelType = new JLabel [n];
    jLabelUnits = new JLabel [n];
    Font font = (new JLabel()).getFont();
    FontMetrics fm = this.getFontMetrics(font);
    String maxName = "Name";
    String maxUnits = "Units";
    int maxNameSize = fm.charsWidth(maxName.toCharArray(),0,maxName.length());
    int maxUnitsSize = fm.charsWidth(maxUnits.toCharArray(),0,maxUnits.length());
    String rec_name;
    for (int r = 0; r < n; r++) {
      stat = (status_rec)section.status_recs.elementAt(r);
      rec_name = EPICS.prefix +"sad:"+ stat.name;
      jTextFieldValue[r] = new EPICSTextField(rec_name,stat.info);
      jTextFieldValue[r].setText(" ");
      jTextFieldValue[r].setColumns(10);
      jLabelName[r] = new JLabel();
      jLabelName[r].setText(rec_name);
      maxNameSize = Math.max(maxNameSize,fm.charsWidth(rec_name.toCharArray(),0,rec_name.length()));
      jLabelUnits[r] = new JLabel();
      jLabelUnits[r].setText(stat.units);
      maxUnitsSize = Math.max(maxUnitsSize,fm.charsWidth(stat.units.toCharArray(),0,stat.units.length()));
      jTextFieldValue[r].setToolTipText(stat.info);
      jLabelName[r].setToolTipText(stat.info);
    }
    jPanel1.setLayout(null);
    gridLayout1.setColumns(4);
    gridLayout1.setRows(0);
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    int lineHeight = 35;
    jLabel1.setBounds(0,0,100,20);
    jLabel2.setBounds(120,0,maxNameSize,20);
    jLabel4.setBounds(140+maxNameSize,0,maxUnitsSize,20);
    jPanel1.add(jLabel1);
    jPanel1.add(jLabel2);
    jPanel1.add(jLabel4);
///    jPanel1.add(jLabel5, new XYConstraints(160+maxNameSize+maxUnitsSize,0,maxInfoSize,20));
    for (int r = 0; r < n; r++) {
      jTextFieldValue[r].setBounds(0,lineHeight*(r+1),100,20);
      jPanel1.add(jTextFieldValue[r]);
      jLabelName[r].setBounds(120,lineHeight*(r+1),maxNameSize,20);
      jPanel1.add(jLabelName[r]);
      jLabelUnits[r].setBounds(140+maxNameSize,lineHeight*(r+1),maxUnitsSize,20);
      jPanel1.add(jLabelUnits[r]);
///      jPanel1.add(jScrollPanes[r], new XYConstraints(160+maxNameSize+maxUnitsSize,lineHeight*(r+1),150,35));
    }
///    this.setSize(new Dimension(160+maxNameSize+maxUnitsSize+180, 110 + lineHeight * n));
    this.setSize(new Dimension(160+maxNameSize+maxUnitsSize, 110 + lineHeight * n));
    //this.setSize(new Dimension(600, 110 + lineHeight * n));
    //this.setSize(new Dimension(700, 310 + lineHeight * n));
    this.setTitle("Status Information for " + section);
    this.getContentPane().add(jPanel2, BorderLayout.SOUTH);
    this.setLocation(100,100);
    jPanel2.add(jButton1, null);
    jButton1.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButton1_actionPerformed(e);
      }
    });
  } //end of jbInit


//---------------------------------------------------------------------------------
  /**
   *Event handler for jButton1
   *statusFrame button 1 action performed
   *@param e not used
   */
  public void jButton1_actionPerformed (ActionEvent e) {
    dispose();
  } //end of jButton1_actionPerformed

} //end of class statusFrame
