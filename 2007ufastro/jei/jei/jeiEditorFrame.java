package ufjei;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;


//===============================================================================
/**
 * Handles the Text Editor frame
 * Can only edit one file at a time
 */
public class jeiEditorFrame extends JFrame {

  JTextArea jTextArea = new JTextArea();
  JButton saveButton = new JButton("Save");
  JButton exitButton = new JButton("Exit");
  JButton searchButton = new JButton("Search");
  JTextField searchTextField = new JTextField(25);
  String filename;
  String win_name;


//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param _filename String: Name of file to be edited
   */
  public jeiEditorFrame(String _filename) {
    filename = _filename;
    File file = new File(filename);
    if (file.isDirectory()) { // choose a file
      JFileChooser chooser = new JFileChooser(filename);
      if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
        filename = chooser.getSelectedFile().getPath();
      }
      else {
        dispose();
	return;
      }
    }
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
        dispose();
      }
    });
    exitButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    saveButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        savefile();
      }
    });
    searchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        search();
      }
    });
    win_name = "Editing " + filename;
    this.setTitle(win_name);
    this.setLocation(jei.get_screen_loc(win_name));
    this.setSize(jei.get_screen_size(win_name));
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    getContentPane().add(new JScrollPane(jTextArea),"Center");
    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(2,1));
    JPanel panel1 = new JPanel();
    panel1.add(saveButton);
    panel1.add(exitButton);
    JPanel panel2 = new JPanel();
    panel2.add(searchTextField);
    panel2.add(searchButton);
    panel.add(panel2);
    panel.add(panel1);
    getContentPane().add(panel,"South");
    loadfile();
    jTextArea.setCaretPosition(0);
    this.show();
  } //end of jeiEditorFrame


//-------------------------------------------------------------------------------
  /**
   * Searches for a string that user is prompted for
   * --non-case-sensative
   */
  public void search() {
    String s = jTextArea.getText();
    int caret_pos = jTextArea.getCaretPosition();
    String criteria = searchTextField.getText();
    int found_pos = -1;
    for (int i=0; i<s.length(); i++) {
      if (s.regionMatches(true,(caret_pos + i) % s.length(),criteria,0,criteria.length())) {
        found_pos = (caret_pos + i) % s.length();
        break;
      }
    }
    if (found_pos != -1) {
      jTextArea.setCaretPosition(found_pos);
      jTextArea.setSelectionStart(found_pos);
      jTextArea.setSelectionEnd(found_pos + criteria.length());
    } else JOptionPane.showMessageDialog(null,"String not found","String not found: " + criteria,JOptionPane.INFORMATION_MESSAGE);
  } //end of search


//-------------------------------------------------------------------------------
  /**
   * Loads the file into the editor
   * The filename is determined when the frame is constructed
   */
  public void loadfile() {
    jTextArea.setText("");
    TextFile inFile = new TextFile(filename, TextFile.IN);
    if (inFile.ok()) {
      String line = "";
      while ((line = inFile.readLine()) != null && inFile.ok()) {
        jTextArea.append(line + "\n");
      }
      inFile.close();
    }
    else {
      jTextArea.setText("CANNOT OPEN " + filename);
    }
  } //end of loadfile


//-------------------------------------------------------------------------------
  /**
   * Saves the text to a file
   * The file name is determined when the frame is constructed
   */
  public void savefile() {
    TextFile outFile = new TextFile(filename, TextFile.OUT);
    if (outFile.ok()) {
      outFile.print(jTextArea.getText());
      outFile.close();
    }
  } //end of savefile

} //end of class jeiEditorFrame
