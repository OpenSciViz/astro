#if !defined(__UFGemDeviceAgent_h__)
#define __UFGemDeviceAgent_h__ "$Name:  $ $Id: UFGemDeviceAgent.h,v 0.19 2006/08/28 21:24:53 hon Exp $"
#define __UFGemDeviceAgent_H__(arg) const char arg##UFGemDeviceAgent_h__rcsId[] = __UFGemDeviceAgent_h__;

#include "UFDeviceAgent.h"
#include "UFSADFITS.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"
#include "deque"

class UFGemDeviceAgent: public UFDeviceAgent {
public:
  UFGemDeviceAgent(int argc, char** argv, char** envp= 0);
  UFGemDeviceAgent(const string& name, int argc, char** argv, char** envp= 0);
  inline virtual ~UFGemDeviceAgent() { if( _capipe ) ::pclose(_capipe); }
  static void sighandler(int sig);
  inline static string epicsDbName() { return _epics; }

  // new public virtual to include one-time optional set/init of sad channel names: 
  virtual void setDefaults(const string& instrum= "", bool initsad= false);

  // this was only meant to be used on boot (one time get), and
  // is very inefficient. should not be used at any other time... (now deprecated!) -- hon
  static int getEpicsOnBoot(const string& edbhost, const string& confrec, vector< string >& recs);

  // get single channel value 
  static int getEpics(const string& edbhost, const string& rec, string& recval);

  // get single channel value and return string result (calls int getEpics)
  static string getEpics( const string& edbhost, const string& edbrec );

  // get multiple channel values (probably SADs) 
  static int getEpics(const string& edbhost, const vector< string >& recs, vector< string >& recvals);

  // simple "plug-in" for epics channel access puts (and gets)   
  // for Epics CA puts:
  static FILE* pipeToEpicsCAchild(const string& epicshost= "");
  // for Epics CA gets (singletons):
  static FILE* pipeFromEpicsCAchild(const string& epicshost, const string& edbrec);
  // for Epics CA gets (multipe records, multiple iterations):

  inline static int pipeToFromEpicsCAchild(const string& epicshost) {
    // spawn child with (pseudo) full duplex pipe to parent:
    // -q quiet outputs single string val. only or error(s)
    _cardwrpid = UFRuntime::pipeReadWriteChild("ufcaget -q", _capipeWr, _capipeRd);
    return (int)_cardwrpid; 
  }

  // of use to apps. that do not derive from this class
  static int sendEpicsSAD(string& sadrec, int val, FILE*& capipe);
 
  static int sendEpics(const string& edbrecdval); // syntax: "edb.fld=value" 
  static int sendEpics(const string& edbrec, const string& val); // use ufcaputarr
  inline static int sendEpics(const string& edbrec, int val)
    { strstream s; s<<val<<ends; string sv = s.str(); delete(s.str()); return sendEpics(edbrec, sv); }
  inline static int sendEpics(const string& edbrec, double val)
    { strstream s; s<<val<<ends; string sv = s.str(); s.str(); return sendEpics(edbrec, sv); }

  static int sendEpics(const string& edbrec, const vector< string >& valvec);

  static int sendEpicsCAR(const string& carname, int val= 0, string* errmsg= 0, int err= 3);
  inline static int setCARIdle(const string& carname) { return sendEpicsCAR(carname, 0); }
  inline static int setCARBusy(const string& carname) { return sendEpicsCAR(carname, 1); }
  inline static int setCARPause(const string& carname) { return sendEpicsCAR(carname, 2); }
  inline static int setCARError(const string& carname, string* errmsg= 0) { return sendEpicsCAR(carname, 3, errmsg); }

protected:
  static bool _confirm, _vverbose;
  string _heart, _confGenSub, _statRec, _carRec;
  vector < string > _StatList, _StatRecs, _CarRecs;
  double _Update, _Clock, _ClockV; // for status update rate
  static FILE *_capipe; // for half-duplex output pipe (to ufcaputarr)
  static int _capipeWr, _capipeRd; // for full-duplex in/out pipe ((multiple/indefinite) to/from ufcaget)
  static pid_t _cardwrpid;
  static string _epics; 

  // must override this:
  virtual int options(string& servlist);

  // it may be useful to override these:
  virtual void updateEpics(const string& edbrec, const string& val, bool immediate= false);
  virtual void updateEpics(const string& edbrec, const vector< string >& vals, bool immediate= false);
};

#endif // __UFGemDeviceAgent_h__
