#if !defined(__UFGemDeviceAgent_cc__)
#define __UFGemDeviceAgent_cc__ "$Name:  $ $Id: UFGemDeviceAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemDeviceAgent_cc__;

#include "UFGemDeviceAgent.h"

bool UFGemDeviceAgent::_confirm= false;
bool UFGemDeviceAgent::_vverbose= false;
FILE* UFGemDeviceAgent::_capipe= 0;
int UFGemDeviceAgent::_capipeWr= -1;
int UFGemDeviceAgent::_capipeRd= -1;
pid_t UFGemDeviceAgent::_cardwrpid= (pid_t)-1;
string UFGemDeviceAgent::_epics;

// static funcs:
void UFGemDeviceAgent::sighandler(int sig) {
  //clog<<"UFGemDeviceAgent:::sighandler> sig: "<<sig<<endl;
  if( sig == SIGCHLD ) { // presumably this is the _capipe child:
    clog<<"UFGemDeviceAgent:::sighandler> child-death, sig: "<<sig<<endl;
    //UFRndRobinServ::_childdeath = true;
    //::pclose(_capipe);
    //_capipe = 0;
    return;
  }
  else if( sig == SIGPIPE ) {
    clog<<"UFGemDeviceAgent:::sighandler> socket or pipe closed, sig: "<<sig<<endl;
    UFRndRobinServ::_lost_connection = true;
    return;
  }
  else if( sig == SIGINT || sig == SIGTERM ) {
    clog<<"UFGemDeviceAgent:::sighandler> shutdown/terminate, sig: "<<sig<<endl;
    UFRndRobinServ::_shutdown = true;
    if( _capipeWr >= 0 ) {
      UFRuntime::psend(_capipeWr, "quit");
      ::close(_capipeWr); _capipeWr = -1;
    }
    if( _capipeRd >= 0 ) {
      ::close(_capipeRd); _capipeRd = -1;
    }
    if( _cardwrpid > 0 ) ::kill(_cardwrpid, SIGKILL);
    //UFRndRobinServ::_childdeath = true;
    if( _capipe ) {
      ::pclose(_capipe);
      _capipe = 0;
    }
    UFRndRobinServ::shutdown();
  }
  return UFRndRobinServ::sighandlerDefault(sig);
}

//public:
// new public virtual to include one-time optional set/init of sad channel names:
void UFGemDeviceAgent::setDefaults(const string& instrum, bool initsad) {
  _epics = instrum;
  if( _epics.empty() || _epics == "false" ) {
    _confGenSub = "";
    _heart = "";
  }
  else if( initsad ) {
    UFSADFITS::initSADHash(_epics);
  }
  //clog<<" UFGemDeviceAgent::setDefaults> _epics: "<<_epics<<endl;
  return UFDeviceAgent::_setDefaults(instrum);
}

// ctors:
UFGemDeviceAgent::UFGemDeviceAgent(int argc, char** argv, char** envp) :
  UFDeviceAgent(argc, argv, envp),
  _heart(), _confGenSub(), _statRec(), _carRec(), 
  _StatList(), _StatRecs(), _CarRecs(),
  _Update(1.0), _Clock(0.0), _ClockV(0.0) { _epics = ""; }

UFGemDeviceAgent::UFGemDeviceAgent(const string& name, int argc,
				   char** argv, char** envp) :
  UFDeviceAgent(name, argc, argv, envp), 
  _heart(), _confGenSub(), _statRec(), _carRec(),
  _StatList(), _StatRecs(), _CarRecs(),
  _Update(1.0), _Clock(0.0), _ClockV(0.0) { _epics = "";  }

int UFGemDeviceAgent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);

  string arg = findArg("-noepics"); // run without epics
  if( arg == "true" ) {
    UFGemDeviceAgent::setDefaults("false");
  }
  
  arg = findArg("-epics"); // epics host/db name
  if( arg != "false" ) {
    if( arg != "true" && !arg.empty() )
      UFGemDeviceAgent::setDefaults(arg, true);
    else
      UFGemDeviceAgent::setDefaults();
  }

  arg = findArg("-sim");
  if( arg != "false" )
    _sim = true;

  arg = findArg("-heart");
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf"); // override config channel name
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-update");
  if( arg != "false" && arg != "true" )
    _heart = atoi(arg.c_str());

  arg = findArg("-vv");
  if( arg != "false" ) {
   _vverbose = true; // very verbose
  }

  return port;
}

// static (public)
int UFGemDeviceAgent::sendEpicsSAD(string& sadrec, int val, FILE*& capipe) {
  if( sadrec.empty() ) {
    clog<<"UFGemDeviceAgent::sendEpicsSAD> no sadrec present..."<<endl;
    return 0;
  }
  if( capipe == 0 ) capipe = pipeToEpicsCAchild();
  if( capipe == 0 ) return -1;
  // should double-check that the hostname appears in the recordname "host:" etc.
  // formulate proper cmd syntax for ufcaput ("dbrec.field=whatever")
  if( UFRuntime::writable(capipe) == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpicsSAD> capipe not writable"<<endl;
    return -1;
  }
  strstream caput;
  caput<<sadrec<<".INP="<<val<<ends;
  string s = caput.str(); delete caput.str();
  if(s.length() <= 2 || s.find(":") == string::npos || s.find("=") == string::npos ) {
    clog<<"UFGemDeviceAgent::sendEpicsSAD> ?badly formed epics record name? "<<s<<endl;
    return -1;
  }

  // be sure to remove any trailing new-lines, since we can only allow one...
  int nlpos = s.find("\n");
  if( nlpos != (int)string::npos ) s = s.substr(0, nlpos-1);
  s += "\n";
  int nb = write(fileno(capipe), s.c_str(), s.length());
  fflush(capipe);
  if( nb > 0 && _verbose )
    clog<<"UFGemDeviceAgent::sendEpicsSAD> edb=val: "<<s<<endl;

  if( capipe && nb <= 0 ) {
    clog<<"UFGemDeviceAgent::sendEpicsSAD> failed to send edb=val: "<<s<<endl;
    ::pclose(capipe);
    capipe = 0;
  }
  return nb;
} // sendEpics (string& sadrec, int val, FILE* capipe)}

FILE* UFGemDeviceAgent::pipeToEpicsCAchild(const string& epicshost) {
  // for epics channel access puts to CARS
  // really should ping epics host first to insure it's alive
  // return UFCAwrap::pipeToCAPut()
  // try to fund ufcaput in a few obvious places:
  // this assumes csh or tcsh is used by popen
  // should really figure out which directory self is invoked
  // and try to find a (self-consistent) ufcaput in the same place
  char* ldp = getenv("LD_LIBRARY_PATH");
  string ldpath = "/usr/local/lib:/gemini/epics/lib:/usr/local/epics/lib:/opt/EDTpdv ";
  if( ldp != 0 )
    ldpath = string(ldp) + ":" + ldpath;
      
  string env = string("/usr/bin/env LD_LIBRARY_PATH=") + ldpath;
  char* ufinst = getenv("UFINSTALL");
  string ufcap;
  struct stat stats;
  if( ufinst )  {
    env = string("/usr/bin/env LD_LIBRARY_PATH=") + ufinst + string("/lib:") + ldpath;
    ufcap = string(ufinst) + string("/bin/ufcaputarr ");
    if( stat(ufcap.c_str(), &stats) == 0 )
      ufcap += " ";
    else
      ufcap = string(ufinst) + string("/sbin/ufcaputarr ");
#if defined(LINUX)
    ufcap = string(ufinst) + string("/sbin/ufcaputarr ");    
#endif
  }
  else {
    ufcap = string("/usr/local/uf/bin/ufcaputarr ");
    if( stat(ufcap.c_str(), &stats) == 0 )
      ufcap += " ";
    else
      ufcap = string("/share/local/bin/ufcaputarr ");
#if defined(LINUX)
    ufcap = string("/share/local/sbin/ufcaputarr ");    
#endif
  }
      
  string ufcaput = env + ufcap; // full invocation of dynamic binary
  if( _confirm ) ufcaput += "-c ";
  if( _verbose ) ufcaput += "-v ";

  if( _verbose )
    clog<<"UFGemDeviceAgent::pipeToEpicsCAchild> "<<ufcaput<<endl;
  FILE* ret= 0;
  ret = popen(ufcaput.c_str(), "w");
  if( ret == 0 )
    clog << "UFGemDeviceAgent::pipeToEpicsCAchild> failed to exec. "<<ufcaput<<endl;
  
  return ret;
}

FILE* UFGemDeviceAgent::pipeFromEpicsCAchild(const string& epicshost, const string& edbrec) {
  // for epics channel access gets from specified channel/rec name.
  // really should ping epics host first to insure it's alive
  // this assumes csh or tcsh is used by popen
  char* ldp = getenv("LD_LIBRARY_PATH");
  string ldpath = "/usr/local/lib:/gemini/epics/lib:/usr/local/epics/lib:/opt/EDTpdv ";
  if( ldp != 0 )
    ldpath = string(ldp) + ":" + ldpath;
 
  string env = string("/usr/bin/env LD_LIBRARY_PATH=") + ldpath;
  char* ufinst = getenv("UFINSTALL");
  string ufcag;
  if( ufinst ) {
    env = string("/usr/bin/env LD_LIBRARY_PATH=") + ufinst + string("/lib:") + ldpath;
    ufcag = string(ufinst) +  string("/bin/ufcaget ");
#if defined(LINUX)
    ufcag = string(ufinst) +  string("/sbin/ufcaget ");
#endif
  }
  else {
    ufcag = string("/usr/local/uf/bin/ufcaget ");
#if defined(LINUX)
    ufcag = string("/usr/local/uf/sbin/ufcaget ");    
#endif
  }
  string ufcaget = env + ufcag; // full invocation of dynamic binary
  if( _verbose ) ufcaget += "-v ";

  // just support single rec. get here:
  ufcaget += "-g " + edbrec;
  //if( _verbose )
    clog<<"UFGemDeviceAgent::pipeFromEpicsCAchild> "<<ufcaget<<endl;
  FILE* ret= 0;
  ret = popen(ufcaget.c_str(), "r");
  if( ret == 0 )
    clog << "UFGemDeviceAgent::pipeFromEpicsCAchild> failed to exec. "<<ufcaget<<endl;
  
  return ret;
}

//public:
int UFGemDeviceAgent::getEpics(const string& edb, const string& rec, string& val) {
  val == "";
  if( rec.find(":") == string::npos || rec.find("::") != string::npos ) {
    clog<<"UFGemDeviceAgent::getEpics> Bad gemini epics channel name: "<<rec<<endl;
    return -1;
  }
  clog<<"UFGemDeviceAgent::getEpics> epics channel name: "<<rec<<endl;
  if( _capipeWr < 0 && _capipeRd < 0 ) {
    clog<<"UFGemDeviceAgent::getEpics> uninitialized full duplex pipe to ufcaget, chan: "<<rec<<endl;
    return -1;
    /* don't attempt to fix things here, allow higher level agent code the priv.
    int stat = pipeToFromEpicsCAchild(edb);
    if( stat < 0 ) {
      clog<<"UFGemDeviceAgent::getEpics> failed to initialize full duplex pipe to ufcaget."<<endl;
      return stat;
    }
    */
  }

  string so = rec + "\n";
  int ns = UFRuntime::psend(_capipeWr, so);
  if( ns <= 0 ) {
    clog<<"UFGemDeviceAgent::getEpics> failed to send to cagetpipe: "<<so<<endl;
    return ns;
  }
  //clog<<"UFGemDeviceAgent::getEpics> sent to cagetpipe: "<<so<<endl;

  int nr = UFRuntime::precv(_capipeRd, val); // if successfull, only one (quiet mode) line is epxected
  if( nr <= 0 ) {
    clog<<"UFGemDeviceAgent::getEpics> failed to recv from cagetpipe: "<<val<<endl;
    return nr;
  }
  //clog<<"UFGemDeviceAgent::getEpics> recv from cagetpipe: "<<val<<endl;

  if( val.find("UFCAget> failed") == string::npos ) // assume all Ok?
    return (int)val.size(); 

  clog<<"UFGemDeviceAgent::getEpics> channel name not found? "<<val<<endl;

  // failed, try to continue reading additional text to flush pipe
  // val = "Failed to access " + rec;
  /*
  UFPosixRuntime::sleep(0.1); // give it a chance to write next line?
  while( UFRuntime::available(_capipeRd)) >= 0 ) {
    UFRuntime::precv(_capipeRd, val); // block until next line is recv'd
    UFPosixRuntime::sleep(0.1); // give it a chance to write next line
  }
  */
  return -1;
}

//Call int getEpics and just return the first string.
// Also, concatenate edbhost + ":" + edbrec to get actual rec name. //  eh always (hon)? 

string UFGemDeviceAgent::getEpics( const string& edbhost, const string& edbrec ) {
  string reply;
  
  if( edbhost != "false" &&  edbhost != "" ) {
    if( edbrec != "" && edbrec.find(":") != 0 ) {
      getEpics( edbhost, edbhost + ":" + edbrec, reply ); // insert :
    }
    else if( edbrec != "") {
      getEpics( edbhost, edbhost + edbrec, reply ); // : present
    }
  }

  if( reply.length() > 0 )
    return reply;
  else
    return "?";
}

int UFGemDeviceAgent::getEpics(const string& edb, const vector< string >& recs, vector< string >& vals) {
  vals.clear();
  string val;
  for( size_t i = 0; i < recs.size(); ++i ) {
    int stat = getEpics(edb, recs[i], val);
    if( stat < 0 ) 
      break;
    vals[i] = val;
  }
  return (int)vals.size();
}

// this was only meant to be used on boot (one time get), and
// is very inefficient. should not be used at any other time... (now deprecated!) -- hon
int UFGemDeviceAgent::getEpicsOnBoot(const string& edb,  const string& conf, vector< string >& recs) {
  // get the names of the Epics analog input or genSub or SAD records to be used 
  // (for temperature monitoring, etc.) and put the readings there
  recs.clear();
  FILE* edbfp = pipeFromEpicsCAchild(edb, conf);
  if( edbfp == 0 ) {
    clog<<"UFGemDeviceAgent::getEpics> failed to open conection to "<<edb<<endl;
    return 0;
  }
  char buf[1024]; memset(buf, 0, sizeof(buf));
  int cnt = 1;
  bool first = true;
  do {
    char* cs = fgets(buf, sizeof(buf), edbfp);
    if( first ) {
      first = false;
      cnt = atoi(buf);
      if( _verbose )
	clog<<"UFGemDeviceAgent::getEpics> expecting "<<cnt<<" elements."<<endl;
      continue;
    }
    string s = cs;
    // find trailing "'\n"
    size_t tailpos = s.rfind("'\n");
    size_t eqpos = s.find("=");
    if( eqpos != string::npos ) {
      s = s.substr(2+eqpos, tailpos-eqpos-2); // eliminate leading & trailing "'"
      char *cs = (char*)s.c_str();
      while( *cs == ' ' || *cs == '\t' ) ++cs; // eliminate leading white sp
      s = cs; recs.push_back(s);
      if( _verbose )
	clog<<"UFGemDeviceAgent::getEpics> "<<s<<endl;
      --cnt;
    }
    memset(buf, 0, sizeof(buf));
  } while( feof(edbfp) == 0 && cnt > 0);

  ::pclose(edbfp);

  return (int)recs.size();
}

void UFGemDeviceAgent::updateEpics(const string& edbrec, const string& val, bool immediate) {
  //float clck = 1.0*clock() / CLOCKS_PER_SEC;
  if( immediate ) {
    sendEpics(edbrec, val);
    return;
  }
  double clck = 1.0*time(0);
  double cdiff = clck - _Clock;
  if( cdiff >= _Update ) {
    _Clock = clck;
    sendEpics(edbrec, val);
  }
  return;  
}

void UFGemDeviceAgent::updateEpics(const string& edbrec, const vector< string >& vals, bool immediate) {
  //float clck = 1.0*clock() / CLOCKS_PER_SEC;
  if( immediate ) {
    sendEpics(edbrec, vals);
    return;
  }
  double clck = 1.0*time(0);
  double cdiff = clck - _ClockV;
  if( cdiff >= _Update ) {
    _ClockV = clck;
    sendEpics(edbrec, vals);
  }
  return;  
}

int UFGemDeviceAgent::sendEpicsCAR(const string& car, int val, string* errmsg, int err) {
  // should double-check that the hostname appears in the recordname "host:" etc.
  // formulate proper cmd syntax for ufcaput
  if( car == "" ) {
    clog<<"UFGemDeviceAgent::sendEpicsCAR> no CAR present..."<<endl;
    return 0;
  }
  if( _capipe == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpicsCAR> no pipe to CAR proc. present..."<<endl;
    return -1;
  }
  if( UFRuntime::writable(_capipe) == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpicsCAR> pipe to CAR proc. not writable"<<endl;
    return -1;
  }

  string carec = car;
  size_t pos = car.find("."); // record.field has been provided
  if( pos != string::npos )
    carec = car.substr(0, pos);
  strstream sv, se, sen;
  sv << carec << ".IVAL="<<val<<"\n"<<ends;
  //if( _verbose )
    clog<<"UFGemDeviceAgent::sendEpicsCAR> "<<sv.str()<<flush;

  // note that writing the ival should cause CAR to 'process'
  // while writing the errmsg and errno should not(?)
  int nb= 0;
  if( val < 3 ) { // not an error, write ival and return
    //nb = fwrite(sv.str(), sizeof(char), strlen(sv.str()), _capipe); 
    nb = write(fileno(_capipe), sv.str(), strlen(sv.str())); 
    fflush(_capipe);
    delete sv.str();
    return nb;
  }
  else {
    if( errmsg ) {
      se << carec << ".IMSS=" << *errmsg <<"\n"<<ends;
      sen << carec << ".IERR=" << err <<"\n"<<ends;
    }
    else {
      se << carec << ".IMSS=" << "Error (unspecific or timeout)" <<"\n"<<ends;
      sen << carec << ".IERR=" << err <<"\n"<<ends;
    }
    if( _verbose ) 
      clog<<"UFGemDeviceAgent::sendEpicsCAR> "<<se.str()<<flush;
  }
  //UFPosixRuntime::sleep(0.5);
  nb = write(fileno(_capipe), se.str(), strlen(se.str())); 
  nb += write(fileno(_capipe), sen.str(), strlen(sen.str())); 
  nb += write(fileno(_capipe), sv.str(), strlen(sv.str())); 
  fflush(_capipe);
  delete sv.str(); delete se.str();

  if( _verbose )
    clog<<"UFGemDeviceAgent::sendEpicsCAR> sent nb = "<<nb<<" to _capipe Fd: "<<fileno(_capipe)<<endl;

  if( _capipe && nb <= 0 ) {
    ::pclose(_capipe);
    _capipe = 0;
  }
  return nb;
}

int UFGemDeviceAgent::sendEpics(const string& dbfieldval) {
  // should double-check that the hostname appears in the recordname "host:" etc.
  // formulate proper cmd syntax for ufcaput ("dbrec.field=whatever")
  if( dbfieldval == "" ) {
    clog<<"UFGemDeviceAgent::sendEpics> no dbfieldval present..."<<endl;
    return 0;
  }
  string s = dbfieldval;
  if( _capipe == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpics> no pipe present..."<<endl;
    return -1;
  }
  if( UFRuntime::writable(_capipe) == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpics> pipe not writable"<<endl;
    return -1;
  }
  if(s.length() <= 2 || s.find(":") == string::npos || s.find("=") == string::npos ) {
    clog<<"UFGemDeviceAgent::sendEpics> ?badly formed epics record name? "<<s<<endl;
    return -1;
  }

  // be sure to remove any trailing new-lines, since we can only allow one...
  int nlpos = s.find("\n");
  if( nlpos != (int)string::npos ) s = s.substr(0, nlpos-1);
  s += "\n";
  int nb = write(fileno(_capipe), s.c_str(), s.length());
  fflush(_capipe);
  if( nb > 0 && _vverbose )
    clog<<"UFGemDeviceAgent::sendEpics> edb=val: "<<s<<endl;
  else if( nb > 0 && _verbose && dbfieldval.find("heart") == string::npos )
    clog<<"UFGemDeviceAgent::sendEpics> edb=val: "<<s<<endl;

  if( _capipe && nb <= 0 ) {
    ::pclose(_capipe);
    _capipe = 0;
    clog<<"UFGemDeviceAgent::sendEpics> failed to send edb=val: "<<s<<endl;
  }
  return nb;
} // sendEpics (const string& dbfieldval)

int UFGemDeviceAgent::sendEpics(const string& dbfield, const string& val) {
  // should double-check that the hostname appears in the recordname "host:" etc.
  // formulate proper cmd syntax for ufcaput ("dbrec.field=whatever")
  if( dbfield.size() < 1 ) {
    clog<<"UFGemDeviceAgent::sendEpics> no dbfield present..."<<endl;
    return 0;
  }
  if( val.size() < 1 ) {
    clog<<"UFGemDeviceAgent::sendEpics> no value present..."<<endl;
    return 0;
  }
  string s = dbfield;
  string sv = val;
  if( _capipe == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpics> no pipe present..."<<endl;
    return -1;
  }
  if( UFRuntime::writable(_capipe) == 0 ) {
    clog<<"UFGemDeviceAgent::sendEpics> pipe not writable"<<endl;
    return -1;
  }
  if(dbfield.length() <= 2 || dbfield.find(":") == string::npos ) {
    clog<<"UFGemDeviceAgent::sendEpics> ?badly formed epics record name? "<<dbfield<<endl;
    return -1;
  }
  if(val.length() == 0 ) { 
    clog<<"UFGemDeviceAgent::sendEpics> no value specified, set to 0"<<endl;
    sv = "0";
  }

  // be sure to remove any trailing new-lines, since we can only allow one...
  int nlpos = s.find("\n");
  if( nlpos != (int)string::npos ) s = s.substr(0, nlpos-1);
  nlpos = sv.find("\n");
  if( nlpos != (int)string::npos ) sv = sv.substr(0, nlpos-1);

  s = s + "=" + sv + "\n"; // ensuring single new-line.
  //int nb = fwrite(s.c_str(), sizeof(char), s.length(), _capipe);
  int nb = write(fileno(_capipe), s.c_str(), s.length());
  fflush(_capipe);
  if( nb > 0 && _vverbose )
    clog<<"UFGemDeviceAgent::sendEpics> edb: "<<dbfield<<", val: "<<val<<endl;
  else if( nb > 0 && _verbose && dbfield.find("heart") == string::npos )
    clog<<"UFGemDeviceAgent::sendEpics> edb: "<<dbfield<<", val: "<<val<<endl;

  if( _capipe && nb <= 0 ) {
    ::pclose(_capipe);
    _capipe = 0;
  }
  return nb;
}

int UFGemDeviceAgent::sendEpics(const string& dbfield, const vector< string >& valvec) {
  if( dbfield == "" ) {
    clog<<"UFGemDeviceAgent::sendEpics> no dbfield present..."<<endl;
    return 0;
  }
  int nv = (int) valvec.size();
  if( nv <= 0 )
    return nv;

  string putarr = valvec[0]; // expects "val0&val1&val2" syntax
  for( int i = 1; i < nv - 1; ++i )
    putarr += "&" + valvec[i];

  // and the final element:
  if( nv > 1 )
    putarr += "&" + valvec[nv-1];

  return sendEpics(dbfield, putarr);
}

#endif // __UFGemDeviceAgent_cc__
