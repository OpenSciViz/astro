#if !defined(__UFSys5Runtime_h__)
#define __UFSys5Runtime_h__ "$Name:  $ $Id: UFSys5Runtime.h,v 0.2 2006/04/27 17:34:51 hon Exp $"
#define __UFSys5Runtime_H__(arg) const char arg##UFSys5Runtime_h__rcsId[] = __UFSys5Runtime_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx

#include "unistd.h"
#include "sys/types.h"
#include "sys/sem.h"

#if defined(LINUX)
#if !defined(_XOPEN_SOURCE)
#define _XOPEN_SOURCE
#endif
#include "sys/ipc.h"
#else
#include "sys/ipc.h"
#endif

// c++ 
using namespace std;
#include "ctime"
#include "cerrno"
#include "cstdio"
#include "iostream"
#include "strstream"
#include "map"
#include "string"

class UFSys5Runtime {
public:
  struct sem5 { key_t key; int semid; inline sem5() : key(-1), semid(-1) {} };
  // provide sys5 sestd::maphores, since linux lacks posix func.
  static int sem5Attach(string& name); // ftok, semget (1)
  static int sem5Create(string& name); // ftok, semget (1)
  static int sem5Value(const string& name); // semop (read)
  static int sem5Release(const string& name); // semop/semctl (alter)
  static int sem5TakeNoWait(const string& name); // ??
  static int sem5Take(const string& name); // semop/semctl (alter)
  static int sem5Delete(const string& name); // semctl IPC_RMID

  // access the table
  static int sem5id(const string& name);
  static key_t sem5key(const string& name);
  static sem5* find(const string& name);

protected:
  inline UFSys5Runtime() {}
  static key_t _ftok(string& name);  
  static std::map< string, UFSys5Runtime::sem5* > _keyTable;
};

#endif // __UFSys5Runtime_h__
