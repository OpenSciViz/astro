#if !defined(__UFLDAP_h__)
#define __UFLDAP_h__ "$Name:  $ $Id: UFLDAP.h,v 0.1 2004/09/17 19:19:11 hon Exp $"
#define __UFLDAP_H__(arg) const char arg##LDAP_h__rcsId[] = __UFLDAP_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFLDAP : public UFDaemon {
public:
  inline UFLDAP(const string& name, char** envp= 0) : UFDaemon(name, envp) {}
  inline UFLDAP(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp) {}
  inline UFLDAP(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp) {}
  inline UFLDAP(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp) {}
  inline virtual ~UFLDAP() {}

  inline virtual string description() const { return __UFLDAP_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // stubs for now, eventually have these manage queued commands:  
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }
};

#endif // __UFLDAP_h__
