#if !defined(__UFRndRobinServ_cc__)
#define __UFRndRobinServ_cc__ "$Name:  $ $Id: UFRndRobinServ.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFRndRobinServ_cc__;

#include "UFRndRobinServ.h"
#include "UFTimeStamp.h"

#include "sys/socket.h"
#include "netdb.h"

using namespace std;
#include "string"

// class public static attributes & functions:
// sig-pipe boolean
bool UFRndRobinServ::_lost_connection = false;
bool UFRndRobinServ::_shutdown = false;
bool UFRndRobinServ::_childdeath = false;
bool UFRndRobinServ::_verbose = false;
UFServerSocket UFRndRobinServ::_theServer;
// globally accessed by sigHandler
UFSocket::ConnectTable* UFRndRobinServ::_theConnections= 0;
pthread_mutex_t* UFRndRobinServ::_theConMutex= 0;
// (support optional) replication service -- send all data to this client
UFSocket* UFRndRobinServ::_replicate= 0; 

const string UFRndRobinServ::_geminiCtrl = "172.16";
const string UFRndRobinServ::_geminiData = "172.17";
const string UFRndRobinServ::_ufirastronet = "192.168";

string UFRndRobinServ::_allowsubnet= UFRndRobinServ::_ufirastronet;

void UFRndRobinServ::threadedServ() { 
  _theConMutex = new pthread_mutex_t;
  ::pthread_mutex_init(_theConMutex, (const pthread_mutexattr_t *) 0);
  ::pthread_mutex_lock(_theConMutex);
  ::pthread_mutex_unlock(_theConMutex);
  //clog<<"UFRndRobinServ::threadedServ> connection mutex initialized."<<endl;
  UFPosixRuntime::_threaded= true;
}
 

void UFRndRobinServ::shutdown() {
  clog<<"UFRndRobinServ::shutdown> closing all connections, cnt: "<<_theConnections->size()<<endl;
  _theServer.closeAndClear(*UFRndRobinServ::_theConnections);
  exit(0);
}

void UFRndRobinServ::sighandlerDefault(int sig) {
  clog<<"UFRndRobinServ::sighandler> sig: "<<sig<<endl;
  if( sig == SIGCHLD ) {
    _childdeath = true;
    clog<<"UFRndRobinServ::sighandler> child proc. died? "<<endl;
    return;
  }
  else if( sig == SIGPIPE ) {
    _lost_connection = true;
    clog<<"UFRndRobinServ::sighandler> lost socket connection or pipe to child? "<<endl;
    return;
  }
  else if( sig == SIGINT || sig == SIGTERM ) {
    _shutdown = true;
    shutdown();
  }

  return UFPosixRuntime::defaultSigHandler(sig);
}

// security accept filter:
bool UFRndRobinServ::_allowableClient(UFSocket* csp, const string& subnet) {
  // subnet based 'host.allow'
  if( _allowsubnet == "all" || _allowsubnet == "All" || _allowsubnet == "ALL" )
    return true;

  if( csp == 0 ) {
    clog<<"UFRndRobinServ::_allowableClient> bad socket..."<<endl;
    return false;
  }
  string peerIP = "unknown";
#if !defined(SunOS5_7) && !defined(CYGWIN)
  peerIP = csp->peerIP();
  if( peerIP == "" ) {
    clog<<"UFRndRobinServ::_allowableClient> unable to get peername, rejecting client."<<endl;
    return false;
  }
  string hostIP = UFSocket::ipAddrOf(hostname());
  size_t pos = peerIP.find(".");
  pos = peerIP.find(".", 1+pos);
  string peersubnet = peerIP.substr(0, pos);
  pos = hostIP.find(".");
  pos = hostIP.find(".", 1+pos);
  // allow hosts from B class subnets (xxx.yyy.*):
  string hsubnet = hostIP.substr(0, pos);
  if( _verbose )
    clog<<"UFRndRobinServ::_allowableClient> client (peer) subnet: "
        <<peersubnet<<", service host subnet: "<<hsubnet<<endl;
  if( peersubnet.find(subnet) == 0 && _verbose ) {
    clog<<"UFRndRobinServ::_allowableClient> Ok, accept peer from private subnet: "
        <<peersubnet<<", service host subnet: "<<hsubnet<<endl;
  }
  if( peersubnet.find("127.0") == 0 && _verbose ) {
    clog<<"UFRndRobinServ::_allowableClient> Ok, accept peer from localhost: "
        <<peersubnet<<", service host subnet: "<<hsubnet<<endl;
  }
  if( peersubnet.find(subnet) != 0 && hsubnet != peersubnet && peersubnet.find("127.0") != 0) {
    clog<<"UFRndRobinServ::_allowableClient> disallow client connection from "<<peerIP<<endl;
    clog<<"UFRndRobinServ::_allowableClient> client subnet: "<<peersubnet<<", service host subnet: "<<hsubnet<<endl;
    return false;
  }
#endif
  if( _verbose )
    clog<<"UFRndRobinServ::_allowableClient> connection OK from "<<peerIP<<endl;
  return true;
}

// the dhs agent implementation seems to throw an exception
// on the return from the version of this function the uses the
// static global '_theServer', try this...
// use server socket provided
// accept restricted to local subnet...
UFSocket* UFRndRobinServ::acceptOn(UFServerSocket& server) {
  //if( _verbose )
  //  clog<<"UFRndRobinServ::acceptOn> check for new client connection on: "
  //      <<server.description()<<endl;

  // non-blocking accept:
  UFSocket* csp= 0;
  int socfd = server.acceptClient(csp, 0.1); // specify 0.1 sec timeOut
  if( socfd <= 0 ) {
    //if( _verbose )
    //  clog<<"UFRndRobinServ::acceptOn> timedOut waiting on new client connection..."<<endl;
    return 0;
  }
  if( csp == 0 ) {
    clog<<"UFRndRobinServ::acceptOn> socket allocation failed..."<<endl;
    return 0;
  }

  bool subnetAllow = _allowableClient(csp, _allowsubnet);
  bool subnetUF = _allowableClient(csp, _ufirastronet);
  bool subnetGeminiData= _allowableClient(csp, _geminiData);
  bool subnetGeminiCtrl= _allowableClient(csp, _geminiCtrl);
  bool allowed = subnetAllow ||  subnetUF || subnetGeminiData || subnetGeminiCtrl;

  if( !allowed ) {
    clog<<"UFRndRobinServ::acceptOn> connection denied.. "<<endl;
    csp->close(); delete csp; csp = 0;
    return csp;
  }

  // if( _verbose )
  // clog<<"UFRndRobinServ::acceptOn> return socket ptr: "<<csp<<endl;
  return csp;
}
 

// the dhs agent implementation seems to through an exception
// on the retrn from the version of this function the uses the
// server arg', try the global...
// accept restricted to local subnet...
// this must change (relaxed?) for delivery
UFSocket* UFRndRobinServ::accept() {
  //if( UFSocket::_verbose )
  //  clog<<"UFRndRobinServ::accept> check for new client connection on: "
  //      <<_theServer.description()<<endl;

  // non-blocking accept:
  UFSocket* csp= 0;
  int socfd = _theServer.acceptClient(csp, 0.1); // specify 0.1 sec timeOut
  if( socfd <= 0 ) {
    //if( _verbose )
    //  clog<<"UFRndRobinServ::accept> timedOut waiting on new client connection..."<<endl;
    return 0;
  }
  if( csp == 0 ) {
    clog<<"UFRndRobinServ::accept> socket allocation for new client failed..."<<endl;
    return 0;
  }

  bool subnetAllow = _allowableClient(csp, _allowsubnet);
  bool subnetUF = _allowableClient(csp, _ufirastronet);
  bool subnetGeminiData= _allowableClient(csp, _geminiData);
  bool subnetGeminiCtrl= _allowableClient(csp, _geminiCtrl);
  bool allowed = subnetAllow ||  subnetUF || subnetGeminiData || subnetGeminiCtrl;

  if( !allowed ) {
    clog<<"UFRndRobinServ::accept> connection denied.. "<<endl;
    csp->close(); delete csp; csp = 0;
    return csp;
  }
  //if( _verbose )
  // clog<<"UFRndRobinServ::accept> return socket ptr: "<<csp<<endl;
  return csp;
}

int UFRndRobinServ::clearAllMsgs(UFRndRobinServ::MsgTable& mtbl) {
  int cnt= 0;
  UFRndRobinServ::MsgTable::iterator i = mtbl.begin();
  for( ; i != mtbl.end(); ++i, ++cnt ) {
    UFProtocol* msg = i->second;
    if( msg != 0 ) {
      delete msg;
    }
  }
  mtbl.clear();
  return cnt;
}

int UFRndRobinServ::main(int argc, char** argv, char** envp) {
  UFRndRobinServ ufrrs("UFRndRobinServ", argc, argv, envp); // ctor binds to listen/socket
  ufrrs.exec((void*) &ufrrs);
  return 0;
}


// return all keys in the ConnectTable (which should be connectionID information)
int UFRndRobinServ::connectList(const string& name,
				const UFSocket::ConnectTable& ct,
				vector< string >& vs) {
  vs.clear();
  UFSocket::ConnectTable::const_iterator i;
  if( _theConMutex ) pthread_mutex_lock(_theConMutex);
  for( i = ct.begin(); i != ct.end(); ++i )
    vs.push_back(i->first);
  if( _theConMutex ) pthread_mutex_unlock(_theConMutex);
  
  return vs.size();
}

// in a newly allocated UFStrings object
UFStrings* UFRndRobinServ::connectList(const string& name,
				       const UFSocket::ConnectTable& ct) {
  vector< string > tmp;
  int cnt = connectList(name, ct, tmp);
  if( cnt <= 0 )
    clog<<"UFRndRobinServ::connectList> not connected? "<< name<<endl;

  return new UFStrings(name, tmp);
}

// non-static stuff:
UFRndRobinServ::UFRndRobinServ(int argc,
			       char** argv,
			       char** envp) : UFDaemon(argc, argv, envp), _ack(false),
                                              _ancil(0), _immed(), _queued() {
  //UFRndRobinServ::_theConnections = new UFSocket::ConnectTable;
  _theConnections = new UFSocket::ConnectTable;
  _allowsubnet = _ufirastronet; // UF
  /* 
  string nc = "null client";
  UFSocket::ConnectTable tmp; 
  tmp[nc] = (UFSocket*) 0;
  (*_theConnections)[nc] = (UFSocket*) 0;
  clog<<"UFRndRobinServ> connection list init: "<<_theConnections->size()<<endl;
  */
}

UFRndRobinServ::UFRndRobinServ(const string& name,
			       int argc, char** argv,
			       char** envp) : UFDaemon(name, argc, argv, envp),
                                              _ack(false), _ancil(0),
					      _immed(), _queued() {
  //UFRndRobinServ::_theConnections = new UFSocket::ConnectTable;
  _theConnections = new UFSocket::ConnectTable;
  _allowsubnet = _ufirastronet; // UF
  /* 
  string nc = "null client";
  UFSocket::ConnectTable tmp; 
  tmp[nc] = (UFSocket*) 0;
  (*_theConnections)[nc] = (UFSocket*) 0;
  clog<<"UFRndRobinServ> connection list init: "<<_theConnections->size()<<endl;
  */
}

UFRndRobinServ::UFRndRobinServ(const string& name,
			       UFRuntime::Argv* args,
			       char** envp) : UFDaemon(name, args, envp),
                                              _ack(false), _ancil(0),
					      _immed(), _queued() {
  //UFRndRobinServ::_theConnections = new UFSocket::ConnectTable;
  _theConnections = new UFSocket::ConnectTable;
  _allowsubnet = _ufirastronet; // UF
  /* 
  string nc = "null client";
  UFSocket::ConnectTable tmp; 
  tmp[nc] = (UFSocket*) 0;
  (*_theConnections)[nc] = (UFSocket*) 0;
  clog<<"UFRndRobinServ> connection list init: "<<_theConnections->size()<<endl;
  */
}

void UFRndRobinServ::startup() {
  setSignalHandler(UFRndRobinServ::sighandlerDefault, threaded());
  //clog << "UFRndRobinServ::startup> established signal handler."<<endl;
  string servlist;
  int listenport = options(servlist);
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFRndRobinServ::> listening on port= " <<listenport
       <<", with server soc: "<<_theServer.description()<<endl;
  return;
}

int UFRndRobinServ::options(string& servlist) {
  string arg = findArg("-ack");
  if( arg != "false" ) {
    _ack = true;
    clog<<name()<<" service will acknowledge all client requests with accept/deny ufstring..."<<endl;
  }
  arg = findArg("-allow");
  if( arg != "false" && arg != "true" ) {
    _allowsubnet = arg;
    clog<<name()<<" service will all client requests within subnet: "<<_allowsubnet<<endl;
  }

  strstream thelist;
  thelist<<"Frame@"<<hostname()<<"~52000, "                  /* 1 */
         <<"Executive@"<<hostname()<<"~52001, "              /* 2 */
         <<"LakeShore218@"<<hostname()<<"~52002, "           /* 3 */
         <<"LakeShore340@"<<hostname()<<"~52003, "           /* 4 */
         <<"GranPhil@"<<hostname()<<"~52004, "               /* 5 */
         <<"PortescapMotors@"<<hostname()<<"~52005, "        /* 6 */
         <<"CryoCooler@"<<hostname()<<"~52006, "             /* 7 */
         <<"PPCVMEpics@"<<hostname()<<"~52007, "             /* 8 */
         <<"MCE4@"<<hostname()<<"~52008, "                   /* 9 */
         <<"MCE4@annex:192.168.111.101~~7008, "              /* 10 */
         <<"PPCVMEpics@annex:192.168.111.101~7007, "         /* 11 */
         <<"CryoCooler@annex:192.168.111.101~7006, "         /* 12 */
         <<"PortescapIndexors@annex:192.168.111.101~7005, "  /* 13 */
         <<"GranPhil@annex:192.168.111.101~7004, "           /* 14 */ 
         <<"LakeShore218@annex:192.168.111.101~7002, "       /* 15 */
         <<"LakeShore340@annex:192.168.111.101~7003, "       /* 16 */
         <<"Aux@annex:192.168.111.101~7001, "                /* 17 */
         <<"Test@"<<hostname()<<"~55555";                    /* 178*/
  
  servlist = thelist.str();
  delete thelist.str();
  int port = 55555; // default/testing service listen port
  arg = findArg("-listen");
  if( arg != "false" && arg != "true" ) {
    port = atoi(arg.c_str());
    if( port == 52000 )
      rename("Frame");
    else if( port == 52001 )
      rename("Executive");
    else if( port == 52002 )
      rename("LakeShore218");
    else if( port == 52003 )
      rename("LakeShore340");
    else if( port == 52004 )
      rename("GranPhil");
    else if( port == 52005 )
      rename("PortescapMotors");
    else if( port == 52006 )
      rename("CryoCooler");
    else if( port == 52007 )
      rename("PPCVMEpics");
    else if( port == 52008 )
      rename("MCE4");
  }

  arg = findArg("-l"); //list all services
  if( arg != "false" ) {
    clog<<servlist<<endl;
  }
  return port;
}

int UFRndRobinServ::pendingReqs(vector <UFSocket*>& clients) {
  clients.clear();
  UFSocket::AvailTable bytcnts;
  if( _theConnections == 0 ) {
    clog<<"UFRndRobinServ::pendingReqs> no connection table?"<<endl;
    return -1;
  }
  if( _theConnections->size() == 0 ) {
    clog<<"UFRndRobinServ::pendingReqs> empty connection table?"<<endl;
    return 0;
  }
  //if( _verbose )
  //clog<<"UFRndRobinServ::pendingReqs> # of clients in connection table to check: "<<_theConnections->size()<<endl;
  int acnt = UFSocket::availableList(*_theConnections, bytcnts, _theConMutex);
  if( acnt <= 0 ) {
    //if( _verbose )
    //clog<<"UFRndRobinServ::pendingReqs> no clients have pending/available i/o, acnt: "<<acnt<<", of "<<_theConnections->size()<<endl;
    return acnt;
  }
  //if( _verbose )
  //clog<<"UFRndRobinServ::pendingReqs> # of clients in connection table to (double) check: "<<acnt<<" out of: "<<_theConnections->size()<<endl;
  UFSocket::AvailTable::iterator i = bytcnts.begin();
  for( ; i != bytcnts.end(); ++i ) {
    UFSocket* socp = i->first; 
    int nb = i->second;
    if( nb > 0 && socp != 0 ) {
      clients.push_back(socp);
    }
    else if( nb < 0 && socp != 0 ) { // stale connection? close it...
      //if( _verbose )
        clog<<"UFRndRobinServ::pendingReqs> connection stale?, closing..."<<acnt<<endl;
      int concnt = UFSocket::closeAndClear(*_theConnections, socp, _theConMutex);
      if( _verbose )
        clog<<"UFRndRobinServ::pendingReqs> current connection cnt= "
	    <<concnt<<", avail. cnt= "<<acnt<<endl;
    }
    else if( socp != 0 ) { // available timed-out?
      if( _verbose && UFSocket::_verbose )
        clog<<"UFRndRobinServ::pendingReqs> nb: "<<nb<<", nothing yet on "<<socp->description()<<endl;
    }
  }

  if( _verbose && clients.size() > 0 )
    clog<<"UFRndRobinServ::pendingReqs> # of clients with (valid) pendingReqs: "
        <<clients.size()<<", out of: "<<_theConnections->size()<<endl;

  return (int) clients.size();   
} 

string UFRndRobinServ::greetClient(UFSocket* clsoc, const string& serviceName) {
  string clname= "";
  //if( _verbose )
    clog<<"UFRndRobinServ::greetClient> "<<serviceName<<" receiving client greeting..."<<endl;
  if( clsoc == 0 ) {
    clog<<"UFRndRobinServ::greetClient> ? unitialized socket..."<<endl;
    return clname;
  }
  if( clsoc->readable() < 0 ) {
    clog<<"UFRndRobinServ::greetClient> ? connection not readable, closing..."<<endl;
    return clname;
  }
 
  // client connected:
  // allocate new protocol object, since we may not know in advance its type:
  UFProtocol* greet = 0;
  try {
    greet = UFProtocol::createFrom(*clsoc);
  }
  catch(std::exception& e) {
    clog<<"UFRndRobin::greetClient> stdlib exception occured: "<<e.what()<<endl;
  }
  catch(...) {
    clog<<"UFRndRobin::greetClient> unknown exception occured"<<endl;
  }
  
  if( greet == 0 ) {
    clog<<"UFRndRobinServ::greetClient> ? error receiving proper client greeting, closing..."<<endl;
    return clname;
  }

  string t = greet->timeStamp();
  clname = greet->name();
  //if( _verbose )
    clog<<"UFRndRobinServ::greetClient> client name: "<<clname<<endl;

  //if( t.rfind(":") == string::npos || clname.length() < 1 ) {
  if( clname.length() < 1 ) {
    // reject bad timestamp or unnamed client
    clog<<"UFRndRobinServ::greetClient> client name and/or timestamp missing? "<<clname<<endl;
#if !defined(SunOS5_7) && !defined(CYGWIN)
    string peer = clsoc->peerName();
    string peerIP = clsoc->peerIP();
    // this is a work-around fix for the epics clients that don't connect properly:
    // this is for UF:
    if( peer.find("vme") != string::npos || peer.find("ppc") != string::npos || peer.find("68k") != string::npos ||
	peerIP.find("192.168.111.198") != string::npos ||
	peerIP.find("192.168.111.199") != string::npos ||
	peerIP.find("192.168.111.233") != string::npos ||
	peer.find("trecs") != string::npos || peer.find("miri") != string::npos ||
	peer.find("flam") != string::npos || peer.find("f2") != string::npos ||
	peer.find("foo") != string::npos || peer.find("fu") != string::npos  ) {
      clname = peer + "Epics@"; clname += UFRuntime::currentTime();
    }
    // for user supplied allowed subnet:
    if( clname.length() < 1 && peerIP.find(_allowsubnet) != string::npos ) {
      clname = "flam2Epics@"; clname += UFRuntime::currentTime();
    }
    // for UF
    if( clname.length() < 1 && peerIP.find(_ufirastronet) != string::npos ) {
      clname = "flam2Epics@"; clname += UFRuntime::currentTime();
    }
    // for Gemini?
    if( clname.length() < 1 && peerIP.find(_geminiCtrl) != string::npos ) {
      clname = "flam2Epics@"; clname += UFRuntime::currentTime();
    }
    if( clname.length() < 1 && peerIP.find(_geminiData) != string::npos ) {
      clname = "flam2Epics@"; clname += UFRuntime::currentTime();
    }
    if( clname.length() < 1 ) {
      clog<<"UFRndRobinServ::greetClient> client failed to send proper greeting, closing..."<<endl;
      return clname;
    }
    else {
      clog<<"UFRndRobinServ::greetClient> guessing client is: "<<clname<<endl;
    }
#else
    clname = "flam2Unknown@"; clname += UFRuntime::currentTime();
    if( _verbose )
      clog<<"UFRndRobinServ::greetClient> guessing client is: "<<clname<<endl;
#endif
  }

  greet->stampTime(UFRuntime::currentTime()); 
  // return indication of time of initial connection along with client name:
  clname += "@"+greet->timeStamp();
  greet->rename(serviceName+" accepted client: "+clname);

  if( clsoc->writable() > 0 ) {
    int ns = clsoc->send(*greet);
    if( ns <= 0 ) {
      clog<<"UFRndRobinServ::greetClient> ? error replying to client greeting, closing..."<<endl;
      delete greet; clname = "";
      return clname;
    }
  }
  else {
    clog<<"UFRndRobinServ::greetClient> ? new client connection not writable, closing..."<<endl;
    delete greet; clname = "";
    return clname;
  }

  clog<<"UFRndRobinServ::greetClient> completed greeting: "<<greet->name()<<endl;
  delete greet; // no memory leak

  return clname;
}

string UFRndRobinServ::newClient(UFSocket* clsoc, const string& agentname) {
  string clname = "";
  if( clsoc == 0 ) {
    clog<<"UFRndRobinServ:::newClient> "<<agentname<<" new client connection null?."<<endl;
    return clname;
  }
  try {
    clname = greetClient(clsoc, agentname);  
  }
  catch(std::exception& e) {
    clog<<"UFRndRobin::newClient> stdlib exception occured in newClient accept: "<<e.what()<<endl;
  }
  catch(...) {
    clog<<"UFRndRobin::newClient> unknown exception occured in newClient accept..."<<endl;
  }
  //if( _verbose ) 
    clog<<"UFRndRobinServ:::newClient> "<<agentname<<" exchanging greeting with "<<clname<<endl;

  // optional replication client support:
  if( clname.find("replica") != string::npos || clname.find("Replica") != string::npos ||
      clname.find("REPLICA") != string::npos ) {
    if( UFRndRobinServ::_replicate != 0 ) {
       clog<<"UFRndRobinServ::newClient> resetting _replicate socket!"<<endl;
       // close and destroy old socket?
       UFRndRobinServ::_replicate->close(); delete UFRndRobinServ::_replicate;
    }
    clog<<"UFRndRobinServ::newClient> set _replicate socket"<<endl;
    UFRndRobinServ::_replicate = clsoc;
  }

  return clname;
} // newClient

int UFRndRobinServ::acknowledge(UFSocket* soc, UFProtocol* req, const string& a) {
  string nm = name(); // service name
  if( req == 0 ) {
    UFStrings ack(nm, (const string*) &a, 1);
    return soc->send(ack);
  }
  nm += "<>" + req->name();
  UFStrings ack(nm, (const string*) &a, 1);
  return soc->send(ack);  
}

bool UFRndRobinServ::acceptable(UFProtocol* req, string& a) {
  bool stat= true;
  if( req != 0 ) 
    a = "Accepted";
  else {
    stat = false;
    a = "Rejected";
  } 
  return stat;
}

int UFRndRobinServ::recvReqs(const vector<UFSocket*>& clients) {
  // for each client that has an un-read request:
  // recv the req. (UFProtocol) and put it into the appropriate
  // msgtable for subsequent processing (by servImmed or servQueued):
  int cnt= 0, rcnt= 0;
  string clientID;
  UFSocket* client;
  UFProtocol* req;
  string ack;
  // recv all requests and segregate:
  while( cnt < (int)clients.size() ) {
    client = clients[cnt++];
    if( !client->validConnection() ) { // client no longer connected?
      UFRndRobinServ::_theServer.closeAndClear(*UFRndRobinServ::_theConnections, client, _theConMutex); // no memory leak
      continue; // skip this client
    }
    if( client->readable() <= 0 ) { // client has not sent anything yet? 
      clog<<"UFRndRobinServ::recvReqs> nothing available/readable from client? "<<client->description()<<endl;
      continue; // skip this client
    }
    else {
      // allocate new protocol object, since we may not know in advance its type:
      // note that while this function allocates a new protocol msg, it deletes none!
      req = UFProtocol::createFrom(*client);
      bool acc = acceptable(req, ack); // accept or reject the request
      if( req != 0 && acc ) {
	++rcnt;
	string t = currentTime();
	//if( t > req->timeStamp() )
          _immed.insert(MsgTable::value_type(client, req));
        /*
        if( _verbose ) {
          clog<<"UFRndRobinServ::recvReqs> req: "<<req->name()<<", _immed cnt: "<<_immed.size()<<endl;
          int ne = req->elements();
          UFStrings* srq = dynamic_cast<UFStrings*>(req);
          for( int i = 0; i < ne; ++i ) clog<<"UFRndRobinServ::recvReqs> "<<(i+1)<<" "<<(*srq)[i]<<endl;
        }
        */
	//else
	//_queued.insert(MsgTable::value_type(client, req));
      }
      else
        clog<<"UFRndRobinServ::handleReqs> ? bad or unacceptable request ?"<<endl;
    }
    if( _ack && client != 0 ) // reply to client with accepted/rejected acknowledgement
      acknowledge(client, req, ack);
  } // for each client
  return rcnt;
}

int UFRndRobinServ::notifyAll(const UFProtocol& notice) const {
  int cnt= 0;
  if( _theConMutex ) pthread_mutex_lock(_theConMutex);
  UFSocket::ConnectTable::iterator i = _theConnections->begin();
  for( ; i != _theConnections->end(); ++i, ++cnt ) {
    UFSocket* socp = i->second; 
    if( socp != 0 && (socp->writable() > 0)) {
      socp->send(notice);
    }
  }
  if( _theConMutex ) pthread_mutex_unlock(_theConMutex);

  return cnt;
}

int UFRndRobinServ::notifyAllStatus(const UFProtocol& notice) const {
  int cnt = 0;
  if( _theConMutex ) pthread_mutex_lock(_theConMutex);
  UFSocket::ConnectTable::const_iterator i = _theConnections->begin();
  for (; i!= _theConnections->end(); ++i) {
    string clname = i->first;
    UFSocket* socp = i->second;
    if (socp != 0 && (socp->writable() > 0) && ( (clname.find("status") != string::npos) ||
						 (clname.find("STATUS") != string::npos) ||
						 (clname.find("Status") != string::npos) )) {
      socp->send(notice);
      cnt++;
    }
  }
  if( _theConMutex ) pthread_mutex_unlock(_theConMutex);
  return cnt;
}

//##############################
// Added by amarin to include new funtions from cancam ufmotord

//Notify clients that are identified with notice.name():

int UFRndRobinServ::notifyStatusClient(const UFProtocol& notice)
{
  string targetClient = notice.name();
  UFStrings::upperCase( targetClient );
  UFSocket::ConnectTable::const_iterator i = _theConnections->begin();
  int cnt= 0;
  for( ; i != _theConnections->end(); ++i ) {
    string clname = i->first;
    UFStrings::upperCase( clname );
    if( clname.find( targetClient ) != string::npos ) {
      UFSocket* socp = i->second;
      if( socp ) {
	if( socp->writable() < 0 ) 
	  clog<<"UFRndRobinServ::notifyAllStatus> client <"<<clname<<"> socket not writable!"<<endl;
	else {
	  socp->send(notice);
	  ++cnt;
	}
      }
    }
  }

  if( cnt == 0 )
    clog<<"UFRndRobinServ::notifyAllStatus> target client <"<<targetClient<<"> not found!"<<endl;
  return cnt;
}
//##############################

int UFRndRobinServ::notifyClients(const UFRndRobinServ::MsgTable& mtbl) const {
  int cnt= 0;
  UFRndRobinServ::MsgTable::const_iterator i = mtbl.begin();
  for( ; i != mtbl.end(); ++i, ++cnt ) {
    UFSocket* socp = i->first;
    UFProtocol* msg = i->second;
    if( msg != 0 && socp != 0 && (socp->writable() > 0) ) {
      socp->send(*msg);
    }
  }
  return cnt;
}

int UFRndRobinServ::echoReq(UFRndRobinServ::MsgTable& reqtbl) {
  int cnt= 0;
  UFRndRobinServ::MsgTable::iterator i = reqtbl.begin();
  for( ; i != reqtbl.end(); ++i, ++cnt ) {
    UFSocket* socp = i->first; 
    UFProtocol* msg = i->second;
    string t = currentTime();
    if( msg != 0 &&(socp->writable() > 0) &&
        t > msg->timeStamp()) {
      msg->rename("UFRndRobinServ::servReqs> default echo -- "+msg->name());
      msg->stampTime(t);
      socp->send(*msg);
      ++cnt;
      // delete the messages allocated by recvReqs
      delete msg; // don't delete the socket!
      // and remove entry from table
      reqtbl.erase(i);
    }
  }
  return cnt;
}

int UFRndRobinServ::servImmed() {
  return echoReq(_immed);
}

int UFRndRobinServ::servQueued() {
  return echoReq(_queued);
}

int UFRndRobinServ::lostConnection(const UFServerSocket& serv) {
  if( _theConnections == 0 ) {
    clog<<"UFRndRobinServ::lostConnection> _theConnections == 0 !"<<endl;
    return 0;
  }
  size_t cs = _theConnections->size();
  //clog<<"UFRndRobinServ::lostConnection> checking for stale connections, current cnt: "<<cs<<endl;
  if( cs <= 0 ) return 0;
  // close server side of all connections and remove from the global connection table
  vector<string> stale;
  serv.findStale(*_theConnections, stale, _theConMutex);
  serv.closeAndClear(*_theConnections, stale, _theConMutex);
  //clog<<"UFRndRobinServ::lostConnection> lost connection cnt: "<<stale.size()<<endl;
 
  //NOTE_-- closeAndClear will clear out the stale vector.  Thus, this method will 
  // always return zero (unless closeAndClear encounters a mutex error

  return (int)stale.size();
  //return retval;
}

// virtual method should be overriden by
// sub-classes can do anything else according 
// to their needs. default does nothing;
void* UFRndRobinServ::ancillary(void* p) { return p; }

// virtual method can be overriden by
// su-classes can hibernate according to their needs
void UFRndRobinServ::hibernate() {
  //clog<<"UFRndRobinServ::hibernate>..."<<endl;
  float tout= 1.0; // standard heartbeat
  // insure timely resp. when client(s) are connected
  if( _theConnections != 0 )
    if( _theConnections->size() <= 0 )
      tout = -1; // no clients, wait forever
  if( UFRndRobinServ::_replicate ) tout = 1.0; // unless replicating...
  /*
  int fdcnt = UFSocket::waitOnAll(tout);
  if( fdcnt < 0 )
    clog<<"UFRndRobinServ::hibernate> error occured on select All..."<<endl;
  */
  UFRuntime::mlsleep(tout);

  //clog<<"UFRndRobinServ::hibernate> wake-up..."<<endl;
  return;
}

// this is the main event-loop of the single-threaded service:
void* UFRndRobinServ::exec(void* p) {
  // an instance of this/myself (UFRndRobinServ*) should be provided:
  UFRndRobinServ* agent = static_cast<UFRndRobinServ*>(p); 
  string agentname = agent->name();
  clog << "UFRndRobinServ::exec> starting service: " << agentname<<endl;

  // check options, which at the minimum should indicate server port #
  agent->startup(); // start listening

  // enter recv req.& send reply loop
  unsigned long reqtot=0;
  string newnam;
  vector <UFSocket*> clientreqs;
  UFRndRobinServ::MsgTable reqtbl;
  while( true ) {
    // check if the sighandler has cought sigint or sigpipe or sigchld:
    if( _shutdown ) {
      clog<<"UFRndRobinServ::exec> termination signal recv'd, or shutdown command..."<<endl;
      shutdown();
    }
    // check for new client connection:
    UFSocket* clsoc= agent->accept();
    //if( _verbose && clsoc == 0 )
    //  clog<<"UFRndRobinServ::exec> no new client connection..."<<endl;

    if( clsoc != 0 ) {
      //clog<<"UFRndRobinServ::exec> check new client connection for correct greeting..."<<endl;
      string clname = newClient(clsoc, agentname);
      if( clname != "" && clsoc != UFRndRobinServ::_replicate ) {
        if( _theConMutex ) pthread_mutex_lock(_theConMutex);
        (*_theConnections)[clname] = clsoc; // insure the table entry exists
        if( _theConMutex ) pthread_mutex_unlock(_theConMutex);
        clog<<"UFRndRobinServ::exec> new client name: "<<clname<<", connection cnt: "<<_theConnections->size()<<endl;
      }
      else if( clsoc == UFRndRobinServ::_replicate ) {
        clog<<"UFRndRobinServ::exec> Replicant client name: "<<clname<<", connection cnt: "<<_theConnections->size()<<endl;
      }
      else {
        clog<<"UFRndRobinServ::exec> new client failed greeting..."<<endl;
        clsoc->close(); delete clsoc; clsoc = 0;
      }
    }
    // check for lost connection, even the newly accepted client could have diconnected!
    //clog<<"UFRndRobinServ::exec> check for lost connection(s)..."<<endl;
    int lcnt = agent->lostConnection(agent->_theServer);
    if( _lost_connection ) { // did we get an interrup (SIGPIPE) before the above finished?
      lcnt = agent->lostConnection(agent->_theServer); // if so, try once more
      _lost_connection = false;
    }
    if( lcnt > 0 ) 
      clog<<"UFRndRobinServ::exec> closed "<<lcnt<<" stale connection(s)..."<<endl;
    //check which clients have submitted requests:
    int reqcnt= 0;
    clientreqs.clear(); reqtbl.clear();
    _immed.clear(); // be sure to start with a cleared 'immediate' list
    size_t ncon = agent->_theConnections->size();
    //if( ncon == 0 && _verbose ) 
    // clog<<"UFRndRobinServ::exec> no clients..."<<endl;
    //else if( _verbose ) 
    //  clog<<"UFRndRobinServ::exec> "<<ncon<<" client connections..."<<endl;
    if( ncon > 0 || UFRndRobinServ::_replicate != 0 ) { 
      reqcnt = agent->pendingReqs(clientreqs);
      if( reqcnt > 0 ) {
        if( _verbose )
          clog<<"UFRndRobinServ::exec> process "<<reqcnt<<" new requests"
	      <<", # of clients= "<<UFRndRobinServ::_theConnections->size()
              <<", # of requests= "<<reqcnt<<endl;
	reqtot += reqcnt;
        int rcnt = agent->recvReqs(clientreqs); // recv & insert ufprotocols into _immed or _queued
        if( rcnt > 0 && _verbose ) 
          clog<<"UFRndRobin::exec> rcvcnt: "<<rcnt<<", immed. cnt: "<<_immed.size()<<endl;
	if( rcnt > 0 && _immed.size() > 0 ) {
          try {
	    agent->servImmed(); // process table, and clear it
          }
          catch(std::exception& e) {
            clog<<"UFRndRobinServ::exec> stdlib exception occured in servImmed: "<<e.what()<<endl;
          }
          catch(...) {
            clog<<"UFRndRobinServ::exec> unknown exception occured in servImmed..."<<endl;
          }
        }
      }
      else if( _queued.size() > 0 ) {
        if( _verbose ) 
          clog<<"UFRndRobin::exec> queued. cnt: "<<_queued.size()<<endl;
        try {
	  agent->servQueued(); // process queued table, and clear it
        }
        catch(std::exception& e) {
          clog<<"UFRndRobinServ::exec> stdlib exception occured in servQueued: "<<e.what()<<endl;
        }
        catch(...) {
          clog<<"UFRndRobinServ::exec> unknown exception occured in servQueued..."<<endl;
        }
      }
      else { // only call ancil if there are no new client reqs.
        // any ancillary logic? heartbeats, status updates, etc.
        if( _ancil ) {
          try {
            void *p = ancillary(_ancil);
            if( p == 0 ) clog<<"UFRndRobinServ> error occured with ancillary logic?"<<endl;
          }
          catch(std::exception& e) {
            clog<<"UFRndRobinServ::exec> stdlib exception occured in ancil: "<<e.what()<<endl;
          }
          catch(...) {
            clog<<"UFRndRobinServ::exec> unknown exception occured in ancil..."<<endl;
          }
        }
	/*      
	if( _verbose )
          clog<<"UFRndRobinServ::exec> no client requests?"
	      <<", # of clients= "<<UFRndRobinServ::_theConnections->size()
	      <<", # of lost clients= "<<lcnt
	      <<", # of requests= "<<reqcnt<<endl;
	*/
        agent->hibernate();
      }
    }
    else { // no new clients or client reqs?
      // any ancillary logic? heartbeats, status updates, etc.
      if( _ancil ) {
        void *p = ancillary(_ancil);
        if( p == 0 ) clog<<"UFRndRobinServ> error occured with ancillary logic?"<<endl;
	/*
        try {
          void *p = ancillary(_ancil);
          if( p == 0 ) clog<<"UFRndRobinServ> error occured with ancillary logic?"<<endl;
        }
        catch(std::exception& e) {
          clog<<"UFRndRobinServ::exec> stdlib exception occured in ancil: "<<e.what()<<endl;
        }
        catch(...) {
          clog<<"UFRndRobinServ::exec> unknown exception occured in ancil..."<<endl;
        }
	*/
      }
      /*
      if( _verbose  )
        clog<<"UFRndRobinServ::exec> no clients?"
          <<", # of clients= "<<UFRndRobinServ::_theConnections->size()
	  <<", # of lost clients= "<<lcnt
          <<", # of requests= "<<reqcnt<<endl;
      */
      agent->hibernate();
    }
  } // while forever
  return p;
}

#endif // __UFRndRobinServ_cc__
