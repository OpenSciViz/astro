#if !defined(__UFTCSClient_h__)
#define __UFTCSClient_h__ "$Name:  $ $Id: UFTCSClient.h,v 0.1 2004/09/17 19:19:13 hon Exp $"
#define __UFTCSClient_H__(arg) const char arg##TCSClient_h__rcsId[] = __UFTCSClient_h__;

#include "UFDaemon.h"
#include "string"

class UFTCSClient : public UFDaemon {
public:
  inline UFTCSClient(const string& name) : UFDaemon(name) {}
  inline UFTCSClient(int argc, char** argv) : UFDaemon(argc, argv) {}
  inline UFTCSClient(const string& name, int argc, char** argv) : UFDaemon(name, argc, argv) {}
  inline virtual ~UFTCSClient() {}

  inline virtual string description() const { return __UFTCSClient_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p=0);
};

#endif // __UFTCSClient_h__
