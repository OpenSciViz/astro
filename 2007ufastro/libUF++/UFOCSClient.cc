#if !defined(__UFOCSClient_cc__)
#define __UFOCSClient_cc__ "$Name:  $ $Id: UFOCSClient.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFOCSClient_cc__;

#include "UFOCSClient.h"
__UFOCSClient_H__(__UFOCSClient_cc);

#include "time.h"

// default behavior of virtuals:
string UFOCSClient::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFOCSClient::exec(void* p) {
  // this is the main function for the Image Server OCSClient
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFOCSClient::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );

  return p;
}

#endif // __UFOCSClient_cc__
