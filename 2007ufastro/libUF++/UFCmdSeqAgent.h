#if !defined(__UFCmdSeqAgent_h__)
#define __UFCmdSeqAgent_h__ "$Name:  $ $Id: UFCmdSeqAgent.h,v 0.1 2004/09/17 19:19:07 hon Exp $"
#define __UFCmdSeqAgent_H__(arg) const char arg##CmdSeqAgent_h__rcsId[] = __UFCmdSeqAgent_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFCmdSeqAgent : public UFDaemon {
public:
  inline UFCmdSeqAgent(const string& name, char** envp= 0) : UFDaemon(name, envp) {}
  inline UFCmdSeqAgent(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp) {}
  inline UFCmdSeqAgent(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp) {}
  inline UFCmdSeqAgent(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp) {}
  inline virtual ~UFCmdSeqAgent() {}

  inline virtual string description() const { return __UFCmdSeqAgent_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // stubs for now, eventually have these manage queued commands:  
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }
};

#endif // __UFCmdSeqAgent_h__
