#if !defined(__UFCmdAgent_h__)
#define __UFCmdAgent_h__ "$Name:  $ $Id: UFCmdAgent.h,v 0.2 2004/09/17 19:19:07 hon Exp $"
#define __UFCmdAgent_H__(arg) const char arg##CmdAgent_h__rcsId[] = __UFCmdAgent_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFCmdAgent : public UFDaemon {
public:
  inline UFCmdAgent(const string& name) : UFDaemon(name) {}
  inline UFCmdAgent(int argc, char** argv) : UFDaemon(argc, argv) {}
  inline UFCmdAgent(const string& name, int argc, char** argv) : UFDaemon(name, argc, argv) {}
  inline UFCmdAgent(const string& name, UFRuntime::Argv* args) : UFDaemon(name, args) {}
  inline virtual ~UFCmdAgent() {}

  inline virtual string description() const { return __UFCmdAgent_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // stubs for now, eventually have these manage queued commands:  
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }
};

#endif // __UFCmdAgent_h__
