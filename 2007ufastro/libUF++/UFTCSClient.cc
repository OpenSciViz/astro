#if !defined(__UFTCSClient_cc__)
#define __UFTCSClient_cc__ "$Name:  $ $Id: UFTCSClient.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFTCSClient_cc__;

#include "UFTCSClient.h"
__UFTCSClient_H__(__UFTCSClient_cc);

#include "time.h"

// default behavior of virtuals:
string UFTCSClient::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFTCSClient::exec(void* p) {
  // this is the main function for the Image Server TCSClient
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFTCSClient::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
  return p;
}

#endif // __UFTCSClient_cc__
