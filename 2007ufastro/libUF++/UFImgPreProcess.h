#if !defined(__UFImgPreProcess_h__)
#define __UFImgPreProcess_h__ "$Name:  $ $Id: UFImgPreProcess.h,v 0.1 2004/09/17 19:19:11 hon Exp $"
#define __UFImgPreProcess_H__(arg) const char arg##ImgPreProcess_h__rcsId[] = __UFImgPreProcess_h__;

#include "UFDaemon.h"
#include "UFObsConfig.h"
#include "string"

class UFImgPreProcess : public UFDaemon {
protected:
  vector< string >* _bufNames; // image diff & co-add buffernames
  UFObsConfig* _obs;

public:
  static UFRingBuff* _theArchiveRingBuff;
  static vector< UFRingBuff* > _theImgRingBuffs;

  inline UFImgPreProcess(const string& name, char** envp= 0) : UFDaemon(name, envp), _bufNames(0), _obs(0) {}
  inline UFImgPreProcess(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp), _bufNames(0), _obs(0) {}
  inline UFImgPreProcess(const string& name, 
			 int argc, char** argv,
			 char** envp= 0) : UFDaemon(name, argc, argv, envp), _bufNames(0), _obs(0) {}
  inline UFImgPreProcess(const string& name, UFRuntime::Argv* args,
			 char** envp= 0) : UFDaemon(name, args, envp), _bufNames(0), _obs(0) {}
  inline virtual ~UFImgPreProcess() {}

  inline virtual string description() const { return __UFImgPreProcess_h__; }

  virtual string heartbeat();

  inline virtual int setObservation(const UFObsConfig& obs) { 
    delete _obs; _obs = new UFObsConfig(obs); return _obs->elements();
 }

  // standard image frmae buffer names, default supports chop-nods:
  inline virtual int frameBufNames(vector< string >& names) {
    if( _bufNames == 0 ) {
      _bufNames= new vector< string >;
      _bufNames->push_back("Sig1"); _bufNames->push_back("Ref1"); _bufNames->push_back("Dif1"); //3
      _bufNames->push_back("Sig2"); _bufNames->push_back("Ref2"); _bufNames->push_back("Dif2"); //6
      _bufNames->push_back("Sig1Accum"); _bufNames->push_back("Ref1Accum"); _bufNames->push_back("Dif1Accum"); //9
      _bufNames->push_back("Sig2Accum"); _bufNames->push_back("Ref2Accum"); _bufNames->push_back("Dif2Accum"); //12
      _bufNames->push_back("Sig"); _bufNames->push_back("SigAccum"); //14
    }
    names.clear();
    for( int i = 0; i < (int) _bufNames->size(); i++ )
      names.push_back((*_bufNames)[i]);

    return  (int) names.size();
  }

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // archive buff, co-add & diff buffs:
  virtual int createSharedResources();
  virtual int deleteSharedResources(); 
};

#endif // __UFImgPreProcess_h__
