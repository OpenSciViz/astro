#if !defined(__UFAcqCtrl_cc__)
#define __UFAcqCtrl_cc__ "$Name:  $ $Id: UFAcqCtrl.cc 14 2008-06-11 01:49:45Z hon $";
const char rcsId[] = __UFAcqCtrl_cc__;

#include "UFSADFITS.h"
#include "UFRuntime.h"
#include "UFAcqCtrl.h"

// globals:
UFAcqCtrl::Observe* UFAcqCtrl::_obssetup= 0;
float UFAcqCtrl::_timeout = -1.0;
bool UFAcqCtrl::_sim = false;
bool UFAcqCtrl::_verbose = false;
bool UFAcqCtrl::_observatory = true;
map< string, UFAcqCtrl::Observe* > UFAcqCtrl::_Observations; // datalabel, ObsSetups

UFAcqCtrl::Observe::Observe(float exptime, int expcnt, const string& dhslabel,
			    const string& datamode, const string& dhsqlook,
			    const string& usrinfo, const string& comment,
			    const string& fits, const string& lut, const string& fileIdx,
			    const string& datadir, const string& nfshost,
			    const string& ds9, const string& pngjpeg) :
  _exptime(""), _expcnt(""), _dhslabel(dhslabel), _datamode(datamode), _dhsqlook(dhsqlook), 
  _nfshost(nfshost), _datadir(datadir), _fitsfile(fits), _lutfile(lut), _index(fileIdx),
  _usrinfo(usrinfo) ,_comment(comment), _ds9(ds9), _pngjpegfile(pngjpeg), _obscfg(0) {
  _exptime = UFRuntime::numToStr(exptime); _expcnt = UFRuntime::numToStr(expcnt);
  _obscfg = new UFFlamObsConf(exptime, dhslabel, expcnt);
}

// start/stop/abort "ObsConf" should be first and "ACQ" should be final
// cmd elements..., "START/STOP/ABORT" in text list 
int UFAcqCtrl::Observe::asStrings(deque< string >& text, const string& directive) {
  text.clear();
  
  text.push_back("OBSCONF"); text.push_back(directive);

  if( _obssetup == 0 && !_exptime.empty() && !_expcnt.empty() && !_dhslabel.empty() ) { // use this
    text.push_back("EXPTIME"); text.push_back(_exptime);
    text.push_back("EXPCNT"); text.push_back(_expcnt);
    text.push_back("DHSLABEL"); text.push_back(_dhslabel);
    text.push_back("DHSQLOOK"); text.push_back(_dhsqlook); 
    text.push_back("DATAMODE"); text.push_back(_datamode); 
    text.push_back("COMMENT"); text.push_back(_comment); 
    text.push_back("USRINFO"); text.push_back(_usrinfo); 
    text.push_back("NFSHOST"); text.push_back(_nfshost); 
    text.push_back("DATADIR"); text.push_back(_datadir); 
    text.push_back("FITSFILE"); text.push_back(_fitsfile); 
    text.push_back("LUTFILE"); text.push_back(_lutfile); 
    text.push_back("INDEX"); text.push_back(_index); 
    text.push_back("PNGJPEG"); text.push_back(_pngjpegfile); 
    text.push_back("DS9"); text.push_back(_ds9);
    // final cmd should be acq directive
    text.push_back("ACQ"); text.push_back(directive);
    return (int) text.size();
  }

  // othwewise if the global _obssetup has been created, use it:
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::Observe::asStrings> no AcqCtrl::Observe* _obssetup?"<<endl;
    return -1;
  }
  else {
    clog<<"UFAcqCtrl::Observe::asStrings> _obssetup: "<<(size_t)_obssetup<<endl;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::Observe::asStrings> no AcqCtrl::Observe _obssetup->_obscfg?"<<endl;
    return -2;
  }
  else {
    clog<<"UFAcqCtrl::Observe::asStrings> _obssetup->obscfg: "<<(size_t)_obssetup->_obscfg<<endl;
  }

  string name = _obssetup->_obscfg->datalabel(); // should be congruent with datalabel
  if( name != _obssetup->_dhslabel ) {
    clog<<"UFAcqCtrl::Observe::asStrings> dhsdatalabel mismatch? "<<name<<" != "<<_obssetup->_dhslabel<<endl;
  }
    
  string expcnt = UFRuntime::numToStr(_obssetup->_obscfg->totImgCnt());
  if( expcnt != _obssetup->_expcnt )
    clog<<"UFAcqCtrl::Observe::asStrings> expcnt mismatch? "<<expcnt<<", "<<_obssetup->_expcnt<<endl;   

  string exptime = UFRuntime::numToStr(_obssetup->_obscfg->expTime());
  if( exptime != _obssetup->_exptime )
    clog<<"UFAcqCtrl::Observe::asStrings> exptime mismatch? "<<exptime<<", "<<_obssetup->_exptime<<endl;   

  text.push_back("EXPTIME"); text.push_back(_obssetup->_exptime);
  text.push_back("EXPCNT"); text.push_back(_obssetup->_expcnt);
  text.push_back("DHSLABEL"); text.push_back(name);
  text.push_back("DHSQLOOK"); text.push_back(_obssetup->_dhsqlook); 
  text.push_back("DATAMODE"); text.push_back(_obssetup->_datamode); 
  text.push_back("COMMENT"); text.push_back(_obssetup->_comment); 
  text.push_back("USRINFO"); text.push_back(_obssetup->_usrinfo); 
  text.push_back("NFSHOST"); text.push_back(_obssetup->_nfshost); 
  text.push_back("DATADIR"); text.push_back(_obssetup->_datadir); 
  text.push_back("FITSFILE"); text.push_back(_obssetup->_fitsfile); 
  text.push_back("LUTFILE"); text.push_back(_obssetup->_lutfile); 
  text.push_back("INDEX"); text.push_back(_obssetup->_index); 
  text.push_back("PNGJPEG"); text.push_back(_obssetup->_pngjpegfile); 
  text.push_back("DS9"); text.push_back(_obssetup->_ds9);

  // final cmd should be acq directive
  text.push_back("ACQ"); text.push_back(directive);
  return (int) text.size();
} 

UFAcqCtrl::UFAcqCtrl(const string& agenthost, const string& framehost, map< string, int>& portmap) :
           _connectedEdt(false), _connectedFITS(false), _agthost(agenthost), _edtfrmhost(framehost), _clientFITS(0) {
  // ctor should not connect to agents
  if( _agthost == "" ) _agthost = UFRuntime::hostname();
  if( _edtfrmhost == "" ) _edtfrmhost = UFRuntime::hostname();
  if( portmap.size() <= 0 ) defaultPorts(portmap);
  map< string, int>::iterator it = portmap.begin();
  for( size_t i = 0; i < portmap.size(); ++i ) {
    _portmap[it->first] = it->second;
  }
  //_obssetup = 0;
} 

UFAcqCtrl::UFAcqCtrl(const string& agenthost, const string& framehost) : 
  _connectedEdt(false), _connectedFITS(false), _agthost(agenthost), _edtfrmhost(framehost), _clientFITS(0) {
  if( _agthost == "" ) _agthost = UFRuntime::hostname();
  if( _edtfrmhost == "" ) _edtfrmhost = UFRuntime::hostname();
  defaultPorts(_portmap);
  //_obssetup = 0;
  clog<<"UFAcqCtrl::UFAcqCtrl> ctor (const string& agenthost, const string& framehost)"<<endl;
}

int UFAcqCtrl::defaultPorts(map< string, int>& portmap) {
  portmap.clear();
  string agnt;

  agnt = "ufgls218d"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52002; // lakeshore 218

  agnt = "ufgls33xd"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52003; // lakeshore 332

  agnt = "ufgpf26xd"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52004; // pfeiffer 262 vacuum

  agnt = "ufgmotord"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52024; // portescap motor indexors

  agnt = "ufgmce4d" ; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52008; // mce4 detector

  agnt = "ufgedtd@" + _edtfrmhost; // no FITS status (as yet)
  portmap[agnt] = 52001; // frame capture/acquisition agent

  agnt = "ufgflam2d@" + _agthost;
  portmap[agnt] = 52000; // executive

  agnt = "ufgsymbarcd@" + _agthost;
  portmap[agnt] = 52005; // symbol barcode

  agnt = "ufglvdtd@" + _agthost;
  portmap[agnt] = 52006; // linear variable differential transformer (focus position)

  agnt = "ufgisplcd@" + _agthost;
  portmap[agnt] = 52007; // linear variable differential transformer (focus position)

  return (int) portmap.size();
}

// minimal (optional?) attributes of the aquisition (ACQ) directive?
// this is obsolete...
UFAcqCtrl::Observe* UFAcqCtrl::attributes(UFDeviceAgent::CmdInfo* act) { 
  // get acq. attributes
  if( act == 0 ) {
    clog<<"UFAcqCtrl::attributes> null pointer arg?"<<endl;
    return 0;
  }
  // car, "carval" is optional, followed by
  // acq, "conf"; obsId, "DHS Label"; DHSwrite, "save|discard"; etc...
  int cnt= 0, elemcnt = (int)act->cmd_name.size();
  UFAcqCtrl::Observe* obs = new UFAcqCtrl::Observe;
  for( int i = 0; i < elemcnt; ++i ) {
    string val = act->cmd_impl[i];
    string key = act->cmd_name[i];
    UFStrings::upperCase( key );

    if( key.find("DHS") != string::npos ) { //the DHSwrite status.
      ++cnt;
      obs->_dhslabel = val;
      UFStrings::upperCase( val );
      if( val.find("DISCARD") != string::npos ) { //no saving data 
	obs->_dhslabel = ""; //do not send to dhs either.
	if( val.find("ALL") != string::npos )
	  obs->_nfshost = obs->_datadir = obs->_fitsfile = "";
      }
    }
    else if( key.find("COMMENT") != string::npos || key.find("INFO") != string::npos ) {
      ++cnt;
      obs->_comment = val;
    }
    else if( key.find("LUT") != string::npos )
      obs->_lutfile = val;
    else if( key.find("ARCHIVE") != string::npos ) {
      if( key.find("HOST") != string::npos )
	obs->_nfshost = val;
      else if( key.find("DIR") != string::npos )
	obs->_datadir = val;
      else if( key.find("FILE") != string::npos )
	obs->_fitsfile = val;
    }
  } //end of loop over act->cmd...
 
  clog<<"UFAcqCtrl::attributes> archive file => "<<obs->_fitsfile<<endl;
  clog<<"UFAcqCtrl::attributes> archive file => "<<obs->_lutfile<<endl;
  clog<<"UFAcqCtrl::attributes> DHS write => "<<obs->_dhslabel<<endl;
  clog<<"UFAcqCtrl::attributes> archive directory => "<<obs->_datadir<<endl;
  clog<<"UFAcqCtrl::attributes> frame server (nfs) host => "<<obs->_nfshost<<endl;
  return obs;
} // UFAcqCtrl::attributes


// create a FlamObsConf and additional observation attributes from 
// parsed UFStrings transation to/from an agent (obssetup prior to acq/observe directive)
UFAcqCtrl::Observe* UFAcqCtrl::obsSetup(UFDeviceAgent::CmdInfo* act) {
  bool dhswrite= false;
  int elemcnt = (int)act->cmd_name.size();
  //if( _verbose )
  //clog<<"UFAcqCtrl::obsSetup> client: "<<act->clientinfo<<", req. elem.: "<<elemcnt<<endl;
  string car= "", sad= "", errmsg= "", mceraw= "";
  float exptime= -1.0;
  int expcnt= -1;
  string dhslabel= "", dhsqlook= "", usrinfo= "", comment= "", nfshost= "", datadir= "", fitsfile= "", index="0000";
  string datamode= "", lutfile= "", ds9= "0", pngjpegfile="";
  /*
  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    clog<<"UFAcqCtrl::obsSetup> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
  }
  */
  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    UFStrings::upperCase(cmdname);
    clog<<"UFAcqCtrl::obsSetup> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
    if( cmdname.find("CAR") != string::npos ) {
      car = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> CAR: "<<car<<endl;
      continue;
    }
    if( cmdname.find("SAD") != string::npos ) {
      sad = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> SAD: "<<sad<<endl;
      continue;
    }
    if( cmdname.find("OBSSETUP") != string::npos ) {
      clog<<"UFAcqCtrl::obsSetup> (re)set obssetup/obsconf: "<<cmdimpl<<endl;
      continue;
    }
    if( cmdname.find("OBSCONF") != string::npos ) {
      clog<<"UFAcqCtrl::obsSetup> (re)set obssetup/obsconf: "<<cmdimpl<<endl;
      continue;
    }

    if( cmdname.find("DATALABEL") != string::npos ||
	cmdname.find("DHSLABEL") != string::npos ||
	cmdname.find("OBSID") != string::npos) {
      dhswrite = true; //assume data mode = save
      dhslabel = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> local archive and dhs: "<<dhslabel<<endl;
      continue;
    }

    if( cmdname.find("DHSQLOOK") != string::npos ) {
      clog<<"UFAcqCtrl::obsSetup> dhsqlook: "<<cmdimpl<<endl;
      dhsqlook = cmdimpl;
      continue;
    }

    // exptime could be an MCE4 Raw EXPTIME "TIMER 0 T S" cmd...
    mceraw = "TIMER 0 ";
    if( cmdname.find("EXPTIME") != string::npos ) {
      if( cmdname.find("RAW") == string::npos && cmdname.find("Raw") == string::npos &&
	  cmdname.find("raw") == string::npos ) {
        exptime = atof(cmdimpl.c_str());
      }
      else if( cmdimpl.find(mceraw) != string::npos ) {
        clog<<"UFAcqCtrl::obsSetup> Raw exptime: "<<cmdimpl<<endl;
        mceraw = cmdimpl.substr(mceraw.length());
        exptime = atof(mceraw.c_str());
      }
      clog<<"UFAcqCtrl::obsSetup> exptime: "<<exptime<<endl;
      continue;
    }

    // expcnt could be an MCE4 Raw EXPCNT "LDVAR 10 CNT" cmd...
    mceraw = "LDVAR 10 ";
    if( cmdname.find("EXPCNT") != string::npos ) {
      if( cmdname.find("RAW") == string::npos && cmdname.find("Raw") == string::npos &&
	  cmdname.find("raw") == string::npos ) {
        expcnt = atoi(cmdimpl.c_str());
      }
      else if( cmdimpl.find(mceraw) != string::npos ) {
        clog<<"UFAcqCtrl::obsSetup> Raw expcnt: "<<cmdimpl<<endl;
        mceraw = cmdimpl.substr(mceraw.length());
        expcnt = atoi(mceraw.c_str());
      }
      clog<<"UFAcqCtrl::obsSetup> expcnt: "<<expcnt<<endl;
      continue;
    }

    if( cmdname.find("NFSHOST") != string::npos ) {
      nfshost = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> nfshost: "<<nfshost<<endl;
      continue;
    }
 
    if( cmdname.find("ROOT") != string::npos ) {
      clog<<"UFAcqCtrl::obsSetup> archive root directory: "<<cmdimpl<<endl;
      datadir = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> archive root directory: "<<datadir<<endl;
      continue;
    }

    if( cmdname.find("FILE") != string::npos ||cmdname.find("FITS") != string::npos ||
	cmdname.find("ARCHIVE") != string::npos ) {
      fitsfile = cmdimpl; //assume data mode = save
      clog<<"UFAcqCtrl::obsSetup> archive fits file: "<<fitsfile<<endl;
      continue;
    }

    if( cmdname.find("INDEX") != string::npos ) {
      index = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> dither index: "<<index<<endl;
      continue;
    }

    if( cmdname.find("LUT") != string::npos ) {
      clog<<"UFAcqCtrl::obsSetup> pixel sort LUT file: "<<cmdimpl<<endl;
      lutfile = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> pixel sort LUT file: "<<lutfile<<endl;
      continue;
    }

    if( cmdname.find("DS9") != string::npos ) {
      ds9 = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> Display to DS9 frame9(s): "<<ds9<<endl;
      continue;
    }

    if( cmdname.find("PNG") != string::npos || cmdname.find("JPG") != string::npos ||
	cmdname.find("JPEG") != string::npos) {
      pngjpegfile = cmdimpl; //assume data mode = save
      clog<<"UFAcqCtrl::obsSetup> PNG or JPEG: "<<pngjpegfile<<endl;
      continue;
    }

    if( cmdname.find("DIR") != string::npos ) { // all (archive and dhs) or just dhs?
      datadir = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> datamode: "<<datamode<<endl;
      continue;
    }

    if( cmdname.find("MODE") != string::npos ) { // all (archive and dhs) or just dhs?
      datamode = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> datamode: "<<datamode<<endl;
      continue;
    }

    if( cmdname.find("DISCARD") != string::npos ) { // all (archive and dhs) or just dhs?
      datamode = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> datamode: "<<datamode<<endl;
      continue;
    }

    if( cmdname.find("COMMENT") != string::npos ) { 
      comment = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> comment: "<<comment<<endl;
      continue;
    }

    if( cmdname.find("USE") != string::npos || cmdname.find("USER") != string::npos ) { 
      usrinfo = cmdimpl;
      clog<<"UFAcqCtrl::obsSetup> user: "<<usrinfo<<endl;
      continue;
    }
  } // finished parsing action array

  // create a new UFAcqCtrl::Observe object for each acq.
  // or onetime create of global obssetup...
  //if( _obssetup != 0 ) { delete _obssetup; _obssetup = 0; }

  if( _obssetup == 0 )
    _obssetup = new (nothrow) UFAcqCtrl::Observe(exptime, expcnt, dhslabel, datamode, dhsqlook, usrinfo, comment,
					         fitsfile, lutfile, index, datadir, nfshost, ds9, pngjpegfile);

  clog<<"UFAcqCtrl::obsSetup> UFAcqCtrl::Observe _obssetup: "<<(size_t)_obssetup<<", _obscfg: "
      <<(size_t)_obssetup->_obscfg<<endl;

  // if there was a ctor issue, force congruency:
  if( exptime > 0 ) {
    _obssetup->_obscfg->setExpTime(exptime);
    _obssetup->_exptime = UFRuntime::numToStr(_obssetup->_obscfg->expTime());
  }
  if( expcnt > 0 ) {
    _obssetup->_obscfg->setImageCnt(expcnt); _obssetup->_obscfg->setFramesPerNod(expcnt);
    _obssetup->_expcnt = UFRuntime::numToStr(_obssetup->_obscfg->totImgCnt());
  }
  if( !dhslabel.empty() ) {
    _obssetup->_obscfg->relabel(dhslabel);
    _obssetup->_dhslabel = dhslabel;
  }

  if( !dhsqlook.empty() )
    _obssetup->_dhsqlook = dhsqlook;
  
  if( !usrinfo.empty() )
    _obssetup->_usrinfo = usrinfo;

  if( !comment.empty() )
    _obssetup->_comment = comment;

  if( !nfshost.empty() )
    _obssetup->_nfshost = nfshost;

  if( !datamode.empty() )
    _obssetup->_datamode = datamode;

  if( !datadir.empty() )
    _obssetup->_datadir = datadir;

  if( !fitsfile.empty() )
    _obssetup->_fitsfile = fitsfile;

  if( !index.empty() )
    _obssetup->_index = index;

  if( !lutfile.empty() )
    _obssetup->_lutfile = lutfile;

  if( !pngjpegfile.empty() )
    _obssetup->_pngjpegfile = pngjpegfile;

  return _obssetup;
} // obsSetup -- full acq ctrl?

// send the obsconf to the dhsclient and get back a text string reply?
string UFAcqCtrl::submitDhsc(Observe* obssetup, UFSocket* replicant) {
  // send flam2obsconf to dhsclient (replicant client)
  string dhsc;
  if( obssetup == 0 )
    return dhsc;

  if( obssetup->_obscfg == 0 )
    return dhsc;

  if( replicant == 0 )
    return dhsc;

  if( !replicant->validConnection() )
    return dhsc;

  int ns= replicant->send(obssetup->_obscfg);
  if( ns > 0 ) dhsc = "DHSclient notified";
  // expect reply with local file name:
  /*
  UFStrings* reply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*this));
  if( reply ) {
    if( reply->numVals() > 0 ) {
      dhsc = (*reply)[0];
    }
  }
  */
  return dhsc;
}

string UFAcqCtrl::submitEdtd(const string& directive) {
  string edtd;
  if( UFAcqCtrl::_obssetup == 0 )
    return edtd;

  if( UFAcqCtrl::_obssetup->_obscfg == 0 )
    return edtd;

  if( !_connectedEdt ) {
    clog<<"UFAcqCtrl::submitEdtd> not connected?"<<endl; 
    int stat = connectEdtd(_obssetup->_obscfg->name(), _edtfrmhost);
    if( stat < 0 )
      return edtd;
    /*
    int fitscnt = (int)_portmap.size() - 1;
    stat = connectFITS(_agthost);
    _connectedFITS = (stat == fitscnt);
    */
  }
  /*
  if( validConnection() ) {
    if( pfits && efits ) {
      pfits->setSeq(1, 3); efits->setSeq(2, 3); obscfg->setSeq(3, 3);
      send(*pfits); send(*efits); send(*obscfg);
    }
    else if( pfits ) {
      pfits->setSeq(1, 2); obscfg->setSeq(2, 2);
      send(*pfits); send(*obscfg);
    }
    else {
      obscfg->setSeq(1, 1);
      send(*obscfg);
    }
  }
  else {
    _connectedFITS = false;
    close();
    return "";
  }
  */
  deque< string > cmds;
  UFAcqCtrl::_obssetup->asStrings(cmds, directive);
  for( size_t nc = 0; nc < cmds.size(); nc += 2 )
    clog<<"UFAcqCtrl::submitEdtd> send: "<<cmds[nc]<<" == "<<cmds[1+nc]<<endl;
    
  UFStrings ufs(_obssetup->name(), cmds);
  int ns = ufs.sendTo(*this);
  clog<<"UFAcqCtrl::submitEdtd> sent ns: "<<ns<<", name: "<<ufs.name()<<endl;

  // expect reply with local file name:
  clog<<"UFAcqCtrl::submitEdtd> expect filename reply from ufgedtd?"<<endl;
  UFProtocol* reply = (UFProtocol::createFrom(*this));
  if( reply )
    edtd = reply->name();

  return edtd;
} // submitEdt

// start observation (connect to ufedtd if necessary) return datalabel/local archive file name
//string UFAcqCtrl::startObs(UFMCE4Config* mce, UFFlamObsConf*& obscfg, 
//			   UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::startObs(UFDeviceConfig* dc) {
  string edtd;
  int width= 0, height= 0;
  bool idle = UFEdtDMA::takeIdle(width,  height);
  if( !idle ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, still busy with prior obs?"<<endl;
    return edtd;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obssetup?"<<endl;
    return edtd;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obsconf?"<<endl;
    return edtd;
  }

  // insure all parameter are set in conf:
  //_obssetup->_obscfg->startObs(); // no longer required!

  // the primary header should be sent to the edtd before it enters into
  // the frame data wait blocking func:
  //prmFITS = fitsHdrPrm(obs.datalabel());
  //extrFITS = fitsHdrExt(obs.datalabel());
  edtd = submitEdtd("AcqStart"); //, prmFITS, extFITS);

  return edtd;
}

// send obssetup start directive as UFStrings to socket, optionally allocating obssetup and/or msg
string UFAcqCtrl::startObs(UFSocket* soc, float exptime, int expcnt, const string& dhslabel,
			   UFAcqCtrl::Observe*& obssetup, UFStrings*& obsmsg) {
  string r;
  int width= 0, height= 0;
  bool idle = UFEdtDMA::takeIdle(width,  height);
  if( !idle ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, still busy with prior obs?"<<endl;
    return r;
  }

  // insure name is set in obs conf:
  if( obssetup == 0 )
    obssetup = new UFAcqCtrl::Observe(exptime, expcnt, "AcqStart||"+dhslabel);
  
  deque< string > cmds ;
  int ns = obssetup->asStrings(cmds);  
  delete(obsmsg); // for now always alloc msg...  
  obsmsg = new UFStrings(obssetup->name(), cmds);
  ns = obsmsg->sendTo(*soc);
  if( ns > 0 ) 
    clog<<"UFAcqCtrl::startObs> sent: "<<obsmsg->name()<<endl;
  else 
    clog<<"UFAcqCtrl::startObs> send failed: "<<obsmsg->name()<<endl;
    
  return r;
}

// send obssetup start directive as UFFlamObsConf to socket, optionally allocating f2obsconf
string UFAcqCtrl::startObs(UFSocket* soc, float exptime, int expcnt, const string& dhslabel, UFFlamObsConf*& obsconf) {
  string r;
  int width= 0, height= 0;
  bool idle = UFEdtDMA::takeIdle(width, height);
  if( !idle ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, still busy with prior obs?"<<endl;
    return r;
  }

  // insure name is set in obs conf:
  if( obsconf == 0 )
    obsconf = new UFFlamObsConf(exptime, dhslabel, expcnt);
  
  r = obsconf->rename("AcqStart||"+dhslabel);
  int ns = obsconf->sendTo(*soc);
  if( ns > 0 ) 
    clog<<"UFAcqCtrl::startObs> sent: "<<obsconf->name()<<endl;
  else 
    clog<<"UFAcqCtrl::startObs> send failed: "<<obsconf->name()<<endl;

  return r;
}
// start a (test) discard observation (connect to ufedtd if necessary)
//string UFAcqCtrl::discardObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg,
//			     UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::discardObs(UFDeviceConfig* dc) {
  string edtd;
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::discardObs> no obssetup?"<<endl;
    return edtd;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::discardObs> no obsconf?"<<endl;
    return edtd;
  }
  _obssetup->_obscfg->discardObs();
  edtd = submitEdtd("AcqAbort");
  return edtd;
}

string UFAcqCtrl::discardObs(UFSocket* replicant) {
  string dhsd;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::discardObs> no dhsclient (replicant) socket?"<<endl;
    return dhsd;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::discardObs> no obssetup?"<<endl;
    return dhsd;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::discardObs> no obsconf?"<<endl;
    return dhsd;
  }

  _obssetup->_obscfg->discardObs();
  dhsd = submitDhsc(_obssetup, replicant);
  return dhsd;
}

// stop observation (connect to ufedtd if necessary)
//string UFAcqCtrl::stopObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg,
//			  UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::stopObs(UFDeviceConfig* dc) {
  string edtd;
  if( UFAcqCtrl::_obssetup == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obssetup?"<<endl;
    return edtd;
  }
  if(  UFAcqCtrl::_obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obsconf?"<<endl;
    return edtd;
  }

  UFAcqCtrl::_obssetup->_obscfg->stopObs();
  edtd = submitEdtd("AcqStop");
  return edtd;
}

string UFAcqCtrl::stopObs(UFSocket* replicant) {
  string edtd;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no dhsclient socket?"<<endl;
    return edtd;
  }
  if(  UFAcqCtrl::_obssetup == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obssetup?"<<endl;
    return edtd;
  }
  if(  UFAcqCtrl::_obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obsconf?"<<endl;
    return edtd;
  }

  _obssetup->_obscfg->stopObs();
  edtd = submitDhsc(_obssetup, replicant);
  return edtd;
}

// stop observation (connect to ufedtd if necessary) { }
//string UFAcqCtrl::abortObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg) {
string UFAcqCtrl::abortObs(UFDeviceConfig* dc) {
  string edtd;
  if( UFAcqCtrl::_obssetup == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obssetup?"<<endl;
    return edtd;
  }
  if( UFAcqCtrl::_obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obsconf?"<<endl;
    return edtd;
  }

   UFAcqCtrl::_obssetup->_obscfg->abortObs();
  edtd = submitEdtd("AcqAbort");
  return edtd;
}

string UFAcqCtrl::abortObs(UFSocket* replicant) {
  string edtd;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no dhsclient socket?"<<endl;
    return edtd;
  }
  if(  UFAcqCtrl::_obssetup == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obssetup?"<<endl;
    return edtd;
  }
  if(  UFAcqCtrl::_obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obsconf?"<<endl;
    return edtd;
  }

  UFAcqCtrl::_obssetup->_obscfg->abortObs();
  edtd = submitDhsc(_obssetup, replicant);
  return edtd;
}

// allocate & fetch/set FITS header for obbs. datalabel:
UFStrings* UFAcqCtrl::fitsHdrPrm(const string& datalabel, UFFITSheader& fh, const string& epics) {
  if( _clientFITS == 0 ) {
    clog<<"UFAcqCtrl::fitsHdrPrm> no FITS client connection?"<<endl;
    return 0;
  }
  // primary header (initial or final vals)
  UFStrings* fitsHdr = _clientFITS->fetchAllFITS(_fitsoc, 0.5, _observatory);
  if( fitsHdr ) {
    fh.add(fitsHdr);
    clog<<"UFAcqCtrl::fitsHdrPrm> Primary: "<<fitsHdr->name()<<endl;
  }
  else {
    clog<<"UFAcqCtrl::fitsHdrPrm> failed FITS fetch..."<<endl;
  }
  delete fitsHdr;
  return fh.Strings();
}

UFStrings* UFAcqCtrl::fitsHdrExt(const string& datalabel, const string& epics) {
  // extension header
  return new UFStrings("F2FITSExt");
}

int UFAcqCtrl::connectFITS(string& agenthost, const string& exclude) { 
  if( agenthost.length() < 1 && _agthost == "" )
    _agthost = agenthost = UFRuntime::hostname();
  if( _agthost != agenthost )
    _agthost = agenthost;

  UFFITSClient::AgentLoc agentLocs;
  agentLocs.clear();
  map< string, int >::iterator it = _portmap.begin();
  for( ; it != _portmap.end(); ++it ) {
    string agt = it->first;
    if( agt.find("FITS") == string::npos )
      continue;
    int port = it->second;
    //clog << "UFAcqCtrl::connectFITS> "<<agt<<", port: "<<port<<endl;
    string agthost = agenthost;
    size_t atpos = agt.find("@");
    if( atpos != string::npos )
      agthost = agt.substr(1+atpos);
    if( agt.find(exclude) == string::npos ) {
      agentLocs[agt] = new UFFITSClient::AgentLocation(agthost, port);
      //clog << "UFAcqCtrl::connectFITS> "<<agt<<", host: "<<agthost<<", port: "<<port<<endl;
    }
    else {
      clog << "UFAcqCtrl::connectFITS> exclude: "<<agt<<endl;
    }
  }
  bool block = false;
  _clientFITS = new UFFITSClient(exclude);
  clog << "UFAcqCtrl::connectFITS> connecting to other Device Agents on host: "
       <<agenthost<<" (block="<<block<<")"<<endl;
  int nfits= _clientFITS->connectAgents(agentLocs, _fitsoc, block);
  if( nfits <= 0 ) {
    clog<<"UFAcqCtrl::AgentsConnected> ERROR: no connections to other device agents !"<<endl;
  }
  else
    _connectedFITS = true;
  return nfits;
} // UFAcqCtrl::connectFITS

// AcqCtrl is a clientsocket subclass -- connected to the ufgedtd...
int UFAcqCtrl::connectEdtd(const string& clientname, string& frmhost, int frmport, bool block) {
  if( frmhost.length() < 1 && _edtfrmhost == "" )
    _edtfrmhost = frmhost = UFRuntime::hostname();
  if( _edtfrmhost != frmhost )
    _edtfrmhost = frmhost;
  string agt = "ufgedtd@" + _edtfrmhost;
  //if( frmport > 0 ) _portmap[agt] = frmport;
  //if( frmport < 0 ) _portmap[agt] = 52001;
  clog<<"UFAcqCtrl::connectEdtd> edtfrmhost: "<<_edtfrmhost<<", edtfrmport: "<<frmport<<endl;
  UFTimeStamp greet(clientname);
  int socfd = connect(frmhost, frmport, block);
  if( socfd < 0 ) {
    clog<<"UFAcqCtrl::connectEdtd> connection failed..."<<endl;
    _connectedEdt = false;
    return socfd;
  }
  clog<<"UFAcqCtrl::connectEdtd> send (clientname) greeting: "<<clientname<<endl;
  int ns = greet.sendTo(*this);
  clog<<"UFAcqCtrl::connectEdtd> sent ns: "<<ns<<", (clientname) greeting: "<<greet.name()<<endl;
  UFProtocol* reply = UFProtocol::createFrom(*this);
  if( reply == 0 ) {
    clog<<"UFAcqCtrl::connectEdtd> greeting failed..."<<endl;
    _connectedEdt = false;
    return 0;
  }
  _connectedEdt = true;
  clog<<"UFAcqCtrl::connectEdtd> greeting reply: "<<reply->name()<<endl;
  delete reply;
  return socfd;
} // UFAcqCtrl::connectEdtd

UFInts* UFAcqCtrl::simAcq(const string& name, int index) {
  return new UFInts("SimInternal", (int*)0, 2028*2048);
}

int UFAcqCtrl::fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

// float/double key value
double UFAcqCtrl::fitsFlt(const string& key, char* fitsHdr) { 
  return -1.0;
}

// string key value
string UFAcqCtrl::fitsStr(const string& key, char* fitsHdr) { 
  return "null";
}

// return FITS header from opened file, seeking to start of first data frame
int UFAcqCtrl::openFITS(const string& filename, UFStrings*& fitsHdr, 
			    int& width, int& height, int& frmtotal) {
  fitsHdr = 0;
  frmtotal = 0;
  struct stat st;
  int rs = ::stat(filename.c_str(), &st);
  if( rs < 0 ) {
    clog<<"UFAcqCtrl::openFITS> unable to stat: "<<filename<<endl;
    return 0;
  }
  int totalbytes = st.st_size;

  int fd = ::open(filename.c_str(), O_RDONLY);
  if( fd <= 0 ) {
    clog<<"UFAcqCtrl::openFITS> unable to open: "<<filename<<endl;
    return 0;
  }

  char fitshd1[80];
  char end[4]; ::memset(end, 0, sizeof(end));
  char naxis[7]; ::memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;

  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  int fitsbytes= 0; // allow 10x2880 (10 headers)?
  UFFITSheader ufits;
  do {
    ::memset(fitshd1, 0, sizeof(fitshd1));
    fitsbytes += ::read(fd, fitshd1, 80);
    memcpy(end, fitshd1, ne);
    s = end;
    memcpy(naxis, fitshd1, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, fitshd1);
    if( key == "NAXIS2" )
      h = fitsInt(key, fitshd1);
    // add entry to header
    string fitshdr = fitshd1;
    ufits.add(fitshdr);
  } while( s != "END" && fitsbytes < 10*2880 );
  if( fitsbytes >= 10*2880 && s != "END" ) {
    clog<<"UFAcqCtrl::openFITS> sorry, improper or no FITS header?"<<endl;
    ::close(fd);
    return 0;
  }
  if( w <= 0 || h <= 0 ) {
    clog<<"UFAcqCtrl::openFITS> sorry FITS header lacks NAXIS1,2"<<endl;
    ::close(fd);
    return 0;
  }
  // fits image data is Big-Endian:
  if( _verbose )
    clog<<"UFAcqCtrl::openFITS> header fitbytes= "<<fitsbytes<<", total= "<<totalbytes<<endl;
  int frmbytes = totalbytes - fitsbytes;
  frmtotal = frmbytes / (w*h*sizeof(int));
  width = w; height = h;
  if( _verbose )
    clog<<"UFAcqCtrl::openFITS> header indicates: w= "<<w<<", h= "<<h
        <<", total frames= "<<frmtotal<<endl;

  fitsHdr = ufits.Strings(filename);
  return fd;
}
  
// return FITS data from open file, seeking to next data frame
UFInts* UFAcqCtrl::seekFITSData(const int fd, const int width, const int height) {
  int* frame = new int[width*height];
  int nb = ::read(fd, frame, width*height*sizeof(int));
  if( nb < width*height*(int)sizeof(int) ) {
    clog<<"UFAcqCtrl::seekFITSData> unable to read full image data, nb= "<<nb
        <<", expected "<< width*height*sizeof(int)<<endl;
    delete frame;
    return 0;
  }    
  return new UFInts("ImageFrame", (const int*) frame);
}

/*
int UFAcqCtrl::fitsHdrs(const string& datalabel, deque< UFStrings* >& fits) {
  multimap< string, UFStrings* >::iterator it = _FITSheaders.lower_bound(datalabel);
  multimap< string, UFStrings* >::iterator stop = _FITSheaders.upper_bound(datalabel);
  fits.clear();
  while( it != stop )
    fits.push_back(it->second);

  return (int) fits.size();
}
*/
#endif // __UFAcqCtrl_cc__
