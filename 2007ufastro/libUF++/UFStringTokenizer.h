/* UFStringTokenizer.h
 * Author: Daniel Karrels dan@karrels.com
 */

#ifndef __UFStringTokenizer_h__
#define __UFStringTokenizer_h__ "$Name:  $ $Id: UFStringTokenizer.h,v 0.2 2004/09/17 19:19:12 hon Exp $" ;
#define __UFStringTokenizer_H__(arg) const char arg##StringTokenizer_h__rcsId[] = __UFStringTokenizer_h__ ;

#include	<vector>
#include	<string>
#include	<iostream>

using std::string ;
using std::vector ;

/**
 * This class provides a clean mechanism for parsing
 * C++ strings based on a given delimiter.
 * It overloads operator[], and does not affect the
 * original string passed as argument to the constructor.
 * The tokens maintained by this class are zero indexed.
 * This class is immutable.
 */
class UFStringTokenizer
{

public:

	/**
	 * Constructor receives the string to be
	 * tokenized, and the delimiter by which
	 * tokens will be generated.
	 */
	UFStringTokenizer( const string& = string( "" ), char = ' ' ) ;

	/**
	 * The destructor is a NOOP because no streams have been
	 * opened, and no memory dynamically explicitly allocated.
	 */
	virtual ~UFStringTokenizer() ;

	/**
	 * This is the type that will be used to store
	 * the tokens in the UFStringTokenizer object.
	 */
	typedef vector< string > vectorType ;

	/**
	 * This is the type of the variable used for
	 * representing the size (number of tokens) of
	 * the UFStringTokenizer object.
	 */
	typedef vectorType::size_type size_type ;

	/**
	 * Retrieve a const reference to a given token, zero-indexed.
	 * This method will assert(false) if the requested index is
	 * out of bounds.  This is for debugging, and may be conditionally
	 * compiled at a later time to throw an exception while
	 * in a production environment.
	 */
	const string&	getToken( const size_type& ) const ;

	/**
	 * This method allows UFStringTokenizer objects to be used like
	 * arrays.  This method just calls the getToken() method, and
	 * its semantics are the same.
	 */
	inline const string&	operator[]( const size_type& sub ) const
		{ return getToken( sub ) ; }

	/**
	 * Return a const reference to the original C++ string before
	 * tokenization.
	 */
	inline const string&	getOriginal() const
		{ return original ; }

	/**
	 * Return a const reference to the original C++ string before
	 * tokenization.
	 */
	inline const string&	toString() const
		{ return original ; }

	/**
	 * Return the number of tokens in this UFStringTokenizer object.
	 */
	inline size_type	size() const
		{ return array.size() ; }

	/**
	 * Return true if the UFStringTokenizer holds no tokens,
	 * false otherwise.
	 * This is equivalent to (size() == 0).
	 */
	inline bool		empty() const
		{ return array.empty() ; }

	/**
	 * Determine if the subscript argument is within the
	 * bounds [0,size()).
	 * Return true if so, false otherwise.
	 * This method works also even if the UFStringTokenizer is empty.
	 */
	inline bool		validSubscript( const size_type& sub ) const
		{ return (sub < size()) ; }

	/**
	 * This method builds and returns a C++ string starting at the given
	 * index, and continuing until the last token, placing the
	 * appropriate delimiter between each token.
	 */
	string			assemble( const size_type& ) const ;

	friend std::ostream& operator<<( std::ostream& out, const UFStringTokenizer& rhs )
		{
		size_type end = rhs.size() ;
		for( register size_type i = 0 ; i < rhs.size() ; ++i )
			{
			out	<< rhs.array[ i ] ;
			if( (i + 1) < end )
				{
				out << rhs.delimiter ;
				}
			}
		return out ;
		}

protected:

	/**
	 * Disable the copy constructor.  This method is declared
	 * but NOT defined.
	 */
	UFStringTokenizer( const UFStringTokenizer& ) ;

	/**
	 * Protected method called internally by the constructor once
	 * at object instantiation to tokenize the given C++ string.
	 */
	virtual void		Tokenize( const string& ) ;

	/**
	 * The non-tokenized string.
	 */
	const string		original ;

	/**
	 * The delimiter by which the (original) string is tokenized.
	 */
	const char		delimiter ;

	/**
	 * The structure for holding the tokens.
	 */
	vectorType		array ;

} ;

#endif /* __UFStringTokenizer_h__ */
