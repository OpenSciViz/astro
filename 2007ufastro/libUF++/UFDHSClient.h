#if !defined(__UFDHSClient_h__)
#define __UFDHSClient_h__ "$Name:  $ $Id: UFDHSClient.h,v 0.1 2004/09/17 19:19:09 hon Exp $"
#define __UFDHSClient_H__(arg) const char arg##DHSClient_h__rcsId[] = __UFDHSClient_h__;

#include "UFDaemon.h"
#include "string"

class UFDHSClient : public UFDaemon {
public:
  inline UFDHSClient(const string& name) : UFDaemon(name) {}
  inline UFDHSClient(int argc, char** argv) : UFDaemon(argc, argv) {}
  inline UFDHSClient(const string& name, int argc, char** argv) : UFDaemon(name, argc, argv) {}
  inline virtual ~UFDHSClient() {}

  inline virtual string description() const { return __UFDHSClient_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p=0);
};

#endif // __UFDHSClient_h__
