#if !defined(__UFEdtDMA_cc__)
#define __UFEdtDMA_cc__ "$Name:  $ $Id: UFEdtDMA.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFEdtDMA_cc__;

#include "UFEdtDMA.h"
__UFEdtDMA_H__(__UFEdtDMA_cc);

#include "UFRuntime.h"
__UFRuntime_H__(__UFEdtDMA_cc);

#include "UFPSem.h"
__UFPSem_H__(__UFEdtDMA_cc);

#include "sys/types.h"
#include "sys/stat.h"
#include "sys/mman.h"
#include "sys/wait.h"
#include "string.h"
#include "time.h"
#include "cstdio"
#include "fstream"
#include "strstream"
#include "unistd.h"
#include "fcntl.h"

#if defined(_NO_EDT_)
int edt_done_count(...) { return 0; }
int edt_timeouts(...) { return 0; }
int edt_reg_write(...) { return 0; }
PdvDev* pdv_open(...) { return new PdvDev(); }
PdvDev* pdv_open_channel(...) { return new PdvDev(); }
int pdv_close(...) { return 0; }
int pdv_set_timeout(...) { return 0; }
int pdv_multibuf(...) { return 0; }
int pdv_set_buffers(...) { return 0; }
int pdv_start_images(...) { return 0; } 
unsigned char* pdv_wait_images(...) { return 0; } 
int pdv_setup_continuous(...) { return 0; } 
int pdv_slop(...) { return 0; } 
int pdv_image_size(...) { return 0; } 
#endif // _NO_EDT_

// global verbose
bool UFEdtDMA::_verbose = false;

// to mmap or not is the question
bool UFEdtDMA::_usemmap = false;

// global show timer/clock
bool UFEdtDMA::_timer = false;

// global lut file option
int* UFEdtDMA::_lut= 0;
string UFEdtDMA::_lutfile;
UFPosixRuntime::MMapFile UFEdtDMA::_lutmmap;

// global fits header file option
char* UFEdtDMA::_fitshdr= 0;
string UFEdtDMA::_fhdrfile;
UFPosixRuntime::MMapFile UFEdtDMA::_fitshdrmmap;

// global ref. file option
int* UFEdtDMA::_ref= 0;
string UFEdtDMA::_reffile;
UFPosixRuntime::MMapFile UFEdtDMA::_refmmap;

// reusable takeAndStore buffers:
int UFEdtDMA::_nbuf= 0;
unsigned char* UFEdtDMA::_imgbuf= 0;
unsigned char** UFEdtDMA::_frms= 0;

// global device ptr re-usable by all funcs:
UFEdtDMA::DevPtr UFEdtDMA::_theDevp= 0;

// current conf:
UFEdtDMA::Conf* UFEdtDMA::_edtcfg = 0;

// global seq. cnt for modulo
int UFEdtDMA::_dpySeq = 1;

// global timeout option
int UFEdtDMA::_timeOut = 0;
int UFEdtDMA::_timeOutCnt = 0;

// simulate exposure time via sleep:
int UFEdtDMA::_simExpTime = 0;

// global rename option
bool UFEdtDMA::_rename = false;

// turm-off semaphores, swapping
bool UFEdtDMA::_nosem = false;

// edt device driver swap option
bool UFEdtDMA::_noswap = false;

// is mempry mapped lut little-endian?
bool UFEdtDMA::_lutLE = false;

// global edtdev lock file 
string UFEdtDMA::_edtlockfile = "/tmp/.ufEdtDevOpen";
// global startdma/imgwait lock file option
string UFEdtDMA::_lockfile = "/tmp/.ufActiveEdtDMA";

// instrument/detector array name (for simulation) option:
string UFEdtDMA::_instrum= "flamingos";

// global semaphores provide "idle" boolean along with
// current frame take count (edt_done_cnt)
// and expected total count for observation
// posix sem names <= 14 chars and must begin with "/" 
// and have only one "/"!
string UFEdtDMA::_semNameAcqCnt = "/ufAcqCntSm";
string UFEdtDMA::_semNameAcqTimeOut = "/ufAcqTimOutSm"; 
string UFEdtDMA::_semNameFrmTakeIdle = "/ufFrmTkIdleSm";
string UFEdtDMA::_semNameLutApply = "/ufLutApplySm";
string UFEdtDMA::_semNameLutId = "/ufLutIdSm";
string UFEdtDMA::_semNameFrmCnt = "/ufFrmCntSm";
string UFEdtDMA::_semNameFrmTotCnt = "/ufFrmTotCntSm";
string UFEdtDMA::_semNameFrmWidth = "/ufFrmWidthSm";
string UFEdtDMA::_semNameFrmHeight = "/ufFrmHeightSm";

// since the above initializations evidently don't work on linux:
#if defined(LINUX)
static char *_xclock = "/usr/X11R6/bin/xclock -d -update 1";
static bool _setNames = false;
#else
static char *_xclock = "/usr/openwin/bin/xclock -d -update 1";
static bool _setNames = true;
#endif

int UFEdtDMA::height() {
  int h= 0;
  if( UFEdtDMA::_edtcfg == 0 )
    UFEdtDMA::_edtcfg = UFEdtDMA::getConf();
  if( UFEdtDMA::_edtcfg != 0 )
    h = UFEdtDMA::_edtcfg->h;

  return h;
}

int UFEdtDMA::width() {
  int w= 0;
  if( UFEdtDMA::_edtcfg == 0 )
    UFEdtDMA::_edtcfg = UFEdtDMA::getConf();
  if( UFEdtDMA::_edtcfg != 0 )
    w = UFEdtDMA::_edtcfg->w;

  return w;
}
	
int UFEdtDMA::dimensions(int& w, int& h) {
  w = h = 0;

  if( UFEdtDMA::_edtcfg == 0 ) 
    UFEdtDMA::_edtcfg = UFEdtDMA::getConf();
  if( UFEdtDMA::_edtcfg != 0 ) {
    w = UFEdtDMA::_edtcfg->w; h = UFEdtDMA::_edtcfg->h;
  }

  return w*h;
}

void UFEdtDMA::_semNames() {
  // UFPSem::_verbose = true;
  if( _setNames )
    return;

  UFEdtDMA::_semNameAcqCnt = "/ufAcqCntSm";
  UFEdtDMA::_semNameAcqTimeOut = "/ufAcqTimOutSm";
  UFEdtDMA::_semNameFrmTakeIdle = "/ufFrmTkIdleSm";
  UFEdtDMA::_semNameLutApply = "/ufLutApplySm";
  UFEdtDMA::_semNameLutId = "/ufLutIdSm";
  UFEdtDMA::_semNameFrmCnt = "/ufFrmCntSm";
  UFEdtDMA::_semNameFrmTotCnt = "/ufFrmTotCntSm";
  UFEdtDMA::_semNameFrmWidth = "/ufFrmWidthSm";
  UFEdtDMA::_semNameFrmHeight = "/ufFrmHeightSm";

  _setNames = true;
  return;
}

// delete all semaphores 
void UFEdtDMA::_semRmAll() {
  _semNames();
  //UFPosixRuntime::_verbose = true;
  UFPosixRuntime::semDelete(_semNameAcqCnt);
  UFPosixRuntime::semDelete(_semNameAcqTimeOut);
  UFPosixRuntime::semDelete(_semNameFrmTakeIdle);
  UFPosixRuntime::semDelete(_semNameLutApply);
  UFPosixRuntime::semDelete(_semNameLutId);
  UFPosixRuntime::semDelete(_semNameFrmCnt);
  UFPosixRuntime::semDelete(_semNameFrmTotCnt);
  UFPosixRuntime::semDelete(_semNameFrmWidth);
  UFPosixRuntime::semDelete(_semNameFrmHeight);
  //UFPosixRuntime::_verbose = false;
}

// insure all semaphores exits (create/open then close)
void UFEdtDMA::_semInitAll(int frmtot, UFEdtDMA::Conf* edtc) {
  //UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  _semNames();

  // should be re-inited or created:
  // (create forces re-init if sem. exists)
  UFPosixRuntime::semDelete(_semNameAcqTimeOut);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameAcqTimeOut));

  UFPosixRuntime::semDelete(_semNameFrmTakeIdle);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmTakeIdle));

  UFPosixRuntime::semDelete(_semNameLutApply);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameLutApply));
  UFPosixRuntime::semDelete(_semNameLutId);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameLutId));

  UFPosixRuntime::semDelete(_semNameFrmCnt);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmCnt));

  // these may init to non-zero:
  //UFPosixRuntime::semDelete(_semNameFrmWidth);
  //UFPosixRuntime::semDelete(_semNameFrmHeight);
  if( edtc ) {
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmWidth, edtc->w));
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmHeight, edtc->h));
  }
  else {
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmWidth));
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmHeight));
  }

  // this one should init to non-zero:
  UFPosixRuntime::semDelete(_semNameFrmTotCnt);
  UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmTotCnt, frmtot));

  // the total acq. cnt should not reinitilized
  sem_t* sem = UFPosixRuntime::semAttach(_semNameAcqCnt);
  if( sem == SEM_FAILED || (long) sem <= 0 )
    sem = UFPosixRuntime::semCreate(_semNameAcqCnt);

  UFPosixRuntime::semClose(sem);
}

int UFEdtDMA::little2BigEnd(unsigned char* little, unsigned char* big, UFEdtDMA::Conf* c) {
  int i, elem = c->w * c->h;
  if( c->d == 32 ) {
    // if the two buffer pointers passed in are pointing to the same memory, use tmp:
    unsigned char tmp[4]; 
    for( i = 0; i < 4*elem; i += 4 ) {
      tmp[0] = little[i+3]; tmp[1] = little[i+2]; tmp[2] = little[i+1]; tmp[3] = little[i];
      big[i] = tmp[0]; big[i+1] = tmp[1]; big[i+2] = tmp[2]; big[i+3] = tmp[3];
    }
    //if( _verbose )
      clog<<"UFEdtDMA::little2BigEnd> completed 32bit swap..."<<endl;
  }
  else if( c->d == 16 ) {
    unsigned char tmp[2];
    for( i = 0; i < 2*elem; i += 2 ) {
      tmp[0] = little[i+1]; tmp[1] = little[i];
      big[i] = tmp[0]; big[i+1] = tmp[1];
    }
    //if( _verbose )
      clog<<"UFEdtDMA::little2BigEnd> completed 16bit swap..."<<endl;
  }
  else {
    clog<<"UFEdtDMA::little2BigEnd> ? bad config ? bits= "<<c->d<<endl;
    elem = -1;
  }
  return elem;
}

bool UFEdtDMA::timedOut(int& tocnt) {
  if( _theDevp == 0 )
    return false;

  tocnt = ::edt_timeouts(_theDevp);

  if( tocnt >  _timeOutCnt ) {
    _timeOutCnt = tocnt;
    return true;
  }
  return false;
}

UFEdtDMA::DevPtr UFEdtDMA::openDev() {
  if( UFEdtDMA::_edtlockfile.empty() )
    UFEdtDMA::_edtlockfile = "/tmp/.ufEdtDevOpen";
  if( UFEdtDMA::_lockfile.empty() )
    UFEdtDMA::_lockfile = "/tmp/.ufActiveEdtDMA";

  if( !_edtlockfile.empty() ) {
    if( UFRuntime::checkLockFile(_edtlockfile) > 0 ) {
      clog<<"UFEdtDMA::openDev> aborting, found edt dev. lock: "<<_edtlockfile<<endl;
      return 0;
    }
  }
  if( !_lockfile.empty() ) {
    if( UFRuntime::checkLockFile(_lockfile) > 0 ) {
      ::remove(_lockfile.c_str());
      clog<<"UFEdtDMA::openDev> found and removed stale lock: "<<_lockfile<<endl;
    }
  }
  // indicate device has been opened via file-system entry:
  mode_t mt = ::umask(0);
  int fd = ::creat(_edtlockfile.c_str(), 0666); 
  if( fd >= 0 )
    ::close(fd);
  else
    clog<<"UFEdtDMA::openDev> unable to creat lock: "<<UFEdtDMA::_edtlockfile<<" "<<strerror(errno)<<endl;
  ::umask(mt);

  _theDevp = ::pdv_open("/dev/pdv0", 0);
  if( _theDevp <= 0 ) {
    clog<<"UFEdtDMA::openDev> pdv_open failed, try pdv_open_channel..."<<endl;
    _theDevp = ::pdv_open_channel("/dev/pdv0", 0, 0);
    if( _theDevp <= 0 ) {
      if( !_edtlockfile.empty() ) ::remove(_edtlockfile.c_str());
      return 0;
    }
  }

  // only root can do this?
  //int mlck = ::mlockall(MCL_CURRENT | MCL_FUTURE);
  //if( mlck < 0 ) clog<<"UFEdtDMA::openDev> mlockall failed: "<<strerror(errno)<<endl;
  
  if( _edtcfg == 0 )
    _edtcfg = getConf(_theDevp);

  _timeOutCnt = edt_timeouts(_theDevp);
  clog<<"UFEdtDMA::openDev> _timeOutCnt: "<<_timeOutCnt<<endl;
  if( !UFRuntime::littleEndian() ) { // big endian system
    // edt card defaults to little endian, tell it to produce big endian pixels via firmware byteswap
    /*
    clog<<"UFEdtDMA::openDev> BigEndian system, setting EDTDEV byteswap to 1..."<<endl;
    _theDevp->dd_p->byteswap = 1;
    // set Byteswap/ Hwpad 
    int padword = _theDevp->dd_p->hwpad << 1;
    padword |= PDV_BSWAP;
    //::edt_msg(DEBUG2, "PAD_SWAP %x\n", padword);
    //::dep_wait(_theDevp);
    ::edt_reg_write(_theDevp, PDV_BYTESWAP, padword);
    _edtcfg->littleEnd = false;
    clog<<"UFEdtDMA::openDev> Ok, set LittleEndian to false..."<<endl;
    */
    _edtcfg->littleEnd = true;
    clog<<"UFEdtDMA::openDev> BigEndian system, EDT output is LittleEndian?"<<endl;
  }
  return _theDevp;
}

int UFEdtDMA::closeDev() {
  if( !_lockfile.empty() ) {
    ::remove(_lockfile.c_str());
    clog<<"UFEdtDMA::closeDev> removed lockfile: "<<_lockfile<<endl;
  }
  if( !_edtlockfile.empty() ) {
    ::remove(_edtlockfile.c_str());
    clog<<"UFEdtDMA::closeDev> removed lockfile: "<<_edtlockfile<<endl;
  }
  
  //int mlck = ::munlockall();
  //if( mlck < 0 ) clog<<"UFEdtDMA::closeDev> munlockall failed: "<<strerror(errno)<<endl;
  
  if( _theDevp <= 0 )
    return -1;

  int stat = ::pdv_close(_theDevp);
  _theDevp = 0;

  if( !_nosem )
    UFPosixRuntime::semPost(_semNameFrmTakeIdle); // idle > 0 (true)

  return stat;
}

unsigned char** UFEdtDMA::allocBufs(int nbuf, UFEdtDMA::Conf* c, int& sz) {
  int elem = c->w * c->h;
  sz = elem * c->d/8; // always return allocation size as byte count
  // consider using an anonymous mmap?
  if( _nbuf != 0 || _frms != 0 ) // one-time allocation
    return _frms;

  _nbuf = nbuf;
  unsigned char** buffers = _frms = new unsigned char*[nbuf];
  switch(c->d) {
  case 8:
    for( int i = 0; i < nbuf; ++i )
      buffers[i] = (unsigned char*)new char[elem];
    sz = elem * sizeof(char);
    break;
  case 16:
    for( int i = 0; i < nbuf; ++i )
      buffers[i] = (unsigned char*)new short[elem];
    sz = elem * sizeof(short);
    break;
  case 32: // default
  default:
    for( int i = 0; i < nbuf; ++i )
      buffers[i] = (unsigned char*)new int[elem];
    sz = elem * sizeof(int);
    break;
  }
  return buffers;
}

int UFEdtDMA::freeBufs(int nbuf, unsigned char**& bufs) {
  int i= 0;
  for(; i < nbuf; ++i ) {
    delete [] bufs[i];
  }
  delete [] bufs; bufs = 0;
  return i;
}

int UFEdtDMA::startDMA(int numbufs, unsigned char** buffers, int numFrms) {
  if( _theDevp == 0 ) {
    closeDev(); // always close & (re)open edt device?
    UFEdtDMA::DevPtr devp = openDev();
    if( devp == 0 ) {
      cerr<<"UFEdtDMA::startDMA> Failed to open default edt pdv device..."<<endl;
      return -1;
    }
  } // _theDevp should now be set...

  if( !UFEdtDMA::_lockfile.empty() ) {
    mode_t mt = ::umask(0);
    int fd = ::creat(UFEdtDMA::_lockfile.c_str(), 0666); 
    ::umask(mt);
    if( fd > 0 ) ::close(fd);
  }
  if( !_nosem )
    _semInitAll(numFrms, _edtcfg);

  int msec = 1000 * resetAcqTimeOut(_timeOut);  // millisec. ( 3000 == 3 sec. )
  ::pdv_set_timeout(_theDevp, msec); // infinite timeout == 0
  ::pdv_setup_continuous(_theDevp);
  int stat= -1, sz= 0, slop= 0;
  if( buffers != 0 ) {
    sz = ::pdv_image_size(_theDevp);
    slop = ::pdv_slop(_theDevp);
    if( _verbose ) clog<<"UFEdtDMA::startDMA> size= "<<sz<<", slop= "<<slop<<endl;
    if( slop != 0 )
      clog<<"UFEdtDMA::startDMA> non page-aligned buffers? slop= "<<slop<<endl;
    stat = ::pdv_set_buffers(_theDevp, numbufs, buffers);
  }
  else { // let dev. driver alloc
    stat = ::pdv_multibuf(_theDevp, numbufs);
  }
  if( stat < 0 ) {
    clog<<"UFEdtDMA::startDMA> pdv_multibuf set failed, numbufs= "<<numbufs<<", buffers= "<<(size_t) buffers<<endl;
  }

  int donecnt = 0;
  //::pdv_start_images(_theDevp, 1); // stop any prior start?
  //donecnt = ::edt_done_count(_theDevp);
  //clog<<"UFEdtDMA::startDMA> (start 1) donecnt= "<<donecnt<<endl;
  //::pdv_start_images(_theDevp, 0);  // numFrms == 0 indicates free-run forever
  ::pdv_start_images(_theDevp, numFrms);  // numFrms == 0 indicates free-run forever
  donecnt = ::edt_done_count(_theDevp);
  //clog<<"UFEdtDMA::startDMA> (start 0) donecnt= "<<donecnt<<endl;
  clog<<"UFEdtDMA::startDMA> (start "<<numFrms<<") donecnt= "<<donecnt<<endl;
  return donecnt;
}

unsigned char* UFEdtDMA::waitImages(int n, const string* semname) {
  if( _theDevp <= 0  ) {
    clog<<"UFEdtDMA::waitImages> Edt dev. not open?"<<endl;
    return 0;
  }
  int sec = acqTimeOut();
  if( sec != _timeOut ) {
    int msec = 1000 * sec;
    ::pdv_set_timeout(_theDevp, msec); // infinite timeout == 0
  }

  if( !_nosem ) {
    // indicate that blacking wait on take image is started:
    UFPosixRuntime::semClear(_semNameFrmTakeIdle); // idle == 0 (false)
    // lut not (yet) applied
    //UFPosixRuntime::semClear(_semNameLutId);
  }

  if( _verbose ) {
    string t = "[" + UFRuntime::currentTime() + "]";
    clog<<"UFEdtDMA::waitImages> "<<t<<" waiting for frame, timeout: "<<sec<<endl;
  }
  unsigned char* buf = 0;
  buf = ::pdv_wait_images(_theDevp, n);

  // if we're keeping count of a multi-frame take:
  if( !_nosem ) { // take complete?
    UFPosixRuntime::semPost(_semNameFrmTakeIdle); // idle > 0 (true)
    if( buf && semname ) {
      UFPosixRuntime::semPost((string&)*semname); // increment counter for current observation
      UFPosixRuntime::semPost(_semNameAcqCnt); // increment total/program acq. counter
    }
  }

  if( _verbose ) {
    string t = "[" + UFRuntime::currentTime() + "]";
    clog<<"UFEdtDMA::waitImages> "<<t<<" done waiting for frame."<<endl;
  }

  return buf;
} // waitImages

int UFEdtDMA::doneCnt() {
  if( _theDevp )
    return ::edt_done_count(_theDevp);

  return -1;
}

// this static is of use to other procs:
int UFEdtDMA::acqCnt() {
  if( _nosem ) 
    return 0;
  else
    _semNames();

  int cnt = UFPosixRuntime::semValue(_semNameAcqCnt);
  if( cnt == INT_MAX ) // getvalue failed
    return -1;
  return cnt;
}

// this static is of use to other procs:
int UFEdtDMA::acqTimeOut() {
  if( _nosem ) 
    return _timeOut;
  else
    _semNames();

  int val = UFPosixRuntime::semValue(_semNameAcqTimeOut);
  if( val == INT_MAX ) // getvalue failed
    return -1;
  return val;
}

// this static is of use to other procs:
int UFEdtDMA::resetAcqTimeOut(int val) {
  if( _nosem ) {
    _timeOut = val;
    return val;
  }
  else
    _semNames();

  sem_t* semAcqTimeOut = UFPosixRuntime::semCreate(_semNameAcqTimeOut, val);
  // since it's already open:
  int semval= 0, stat = UFPSem::getvalue(semAcqTimeOut, semval);
  if( stat < 0 )
    clog<<"UFEdtDMA::acqCnt> sem_getvalue failed for: "<<_semNameLutApply<<", "<<strerror(errno)<<endl;
  UFPosixRuntime::semClose(semAcqTimeOut);
  return semval;
}

bool UFEdtDMA::applyingLut(int& Id) {
  Id = 0;
  if( UFRuntime::checkLockFile(UFEdtDMA::_lockfile) > 0 )
    return true;
  else if( _nosem ) 
    return false;

  _semNames();

  int semval= UFPosixRuntime::semValue(_semNameLutApply);
  Id = UFPosixRuntime::semValue(_semNameLutId);
  if( Id <= 0 || Id == INT_MAX )
    Id = 0;
  if( semval == 0 || semval == INT_MAX )
    return false;

  return true;
}

void UFEdtDMA::takeAbort(pid_t takepid) {
  if( takepid > 0 ) {
    ::kill(takepid, SIGTERM);
    int status;
    ::waitpid(takepid, &status, WNOHANG);
  }
  
  // delete the dev. open lock file
  if( !_edtlockfile.empty() ) ::remove(_edtlockfile.c_str());
  // delete the waitImage lock file
  if( !_lockfile.empty() ) ::remove(_lockfile.c_str());

  if( _nosem )
    return;
  else
    _semNames();

  // clear semaphores
  UFPosixRuntime::semClear(_semNameAcqTimeOut);
  UFPosixRuntime::semClear(_semNameFrmCnt);
  UFPosixRuntime::semClear(_semNameFrmTotCnt);
  //UFPosixRuntime::semClear(_semNameFrmTakeIdle);
  UFPosixRuntime::semClear(_semNameLutId);
  UFPosixRuntime::semClear(_semNameLutApply);
}

// these static is of use to multiple procs:
bool UFEdtDMA::takeIdle(int& width, int& height) {
  width = height = 0;
  int s = UFRuntime::checkLockFile(_edtlockfile);
  if( s <= 0 ) { // lock file does not exist -- idle?
    if( _verbose )
      clog<<"UFEdtDMA::takeIdle> "<<_edtlockfile<<" "<<strerror(errno)<<endl;
    return true;
  }
  s = UFRuntime::checkLockFile(_lockfile);
  if( s <= 0 ) { // lock file does not exist -- idle?
    if( _verbose )
      clog<<"UFEdtDMA::takeIdle> "<<_lockfile<<" "<<strerror(errno)<<endl;
    return true;
  }

  if( _nosem ) // if there are lock files present, and we are not using semaphores.....
    return false; // assume the observation still in progress
  else
    _semNames();

  // get frame dimensions, if available (from current or prior take?)
  width = UFPosixRuntime::semValue(_semNameFrmWidth);
  if( width <= 0 || width == INT_MAX )
    width = 0;

  height = UFPosixRuntime::semValue(_semNameFrmHeight);
  if( height <= 0 || height == INT_MAX )
    height = 0;

  int cnt = UFPosixRuntime::semValue(_semNameFrmTakeIdle);
  if( cnt > 0 ) // getvalue failed
    return true;

  return false;
}

// this static is of use to other procs:
int UFEdtDMA::takeCnt() {
  if( _nosem )
    return 0;
  else
    _semNames();

  int cnt = UFPosixRuntime::semValue(_semNameFrmCnt);
  if( cnt == INT_MAX ) // getvalue failed
    return -1;
  return cnt;
}

// this static is of use to other procs:
int UFEdtDMA::takeTotCnt() {
  if( _nosem )
    return 0;
  else
    _semNames();

  int cnt = UFPosixRuntime::semValue(_semNameFrmTotCnt);
  if( cnt == INT_MAX ) // getvalue failed
    return -1;
  return cnt;
}

UFEdtDMA::Conf* UFEdtDMA::getConf(UFEdtDMA::DevPtr edtp) {
  if( _edtcfg == 0 )
    _edtcfg = new UFEdtDMA::Conf(); // F2 default ctor

  UFEdtDMA::Conf* c = _edtcfg;

  const char* logfile = "/usr/tmp/.ufEdtConf.log";
#if defined(_NO_EDT_) 
  // F2 default is 2048x2048x32 BigEndian (sparc)
  _edtcfg->littleEnd = UFRuntime::littleEndian(); // but this may be an intel cpu...
  clog<<"UFEdtDMA::getConf> simpdv: "<<c->w<<"x"<<c->h<<"x"<<c->d<< endl;
#else // edt device present
  char pdv[] = "/dev/pdv0";
  PdvDev *pdv_p = edtp;
  if( pdv_p == 0 ) // temporarily open & inspect & close device:
    pdv_p = ::pdv_open(pdv, 0);
  if( pdv_p == 0 ) {
    clog<<"UFEdtDMA::getDeviceConf> pdv_open failed, try pdv_open_channel..."<<endl;
    pdv_p = ::pdv_open_channel("/dev/pdv0", 0, 0);
    if(  pdv_p == 0 ) return 0; // or 0?
  }
  c->w = ::pdv_get_width(pdv_p);
  c->h = ::pdv_get_height(pdv_p);
  c->d = ::pdv_get_depth(pdv_p);
  clog<<"UFEdtDMA::getConf> "<<pdv<<": "<<c->w<<"x"<<c->h<<"x"<<c->d<< endl;
  if( edtp == 0 )
    ::pdv_close(pdv_p);
#endif
  // log it?
  if( _verbose ) {
    mode_t mt = ::umask(0);
    //ofstream oftmp(logfile, ios::in | ios::out, 0666);
    ofstream oftmp(logfile, ios::in | ios::out);
    if( oftmp.bad() ) {
      clog<<"UFEdtDMA::getConf> failed to open "<<logfile<<endl;
    }
    else {
      int endianL = 1;
      if( !c->littleEnd ) {
        endianL = 0;
        oftmp <<c->w<<" "<<c->h<<" "<< c->d << " (big-endian) @ " << UFRuntime::currentTime() << endl;
      }
      else {
        oftmp <<c->w<<" "<<c->h<<" "<< c->d << " (little-endian) @ " << UFRuntime::currentTime() << endl;
      }
      oftmp.close();
    }
    ::umask(mt);
  }
  // post semaphores?
  if( c && !_nosem ) {
    if( !_setNames ) 
      _semNames();
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmWidth, c->w));
    UFPosixRuntime::semClose(UFPosixRuntime::semCreate(_semNameFrmHeight, c->h));
  }

  return c;
}

string UFEdtDMA::mkname(const string& filehint, int index0, int cnt, bool hidden) {
  string filenm = filehint;

  if( hidden ) { // initial name of hidden file
    int slshpos = filehint.rfind("/");
    if( slshpos == (int)string::npos )
      filenm = "." + filehint; // + t;
    else
      filenm = filehint.substr(0,1+slshpos) + "." + filehint.substr(1+slshpos); // + t;
  }

  // this is useful for really long observation/test sequences
  int index = cnt + index0;
  strstream sc;
  if( index < 10 )
    sc << "000" << index << ends;
  else if( index < 100 )
    sc << "00" << index << ends;
  else if( index < 1000 )
    sc << "0" << index << ends;
  else
    sc << index << ends;

  string ext(".");
  ext += sc.str(); delete sc.str();
  int fpos = filenm.rfind(".fits");
  if( fpos != (int)string::npos ) {
    ext += ".fits";
    filenm = filenm.substr(0, fpos) + ext;
  }
  else {
    filenm += ext;
  }
  // finally, filename really should be a full path, if not, preped cwd:
  if( filenm.find("/") != 0 ) {
    string cwd = UFRuntime::cwdPath();
    filenm = cwd + "/" + filenm;
  }

  return filenm;
}

int UFEdtDMA::writeTo(const string& filenm, const char* imgfits, int total_sz, int pad) {
  int fd = ::creat(filenm.c_str(), 0644);
  if( fd < 0 ) { 
    clog<<"UFEdtDMA::writeTo> unable to creat: "<<filenm<<endl;
    return -1;
  }
  int nb = ::write(fd, imgfits, total_sz);
  if( nb < total_sz )
    clog<<"UFEdtDMA::writeTo> unable to write full image, nb: "<<nb<<", total: "<<total_sz<<endl;
  else
    clog<<"UFFEdtDMA::writeTo> fits data file: "<<filenm<<endl;    
  if( pad <= 0 )
    return ::close(fd);

  char padding[pad]; ::memset(padding, 0, sizeof(padding));
  nb = ::write(fd, padding, pad);

  return ::close(fd);
}

// for unit tests:
// loosely based on edt's take example:
// optimizes file i/o via posix mmap
int UFEdtDMA::takeAndStore(int nf, int index0, char* filehint, int* lut,
			   char* fits, int fits_sz, PostAcq postacq, AcqFunc simacq) {
  UFEdtDMA::Conf* c = getConf(); // should be non-0 for configure edt dev.

  if( c == 0 && simacq == 0 ) {
    clog<<"UFEdtDMA::takeAndStore> Not simulation, but No EDT config! aborting..."<<endl;
    return -1;
  }

  clog<<"UFEdtDMA::takeAndStore> edt conf: "<<c->w<<"x"<<c->h<<endl;

  string filehnt;
  if( filehint ) {
    filehnt = filehint; // save copy of filehint
  }
  else {
    strstream s;
    s<<".edtdma"<<c->w<<"x"<<c->h<<ends;
    filehnt =  s.str(); delete s.str();
  }
  if( fits && filehnt.find(".fits") == string::npos ) {
    //if( filehnt.rfind(".") == filehnt.length()-1 )
    //  filehnt += "fits"; 
    //else
      filehnt += ".fits";
  }
  int nbuf= 4, sz= 0, cnt= 0;
  // allocBuffs should re-use one-time allocation
  unsigned char** frms = allocBufs(nbuf, c, sz); // set sz to c->w*c->h*sizeof(int)
  if( _verbose )
    clog<<"UFEdtDMA::takeAndStore> nbuf: "<<nbuf<<", sz: "<<sz<<", w: "<<c->w<<", h: "<<c->h<<endl;

  int total_sz = sz;
  if( fits )
    total_sz += fits_sz; // size with fits header (in bytes)

  double fitsunit = 36.0*80.0;
  //int pad = (int)(::ceil(sz / (fitsunit)))*(fitsunit) - sz; // number of bytes to pad out to integral 2880
  // this is testable in perl since perl's int() is equiv. to ::floor()
  // number of bytes to pad out to integral 2880:
  int pad = (int) ::floor((sz/(fitsunit) - ::floor(sz/fitsunit))*(fitsunit)); 

  int fits_szi = fits_sz/sizeof(int);
  const int total_szp = (total_sz+pad);
  int edtDone= 0;
  if( simacq == 0 ) {
    edtDone = startDMA(nbuf, frms, nf); // one-time call to openDev()
    if( edtDone < 0 ) {
      clog<<"UFEdtDMA::> startDMA failed (no EDT sync?)"<<endl;
      closeDev();
      return edtDone;
    }
  }
  else { 
    // since the above startDMA (via openDev) would have init'd the semaphores, and the edtdev lockfile
    mode_t mt = ::umask(0);
    int fd = ::creat(_edtlockfile.c_str(), 0666); 
    if( fd > 0 ) ::close(fd);
    ::umask(mt);
    if( !_nosem )
      _semInitAll(nf, c);
  }

  // imgfits should point to the start of mmap (or not) of fits buff
  unsigned int *imgfits = 0; //unsigned char imgbuf[total_szp];
  if( !_usemmap ) {
    if( UFEdtDMA::_imgbuf == 0 ) UFEdtDMA::_imgbuf = new unsigned char [total_sz];
    //clog<<"UFEdtDMA::> clear the imgbuffer so that timed-out frames don't contain old data."<<endl;
    ::memset(UFEdtDMA::_imgbuf, 0, total_sz);
    //clog<<"UFEdtDMA::> cleared the imgbuffer so that timed-out frames don't contain old data."<<endl;
    imgfits = (unsigned int*) &UFEdtDMA::_imgbuf[0];
  }

  string t = UFRuntime::currentTime();;
  if( _verbose )
    clog<<"UFEdtDMA::> startDMA to "<<filehnt<<"..., frmCnt= "<<nf<<" (WxH = "<<c->w<<"x"<<c->h<<") at t= "<<t<<endl;

  // acquire frame(s) & optionally display and post-process them:
  while( true ) {
    string filenm = mkname(filehnt, index0, cnt, _rename);
    // imgfits should point to the start of mmap (heap space) of fits buff
    UFPosixRuntime::MMapFile mf;
    if( _usemmap ) {
      mf = UFPosixRuntime::mmapFileCreate(filenm, total_szp);
      if( mf.fd < 0 ) {
        clog<<"UFEdtDMA::takeAndStore> unable to mmap "<<filenm<<endl;
        closeDev();
        return cnt;
      }
      ::memset(mf.buf, 0, total_szp);
      // imgfits points to the start of mmap of fits file
      imgfits = (unsigned int*)mf.buf;
    }

    // pixfits points to the first image pixel of fits file
    unsigned int* pixfits = imgfits;
    int status= 0;
    if( fits && imgfits ) { // prepend fits template
      pixfits = imgfits + fits_szi; // pixfits points to the first image pixel (after any header)
      if( _verbose )
	clog<<"UFEdtDMA::> prepending fits header size= "<<fits_sz<<", total file size= "<<total_sz<<endl;
      ::memcpy(imgfits, fits, fits_sz);
    } 

    pid_t pidtimer= (pid_t)-1;
    if( _timer )
      pidtimer = UFRuntime::execApp(_xclock);

    // waitImage can return null if device is dead/not open.
    // post (increment) optional semaphore upon successfull frame capture: 
    // raw image data from mce4 is 32bit littleEndian:
    unsigned char* frm= 0;
    bool timedout = false;
    int tocnt= 0;

    if( simacq == 0 ) {
      // startDMA creates _lockfile, do not remove until after postacq func. or stop/abort
      if( _nosem )
        frm = waitImages(1);
      else
        frm = waitImages(1, &_semNameFrmCnt);

      if( frm == 0 ) {
        clog<<"UFEdtDMA::>waitImages returned null, continuing..."<<endl;
        continue;
      }
      edtDone = doneCnt();
      timedout = timedOut(tocnt); 
      ++cnt;
    }
    else {
      if( !_nosem ) { // indicate that blocking wait on take image is started:
        UFPosixRuntime::semClear(_semNameFrmTakeIdle); // idle == 0 (false)
      }
      UFPosixRuntime::sleep(0.5);
      frm = frms[cnt % nbuf];
      (*simacq)( frm, (char*) imgfits, total_sz, filenm.c_str() );
      edtDone = ++cnt;
      // if we're keeping count of a multi-frame take:
      if( !_nosem ) { // take complete?
        UFPosixRuntime::semPost(_semNameFrmTakeIdle); // idle > 0 (true)
        UFPosixRuntime::semPost(_semNameFrmCnt); // increment counter for current observation
        UFPosixRuntime::semPost(_semNameAcqCnt); // increment total/program acq. counter
      }
    }

    if( _verbose ) {
      t = UFRuntime::currentTime(); t = t.substr(0,21); // resoluton to milli. sec.
      clog<<"UFEdtDMA::> edtDoneCnt= "<<edtDone<<", acquired at t= "<<t<<endl;
    }
    if( timedout ) {
      t = UFRuntime::currentTime(); t = t.substr(0,21); // resoluton to milli. sec.
      clog<<"UFEdtDMA::> timedOut!, timeOutCnt= "<<tocnt<<", edtDoneCnt= "<<edtDone<<", at t= "<<t<<endl;
    }
    else {
      //if( c->littleEnd && !UFRuntime::littleEndian() ) { 
      if( !UFRuntime::littleEndian() && !_noswap ) { 
        clog<<"UFEdtDMA::> assuming MCE4/EDT output is littlEndian, but host is NOT..."<<endl;
        // mce4/edt data is littleEndian, but this is a big-endian machine:
        status = little2BigEnd(frm, frm, c); 
      }
      if( lut ) {
  	status = applyLut(lut, (unsigned int*)frm, pixfits, c->w*c->h); // sort into pixfits
      } // LUT
      else { 
        ::memcpy((unsigned char*)pixfits, frm, sz); // copy into pixfits (offset into imgfits)
      } // no LUT
      if( _ref ) {
        clog<<"UFEdtDMA::takeAndStore> subtracting _ref image:  "<<_reffile<<endl;
	int npix = c->w*c->h, zerocnt= 0;
        int *ifrm = (int*) frm, *ipixfits = (int*) pixfits;
	for( int i = 0; i < npix; ++i ) {
	  if( ifrm[i] == 0 ) ++zerocnt;
	  ifrm[i] -= _ref[i]; ipixfits[i] -= _ref[i];
	}
	if( zerocnt == npix )
	  clog<<"UFEdtDMA::takeAndStore> warning, all frame pixels were 0!"<<endl;
	else
	  clog<<"UFEdtDMA::takeAndStore> zeropix: "<<zerocnt<<", npix: "<<npix<<endl;
      } 
    } // not timedout

    if( _timer && pidtimer != (pid_t) -1 ) {
      UFRuntime::bell();
      ::kill(pidtimer, SIGTERM);
    }

    // assume postacq will swap bytes from little2big endian if needed
    if( postacq != 0 ) { 
      //if( _verbose )
        clog<<"UFEdtDMA::takeAndStore> invoking postacq func..."<<endl;

      // provide mce4/edt raw and fits to postacq func:
      (*postacq)( frm, (char*) imgfits, total_sz, filenm.c_str() );

      if( _verbose )
	clog<<"UFEdtDMA::takeAndStore> postacq/display func. done. "<<endl;
    } // swapped bytes from little2big endian if needed!
    else if( !_noswap ) { // if no postacq func. indicated and require swab, do it here:
      // mce4/edtpdv data (and host) is littleEndian, but fits is a big-endian:
      // 4 byte swap into bigendian if host is littleEndian and no postacq
      clog<<"UFEdtDMA::> output FITS file must be BigEndian..."<<endl;
      // host integer format:
      status = little2BigEnd((unsigned char*)pixfits, (unsigned char*)pixfits, c);
    } // no postacq func

    // even if the dma timed-out, save the buffer to disk to insure it is noted:
    if( _usemmap ) { // close the mmap
      clog<<"UFEdtDMA::takeAndStore> close mmap file:  "<<filenm<<endl;
      status = UFPosixRuntime::mmapFileClose(mf);  // why does this take so long?
      if( status < 0 ) {
        clog<<"UFEdtDMA::takeAndStore> unable to close mmap file:  "<<filenm<<endl;
        if( cnt < nf ) {
          closeDev();
          return cnt;
        }
      }
      imgfits = 0; // reset mmap pointer
    }
    else { // write out the file
      writeTo(filenm, (char*)imgfits, total_sz, pad);
    } // file output
    if( _rename ) { // mmap file completed, rename it?
      // since cnt has been incremented, allow for index 0 by subtracting 1
      rename(index0+cnt-1, filehnt, filenm);
    }

    clog<<"UFEdtDMA::takeAndStore> completed: "<<cnt<<" -- "<<filenm<<endl;
    if( _simExpTime > 0 ) {
      sleep(_simExpTime);
    }

    if( nf > 0 && cnt >= nf ) { // 0 nf indicates infinite frames
      break;
    } // exit while-loop?

    clog<<"UFEdtDMA::> clear the imgbuffer so that timed-out frames don't contain old data."<<endl;
    ::memset(imgfits, 0, total_sz);
  } // end while

  if( nf > 0 && edtDone != cnt )
    clog<<"UFEdtDMA::takeAndStore> ?missed frames, edtDone= "<<edtDone<<", cnt= "<<cnt<<", req.= "<<nf<<endl;
  endDMA(); // keep device open or close it?

  return cnt;
} // takeAndStore

int UFEdtDMA::endDMA() {
  int r= 0;
  if( _lockfile.empty() ) {
    // DMA no longer active, remove lockfile created by startDMA:
    r = ::remove(_lockfile.c_str());
    clog<<"UFEdtDMA::endDMA> removed lockfile: "<<_lockfile<<endl;
  }
  return r;
}

void UFEdtDMA::terminate() {
  closeDev();

  if( _imgbuf ) delete [] _imgbuf;

  if( _lut ) {
    if( _lutmmap.fd > 0 )
      UFPosixRuntime::mmapFileClose(_lutmmap); 
    else
      delete [] _lut; // internally generated lut(s) is/are on the heap
  }

  if( _ref ) {
    if( _refmmap.fd > 0 )
      UFPosixRuntime::mmapFileClose(_refmmap); 
    else
      delete [] _ref; // internally generated lut(s) is/are on the heap
  }

  if( _fitshdr && _fitshdrmmap.fd > 0 ) // internally generated fits headers are on the stack, external are mmaped
    UFPosixRuntime::mmapFileClose(_fitshdrmmap); 

  if( _nbuf > 0 && _frms ) // non mmaped frame ring buffer must be freed
    freeBufs(_nbuf, _frms);
}

void UFEdtDMA::rename(int index, string& filehnt, string& filenm) {
  int tpos = filenm.find(":");
  if( tpos != (int)string::npos && tpos >= 6 ) // ".[A-z]yyyy:" shortest possible name  with jtime
    tpos -= 5;
  // rename the file...
  // filenm should be long version (with jtime)
  string newname;
  // first check if it is a silent file
  if( filenm.find('.') == 0 ) { // . at start
    newname = filenm.substr(1, tpos);
  }
  else {
    int slshpos = filenm.find("/.");
    if( slshpos != (int)string::npos )
      newname = filenm.substr(0,slshpos+1) +  filenm.substr(slshpos+2, tpos-1); 
  }

  if( newname != filenm ) {
    // rename to more simply-named / shortened string and preserve original name as symlink
    clog<<"UFEdtDMA::rename>"<<filenm<<endl;
    int status = ::rename(filenm.c_str(), newname.c_str());

    if( status < 0 )
      clog<<"UFEdtDMA::rename> unable to rename "<<filenm<<" to "<<newname
	  <<UFPosixRuntime::errStr()<<endl;
    else
      if( _verbose ) clog<<"UFEdtDMA::rename> renamed "<<filenm<<" to "<<newname<<endl;
  }
  else
    clog<<"UFEdtDMA::rename> !refuse to rename "<<filenm<<" to "<<newname
	  <<UFPosixRuntime::errStr()<<endl;
  return;
}

int UFEdtDMA::applyLut(int* lut, unsigned int* frm, unsigned int* img, int elem, int Id) {
  if( !_nosem ) {
    UFPosixRuntime::semPost(_semNameLutApply); // indicate lut apply has commenced
    UFPosixRuntime::semClear(_semNameLutId);
  }

  clog<<"UFEdtDMA::applyLut> "<<flush;
  for(int i= 0; i < elem; ++i ) {
    int idx= lut[i];
    if( _verbose ) { 
      if( idx >= elem ) {
        clog<<"(bad lut val: "<<idx<<" at i: "<<i<<" ) ... "<<flush;
        idx = elem-1;
      }
      if( idx < 0 ) {
        clog<<"(bad lut val: "<<idx<<" at i: "<<i<<" ) ... "<<flush;
        idx = 0;
      }
    }
    img[idx] = frm[i];
    if( i % (elem/10) == 1 ) clog<<" . "<<flush;
  }
  clog<<"!"<<endl;

  if( !_nosem ) {
    UFPosixRuntime::semClear(_semNameLutApply);
    UFPSem::setvalue(_semNameLutId, Id); // indicate which lut id applied
  }
  return elem;
}

int UFEdtDMA::applyLutLit2BigEnd(int* lut, unsigned int* frm, unsigned int* img, Conf* c) {
  if( !_nosem )
    UFPosixRuntime::semPost(_semNameLutApply);

  clog<<"UFEdtDMA::applyLutLit2BigEnd> "<<flush;
  int i, elem = c->w * c->h;
  char *big = 0, *little = (char*) frm;
  if( c->d == 32 ) {
    // if the two buffer pointers passed in are pointing to the same memory, use tmp:
    unsigned char tmp[4]; 
    for( i = 0; i < 4*elem; i += 4 ) {
      tmp[0] = little[i+3]; tmp[1] = little[i+2]; tmp[2] = little[i+1]; tmp[3] = little[i+0];
      big = (char*) &img[lut[i]];
      big[0] = tmp[0]; big[1] = tmp[1]; big[2] = tmp[2]; big[3] = tmp[3];
      if( i > 1 && i % (elem/10) == 1 ) clog<<" . "<<flush;
    }
  }
  else {
    clog<<"UFEdtDMA::applyLutLit2BigEnd> ? bad config ? bits= "<<c->d<<endl;
    elem = -1;
  }
  clog<<endl;
  if( !_nosem )
    UFPosixRuntime::semClear(_semNameLutApply);

  return elem;
}
  
// write frame(s) to stdout, 0 indicates infinite frame count
int UFEdtDMA::takeStdOut(int nf, int* lut) { 
  UFEdtDMA::Conf* c = getConf();
  if( c == 0 )
    return -1;

  int nbuf= 4, cnt= 0, sz= 0, elem= c->w*c->h;
  unsigned int img[(const int)elem];
  unsigned char** frms = allocBufs(nbuf, c, sz);
  int edtDone = startDMA(nbuf, frms, nf); // opens dev, etc.
  if( edtDone < 0 ) {
    clog<<"UFEdtDMA::> startDMA failed (no EDT sync?)"<<endl;
    return edtDone;
  }
  while( edtDone >= 0 && edtDone < nf && cnt < nf ) {
    unsigned char* frm = waitImages(1);
    edtDone = doneCnt();
    if( lut )
      UFEdtDMA::applyLut(lut, (unsigned int*)frm, img, elem);
    else
      ::memcpy(img, frm, sz);
    int nb = ::write(fileno(stdout), (char*) img, sz);
    if( nb < sz ) {
      clog<<"UFEdtDMA::takeStdOut> ?failed write to stdout, nb= "<<nb<<"\n"<<
	    "edtDone= "<<edtDone<<", cnt= "<<cnt<<endl;
      if( nb <= 0 ) {
        terminate();
	return cnt;
      }
    }
    ++cnt;
  }
  if( edtDone != cnt ) {
    clog<<"UFEdtDMA::takeStdOut> ?missed frames, edtDone= "<<edtDone<<", cnt= "<<cnt<<endl;
  }

  terminate();

  return cnt;
}

UFEdtDMA::Conf*
UFEdtDMA::init(const UFRuntime::Argv& args,
	       int& cnt, int& timeOut, int*& lut,
	       int& index, char*& filehint,
	       char*& fits, int& fits_sz) {

  string usage("UFEdtDMA:: [-v] [-stdout] [-index start index] [-[exp]cnt frmcnt] [-lut lutfile] [-fits fitsfile] [-file hint] [-rename] [-lock lockfile] [-timeout sec.]");
  string arg;
  int argc = (int) args.size();
  if( argc <= 1 ) {
    clog<<usage<<endl;
    exit(0);
  }

  arg = UFRuntime::argVal("-help", args);
  if( arg == "true" ) {
    clog<<usage<<endl;
    exit(0);
  }

  _edtcfg = getConf();
  if( _edtcfg == 0 ) {
    clog<<"unable to get EDT config..."<<endl;
    exit(1);
  }

  arg = UFRuntime::argVal("-v", args);
  if( arg == "true" )
    _verbose = true;

  arg = UFRuntime::argVal("-rename", args);
  if( arg == "true" )
    _rename = true;

  arg = UFRuntime::argVal("-nommap", args);
  if( arg == "true" )
    _usemmap = false;

  arg = UFRuntime::argVal("-mmap", args);
  if( arg == "true" )
    _usemmap = true;

  arg = UFRuntime::argVal("-nosem", args);
  if( arg == "true" )
    _nosem = true;

  arg = UFRuntime::argVal("-noswap", args);
  if( arg == "true" )
    _noswap = true;

  /*
  arg = UFRuntime::argVal("-lutle", args);
  if( arg == "true" )
    _lutLE = true;
  */

  arg = UFRuntime::argVal("-lock", args);
  if( arg != "true" && arg != "false")
    _lockfile = arg;

  index= 0; cnt = 0; // run forever = 0?
  timeOut = 0; // infinite timeOut
  filehint = 0;
  string file, flut, ffits;
  arg = UFRuntime::argVal("-cnt", args); // frame cnt
  if( arg != "false" && arg != "true" )
    cnt = atoi(arg.c_str());
  arg = UFRuntime::argVal("-expcnt", args); // frame cnt
  if( arg != "false" && arg != "true" )
    cnt = atoi(arg.c_str());

  arg = UFRuntime::argVal("-timeout", args); // frame cnt
  if( arg != "false" && arg != "true" )
    timeOut = _timeOut = atoi(arg.c_str());

  arg = UFRuntime::argVal("-index", args); // frame cnt
  if( arg != "false" && arg != "true" )
    index = atoi(arg.c_str());

  arg = UFRuntime::argVal("-file", args); // output file name "hint" (prefix)
  if( arg != "false" && arg != "true" ) {
    filehint = (char*)arg.c_str(); // but this is on the stack
    char* tmp = new char[1+strlen(filehint)]; memset(tmp, 0, 1+strlen(filehint));
    strcpy(tmp, filehint); // return heap mem.:
    filehint = tmp;
  }
  arg = UFRuntime::argVal("-fitsfile", args); // output file name "hint" (prefix)
  if( arg != "false" && arg != "true" ) {
    filehint = (char*)arg.c_str(); // but this is on the stack
    char* tmp = new char[1+strlen(filehint)]; memset(tmp, 0, 1+strlen(filehint));
    strcpy(tmp, filehint); // return heap mem.:
    filehint = tmp;
  }
  if( filehint ) {
    string datadir= "/data/", nfshost;
    string ffits;
    if( filehint[0] != '/' ) {
      clog<<"UFEdtDMA::init> filehint: "<<filehint<<", check 'datadir' and 'nfshost' options..."<<endl;
      arg = UFRuntime::argVal("-nfshost", args);
      if( arg != "false" && arg != "true" ) {
        if( arg.find("localhost") == string::npos && arg.find("LOCALHOST") == string::npos) {
          nfshost = arg; // /net/nfshost automount or /nfs/host perm. mount?
          if( nfshost.find("/") != 0 ) nfshost = "/" + nfshost;
          if( nfshost.rfind("/") != nfshost.length()-1 ) nfshost += "/";
	}
      }
      if( !nfshost.empty() ) ffits = nfshost;
      arg = UFRuntime::argVal("-datadir", args);
      if( arg != "false" && arg != "true" ) {
        datadir = arg; //
        if( datadir.find("/") != 0 ) datadir = "/" + datadir;
        if( datadir.rfind("/") != datadir.length()-1 ) datadir += "/";
      }
      if( !datadir.empty() ) {
	// silently create the directory if nec.
	mode_t m= 0775, m0 = ::umask(0); ::mkdir(datadir.c_str(), m); ::umask(m0);
	ffits += datadir;
      }
      clog<<"UFEdtDMA::init> fits data location: "<<ffits<<endl;
      ffits += filehint;
      size_t salloc = ffits.length()+1;
      char* tmp = new char[salloc]; memset(tmp, 0, salloc);
      strcpy(tmp, ffits.c_str()); // return heap mem.:
      filehint = tmp;
      clog<<"UFEdtDMA::init> filehint: "<<filehint<<endl;
    }
  } 
   
  int elem = _edtcfg->w * _edtcfg->h;
  int sz = elem*sizeof(int);

  arg = UFRuntime::argVal("-ref", args);
  if( arg != "false" ) {
    if( arg == "true" ) { // if no file name supplied, use first acquiredframe...
      _reffile = "ref0.raw";
      _refmmap = UFPosixRuntime::mmapFileCreate(_reffile, sz);
      if( _refmmap.fd < 0 ) {
        clog<<"UFEdtDMA::init> unable to mmap "<<_reffile<<endl;
        exit(0);
      }
      _ref = (int*) _refmmap.buf;
      ::memset(_ref, 0, sz);
    } 
    else {
      _reffile = arg;
      struct stat sa; int exists = ::stat(_reffile.c_str(), &sa);
      if( exists >= 0 ) {
        _refmmap = UFPosixRuntime::mmapFileOpenR(_reffile, sz);
      }
      else {
        _refmmap = UFPosixRuntime::mmapFileCreate(_reffile, sz);
      }
      if( _refmmap.fd < 0 ) {
        clog<<"UFEdtDMA::creatLut> unable to mmap "<<_reffile<<endl;
        exit(0);
      }
      _ref = (int*) _refmmap.buf;
      ::memset(_ref, 0, sz);
    } 
  }

  char* lutfile= 0;
  arg = UFRuntime::argVal("-lut", args);
  lut= 0;
  if( arg != "true" && arg != "false" ) {
    flut = arg; // make copy of arg
    lutfile = (char*)flut.c_str(); 
  }
  if( arg == "true" ) { // generate lut from algorithm:
    //_lutmmap.fd = -1; _lutmmap.buf = 0;
    //clog<<"UFEdtDMA::> create (internal) lut."<<endl;
    //if( UFEdtDMA::_lut == 0 ) UFEdtDMA::_lut = UFEdtDMA::creatLut(elem); // one-time alloc.
    //lut = UFEdtDMA::_lut;
    //if( lut == 0 ) clog<<"UFEdtDMA::> failed to create lut."<<endl;
    const char* ufi = getenv("UFINSTALL"); // use installed (BE) lut
    if( ufi != 0 ) { flut = ufi; flut += "/etc/UFFlamingos.lut"; lutfile = (char*)flut.c_str(); }
  }
  arg = UFRuntime::argVal("-lutfile", args);
  lut= 0;
  if( arg != "true" && arg != "false" ) {
    flut = arg; // make copy of arg
  }
  if( arg == "true" ) { // generate lut from algorithm:
    //_lutmmap.fd = -1; _lutmmap.buf = 0;
    //clog<<"UFEdtDMA::> create (internal) lut."<<endl;
    //if( UFEdtDMA::_lut == 0 ) UFEdtDMA::_lut = UFEdtDMA::creatLut(elem); // one-time alloc.
    //lut = UFEdtDMA::_lut;
    //if( lut == 0 ) clog<<"UFEdtDMA::> failed to create lut."<<endl;
    const char* ufi = getenv("UFINSTALL"); // use installed (BE) lut
    if( ufi != 0 ) { flut = ufi; flut += string("/etc/UFFlamingos.lutBE"); lutfile = (char*)flut.c_str(); }
  }
  // also check if lut file content is indicated to be little-endian
  arg = UFRuntime::argVal("-lutLE", args);
  if( arg != "false" ) {
    _lutLE = true;
    if( arg == "true" ) {
      clog<<"UFEdtDMA::> -lutLE not followed by filename!"<<endl;
      const char* ufi = getenv("UFINSTALL"); // use installed (BE) lut
      if( ufi != 0 ) { flut = ufi; flut += string("/etc/UFFlamingos.lutLE"); lutfile = (char*)flut.c_str(); }
    }
    else {
      flut = arg; // make copy of arg
    }
  }
  if( !flut.empty() ) {
    if( flut.find("/") != 0 ) {
      clog<<"UFEdtDMA::init> LUT file not absolute path, checking $UFINSTALL/etc/"<<endl;
      const char* ufi = getenv("UFINSTALL"); // use installed (BE) lut
      if( ufi != 0 )
	flut = string(ufi) + string("/etc/") + flut;
      else
	flut = string("/usr/local/uf/etc/") + flut;
    }
    size_t salloc = flut.length()+1;
    char* tmp = new char[salloc]; memset(tmp, 0, salloc);
      strcpy(tmp, flut.c_str()); // return heap mem.:
    lutfile = tmp;
  } 
   
  if( lutfile != 0 ) { // read-in lut file
    clog<<"UFEdtDMA::> read-in lut file: "<<lutfile<<endl;
    /* 
    _lutmmap = UFPosixRuntime::mmapFileOpenR(lutfile, sz);
    if( _lutmmap.fd < 0 ) {
      clog<<"UFEdtDMA::> unable to mmap to lut file: "<<lutfile<<", will not apply a lut..."<<endl;
    }
    else {
      //if( _verbose ) 
        clog<<"UFEdtDMA::> mmapped little-endian (LE) lutfile: "<<flut<<endl;
      lut = (int*)_lutmmap.buf;
    }
    */
    //_usemmap = false;
    _lutmmap.fd = -1; // indicate non-memory-mapped lut
    UFEdtDMA::_lut = lut = new int[elem];
    // simple binary read allows us to perform byte-swap in-place
    struct stat st; int success = ::stat(lutfile, &st); 
    if( success < 0 ) {
      clog<<"UFEdtDMA::> unable to stat lut file: "<<lutfile<<endl;
      exit(1);
    }
    int fd = ::open(lutfile, O_RDONLY);
    if( fd < 0 ) {
      clog<<"UFEdtDMA::> unable to open lut file: "<<lutfile<<endl;
      exit(1);
    }
    int nr = ::read(fd, lut, sz); 
    if( nr != sz ) {
      clog<<"UFEdtDMA::> mismatch in expected LUT size and file size/byte read: "<<nr<<" / "<<sz<<endl;
      exit(1);
    }
    // if mmapped lut was stored as little-endian, but this is a big-endian machine?
    if( _lutLE && !UFRuntime::littleEndian() ) {
      clog<<"UFEdtDMA::> LUT is littlEndian, host is NOT! Performing one-time byte-swap..."<<endl;
      if( _lutmmap.fd > 0) { 
        int* BElut = new int[_edtcfg->w*_edtcfg->h];
        little2BigEnd((unsigned char*)lut, (unsigned char*)BElut, _edtcfg);
        delete [] lut;
        UFEdtDMA::_lut = lut = BElut;
      }
      else { // not memory-mapped, swap in-place
        little2BigEnd((unsigned char*)lut, (unsigned char*)lut, _edtcfg);
        // one-time save/write of swapped lut:
        //mode_t m = ::umask(0);
        //int fd = ::open("/tmp/f2swapped.lut", O_CREAT | O_RDWR , 0664);
	//if( fd > 0 ) { ::write(fd, lut, _edtcfg->w*_edtcfg->h*sizeof(int)); ::close(fd); }
	//::umask(m);
      }
    }
    else {
      clog<<"UFEdtDMA::> assuming LUT file endian matches host endian..."<<endl;
    }
  }

  // insure fits buf ptr is clear:
  fits= 0;
  fits_sz = 1*36*80; // 2880 is no longer the minimum, but n*80 still applies
  char* fitsfile= 0;
  UFPosixRuntime::MMapFile mfits;
  arg = UFRuntime::argVal("-fits", args);
  if( arg != "true" && arg != "false" ) {
    ffits = arg; // make copy of arg
    fitsfile = (char*)ffits.c_str(); 
    struct stat st; int success = ::stat(fitsfile, &st); 
    if( success < 0 ) {
      clog<<"UFEdtDMA::> unable to stat to fits file: "<<fitsfile<<", will not prepend fits header..."<<endl;
    }
    else {
      fits_sz = st.st_size; // actuall size may be greater than min.
      _fitshdrmmap = UFPosixRuntime::mmapFileOpenR(fitsfile, fits_sz);
      if( _fitshdrmmap.fd < 0 ) {
        clog<<"unable to mmap to fits file: "<<fitsfile<<", will not prepend fits header..."<<endl;
      }
      else { // ok to set the fits buff ptr now:
        fits = (char*)mfits.buf;
      }
    }
  }

  return _edtcfg;
}

string UFEdtDMA::status() {
  int w, h, id;
  string idle = "false";
  string applyingLut = "false";
  if( UFEdtDMA::takeIdle(w, h) )
    idle = "true";
  if( UFEdtDMA::applyingLut(id) )
    applyingLut = "true";

  int tkcnt = UFEdtDMA::takeCnt();
  int totcnt = UFEdtDMA::takeTotCnt();
  int acqcnt = UFEdtDMA::acqCnt();
  int acqto = UFEdtDMA::acqTimeOut();

  strstream s;
  s<<"Edt frame: "<<w<<"x"<<h<<", acq. idle: "<<idle
   <<", applyingLUT: "<<applyingLut<<", LUT Id: "<<id
   <<", seq/total: "<< tkcnt <<" / "<< totcnt
   <<", cumulative: "<< acqcnt <<", timeOut: "<< acqto <<ends;

  string stat = s.str(); delete s.str();
  return stat;
}

int* UFEdtDMA::creatLut(const int elem, const int Id, bool reverseH, bool savelut) {
  // robert's trecs algorithm
  if( elem <= 320*240 ) { // trecs
    clog<<"UFEdtDMA::creatLut> TReCS default LUT (320x240)..."<<endl;
    int iPix, iRow, iCol, iFast, iChan, iMap; 
    int nRow=240, nCol=320, nFast=20, nChan=16; 
    int* lut = new int[nCol*nRow];
    if( lut == 0 ) {
      clog<<"UFEdtDMA::creatLut> sorry, heap out of memory, elem: "<<elem<<endl;
      return 0;
    }

    for (iRow=0, iPix=0; iRow<nRow; iRow++) {
      for (iFast=0; iFast<nFast; iFast++) {
        for (iChan=0; iChan<nChan; iChan++, iPix++) { 
	  iCol = iChan*nFast + iFast; 
	  iMap = iRow*nCol + iCol; 
	  lut[iPix] = iMap; 
	} 
      }
    }

    return lut;
  }

  // richard's flamingos algorithm
  if( elem != 2048*2048 ) {
    clog<<"UFEdtDMA::creatLut> sorry, only trecs 320x240 & flamingos 2048x2048 supported, elem: "<<elem<<endl;
    return 0;
  }
  clog<<"UFEdtDMA::creatLut> Flamingos default LUT (2048x2048)..."<<endl;
  int *lut = new (nothrow) int[1+elem];
  if( lut == 0 ) {
    clog<<"UFEdtDMA::creatLut> sorry, heap out of memory, elem: "<<elem<<endl;
    return 0;
  }
  int* array0 = new (nothrow) int[1+elem];
  if( array0 == 0 ) {
    clog<<"UFEdtDMA::creatLut> (array0) sorry, heap out of memory, elem: "<<elem<<endl;
    return 0;
  }
  int* array1 = new (nothrow) int[1+elem];
  if( array1 == 0 ) {
    clog<<"UFEdtDMA::creatLut> (array1) sorry, heap out of memory, elem: "<<elem<<endl;
    return 0;
  }
  int* array[2] = { array0, array1 };
  int i, j, amp, index, line, pixel= 0;
  //
  // The line loop 
  //
  for(line= 0; line <= 1023; ++line) {
    for(index= 0; index <= 127;  ++index) {
      //
      // Quadrant 1-1
      //
      for(amp= 0; amp <= 3; ++amp) {
        i= 1023-256*amp-index;
        j= 2047-line;
        array[0][pixel]= i;
        array[1][pixel]= j;
	++pixel;
      }
      //
      // Quadrant 2-1
      //
      for(amp= 0; amp <=3; ++amp) {
        j= 1023-256*amp-index;
        i= line;
        array[0][pixel]= i;
        array[1][pixel]= j;
        ++pixel;
      }
      //
      // Quadrant 3-1
      //
      for(amp= 0; amp <=3; ++amp) {
        i= 1024+256*amp+index;
        j= line;
        array[0][pixel]= i;
        array[1][pixel]= j;
        ++pixel;
      }
      //
      // Quadrant 4-1
      //
      for(amp= 0; amp <=3; ++amp) {
        j= 1024+256*amp+index;
        i= 2047-line;
        array[0][pixel]= i;
        array[1][pixel]= j;
        ++pixel;
      }
      //
      //Quadrant 1-2
      //
      for(amp= 0; amp <=3; ++amp) {
         i= 895-256*amp-index;
         j= 2047-line;
	 array[0][pixel]= i;
         array[1][pixel]= j;
         ++pixel;
      }
      //
      // Quadrant 2-2
      //
      for(amp= 0; amp <=3; ++amp) {
        j= 895-256*amp-index;
        i= line;
        array[0][pixel]= i;
        array[1][pixel]= j;
        ++pixel;
      }
      //
      // Quadrant 3-2
      //
      for(amp= 0; amp <=3; ++amp) {
        i= 1152+256*amp+index;
        j= line;
        array[0][pixel]= i;
        array[1][pixel]= j;
        ++pixel;
      }
      //
      // Quadrant 4-2
      //
      for(amp= 0; amp <=3; ++amp) {
	j= (1024+128)+256*amp+index;
	i= 2047-line;
	array[0][pixel]= i;
	array[1][pixel]= j;
	++pixel;
      }
    }
  }
  //
  // set the pixel numbers in lut table
  //
  int min= elem, max= 0;
  for( i= 0; i < elem; ++i ) {
    lut[i]= array[0][i] + array[1][i]*2048;
    if( lut[i] > max ) max = lut[i];
    if( lut[i] < min ) min = lut[i];
  }
  if( max >= elem || min != 0 )
    clog<<"UFEdtDMA::creatLut> elem= "<<elem<<", ? bad lut min, max: "<<min<<", "<<max<<endl;

  delete [] array0; delete [] array1;

  if( _verbose )
    clog<<"UFEdtDMA::creatLut> elem= "<<elem<<", lut min, max: "<<min<<", "<<max<<endl;

  // reversed lut is true for richard's original
  if( !reverseH ) {
    if( _verbose )
      clog<<"UFEdtDMA::creatLut> invert LUT horizontally..."<<endl;
    int tmp = 0;
    for( j= 0; j < 2048; ++j ) {
      int *scan = &lut[j];
      for( i= 0; i < 2048/2; ++i ) {
	tmp = scan[i];
	scan[i] = scan[2047-i];
	scan[2047-i] = tmp;
      }
    }
  }
  //savelut = true;
  if( !savelut )
    return lut;

  string lutfile = "/usr/tmp/flam.lut";
  if( _verbose )
    clog<<"UFEdtDMA::creatLut> save LUT to: "<<lutfile<<endl;
  size_t sz = elem*sizeof(int);
  UFPosixRuntime::MMapFile mf = UFPosixRuntime::mmapFileCreate(lutfile, sz);
  if( mf.fd < 0 ) 
    clog<<"UFEdtDMA::creatLut> unable to mmap "<<lutfile<<endl;
  else {
    memcpy(mf.buf, lut, sz);
    UFPosixRuntime::mmapFileClose(mf);
    //clog<<"UFEdtDMA::creatLut>> closed mmap file:  "<<lutfile<<endl;
  }
  return lut;
}

// test main just calls takeAndStore or takeStdOut with args set by init:
int UFEdtDMA::main(int argc, char** argv) {
  int cnt= 1, timeOut= 0, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0;
  UFRuntime::Argv args(argc, argv);
    
  bool store = true;
  string arg = UFRuntime::argVal("-stdout", args);
  if( arg == "true" )
    store = false;

  UFEdtDMA::Conf* ec = init(args, cnt, timeOut, lut, index, filehint, fits, fits_sz);
  if( ec == 0 ) {
    clog<<"UFEdtDMA::main> edt frame card failed to open: "<<endl;
    return -1;
  }

  if( store ) 
    cnt = UFEdtDMA::takeAndStore(cnt, index, filehint, lut, fits, fits_sz);
  else
    cnt = UFEdtDMA::takeStdOut(cnt, lut);

  clog<<"UFEdtDMA::main> took/grabbed frames: "<<cnt<<endl;
  return 0;
}

#endif // __UFEdtDMA_cc__
