#if !defined(__UFEdtDMA_h__)
#define __UFEdtDMA_h__ "$Name:  $ $Id: UFEdtDMA.h,v 0.15 2006/01/18 17:29:46 hon Exp $"
#define __UFEdtDMA_H__(arg) const char arg##EdtDMA_h__rcsId[] = __UFEdtDMA_h__;

#if defined(_NO_EDT_)
#define PDV_BSWAP 1
#define PDV_BYTESWAP 1
struct PdvDD { int byteswap; int hwpad; };
struct  PdvDev { int edtfd; PdvDD* dd_p; inline PdvDev() : edtfd(0) { dd_p = new PdvDD; } };
extern int edt_done_count(...);
extern int edt_reg_write(...);
extern PdvDev* pdv_open(...);
extern PdvDev* pdv_open_channel(...);
extern int pdv_set_timeout(...);
extern int pdv_set_buffers(...);
extern int pdv_start_images(...); 
extern unsigned char* pdv_wait_images(...); 
extern int pdv_setup_continuous(...); 
extern int pdv_slop(...);
extern int pdv_close(...);
extern int pdv_image_size(...);
extern int pdv_multibuf(...);
#else // _NO_EDT_
#if defined(LINUX)
#define __restrict 
#include "sys/time.h"
#undef __restrict
#endif // LINUX
#include "edtinc.h" // includes libedt.h & libpdv.h
#ifdef __cplusplus
extern "C" {
#endif
extern int pdv_slop(PdvDev* pdv_p);
extern int pdv_image_size(PdvDev* pdv_p);
extern int pdv_multibuf(EdtDev *edt_p, int numbufs);
extern int pdv_close(PdvDev *pdv_p);
#ifdef __cplusplus
}
#endif
#endif // !_NO_EDT_

#include "UFRuntime.h"
#include "UFPSem.h"

// std c++
using namespace std;
#include "string"
#include "vector"

struct UFEdtDMA {
  // note that depth (d) is counted in bits, width (w) & height (h) in pixels:
  // /opt/EDTpdv/initcam settings (F2 sparc default):
  struct Conf { int w; int h; int d; bool littleEnd;
                Conf(int width=2048, int height=2048) : w(width), h(height), d(32), littleEnd(false) {}
                Conf(const UFEdtDMA::Conf& rhs) : w(rhs.w), h(rhs.h), d(rhs.d), littleEnd(rhs.littleEnd) {}
  };

  // maintain list of all know configs
  typedef vector< UFEdtDMA::Conf* > ConfVec;

  // optional Display/PostProcess function invoked immediately after acquisition,
  // well, immiediately after lut and little2big (if any) is applied:
  //typedef void (*PostAcq)(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fits_filenm= 0);
  typedef void (*PostAcq)(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fits_filenm);
  // optional Simulation function invoked for acquisition,
  //typedef void (*AcqFunc)(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fits_filenm= 0);
  typedef void (*AcqFunc)(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fits_filenm);

  // according to EDT PdvDev == EdtDev
  typedef PdvDev* DevPtr; 

  // verbose logging
  static bool _verbose;

  // verbose logging
  static bool _usemmap;

  // show timer (xclock)
  static bool _timer;

  // re-usable global device ptr:
  static UFEdtDMA::DevPtr _theDevp;

  // current conf:
  static UFEdtDMA::Conf* _edtcfg;

  // reference frame/image for differencing
  static int* _ref;
  static string _reffile;

  // global lut & fits header file options
  static UFPosixRuntime::MMapFile _lutmmap, _fitshdrmmap, _refmmap;
  static string _lutfile, _fhdrfile; // lut and fits-hdr filenames

  // frequency of calls to display func:
  static int _dpySeq;
  inline static void setDpySeq(int dps) { _dpySeq = dps; }

  //  .hidden rename to visible file name
  static bool _rename;

  // cmd-line timeOut option:
  static int _timeOut;
  static int _timeOutCnt;

  // simulate an exposure time (from external pixel generator) via sleep:
  static int _simExpTime;

  // turn-off semaphores, byte-swapping
  static bool _nosem, _noswap;

  static bool _lutLE; // is mmapped lut little-endian?

  // lock-file option -- allows cmdline designation
  static string _lockfile, _edtlockfile;
  //inline static string lock() { return _lockfile; }

  // instrument name (for simulation mode)
  static string _instrum;

  // posix semaphore option
  // detector/mce4 idle or generating & grabbing/taking frames, apply LUT? 
  // helper funcs.
  static void _semNames();
  static void _semRmAll();
  static void _semInitAll(int totfrm= 1, UFEdtDMA::Conf* edtc= 0);
  static string _semNameAcqCnt; // acquisition counter is perpertually incremeted 
  static string _semNameAcqTimeOut; // acquisition time-out > mce4 exposure time 
  static string _semNameFrmTakeIdle; // 0 or 1
  static string _semNameLutApply; // 0 or 1
  static string _semNameLutId; // set to > 0 when/after lut applied with lut Id #
  static string _semNameFrmWidth; // set to > 0 when/after edt config
  static string _semNameFrmHeight; // ditto
  // if take is true (non-zero) the FrmCnt & FrmTotCnt sems are used
  static string _semNameFrmCnt; 
  static string _semNameFrmTotCnt;
  static int acqCnt();
  static int acqTimeOut();
  static int resetAcqTimeOut(int val= 0);
  static bool takeIdle(int& width, int& height);
  static bool applyingLut(int& Id);
  static int takeCnt();
  static int takeTotCnt();
  static void takeAbort(pid_t takepid= (pid_t) -1);

  inline UFEdtDMA() {}
  inline virtual ~UFEdtDMA() {}
  // contents of /opt/EDTpdv/version file:
  inline virtual string description() const { return __UFEdtDMA_h__; }

  // return current config from device query
  static UFEdtDMA::Conf* getConf(UFEdtDMA::DevPtr edtp= 0);

  static int width();
  static int height();

  // write current config to log
  static string logConf(const Conf& devc, const string& configfile);
  static int dimensions(int& w, int& h);

  // essential EDT functions wrapped:
  static UFEdtDMA::DevPtr openDev();
  static int closeDev();
  static bool timedOut(int& tocnt);
  static int startDMA(int numbufs= 2, unsigned char** buffers= 0, int numFrms= 0);
  static int endDMA();
  static unsigned char* waitImages(int n= 1, const string* semname= 0);
  static int doneCnt();
  static int little2BigEnd(unsigned char* little, unsigned char* big, UFEdtDMA::Conf* c);

  // for unit tests:
  // loosely based on edt's take example:
  static unsigned char** allocBufs(int nbuf, UFEdtDMA::Conf* c, int& sz);
  static unsigned char** bufPtrs(); // allow reusing pre-allocated bufs
  static int freeBufs(int nbuf, unsigned char**& bufs);
  // mmap to file(s)
  // if lut != 0 assume it points to 1-D LUT (frame to image sort)
  static int applyLut(int* lut, unsigned int* frm, unsigned int* img, int elemi, int Id= 1);
  static int applyLutLit2BigEnd(int* lut, unsigned int* frm, unsigned int* img, UFEdtDMA::Conf* c);
 
  // generate unique? filename:
  static string mkname(const string& filehint, int index0, int cnt, bool hidden= false);
  // use system open/write for output fits file:
  static int writeTo(const string& filenm, const char* imgfits, int total_sz, int pad= 0);

  // nf = 0 indicates infinite frame count
  static int takeAndStore(int numFrm= 0, int index0= 0, char* filehint= 0,
			  int* lut= 0, char* fits= 0, int fits_sz= 0,
			  PostAcq postacq= (PostAcq) 0, AcqFunc simacq= (AcqFunc) 0);
  // rename hidden file
  static void rename(int index, string& filehnt, string& filenm);
  // write frame(s) to stdout (for use with pipe)
  static int takeStdOut(int numFrm= 0, int* lut= 0);

  // richard's original algorgithm for creating the (flamingos hawaii-2) lut was reversed
  // so specifying reversH true should create richards original lut
  // also supports robert's trecs lut... 
  static int* creatLut(const int elem, const int lutId= 1, bool reverseH= false, bool savelut= false);

  static UFEdtDMA::Conf* init(const UFRuntime::Argv& args,
		              int& cnt, int& timeOut, int*& lut,
		              int& index, char*& filehint,
			      char*& fits, int& fits_sz);

  // this should only be invoked on app. shutdown
  static void terminate();
  static int _nbuf, *_lut; 
  static char* _fitshdr;
  static unsigned char* _imgbuf;
  static unsigned char** _frms;

  // return uffrmstatus (semaphore counter values) string
  static string status(); 

  // test main just calls takeAndStore or takeStdOut
  static int main(int argc, char** argv);
};

// global equality operator:
inline bool operator==(const UFEdtDMA::Conf& lhs, const UFEdtDMA::Conf& rhs) {
  if( lhs.w == rhs.w && lhs.h == rhs.h && lhs.d == rhs.d && lhs.littleEnd == rhs.littleEnd )
    return true;
  else
    return false;
}

#endif // __UFEdtDMA_h__
