#if !defined(__UFDeviceConfig_cc__)
#define __UFDeviceConfig_cc__ "$Name:  $ $Id: UFDeviceConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDeviceConfig_cc__;

#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFStrings.h"
#include "UFFITSheader.h"

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

// statics:
bool UFDeviceConfig::_verbose= false;
UFTermServ* UFDeviceConfig::_devIO = 0;

// default agent querry/command tables (elements of future UFDeviceConfig):
const char* _QuerryCmdNames__[] = {
  "clientlist", // return UFRndRobinServ::theConnections;
  "completedlist", // return UFDeviceAgent::CmdList _completed;
  "queuedlist", // return UFDeviceAgent::CmdList _queued;
  "stateOfdevice" // return _active or last/latest completion
};

const char* _ActionCmdNames__[] = {
  "null", // no-op command
  "default", // set device to default configuration
  "raw" // untranslated/uninspected (uncooked) command string
};

vector< string > UFDeviceConfig::_queries;
vector< string > UFDeviceConfig::_actions;

UFDeviceConfig::UFDeviceConfig(const string& name) : _tsport(0), _tshost(""),  _name(name) {}

UFDeviceConfig::UFDeviceConfig(const string& name,
			       const string& tshost,
			       int tsport) : _tsport(tsport), _tshost(tshost),  _name(name) {}

// default terminator is new-line:
string UFDeviceConfig::terminator() { return "\n"; }

// default prefix is none:
string UFDeviceConfig::prefix() { return ""; }

bool UFDeviceConfig::termServ(int& port, string& host) {
  port = _tsport;
  host = _tshost;
  return true;
}

UFTermServ* UFDeviceConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    _devIO = UFTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFDeviceConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }
  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);
  string sot = prefix();
  _devIO->resetSOTS(sot);
  return _devIO;
}

vector< string >& UFDeviceConfig::queryCmds() {
  if( _queries.size() == 0 ) {
    int cnt = _ArrayLen__(_QuerryCmdNames__);
    for( int i = 0; i < cnt; i++ )
      _queries.push_back(_QuerryCmdNames__[i]);
  }
  return _queries;
}

vector< string >& UFDeviceConfig::actionCmds() {
  if( _actions.size() == 0 ) {
    int cnt = _ArrayLen__(_ActionCmdNames__);
    for( int i = 0; i < cnt; i++ )
      _actions.push_back(_ActionCmdNames__[i]);
  }
  return _actions;
}

int UFDeviceConfig::validCmd(const string& c) { return 0; }

// return configuration status in FITS format:
UFStrings* UFDeviceConfig::statusFITS(UFDeviceAgent* da) {
  map< string, string > stat, com;
  stat["UFDeviceConfig"] = "No status available from base class";
  com["UFDeviceConfig"] = "No comments available from base class";
  if( da ) {
    stat["UFDeviceAgent"] = da->name();
    com["UFDeviceAgent"] = da->name();
  }
  return UFFITSheader::asStrings(stat, com);
}

// return configuration status in FITS format:
UFStrings* UFDeviceConfig::statusFITS(UFDeviceAgent* da, UFObsConfig* oc) {
  if( oc == 0 )
    return statusFITS(da);

  return statusFITS(da);
}

// return (current) configuration status in hash format (key == value+comments):
UFStrings* UFDeviceConfig::status(UFDeviceAgent* da) {
  map< string, string > stat, com;
  stat["UFDeviceConfig"] = "No status available from base class";
  com["UFDeviceConfig"] = "No comments available from base class";

  vector < string > vs;

  string s = "UFDeviceConfig";
  s = s +  " == " + stat["UFDeviceConfig"] + " // " + com["UFDeviceConfig"];
  vs.push_back(s);

  if( da != 0 ) {
    stat["UFDeviceAgent"] = da->name();
    com["UFDeviceAgent"] = "No comments available from base class";
    s = "UFDeviceAgent";
    s = s +  " == " + stat["UFDeviceAgent"] + " // " + com["UFDeviceAgent"];
    vs.push_back(s);
  }  
  return new (nothrow) UFStrings("UFDeviceConfig", vs);
}

UFStrings* UFDeviceConfig::status(UFDeviceAgent* da, UFObsConfig* oc) {
  if( oc == 0 )
    return status(da);

  map< string, string > stat, com;
  stat["UFDeviceConfig"] = "No status available from base class";
  com["UFDeviceConfig"] = "No comments available from base class";

  vector < string > vs;
  string s = "UFDeviceConfig";
  s = s + " == " + stat["UFDeviceConfig"] + " // " + com["UFDeviceConfig"];
  vs.push_back(s);

  if( da != 0 ) {
    stat["UFDeviceAgent"] = da->name();
    com["UFDeviceAgent"] = "No comments available from base class";
    s = "UFDeviceAgent";
    s = s +  " == " + stat["UFDeviceAgent"] + " // " + com["UFDeviceAgent"];
    vs.push_back(s);
  }  

  if( da != 0 ) {
    stat["UFObsConfig"] = oc->name();
    com["UFObsConfig"] = "No comments available from base class";
    s = "UFObsConfig";
    s = s +  " == " + stat["UFObsConfig"] + " // " + com["UFObsConfig"];
    vs.push_back(s);
  }

  return new (nothrow) UFStrings("UFDeviceConfig", vs);

}

#endif // __UFDeviceConfig_cc__
