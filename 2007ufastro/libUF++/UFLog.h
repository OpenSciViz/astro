#if !defined(__UFLog_h__)
#define __UFLog_h__ "$Name:  $ $Id: UFLog.h,v 0.1 2004/09/17 19:19:11 hon Exp $"
#define __UFLog_H__(arg) const char arg##Log_h__rcsId[] = __UFLog_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFLog : public UFDaemon {
public:
  inline UFLog(const string& name, char** envp= 0) : UFDaemon(name, envp) {}
  inline UFLog(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv) {}
  inline UFLog(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp) {}
  inline UFLog(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp) {}
  inline virtual ~UFLog() {}

  inline virtual string description() const { return __UFLog_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // stubs for now, eventually have these manage queued commands:  
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }
};

#endif // __UFLog_h__
