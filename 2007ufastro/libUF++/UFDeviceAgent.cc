#if !defined(__UFDeviceAgent_cc__)
#define __UFDeviceAgent_cc__ "$Name:  $ $Id: UFDeviceAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDeviceAgent_cc__;

#include "UFDeviceAgent.h"

// static vars:
bool UFDeviceAgent::_verbose= false;
bool UFDeviceAgent::_sim= false;

// static funcs:
int UFDeviceAgent::main(int argc, char** argv, char** envp) {
  UFDeviceAgent ufs("UFDeviceAgent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// non-static stuff:
UFDeviceAgent::CmdInfo::CmdInfo() { // default c-tor should be null/no-op command, or default configure?
  clientinfo = "client@connect_time~hostname~hostIP";
  time_received = UFRuntime::currentTime();
  time_submitted = "yyyy:ddd:hh:mm:ss:uuuuuu"; // identical or very nearly to cmd_time
  time_completed = "yyyy:ddd:hh:mm:ss:uuuuuu"; // i.e. time of device reply
  status_cmd = "unknown"; // "rejected", "active", "succeeded", "failed"
  status_config =  "idle-UnConfigured"; // "idle-UnConfigured", "idle-DefaultConfig", "idle-<ConfigName>", "busy-<ConfigName>"
  // (nearly)current or past times indicate immediate actions; future indicates queued actions
  cmd_time = "yyyy:ddd:hh:mm:ss:uuuuuu"; 
  // these are all now vector<string> objects:
  cmd_name.clear();
  cmd_params.clear();
  // command implementations should be new-line terminated strings
  cmd_impl.clear();
  // device response may be none, single-line, or multi-line
  cmd_reply.clear();
  string _remWhites(const string* s);
}

string UFDeviceAgent::CmdInfo::_remWhites(const string& s) {
  if( s.length() <= 0 ) {
    clog<<"UFDeviceAgent::CmdInfo::_remWhites> ?empty string"<<endl;
    return s;
  }

  char* cs = (char*) s.c_str();
  // eliminate white multiples
  bool curr= false, prev= false;
  const size_t len = s.length();
  char* buff = new char[1+len]; memset(buff, 0, 1+len);
  size_t pos=0, cnt=0;
  for( pos = 0; pos < len; ++pos ) {
    prev = curr;
    if( cs[pos] == ' ' || cs[pos] == '\t' )
      curr = true;
    else
      curr = false;
    if( prev && curr )
      continue;
    else
      buff[cnt++] = cs[pos];
  }

  cs = buff;
  // eliminate leading whites
  while( *cs == ' ' || *cs == '\t' ) ++cs;
  string ret = cs;
  delete buff;

  // eliminate trailing whites
  pos = ret.rfind(" ");
  while( pos != string::npos && pos == ret.length()-1 ) {
    ret = ret.substr(0, pos);
    pos = ret.rfind(" ");
  }

  return ret;    
}

UFDeviceAgent::CmdInfo::CmdInfo(const UFStrings& ufs, const UFDeviceConfig* devconf) {
  clientinfo = "client@connect_time~hostname~hostIP";
  time_received = UFRuntime::currentTime();
  time_submitted = "yyyy:ddd:hh:mm:ss:uuuuuu"; // identical or very nearly to cmd_time
  time_completed = "yyyy:ddd:hh:mm:ss:uuuuuu"; // i.e. time of device reply
  status_cmd = "rejected"; // "rejected", "queued", "active", "succeeded", "failed"
  status_config =  "idle-UnConfigured"; // "idle-UnConfigured", "idle-DefaultConfig", "idle-<ConfigName>", "busy-<ConfigName>"
  // (nearly)current or past times indicate immediate actions; future indicates queued actions
  cmd_time = "yyyy:ddd:hh:mm:ss:uuuuuu"; 
  cmd_name.clear(); // "Raw" or should indicate "device::CmdName", e.g. "mce4::sim"
  cmd_params.clear(); // if any, e.g. "3 128 128 1000"
  cmd_impl.clear(); // "sim 3 128 128 1000\n"
  cmd_reply.clear(); // 
  // reset with ufs content & global content
  // check client name, lookup its entry in theConnections, and concat...
  // ufs.name should provide clientID & device name
  if( strlen(ufs.cname()) > 0 )
    clientinfo = ufs.name(); // for now...
  cmd_time = ufs.timeStamp();
  // the following really should be delegated to a UFDeviceConfig func:
  if( devconf != 0 ) {
    clog<<"UFDeviceAgent::CmdInfo> sorry don't know how to use device config objects..."<<endl;
  }
  // assume ufs values are "paired" in this order:
  // 1st cmd_name, 1st cmd_params, 2nd cmd_name, 2nd cmd_params, etc.
  string cmdname, cmdparam, cmdimpl;
  for( int offset = 0; offset < ufs.elements()-1; offset += 2 ) {
    // strip off any leading white spaces:
    cmdname = _remWhites(ufs[offset]);
    cmd_name.push_back(cmdname); // assuming cmd is found in table
    cmdparam = _remWhites(ufs[1+offset]);
    cmd_params.push_back(cmdparam); // params parsed via table?
    // currently all comands are "raw commands", i.e. assume params provide complete command
    cmdimpl = cmdparam; // nominally
    //clog<<"UFDeviceAgent::CmdInfo()> cmdimpl: "<<cmdimpl<<endl;
    cmd_impl.push_back(cmdimpl);
  }
}

int UFDeviceAgent::CmdInfo::cmdDeque(deque< string >& cmds) {
  // just cmd_name and cmd_params
  cmds.clear();
  if( cmd_name.size() != cmd_params.size() )
    clog<<"UFDeviceAgent::CmdInfo::cmdDeque> mismatched cmd/params cnt, using min."<<endl;
  int nc = min(cmd_name.size(), cmd_params.size());
  for( int i = 0; i < (int)cmd_name.size(); ++i ) {
    cmds.push_back( cmd_name[i] ); cmds.push_back( cmd_params[i] );
  }
  return nc;
}

UFStrings* UFDeviceAgent::CmdInfo::ufStrings(const string& name) {
  vector < string > s;
  s.push_back( clientinfo );
  s.push_back( time_received );
  s.push_back( time_submitted );
  s.push_back( time_completed );
  s.push_back( status_cmd );
  s.push_back( status_config );
  s.push_back( cmd_time );

  for( int i = 0; i < (int)cmd_name.size(); ++i )
    s.push_back( cmd_name[i] );

  for( int i = 0; i < (int)cmd_params.size(); ++i )
    s.push_back( cmd_params[i] );

  for( int i = 0; i < (int)cmd_impl.size(); ++i )
    s.push_back( cmd_impl[i] );

  for( int i = 0; i < (int)cmd_reply.size(); ++i )
    s.push_back( cmd_reply[i] );

  return new UFStrings(name, s);
}

// any additional strings...
UFStrings* UFDeviceAgent::CmdInfo::ufStrings(const string& name, const vector<string>& vs) {
  vector < string > s;
  s.push_back( clientinfo );
  s.push_back( time_received );
  s.push_back( time_submitted );
  s.push_back( time_completed );
  s.push_back( status_cmd );
  s.push_back( status_config );
  s.push_back( cmd_time );

  for( int i = 0; i < (int)cmd_name.size(); ++i )
    s.push_back( cmd_name[i] );

  for( int i = 0; i < (int)cmd_params.size(); ++i )
    s.push_back( cmd_params[i] );

  for( int i = 0; i < (int)cmd_impl.size(); ++i )
    s.push_back( cmd_impl[i] );

  for( int i = 0; i < (int)cmd_reply.size(); ++i )
    s.push_back( cmd_reply[i] );

  for(int i= 0; i < (int)vs.size(); ++i )
    s.push_back(vs[i]);

  return new UFStrings(name, s);
}

UFDeviceAgent::UFDeviceAgent(int argc, char** argv,
			     char** envp) : UFRndRobinServ(argc, argv, envp),
                                            _active(0), _completed(), _config(0),
                                            _flush(0.0), _DevHistSz(1000) {}

UFDeviceAgent::UFDeviceAgent(const string& name,
			     int argc, char** argv,
			     char** envp) : UFRndRobinServ(name, argc, argv, envp), 
                                            _active(0), _completed(), _config(0),
                                            _flush(0.0), _DevHistSz(1000) {}
  
// this is the key virtual function to override,
// default behavior is as usual just an echo:
UFProtocol* UFDeviceAgent::action(UFDeviceAgent::CmdInfo* act) {
  // use the annex/iocomm port to submit the action command and get reply:
  _active = act;
  vector< string > reply;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    // deal with queries locally first
    int qcnt = query(cmdname, reply);
    if( qcnt < 0 )
      clog<<"UFDeviceAgent::> error checking for query?"<<endl;
    // check if requires device iterraction, or is a query that can be handled 
    // without actual device interaction
    // if( _config && _config->interaction(cmdname, cndimpl) ) ...
    reply.push_back(cmdname+"!! OK"); // echo back OK
  }
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));
  return act->ufStrings(name(), reply);
}

int UFDeviceAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& replies) {
  // default is one (echo) reply
  replies = new vector< UFProtocol* >;
  UFProtocol* ufp = action(act);
  if( ufp )
    replies->push_back(ufp);

  return (int)replies->size();
}

// override this to create the basic FITS header from agent attributes:
UFFITSheader* UFDeviceAgent::basicFITSheader() {
  return new UFFITSheader();
}

// override this to create the basic FITS header from agent & obs attributes:
UFFITSheader* UFDeviceAgent::basicFITSheader(UFObsConfig* oc) {
  if( oc == 0 )
    return basicFITSheader();

  return new UFFITSheader();
}

int UFDeviceAgent::query(const string& q, vector<string>& qreply) {
  // this needs more generalization... later
  if( _config == 0 ) {
    return 0;
  }
  vector< string >& qc = _config->queryCmds();
  int cnt = (int)qc.size(); // init list of know query cmds
  if( cnt <= 0 ) {
    clog<<"UFDeviceAgent::query> uninitialized query command table!"<<endl;
    return 0;
  }
  for( int i = 0; i < (int)qc.size(); ++i ) {
    if( q.find(qc[0]) != string::npos ) {
      //"clientlist", return UFRndRobinServ::theConnections;
      vector <string> vc;
      int cnt = connectList(name(), *_theConnections, qreply);
      if( cnt <= 0 ) 
	break;
    }
    else if( q.find(qc[1]) != string::npos ) {
      //"completedlist", return UFDeviceAgent::CmdList _completed;
    }
    else if( q.find(qc[2]) != string::npos ) {
      //"queuedlist", return UFDeviceAgent::CmdList _queued;
    }
    else if( q.find(qc[3]) != string::npos ) {
      //"stateOfdevice," return _active or last/latest completion
    }
    else {
      clog<<"UFDeviceAgent::servReqs> unknown query: "<<q<<endl;
    }
  }
  return qreply.size();
} 

// this is called by UFRndRobinServ::exec once all connected client 
// messages have been received 
int UFDeviceAgent::servImmed() {
  // this needs more generalization... later
  if( _config == 0 ) {
    clog<<"UFDeviceAgent::servImmed> no config?"<<endl;
    return 0;
  }
  if( _immed.size() == 0 ) {
    clog<<"UFDeviceAgent::servImmed> no reqs. in _immed table?"<<endl;
    return 0;
  }

  vector< string >& qc = _config->queryCmds();
  vector< string >& ac = _config->actionCmds();
  int cnt = (int)qc.size();
  if( cnt < 0 ) {
    clog<<"UFDeviceAgent::servImmed> uninitialized query command table?"<<endl;
  }
  cnt = (int)ac.size();
  if( cnt < 0 ) {
    clog<<"UFDeviceAgent::servImmed> uninitialized action command table?"<<endl;
    return 0;
  }
  // for each client that has submitted a cmd/query request:
  // segregate req. types into queries and actions
  // traverse list of requests and construct _arrivals, action_reqs, and query_reqs
  // future cmds are queues, all others immediately call action or query
  cnt= 0;
  UFRndRobinServ::MsgTable::iterator it = _immed.begin();
  for( ; it != _immed.end(); ++it, ++cnt ) {
    UFSocket* soc = it->first;
    UFProtocol* ufp = it->second;
    if( ufp == 0 || soc == 0 ) // skip this client?
      continue;

    UFStrings *ufs = dynamic_cast< UFStrings* > (ufp); // client request should always be strings
    if( ufs == 0 ) { // skip this client?
      clog<<"UFDeviceAgent::servImmed> client sent non-text UFProtocol type: "<<ufp->description()<<endl;
      continue;
    }
    string reqnam = ufp->name();
    if( _verbose )
      clog<<"UFDeviceAgent::servImmed> "<<(1+cnt)<<" of "<<_immed.size()<<" req: "<<reqnam<<endl;
    if( ufs == 0 || ufs->elements() <= 0)  { // skip this client? expecting UFStrings
      clog<<"UFDeviceAgent::servImmed> client sent empty request?"<<endl;
      continue;
    }
    // create a CmdInfo from req and either process it now or later
    if( _verbose ) {
      clog<<"UFDeviceAgent::servImmed> new client req: "<<ufs->name()<<" elemcnt: "<<ufs->elements()<<endl;
    //for( int i = 0; i < ufs->elements(); ++i ) clog<<"UFDeviceAgent::servImmed> "<<i+1<<" "<<(*ufs)[i]<<endl;
    }
    int replycnt= 0;
    vector< UFProtocol* >* replies = 0;
    UFProtocol* reply= 0; // reply can be any UFProtocol object, not just UFStrings
    UFDeviceAgent::CmdInfo* req = new CmdInfo(*ufs);
    delete ufs; // assume no longer needed, ensure no memory leak here (thanks frank & davidr)
    /*
    if( req->cmd_time > UFRuntime::currentTime() ) {// move into the queued cmd list
      _queued.insert(UFRndRobinServ::MsgTable::value_type(soc, ufp));
      req->status_cmd = "queued";
      reply = req->ufStrings(name()+"> queued..."); // turn the (modified) req. into a UFStrings
    }
    */
    //else {
      // action clears and set replies vec.
      replycnt = action(req, replies); // process immediately, reset req. status, etc.
    //}

    // reply to client with result(s)
    // don't create sendThread on shutdown
    bool sendthread = false; //threaded();
    if( !_shutdown && replies && replycnt > 0 && sendthread) { 
      // threaded agent can send reply(ies) back to client asynchronously via new thread:
      // all heap allocations (except the soc*) should be deleted by the thread
      pthread_t pthrd = soc->sendThread(replies);
      if( pthrd == 0 ) {
	clog<<"UFDeviceAgent::servImmed> unable to create send thread"<<endl;
        for( int i = 0; i < replycnt && replies && !_shutdown; ++i ) {
	  delete (*replies)[i];
        }
	delete replies;
      }
      else if( _verbose )
	clog<<"UFDeviceAgent::servImmed> sendThread: "<<pthrd<<", soc: "<<soc->description()<<endl;
    }
    else if( !_shutdown && replies && replycnt > 0  ) { // don't use async/threaded send:
      //if( _verbose )
        clog<<"UFDeviceAgent::servImmed> synchronous send for "<<soc->description()<<endl;
      for( int i = 0; i < replycnt && replies && !_shutdown; ++i ) {
	reply = (*replies)[i]; soc->send(reply); delete reply;
      }
      delete replies;
    }
  }
  // and clear immed. table
  _immed.clear();

  return cnt;
}

int UFDeviceAgent::servQueued() {
  string newnam;
  int cnt= 0;
  UFRndRobinServ::MsgTable::iterator i = _queued.begin();
  for( ; i != _queued.end(); ++i) {
    // don't bother replying to client with result of queued
    // action -- the client may be long gone along with its socket
    //UFSocket* soc = i->first;
    UFProtocol* ufp = i->second;
    if( ufp->timeStamp() < UFRuntime::currentTime() ) {
      string reqnam = ufp->name();
      UFStrings *ufs = dynamic_cast< UFStrings* > (ufp);
      UFDeviceAgent::CmdInfo* req = new CmdInfo(*ufs);
      delete ufs; // assume no longer needed, ensure no memory leak here (thanks frank & davidr)
      clog<<"UFDeviceAgent::servReqs> queued request from "<<req->clientinfo<<endl;
      UFProtocol* reply = action(req); // reply should be logged to UF status service (via UFLog)
      if( reply == 0 ) {
        clog<<"UFDeviceAgent::servQueued> ? failed to perform queued action command: "<<req->clientinfo<<endl;
      }
      else {
      //clog<<"UFDeviceAgent::servQueued> performed queued action command: "
      //    <<reply->name()<<"@"<<reply->timeStamp()<<endl;
      }
      _queued.erase(i);
      ++cnt;
    }
  }
  return cnt;
} // queued actions

int UFDeviceAgent::options(string& servlist) {
  int listenport = UFRndRobinServ::options(servlist);
  if( _config == 0 )
    _config = new UFDeviceConfig();

  string arg = findArg("-sim"); // epics host/db name
  if( arg != "false" )
    _sim = true;

  arg = findArg("-v");
  if( arg != "false" ) 
    UFRndRobinServ::_verbose = _verbose = true;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" && _config != 0 )
    _config->_tshost = arg;

  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" && _config != 0 )
    _config->_tsport = atoi(arg.c_str());

  if( _config->_tsport && _config != 0 ) { // 7001 <= _tsport <= 7008
    listenport = 52000 +  _config->_tsport - 7000;
  }
  else {
    listenport = 0;
  }
  //if( _verbose )
  // clog<<"UFDeviceAgent::options> set listen port to: "<<listenport<<endl;

  return listenport;
}

// override base class startup here to 
// specify a specialized devic config and
// to do things in a different order
void UFDeviceAgent::startup() {
  bool thrd = threaded(); // if threaded, override this function to use sigWaitThread
  if( !thrd ) // non-threaded, set the sig handler 
    setSignalHandler(UFRndRobinServ::sighandlerDefault, thrd);
  //clog << "UFDeviceAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  init();
  if( _config->_devIO == 0 ) {
    clog<<"UFDeviceAgent::startup> failed to init DeviceCOnfig, abort..."<<endl;
    exit(1);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFDeviceAgent::startup> listening on port= " <<listenport
       <<", with server soc: "<<_theServer.description()<<endl;
  return;  
}

UFTermServ* UFDeviceAgent::init(const string& host, int port) {
  return _config->connect(host, port);
}
    
#endif // UFDeviceAgent
