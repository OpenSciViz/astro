#if !defined(__UFECSTOP_CC__)
#define __UFECSTOP_CC__ "$Name:  $ $Id: UFECStop.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECSTOP_CC__;

#include "UFECStop.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFECStop::stopit(void* p) {
  // stop really just needs?
  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFECSetup::EnvParm* parms = arg->_parms;
  UFCAR* car = arg->_car;

  // simple simulation...
  clog<<"UFECStop::stopping> ..."<<endl;
  if( parms ) {
    clog<<"UFECStop::stoping> environment: "<<parms->_MOSKelv<<", "<<parms->_CamAKelv<<", "<<parms->_CamBKelv<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose stop func. 

/// arg:
int UFECStop::stop(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  // create and run pthread and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECStop::stopit, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECStop::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".ctrl1";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // non null value should indicate stop action 
  attachInOutPV(this, pva);

  pvname = _name + ".ctrl2";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // non null value should indicate stop action mark
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform stop 
int UFECStop::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFECStop::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".ctrl1";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFECStop::genExec> stop control loop 1 ..."<<endl;
    pva->clearVal(); // clear
  }

  pvname = _name + ".ctrl2";
  pva = (*this)[pvname];
  input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFECStop::genExec> stop control loop 2 ..."<<endl;
    pva->clearVal(); // clear
  }

  UFECSetup::EnvParm* p = new UFECSetup::EnvParm;
  int stat = stop(p, timeout, this, _car);  // perform requested stop

  if( stat < 0 ) { 
    clog<<"UFStop::genExec> failed to start stop pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECStop::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFECSTOP_CC__
