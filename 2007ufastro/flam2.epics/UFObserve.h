#if !defined(__UFObserve_h__)
#define __UFObserve_h__ "$Name:  $ $Id: UFObserve.h,v 0.8 2006/01/27 21:20:59 hon Exp $"
#define __UFObserve_H__(arg) const char arg##Observe_h__rcsId[] = __UFObserve_h__;

#include "UFCAD.h"

class UFObserve : public UFCAD {
public:
  struct Parm {
    int _timeout, _index, _wcpix1, _wcpix2;
    // datalabel and datamode should/can be set from SetDHS CAD...
    string _datalabel, _datamode, _usrinfo;
    inline Parm(const string& usrinfo= "RichardElston", const string& label= "Test", const string& mode= "Save",
		int t=10, int idx= 1, int wcpx1= 1023, int wcpx2= 1023) :
      _timeout(t), _index(idx), _wcpix1(wcpx1), _wcpix2(wcpx2), _datalabel(label) , _datamode(mode), _usrinfo(usrinfo) {}
    inline ~Parm() {}
  };
  // thread arg:
  struct OThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline OThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~OThrdArg() { delete _parms; }
  };

  inline UFObserve(const string& instrum= "instrum") : UFCAD(instrum+":observe") { _create(); }
  inline virtual ~UFObserve() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // thread entry func.
  static void* observation(void* p= 0);
  /// general instrument seq. observe func. (non datum)
  /// IS. starts a new daemon thread for every propper seq.
  static int observe(UFObserve::Parm* p, int timeout= 10, UFGem* rec= 0,  UFCAR* car= 0); 

  // InstrumSetup and ObsSetup have their own version of this. On starting the observation
  // (processing this CAD) one the ObsSetup bias may override the InstrumSetp value. The
  // chosen value is copied here, then both of the 'setup' values should be 'cleared/nulled'.
  // this value need not be 'cleared/nulled' on processing the Observe CAD, can be re-used?
  static string _BiasMode;

protected:
  // ctor helper
  void _create();
};

#endif // __UFObserve_h__
