#if !defined(__UFCCINIT_CC__)
#define __UFCCINIT_CC__ "$Name:  $ $Id: UFCCInit.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCINIT_CC__;

#include "UFCCInit.h"
#include "UFInit.h"
#include "UFCCSetup.h" // this includes UFClientSocket.h 
#include "UFCAServ.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

string* UFCCInit::_datumCnt= 0;
// ctors:

// static funcs:
void UFCCInit::_clearDatumCnt() {
  string recname = UFCAServ::_instrum + ":sad:DatumCnt";
  if( _datumCnt == 0 ) {
    _datumCnt = new string(recname);
  }
  else if( _datumCnt->empty() ) {
    *_datumCnt = recname;
  }

  UFSIR* sad = UFSIR::_theSAD[*_datumCnt];
  if( sad == 0 ) {
    clog<<"UFCCInit::_clearDatumCnt> _theSAD lacks: "<<*_datumCnt<<endl;
    return;
  }
  UFPVAttr* pva = sad->pinp();
  double val = pva->getNumeric();
  if( val < 0 ) {
    // clear the datum count input field
    pva->clearVal();
    // call genExec to process record input to output
    sad->start();
  }
}


// pthread static func:
void* UFCCInit::connection(void* p) {
  UFInit::_CCthrdId = pthread_self();
  // just connects, if needed, to portescap motor indexor agent
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  string option = arg->_option;
  //std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec ) {
    name = rec->name();
    if( car == 0 && rec->hasCAR() ) 
      car = dynamic_cast<UFCAR*> (rec->hasCAR());
  }

  int miport, bcport;
  string host = UFCCSetup::agentHostAndPort(miport, bcport);
  clog<<"UFCCInit::connection> "<<host<<", MotorAgent on port: "<<miport<<", BarCodeAgent on port: "<<bcport<<endl;

  if( UFCCSetup::_motoragntsoc != 0 ) {
    UFCCSetup::_motoragntsoc->close();
    delete UFCCSetup::_motoragntsoc; UFCCSetup::_motoragntsoc= 0;
  }
  UFCCSetup::_motoragntsoc = new UFClientSocket;
  bool block= false, validsoc= true;
  int socfd = UFCCSetup::_motoragntsoc->connect(host, miport, block);
  if( socfd < 0 ) {
    validsoc = false;
    clog<<"UFCCInit::connection> motor agent failed"<<endl;
  }
  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  validsoc = connected(UFCCSetup::_motoragntsoc);
  if( validsoc ) {
    //UFTimeStamp greet(name.c_str());
    UFTimeStamp greet(name);
    int ns = UFCCSetup::_motoragntsoc->send(greet);
    ns = UFCCSetup::_motoragntsoc->recv(greet);
    //if( _verbose )
      clog<<"UFCCInit::connect> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;
    if( option.find("Firmware") != string::npos ) {
      UFCCSetup::nomotion(p); // since this frees p (arg)
      arg = 0; // insure it does not get freed twice!
    }
  }
  else {
    clog<<"UFCCInit::connect> unable to transact greeting with portescap motor agent due to invalid connection..."<<endl;
  }                                                                                        

  // and barcode agent
  if( UFCCSetup::_barcdagntsoc != 0 ) {
    UFCCSetup::_barcdagntsoc->close();
    delete UFCCSetup::_barcdagntsoc; UFCCSetup::_barcdagntsoc= 0;
  }
  UFCCSetup::_barcdagntsoc = new UFClientSocket;
  block= false; validsoc= true;
  socfd = UFCCSetup::_barcdagntsoc->connect(host, bcport, block);
  if( socfd < 0 ) {
    validsoc = false;
    clog<<"UFCCInit::connection> barcode failed"<<endl;
  }
  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  validsoc = connected(UFCCSetup::_barcdagntsoc);
  if( validsoc ) {
    //UFTimeStamp greet(name.c_str());
    UFTimeStamp greet(name);
    int ns = UFCCSetup::_barcdagntsoc->send(greet);
    ns = UFCCSetup::_barcdagntsoc->recv(greet);
    //if( _verbose )
      clog<<"UFCCInit::connect> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;
  }
  else {
    clog<<"UFCCInit::connect> unable to transact greeting with portescap motor agent due to invalid connection..."<<endl;
  }                                                                                           

  // free arg
  delete arg;

  // no need for agent(s) to set car if the connection(s) succeeded...
  //UFPosixRuntime::sleep(0.5);
  if( validsoc ) {
    // if this is first cc init, clear the DatumCnt
    _clearDatumCnt();
    if( rec ) {
      rec->setIdle();
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && car != 0 )
        car->setIdle();
    }
    else if( car != 0 ) {
      car->setIdle();
    }
    clog<<"UFCCInit::connect> completed."<<endl;
  }
 
  // exit daemon thread
  return p;
}

/// general purpose connect func. 
/// arg: mechName, motparm*
int UFCCInit::connect(std::map< string, UFCCSetup::MotParm* >* mparms,
		      int timeout, UFGem* rec, UFCAR* car, const string& options) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }
  clog<<"UFCCInit::connect> options: "<<options<<endl;

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car, options);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCInit::connection, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFCCInit::_create() {
  string host = UFRuntime::hostname();
  string pvname = _name + ".host";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, host); // host
  attachInOutPV(this, pva);

  pvname = _name + ".port";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52024); // port
  attachInOutPV(this, pva);

  pvname = _name + ".SetFirmware";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform initial connection to portescap motor agent
int UFCCInit::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFCCInit::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // which pv mechNames are non null?
  string options;
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >; // set firmware list
  string input, pvname = _name + ".SetFirmware";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    // origin all or individual indexor
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      options = "SetFirmware";

      pva->clearVal();
    }
  }

  pvname =  _name + ".host";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    // this pv is indicates the agent host for connection
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      UFCCSetup::setHost(input);
      pva->clearVal();
    }
  }

  pvname =  _name + ".port";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    // this pv is indicates the agent port for connection
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      char* cp = (char*) input.c_str();
      while( *cp == ' ' || *cp == '\t' ) ++cp;
      if( isdigit(cp[0]) ) UFCCSetup::setPort(atoi(cp));
      pva->clearVal(); // clear
    }
  }

  int stat = connect(mechs, timeout, this, _car, options);  // perform requested abort

  if( stat < 0 )
    clog<<"UFCCInit::genExec> failed to start connection pthread..."<<endl;

  return stat;
} // genExec

int UFCCInit::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();

  if( pvname.find("direction") != string::npos ) {
    if( input.find("+") != string::npos || input.find("1") != string::npos ) 
      return 0; // home + ok

    if( input.find("-") != string::npos || input.find("0") != string::npos ) 
      return 0; // home - ok
  }

  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      if( input == "" )
        return 0; // clear ok
      if( input.find("null") != string::npos || input.find("Null") != string::npos || input.find("NULL") != string::npos )
        return 0; // null + ok

      char* hv = (char*)input.c_str();
      while( *hv == ' ' || *hv == '\t' )
        ++hv; // ignore white spaces

      if( isdigit(hv[0]) ) // home velocity must be alphanumric
        return 0;
    }
  }

  return -1;
} // validate


#endif // __UFCCINIT_CC__
