#if !defined(__UFSIR_cc__)
#define __UFGISIR_cc__ "$Name:  $ $Id: UFGISIR.cc 14 2008-06-11 01:49:45Z hon $"

#include "UFGISIR.h"
__UFGISIR_H__(__UFGISIR_cc);

#include "UFPVAttr.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"

#include "strstream"

// statics
UFGISIR* UFGISIR::create(const string& instrum) {
  string recname = instrum + ":sad::gis";
  vector< UFPVAttr* > inputs, outputs;

  string pvname;
  pvname = recname; pvname += ".packet"; //
  inputs.push_back(new UFPVAttr(pvname, "null"));

  pvname = recname; pvname += ".inp01line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp02line"; // 
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp03line"; // 
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp04line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp05line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp06line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp07line"; // 
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp08line"; // 
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp09line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp10line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp11line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp12line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp13line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp14line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp15line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp16line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp17line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp18line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp19line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".inp20line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".out01line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".out02line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".out03line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  pvname = recname; pvname += ".out04line"; //
  inputs.push_back(new UFPVAttr(pvname, (int)0));

  //UFGISIR* gis = new (nothrow) UFGISIR(recname, inputs, outputs);
  UFGISIR* gis = new UFGISIR(recname, "null", inputs, outputs);

  return gis;
}

// virtuals
// the processing & 'genSub' record processing entry point:
int UFGISIR::genExec(int timeout) {
  UFPVAttr *pin = pinp(), *pout = pval();
  if( pin == 0 || pout == 0 ) {
    if( _verbose )
      clog<<"UFGISIR::genExec> pin: "<<(size_t)pin<<", pout: "<<(size_t)pout<<endl;
    return -1;
  }
  if( _verbose )
    clog<<"UFGISIR::genExec> pin: "<<pin->name()<<", pout: "<<pout->name()<<endl;

  vector<UFPVAttr*> inpline, outline;
  inpline.push_back(inp01());
  inpline.push_back(inp02());
  inpline.push_back(inp03());
  inpline.push_back(inp04());
  inpline.push_back(inp05());
  inpline.push_back(inp06());
  inpline.push_back(inp07());
  inpline.push_back(inp08());
  inpline.push_back(inp09());
  inpline.push_back(inp10());
  inpline.push_back(inp11());
  inpline.push_back(inp12());
  inpline.push_back(inp13());
  inpline.push_back(inp14());
  inpline.push_back(inp15());
  inpline.push_back(inp16());
  inpline.push_back(inp17());
  inpline.push_back(inp18());
  inpline.push_back(inp19());
  inpline.push_back(inp20());

  outline.push_back(out01());
  outline.push_back(out02());
  outline.push_back(out03());
  outline.push_back(out04());

  int linestate;
  int val = 0; // bitwise OR of all lines

  for( size_t i = 0; i < inpline.size(); ++i ) {
    linestate = (int)inpline[i]->getNumeric();
    //copyInOut(inpline[i]);
    val = val | (linestate << i );
  }

  for( size_t i = 0; i < outline.size(); ++i ) {
    linestate = (int)outline[i]->getNumeric();
    //copyInOut(outline[i]);
    val = val | (linestate << 24 + i);
  }

  // allow external test via record's standard input field:
  int inval =(int) pin->getNumeric();
  val = val | inval;
  strstream s; s<<val<<ends;
  string sv = s.str(); delete s.str();
  pout->setVal(sv);
  
  return timeout;
}



#endif // __UFGISIR_cc__
