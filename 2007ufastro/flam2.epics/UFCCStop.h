#if !defined(__UFCCStop_h__)
#define __UFCCStop_h__ "$Name:  $ $Id: UFCCStop.h,v 0.4 2004/11/10 16:47:19 hon Exp $"
#define __UFCCStop_H__(arg) const char arg##CCStop_h__rcsId[] = __UFCCStop_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFCCStop : public UFCAD {
public:
  inline UFCCStop(const string& instrum= "instrum") : UFCAD(instrum+":cc:stop") { _create(); }
  inline virtual ~UFCCStop() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* stopit(void* p);
  static int stop(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // available to IS
  inline static int stopAll(int timeout, UFGem* rec= 0, UFCAR* car= 0) { return 0; }

protected:
  // ctor helper
  void _create();
};

#endif // __UFCCStop_h__
