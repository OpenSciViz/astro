#if !defined(__UFECSETUP_CC__)
#define __UFECSETUP_CC__ "$Name:  $ $Id: UFECSetup.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECSETUP_CC__;

#include "UFECSetup.h"
#include "UFCAServ.h"
#include "UFSIR.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFSADFITS.h"

// global (static) attributes:
string UFECSetup::_host; // agent location
int UFECSetup::_ls332port= 52003;
int UFECSetup::_lvdtport= 52006;
UFClientSocket* UFECSetup::_ls332d= 0;
UFClientSocket* UFECSetup::_lvdtd= 0;

// ctors:

// static class funcs:

// default list of SADs to be used for monitoring
int UFECSetup::sadChans(const string& instrum, vector< string>& sadrecs) {
  sadrecs.clear();
  string name = instrum + ":ec";
  /*
  string dbrec = instrum;
  dbrec += ":sad:";
  sadrecs.push_back(dbrec + "MOSVacuum");
  sadrecs.push_back(dbrec + "CamVacuum");
  sadrecs.push_back(dbrec + "LS218Kelvin1");
  sadrecs.push_back(dbrec + "LS218Kelvin2");
  sadrecs.push_back(dbrec + "LS218Kelvin3");
  sadrecs.push_back(dbrec + "LS218Kelvin4");
  sadrecs.push_back(dbrec + "LS218Kelvin5");
  sadrecs.push_back(dbrec + "LS218Kelvin6");
  sadrecs.push_back(dbrec + "LS218Kelvin7");
  sadrecs.push_back(dbrec + "LS218Kelvin8");
  //sadrecs.push_back(dbrec + "LS332Kelvin1");
  //sadrecs.push_back(dbrec + "LS332Kelvin2");
  sadrecs.push_back(dbrec + "MOSKelvin");
  // cold plate -- which may be controlled but only optionally (and unlikely, according to jeff):
  sadrecs.push_back(dbrec + "CamAKelvin");
  // detector -- which will be controlled. b channel is only on/off (no range control, default PID: 1000,200,100)
  // however, there should be a 'manual offset' to 25% to insure the power level never goes to 0: 
  sadrecs.push_back(dbrec + "CamBKelvin");
  return (int) sadrecs.size();
  */

  // use SADFITS func:
  int nsad = UFSADFITS::ecSAD(sadrecs, "all", instrum);
  for( int i = 0; i < nsad; ++i ) {
    clog<<"UFECSetup::sadChans> "<<i<<" "<<sadrecs[i]<<endl;
  }
  return nsad;
 }


void* UFECSetup::envControl(void* p) {
  // use these statics to reuse prior values not presently provided for pids
  //static int _pmos= 30, _imos= 2, _dmos= 0;
  static int _pa= 30, _ia= 2, _da= 0;
  static int _pb= 30, _ib= 2, _db= 0;
  static double _amanual= 0.0, _bmanual= 0.0;
  static double _lock = 0;

  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFECSetup::EnvParm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  string Asadname = UFCAServ::_instrum + ":sad:CAMSETPA";
  string Bsadname = UFCAServ::_instrum + ":sad:CAMSETPB";
  string carname = UFCAServ::_instrum + ":ec:datumC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  clog<<"UFECSetup::envControl> "<<name<<", MOS set point (ignoring): "<<parms->_MOSKelv
      <<", CamA (cold plate) set point: "<<parms->_CamAKelv
      <<", CamB (detector) set point: "<<parms->_CamBKelv<<endl;

  int err= 0;
  string errmsg;
  // on error or simulation...
  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && !err) 
	c->setIdle();
      else if( c != 0 && c != car && err ) {
        c->setErr(errmsg, err);
      }
    }
    return p;
  }

  bool validsoc= false;
  deque< string > cmdvec, cmdlvdt; // req. bundle is pairs of strings:
  // if agent should set car on completion or error, indicate car channel (pvname):
  // current agents will not do the right thing for this yet...
  cmdvec.push_back("CAR"); cmdvec.push_back(carname);
  clog<<"UFECSetup::envControl> "<<cmdvec[0]<<", "<<cmdvec[1]<<endl;

  // handle LVDT ON/OFF, etc.
  if( parms->_OnOffLVDT >= 0 ) { // transact with ufglvdtd
    cmdlvdt.push_back("CAR"); cmdlvdt.push_back(carname);
    clog<<"UFECSetup::envControl> LVDT "<<cmdlvdt[0]<<", "<<cmdlvdt[1]<<endl;
    if( parms->_OnOffLVDT == 0 ) {
      cmdlvdt.push_back("Off"); cmdlvdt.push_back("True");
    }
    else {
      cmdlvdt.push_back("On"); cmdlvdt.push_back("True");
    }
    clog<<"UFECSetup::envControl> LVDT "<<cmdlvdt[2]<<", "<<cmdlvdt[3]<<endl;
    validsoc = connected(UFECSetup::_lvdtd);
    if( ! validsoc ) {
      clog<<"UFECSetup::envControl> unable to transact request with LVDT agent due to invalid connection..."<<endl;
      err = 1; errmsg = "EnvSetup invalid agent socket ";
    }
  }
 
  if( err == 0 && cmdlvdt.size() > 0 ) {
    // send entire cmd bundle:
    UFStrings req("OnOffLVDT", cmdlvdt);
    //for( int i= 0; i < req.elements(); ++i )
    // clog<<"UFECSetup::envControl> "<<i<<" "<<req[i]<<endl;
    int ns = UFECSetup::_lvdtd->send(req);
    if( ns <= 0 ) {
      clog<<"UFECSetup::envControl> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = 2; errmsg = "OnOffLVDT transaction failed ";
    }
    else { //if( _verbose )
      clog<<"UFECSetup::envControl> sent req: "<< req.name() << " " << req.timeStamp() <<endl;
    }
  }
      
  // Lakeshore temperature control
  if( parms->_AManual != INT_MIN ) _amanual = parms->_AManual;
  if( parms->_BManual != INT_MIN ) _bmanual = parms->_BManual;
  if (parms->_Lock != INT_MIN) _lock = parms->_Lock;

  // ignore MOS for now, control loop 1 == camA (cold plate), control loop 2 == camB (detector)
  if( parms->_PCamA != INT_MIN ) _pa = parms->_PCamA;
  if( parms->_ICamA != INT_MIN ) _ia = parms->_ICamA;
  if( parms->_DCamA != INT_MIN ) _da = parms->_DCamA;
  if( parms->_PCamA != INT_MIN || parms->_ICamA != INT_MIN ||  parms->_DCamA != INT_MIN) {
    cmdvec.push_back("SAD"); cmdvec.push_back(Asadname);
    clog<<"UFECSetup::envControl> sad: "<<Asadname<<endl;
    string ca = "CMODE 1,1";
    cmdvec.push_back("Raw"); cmdvec.push_back(ca);
    strstream spid1;
    spid1 << "PID 1, ";
    spid1 << _pa << ", " << _ia<< ", " << _da << ends;
    string pid1 = spid1.str(); delete spid1.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(pid1); 
    clog<<"UFECSetup::envControl> Raw, "<<ca<<" "<<pid1<<endl;
  }

  if( parms->_CamAKelv != INT_MIN ) {
    strstream sp1;
    sp1 << "SETP 1, ";
    sp1 << parms->_CamAKelv << ends;
    string setp1 = sp1.str(); delete sp1.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(setp1); 
    clog<<"UFECSetup::envControl> Raw, "<<setp1<<endl;
  }

  int onoff= 0;
  if( parms->_OnOffCamA != INT_MIN ) {
    string ca = "CSET 1,A,0,2";
    //string ca = "CSET?1"; 
    //cmdvec.push_back("Raw"); cmdvec.push_back(ca); 
    //clog<<"UFECSetup::envControl> Raw, "<<ca<<endl;
    if( parms->_OnOffCamA > 0 ) onoff = 1;
    strstream rca;
    rca << "RANGE " << onoff <<ends;
    ca = rca.str(); delete rca.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(ca); 
    clog<<"UFECSetup::envControl> Raw, "<<ca<<endl;
    //strstream mca;
    //mca << "MOUT 1, " << onoff <<ends;
    //ca = mca.str(); delete mca.str();
    //cmdvec.push_back("Raw"); cmdvec.push_back(ca); 
    //clog<<"UFECSetup::envControl> Raw, "<<ca<<endl;
  }

  if(parms->_RCamA != INT_MIN ) {
    strstream ra;
    ra << "RANGE ";
    ra << parms->_RCamA << ends;
    string heatra = ra.str(); delete ra.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(heatra); 
    clog<<"UFECSetup::envControl> Raw, "<<heatra<<endl;
  }

  if(parms->_AManual != INT_MIN ) {
    strstream sm;
    sm << "MOUT 1, ";
    sm << parms->_AManual << ends;
    string s = sm.str(); delete sm.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(s); 
    clog<<"UFECSetup::envControl> Raw, "<<s<<endl;
  }

  if( parms->_PCamB != INT_MIN ) _pb = parms->_PCamB;
  if( parms->_ICamB != INT_MIN ) _ib = parms->_ICamB;
  if( parms->_DCamB != INT_MIN ) _db = parms->_DCamB;
  if( parms->_PCamB != INT_MIN || parms->_ICamB != INT_MIN ||  parms->_DCamB != INT_MIN) {
    cmdvec.push_back("SAD"); cmdvec.push_back(Bsadname);
    clog<<"UFECSetup::envControl> sad: "<<Bsadname<<endl;
    string cb = "CMODE 2,1";
    cmdvec.push_back("Raw"); cmdvec.push_back(cb);
    strstream spid2;
    spid2 << "PID 2, ";
    spid2 << _pb << ", " << _ib << ", " << _db << ends;
    string pid2 = spid2.str(); delete spid2.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(pid2);
    clog<<"UFECSetup::envControl> Raw, "<<pid2<<endl;
  }

  if( parms->_CamBKelv != INT_MIN ) {
    strstream sp2;
    sp2 << "SETP 2, ";
    sp2 << parms->_CamBKelv << ends;
    string setp2 = sp2.str(); delete sp2.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(setp2);
    clog<<"UFECSetup::envControl> Raw, "<<setp2<<endl;
  }

  onoff = 0;
  if( parms->_OnOffCamB != INT_MIN ) {
    string cb = "CSET 2,B,0,2";
    //string cb = "CSET?2"; 
    //cmdvec.push_back("Raw"); cmdvec.push_back(cb); 
    //clog<<"UFECSetup::envControl> Raw, "<<cb<<endl;
    if( parms->_OnOffCamB > 0 ) onoff = 1;
    strstream rcb;
    //rcb << "MOUT 2, " << onoff <<ends;
    if (onoff == 1) rcb << "ANALOG 0,3,B" << ends; else rcb << "ANALOG 0,0,B" << ends;
    cb = rcb.str(); delete rcb.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(cb); 
    clog<<"UFECSetup::envControl> Raw, "<<cb<<endl;
  }

  if( parms->_BManual != INT_MIN ) {
    strstream sm;
    sm << "MOUT 2, ";
    sm << parms->_BManual << ends;
    string s = sm.str(); delete sm.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(s); 
    clog<<"UFECSetup::envControl> Raw, "<<s<<endl;
  }

  if (parms->_Lock != INT_MIN) {
    strstream sl;
    sl << "LOCK " << parms->_Lock << ", 123";
    sl << ends;
    string slock = sl.str(); delete sl.str();
    cmdvec.push_back("Raw"); cmdvec.push_back(slock);
    clog<<"UFECSetup::envControl> Raw, "<<slock<<endl;
  }

  // free arg
  delete arg;
  validsoc = connected(UFECSetup::_ls332d);
  if( ! validsoc ) {
    clog<<"UFECSetup::envControl> unable to transact request with Lakeshore 332 agent due to invalid connection..."<<endl;
    err = 1; errmsg = "EnvSetup invalid agent socket ";
  }
 
  if( err == 0 ) {
    // send entire cmd bundle:
    UFStrings req("EnvSetup", cmdvec);
    //for( int i= 0; i < req.elements(); ++i )
    // clog<<"UFECSetup::envControl> "<<i<<" "<<req[i]<<endl;
    int ns = UFECSetup::_ls332d->send(req);
    if( ns <= 0 ) {
      clog<<"UFECSetup::envControl> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = 2; errmsg = "EnvSetup transaction failed ";
    }
    else { //if( _verbose )
      clog<<"UFECSetup::envControl> sent req: "<< req.name() << " " << req.timeStamp() <<endl;
    }
  }

  if( err ) { // on error
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && !err) 
	c->setIdle();
      else if( c != 0 && c != car && err ) {
        c->setErr(errmsg, err);
      }
    }
  }
  
  // exit daemon thread
  return p;
}

/// general PID and set point  func. (non datum)
/// arg: envparm*
int UFECSetup::environ(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  ECThrdArg* arg = new ECThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECSetup::envControl, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECSetup::_create(const string& host, int ls332port, int lvdtport) {
  UFPVAttr* pva= 0;
  _host = host; _ls332port = ls332port; _lvdtport = lvdtport;  // set global statics
  if( _host == "" ) _host = UFRuntime::hostname();
  // clog<<"UFECSetup::_create> host: "<<_host<<", LS332port: "<<_ls332port<<", LVDTport: "<<_lvdtport<<endl;
  // allow turning ctrl loops on/off
 
  string pvname = _name + ".LVDTOnOff";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);


  pvname = _name + ".MOSOnOff"; 
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSRange";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSSetPnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, (double) INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".MOSP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1000); 
  attachInOutPV(this, pva);

  pvname = _name + ".MOSI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 200); 
  attachInOutPV(this, pva);

  pvname = _name + ".MOSD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 100); 
  attachInOutPV(this, pva);

  pvname = _name + ".AManualPcnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0.0);
  attachInOutPV(this, pva);

  pvname = _name + ".CamAOnOff";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".CamARange";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name +  ".CamASetPnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, (double)INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".CamAP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 30); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamAI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 2); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamAD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".BManualPcnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0.0);
  attachInOutPV(this, pva);

  pvname = _name + ".CamBOnOff"; // on/off
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name +  ".CamBSetPnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 75.5);
  attachInOutPV(this, pva);

  pvname = _name + ".CamBP";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 30); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamBI";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 2); 
  attachInOutPV(this, pva);

  pvname = _name + ".CamBD";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".Lock";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  registerRec();

  // automatically create a SIR/SAD for each environment monitor?
  vector< string > chans;
  int mons = sadChans(UFCAServ::_instrum, chans);
  for( int i = 0; i < mons; ++i ) {
    string sadname = chans[i];
    //clog<< "UFECSetup::_create> SAD allocate: "<<sadname<<endl;
    UFSIR::_theSAD[sadname] = new UFSIR(sadname); // new UFSIR(sadname, 0.0);
  }
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
} // _create

/// virtual funcs:

int UFECSetup::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;
  if( pva == 0 )
    return 0;

  string input = pva->valString();
  char* cs = (char*) input.c_str();
  while( *cs == ' ' && *cs == '\t' )
    ++cs;

  if( isdigit(cs[0]) )
    return 0;

  return -1;
}

int UFECSetup::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCSetup::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // which pvs are non null?
  double amanual= INT_MIN, bmanual= INT_MIN, kmos= INT_MIN, kcama= INT_MIN, kcamb= INT_MIN;
  int pmos= INT_MIN, imos= INT_MIN, dmos= INT_MIN, rmos= INT_MIN, mosonoff= INT_MIN;
  int pcama= INT_MIN, icama= INT_MIN, dcama= INT_MIN, rcama= INT_MIN, aonoff= INT_MIN;
  int pcamb= INT_MIN, icamb= INT_MIN, dcamb= INT_MIN, bonoff= INT_MIN;
  int lvdtonoff= INT_MIN;
  int lock = INT_MIN;

  string pvname = _name + ".LVDTOnOff";
  UFPVAttr* pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      lvdtonoff = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }


  pvname = _name + ".MOSOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      mosonoff = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".MOSRange";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      rmos = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".MOSSetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kmos = pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".MOSP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pmos = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }
  pvname = _name + ".MOSI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      imos = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".MOSD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dmos = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamAOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      aonoff = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".AManualPcnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      amanual = pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamARange";
  pva = (*this)[pvname]; //
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      rcama = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CAMASetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kcama = pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamAP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pcama = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamAI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      icama = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamAD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dcama = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamBOnOff";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      bonoff = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".BManualPcnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      bmanual = pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamBSetPnt";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      kcamb = pva->getNumeric();
     pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamBP";
  pva = (*this)[pvname]; 
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      pcamb = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamBI";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      icamb = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".CamBD";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      dcamb = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".Lock";
  pva = (*this)[pvname];
  if( pva ) {
    if( pva->isSet() ) { // assume valid
      lock = (int) pva->getNumeric();
      pva->clearVal(); // clear
    }
  }

  UFECSetup::EnvParm* parms = new UFECSetup::EnvParm(lvdtonoff, kmos, kcama, kcamb, pmos, imos, dmos, rmos, mosonoff,
						     pcama, icama, dcama, rcama, aonoff, amanual, 
						     pcamb, icamb, dcamb, bonoff, bmanual, lock);
  int stat= environ(parms, timeout, this, _car);
  if( stat < 0 ) { 
    clog<<"UFECSetup::genExec> failed to start environment control pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

#endif // __UFECSETUP_CC__
