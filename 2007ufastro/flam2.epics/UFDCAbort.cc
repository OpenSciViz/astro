#if !defined(__UFDCABORT_DC__)
#define __UFDCABORT_DC__ "$Name:  $ $Id: UFDCAbort.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCABORT_DC__;

#include "UFDCAbort.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFDCAbort::abortion(void* p) {
  // abort really just needs the mech. names
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  /*
  int err= 0;
  string errmsg;
  */
  // simple simulation...
  clog<<"UFDCAbort::aborting> ..."<<endl;
  if( parms ) {
    clog<<"UFAbort::aborting> detector pixels: "<<parms->_w * parms->_h<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose abort func. 

/// arg: mechName, motparm*
int UFDCAbort::abort(UFDCSetup::DetParm* parms, int timeout, UFGem* rec,  UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCAbort::abortion, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCAbort::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".DataLabel";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate abort action 
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform abort mechanisms with non-null step cnts or named-position inputs
int UFDCAbort::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCAbort::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDCAbort::genExec> abort marked..."<<endl;
      pva->clearVal(); // clear
    }
  }
  UFDCSetup::DetParm* p = new UFDCSetup::DetParm;
  int stat = abort(p, timeout, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFAbort::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCAbort::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFDCABORT_DC__
