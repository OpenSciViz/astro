#if !defined(__UFGem_h__)
#define __UFGem_h__ "$Name:  $ $Id: UFGem.h,v 0.31 2007/02/02 17:09:20 hon Exp $"
#define __UFGem_H__(arg) const char arg##Gem_h__rcsId[] = __UFGem_h__;

#include "pthread.h"

// forward declaration of cas and pvattr classes:
class UFCAServ;
class UFPVAttr;
class UFCAR; // put hasCAR() virtual in base class, even though only CAD and Apply care

#include "map"
#include "deque"
#include "string"
#include "iostream"

using namespace std;

// these directives are inputs to CADs and Apply(s)
const int _ABORT  = 0; ///
const int _MARK   = 0; ///
const int _CLEAR  = 1; ///
const int _PRESET = 2; ///
const int _START  = 3; ///
const int _STOP   = 4; ///
const int _ACCEPT = 0; ///
const int _REJECT = -1;///
const int _UFTO = 10; // timeout

/** This class is not part of the server interface.
 * Associate (contain) multiple PV values with(in) a virtual 
 * epics 'gemini record': apply, cad, car, and sir.
 * records presumably processed in well defined order as "links" in a master apply rec.
 * Inherit from an STL container (for the PVs); provide virtual funcs for record processing;
 * and indicate some sense of if when processing is allowed and/or needed (timeout, mark and preset)
 *
 * for apply and cad, allow standard directive inputs:: clear, mark, preset, start, stop
 * ignore preset/start/stop unless record is marked.
 * clear directive only unmarks record, does not clear inputs
 * setting an input field (pva) marks the record.
 * mark directive marks the record and invokes validate func.
 * preset directive invokes validate func. only if rec. is marked
 * start directive invokes validate and genexec funcs, only if rec. marked
 *
 * no need to distinguish between input and output fields (combined i/o for most pvs)
 * or between output field links and flnks, just use deque of 'linked/attached pvs'.

 * don't bother to provide sim. field pvs
 *
 */
class UFGem: public std::map< string, UFPVAttr* >  {
public:
  /// enumerate gemini custom records
  enum RecType { _Gem= 1, _Apply= 2, _CAD= 3, _CAR= 4, _SIR= 5 };
  enum Directive { _Reject= -1, _Accept= 0, _Mark= 0, _Clear= 1, _Preset= 2, _Start= 3, _Stop= 4 };
  enum Status { _Idle= 0, _Pause= 1, _Busy= 2, _Error= 3, _Marked= 10 };

  /// default ctor
  UFGem(const string& recname= "instrum:whatever");
  /// ctor uses default proc. func. for all pvas
  UFGem(const string& name, const deque< UFPVAttr* >& inpva, const deque< UFPVAttr* >& outpva);

  /// dtor
  virtual ~UFGem();

  inline string name() { return _name; }
  inline RecType type() { return _type; }
  inline bool marked() { return _mark; }
  inline int timeout() { return _timeout; }
  inline int setTO(int timeout= _UFTO) { int oldto= _timeout; _timeout = timeout;  return oldto; }

  // pv pointer accessors:
  // various (gemini standard) input values:
  inline UFPVAttr* picid() { string p= _name+".ICID"; if( find(p) != end() ) return (*this)[p]; return 0; } // int 
  inline UFPVAttr* pival() { string p= _name+".IVAL"; if( find(p) != end() ) return (*this)[p]; return 0; } // int/string
  inline UFPVAttr* pinp()  { string p= _name+".INP"; if( find(p) != end() ) return (*this)[p]; return 0; } // int/string
  inline UFPVAttr* pdir()  { string p= _name+".DIR";  if( find(p) != end() ) return (*this)[p]; return 0; } // int
  inline UFPVAttr* pierr() { string p= _name+".IERR"; if( find(p) != end() ) return (*this)[p]; return 0; } // int
  inline UFPVAttr* pimss() { string p= _name+".IMSS"; if( find(p) != end() ) return (*this)[p]; return 0; } // string

  // standard output value defined for all recs:
  inline UFPVAttr* pval() { string p= _name+".VAL"; if( find(p) != end() ) return (*this)[p]; return 0; } // int/string

  // various (gemini standard) output values:
  inline UFPVAttr* pmess() { string p= _name+".MESS"; if( find(p) != end() ) return (*this)[p]; return 0; } // string
  inline UFPVAttr* pocid() { string p= _name+".OCID"; if( find(p) != end() ) return (*this)[p]; return 0; } // int  
  inline UFPVAttr* pclid() { string p= _name+".CLID"; if( find(p) != end() ) return (*this)[p]; return 0; } // int  
  inline UFPVAttr* pmark() { string p= _name+".MARK"; if( find(p) != end() ) return (*this)[p]; return 0; } // int (CAD input)
  inline UFPVAttr* pmarked() { string p= _name+".marked"; if( find(p) != end() ) return (*this)[p]; return 0; } // int (cad output)
  inline UFPVAttr* poerr() { string p= _name+".OERR";  if( find(p) != end() ) return (*this)[p]; return 0; } // int
  //inline UFPVAttr* pmess() { string p= _name+".MESS"; if( find(p) != end() ) return (*this)[p]; return 0; } // string 
  inline UFPVAttr* pomss() { string p= _name+".OMSS"; if( find(p) != end() ) return (*this)[p]; return 0; } // string 
  inline UFPVAttr* pto() { string p= _name+".VALtimeout"; if( find(p) != end() ) return (*this)[p]; return 0; } // int  

  // each directve action is virtual and can/should be overriden by extension classes:
  // provide simple default behaviors...
  virtual void unmark();
  virtual int mark(UFPVAttr* pva= 0);

  inline virtual int status() { if( _timeout > 0 ) return _Busy; if( _mark ) return _Marked; return _Idle; }
  inline virtual void clear() { if( _type != _CAR && _type != _SIR ) unmark(); _timeout= 0; }
  inline virtual int preset() { /* if( !_mark ) return -1; */  return mark(); }
  inline virtual int stop() { clear(); return 0; }
  virtual int start(int timeout= _UFTO);

  virtual int directive(int d);

  // each validation and record general processing action is virtual and can/should be overriden by extension classes:
  // either validate just the specified PV (on mark) or all PVs in rec. (on preset)
  inline virtual int validate(const UFPVAttr* pva= 0) const { return 0; }

  // effectively obviates need for genSub record
  // the processing & 'genSub' record processing entry point:
  virtual int genExec(int timeout= 0);

  // whatever is needed -- for the moment this can create the essential/default set of pvs
  // either internally or from a file...
  inline virtual int setDefaults() { return 0; }

  // static functions to be called from wherever (most likely from PV write virtual) by PCAS runtime)
  // find the rec. this pv attrib. belongs to and mark it for processing 
  // Any PV::write should result in the pv's assoc. dbrec to be marked
  static int mark(UFPVAttr* pva, const deque< UFGem* >& dbrecs); 

  /// find all recs marked it for processing (for use in apply)
  static int marked(deque< UFGem* >& marked, deque< int >& timeouts); 

  /// find all linked recs marked for processing (for use in apply)
  int markedLinks(deque< UFGem* >& marked, deque< int >& timeouts);

  /// process all marked records provided in list, with associated timeouts (for use in apply)
  // if timeout list is empty use any/def. value provided in optional final arg...
  static int execMarked(const deque< UFGem* >& dbrecs, const deque< int >& timeouts, int timeout= 0);

  /// process specifig marked record provided with associated timeouts (for use on SAD/SIR?)
  static int execMarked(UFGem* dbrec, int timeout= 0);

  /// validate all pv's in container rec:
  static int validate(UFGem* dbrec);

  // if this has an associated CAR return its pointer
  inline virtual UFCAR* hasCAR() { return 0; }

  // these are meant to be overriden by any record that has an associated _car (apply, cad, ?)?
  // allow this to reset suggested timeout?
  virtual void setBusy(size_t timeout= _UFTO);
  // proxy is likely to be the cad to whom the car belongs...
  virtual void setBusy(size_t timeout, UFGem* proxy);
  virtual void setIdle();
  virtual string errMsg(int val); 
  virtual void setErr(string& errmsg, int err= 1);

  // set (output val is default pva) pv val of record
  virtual void setPV(string& val, UFPVAttr* pva= 0);
  virtual void setPV(int val, UFPVAttr* pva= 0);
  virtual void setPV(double val, UFPVAttr* pva= 0);

  // input-output helper
  virtual void copyInOut(UFPVAttr* pin);
  static int copyAllInOut(UFGem* rec);

  // use these to (re)set or (re)define/(re)allocate the UFPVAttr of a recond's field/element
  // to the desired data type:
  UFPVAttr* setPVAttrOf(const string& pvname, const string& sval);
  UFPVAttr* setPVAttrOf(const string& pvname, double val);
  UFPVAttr* setPVAttrOf(const string& pvname, int val);
 
  /// attach/assoc. this record to (input) pva, and conversely, pva to this object
  int attachInputPV(UFPVAttr* pva);

  /// attach/assoc. this record to (output) pva, and conversely, pva to this object
  int attachOutputPV(UFPVAttr* pva, bool clear= false); // and optionally clear it

  // conveinence function return outpva ptr
  static UFPVAttr* attachInOutPV(UFGem* rec, UFPVAttr* inpva, const string& prepend= "VAL",
			   const string& append= "", UFPVAttr* outpva= 0);

  /// indicate one or more external (i.e. other records's) pvas as output
  /// of the (named) pva in this record
  //int attachFanOuts(const string& inpvname, const vector< UFPVAttr* >& outpvas );
  int attachFanOut(const string& inpvname, UFPVAttr* outpva);

  /// add rec. to start of _reclinks 
  inline int prependRec( UFGem* dbr ) { _reclinks.push_front(dbr); return (int)_reclinks.size(); }

  /// add rec. to end on _reclinks 
  inline int appendRec( UFGem* dbr ) { _reclinks.push_back(dbr); return (int)_reclinks.size(); }

  inline int attached(deque< UFGem* >*& links) { links = &_reclinks; return (int)links->size(); }

  inline int fanouts(std::multimap< string, UFPVAttr* >*& fanouts) { fanouts = &_pvfanouts; return fanouts->size(); }

  // called each heartbeat, if needed:
  inline virtual int heartpulse() { if( status() == _Busy ) --_timeout; return _timeout; }
  //inline virtual int heartpulse() { return --_timeout; }

  inline static int heartbeat(UFGem* rec) { if( rec ) return rec->heartpulse(); return 0; }

  // heartbeat thread func should scan _timeOutList and _periodicList, and for
  // each rec. in the combined list, invoke heartbeat, which invokes heartpulse...
  static void* heartThread(void* p=0);

  // start the heartbeat thread:
  static pthread_t startHeartThread(const string& instrum);

  // register rec and its pvs (should be performed by each ctor):
  int registerRec(const string& recalias= "");
  inline static int registerRec(UFGem* dbrec, const string& recalias= "")
    { if( dbrec ) return dbrec->registerRec(recalias); return -1; }

  // this allocates and registers all pvs & recs via internal test list
  static int createDB(const string& instrum= "flam", bool pvalloc= true, const string& apply= "");
  // this parse text & fileallocates and registers all pvs & recs
  inline static int createDBfrom(const string& file) { return 0; } // stub

  // is this pva an input of this record?
  //bool hasInput(UFPVAttr* pva);


  int lock(); // lock rec, return pv list size 
  int unlock(); // unlock rec, return pv list size 
  static int lockTO(); // lock rec, return pv list size 
  static int unlockTO(); // unlock rec, return pv list size 
  // set pointer to full rec list if successfull and return size:
  int lockRecList(std::map< string, UFGem* >*& reclist);
  // set pointer to timeout rec list if successfull and return its size
  int lockTOList(std::map< string, UFGem* >*& tolist);
  static int insertTO(UFGem* rec, bool lock= true);
  static int removeTO(UFGem* rec, bool lock= true);

  static void printRecs();
  static void printXML();
  static void UFGem::shutdown();
  static void sigHandler(int signum);

  // public attributes:
  static bool _fullDB, _shutdown, _sim, _paused;
  static bool _xml, _verbose, _vverbose;

  // it is simplest to allow these booleans to be public:
  inline virtual void pause() { _paused = true; }
  inline virtual void resume() { _paused = false; }

  // allow aliases for specific PVs
  // all rec.val == rec (.val PV should have the record name as an alias --
  // foo:rec == foo:rec.val
  std::map< string, UFPVAttr* > _pvalias;
  
  // the full set of records in the database
  static std::map< string, UFGem* > _theRecList;
  // allow record name aliases: foo:carC == foo:carR
  static std::map< string, string > _recAlias;

protected:
  string _name; // record name (prepended to all contained pvs -- recname.pvsuffix)
  RecType _type; // apply, cad, or car
  bool _mark; // if a write occurs on any PV, this should be set true
  int _timeout; // starts should be rejected if this is > 0
  static pthread_mutex_t* _mutex;

  // list of other records linked to this one (processed before or after this?)
  // not to include _car of apply or cad, for master apply rec, this may be nearly congruent with _theRecList
  deque< UFGem* > _reclinks;

  // table of other pvs linked to this records pvs, key == this rec. pvname; val == some other record(s) pv(s):
  // allow genExec() to use value of input pvname to set values of one or more output pvs that can be inputs
  // to one or more other records (causing other records to become marked)
  std::multimap< string, UFPVAttr* > _pvfanouts;
 
  // the full set of input pvas in the database
  std::map< string, UFPVAttr* > _inputs;

  // these 2 lists should be examined every pulse of the heartbeat thread:
  // the set of records with pending timeouts should be mutex protected:
  static std::map< string, UFGem* > _timeOutList;
  static pthread_mutex_t* _TOmutex;

  // the set of records (SIRs) requiring periodic processing:
  // presumably this is readonly after bootup and so needs no mutex...
  static std::map< string, UFGem* > _periodicList;

  // allows heartThread to catch stdlib exceptions...
  static void _heartBeat(const string& instrum);

  // ctor helper:
  void _create(const std::deque< UFPVAttr* >& inputs, const std::deque< UFPVAttr* >& outputs);
};

#endif // __UFGem_h__
