#if !defined(__ufflam2epicsd_cc__)
#define __ufflam2epicsd_cc__ "$Name:  $ $Id: ufflam2epicsd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufflam2epicsd_cc__;
 
#include "UFCAServD.h"
//__UFCAServD_H__(ufflam2epicsd_cc);
 
int main(int argc, char** argv, char** envp) {
  // just call UFFlamisd main:
  return UFCAServD::main(argc, argv, envp);
}
 
#endif // __ufflam2epicsd_cc__
