#if !defined(__UFContinue_CC__)
#define __UFContinue_CC__ "$Name:  $ $Id: UFContinue.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFContinue_CC__;

#include "UFContinue.h"
#include "UFCAR.h"
//#include "UFCCContinue.h"
//#include "UFDCContinue.h"
//#include "UFECContinue.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFContinue::contSys(void* p) {
  // continue really just needs the mech. names
  UFContinue::ThrdArg* arg = (UFContinue::ThrdArg*) p;
  UFContinue::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFContinue::continueing>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFContinue::continueing> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFContinue::continueing> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFContinue::continueing> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFContinue::continueing> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose continue func. 

/// arg: mechName, motparm*
int UFContinue::cont(UFContinue::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to continue whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFContinue::ThrdArg* arg = new UFContinue::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFContinue::contSys, arg);
    return (int) thrid;
  }

  //UFCCContinue::contAll(timeout, rec, car);
  //UFDCContinue::contAll(timeout, rec, car);
  //UFECContinue::conttAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFContinue::_create() {
  string pvname = _name + ".DataLabel";
  // any non null string should indicate continue action
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); 
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:

void UFContinue::setIdle() {
  _paused = false; // make doubly sure sys is not paused!
  if( _timeout > 0 && _verbose )
    clog<<"UFGem::setIdle> "<<_name<<" clearing remaining timeout: "<<_timeout<<endl;
  _timeout = 0;
}

void UFContinue::setBusy(size_t timeout) { 
  if( !_paused )
    clog<<"UFGem::setBusy> note that system is not currently paused, so this just increases/resets the timeout, pending completion or timeout in heartbeat ticks: "<<timeout<<endl;
  _paused = false;
  if( timeout > 0 ) _timeout = timeout;
  //insertTO(this); only CARs in TOlist
}

int UFContinue::start(int timeout) { 
  if( _timeout > 0 ) {
    clog<<"UFGem::start> "<<_name<<", busy? timeout: "<<_timeout<<endl;
    return -1;
  }
  if( !_mark ) {
    clog<<"UFGem::start> "<<_name<<", not marked! timeout: "<<_timeout<<endl;
    return 0;
  }
  if( !_paused ) {
    clog<<"UFGem::start> "<<_name<<", system is not paused?, timeout: "<<_timeout<<endl;
  }

  // Ok to start processing, clear mark and call genExec
  // note these are virtual funcs...
  unmark();
  // marking this should have called validate, no need to do it here:
  /*
  if(timeout > 0) val = validate(); // otherwise assume valid input 
  if( val < 0 ) { // invalid input
    string msg = errMsg(val); setErr(msg, val);
    return val; 
  }
  */
  // calling setBusy before genExec allows all inputs to be copied to their outputs readily
  // before genExec (optionally) clears input(s).
  setBusy(timeout);
  // lock(); int val = genExec(timeout); unlock(); // safer to wrap the genExec in mutex lock?
  int val = genExec(timeout); // don't wrap the genExec in mutex lock?
  if( timeout <= 0 && val >= 0 )
    setIdle();

  return val;
} // start

/// perform continue mechanisms with non-null step cnts or named-position inputs
int UFContinue::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFContinue::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFContinue::genExec> datalabel: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFContinue::Parm* p = new UFContinue::Parm;
  int stat = cont(p, timeout, this, _car);  // perform requested continue

  if( stat < 0 ) { 
    clog<<"UFContinue::genExec> failed to start continue pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFContinue::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFContinue_CC__
