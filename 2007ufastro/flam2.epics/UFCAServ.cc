#if !defined(__UFCASERV_CC__)
#define __UFCASERV_CC__ "$Name:  $ $Id: UFCAServ.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCASERV_CC__;

#include "UFCAServ.h"
#include "UFGem.h"
#include "UFRuntime.h" // system & posix conv. funcs.

// These static members must be re-declared in this file.
bool UFCAServ::_verbose= false;
bool UFCAServ::_vverbose= false;
string UFCAServ::_instrum;

UFCAServ* UFCAServ::_instance= 0;
gddAppFuncTable<UFPV> UFCAServ::_funcTable;
std::map< string, UFPV* > UFCAServ::_createTable;
pthread_mutex_t* UFCAServ::_mutex= 0;

int UFCAServ::applysfrom(const string& file, std::map<string, string>& applylist) {
  string cmd = "cat " + file;
  cmd += " | grep -v '\\#'  | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -i apply | grep -v applyR";
  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::applysfrom> unable to open: "<<file<<endl;
    return 0;
  }

  applylist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     applylist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)applylist.size();
}

int UFCAServ::cadsfrom(const string& file, std::map<string, string>& cadlist) {
  string cmd = "cat " + file;
  cmd += " | grep -v '\\#'  | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -v apply";
  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::cadsfrom> unable to open: "<<file<<endl;
    return 0;
  }

  cadlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     cadlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)cadlist.size();
}

int UFCAServ::carsfrom(const string& file, std::map<string, string>& carlist) {
  string cmd = "cat " + file;
  cmd += " grep -i -v '\\#' | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -v R | grep -v DIR";

  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::carsfrom> unable to open: "<<file<<endl;
    return 0;
  }
  carlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     carlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)carlist.size();
}

int UFCAServ::sadsfrom(const string& file, std::map<string, string>& sadlist) {
  string cmd = "cat " + file;
  cmd += " | grep -i -v '\\#' | cut -d ' ' -f1 | sort -u | grep sad";

  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::sadsfrom> unable to open: "<<file<<endl;
    return 0;
  }
  sadlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     sadlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)sadlist.size();
}

// stubs:
int UFCAServ::createPVsfrom(std::map<string, UFPVAttr*>& pvlist) {
  pvlist.clear();
  return (int)pvlist.size();
}

int UFCAServ::createPVsfrom(const std::map<string, string>& dblist, std::map<string, UFPVAttr*>& pvlist) {
  pvlist.clear();
  return (int)pvlist.size();
}

int UFCAServ::createPVsfrom(const string& file, std::map<string,  UFPVAttr*>& pvlist) {
  pvlist.clear();
  if( file == "" ) {
    return 0;
  }
  clog<<" UFCAServ::createPVsfrom> file: "<<file<<endl;
  /*
  if( file == "" ) {
    string pvname;
    aitString aits = _pvtest[0].getName();
    const char* cs = aits.string();
    if( cs != 0 ) {
      pvname = cs;
      pvlist[pvname] = &_pvtest[0];
      clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(int)pvlist[pvname]<<endl;
    }
    aits = _pvtest[1].getName();
    cs = aits.string();
    if( cs != 0 ) {
      pvname = cs;
      pvlist[pvname] = &_pvtest[1];
      clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(int)pvlist[pvname]<<endl;
    }
    return pvlist.size();
  }
  */
  FILE* fs = ::fopen(file.c_str(), "r");
  if( fs == 0 ) {
    clog<<"createPVfrom> unable to open: "<<file<<endl;
    return -1;
  }

  char pv[BUFSIZ]; int sz = sizeof(pv); memset(pv, 0, sz);
   while( fgets(pv, sz, fs) != 0 ) {
    char *tmp= pv, *pvtmp= pv;
    while( *tmp == ' ' || *tmp == '\t' || *tmp == '\n' || *tmp == '\r' ) ++tmp;
    pvtmp = tmp;
    tmp = &pv[BUFSIZ-1];
    do {
      if( *tmp == ' ' || *tmp == '\t' || *tmp == '\n' || *tmp == '\r' )
	*tmp = '\0';
    } while( --tmp != pvtmp );
    if( strlen(pvtmp) >= 0 ) {
      string pvname = pvtmp;
      //UFPVAttr* pva =  new UFPVAttr(pvtmp, _sdefault);
      UFPVAttr* pva =  new UFPVAttr(pvtmp);
      pvlist[pvname] = pva;
      clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(size_t)pvlist[pvname]<<endl;
    }
    memset(pv, 0, sz);
  }
  return pvlist.size(); 
}

// this is used by the exist and the create virtuals
// although they are deprecated, this is presumably
// of use to pvAttach:
const UFPVAttr* UFCAServ::findPV(const char *pName) {
  const UFPVAttr *pPVAttr= 0;
  string pvname = pName;
  int nr = UFRuntime::rmJunk(pvname);
  if( nr > 0 )
    clog<<"UFCAServ::findPV> removed or replaced "<<nr<<" extraneous characters; pvname: "<<pvname<<endl;
  //if( _verbose )
  //clog<<"UFCAServ::findPV> "<<pvname<<endl;
  std::map< string, UFPVAttr* >::const_iterator pvi = UFPVAttr::_thePVList.find(pvname);
  if( pvi != UFPVAttr::_thePVList.end() ) { // it exists
    //pPVAttr = UFPVAttr::_thePVList[pvname];
    pPVAttr = pvi->second;
    //if( _verbose )
    //clog<<"UFCAServ::findPV> found "<<pvname<<", "<<pPVAttr->name()<<endl;
  }
  //else if( pvname.find(_instrum) != string::npos && _verbose ) {
  //  clog<<"UFCAServ::findPV> "<<pvname<<" not found"<<endl;
  //}
  return pPVAttr;
}

// new virtual (meant to deprecate pvExistTest and createPV),
// but I find them potentially of use to preserve in this
// subclass:
pvAttachReturn UFCAServ::pvAttach(const casCtx& ctx, const char* pPVName) {
  pvExistReturn exist = pvExistTest(ctx, pPVName);
  if( exist.getStatus() == pverDoesNotExistHere )
    return S_casApp_pvNotFound;

  // this should ensure unique one-time ctor invocation of UFPV::UFPV()
  // and also (hopefully) support alias record and field names:
  return createPV(ctx, pPVName);
}

// according to the comment in casdef.h, these two virtuals
// are deprecated, and instead pvAttach should be overriden (need example!):
pvExistReturn UFCAServ::pvExistTest(const casCtx& ctx, const char *pPVName) { // CA Context, pvname
  // If the PV exists, write its name to the canonical PV name object.
  if( pPVName == 0 ) {
    clog<<"UFCAServ::pvExistTest> null PV name!"<<endl;
    return pverDoesNotExistHere;
  }
  //if( _verbose )
  // clog<<"UFCAServ::pvExistTest> of: "<<pPVName<<endl;
  //if( strstr(pPVName, _instrum.c_str()) == 0 ) {
  //    if( _verbose )
  //     clog<<"UFCAServ::pvExistTest> not this instrument? "<<_instrum<<", "<<pPVName<<endl;
  //  return pverDoesNotExistHere; // allow testing of multiple dbs.
  //}
    
  if( _mutex == 0 ) {
     _mutex = new pthread_mutex_t; pthread_mutex_init(_mutex, 0);
  }

  pthread_mutex_lock(_mutex);
  const UFPVAttr *pPVAttr = UFCAServ::findPV(pPVName);
  pthread_mutex_unlock(_mutex);

  if( pPVAttr ) {
    //if( _verbose )
    //  clog<<"UFCAServ::pvExistTest> Implemented by this DB: "<<_instrum<<", "<<pPVName<<endl;
    return pverExistsHere;
  }
  
  if( _verbose )
    clog<<"UFCAServ::pvExistTest> NOT implemented by this DB: "<<_instrum<<", "<<pPVName<<endl;

  return pverDoesNotExistHere;
}

pvCreateReturn UFCAServ::createPV(const casCtx& ctx, const char *pPVName) {
  if( pPVName == 0 ) {
    clog<<"UFCAServ::createPV> null PV name!"<<endl;
    return S_casApp_pvNotFound;
  }

  string pvname= pPVName;
  if( pvname == "" || pvname.find(":") == string::npos ) {
    clog<<"UFCAServ::createPV> null or malformed PV name: "<<pvname<<endl;
    return S_casApp_pvNotFound;
  }
  // If PV doesn't exist, return NULL. Otherwise, return a pointer
  // to a new or existing UFPV object.
  UFPV* pv = createPV(pvname, this);
  if( pv == 0 ) {
    pthread_mutex_unlock(_mutex);
    return S_casApp_pvNotFound;
  }
  return static_cast< casPV& > (*pv);
}

  
UFPV* UFCAServ::createPV(const string& pvname, UFCAServ* cas) {
  // there is only one instance of UFCAServ in a threaded application
  // should all static containers accessed here be protected by mutex?
  if( _mutex == 0 ) {
     _mutex = new pthread_mutex_t; pthread_mutex_init(_mutex, 0);
  }
  pthread_mutex_lock(_mutex);

  const UFPVAttr *pAttr = UFCAServ::findPV(pvname.c_str());
  // If PV doesn't exist, return NULL. Otherwise, return a pointer
  // to a new or existing UFPV object.
  if( !pAttr ) {
    pthread_mutex_unlock(_mutex);
    return 0;
  }

  if( _vverbose )
   clog<<"UFCAServ::createPV> pvname: "<<pvname
       <<", check _createTable for existing UFPV*, table size:"<<_createTable.size()<<endl;
  std::map< string, UFPV* >::const_iterator pvi = _createTable.find(pvname);
  if( pvi != _createTable.end() ) { // pv already created once
    UFPV* pv = pvi->second;
    if( _verbose )
     clog<<"UFCAServ::createPV> pvname: "<<pvname<<" found in _createTable"<<endl;
    pthread_mutex_unlock(_mutex);
    return pv;
  } // all done: pvname == rec.field

  if( _vverbose )
    clog<<"UFCAServ::createPV> pvname: "<<pvname<<" not in _createTable, will create."<<endl;

  // also allow for following: pvname == rec.fieldalias, recalias.field, and recalias.fieldalias
  size_t dotpos = pvname.find(".");
  string rec = pvname.substr(0, dotpos);
  string field = pvname.substr(1+dotpos);
  string recalias= "", fieldalias= "";
  vector< string > allnames;
  allnames.push_back(pvname); // principal name

  UFGem* gemi = pAttr->getDBRecInput();
  if( gemi ) {
    std::map< string, UFPVAttr* >& alias = gemi->_pvalias;
    //std::map< string, UFPVAttr* >::const_iterator pvati = gemi->_pvalias.find(pvname);
    std::map< string, UFPVAttr* >::const_iterator pvati = alias.find(pvname);
    // std::map< string, UFPVAttr* >::const_iterator pvati = find(alias.begin(), alias.end(), pvname);
    if( pvati != gemi->_pvalias.end() ) { // get the field alias
      UFPVAttr* pva = pvati->second;
      string alias = pva->name();
      size_t dotpos = alias.find(".");
      if( dotpos != string::npos )
        fieldalias = alias.substr(1+dotpos);
    }
  }
  UFGem* gemo = pAttr->getDBRecOutput();
  if( gemo ) {
    std::map< string, UFPVAttr* >& alias = gemo->_pvalias;
    //std::map< string, UFPVAttr* >::const_iterator pvato = gemo->_pvalias.find(pvname);
    std::map< string, UFPVAttr* >::const_iterator pvato = alias.find(pvname);
    //std::map< string, UFPVAttr* >::const_iterator pvato = find(alias.begin(), alias.end(), pvname);
    if( pvato != gemo->_pvalias.end() ) { // get the field alias
      UFPVAttr* pva = pvato->second;
      string alias = pva->name();
      size_t dotpos = alias.find(".");
      if( dotpos != string::npos )
        fieldalias = alias.substr(1+dotpos);
    }
  }
  // check pvname == rec.fieldalias
  string pvalias = rec; pvalias += "."; pvalias += fieldalias;
  if( fieldalias != "" ) {
    allnames.push_back(pvalias);
    if( _vverbose )
      clog<<"UFCAServ::createPV> pvalias: "<<pvalias
          <<", check _createTable for existing UFPV, table size:"<<_createTable.size()<<endl;
    pvi = _createTable.find(pvalias);
    if( pvi != _createTable.end() ) { // pv already created once
      UFPV* pv = pvi->second;
      pthread_mutex_unlock(_mutex);
      return pv;
    } // all done: pvname == rec.fieldalias
  }

  // should optionally support rec alias init (as well as field alias)
  std::map< string, string >::const_iterator reci = UFGem::_recAlias.find(rec);
  if( reci != UFGem::_recAlias.end() ) { // record alias exists
    // check recalias.field:
    string recalias = pvalias = reci->second; pvalias += "."; pvalias += field;
    if( recalias != "" ) {
      allnames.push_back(pvalias);
      if( _verbose )
        clog<<"UFCAServ::createPV> pvalias: "<<pvalias
            <<", check _createTable for existing UFPV, table size:"<<_createTable.size()<<endl;
      std::map< string, UFPV* >::const_iterator pvai = _createTable.find(pvalias);
      if( pvai != _createTable.end() ) { // pv already created once
        UFPV* pv = pvi->second;
        pthread_mutex_unlock(_mutex);
        return pv;  // all done: pvname == recalias.field
      }
    }
    // check recalias.fieldalias:
    pvalias = recalias; pvalias += "."; pvalias += fieldalias;
    if( recalias != "" && fieldalias != "" ) {
      allnames.push_back(pvalias);
      if( _verbose )
        clog<<"UFCAServ::createPV> pvalias: "<<pvalias
            <<", check _createTable for existing UFPV, table size:"<<_createTable.size()<<endl;
      std::map< string, UFPV* >::const_iterator pvai = _createTable.find(pvalias);
      //std::map< string, UFPV* >::const_iterator pvai = find(_createTable.begin(), _createTable.end(), pvalias);
      if( pvai != _createTable.end() ) { // pv already created once
        UFPV* pv = pvi->second;
        pthread_mutex_unlock(_mutex);
        return pv;  // all done: pvname == recalias.fieldalias
      }
    }
  }

  // getting this far means that the pv has never been created in any form:
  //UFPV* pv = new (nothrow) UFPV(*this, *pAttr);
  UFPV* pv = new UFPV(*cas, *pAttr);
  if( pv == 0 ) {
    pthread_mutex_unlock(_mutex);
    return pv;
  }  

  if( _verbose )
    clog<<"UFCAServ::createPV> new PV: "<<pAttr->name()<<", aliases: "<<ends;   

  for( size_t i = 0; i < allnames.size(); ++i ) {
    string name = allnames[i];
    _createTable[name] = pv;
    if( _verbose )
      clog<<name<<", "<<ends;   
  }
  if( _verbose )
    clog<<endl;

  pthread_mutex_unlock(_mutex);
  return pv;
} // static createPV


// Constructor for UFCAServ. After passing arguments to caServer 
// constructor all of the read functions are installed in the function
// table.
//UFCAServ::UFCAServ(unsigned pvCountEstimate) : caServer(pvCountEstimate) {
UFCAServ::UFCAServ(const char* name) : caServer() {
  _funcTable.installReadFunc("status", &UFPV::readStatus);
  _funcTable.installReadFunc("severity", &UFPV::readSeverity);
  _funcTable.installReadFunc("precision", &UFPV::readPrecision);
  _funcTable.installReadFunc("alarmHigh", &UFPV::readHighAlarm);
  _funcTable.installReadFunc("alarmHighWarning", &UFPV::readHighWarn);
  _funcTable.installReadFunc("alarmLowWarning", &UFPV::readLowWarn);
  _funcTable.installReadFunc("alarmLow", &UFPV::readLowAlarm);
  _funcTable.installReadFunc("value", &UFPV::readVal);
  _funcTable.installReadFunc("graphicHigh", &UFPV::readHighOperation);
  _funcTable.installReadFunc("graphicLow", &UFPV::readLowOperation);
  _funcTable.installReadFunc("controlHigh", &UFPV::readHighCtrl);
  _funcTable.installReadFunc("controlLow", &UFPV::readLowCtrl);
  _funcTable.installReadFunc("units", &UFPV::readUnits);

  //_instrum = "";
  if( name )
    _instrum = name;
}

int UFCAServ::main(int argc, char** argv, char** env) {
  // Create server object.
  // any chance the ::main has set the instrument name?
  bool dblist = false;
  string file;
  char* name= 0;
  if( UFCAServ::_instrum == "" ) // it has not, set it to default:
    name = "flam"; // provide this to the ctor...
  else
    name = (char*) UFCAServ::_instrum.c_str();

  // if i choose to enhance the server object by having it inherit
  // UFDaemon, then this clumsy command line logic will be replaced...
  if( argc > 1 ) 
    if( argv[1][0] != '-' )  // assume first option instrument name, and possibly db file name too
      name = argv[1];
  if( argc > 2 ) 
    if( argv[2][0] != '-' )  // assume 2nd option is instrument name, etc.
      name = argv[2];

  struct stat s;
  if( stat(name, &s) == 0 ) // assume instrument name is also db file name:
    file = name;

  char* dot = index(name, '.');
  if( dot != 0 ) strncpy(name, name, (dot-name)); // strip dot from file name for instrum. name

  UFPVAttr::_cas = UFCAServ::_instance = new UFCAServ(name);
  UFCAServ cas = *UFCAServ::_instance;
  //cas.setDebugLevel(0u); //cas.setDebugLevel(1u); //cas.setDebugLevel(5u);

  if( argc > 1 ) { 
    if( argv[1][0] == '-' && argv[1][1] == 'l' ) 
      dblist = true;
    if( argv[1][0] == '-' && argv[1][1] == 'v' ) {
      UFCAServ::_verbose = UFPV::_verbose = UFGem::_verbose = true;
      if( argv[1][0] == '-' && argv[1][1] == 'v' && argv[1][2] == 'v' ) { // -vv
        UFPV::_vverbose = UFGem::_vverbose = true; //cas.setDebugLevel(0u);
      }
    }
  }

  if( argc > 2 ) { 
    if( argv[2][0] == '-' && argv[2][1] == 'l' ) 
      dblist = true;
    if( argv[2][0] == '-' && argv[2][1] == 'v' ) {
      UFCAServ::_verbose = UFPV::_verbose = UFGem::_verbose = true;
      if( argv[2][0] == '-' && argv[2][1] == 'v' && argv[2][2] == 'v' ) { // -vv
        UFGem::_vverbose = true; //cas.setDebugLevel(0u);
      }
    }
  }

  int nrec = 0;
  if( !file.empty() ) {
    //UFCAServ::createPVsfrom(file, UFPVAttr::_thePVList);
    nrec = UFGem::createDBfrom(file);
  }
  else { // internal test DB -- each ctor registers rec & pvs
    nrec = UFGem::createDB(UFCAServ::_instrum, !dblist);
  }

  if( dblist || nrec == 0 ) // just list the db rec & optionally fields (-v or -vv) and exit
    return 0;

  // start sighandler thread:
  UFPosixRuntime::sigWaitThread(UFGem::sigHandler);

  // start heart thread:
  UFGem::startHeartThread(UFCAServ::_instrum);

  // forever until shutdown:
  double delay = 1000;
  while( ! UFGem::_shutdown ) // fileDescriptorManager is a predeclared object found in fdMgr.h
    fileDescriptorManager.process(delay);

  UFGem::shutdown();
  return 0;
}

#endif // __UFCASERV_CC__
