#if !defined(__UFECAbort_h__)
#define __UFECAbort_h__ "$Name:  $ $Id: UFECAbort.h,v 0.3 2003/11/13 21:11:41 hon beta $"
#define __UFECAbort_H__(arg) const char arg##ECAbort_h__rcsId[] = __UFECAbort_h__;

#include "UFCAD.h"
#include "UFECSetup.h"

class UFECAbort : public UFCAD {
public:
  inline UFECAbort(const string& instrum= "instrum") : UFCAD(instrum+":ec:abort") { _create(); }
  inline virtual ~UFECAbort() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* abortion(void* p);
  static int abort(UFECSetup::EnvParm* parms, int timeout,UFGem* rec= 0, UFCAR* car= 0);
  // available to IS
  inline static int abortAll(int timeout, UFGem* rec= 0, UFCAR* car= 0) { return 0; }

protected:
  // ctor helper
  void _create();
};

#endif // __UFECAbort_h__
