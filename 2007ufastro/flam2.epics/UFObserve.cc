#if !defined(__UFOBSERVE_CC__)
#define __UFOBSERVE_CC__ "$Name:  $ $Id: UFObserve.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFOBSERVE_CC__;

#include "UFObserve.h"
#include "UFObsSetup.h"
#include "UFInstrumSetup.h"
#include "UFDCMCECmd.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

string UFObserve::_BiasMode;

// ctors:

// static class funcs:
void* UFObserve::observation(void* p) {
  // observe really just needs timeout and cars?
  UFObserve::OThrdArg* arg = (UFObserve::OThrdArg*) p;
  UFObserve::Parm* parms = arg->_parms;
  int timeout = 10;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  clog<<"UFObserve::observation>... "<<endl;

  // simple simulation...
  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if(c != 0 && c != car ) c->setIdle();
    }
    // free arg
    delete arg;
    // exit daemon thread
    return p;
  }

  // send observation acq start directive to detector agent
  // could also submit obsconf to edtd here (before mce acq), but
  // for now follow the trecs precedent and assume the detector agent will
  // notify/provide the edtd with the obsconf...
  if( parms ) {
    timeout = parms->_timeout;
    clog<<"UFObserve::observing> obs timeout: "<<timeout<<endl;
    UFDCMCECmd::submitAcq("Start",  parms->_datalabel, parms->_index, timeout, rec, car);
  }
  else {
    clog<<"UFObserve::observing> no obs parms?"<<endl;
  }
  // free arg
  delete arg;

  // exit daemon thread
  return p;
}

/// general purpose observe func. 

/// arg: 
int UFObserve::observe(UFObserve::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // reuse currently set bias or reset based on IS input/override:
  if( UFObsSetup::_BiasMode.find("null") != string::npos ||
      UFObsSetup::_BiasMode.find("Null") != string::npos ||
      UFObsSetup::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = UFObsSetup::_BiasMode;
  }
  else if( UFInstrumSetup::_BiasMode.find("null") != string::npos ||
	   UFInstrumSetup::_BiasMode.find("Null") != string::npos ||
	   UFInstrumSetup::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = UFInstrumSetup::_BiasMode;
  }
  else if( UFObserve::_BiasMode.find("null") != string::npos ||
	   UFObserve::_BiasMode.find("Null") != string::npos ||
	   UFObserve::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = "imaging"; // default if never set
  }

  // clear all others:
  UFObsSetup::_BiasMode = UFInstrumSetup::_BiasMode = "null";
  // create and run pthread to observe then either reset car to idle or error
  // thread should free the tParm* allocations...
  //
  UFObserve::OThrdArg* arg = new UFObserve::OThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFObserve::observation, arg);

  UFObsSetup::_BiasMode = UFInstrumSetup::_BiasMode = "null";
  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFObserve::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".DataLabel"; // from DHS/OCS
  // any non null value should indicate observe action 
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Test"); 
  attachInOutPV(this, pva);

  pvname = _name + ".DataMode"; // save or discard
  // any non null value should indicate observe action 
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Save Or Discard"); 
  attachInOutPV(this, pva);

  pvname = _name + ".WCSCRPX1"; // FITS keyword option in header
  // any non null value should indicate observe action 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1023); 
  attachInOutPV(this, pva);

  pvname = _name + ".WCSCRPX2"; // FITS keyword option in header
  // any non null value should indicate observe action 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1023); 
  attachInOutPV(this, pva);

  pvname = _name + ".UserInfo"; // FITS keyword option in header
  // any non null value should indicate observe action 
  pva = (*this)[pvname] = new UFPVAttr(pvname, "RichardElston"); 
  attachInOutPV(this, pva);

  pvname = _name + ".PixLUTFile"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "UFFlamingos.lutBE");
  attachInOutPV(this, pva);

  pvname = _name + ".DitherIndex"; // <0 indicate automatic incrementing?
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1); //new UFPVAttr(pvname, (int)INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".FITSFileName"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Flamingos2.fits");
  attachInOutPV(this, pva);

  pvname = _name + ".PNG-Or-JPEGFile"; // for local/nfs pngs file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Flamingos2.png");
  attachInOutPV(this, pva);

  pvname = _name + ".DataDir"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "/data/flam/");
  attachInOutPV(this, pva);

  pvname = _name + ".NFSHost"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "localhost");
  attachInOutPV(this, pva);

  pvname = _name + ".DS9Display";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); //new UFPVAttr(pvname, (int)INT_MIN);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform observation
int UFObserve::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFObserve::genExec> no pvs?"<<endl;
    return (int) pvcnt;
  }
  string input, pvname;

  string datalabel;
  pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    datalabel = UFDCMCECmd::_dhslabel = input;
    clog<<"UFObserve::genExec> DataLabel: "<<input<<endl;
    pva->clearVal(); // clear
  }

  string datamode;
  pvname = _name + ".DataMode"; // < Discard or Keep: All | DHS | None >
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    datamode = UFDCMCECmd::_datamode = input;
    clog<<"UFObserve::genExec> DataMode: "<<input<<endl;
  }

  string usrinfo;
  pvname = _name + ".UserInfo";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    usrinfo = UFDCMCECmd::_usrinfo = UFDCMCECmd::_comment = input;
    clog<<"UFObserve::genExec> UserInfo: "<<input<<endl;
  }

  int index= 1; 
  pvname = _name + ".DitherIndex";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    index = UFDCMCECmd::_index = (int)pva->getNumeric(); // integer input
    clog<<"UFObserve::genExec> DitherIndex: "<<index<<endl;
  }

  string datadir= "/data/flam2/"; // input is anything non-null
  pvname = _name + ".DataDir"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    datadir = UFDCMCECmd::_datadir = input;
    clog<<"UFObsSetup::genExec> data directory: "<<datadir<<endl;
    //pva->clearVal(); // clear
  }

  string nfshost= "localhost"; // input is anything non-null
  pvname = _name + ".NFSHost"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    nfshost = UFDCMCECmd::_nfshost = input;
    clog<<"UFObsSetup::genExec> localhost or nfs mount host: "<<nfshost<<endl;
    //pva->clearVal(); // clear
  }

  int ds9 = 0; //UFDCMCECmd::_ds9 = 0;
  pvname = _name + ".DS9Display";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    UFDCMCECmd::_ds9 = ds9 = (int) pva->getNumeric(); // input is anything non-null
    clog<<"UFObsSetup::genExec> DS9 frame(s) Display, if any: "<<ds9<<endl;
    //pva->clearVal(); // clear?
  }
  string fitsfile= "Flamingos2.fits"; // input is anything non-null
  pvname = _name + ".FITSFileName"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    fitsfile = UFDCMCECmd::_fitsfile = input;
    clog<<"UFObsSetup::genExec> local fits file: "<<fitsfile<<endl;
    //pva->clearVal(); // clear
  }

  string lutfile= "UFFlamingos.lutBE"; // input is anything non-null
  pvname = _name + ".PixLUTFile"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    lutfile = UFDCMCECmd::_lutfile = input;
    clog<<"UFObsSetup::genExec> Pixel LUT (sort) file: "<<lutfile<<endl;
    //pva->clearVal(); // clear
  }

  string pngjpegfile= "UFFlamingos.lutBE"; // input is anything non-null
  pvname = _name + ".PNG-Or-JPEGFile"; // for local/nfs pngs file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    pngjpegfile = UFDCMCECmd::_pngjpegfile = input;
    clog<<"UFObsSetup::genExec> local fits file: "<<pngjpegfile<<endl;
    //pva->clearVal(); // clear
  }

  int wcpx1= 1023;
  pvname = _name + ".WCSCRPX1";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    wcpx1 = UFDCMCECmd::_wcpix1 = (int)pva->getNumeric(); // integer input
    clog<<"UFObserve::genExec> WCSCRPX1: "<<wcpx1<<endl;
  }

  int wcpx2= 1023; 
  pvname = _name + ".WCSCRPX2";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    wcpx2 = UFDCMCECmd::_wcpix2 = (int)pva->getNumeric(); // integer input
    clog<<"UFObserve::genExec> WCSCRPX2: "<<wcpx2<<endl;
  }

  int to = 10;
  pvname = _name + ".timeout";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    to = (int)pva->getNumeric(); // integer input
    if( to < 0 )
      to = timeout;
    else
      timeout = to;
    clog<<"UFObserve::genExec> timeout: "<<to<<endl;
  }

  UFObserve::Parm* p = new UFObserve::Parm(usrinfo, datalabel, datamode, to, index, wcpx1, wcpx2);
  int stat = observe(p, timeout, this, _car);  // perform requested observe
  if( stat < 0 ) { 
    clog<<"UFObserve::genExec> failed to start observe pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFObserve::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFOBSERVE_CC__
