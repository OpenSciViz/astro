#if !defined(__UFCAR_h__)
#define __UFCAR_h__ "$Name:  $ $Id: UFCAR.h,v 0.13 2005/10/20 19:26:27 hon Exp $"
#define __UFCAR_H__(arg) const char arg##CAR_h__rcsId[] = __UFCAR_h__;

#include "UFGem.h"
const int _UFCARTO = 10;

class UFCAR : public UFGem {
public:
  inline UFCAR(const string& recname, UFGem* proxy= 0) : UFGem(recname), _status(0) { _create(proxy); }
  inline virtual ~UFCAR() {}

  // always marked:
  inline virtual void unmark() {}
  inline virtual int status() { return _status; }

  // these are meant to be overriden by any record that has an associated _car (apply, cad, ?)?
  // allow this to reset suggested timeout?
  virtual int start(int timeout= 10);

  // proxy is likely to be the cad to whom the car belongs...
  virtual int genExec(int timeout= _UFCARTO);
  virtual void setBusy(size_t timeout= _UFCARTO, UFGem* proxy= 0);
  virtual void pause();
  virtual void setIdle();
  virtual void setErr(string& errmsg, int err= 1);
  virtual string errMsg(int val);
  virtual void clear() { setIdle(); }

  int setTO(int timeout=10);
  void setIVAL(int val);

  static int _ErrorCnt;

protected:
  int _status;
  UFGem* _proxy; // the dbrec (apply or cad) that caused this car to go busy...
  // ctor helper
  void _create(UFGem* proxy= 0);
};

#endif // __UFCAR_h__
