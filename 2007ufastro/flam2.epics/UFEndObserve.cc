#if !defined(__UFEndObserve_CC__)
#define __UFEndObserve_CC__ "$Name:  $ $Id: UFEndObserve.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFEndObserve_CC__;

#include "UFEndObserve.h"
#include "UFCAR.h"
//#include "UFCCEndObserve.h"
//#include "UFDCEndObserve.h"
//#include "UFECEndObserve.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFEndObserve::endAll(void* p) {
  // endobs really just needs the mech. names
  UFEndObserve::ThrdArg* arg = (UFEndObserve::ThrdArg*) p;
  UFEndObserve::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFEndObserve::endAll>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFEndObserve::endAll> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFEndObserve::endAll> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFEndObserve::endAll> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFEndObserve::endAll> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose endobs func. 

/// arg: mechName, motparm*
int UFEndObserve::end(UFEndObserve::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to endobs whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFEndObserve::ThrdArg* arg = new UFEndObserve::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFEndObserve::endAll, arg);
    return (int) thrid;
  }

  //UFCCEndObserve::endAll(timeout, rec, car);
  //UFDCEndObserve::endAll(timeout, rec, car);
  //UFECEndObserve::endAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFEndObserve::_create() {
  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate endobs action 
  attachInOutPV(this, pva); 

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform endobs mechanisms with non-null step cnts or named-position inputs
int UFEndObserve::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFEndObserve::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFEndObserve::genExec> dhslabel: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFEndObserve::Parm* p = new UFEndObserve::Parm;
  int stat = end(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFEndObserve::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFEndObserve::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFEndObserve_CC__
