#if !defined(__UFCCDATUM_CC__)
#define __UFCCDATUM_CC__ "$Name:  $ $Id: UFCCDatum.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCDATUM_CC__;

#include "UFCCDatum.h"
#include "UFCAServ.h"
#include "UFCCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// from portescap motor indexor logic:
#include "ufflam2mech.h"
#include "ufF2mechMotion.h" // provide  F2 compound motion logic

// ctors:

// static class funcs:
// deprecated compound home/datum
int UFCCDatum::_homestephome(const string& mname, const UFCCSetup::MotParm& mp, deque<string>& cmds) {
  // if backlash input is non-zero assume backlash datum sequence
  int blash = mp._blash;
  if( blash == INT_MIN ) {
    clog<<"UFCCSetup::_homestephome> backlash supplied is not allowed: "<<blash<<", just perform raw home..."<<endl;
    blash = 0;
  }
  //UFCCSetup::MechPos* mech = UFCCSetup::_mechPos[mp._indexor];
  //if( _verbose )
    clog<<"UFCCDatum::_homestephome> backlash: "<<blash<<endl;
  // use mp._steps in N cmd to position indexor at intermediate 'backlash' position
  // then step remaining delta to desireed named position:
  string cmd = "Raw "; cmd += " !!Err. step indexor: "; cmd += mp._indexor ; cmd += " to "; cmd += blash;
  string cmdpar;
  if( blash != 0 ) {
    strstream sB;
    sB << mp._indexor;
    if( blash > 0 )
      sB << " +" << abs(blash) << ends;
    else 
      sB << " -" << abs(blash) << ends;

    cmdpar = sB.str(); delete sB.str();
    cmds.push_back(cmd); cmds.push_back(cmdpar);
  }

  strstream sF;
  int direction = mp._direction;
  if( direction != 0 )
    direction = 0; // final home in opposite direction
  else
    direction = 1;

  sF<<mp._indexor<<" F "<<mp._vel<<" "<<direction<<ends;

  cmdpar = sF.str(); delete sF.str();
  cmds.push_back(cmd); cmds.push_back(cmdpar);
  
  return (int)cmds.size();
}

// new F2 compound home/datum
int UFCCDatum::_homeF2(const string& mname, const UFCCSetup::MotParm& mp, deque<string>& cmds) {
  // if backlash input is non-zero assume backlash datum sequence
  int blash = mp._blash;
  if( blash == INT_MIN ) {
    clog<<"UFCCDatum::_homeF2> backlash supplied is not allowed: "<<blash<<", set to 0..."<<endl;
    blash = 0;
  }
  // assuming some sort of compund datum is required/requested, but no more than 4?
  // preallocate the 4 text buffers and invoke crag's func, which will return number of string
  // cmds actually set...
  double current = UFCCSetup::fetchCurrentSteps(mname);
  double desired = 0.0; // home!
  if( fabs(desired - current) <= 0.01 )
    clog<<"UFCCDatum::_homeF2> desired: "<<desired<<", current: "<<current<<" position steps are identical?"<<endl;

  /* old 'raw cmd' syntax  
  char c0[BUFSIZ], c1[BUFSIZ], c2[BUFSIZ], c3[BUFSIZ];
  int nc = ::ufF2mechMotion4((char*)mname.c_str(), current, desired, blash, mp._vel, mp._slow, c0, c1, c2, c0);
  string raw = "Raw!!Err.StepOrHome";
  if( nc > 0 ) { cmds.push_back(raw); cmds.push_back(c0); }
  if( nc > 1 ) { cmds.push_back(raw); cmds.push_back(c1); }
  if( nc > 2 ) { cmds.push_back(raw); cmds.push_back(c2); }
  if( nc > 3 ) { cmds.push_back(raw); cmds.push_back(c3); }
  */

  /* new 'trusted cmd seq.' syntax */
  char cmdname[BUFSIZ], cmdimpl[BUFSIZ];
  int nc = ::ufF2mechMotionP((char*)mname.c_str(), current, desired, blash, mp._vel, mp._slow, (char*)cmdimpl);
  ::cmdTransaction((char*)cmdimpl, (char*)cmdname);
  cmds.push_back(cmdname); cmds.push_back(cmdimpl);

  return nc;
}

void* UFCCDatum::gohome(void* p) {
  UFFLAM2Mech* f2mech = ::getTheUFFLAM2Mech();
  string focusindexor = f2mech->indexor[UFF2FocusIdx];
  string windowindexor = f2mech->indexor[UFF2WindowIdx];
  // home specified indexor in 0 or 1 direction, at specified velocity
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:datumC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  int mcnt = 0;
  if( mparms ) mcnt = mparms->size();
  //if( _verbose )
  clog<<"UFCCDatum::gohome> car: "<<carname<<", mparms cnt: "<<mcnt<<endl;

  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  bool validsoc = connected(UFCCSetup::_motoragntsoc);
  if( !_sim && !validsoc ) {
    clog<<"UFCCDatum::gohome> not connected to motor indexor agent, need to (re)init..."<<endl;
    // and free the motparms
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      UFCCSetup::MotParm* mp = mit->second;
      delete mp;
    }
    // free arg
    delete arg;
    return p;
  }

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  // req. bundle is pairs of strings:
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    string mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; // _posit should be "datum" default posname
    if( mp == 0 ) {
      clog<<"UFCCDatum::gohome> "<<mname<<" has no parameter values!"<<endl;
      continue;
    }
    //if( _verbose )
      clog<<"UFCCDatum::gohome> "<<mname<<" indexor: "<<mp->_indexor<<", vel: "<<mp->_vel<<endl;

    cmdvec.clear();
    // datum indexor 'I', and indicate suggested error message,
    // followed by full syntax command and parameter:
    // if agent should set car on completion or error, indicate car channel (pvname):
    string cmd = mp->_indexor; cmd += "CAR";
    string cmdpar = carname;
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    if( _verbose )
      clog<<"UFCCDatum::gohome> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "SAD";
    cmdpar = UFCCSetup::sadChan(mname); cmdpar += ".INP";
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
      clog<<"UFCCDatum::gohome> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    // atomic datum?
    if( mp->_blash == 0 || mp->_blash == INT_MIN ) {
      clog<<"UFCCDatum::gohome> no blash supplied, performing atomic datum..."<<endl;
      cmd = "Raw "; cmd += mp->_indexor; cmd += " F !! Error Datuming "; cmd += mname;
      strstream s;
      if( mp->_direction <= 0 && mp->_vel > 0 ) { // go in negative direction?
        s<<mp->_indexor<<" F "<<mp->_vel<<" 0"<<ends;
      }
      else if( mp->_vel < 0 ) { // also negative direction?
        s<<mp->_indexor<<" F "<<mp->_vel<<" 0"<<ends;
      }
      else { // positive direction?
        s<<mp->_indexor<<" F "<<mp->_vel<<" 1"<<ends;
      }
      cmdpar = s.str(); delete s.str();
    }
    // if backlash input is non-zero supplement with backlash sequence
    else { // use 'trusted' F2 compound motion function
      clog<<"UFCCDatum::gohome> blash supplied, performing trusted F2 compound datum..."<<endl;
      //_homestephome(mname,*mp, cmdvec); deprecated for F2
      _homeF2(mname, *mp, cmdvec); // new compound home for F2 written by craig warner
    }

    // send the bundle of cmds for this indexor to the motor agent
    if( !_sim && validsoc ) {
      UFStrings req(name, cmdvec);
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCDatum::gohome> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      if( _verbose )
        clog<<"UFCCDatum::gohome> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
      //int nr = UFCCSetup::_motoragntsoc->recv(req);
    }
    else if( !validsoc ) {
      clog<<"UFCCDatum::gohome> unable to transact request with portescap motor agent due to invalid connection. need to (re) init..."<<endl;
      err = 1; errmsg = "Error Datuming";
    }
  } // end for loop over indexor list

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }
  // free arg
  delete arg;
  
  if( _sim || err != 0 ) {
    clog<<"UFCCDatum::gohome> error or sim..."<<endl;
    // set car idle after 1/2 sec:
    if( _sim ) UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && err == 0 ) 
	c->setIdle();
      else if( c != 0 && c != car && err != 0 ) {
        c->setErr(errmsg, err);
      }
    }
  }

  // exit daemon thread
  return p;
} // gohome

// origen specified mechs
void* UFCCDatum::originate(void* p) {
  UFFLAM2Mech* f2mech = ::getTheUFFLAM2Mech();
  string focusindexor = f2mech->indexor[UFF2FocusIdx];
  string windowindexor = f2mech->indexor[UFF2WindowIdx];
  // home specified indexor in 0 or 1 direction, at specified velocity
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string option = arg->_option;
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:datumC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  int mcnt = 0;
  if( mparms ) mcnt = mparms->size();
  //if( _verbose )
  clog<<"UFCCDatum::orignate> car: "<<carname<<", mparms cnt: "<<mcnt<<endl;

  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  int err= 0;
  string errmsg;
  bool validsoc = connected(UFCCSetup::_motoragntsoc);
  if( !_sim && !validsoc ) {
    clog<<"UFCCDatum::originate> not connected to motor indexor agent, need to (re)init..."<<endl;
    err = 1; errmsg = "ErrOrigin";
  }
  else {
    UFCCSetup::cmdOrigin(option, mparms, rec, car);
  }

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }
  // free arg
  delete arg;
  
  if( _sim || err != 0 ) {
    clog<<"UFCCDatum::gohome> error or sim..."<<endl;
    // set car idle after 1/2 sec:
    if( _sim ) UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && err == 0 ) 
	c->setIdle();
      else if( c != 0 && c != car && err != 0 ) {
        c->setErr(errmsg, err);
      }
    }
  }

  // exit daemon thread
  return p;
} // originate

/// general purpose home func.
/// arg: mechName, motparm*
int UFCCDatum::home(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec != 0 ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && c != 0 && car != c ) car->setBusy(timeout);
  }

  clog << "UFCCDatum::home> mparms size: "<< mparms->size() << endl;

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCDatum::gohome, arg);

  return (int) thrid;
} // home


/// special purpose origin func.
int UFCCDatum::origin(const string& option, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec != 0 ) {
    UFCAR* c = rec->hasCAR();
    //if( c != 0 )
    //clog<<"UFCCDatum::homeAll> rec-car-name: "<<c->name()<<endl;
    if( car != c && car != 0 ) {
      car->setBusy(timeout);
      //clog<<"UFCCDatum::homeAll> supplemtary carname: "<<car->name()<<endl;
    }
  }
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  //std::map< string, UFPVAttr*>::const_iterator it;
  string mname, indexor;
  deque< string >* mechNames = UFCCSetup::getMechNames();

  // origin all?
  if( option.find("all") != string::npos || option.find("All") != string::npos || option.find("ALL") != string::npos ) {
    for( size_t i= 0; i < mechNames->size(); ++i ) {
      mname= (*mechNames)[i]; indexor= UFCCSetup::_indexorNames[mname];
      UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
      (*mechs)[mname] = mp; // use default motion "datum"
    }
  }
  else { // just the individual mech./indexor specified:
    for( size_t i= 0; i < mechNames->size(); ++i ) {
      mname= (*mechNames)[i]; indexor= UFCCSetup::_indexorNames[mname];
      if( option.find(mname) != string::npos || option == indexor ) {
        UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
        (*mechs)[mname] = mp; // use default motion "datum"
	break;
      }
    }
  }

  if( mechs->size() <= 0 )
    return -1;

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mechs, rec, car, option);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCDatum::originate, arg);

  return (int) thrid;
} // origin

// non static, non virtual funcs:
// protected ctor helper:
void UFCCDatum::_create() {
  /// pvs should look something like:
  /// flam:cc:datum.mech1Vel == home velocity
  /// flam:cc:datum.mech1Direction == home direction
  /// ... thru mechNvel
  int mcnt = UFCCSetup::setDefaults(_name); 
  deque< string >* mnames = UFCCSetup::getMechNames();
  UFPVAttr* pva= 0;

  // Origin specified mechanism by name or indexor, or all:
  string pvpar, pvname = _name + ".Origin";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva);

  // Datum all mechanisms using default motion parameters:
  pvname = _name + "."; pvname += "All";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva);

  for( int i = 0; i < mcnt; ++i ) {
    string mechname = (*mnames)[i];
    pvname = _name + "."; pvname += mechname;
    pvpar = pvname;
    // no need for barcode datum fields? 
    //if( pvname.find("BarC") != string::npos )
    //  continue;
    pva = (*this)[pvpar] = new UFPVAttr(pvpar); // mechanism name
    attachInOutPV(this, pva);

    pvname = _name + "."; pvname += (*mnames)[i];
    pvpar = pvname + "Vel";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar); // home vel in - direction?
    attachInOutPV(this, pva);
    if( pvname.find("ocus") != string::npos ||pvname.find("indow") != string::npos ) {
      // alias LimitSeekVel
      pvpar = pvname + "LimitSeekVel";
      _pvalias[pvpar] = pva;
    }

    pvpar = pvname + "InitVel";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN); // reset initial velocity
    attachInOutPV(this, pva);

    pvpar = pvname + "Slow";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN); // compound home 'slow' slew velocity
    attachInOutPV(this, pva);
    /*
    pvpar = pvname + "InitSlow";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN); // compound home 'slow' initial velocity
    attachInOutPV(this, pva);
    */
    pvpar = pvname + "Direction"; // - (<= 0) or + (> 0) ?
    pva = (*this)[pvpar] = new UFPVAttr(pvpar);
    attachInOutPV(this, pva);

    pvpar = pvname + "Backlash"; // +- step cnt
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "Acc";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "Dec";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "RunCur";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "HoldCur";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);
  }

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
} // _create

int UFCCDatum::homeListAlloc(std::map< string, UFCCSetup::MotParm* >& mechs, bool all) {
  mechs.clear();
  int blash= 0, vel= 0, slow= 25, ivel= 25, /* islow= 25,*/ dir= 1, acc= 100, dec= 100, runc= 25, holdc= 0;
  string pvname, mechName, indexor;
  UFPVAttr* pva= 0;
  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    blash = vel = dir = 0;
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    pvname = name() + "." + mechName;
    pva = (*this)[pvname]; // only interested in mechName, velocity, and direction values (for now)
    if( pva == 0 )
      continue;

    int input = (int)pva->getNumeric();

    if( !all && pva->isClear() ) {
      clog<<"UFCCDatum::genExec> is clear "<<pvname<<", input: "<<input<< "; all: " << all <<endl;
      continue;
    }
    else if( indexor == "H" ) {
      clog<<"UFCCDatum::genExec> ignoring indexor: "<<indexor<<endl;
      continue;
    }

    string blsh = pvname + "Backlash";
    UFPVAttr* bpva = (*this)[blsh]; // only interested in mechName, velocity, and direction values (for now)
    if( bpva != 0 ) {
      if( bpva->isSet() )
        blash = abs((int)bpva->getNumeric());
    }

    string dname = pvname + "Direction";
    UFPVAttr* dpva = (*this)[dname]; // only interested in mechName, velocity, and direction values (for now)
    if( dpva != 0 ) {
      if( dpva->isSet() ) {
        dir = (int)dpva->getNumeric();
        if( dir <= 0 )
          dir = 0;
        else
          dir = 1;
      }
    }
    string vname = pvname + "Vel";
    UFPVAttr* vpva = (*this)[vname]; // only interested in mechName, velocity, and direction values (for now)
    if( vpva != 0 )
      if( vpva->isSet() )
        vel = (int) vpva->getNumeric();

    string ivname = pvname + "InitVel";
    UFPVAttr* ivpva = (*this)[ivname]; // only interested in mechName, velocity, and direction values (for now)
    if( ivpva != 0 )
      if( ivpva->isSet() )
        ivel = (int) vpva->getNumeric();

    string sname = pvname + "Slow";
    UFPVAttr* spva = (*this)[sname]; // only interested in mechName, velocity, and direction values (for now)
    if( spva != 0 )
      if( spva->isSet() )
        slow = (int) spva->getNumeric();
    /*
    string isname = pvname + "InitSlow";
    UFPVAttr* ispva = (*this)[isname]; // only interested in mechName, velocity, and direction values (for now)
    if( ispva != 0 )
      if( ispva->isSet() )
        islow = (int) ispva->getNumeric();
    */
    string acname = pvname + "Acc";
    UFPVAttr* acpva = (*this)[acname]; // only interested in mechName, velocity, and direction values (for now)
    if( acpva != 0 )
      if( acpva->isSet() )
        acc = (int) acpva->getNumeric();

    string dcname = pvname + "Dec";
    UFPVAttr* dcpva = (*this)[dcname]; // only interested in mechName, velocity, and direction values (for now)
    if( dcpva != 0 )
      if( dcpva->isSet() )
        dec = (int) dcpva->getNumeric();

    string rname = pvname + "RunCur";
    UFPVAttr* rpva = (*this)[rname]; // only interested in mechName, velocity, and direction values (for now)
    if( rpva != 0 )
      if( rpva->isSet() )
        runc = (int) rpva->getNumeric();

    string hname = pvname + "HoldCur";
    UFPVAttr* hpva = (*this)[hname]; // only interested in mechName, velocity, and direction values (for now)
    if( hpva != 0 )
      if( hpva->isSet() )
        holdc = (int) hpva->getNumeric();

    if( vel != 0 && vel != INT_MIN ) {
      bool firmware= true; // datum should set indexor firmware motion params
      UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(firmware, indexor, "datum", 0, blash, 0, ivel, vel,
						      /*islow,*/ slow, dir, acc, dec, runc, holdc);
      mechs[mechName] = mp; // use default motion "datum"
    }
    //if( _verbose )
      clog<<"UFCCDatum::genExec> mech: "<<mechName<<", indexor: "<<indexor<<", vel: "<<vel<<", dir: "<<dir<<", blash: "<<blash<<endl;
    // allow reuse of values?
    pva->clearVal(); // only clear mechName/All input
  } // for each mech.
  return (int) mechs.size();
}

/// special purpose datum all static func.
int UFCCDatum::homeAll(int timeout, UFGem* rec, UFCAR* car) {
  string recname = UFCAServ::_instrum + ":cc:datum";
  UFCCDatum* ccdatum = dynamic_cast< UFCCDatum* > ( _theRecList[recname] );
  if( ccdatum == 0 )
    return -1;

  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec != 0 ) {
    UFCAR* c = rec->hasCAR();
    //if( c != 0 )
    //clog<<"UFCCDatum::homeAll> rec-car-name: "<<c->name()<<endl;
    if( car != c && car != 0 ) {
      car->setBusy(timeout);
      //clog<<"UFCCDatum::homeAll> supplemtary carname: "<<car->name()<<endl;
    }
  }

  // datum all mechanisms
  bool all= true;
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  int nm = ccdatum->homeListAlloc(*mechs, all);
  if( nm < 0 )
    return nm;

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mechs, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCDatum::gohome, arg);

  return (int) thrid;
} // homeAll

/// virtual funcs:
/// perform datum for those mechanisms with non-null inputs
int UFCCDatum::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFCCDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }
 
  // datum all mechanisms using default motion parameters:
  string pvname = name() + ".All";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      pva->clearVal();
      clog<<"UFCCDatum::genExec> starting thread to home ALL mechanism..."<<endl;
      return homeAll(timeout, this, _car);
    }
  }
  // orign one or all mechanisms:
  pvname = name() + ".Origin";
  pva = (*this)[pvname];
  if( pva->isSet() ) {
    string input = pva->valString();
    pva->clearVal();
    return origin(input, timeout, this, _car);
  }

  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >; // datum these
  int nm = homeListAlloc(*mechs);
  // perform requested motion...
  int stat= 0;
  if( nm > 0 ) {
    //if( _verbose )
      clog<<"UFCCDatum::genExec> starting thread to home "<<nm<<" mechanism(s)..."<<endl;
    stat = home(mechs, timeout, this, _car);
  }
  else {
    clog<<"UFCCDatum::genExec> no mechanisms indicated for datum? "<<endl;
    return 0;
  }

  if( stat < 0 ) { 
    clog<<"UFCCDatum::genExec> failed to start home pthread..."<<endl;
    return stat;
  }

  return nm;
} // genExec

int UFCCDatum::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();

  if( pvname.find("Direction") != string::npos ) {
    if( input.find("+") != string::npos || input.find("1") != string::npos ) 
      return 0; // home + ok

    if( input.find("-") != string::npos || input.find("0") != string::npos ) 
      return 0; // home - ok
  }

  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      if( input == "" )
        return 0; // clear ok
      if( input.find("null") != string::npos || input.find("Null") != string::npos || input.find("NULL") != string::npos )
        return 0; // null + ok

      char* hv = (char*)input.c_str();
      while( *hv == ' ' || *hv == '\t' )
        ++hv; // ignore white spaces

      if( isdigit(hv[0]) ) // home velocity must be alphanumric
        return 0;
    }
  }

  return -1;
} // validate


#endif // __UFCCDATUM_CC__
