#if !defined(__UFCAR_CC__)
#define __UFCAR_CC__ "$Name:  $ $Id: UFCAR.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAR_CC__;
  
#include "UFCAR.h"
#include "UFSIR.h"
#include "UFCCInit.h"
#include "UFPVAttr.h"
#include "UFRuntime.h"
#include "strstream"

int UFCAR::_ErrorCnt= 0;

// ctor:

// static class funcs:

// non static funcs:
void UFCAR::setIVAL(int val) {
  UFPVAttr *pin = pival();
  if( pin != 0 ) pin->setVal(val);
}

int UFCAR::setTO(int timeout) {
  int oldto = _timeout; _timeout = timeout; 
  // insure proxy does not get this into infinite recursion...
  if( _proxy != 0 && _proxy->timeout() != _timeout )
    _proxy->setTO(timeout);

  return oldto;
}

// virtuals 
void UFCAR::setBusy(size_t timeout, UFGem* proxy) { 
  if( _paused && _timeout > 0 ) {
    clog<<"UFCAR::setBusy> paused, _timeout: "<<_timeout<<endl;
    return;
  }
  // unlike the CAD setBusy, this assumes its genExec will copy inputs to outputs...
  _paused = false;
  _status = UFGem::_Busy; // setPV(_status); 
  if( proxy ) // proxy is the cad or apply that set this car busy
    _proxy = proxy;

  if( _timeout <= 0 && timeout > 0 ) {
    int nt = insertTO(this); // in theory only CARs need to be in TO list
    clog<<"UFCAR::setBusy> "<<_name<<", _timeout: "<<_timeout<<", insertedTO, TO list size: "<<nt<<endl;
    _timeout = timeout;
  }
  //else { // presumably already in TO list
  //  clog<<"UFCAR::setBusy> should still be it TO list? "<<_name<<", _timeout: "<<_timeout<<endl;
  //}

  // put this logic here and in time-out hearbeat thread?
  string recname = _name;
  //UFRuntime::lowerCase(recname);
  if( recname.find("focus") != string::npos ) // focussing
    UFSIR::setPrep(true);
  if( recname.find("instrumSetup") != string::npos ) // setup opto-mechaniccal configuration
    UFSIR::setPrep(true);
  if( recname.find("obsSetup") != string::npos ) // setup detector configuration
    UFSIR::setPrep(true);
  if( recname.find("end") == string::npos && recname.find("observe") != string::npos )
    UFSIR::setAcq(true); // start obs
  if( recname.find("pause") != string::npos )
    UFSIR::setRdout(); // false
  if( recname.find("continue") != string::npos )
    UFSIR::setRdout(true); 
}

void UFCAR::pause() {
  _paused = true;
  if( _proxy ) 
    _proxy->pause(); // insure proxy is setIdle (ok if redundant)
}

int UFCAR::start(int timeout) {
  //_paused = false;
  // genExec must deterime pause/continue, busy/idle/error action on input:
  return genExec(timeout);
}

void UFCAR::setIdle() {
  if( _verbose )
   clog<<"UFCAR::setIdle> "<<_name<<" timeout ticks: "<<_timeout<<endl;
  _ErrorCnt = 0;
  if( _timeout == 0 ) {
    //clog<<"UFCAR::setIdle> "<<_name<<" is already idle..."<<endl;
    return;
  }
  //removeTO(this); allow hearbeat thread to handle TO list?
  _status= UFGem::_Idle; 
  _timeout= 0; 
  setPV(_status);
  //clog<<"UFCAR::setIdle> "<<_name<<" idle, _timeout: "<<_timeout<<endl;

  if( _proxy != 0 && _proxy != this ) {
    _proxy->setIdle(); // insure proxy is setIdle (ok if redundant)
    string proxyname = _proxy->name();
    // this is a kludge and really should be put elsewhere...
    // after a fully successful datum, the datum count should be incremented,
    // after a full successful init, it should be (re)set to 0
    clog<<"UFCAR::setIdle> _name: "<<_name<<", proxyname: "<<proxyname<<endl;
    UFSIR* sad = 0;
    if( UFCCInit::_datumCnt != 0 ) sad = UFSIR::_theSAD[*UFCCInit::_datumCnt];
    if( sad == 0 ) {
      clog<<"UFCAR::setIdle> _theSAD lacks "<<*UFCCInit::_datumCnt<<endl;
      return;
    }
    if( proxyname.find("Datum") != string::npos ) { // special case
      // successful datums should increment system and/or low level datum cnt
      UFPVAttr* pva = sad->pinp();
      if( pva == 0 ) { clog<<"UFCAR::setIdle> null INP PVAttr* for sad: "<<sad->name()<<endl; return; }
      double val = pva->getNumeric();
      int newval = (int)val;
      if( _name.find("Init") != string::npos ) // clear the datum count input field
	newval = 0;
      else // if( val >= 0 ) 
        ++newval;
      pva->setVal(newval); // this now calls PV::write which should process sad...
      // call genExec to process record input to output
      //sad->start();
    }
  }
}

void UFCAR::setErr(string& errmsg, int err) {
  //if( _verbose )
    clog<<"UFCAR::setErr> "<<name()<<", err: "<<err<<", "<< errmsg<<endl;

  //removeTO(this); allow hearbeat thread to handle TO list?
  if( err != 0 ) {
    _ErrorCnt++;
    _status= UFGem::_Error;
  }
  else {
    _ErrorCnt = 0;
  }
  _timeout= 0;
  setPV(_status); setPV(err, poerr()); setPV(errmsg, pomss()); 
  if( _proxy != 0 && _proxy != this )
    _proxy->setErr(errmsg, err); // insure proxy is setErr (ok if redundant)
}

string UFCAR::errMsg(int val) { 
  strstream s;
  s<<_name<<" generic error: "<<val<<ends;
  string msg = s.str(); delete s.str();
  return msg;
}

int UFCAR::genExec(int timeout) {
  UFPVAttr *pin = pival(), *pout = pval();
  if( pin == 0 ) { clog<<"UFCAR::genExec> null PVAttr* pival for: "<<_name<<endl; return -1; }
  if( pout == 0 ) { clog<<"UFCAR::genExec> null PVAttr* pval for: "<<_name<<endl; return -1; }

  //clog<<"UFCAR::genExec> pin: "<<pin->name()<<", pout: "<<pout->name()<<endl;
  int val = (int) pin->getNumeric();
  if( val < 0 ) { // somehow this is being processed on a 'cleared' input?
    if( _verbose ) {
      if( val == INT_MIN )
        clog<<"UFCAR::genExec> "<<_name<<", no-op on cleared input..."<<endl;
      else    
        clog<<"UFCAR::genExec> "<<_name<<", no-op inpu: "<<val<<endl;
    }
    return -1; // do nothing!
  }
  
  pout->copyVal(pin);
  val = (int) pout->getNumeric();
  //clog<<"UFCAR::genExec> copied pin: "<<pin->name()<<", to pout: "<<pout->name()<<" val: "<<val<<endl;

  pin->clearVal(false); // and clear input without write/post monitor (false)

  pin = pierr(); pout = poerr();
  if( pin == 0 ) { clog<<"UFCAR::genExec> null PVAttr* pierr for: "<<_name<<endl; return -1; }
  if( pout == 0 ) { clog<<"UFCAR::genExec> null PVAttr* poerr for: "<<_name<<endl; return -1; }
  //clog<<"UFCAR::genExec> pin: "<<pin->name()<<", pout: "<<pout->name()<<endl;
  pout->copyVal(pin);
  pin->clearVal(false); // and clear input without write/post monitor (false)

  int err = (int) pout->getNumeric();
  pin = pimss(); pout = pomss();
  if( pin == 0 ) { clog<<"UFCAR::genExec> null PVAttr* pimss for: "<<_name<<endl; return -1; }
  if( pout == 0 ) { clog<<"UFCAR::genExec> null PVAttr* pomss for: "<<_name<<endl; return -1; }
  //clog<<"UFCAR::genExec> pin: "<<pin->name()<<", pout: "<<pout->name()<<endl;
  pout->copyVal(pin);
  pin->clearVal(false); // and clear input without write/post monitor (false)
  string errmsg = pout->valString();

  if( _verbose )
    clog<<"UFCAR::genExec> "<<_name<<" val: "<<val<<endl;

  switch(val) {
  case UFGem::_Error: clog<<"UFCAR::genExec> "<<_name<<", error: "<<errmsg<<endl;
    setErr(errmsg, err);
    break;
  case UFGem::_Pause: clog<<"UFCAR::genExec> "<<_name<<" pausing..."<<endl;
    pause();
    break;
  case UFGem::_Busy: clog<<"UFCAR::genExec> "<<_name<<" go busy..."<<endl;
    setBusy(timeout);
    break;
  case UFGem::_Idle: clog<<"UFCAR::genExec> "<<_name<<" go idle..."<<endl;
  default:
    setIdle();
    break;
  }
  return val;
}

// (protected) helper  funcs:
void UFCAR::_create(UFGem* proxy) {
  _type = UFGem::_CAR; _mark = true; // cars are always marked for processing on any new (non-clear) input
  _proxy = proxy; // the apply or cad associated with this car
  string recname = _name;
  string pvname = recname + ".IVAL";
  // input to car is "ival"
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInputPV(pva);
  // and other inputs pvs: 
  pvname = recname + ".CLID";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInputPV(pva);

  pvname = recname + ".IMSS";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInputPV(pva);

  pvname = recname + ".IERR";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInputPV(pva);

  // and outputs:
  pvname = recname + ".OERR";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachOutputPV(pva);

  pvname = recname + ".VAL";
  pva = new (nothrow) UFPVAttr(pvname, INT_MIN); // int outval
  attachOutputPV(pva);

  registerRec();
};

#endif // __UFCAR_CC__
