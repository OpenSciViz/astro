#if !defined(__UFOBSSETUP_CC__)
#define __UFOBSSETUP_CC__ "$Name:  $ $Id: UFObsSetup.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFOBSSETUP_CC__;

#include "UFObsSetup.h"
#include "UFInstrumSetup.h"
#include "UFObserve.h"
#include "UFDCMCECmd.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// may be set by InstrumSetup WheelBias selection, or overriden in this CAD
// should be cleared each time this record gets processed.
string UFObsSetup::_BiasMode;

// ctors:

// static class funcs:
void* UFObsSetup::sim(void* p) {
  // obssetup really just needs ?
  UFObsSetup::OSThrdArg* arg = (UFObsSetup::OSThrdArg*) p;
  UFObsSetup::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFObsSetup::sim>... "<<endl;
  if( parms )
    clog<<"UFObsSetup::sim> exptime: "<<parms->_exptime<<endl;

  // free arg
  delete arg;

  //UFSIR::setAcq(true);
  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if(c != 0 && c != car ) c->setIdle();
  }

  //UFSIR::setAcq(false);
  // exit daemon thread
  return p;
}

/// general purpose obssetup func. 

/// arg: 
int UFObsSetup::config(UFObsSetup::Parm* p, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  clog<<"UFObsSetup::config> UFObserve::_BiasMode: "<<UFObserve::_BiasMode<<endl;
  p->_dparms = new UFDCSetup::DetParm; // default ctor expcnt= 1 -- more refactoring needed here!
  p->_dparms->_expcnt = p->_expcnt;
  p->_dparms->_sec = (int)p->_exptime;
  p->_dparms->_cdsreads = p->_numreads;
 
  // reuse currently set bias or reset based on IS input/override:
  if( UFObsSetup::_BiasMode.find("null") != string::npos ||
      UFObsSetup::_BiasMode.find("Null") != string::npos ||
      UFObsSetup::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = UFObsSetup::_BiasMode;
    clog<<"UFObsSetup::config> UFObsSetup::_BiasMode: "<<UFObsSetup::_BiasMode<<endl;
  }
  else if( UFInstrumSetup::_BiasMode.find("null") != string::npos ||
	   UFInstrumSetup::_BiasMode.find("Null") != string::npos ||
	   UFInstrumSetup::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = UFInstrumSetup::_BiasMode;
    clog<<"UFObsSetup::config> UFInstrumSetup::_BiasMode: "<<UFInstrumSetup::_BiasMode<<endl;
  }
  else if( UFObserve::_BiasMode.find("null") != string::npos ||
	   UFObserve::_BiasMode.find("Null") != string::npos ||
	   UFObserve::_BiasMode.find("NULL") != string::npos ) {
    UFObserve::_BiasMode = "imaging"; // default if never set
  }

  if( !UFObserve::_BiasMode.empty() &&
      ( UFObserve::_BiasMode.find("null") != string::npos ||
	UFObserve::_BiasMode.find("null") != string::npos ||
	UFObserve::_BiasMode.find("null") != string::npos ) ) {
    clog<<"UFInstrumSetup::allocParm> allocated/preset BiasMode: "<<_BiasMode<<endl;
    UFDCSetup::allocBiasAndPreamps(UFObserve::_BiasMode, p->_dparms->_bias, p->_dparms->_preamp);
  }
  else {
    clog<<"UFInstrumSetup::config> Not presetting BiasModes..."<<endl;
  }
 
  // create and run pthread to obssetup whatever is currently being performed,
  // then either reset car to idle or error.
  // thread should free the tParm* allocations...
  //
  UFObsSetup::OSThrdArg* arg = new UFObsSetup::OSThrdArg(p, rec, car);
  pthread_t thrid= 0;
  if( _sim ) {
    thrid = UFPosixRuntime::newThread(UFObsSetup::sim, arg);
    return (int)thrid;
  }

  if( p->_dparms ) { 
    clog<<"UFObsSetup::config> using DCSetup::config to handle DC parms."<<endl;
    thrid = UFDCSetup::config(p->_dparms, timeout, rec, car);
    if( thrid <= 0 ) {
      clog<<"UFObsSetup::config> DC setup thread problem."<<endl;
      return (int)thrid;
    }
  }
  else {
    clog<<"UFObsSetup::config> No DC parms?"<<endl;
  }

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFObsSetup::_create() {
  _BiasMode = "null";
  UFPVAttr* pva= 0;
  string pvname = _name + ".WheelBiasMode"; // Imaging or Spectroscopy MCE4 biases (from insrtumSetup)
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva);

  pvname = _name + ".BiasMode";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // override value?
  attachInOutPV(this, pva);

  pvname = _name + ".OverrideBias";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); // true/false or value?
  attachInOutPV(this, pva);

  pvname = _name + ".ExpTime";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1.0);
  attachInOutPV(this, pva);

  // SciMode == 1 frame cnt; EngMode == 2 (mce4 CT for EngMode is TBD)
  pvname = _name + ".ReadoutMode"; // alias "FrameMode" or "
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Sci.(<=1) or Eng.(>=2)");
  attachInOutPV(this, pva);

  /*
   flam:obsSetup.numReads = < 1 | 3 | 4 | ... | 16 >
   This is the commanded number of non-destructive reads.  numReads=1
   corresponds to CDS (CT 20, no ldvar); numReads >= 3 correspondsd to multiple reads, for
   which ldvar 2 gets set to numReads-2 (LDVAR NR-2) and cycle type (CT) 42 is called.
  */
  pvname = _name + ".NumReads"; // < 1 | 3 | 4 | ... | 16 >
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)1);
  attachInOutPV(this, pva);

  pvname = _name + ".PixLUTFile"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "UFFlamingos.lutBE");
  attachInOutPV(this, pva);

  pvname = _name + ".DitherIndex"; // <0 indicate automatic incrementing?
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1); //new UFPVAttr(pvname, (int)INT_MIN);
  attachInOutPV(this, pva);

  pvname = _name + ".FITSFileName"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Flamingos2.fits");
  attachInOutPV(this, pva);

  pvname = _name + ".PNG-Or-JPEGFile"; // for local/nfs pngs file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Flamingos2.png");
  attachInOutPV(this, pva);

  pvname = _name + ".DataDir"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "/data/flam/");
  attachInOutPV(this, pva);

  pvname = _name + ".NFSHost"; // for local/nfs fits file
  pva = (*this)[pvname] = new UFPVAttr(pvname, "localhost");
  attachInOutPV(this, pva);

  pvname = _name + ".DS9Display";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); //new UFPVAttr(pvname, (int)INT_MIN);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform obssetup
int UFObsSetup::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFObsSetup::genExec> no pvs?"<<endl;
    return (int) pvcnt;
  }

  int obsmode = UFObsSetup::_Imaging;
  string input, obsmodebias = "imaging";
  string pvname = _name + ".WheelBiasMode"; // mce4 det array bias for imaging or spectroscopy
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    obsmodebias = input;
    clog<<"UFObsSetup::genExec> observing mode bias (from instrumSetup): "<<input<<endl;
    if( obsmodebias.find("spect") != string::npos ||
	obsmodebias.find("Spect") != string::npos ||
	obsmodebias.find("SPECT") != string::npos ) obsmode = UFObsSetup::_Spect;
    //pva->clearVal(); // clear?
  }

  string overridebias; // input is anything non-null
  pvname = _name + ".BiasMode"; // mce4 det array bias for imaging or spectroscopy
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    overridebias = input; // input is anything non-null
    clog<<"UFObsSetup::genExec> new observing mode bias: "<<input<<endl;
    pva->clearVal(); // clear!
  }

  string override; // input is anything non-null
  pvname = _name + ".OverrideBias"; // override mce4 bias for imaging or spectroscopy
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    override = input; // input is anything non-null
    clog<<"UFObsSetup::genExec> override wheel bias: "<<obsmodebias<<", with: "<<overridebias<<endl;
    obsmodebias = overridebias;
    pva->clearVal(); // clear!
  }

  float exptime= 0; // input is anything non-null
  pvname = _name + ".ExpTime"; // in seconds
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    UFDCMCECmd::_exptime = exptime = (float) pva->getNumeric(); // input is anything non-null
    clog<<"UFObsSetup::genExec> exposure time: "<<exptime<<endl;
    pva->clearVal(); // clear!
  }

  int index= 1; 
  pvname = _name + ".DitherIndex";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    index = UFDCMCECmd::_index = (int)pva->getNumeric(); // integer input
    clog<<"UFObserve::genExec> DitherIndex: "<<index<<endl;
  }

  int ds9 = 0; //UFDCMCECmd::_ds9 = 0;
  pvname = _name + ".DS9Display";
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    UFDCMCECmd::_ds9 = ds9 = (int) pva->getNumeric(); // input is anything non-null
    clog<<"UFObsSetup::genExec> DS9 frame(s) Display, if any: "<<ds9<<endl;
    //pva->clearVal(); // clear?
  }

  // mce4 det array readout mode affects expcnt: SciMode== 1, EngMode== 2
  // current lab setup --
  // SciMode: LDVAR 10 1 and CT 10
  // EngMode: LDVAR 10 2 and CT 30
  int nfrm = UFDCMCECmd::_expcnt = 1;
  string rdout; // input is anything non-null
  pvname = _name + ".ReadoutMode"; // alias "FrameMode"
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    rdout = input;
    nfrm = (int)pva->getNumeric();
    clog<<"UFObsSetup::genExec> mce4 readout mode: "<<rdout<<", nfrm: "<<nfrm<<endl;
    if( rdout.find("eng") != string::npos || rdout.find("Eng") != string::npos || rdout.find("ENG") != string::npos ) {
      clog<<"UFObsSetup::genExec> mce4 readout mode: "<<rdout<<", assume Eng. Mode == 2 frames."<<nfrm<<endl;
      UFDCMCECmd::_expcnt = nfrm = 2; // must be engineering mode
    }
    else if( nfrm > 1 ) {
      clog<<"UFObsSetup::genExec> mce4 readout mode: "<<rdout<<", assume Eng. Mode >= 2 frames: "<<nfrm<<endl;
      UFDCMCECmd::_expcnt = nfrm;
    }
    else {
      UFDCMCECmd::_expcnt = nfrm = 1; // sci mode
      clog<<"UFObsSetup::genExec> mce4 readout mode: "<<rdout<<", assume Sci. Mode == 1 frame: "<<nfrm<<endl;
    }
    //pva->clearVal(); // clear
  }

  int nr= 0;
  pvname = _name + ".NumReads"; // mce4 internal reads (and coadds?)
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    nr = (int)pva->getNumeric(); // input is anything non-null
    clog<<"UFObsSetup::genExec> number of mce4 internal reads (and coadds/): "<<nr<<endl;
    //pva->clearVal(); // clear
  }

  string fitsfile= "Flamingos2.fits"; // input is anything non-null
  pvname = _name + ".FITSFileName"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    fitsfile = UFDCMCECmd::_fitsfile = input;
    clog<<"UFObsSetup::genExec> local FITS file: "<<fitsfile<<endl;
    //pva->clearVal(); // clear
  }

  string lutfile= "UFFlamingos.lutBE"; // input is anything non-null
  pvname = _name + ".PixLUTFile"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    lutfile = UFDCMCECmd::_lutfile = input;
    clog<<"UFObsSetup::genExec> Pixel LUT (sort) file: "<<lutfile<<endl;
    //pva->clearVal(); // clear
  }

  string pngjpegfile= "UFFlamingos.lutBE"; // input is anything non-null
  pvname = _name + ".PNG-Or-JPEGFile"; // for local/nfs pngs file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    pngjpegfile = UFDCMCECmd::_pngjpegfile = input;
    clog<<"UFObsSetup::genExec> local PNG or JPEG file: "<<pngjpegfile<<endl;
    //pva->clearVal(); // clear
  }

  string datadir= "/data/flam2/"; // input is anything non-null
  pvname = _name + ".DataDir"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    datadir = UFDCMCECmd::_datadir = input;
    clog<<"UFObsSetup::genExec> data directory: "<<datadir<<endl;
    //pva->clearVal(); // clear
  }

  string nfshost= "localhost"; // input is anything non-null
  pvname = _name + ".NFSHost"; // for local/nfs fits file
  pva = (*this)[pvname];
  if( pva != 0 )
    input = pva->valString(); // input is anything non-null
  else 
    input = "null";
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    nfshost = UFDCMCECmd::_nfshost = input;
    clog<<"UFObsSetup::genExec> localhost or nfs mount host: "<<nfshost<<endl;
    //pva->clearVal(); // clear
  }

  UFObsSetup::Parm* p = new UFObsSetup::Parm(obsmode, nfrm, nr, exptime, fitsfile, datadir, nfshost);
  int stat = config(p, timeout, this, _car);  // perform requested obssetup

  if( stat < 0 ) { 
    clog<<"UFObsSetup::genExec> failed to start obssetup pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFObsSetup::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFOBSSETUP_CC__
