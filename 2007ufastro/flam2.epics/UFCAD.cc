#if !defined(__UFCAD_CC__)
#define __UFCAD_CC__ "$Name:  $ $Id: UFCAD.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAD_CC__;
  
#include "UFCAD.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctor:

// static class funcs:
   
bool UFCAD::connected(UFClientSocket* agntsoc, int trycnt) {
  if( agntsoc == 0 )
    return false;

  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  bool validsoc = agntsoc->validConnection();
  int cnt = trycnt;
  while( --cnt > 0 && !validsoc ) { // try a few times
    validsoc = agntsoc->validConnection();
    if( !validsoc ) {
      clog<<"UFCAD::connnected> failed socket validation? trycnt: "<<cnt<<endl;
        UFPosixRuntime::sleep(0.25);
    }
  }
  return validsoc;
}

// choose primay CAR channel name to send to agent:
string UFCAD::carChan(UFGem* rec, UFCAR* car) {
  string carname = "";
  if( rec == 0 && car == 0 )
    return carname;

  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car && carname == "" ) {
    carname = car->name();
  }

  carname += ".IVAL";
  return carname;
}

// non static funcs:

// virtuals 
void UFCAD::setIdle() {
  _timeout = 0;
  if( _car) { // force car to process in/out?
    //clog<<"UFCAD::setIdle> "<<_name<<", car: "<<_car->name()<<endl;
    _car->setIVAL(UFGem::_Idle);
    _car->genExec(1); // force car to process in/out if its setIde does not?
  }
}

// overriding setBusy allows preprocessing of record input->outputs 
void UFCAD::setBusy(size_t timeout, UFGem* proxy) {
  if( _timeout > 0 ) {
    if( _verbose )
      clog<<"UFCAD::setBusy> _timeout: "<<_timeout<<" already busy..."<<endl;
    return;
  }
  _timeout = timeout; 
  if( _verbose ) {
    clog<<"UFCAD::setBusy> "<<_name<<", busy until completion or timeout in heartbeat ticks: "<<timeout<<endl;
  }
  int nc = copyAllInOut(this);
  //if( _verbose )
    clog<<"UFCAD::setBusy> "<<_name<<": set output parameter fields via copy of "<<nc<<" inputs..."<<endl;

  if( _car ) {
    _car->setIVAL(UFGem::_Busy);
    _car->genExec(1); // force car to process in/out since its setBusy does not...
    //if( proxy != 0 && proxy != this ) {
    //  _car->setBusy(timeout, proxy);
  }
}

// (protected) helper  funcs:
void UFCAD:: _create(const string& recname, UFCAR* car) {
  _type = UFGem::_CAD;
  _car = car;
  //if( UFGem::_vverbose)
  //clog<<"UFCAD:: _create> "<<recname<<", "<< _name<<endl;
  _name = recname; 
  // input to cad is "dir"
  string pvname = recname + ".DIR";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN); // int enum 1 == clear
  attachInputPV(pva);

  // CLID elswhere
  pvname = recname + ".ICID";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInputPV(pva);

  // mark input field
  pvname = recname + ".MARK";
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachOutputPV(pva);

  // alias marked output field
  pvname = recname + ".marked";
  _pvalias[pvname] = pva;
  //attachOutputPV(pva);

  // message output
  pvname = recname + ".MESS";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachOutputPV(pva);
  
  // output client id
  pvname = recname + ".OCID";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachOutputPV(pva);

  pvname = recname + ".VAL";
  pva = new (nothrow) UFPVAttr(pvname, INT_MIN); // int outval
  attachOutputPV(pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
};  

#endif // __UFCAD_CC__
