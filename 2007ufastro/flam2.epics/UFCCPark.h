#if !defined(__UFCCPark_h__)
#define __UFCCPark_h__ "$Name:  $ $Id: UFCCPark.h,v 0.1 2005/05/10 20:28:59 hon Exp $"
#define __UFCCPark_H__(arg) const char arg##CCPark_h__rcsId[] = __UFCCPark_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFCCPark : public UFCAD {
public:
  inline UFCCPark(const string& instrum= "instrum") : UFCAD(instrum+":cc:park") { _create(); }
  inline UFCCPark(const string& instrum, UFCAR* car) : UFCAD(instrum+":cc:park", car) { _create(); }
  inline virtual ~UFCCPark() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // pthread func.
  static void* seekit(void* p);

  // genExec invokes this, and so can CCSetup...
  static int seek(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // available to IS
  static int seekAll(int timeout, UFGem* rec= 0, UFCAR* car= 0);

  static int _ParkCnt;

protected:
  // ctor helper
  void _create();
};

#endif // __UFCCPark_h__
