#if !defined(__UFECPark_h__)
#define __UFECPark_h__ "$Name:  $ $Id: UFECPark.h,v 0.1 2006/05/19 20:00:52 hon Exp $"
#define __UFECPark_H__(arg) const char arg##ECPark_h__rcsId[] = __UFECPark_h__;

#include "UFCAD.h"
#include "UFECSetup.h"

class UFECPark : public UFCAD {
public:
  inline UFECPark(const string& instrum= "instrum") : UFCAD(instrum+":ec:park") { _create(); }
  inline virtual ~UFECPark() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* envThrd(void* p);
  static int env(UFECSetup::EnvParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  // available to IS
  inline static int env(int timeout, UFGem* rec= 0, UFCAR* car= 0) { return 0; }

protected:
  // ctor helper
  void _create();
};

#endif // __UFECPark_h__
