#if !defined(__UFDATUM_CC__)
#define __UFDATUM_CC__ "$Name:  $ $Id: UFDCDatum.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDATUM_CC__;

#include "UFDCDatum.h"
#include "UFCAServ.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

/* from nick's email dec 16, 2005, and jun 26, 2006, and jeff jun 27
stop
sim 0
apwb engineering
p 1400030 3FFFF
pbc 10
ldvar 0 10
ldvar 1 1000
ldvar 2 2
ldvar 10 1
timer 0 6 s
dssb 0 195 -- back to 180 ala nicks' aug 22, 2006 email
dssb 1 180 -- backto 255
dssb 2 219 -- ok no change
dlab
dsap 190 ??
ct 20
//start 
*/

// ctors:

// helpers:
int UFDCDatum::_defaultCmdsLab(std::deque< string >& cmds) {
  cmds.clear();
  string cmd = "Raw ";
  string cmdpar = "stop"; cmds.push_back(cmd); cmds.push_back(cmdpar); 
  cmdpar = "sim 0"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "apwb engineering"; cmds.push_back(cmd); cmds.push_back(cmdpar); 
  cmdpar = "p 1400030 3FFFF"; cmds.push_back(cmd); cmds.push_back(cmdpar); 
  cmdpar = "pbc 10"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "ldvar 0 10"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "ldvar 1 1000"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "ldvar 2 2"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "ldvar 10 1"; cmds.push_back(cmd); cmds.push_back(cmdpar); // expcnt
  cmdpar = "timer 0 6 s"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "dssb 0 180"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "dssb 1 225"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "dssb 2 219"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  //cmdpar = "dsdb"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "dlab"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  //cmdpar = "dsap 220"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "dsap 160"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  //cmdpar = "ct 10"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  cmdpar = "ct 20"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  // evidently this start produces a frame:
  //cmdpar = "start"; cmds.push_back(cmd); cmds.push_back(cmdpar);
  clog<<"UFDCDatum::_defaultCmdsLab> current default CT: "<<cmdpar<<endl;
  return (int) cmds.size();
}

int UFDCDatum::_parmCmds(UFDCSetup::DetParm* parm, std::deque< string >& cmdvec) {
  // check if we want to use lab override:
  if( parm->_lab > 0 )
    return _defaultCmdsLab(cmdvec);

  cmdvec.clear();
  string cmd = "Raw "; cmd += "Sim!!ErrDatum ";
  string cmdpar = "SIM 0"; // turn off simulation
  cmdvec.push_front(cmdpar); cmdvec.push_front(cmd);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "Kelvin!!ErrDatum ";
  cmdpar = "ARRAY_TEMP 75.5"; // inform mce4 that array is cold and can be turned on and clocked
  if( parm->_k > 0 ) {
    strstream s; s<<"ARRAY_TEMP "<<parm->_k<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "ExpCnt!!ErrDatum ";
  cmdpar = "LDVAR 10 1";
  if( parm->_expcnt > 0 ) {
    strstream s; s<<"LDVAR 10 "<<parm->_expcnt<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "PixBaseClck!!ErrDatum ";
  cmdpar = "PBC 10";
  if( parm->_pbc > 0 ) {
    strstream s; s<<"PBC "<<parm->_pbc<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "CDSReadout!!ErrDatum ";
  cmdpar = "LDVAR 2 4";
  if( parm->_cdsreads > 0 ) {
    strstream s; s<<"LDVAR 2 "<<parm->_cdsreads<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "SAD";
  cmdpar = UFCAServ::_instrum + ":sad:EXPTIME";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "ExpTime!!ErrDatum";
  cmdpar = "TIMER 0 1 S";
  if( parm->_sec > 0 ) {
    strstream s; s<<"TIMER 0 "<<parm->_sec<<" S"<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "Presets!!ErrDatum ";
  cmdpar = "LDVAR 0 10"; // 10 presets
  if( parm->_presets > 0 ) {
    strstream s; s<<"LDVAR 0 "<<parm->_presets<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "Postsets!!ErrDatum ";
  cmdpar = "LDVAR 1 1000"; // 1000 postsets
  if( parm->_postsets > 0 ) {
    strstream s; s<<"LDVAR 0 "<<parm->_postsets<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "SetDefaults!!ErrDatum ";
  cmdpar = "DSAB";
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "SetDefaults!!ErrDatum ";
  cmdpar = "DSAP";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "LatchDefaults!!ErrDatum ";
  cmdpar = "DLAB";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
 
  cmd = "Raw "; cmd += "LatchDefaults!!ErrDatum ";
  cmdpar = "DLAP";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "SAD";
  cmdpar = UFCAServ::_instrum + ":sad:CYCLETYP";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  cmd = "Raw "; cmd += "CycleType 0!!ErrDatum";
  cmdpar = "CT 0"; // idle == datum
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
 
  cmd = "Raw "; cmd += "Start!!ErrDatum";
  cmdpar = "START"; // run idle cycle
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  return (int) cmdvec.size();
}

// static class funcs:
void* UFDCDatum::datumMCE4(void* p) {
  // datum really just needs ?
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parm = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = UFCAServ::_instrum + ":dc:datumC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";
  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  bool validsoc = connected(UFDCSetup::_connection);
  if( !_sim && !validsoc ) {
    clog<<"UFDCDatum::datumMCE4> not connected to UFMCE agent, need to (re)init..."<<endl;
    // and free the arg & parms
    delete arg;
    return p;
  }
  int err= 0;
  string errmsg;
  deque< string > cmds;

  if( !_sim ) {
     _parmCmds(parm, cmds);

    string cmd = "SAD";
    //string cmdpar = UFCAServ::_instrum + ":sad:FRMMODE";
    string cmdpar = UFCAServ::_instrum + ":sad:MCEReply";
    cmds.push_front(cmdpar); cmds.push_front(cmd);
    //if( _verbose )
      clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    cmd = "CAR";
    cmdpar = carname;
    cmds.push_front(cmdpar); cmds.push_front(cmd);
    //if( _verbose )
      clog<<"UFDCDatum::datumMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;


    UFStrings req(name, cmds);
    int ns = UFDCSetup::_connection->send(req);
    if( ns <= 0 ) {
      clog<<"UFDCDatum::datumMCE4> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = 1;
      errmsg = "Failed MCE4 Datum";
    }
    //if( _verbose )
      clog<<"UFDCDatum::datumMCE4> sent req: "<< req.name() << " " << req.timeStamp() <<", car: " <<cmds[1]<<endl;
    //int nr = UFDCSetup::_motoragntsoc->recv(req);
  }
  else { // simple simulation...
    clog<<"UFDCDatum::datumMCE4>... "<<endl;
    if( parm ) {
      clog<<"UFDatum::datuming> detector pixels: "<<parm->_w * parm->_h<<endl;
    }
    // set cad/car idle after 1/2:
    UFPosixRuntime::sleep(0.5);
  }
  // free arg
  delete arg;

  if( car ) {
    if( err != 0 )
      car->setErr(errmsg);
    else
      car->setIdle();
  }
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if( c != 0 && c != car ) {
      if( err != 0 )
	c->setErr(errmsg);	
      else
        c->setIdle();
    }
  }
  // exit daemon thread
  return p;
}

/// general purpose datum func. 

/// arg:
int UFDCDatum::datum(UFDCSetup::DetParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec != 0 ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && c != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to datum whatever is currently being performed
  // then either reset car to idle or error. thread should free the tParm* allocations...
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCDatum::datumMCE4, arg);

  return (int) thrid;
}

/// arg:
int UFDCDatum::datum(int timeout, int lab, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec != 0 ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && c != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to datum whatever is currently being performed
  // then either reset car to idle or error. thread should free the tParm* allocations...
  UFDCSetup::DetParm* parms = new UFDCSetup::DetParm;
  parms->_lab = lab;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCDatum::datumMCE4, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCDatum::_create() {
  UFPVAttr* pva= 0;
  string pvname;
  /*
  pvname = _name + ".CycleType"; // CT 0 == datum, 10, 20, 40, ?
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);
  */

  pvname = _name + ".Kelvin"; // "ARRAY_TEMP k"
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInOutPV(this, pva);

  pvname = _name + ".LabDef"; // lab (built-in) defaults overrides all other
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".ExpCnt";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".Width";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 2048);
  attachInOutPV(this, pva);

  pvname = _name + ".Height";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 2048);
  attachInOutPV(this, pva);

  pvname = _name + ".BitDepth";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 2048);
  attachInOutPV(this, pva);

  pvname = _name + ".Seconds"; // Timer 0 # (1->?)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".MilliSec"; // Timer 0 # 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".CDSReads"; // LDVAR 2 # (4-64)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 4);
  attachInOutPV(this, pva);

  pvname = _name + ".Presets"; // LDVAR 0 # (10)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 10);
  attachInOutPV(this, pva);

  pvname = _name + ".Postsets"; // LDVAR 1 # (1000)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1000);
  attachInOutPV(this, pva);

  pvname = _name + ".PixelBaseClock"; // PBC 10 
  pva = (*this)[pvname] = new UFPVAttr(pvname, 10);
  attachInOutPV(this, pva);

  pvname = _name + ".AllPreamp"; // DAC_SET_ALL_PREAMP (0-255)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".AllBias"; // DAC_SET_ALL_BIAS (0-255)
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform dc datum
int UFDCDatum::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFDCDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  int ct= 0, lab = -1;
  double k= -1;
  string pvname, passwd;
  UFPVAttr* pva = 0;

  /*
  pvname = _name + ".CycleType";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      ct = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> ct: "<<ct<<endl;
      pva->clearVal(); // clear
    }
  }
  */

  pvname = _name + ".Kelvin";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      double val = pva->getNumeric();
      clog<<"UFDCDatum::genExec> lab: "<<val<<endl;
      if( val >=  0 )
	k = val; 
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".LabDef";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      int ival = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> lab: "<<ival<<endl;
      if( ival >=  0 )
	lab = 1; 
      pva->clearVal(); // clear
    }
  }

  int excnt= 1;
  pvname = _name + ".ExpCnt";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      excnt = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> excnt: "<<excnt<<endl;
      pva->clearVal(); // clear
    }
  }

  int w= 2048;
  pvname = _name + ".Width";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      w = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> w: "<<w<<endl;
      pva->clearVal(); // clear
    }
  }

  int h= 2048;
  pvname = _name + ".Height";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      h = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> h: "<<h<<endl;
      pva->clearVal(); // clear
    }
  }

  int d= 32;
  pvname = _name + ".BitDepth";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      d = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> d: "<<d<<endl;
      pva->clearVal(); // clear
    }
  }

  int s= 1;
  pvname = _name + ".Seconds";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      s = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> s: "<<s<<endl;
      pva->clearVal(); // clear
    }
  } 
  
  int ms= 0;
  pvname = _name + ".MilliSec";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      ms = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> ms: "<<ms<<endl;
      pva->clearVal(); // clear
    }
  }

  int rds= 0;
  pvname = _name + ".CDSReads";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      rds = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> rds: "<<rds<<endl;
      pva->clearVal(); // clear
    }
  }

  int presets= 0;
  pvname = _name + ".Presets";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      presets = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> presets: "<<presets<<endl;
      pva->clearVal(); // clear
    }
  }

  int postsets= 0;
  pvname = _name + ".Postsets";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      postsets = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> postsets: "<<postsets<<endl;
      pva->clearVal(); // clear
    }
  }

  int pbc= 0;
  pvname = _name + ".PixelBaseClock";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      pbc = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> pbc: "<<pbc<<endl;
      pva->clearVal(); // clear
    }
  }

  std::map<string, double>* allpreamp= 0;
  std::map<string, double>* allbias= 0;
  UFDCSetup::allocBiasAndPreamps("imaging", allbias, allpreamp);
  pvname = _name + ".AllPreamp";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      int all = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> allpreamp: "<<all<<endl;
      pva->clearVal(); // clear
    }
    else {
      delete allpreamp; allpreamp = 0;
    }
  }

  pvname = _name + ".AllBias";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      int all = (int)pva->getNumeric();
      clog<<"UFDCDatum::genExec> allbias: "<<all<<endl;
      pva->clearVal(); // clear
    }
    else {
      delete allbias; allbias = 0;
    }
  }

  int sim= INT_MIN, bootq= INT_MIN;
  UFDCSetup::DetParm* p = new UFDCSetup::DetParm(sim, bootq, lab, ct, excnt, s, ms, rds, w, h, d,
						 pbc, presets, postsets, k, passwd, allpreamp, allbias);
  int stat = datum(p, timeout, _car);  // perform requested datum

  if( stat < 0 ) { 
    clog<<"UFDCDatum::genExec> failed to start datum pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCDatum::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFDATUM_CC__
