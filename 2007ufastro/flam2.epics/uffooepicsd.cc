#if !defined(__uffooepicsd_cc__)
#define __uffooepicsd_cc__ "$Name:  $ $Id: uffooepicsd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __uffooepicsd_cc__;
 
#include "UFCAServD.h"
//__UFCAServD_H__(uffooepicsd_cc);
 
int main(int argc, char** argv, char** envp) {
  // set ecpics db name:
  UFCAServ::_instrum = UFCAServD::_instrum = "foo";
  // and call UFCAServ main:
  return UFCAServD::main(argc, argv, envp, "foo");
}
 
#endif // __uffooepicsd_cc__
