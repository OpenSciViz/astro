#if !defined(__UFTEST_CC__)
#define __UFTEST_CC__ "$Name:  $ $Id: UFTest.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFTEST_CC__;

#include "UFTest.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFTest::testAll(void* p) {
  // test really just needs?
  UFTest::ThrdArg* arg = (UFTest::ThrdArg*) p;
  UFTest::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFTest::testing>... "<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFTest::testing> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFTest::testing> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFTest::testing> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFTest::testing> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if(c != 0 && c != car ) c->setIdle();
  }

  // exit daemon thread
  return p;
}

/// general purpose test func. 

/// arg:
int UFTest::test(UFTest::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to test whatever is currently being performed then either reset car to idle or error
  // thread should free the Parm* allocation(s)
  if( parms->_sys == "IS" ) { // test the IS level only
    UFTest::ThrdArg* arg = new UFTest::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFTest::testAll, arg);
    return (int) thrid;
  }
  if( parms->_sys == "CC" ) { // test CC only
    UFCCTest::statAll(timeout, rec, car);
  }
  else if( parms->_sys == "DC" ) { // test DC only
    UFDCSetup::DetParm* dp = new UFDCSetup::DetParm;
    UFDCTest::mce4(dp, timeout, rec, car);
  }
  else if( parms->_sys == "EC" ) { // test EC only
    UFECSetup::EnvParm* ep = new UFECSetup::EnvParm;
    UFECTest::statAll(ep, timeout, rec, car);
  }
  else { // test all subsys
    UFCCTest::statAll(timeout, rec, car);
    UFDCSetup::DetParm* dp = new UFDCSetup::DetParm;
    UFDCTest::mce4(dp, timeout, rec, car);
    UFECSetup::EnvParm* ep = new UFECSetup::EnvParm;
    UFECTest::statAll(ep, timeout, rec, car);
  }
  return (int) pthread_self();
}

// non static, non virtual funcs:
// protected ctor helper:
void UFTest::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".subsys"; // subsystem name or all/full system
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate test action mark
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform system test (could just execut system pulse to verify aliveness)
int UFTest::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFTest::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  UFTest::Parm* parm= 0;
  string pvname = _name + ".subsys";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    parm = new UFTest::Parm(input);
    clog<<"UFTest::genExec> subsys: "<<input<<endl;
    pva->clearVal(); // clear
  }
  if( parm == 0 ) parm = new UFTest::Parm;
  int stat = test(parm, timeout, this, _car);  // perform requested test

  if( stat < 0 ) { 
    clog<<"UFTest::genExec> failed to start test pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFTest::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFTEST_CC__
