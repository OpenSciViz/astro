#if !defined(__UFApply_h__)
#define __UFAPPLY_h__ "$Name:  $ $Id: UFApply.h,v 0.17 2005/10/26 19:13:49 hon Exp $"
#define __UFAPPLY_H__(arg) const char arg##Apply_h__rcsId[] = __UFPAPPLY_h__;

#include "UFGem.h"
#include "UFCAR.h"

class UFApply : public UFGem {
public:
  inline UFApply(const string& instrum, UFCAR* car= 0) : UFGem(instrum+":apply") { _create(car); }
  inline UFApply(const string& instrum,
		 const string& applyname,  UFCAR* car= 0) : UFGem(instrum+":"+applyname) { _create(car); }
  inline virtual ~UFApply() {}

  inline virtual UFCAR* hasCAR() { return _car; }

  // override to process all linked records
  virtual int directive(int d);

  // always marked?
  inline virtual void mark() { _mark = true; }
  inline virtual void unmark() {}

  // note that genExec assumes setBusy(timeout) is called prior
  virtual int genExec(int timeout= 10);

  // use UFGem base class start:
  //inline virtual int start(int timeout= 10) { return UFGem::start(timeout); }

  // force car to process too?
  virtual void setBusy(size_t timeout= 10);
  virtual void setIdle();

  inline int setTO(int timeout=10)
    { int oldto = _timeout; _timeout = timeout; if( _car != 0 ) _car->setTO(timeout); return oldto; }

  // db should have only one top-level system apply rec:
  inline static UFApply* sysApply() { return _sys; }

  // priority helper func.
  int priorityUnMark(const string& recname);

protected:
  static UFApply* _sys;
  UFCAR* _car;  // apply (and cad) should have a car
  // ctor helper
  void _create(UFCAR* car= 0);
};

#endif // __UFAPPLY_h__
