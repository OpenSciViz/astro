#if !defined(__UFDCTEST_h__)
#define __UFDCTEST_h__ "$Name:  $ $Id: UFDCTest.h,v 0.7 2006/08/29 20:16:48 hon Exp $"
#define __UFDCTEST_H__(arg) const char arg##DCTest_h__rcsId[] = __UFDCTEST_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

class UFDCTest : public UFCAD {
public:
  inline UFDCTest(const string& instrum= "instrum") : UFCAD(instrum+":dc:test") { _create(); }
  inline virtual ~UFDCTest() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  // thread that performs portescap agent connection test
  static void* thrdFunc(void* p);

  // start connection thread
  static int newThrd(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  inline static int mce4(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0)
    { return newThrd(parms, timeout, rec, car); }

protected:
  // ctor helper
  void _create();
};

#endif//  __UFDCTEST_h__
