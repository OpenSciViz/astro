#if !defined(__UFEndVerify_CC__)
#define __UFEndVerify_CC__ "$Name:  $ $Id: UFEndVerify.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFEndVerify_CC__;

#include "UFEndVerify.h"
#include "UFCAR.h"
//#include "UFCCEndVerify.h"
//#include "UFDCEndVerify.h"
//#include "UFECEndVerify.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFEndVerify::endAll(void* p) {
  // end really just needs the mech. names
  UFEndVerify::ThrdArg* arg = (UFEndVerify::ThrdArg*) p;
  UFEndVerify::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFEndVerify::ending>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFEndVerify::ending> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFEndVerify::ending> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFEndVerify::ending> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFEndVerify::ending> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose end func. 

/// arg: mechName, motparm*
int UFEndVerify::end(UFEndVerify::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to end whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFEndVerify::ThrdArg* arg = new UFEndVerify::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFEndVerify::endAll, arg);
    return (int) thrid;
  }

  // UFCCEndVerify::endAll(timeout, rec, car);
  //UFDCEndVerify::endAll(timeout, rec, car);
  //UFECEndVerify::endAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFEndVerify::_create() {
  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate end action 
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform end mechanisms with non-null step cnts or named-position inputs
int UFEndVerify::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFEndVerify::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFEndVerify::genExec> mode: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFEndVerify::Parm* p = new UFEndVerify::Parm;
  int stat = end(p, timeout, this, _car);  // perform requested end

  if( stat < 0 ) { 
    clog<<"UFEndVerify::genExec> failed to start end pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFEndVerify::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFEndVerify_CC__
