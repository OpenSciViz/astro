#if !defined(__UFECInit_h__)
#define __UFECInit_h__ "$Name:  $ $Id: UFECInit.h,v 0.4 2004/06/22 14:46:55 hon beta $"
#define __UFECInit_H__(arg) const char arg##ECInit_h__rcsId[] = __UFECInit_h__;

#include "UFCAD.h"

// init the _connection socket here
#include "UFECSetup.h"

class UFECInit : public UFCAD {
public:
  inline UFECInit(const string& instrum= "instrum") : UFCAD(instrum+":ec:init") { _create(); }
  inline virtual ~UFECInit() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  // thread that performs portescap agent connection
  static void* connection(void* p);

  // start connection thread
  static int connect(UFECSetup::EnvParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif//  __UFECInit_h__
