#if !defined(__UFDCAbort_h__)
#define __UFDCAbort_h__ "$Name:  $ $Id: UFDCAbort.h,v 0.3 2003/11/13 21:11:41 hon beta $"
#define __UFDCAbort_H__(arg) const char arg##DCAbort_h__rcsId[] = __UFDCAbort_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

class UFDCAbort : public UFCAD {
public:
  inline UFDCAbort(const string& instrum= "instrum") : UFCAD(instrum+":dc:abort") { _create(); }
  inline virtual ~UFDCAbort() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* abortion(void* p);
  static int abort(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  // available to IS
  inline static int abortAll(int timeout, UFGem* rec= 0, UFCAR* car= 0) { return 0; }

protected:
  // ctor helper
  void _create();
};

#endif // __UFDCAbort_h__
