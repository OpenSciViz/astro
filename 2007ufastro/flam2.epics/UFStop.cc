#if !defined(__UFSTOP_C__)
#define __UFSTOP_C__ "$Name:  $ $Id: UFStop.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSTOP_C__;

#include "UFStop.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFStop::stopAll(void* p) {
  // stop really just needs ?
  UFStop::ThrdArg* arg = (UFStop::ThrdArg*) p;
  UFStop::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFStop::stopping>... "<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFStop::stopAll> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFStop::stopAll> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFStop::stopAll> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFStop::stopAll> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if(c != 0 && c != car ) c->setIdle();
  }

  // exit daemon thread
  return p;
}

/// general purpose stop func. 

/// arg: 
int UFStop::stop(UFStop::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to stop whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  UFStop::ThrdArg* arg = new UFStop::ThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFStop::stopAll, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFStop::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".DataLabel";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate stop action
  attachInputPV(pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform system stop 
int UFStop::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFStop::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    clog<<"UFStop::genExec> datalabel: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFStop::Parm* p = new UFStop::Parm;
  int stat = stop(p, timeout, this, _car);  // perform requested stop

  if( stat < 0 ) { 
    clog<<"UFStop::genExec> failed to start stop pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFStop::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFSTOP_C__
