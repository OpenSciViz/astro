import java.io.*;
import java.awt.*;

public class flamdblist{

public static void main(String [] args) {

String dblst= "sh -c 'ufflamisd -l -vv 2>&1' | grep pvname | awk '{print $4}' | sort -u";
Process p= null;
BufferedReader in= null;
try {
  p = Runtime.getRuntime().exec(dblst);
}
catch(IOException e) {
  System.err.println(e);
  p = null;
}
if( p != null ) {
  in = new BufferedReader(new InputStreamReader(p.getInputStream()));
}
String s = null;
TextArea ta = new TextArea(1, 20);
try {
  do {
    s = in.readLine();
    if( s != null ) {
      System.out.println(s);
      if( ta != null )
	ta.append(s);
    }
  } while(s != null);
}
catch(IOException e) {
  System.err.println(e);
}

}
}
