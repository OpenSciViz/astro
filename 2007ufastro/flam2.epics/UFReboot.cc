#if !defined(__UFABORT_CC__)
#define __UFREBOOT_CC__ "$Name:  $ $Id: UFReboot.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFREBOOT_CC__;

#include "UFReboot.h"
#include "UFPVAttr.h"
#include "UFRuntime.h"
#include "UFClientSocket.h"
#include "UFCAwrap.h"

UFClientSocket* UFReboot::_inetd= 0;
UFClientSocket* UFReboot::_execd= 0;
UFCAwrap* UFReboot::_ufca= 0;
string UFReboot::_oiwfschan;

// ctors:
// (inlined)

// static class funcs:
void* UFReboot::shutdown(void* p) {
  // shutdown via (x)inted...
  UFReboot::ThrdArg* arg = (UFReboot::ThrdArg*) p;
  UFReboot::Parm parm = arg->_parm;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  clog<<"UFReboot::shutdown> ... "<<endl;
  clog<<"UFReboot::shutdown> host: "<<parm._host<<", ports: "
      <<parm._inetport<<", "<<parm._execport<<endl;

  delete arg; // since all info. of use has been extracted

  // simple simulation... set car idle after 1/2 sec:
  if( _sim ) { 
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != car ) c->setIdle();
    }
    // exit daemon thread
    return p;
  }

  if( UFReboot::_inetd == 0 )
    UFReboot::_inetd = new UFClientSocket();

  // non simulation -- do not assume this is running on the same host
  // as the executive agent, so we cannot simply send it a terminate sig.
  // use the (x) inet boot method for max. portability
  string clname = "UFFLAMShutDown@" + parm._host;
  clog<<"UFReboot::shutdown> connect to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
  int socfd =_inetd->connect(parm._host, parm._inetport);
  if( socfd < 0 ) {
    clog<<"UFReboot::shutdown> Failed connection to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
    delete arg;
    return p;
  }
  else if( ! _inetd->validConnection() ) {
    clog<<"UFReboot::shutdown> Invalid connection to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
    return p;
  }
  clog<<"UFReboot::shutdown> send greeting/request: "<<clname<<endl;
  int ns = UFReboot::_inetd->send(clname); // send shutdown request
  if( ns <= 0 ) {
    clog<<"UFReboot::shutdown> failed to send request. "<<clname<<endl;
    return p;
  }
  else {
    clog<<"UFReboot::shutdown> sent request, ns= "<<ns<<", "<<clname<<endl;
    int nr = UFReboot::_inetd->recv(clname);
    clog<<"UFReboot::shutdown> recvd reply, nr= "<<nr<<", "<<clname<<endl;
  }
  // inetd will close its side of the socket, so we should too...
  _inetd->close();

  // indicate db (self) shutdown
  UFGem::_shutdown = true;

  return p; // exit daemon thread
} // shutdown

void* UFReboot::reboot(void* p) {
  // full mode boot via (x)inted....
  // any other type of partial boot should use f2 executive
  UFReboot::ThrdArg* arg = (UFReboot::ThrdArg*) p;
  UFReboot::Parm parm = arg->_parm;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  clog<<"UFReboot::reboot> ... "<<endl;
  clog<<"UFReboot::reboot> host: "<<parm._host<<", ports: "
      <<parm._inetport<<", "<<parm._execport<<endl;

  delete arg; // since all info. of use has been extracted

  // simple simulation... set car idle after 1/2 sec:
  if( _sim ) { 
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != car ) c->setIdle();
    }
    // exit daemon thread
    return p;
  }

  if( UFReboot::_inetd == 0 )
    UFReboot::_inetd = new UFClientSocket();

  // non simulation -- do not assume this is running on the same host
  // as the executive agent, so we cannot simply send it a terminate sig.
  // use the (x) inet boot method for max. portability
  string clname = "UFFLAMReBoot@" + parm._host;
  if( parm._mode.find("sim") != string::npos || 
      parm._mode.find("Sim") != string::npos || 
      parm._mode.find("SIM") != string::npos ) {
    clname =  "UFFLAMReBootSim@" + parm._host;
  }

  clog<<"UFReboot::reboot> connect to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
  int socfd = _inetd->connect(parm._host, parm._inetport);
  if( socfd < 0 ) {
    clog<<"UFReboot::reboot> Failed connection to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
    return p;
  }
  else if( ! _inetd->validConnection() ) {
    clog<<"UFReboot::reboot> Invalid connection to (x)inetd on: "<<parm._host<<", port: "<< parm._inetport<<endl;
    return p;
  }
 
  clog<<"UFReboot::reboot> send greeting/request: "<<clname<<endl;
  int ns = UFReboot::_inetd->send(clname); // send shutdown request
  if( ns <= 0 ) {
    clog<<"UFReboot::reboot> failed to send request. "<<clname<<endl;
    return p;
  }
  else {
    clog<<"UFReboot::reboot> sent request, ns= "<<ns<<", "<<clname<<endl;
    int nr = UFReboot::_inetd->recv(clname);
    clog<<"UFReboot::reboot> recvd reply, nr= "<<nr<<", "<<clname<<endl;
  }
  // inetd will close its side of the socket, so we should too...
  _inetd->close();

  // indicate db (self) shutdown
  UFGem::_shutdown = true;

  return p; // exit daemon thread
}

/// general purpose reboot func. 

/// arg: 
int UFReboot::haltstart(UFReboot::Parm* p, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // mark and start/process the oiwfs reboot cad..
  if( _ufca == 0 ) {
    _ufca = new UFCAwrap;
  }
  clog<<"UFReboot::haltstart> subsys: "<<p->_subsys<<", mode: "<<p->_mode<<endl;
  string mark= "0", start= "3";
  string chmark= _oiwfschan + ".MARK", chdir= _oiwfschan + ".DIR";
  chid ch = _ufca->put(&chmark, &mark);
  if( ch == 0 )
    clog<<"UFReboot::haltstart> unable to connect to OIWFS EpicsDB and mark reboot"<<endl;
  else
    clog<<"UFReboot::haltstart> connected to OIWFS EpicsDB and marked reboot"<<endl;

  ch = _ufca->put(&chdir, &start);
  if( ch == 0 )
    clog<<"UFReboot::haltstart> unable to connect to OIWFS EpicsDB and start reboot"<<endl;
  else
    clog<<"UFReboot::haltstart> connected to OIWFS EpicsDB and started reboot"<<endl;

  // create and run pthreads to reboot whatever daemons and device agents are currently running
  // thread should free the Parm* allocations...
  //
  if( p->_mode.find("halt") != string::npos ||
      p->_mode.find("Halt") != string::npos ||
      p->_mode.find("HALT") != string::npos ||
      p->_mode.find("shutdown") != string::npos || p->_mode.find("Shutdown") != string::npos ||
      p->_mode.find("ShutDown") != string::npos || p->_mode.find("SHUTDOWN") != string::npos ) {
    UFReboot::ThrdArg* argshutdown = new UFReboot::ThrdArg(*p, car);
    pthread_t shutdownthrd = UFPosixRuntime::newThread(UFReboot::shutdown, argshutdown);
    clog<<"UFReboot::haltstart> shutdown thread executing, thrdId: "<<shutdownthrd<<endl;
    // free parms here
    delete p;
    return (int) shutdownthrd;
  }
  // or reboot
  UFReboot::ThrdArg* argboot = new UFReboot::ThrdArg(*p, car);
  pthread_t rebootthrd = UFPosixRuntime::newThread(UFReboot::reboot, argboot);
  clog<<"UFReboot::haltstart> reboot thread executing, thrdId: "<<rebootthrd<<endl;

  // free parms here
  delete p;

  return (int) rebootthrd;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFReboot::_create() {
  // chanid of wfs reboot cad:
  _oiwfschan = "f2:wfs:reboot";

  // client socket allocations
  _inetd = new UFClientSocket; // unconnected
  //_execd = new UFClientSocket; // unconnected

  UFPVAttr* pva= 0;
  string pvname = _name + ".subsys"; // all, oiwfs, f2cc, f2dc, f2ec only, etc.
  pva = (*this)[pvname] = new UFPVAttr(pvname, "full"); 
  attachInOutPV(this, pva);

  pvname = _name + ".mode";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "reboot"); 
  attachInOutPV(this, pva);

  string host= UFRuntime::hostname();
  pvname = _name + ".F2host"; // nameserver or IP address
  pva = (*this)[pvname] = new UFPVAttr(pvname, host); 
  attachInOutPV(this, pva);

  string f2oiwfs= "f2:wfs:reboot"; // .MARK then .DIR;
  pvname = _name + ".F2OIWFS"; // reboot channel
  pva = (*this)[pvname] = new UFPVAttr(pvname, f2oiwfs);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform reboot 
int UFReboot::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFReboot::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string input, pvname;
  string host = UFRuntime::hostname();
  pvname = _name + ".F2host";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      host = input;
      clog<<"UFReboot::genExec> F2host: "<<host<<endl;
      //pva->clearVal(); // clear
    }
  }

  string subsys = "full";
  pvname = _name + ".subsys";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      subsys = input;
      clog<<"UFReboot::genExec> subsys: "<<subsys<<endl;
      pva->clearVal(); // clear
    }
  }

  string mode = "reboot"; // "shutdown";
  pvname = _name + ".mode";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      mode = input;
      clog<<"UFReboot::genExec> mode: "<<mode<<endl;
      pva->clearVal(); // clear
    }
  }

  UFReboot::Parm* p = new UFReboot::Parm(host, subsys, mode);
  int stat = haltstart(p, timeout, this, _car);  // perform requested reboot

  if( stat < 0 ) { 
    clog<<"UFReboot::genExec> failed to start test pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFReboot::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFREBOOT_CC__
