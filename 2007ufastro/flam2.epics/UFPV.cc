#if !defined(__UFPV_CC__)
#define __UFPV_CC__ "$Name:  $ $Id: UFPV.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPV_CC__;

#include "UFRuntime.h" // system & posix conv. funcs.
#include "UFPV.h"
#include "UFCAServ.h"
#include "UFGem.h"
#include "UFCAR.h"
#include "UFSIR.h"

// These static members must be re-declared in this file.
bool UFPV::_verbose= false;
bool UFPV::_vverbose= false;
int UFPV::_currentOps;

/* copy ctor in use somewhere somehow?
UFPV::UFPV() : casPV() {
  clog<<"UFPV:: default ctor invoked when UFCAServ::_instance== "<<(int)UFCAServ::_instance<<endl;
}

UFPV::UFPV(const UFPV& rhs) : casPV() {
  if( UFCAServ::_instance == 0 ) {
    clog<<"UFPV:: copy ctor invoked when UFCAServ::_instance == 0!"<<endl;
  }
  // *this = rhs; // assignment op is private! should be protected
  _attr = rhs._attr;  /// see UFPVAttr
  _interest = rhs._interest; /// monitor trigger 
}
*/

UFPV::UFPV(const caServer &cas,
	   const UFPVAttr &attr,
	   aitBool interest) : casPV((caServer&)cas), _attr(attr), _interest(interest) {
  gdd *pValue = _attr.getVal(); // Get pointer to gdd object.
  if( !pValue )
    return;

  double& value = ((UFPVAttr &)_attr).getNumeric();
  aitString name = getName();
  string pvname;
  const char* cname = name.string();
  if( cname != 0 ) pvname = cname; 
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString sval; pValue->getConvert(sval);
    string sv;
    const char* cs = sval.string();
    if( cs != 0 ) sv = cs;
    if( _vverbose )
      clog<<"UFPV::> char* val: "<<sv<<", pvname: "<<pvname<<endl;
    //if( !isnanf(_attr._numeric) ) 
    //if( _attr._numeric < MAXFLOAT ) 
    if( value < INT_MAX )
      if( _vverbose )
        clog<<"UFPV::> _numeric: "<<value<<", pvname: "<<pvname<<", string input: "<<sv<<endl;
    else
      return;
  }
  else {
    pValue->getConvert(value);
  }
  
  if( value >= _attr.getHighAlarm() ) {
    pValue->setStat(epicsAlarmHigh);
    pValue->setSevr(epicsSevMinor);
  }
  else if (value <= _attr.getLowAlarm() ) {
    pValue->setStat(epicsAlarmLow);
    pValue->setSevr(epicsSevMinor);
  }
  else {
    pValue->setStat(epicsAlarmNone);
    pValue->setSevr(epicsSevNone);
  }

  if( primtyp == aitEnumInt32 ) {
    if( _vverbose )
      clog<<"UFPV::> int val: "<<value<<", pvname: "<<pvname<<endl;
    return;
  }
  if( _vverbose )
    clog<<"UFPV::> double val: "<<value<<", pvname: "<<pvname<<endl; 
}

caStatus UFPV::beginTransaction() {
  // Trivial implementation that informs the user of the number of
  // current IO operations in progress for the server tool. currentOps
  //  is a static member.
  _currentOps++;
    if( _vverbose )
      clog << "Number of current operations = " << _currentOps << "\n"; 
  return S_casApp_success;
}

void UFPV::endTransaction() {
  _currentOps--;
}

caStatus UFPV::read(const casCtx& ctx, gdd& prototype) {
  // Calls UFCAServ::read() which calls the appropriate function
  // from the application table.
  return UFCAServ::read(*this, prototype);
}

gddAppFuncTableStatus UFPV::readStatus(gdd& val) {
  gdd *pValue = _attr.getVal();
  if( pValue )
    val.putConvert(pValue->getStat());
  else
    val.putConvert(epicsAlarmUDF);

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readSeverity(gdd& val) {
  gdd *pValue = _attr.getVal();
  if( pValue )
    val.putConvert(pValue->getSevr());
  else
    val.putConvert(epicsSevNone);

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readPrecision(gdd& val) {
  val.putConvert(_attr.getPrec());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighOperation(gdd& val) {
  val.putConvert(_attr.getHighOperation());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowOperation(gdd& val) {
  val.putConvert(_attr.getLowOperation());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighAlarm(gdd& val) {
  val.putConvert(_attr.getHighAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighWarn(gdd& val) {
  val.putConvert(_attr.getHighWarn());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowWarn(gdd& val) {
  val.putConvert(_attr.getLowWarn());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowAlarm(gdd& val) {
  val.putConvert(_attr.getLowAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighCtrl(gdd& val) {
  val.putConvert(_attr.getHighCtrl());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowCtrl(gdd& val) {
  val.putConvert(_attr.getLowCtrl());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readVal(gdd& val) {
  // If UFPVAttr::_pVal exists, then use the gdd::get() function to 
  // assign the current value of pValue to currentVal; then use the
  // gdd::putConvert() to write the value into value.
  aitString name = getName();
  const char* pvname = name.string();
  //if( _verbose )
   // clog<<"UFPV::readVal> pvname: "<<pvname<<endl;
  gdd *pValue = _attr.getVal();
  if( !pValue )
    return S_casApp_undefined;

  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString curval;
    pValue->getConvert(curval);
    val.putConvert(curval);
    if( _verbose )
      clog<<"UFPV::readVal> pvname: "<<pvname<<", (string) val: "<<curval.string()<<endl;
  }
  else if( primtyp == aitEnumInt32 || primtyp == aitEnumInt16 ) {
    int curval= 0;
    pValue->getConvert(curval);
    val.putConvert(curval);
    if( _verbose )
      clog<<"UFPV::readVal> pvname: "<<pvname<<", (int32) val: "<<curval<<endl;
  }
  else {
    double curval;
    pValue->getConvert(curval);
    val.putConvert(curval);
    if( _verbose )
      clog<<"UFPV::readVal> pvname: "<<pvname<<", (double) val: "<<curval<<endl;
  }

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readUnits(gdd& val) {
  val.put(_attr.getUnits());
  return S_casApp_success;
}

// bestExternalType() is a virtual function that can redefined to
// return the best type with which to access the PV. Called by the
// server library to respond to client request for the best type.
aitEnum UFPV::bestExternalType() {
  gdd* pValue = _attr.getVal();
  if( !pValue )
    return aitEnumInvalid;

  return pValue->primitiveType();
}

// static write func. available to any part of the application:
caStatus UFPV::write(const gdd& val, UFPV& pv, UFCAServ* cas) {
  aitString name = pv.getName();
  const char* pvname = name.string();
  if( _verbose )
    clog<<"UFPV::write> pvname: "<<pvname<<endl;
  UFPVAttr& pva = (UFPVAttr&) pv.getAttr();
  gdd* pValue = pva.getVal(); // return ptr by ref?
  if( !pValue ) {
    clog<<"UFPV::write> PV gdd val not available?"<<endl;
    return S_casApp_noSupport;
  }
  
  UFGem* direct = pva.isDirectiveInput();
  if( direct != 0 )
    clog<<"UFPV::write> directive: "<<pvname<<endl;

  UFGem* rec = pva.getDBRecInput();
 
  // according to example: If pValue exists, unreference it, set the pointer to the new gdd
  // object, and reference it. (according to example, yet this seems to cause runtime error?)
  /* seems to me this would only work properly if gdd* were returned by ref.
  if( pValue )
    pValue->unreference();

  pValue = (gdd*) &val;
  pValue->reference();
  */

  // Set the timespec structure to the current time stamp the gdd.
  //struct timespec t;
  epicsTimeStamp ts = epicsTime();
  pValue->setTimeStamp(&ts);

  // use double _numeric attribut by reference:
  double& value = pva.getNumeric();
  int valasint= INT_MIN;
  aitString valasstr;
  val.getConvert(valasstr); 
  const char* ccs = valasstr.string(); 
  const size_t slen = 1 + strlen(ccs);
  char cs[slen]; memset(cs, 0, slen);
  strcpy(cs, ccs);
  UFRuntime::rmJunk(cs);
  //clog<<"UFPV::write> rmJunk returns: "<<cs<<endl;
  // Get the new value and set the severity and status according
  // to its value.
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    pValue->putConvert(valasstr); // accept all strings
    errno = 0;
    double dval = atof(cs);
    // restrict allowed text-to-numeric options:
    //if( cs[0] != '+' && cs[0] != '-' && cs[0] != '.' && !isdigit(cs[0])  )
    // errno = EINVAL;
    if( errno == 0 ) {
      value = dval; 
      valasint = (int) dval; 
    }
    else {
      value = INT_MIN;
    }
    if( rec ) rec->validate(&pva); // validate after setting new value, if it is an input field
    if( _vverbose )
      clog<<"UFPV::write> "<<pvname<<", (string) new value: "<<valasstr.string()<<endl;
  }
  else if( primtyp == aitEnumInt32 || primtyp == aitEnumInt16 ) {
    errno = 0;
    double dval = atof(cs); // has a string been put rather than an int?
    // restrict allowed text-to-numeric options:
    //if( cs[0] != '+' && cs[0] != '-' && cs[0] != '.' && !isdigit(cs[0])  )
    //  errno = EINVAL;
    if( errno == 0 ) {
      val.getConvert(valasint);
      pValue->putConvert(valasint); // since ptr was not reset above, use put
      value = (double) valasint; // reset value
    }
    else {
      value = INT_MIN;
      pva.clearVal(false); // avoid infinite recursion -- inidcate clear need not call write!
    }
    if( rec && valasint != INT_MIN ) rec->validate(&pva); // validate after setting new value, if it is an input field
    if( _vverbose )
      clog<<"UFPV::write> "<<pvname<<", (int) new value: "<<valasint<<", dval: "<<dval<<endl;
  }
  else if( primtyp == aitEnumFloat32 || primtyp == aitEnumFloat64 ) {
    errno = 0;
    double dval = atof(cs); // has a string been put rather than a float/double?
    // restrict allowed text-to-numeric options:
    //if( cs[0] != '+' && cs[0] != '-' && cs[0] != '.' && !isdigit(cs[0])  )
    // errno = EINVAL;
    if( errno == 0 ) {
      val.getConvert(value);
      pValue->putConvert(value); // since ptr was not reset above, use put
      valasint = (int)value;
    }
    else {
      value = INT_MIN;
      pva.clearVal(false); // avoid infinite recursion -- inidcate clear need not call write!
    }
    if( _vverbose )
      clog<<"UFPV::write> "<<pvname<<", (double) new value: "<<value<<", dval: "<<dval<<endl;
    if( rec && valasint != INT_MIN ) rec->validate(&pva); // validate after setting new value, if it is an input field
  }
  else { // just use int for all other enums?
    errno = 0;
    double dval = atof(cs); // has a string been put rather than an int?
    // restrict allowed text-to-numeric options:
    //if( cs[0] != '+' && cs[0] != '-' && cs[0] != '.' && !isdigit(cs[0])  )
    //  errno = EINVAL;
    if( errno == 0 ) {
      val.getConvert(valasint);
      pValue->putConvert(valasint); // since ptr was not reset above, use put
      value = (double) valasint; // reset value
    }
    else {
      value = INT_MIN;
      pva.clearVal(false); // avoid infinite recursion -- inidcate clear need not call write!
    }
    if( _vverbose )
      clog<<"UFPV::write> "<<pvname<<", (enum?) new value: "<<valasint<<", dval: "<<dval<<endl;
    if( rec && valasint != INT_MIN ) rec->validate(&pva); // validate after setting new value, if it is an input field
  }
    
  if( direct ) { // directives are ints/enums
    clog<<"UFPV::write> directive: "<<pvname<<", val: "<<valasint<<endl;
    if( valasint >= 0 )
      direct->directive(valasint); 
  } // directive
  else {
    // check limits?
    if( primtyp == aitEnumFloat32 || primtyp == aitEnumFloat64 ||
        primtyp == aitEnumInt32 || primtyp == aitEnumInt16 ||
        primtyp == aitEnumUint32 || primtyp == aitEnumUint16 ) {
      if( value > 1.1 * pva.getHighAlarm() ) {
         pValue->setStat(epicsAlarmHiHi);
         pValue->setSevr(epicsSevMajor);
      }
      else if( value >= pva.getHighAlarm() ) {
        pValue->setStat(epicsAlarmHigh);
        pValue->setSevr(epicsSevMinor);
      }
      else if( value <= pva.getLowAlarm() ) {
        pValue->setStat(epicsAlarmLow);
        pValue->setSevr(epicsSevMinor);
      }
      else if( value < 0.9 * pva.getLowAlarm() ) {
        pValue->setStat(epicsAlarmLoLo);
        pValue->setSevr(epicsSevMajor);
      }
      else {
        pValue->setStat(epicsAlarmNone);
        pValue->setSevr(epicsSevNone);
      }
    }
    // this pv may be associated with a CAD, if it is an input pv of a CAD
    // mark the container/record (CADs and Applys only)
    // -1 is returned for output field writes (output writes should not mark record)
    // 0 for nulled or ambiguous inputs...
    // if( _verbose ) 
    // mark returns = rectype if this is a (valid, non-cleared) input
    int m = pva.mark();
    if( m == UFGem::_CAD )
      clog<<"UFPV::write> marked record for CAD input field: "<<pva.name()<<endl;

    // in the special case that this is a SAD or CAR input field,
    // force SIR or CAR record processing if not cleared...
    UFGem* car = pva.isCARInput();
    if( car ) {
      if( m <= 0 ) car = 0;
      //if( _verbose ) clog<<"UFPV::write> pvamark: "<<m<<", CAR: "<<pva.name()<<endl;
    }
    UFGem* sad = pva.isSADInput();
    if( sad ) {
      if( m <= 0 ) sad = 0;
      //if( _verbose ) clog<<"UFPV::write> pvamark: "<<m<<",SAD: "<<pva.name()<<endl;
    }

    if( car ) // always process car (non directive) input (car always marked)
      car->start(); // default timeout

    if( sad ) // always process sir (non directive) input (sar always marked)
      sad->start(0); // 0 timeout
  } // non-directive

  // always post event?
  //if( pv.interest() ) {
    if( cas == 0 )
      cas = UFPVAttr::_cas;
    if( cas != 0 ) {
      casEventMask select( cas->valueEventMask() | cas->alarmEventMask() );
      pv.postEvent(select, *pValue);
    }
  // }

  return S_casApp_success;
}

// static sig write func. available to any part of the application:
caStatus UFPV::write(const gdd& val, UFPVAttr& pva, UFCAServ* cas) {
  if( cas == 0 && pva._cas == 0 )
    return 0;

  if( cas == 0 )
    cas = pva._cas;

  // use unique ctor for pv
  string pvname = pva.name();
  UFPV* pv = UFCAServ::createPV(pvname, cas);
  if( pv == 0 )
    return 0;

  return write(val, *pv, cas);
}

// this is passed a context and a new value the the cas runtime:
caStatus UFPV::write(const casCtx& ctx, const gdd& val) {
  caServer* pServer = getCAS();

  if( !pServer ) {
    clog<<"UFPV::write> CAS service not available?"<<endl;
    return S_casApp_noSupport;
  }

  UFCAServ* cas = dynamic_cast< UFCAServ* > (pServer);
  if( cas != 0 && cas != UFPVAttr::_cas ) UFPVAttr::_cas = cas;

  aitString name = getName();
  //if( _verbose )
  //  clog<<"UFPV::write> name: "<<name.string()<<endl;

  // Doesn't support writing to arrays or container objects
  // (gddAtomic or gddContainer).
  if( ! val.isScalar() ) {
    clog<<"UFPV::write> non scalar PV currently not supported..."<<endl;
    return S_casApp_noSupport;
  }

  // call static func. (which can presumable be used elsewhere in app.)
  return write(val, *this, cas);
}

#endif // __UFPV_CC__
