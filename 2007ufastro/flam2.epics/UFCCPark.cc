#if !defined(__UFCCPark_CC__)
#define __UFCCPark_CC__ "$Name:  $ $Id: UFCCPark.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCPark_CC__;

#include "UFCCPark.h"
#include "UFCCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

#include "ufflam2mech.h"

// ctors:

// static class funcs:
void* UFCCPark::seekit(void* p) {
  // seek specified indexor in 0 or 1 direction, at specified velocity
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:parkC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed)
  bool validsoc = connected(UFCCSetup::_motoragntsoc);
  if( !_sim && !validsoc ) {
    clog<<"UFCCPark::seekit> not connected to motor indexor agent, need to (re)init..."<<endl;
    // and free the motparms
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      UFCCSetup::MotParm* mp = mit->second;
      delete mp;
    }
    // free arg
    delete arg;
    return p;
  }

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  // req. bundle is pairs of strings:
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    string mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; // _posit should be "park" default posname
    if( mp == 0 )
      continue;
    if( _verbose )
      clog<<"UFCCPark::seekit> "<<mname<<" indexor: "<<mp->_indexor<<", vel: "<<mp->_vel<<endl;

    cmdvec.clear();
    // park indexor 'I', and indicate suggested error message,
    // followed by full syntax command and parameter:
    // if agent should set car on completion or error, indicate car channel (pvname):
    string cmd = mp->_indexor; cmd += "CAR";
    string cmdpar = carname;
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    if( _verbose )
      clog<<"UFCCPark::seekit> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "SAD";
    cmdpar = UFCCSetup::sadChan(mname);
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
      clog<<"UFCCPark::seekit> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    // use ccsetup helper function here to decompose
    // into a near/not-home N+-cnt followed by
    // a raw +-bcnt step (backlash) request
    int nc = UFCCSetup::backlashSeq(*mp, cmdvec);

    // current version of agent can't handle entire  bundle for multiple indexors,
    // need to send bundle for earch indexor, and be sure that indexor name is 
    // at position 0 of each car:
    clog<<"UFCCPark::seekit> cmdque cnt: "<<nc<<endl;
    if( nc <= 0 )
      break;

    if( !_sim && validsoc ) {
      UFStrings req(name, cmdvec);
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCPark::seekit> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      if( _verbose )
        clog<<"UFCCPark::seekit> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
      //int nr = UFCCSetup::_motoragntsoc->recv(req);
    }
    else if( !validsoc ) {
      clog<<"UFCCPark::seekit> unable to transact request with portescap motor agent due to invalid connection. need to (re) init..."<<endl;
      err = 1; errmsg = "Error Parking";
    }
  }

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }
  // free arg
  delete arg;
  
  if( _sim || err != 0 ) {
    clog<<"UFCCPark::seekit> error or sim..."<<endl;
    // set car idle after 1/2 sec:
    if( _sim ) UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && err == 0 ) 
	c->setIdle();
      else if( c != 0 && c != car && err != 0 ) {
        c->setErr(errmsg, err);
      }
    }
  }

  // exit daemon thread
  return p;
} // seekit

/// general purpose park func.
/// arg: mechName, motparm*
int UFCCPark::seek(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCPark::seekit, arg);

  return (int) thrid;
} // seek

/// special purpose park all func.
/// arg: mechName, motparm*
int UFCCPark::seekAll(int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  std::map< string, UFCCSetup::MotParm* >* mparms = new std::map< string, UFCCSetup::MotParm* >; // seek these

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCPark::seekit, arg);

  return (int) thrid;
} // seekAll

// non static, non virtual funcs:
// protected ctor helper:
void UFCCPark::_create() {
  /// pvs should look something like:
  /// flam:cc:park.mech1Vel == park velocity
  /// flam:cc:park.mech1Direction == park direction
  /// ... thru mechNvel
  int mcnt = UFCCSetup::setDefaults(_name); 
  deque< string >* mnames = UFCCSetup::getMechNames();
  UFPVAttr* pva= 0;
  for( int i = 0; i < mcnt; ++i ) {
    string pvname = _name + "."; pvname += (*mnames)[i];
    string pvpar = pvname;
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, (int)0); // mechanism
    attachInOutPV(this, pva);

    pvpar = pvname + "Vel";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, (int)0); // park vel
    attachInOutPV(this, pva);

    pvpar = pvname + "Direction"; // -(1) or +(0)
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, (int)0); // park direction
    attachInOutPV(this, pva);

    pvpar = pvname + "Backlash"; // +- steps
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, (int)0); // park direction
    attachInOutPV(this, pva);
  }
  // to park all mechanisms using default motion parameters:
  string pvname = _name + "."; pvname += "All";
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)-1);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
} // _create

/// virtual funcs:
/// perform park for those mechanisms with non-null inputs
int UFCCPark::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFCCPark::genExec> no pvs?"<<endl;
    return (int) pvcnt;
  }

  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >; // park these
  //std::map< string, UFPVAttr*>::const_iterator it;
  int steps, blash, vel, dir;
  string mechName, indexor;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    blash = vel = dir = 0;
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    steps = (int) parkFLAM2(mechName.c_str());
    string pvname = name() + "." + mechName;
    UFPVAttr* pva = (*this)[pvname]; // only interested in mechName, velocity, and direction values (for now)
    if( pva == 0 )
      continue;
    int input = (int)pva->getNumeric();
    if( _verbose )
      clog<<"UFCCPark::genExec> "<<pvname<<", input: "<<input<<endl;
    if( input == 0 )
      continue;

    if( pva ) { // velocity pv better not change this pv name!
      // only interested in mechName, blash, velocity, and direction values (for now)
      string bcklsh = pvname + "Backlash";
      UFPVAttr* bpva = (*this)[bcklsh];
      if( bpva != 0 ) {
	blash = (int) bpva->getNumeric();
	blash = abs(blash);
      }
      string dname = pvname + "Direction";
      UFPVAttr* dpva = (*this)[dname];
      if( dpva != 0 ) {
	dir = (int) dpva->getNumeric();
	if( dir < 0 ) dir = 1;
      }
      string vname = pvname + "Vel";
      UFPVAttr* vpva = (*this)[vname]; // only interested in mechName, velocity, and direction values (for now)
      if( vpva != 0 ) {
	vel = (int) vpva->getNumeric();
        if( vel < 0 ) // allow sign of velocity to indicate direction too
	  dir = 1;
	vel = abs(vel);
      }
      if( _verbose )
        clog<<"UFCCPark::genExec> "<<indexor<<", "<<mechName<<", blash: "<<blash<<", vel: "<<vel<<", dir: "<<dir<<endl;
      if( vel != 0 ) {
        UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, steps, blash, 0, vel, dir); // use default posname "park"
        (*mechs)[mechName] = mp; // use default motion "park"
        if( _verbose )
          clog<<"UFCCPark::genExec> mech: "<<mechName<<", indexor: "<<indexor<<", vel: "<<vel<<", dir: "<<dir<<endl;
      }
      pva->clearVal(); // clear mechName input
    }
  }

  // perform requested motion...
  int stat= 0, nm = (int) mechs->size();
  if( nm > 0 ) {
    if( _verbose )
      clog<<"UFCCPark::genExec> starting thread to park "<<nm<<" mechanism(s)..."<<endl;
    stat = seek(mechs, timeout, this, _car);
  }
  else {
    clog<<"UFCCPark::genExec> no mechanisms indicated for park? "<<endl;
    return 0;
  }

  if( stat < 0 ) { 
    clog<<"UFCCPark::genExec> failed to start park pthread..."<<endl;
    return stat;
  }

  return nm;
} // genExec

int UFCCPark::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();

  if( pvname.find("Direction") != string::npos ) {
    if( input.find("+") != string::npos || input.find("1") != string::npos ) 
      return 0; // park + ok

    if( input.find("-") != string::npos || input.find("0") != string::npos ) 
      return 0; // park - ok
  }

  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      if( input == "" )
        return 0; // clear ok
      if( input.find("null") != string::npos || input.find("Null") != string::npos || input.find("NULL") != string::npos )
        return 0; // null + ok

      char* hv = (char*)input.c_str();
      while( *hv == ' ' || *hv == '\t' )
        ++hv; // ignore white spaces

      if( isdigit(hv[0]) ) // park velocity must be alphanumric
        return 0;
    }
  }

  return -1;
} // validate


#endif // __UFCCPark_CC__
