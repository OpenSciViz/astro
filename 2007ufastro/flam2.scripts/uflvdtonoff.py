#!/usr/bin/python
import os, sys
from datetime import *
from time import sleep, time

def getLVDTVolts():
   f = os.popen('ufcaget flam:sad:LVDTVLTS');
   lvdtvlts = f.read().split('\n')[0]
   f.close();
   return lvdtvlts;

f = os.popen('ufbaytech -stat 6');
temp = f.read();
f.close();
if (temp.find('On') != -1):
   status = 'on';
   lstatus = '1';
else:
   status = 'off';
   lstatus = '0';
rep = 1;
ontime = 0;
offtime = 0;
if (len(sys.argv) == 1):
   print 'Usage: uflvdtonoff.py [-on OnTime] [-off OffTime] [-r repeat] [-h]'
   print 'OnTime and OffTime must specify unit: s (seconds), m (minutes), or h (hours)'
   print 'Example: uflvdtonoff.py -on 10m -off 30s -r 2'
   sys.exit(1)
for j in range(1, len(sys.argv)):
   if (sys.argv[j].lower() == '-on'):
      j+=1;
      temp = sys.argv[j];
      if (temp[-1].lower() == 's'):
	ontime = int(temp[:-1]);
      elif (temp[-1].lower() == 'm'):
	ontime = int(temp[:-1])*60;
      elif (temp[-1].lower() == 'h'):
	ontime = int(temp[:-1])*3600;
      else:
	print 'Usage: uflvdtonoff.py [-on OnTime] [-off OffTime] [-r repeat] [-h]'
        print 'OnTime and OffTime must specify unit: s (seconds), m (minutes), or h (hours)'
        print 'Example: uflvdtonoff.py -on 10m -off 30s -r 2'
	sys.exit
   elif (sys.argv[j].lower() == '-off'):
      j+=1;
      temp = sys.argv[j];
      if (temp[-1].lower() == 's'):
        offtime = int(temp[:-1]);
      elif (temp[-1].lower() == 'm'):
        offtime = int(temp[:-1])*60;
      elif (temp[-1].lower() == 'h'):
        offtime = int(temp[:-1])*3600;
      else:
        print 'Usage: uflvdtonoff.py [-on OnTime] [-off OffTime] [-r repeat] [-h]'
        print 'OnTime and OffTime must specify unit: s (seconds), m (minutes), or h (hours)'
        print 'Example: uflvdtonoff.py -on 10m -off 30s -r 2'
        sys.exit(1)
   elif (sys.argv[j].lower() == '-r' or sys.argv[j].lower() == '-repeat'):
      j+=1;
      rep = int(sys.argv[j]);
   elif (sys.argv[j].lower() == '-h' or sys.argv[j].lower() == '-help'):
      print 'Usage: uflvdtonoff.py [-on OnTime] [-off OffTime] [-r repeat] [-h]'
      print 'OnTime and OffTime must specify unit: s (seconds), m (minutes), or h (hours)'
      print 'Example: uflvdtonoff.py -on 10m -off 30s -r 2'
      sys.exit(1)
print "LVDT Status is: ", status;
print datetime.today(); 
lvdtvlts = getLVDTVolts(); 
logfile = 'uflvdtlog-'+str(datetime.today().date());
logs = [];
flogs = [];
logs.append('/nfs/irdoradus/share/data/environment/'+logfile);
logs.append('/usr/tmp/'+logfile);
for j in range(len(logs)):
   if (os.access(logs[j], os.F_OK)):
      flogs.append(open(logs[j], 'ab'));
   else:
      flogs.append(open(logs[j], 'wb'));
      flogs[j].write('Start Date / Time\t\tElapsed\tState\tLVDTVLTS\n');
   flogs[j].write(str(datetime.today())+'\t0\t'+lstatus+'\t'+lvdtvlts+'\n');
if (status == 'off'):
   for j in range(rep):
      print "Iteration",j
      if (ontime > 0):
	os.system('ufbaytech -on 6');
        print "LVDT is ON";
        print datetime.today();
      startTime = time();
      endTime = startTime+ontime;
      if (ontime > 0):
	lvdtvlts = getLVDTVolts(); 
	for l in range(len(flogs)):
	   flogs[l].write(str(datetime.today())+'\t'+str(ontime)+'\t1\t'+lvdtvlts+'\n');
      print "";
      while (time() < endTime):
	diff = endTime-time();
	h = int(diff/3600);
	m = int((diff-3600*h)/60);
	s = int((diff-3600*h-60*m));
	st = '%(h)02d:%(m)02d:%(s)02d' % { 'h':h, 'm':m,'s':s};
	sys.stdout.write('\rTime Remaining: '+st+'   ');
	sys.stdout.flush()
	sleep(1);
      print "";
      if (offtime > 0):
	os.system('ufbaytech -off 6');
        print "LVDT is OFF";
        print datetime.today();
      startTime = time();
      endTime = startTime+offtime;
      if (offtime > 0):
        lvdtvlts = getLVDTVolts();
        for l in range(len(flogs)):
           flogs[l].write(str(datetime.today())+'\t'+str(offtime)+'\t0\t'+lvdtvlts+'\n');
      print "";
      while (time() < endTime):
        diff = endTime-time();
        h = int(diff/3600);
        m = int((diff-3600*h)/60);
        s = int((diff-3600*h-60*m));
        st = '%(h)02d:%(m)02d:%(s)02d' % { 'h':h, 'm':m,'s':s};
        sys.stdout.write('\rTime Remaining: '+st+'   ');
        sys.stdout.flush()
        sleep(1);
      print "";
else:
   for j in range(rep):
      print "Iteration",j
      if (offtime > 0):
	os.system('ufbaytech -off 6');
        print "LVDT is OFF";
        print datetime.today();
      startTime = time();
      endTime = startTime+offtime;
      if (offtime > 0):
        lvdtvlts = getLVDTVolts();
        for l in range(len(flogs)):
           flogs[l].write(str(datetime.today())+'\t'+str(offtime)+'\t0\t'+lvdtvlts+'\n');
      print "";
      while (time() < endTime):
        diff = endTime-time();
        h = int(diff/3600);
        m = int((diff-3600*h)/60);
        s = int((diff-3600*h-60*m));
        st = '%(h)02d:%(m)02d:%(s)02d' % { 'h':h, 'm':m,'s':s};
        sys.stdout.write('\rTime Remaining: '+st+'   ');
        sys.stdout.flush()
        sleep(1);
      print "";
      if (ontime > 0):
	os.system('ufbaytech -on 6');
        print "LVDT is ON";
        print datetime.today();
      startTime = time();
      endTime = startTime+ontime;
      if (ontime > 0):
        lvdtvlts = getLVDTVolts();
        for l in range(len(flogs)):
           flogs[l].write(str(datetime.today())+'\t'+str(ontime)+'\t1\t'+lvdtvlts+'\n');
      print "";
      while (time() < endTime):
        diff = endTime-time();
        h = int(diff/3600);
        m = int((diff-3600*h)/60);
        s = int((diff-3600*h-60*m));
        st = '%(h)02d:%(m)02d:%(s)02d' % { 'h':h, 'm':m,'s':s};
        sys.stdout.write('\rTime Remaining: '+st+'   ');
        sys.stdout.flush()
        sleep(1);
      print "";
os.system('ufbaytech -off 6');
print "LVDT is OFF";
lvdtvlts = getLVDTVolts();
for j in range(len(flogs)):
   flogs[j].write(str(datetime.today())+'\t0\t0\t'+lvdtvlts+'\n');
