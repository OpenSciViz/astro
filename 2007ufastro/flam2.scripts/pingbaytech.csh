#!/bin/csh -f
set ip = 192.168.111.6
if( "$1" != "" ) set ip = "$1"
set fcnt = 0
set scnt = 0
while ( 1 )
  date
  ping -i 1 -c 1 $ip >& /dev/null
  if( $status != 0 ) then
    set scnt = 0
    @ fcnt = $fcnt + 1
  else
    set fcnt = 0
    @ scnt = $scnt + 1
endif
echo ping $ip success count == $scnt, failure count == $fcnt
echo ______________________________________________________
if( $fcnt == 0 ) sleep 1
end
