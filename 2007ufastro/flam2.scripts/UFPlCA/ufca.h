#if !defined(UFCA_h__)
#define UFCA_h__ "$Name:  $ $Id: ufca.h,v 0.9 2006/09/29 18:06:39 hon Exp $"
#define UFCA_H__(arg) const char arg##CA_h__rcsId[] = UFCA_h__;

#ifdef SWIG
%module UFCA
%{
#include "ufca.h"
%}
#endif

#include "cadef.h"

extern char* getPVName(chid chan);
extern int eventMon(chid chan);
extern chid connectPV(const char* name, double timeout);
extern void disconnectPV(chid chan);
extern chid subscribe(chid chan);
extern int unsubscribe(chid chan);
extern int getType(chid chan);
extern char* getTypeStr(chid chan);
extern int getElemCnt(chid chan);
extern int getInt(chid chan);
extern double getDouble(chid chan);
extern char* getString(chid chan);
extern char* getAsString(chid chan);
extern int putInt(chid chan, int i);
extern int putDouble(chid chan, double d);
extern int putString(chid chan, const char* s);
extern int nullPV(chid chan);

#endif /* UFCA_h__ */
