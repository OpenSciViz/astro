#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: date.pl 14 2008-06-11 01:49:45Z hon $';
#
$unixtime = time();
if( @ARGV > 0 ) { $unixtime = shift; }
@weekday = ( 'Sun.', 'Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.' ); 
@month = ( 'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May.', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.' ); 
($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime($unixtime);
$y += 1900; $yd++; #$mo++;
if( $mo < 10 ) { $mo = "0$mo"; }
if( $md < 10 ) { $md = "0$md"; }
if( $s < 10 ) { $s = "0$s"; }
if( $m < 10 ) { $m = "0$m"; }
if( $h < 10 ) { $h = "0$h"; }
print "$unixtime == $weekday[$wd] $month[$mo] $md $y $h:$m:$s\n";
exit;

