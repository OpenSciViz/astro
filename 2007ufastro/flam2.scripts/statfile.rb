#!/usr/local/bin/ruby
$rcsId = '$Name:  $ $Id: statfile.rb 14 2008-06-11 01:49:45Z hon $'
require 'ftools'
filename = "RichardElston.fits"
filesz = 4*2048*2048 + 2800 # F2 FITS min. size in bytes
if ARGV.length > 0; filename = ARGV[0]; end 
if ARGV.length > 1; filesz = ARGV[1].to_i; end
bytes = 0
while bytes < filesz
  bytes = File.stat(filename).size
  print "statfile> ", filename, " bytes: ", bytes, " expect (at least): ", filesz, "\n"
  if bytes < 0; exit; end;
  sleep(0.1)
end
exit
