#!/share/local/bin/perl
use UFF2;  # the swig generated module with ufF2
use IO::Socket;  # standard Perl TCP/UDP/IP socket module

$host = "flamperle";
#Filter1
$portA = 7021;

$socA = IO::Socket::INET->new ( PeerAddr => $host, PeerPort => $portA, Proto => 'tcp', Type => SOCK_STREAM );
if (!defined($socA)) {
   die "failed to connect to $host on port $portA\n";
} else {
   print "connected to $host on port $portA\n";
}
$cmd[0] = "A\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"; 
$cmd[1] = "B\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
$cmd[2] = "C\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
$cmd[3] = "D\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

$timeout = 140;
$motor = "Filter1";
$curPos = 0; 
$desPosName = "J-lo";
$blash = 10;
$fastVel = 600;
$slowVel = 200;

if( @ARGV > 0 ) {
  $argval = shift;
  $desPosName = $argval;
  if ($desPosName eq "debug") {
     $debug = 1;
     $argval = shift;
     $desPosName = $argval;
  } else {
     $debug = 0;
  }
  $status = 0;
} else {
  $status = 1;
}

#Read current position
$cmd = "Z\r\n"; #req. indexor A step count
print $socA "$cmd";
$socA->flush();
$curPos = <$socA>;
chomp($curPos);
@temp = split(/ /,$curPos);
$curPos = $temp[-2];
print "CUR POS: $curPos\n";

if ($status == 1) {
   exit;
}

$count = UFF2::ufF2mechMotion4Named($motor, $curPos, $desPosName, $blash, $fastVel, $slowVel, $cmd[0], $cmd[1], $cmd[2], $cmd[3]);
print "OUTPUT: $count \n";
for ($j = 0; $j < $count; $j++) {
   print "$cmd[$j]\n";
   #Strip out indexor name
   $cmd[$j] = substr($cmd[$j], 1);
   #Strip out U
   while (index($cmd[$j], "U") != -1) {
      $upos = index($cmd[$j], "U");
      substr($cmd[$j], $upos, 1, "");
   }
   #Strip out leading spaces
   while (substr($cmd[$j], 0, 1) eq ' ') {
      $cmd[$j] = substr($cmd[$j], 1);
   }
   if (substr($cmd[$j],0,7) eq "F 200 0") {
      $doOrigin = 1;
   } else {
      $doOrigin = 0;
   }
   print "Doing command number $j:\n";
   print "$cmd[$j]\n";
   if (substr($cmd[$j], 0, 1) eq 'F') {
      $desPos = 0;
   } elsif (substr($cmd[$j], 0, 1) eq '+' or substr($cmd[$j], 0, 1) eq '-') {
      $desPos = $curPos+$cmd[$j];
   }
   print "Curr: $curPos   Des: $desPos\n\n";
  if ($debug == 0) {
   print $socA "$cmd[$j]\r\n";
   $socA->flush();
   $reply = <$socA>;
   print "REPLY: $reply\n";
   $isFinished = 0;
   $time = 0;
   $status2 = $curPos;
   while ($isFinished == 0) {
      sleep(4);
      $time+=4;
      if ($time > $timeout) {
	$issueCmds = 0;
        print "Time exceeded $timeout sec!\n";
	while ($issueCmds == 0) {
	   print "Enter a command (or ENTER to exit): ";
	   $userCmd = <STDIN>;
	   print "CMD: $userCmd\n";
	   if ($userCmd eq "\n") {
	      $issueCmds = 1;
	      $isFinished = 1;
	   } else {
	      print $socA "$userCmd\r\n";
	      $socA->flush();
	      $reply = <$socA>;
	      print "REPLY: $reply\n";
	   }
	}
      }
      $oldPos = $status2;
      print $socA "^\r\n";
      $socA->flush();
      $status1 = <$socA>;
      $pos1 = rindex($status1, " ");
      $pos2 = rindex($status1, "\r");
      $status1 = substr($status1, $pos1+1, $pos2-$pos1-1);
      sleep(1);
      $time+=1;
      print $socA "Z\r\n";
      $socA->flush();
      $status2 = <$socA>;
      chomp($status2);
      @temp = split(/ /,$status2);
      $status2 = $temp[-2];
      print "TIME: $time;  ^: $status1;  Z: $status2\n";
      if ($desPos != 0) {
	if ($status1 == 0 and $status2 == $oldPos) {
	   $isFinished = 1;
	   $curPos = $status2;
	}
      } else {
	if ($status1 == 0 and $status2 == $oldPos) {
	   $isFinished = 1;
	   $curPos = 0;
	}
      }
   }
   if ($doOrigin == 1) {
      print $socA "o\r\n";
      $socA->flush();
      $reply = <$socA>;
      print "ORIGIN REPLY: $reply";
      sleep(1);
      print $socA "Z\r\n";
      $socA->flush();
      $reply = <$socA>;
      print "CUR POS: $reply";
   }
  }
}

exit;
