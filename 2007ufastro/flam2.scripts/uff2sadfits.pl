#!/usr/bin/perl
$rcsId = '$Name:  $ $Id: uff2sadfits.pl 14 2008-06-11 01:49:45Z hon $';
# read stdin list of F2 FITS SAD names (stdout of ufsadfits app.)
# and genereate capfast schematic file to stdout (or named file: flamSadFits.sch)
# output for 4 elemnt schematic should look something like the following
# manually dranw with capfast:
#
#[schematic2]
#uniq 4
#[tools]
#[detail]
#[cell use]
#use esirs 96 -377 100 0 esirs#3
#xform 0 304 -224
#p 208 -384 100 1024 1 name:$(top)generic4
#use esirs -480 -377 100 0 esirs#2
#xform 0 -272 -224
#p -368 -384 100 1024 1 name:$(top)generic3
#use esirs 96 7 100 0 esirs#1
#xform 0 304 160
#p 208 0 100 1024 1 name:$(top)generic2
#use esirs -464 7 100 0 esirs#0
#xform 0 -256 160
#p -352 0 100 1024 1 name:$(top)generic1
#[comments]
#
######################### main #########################
$capfast = "flamSadFits.sch";
$instrum = "flam2";
$fitskey = '000.) flam:sad:AIRMASS\n'; # sample output line from ufsadfits app.

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "datum.pl [-h[elp]] [instrum] [subsys: all/cc,ec,dc]\n";
  }
  $instrum = $argval;
#  if( @ARGV > 0 ) { $capfast = shift; }
}
#print "$instrum\n";
print "[schematic2]\nuniq 4\n[tools]\n[detail]\n[cell use]\n";

$xdelta = 600; $ydelta = 400;
$esx = -500; $esy = - $ydelta ;
$xfx = -300; $xfy = 200 - $ydelta ;
$px = -400; $py = - $ydelta ;

# expecting ~200 or less items, say 14x14 or smaller grid
$grid = 14;
$cnt = 0;
while(<STDIN>) {
  $cnt++;
# increment x on cnt and y on  cnt:
  $esx += $xdelta; $xfx += $xdelta; $px += $xdelta;
  if( $cnt % $grid == 0 ) {
    $esy += $ydelta; $xfy += $ydelta; $py += $ydelta; # advance y
    $esx = -500; $xfx = -300; $px = -400; # reset x
  }
  $fitskey = $_;
  chomp $fitskey;
  $fitskey = substr($fitskey, 1+rindex($fitskey, ':'));
#  $f2sad = $instrum . ":sad:" . $fitskey;
  $f2sad = $fitskey;

  $esirs = "use esirs $esx $esy 100 0 esirs\#$cnt\n";
  $xform = "xform 0 $xfx $xfy\n";
  $psch = "p $px $py 100 1024 1 name:\$(top)$f2sad\n";

  print "$esirs$xform$psch";
}
print "[comments]\n";
$version = substr($rcsId, 4+rindex($rcsId, 'Id:'));
print "Generated via Perl script version: ";
print "$version\n";

exit;
