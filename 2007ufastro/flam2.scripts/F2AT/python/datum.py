#!/bin/env python
rcsId = '$Name:  $ $Id: datum.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

instrum= "flam"
subsys = "all" # "cc,ec,dc"

  #end
#end
print "datum> instrum, subsys: subsys\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
sysD = UFCA.connectPV(instrum+":datum.DIR", to)
sysDM = UFCA.connectPV(instrum+":datum.MARK", to)
sysDsub = UFCA.connectPV(instrum+":datum.subsys", to)
sysDC = UFCA.connectPV(instrum+":datumC", to)
pv = UFCA.getPVName(apply)
print "datum> clear system: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
#p = UFCA.putInt(sysD, Mark)
pv = UFCA.getPVName(sysDsub)
p = UFCA.putString(sysDsub, subsys)
m = UFCA.getInt(sysDM)
print "datum> set pv to subsys subsys. instrum:datum marked applying start...\n"
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(sysDC)
ca = UFCA.getInt(applyC)
pv = UFCA.getPVName(sysDC)
pva = UFCA.getPVName(apply)
cnt = 10
while  c == Busy and cnt-- > 0  :
  print "datum> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(sysDC)
  ca = UFCA.getInt(applyC)
#end
cnt = 10
while ca == Busy and cnt-- > 0  :
  print "datum> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(sysDC)
  ca = UFCA.getInt(applyC)
#end
print "datum> pv: c, pva: ca\n"
exit

