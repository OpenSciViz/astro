#!/bin/env python
rcsId = '$Name:  $ $Id: observe.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# main
#dbprint()
instrum = "flam"
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0
#
label = "Test"
mode = "SaveAll" #mce4 cds read 4-64?
index = 1

    exit
  #end
  else :
  #end
  #end
  #end
  #end
  #end
#end

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
obsC = UFCA.connectPV(instrum+":observeC", to)
vobs = UFCA.connectPV(instrum+":observe.VAL", to)
datalabel = UFCA.connectPV(instrum+":observe.DataLabel", to)
vdatalabel = UFCA.connectPV(instrum+":observe.VALDataLabel", to)
usrinfo = UFCA.connectPV(instrum+":observe.UserInfo", to)
vusrinfo = UFCA.connectPV(instrum+":observe.VALUserInfo", to)
datamode = UFCA.connectPV(instrum+":observe.DataMode", to)
vdatamode = UFCA.connectPV(instrum+":observe.VALDataMode", to)
ditheridx = UFCA.connectPV(instrum+":observe.DitherIndex", to)
vditheridx = UFCA.connectPV(instrum+":observe.VALDitherIndex", to)
#
p = UFCA.putInt(apply, Clear)
ac = UFCA.getInt(applyC)
print "observe> clear car: ac\n"
#vdlabel = UFCA.getString(vdatalabel)
#print "observe> current instrum datalabel: ddlabel\n"
p = UFCA.putString(datalabel, label)
dl = UFCA.getString(datalabel)
print "observe> put datalabel: dl\n"
ui = UFCA.getString(usrinfo)
p = UFCA.putString(datamode, mode)
dm = UFCA.getString(datamode)
print "observe> put save/discard mode: dm\n"
p = UFCA.putInt(ditheridx, index)
di = UFCA.getInt(ditheridx)
#
print "observe> clear car: ac\n"
p = UFCA.putInt(apply, Start)
ac = UFCA.getInt(applyC)
c = UFCA.getInt(obsC)
print "observe> obsserve car: c, apply car: ac\n"
cnt = 3
while c == Busy and ac == Busy  and cnt-- > 0  :
  print "observe> cnt, observe car: c, apply car: ac\n"
  time.sleep 1
  c = UFCA.getInt(obsC)
  ac = UFCA.getInt(applyC)
#end
exit
