#!/bin/env python
rcsId = '$Name:  $ $Id: ecsetup.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
instrum= "flam"
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

onoff = "off" # leave LVDT off
P = 30
I = 2
D = 0
terminate = false # global

def sethandler sig 
  trap sig  :
    print "sig = ",sig,"\n"
    if sig == "SIGINT" || sig == "SIGABRT" || sig == "SIGTSTP" 
      if reply == "y"  then terminate = true #end #print "your reply: ", reply, "\n" #end
    #end
  #end
#end

#end
print "ecsetup> ",instrum,"\n"
sethandler("SIGINT") sethandler("SIGABRT") sethandler("SIGTSTP")

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
ecD = UFCA.connectPV(instrum+":ec:setup.DIR", to)
ecDC = UFCA.connectPV(instrum+":ec:setupC", to)
ecDM = UFCA.connectPV(instrum+":ec:setup.marked", to)

ecDCamA = UFCA.connectPV(instrum+":ec:setup.CamAOnOff", to)
ecDCamAK = UFCA.connectPV(instrum+":ec:setup.CamASetPnt", to)
ecDCamAP = UFCA.connectPV(instrum+":ec:setup.CamAP", to)
ecDCamAI = UFCA.connectPV(instrum+":ec:setup.CamAI", to)
ecDCamAD = UFCA.connectPV(instrum+":ec:setup.CamAD", to)

ecDCamB = UFCA.connectPV(instrum+":ec:setup.CamBOnOff", to)
ecDCamBK = UFCA.connectPV(instrum+":ec:setup.CamBSetPnt", to)
ecDCamBP = UFCA.connectPV(instrum+":ec:setup.CamBP", to)
ecDCamBI = UFCA.connectPV(instrum+":ec:setup.CamBI", to)
ecDCamBD = UFCA.connectPV(instrum+":ec:setup.CamBD", to)

ecDLVDT = UFCA.connectPV(instrum+":ec:setup.LVDTOnOff", to)
ecDLVDTV = UFCA.connectPV(instrum+":ec:sad:LVDTVLTS", to)

pv = UFCA.getPVName(apply)
print "ecsetup> clear: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
#p = UFCA.putInt(sysD, Mark)
pvA = UFCA.getPVName(ecDCamA) p = UFCA.putInt(ecDCamA, 1)

pvB = UFCA.getPVName(ecDCamB) p = UFCA.putInt(ecDCamB, 1)

pvL = UFCA.getPVName(ecDLVDT)
if onoff == "off"
  p = UFCA.putInt(ecDLVDT, 0)
else
  p = UFCA.putInt(ecDLVDT, 1)
#end
m = UFCA.getInt(ecDM)
if m <= 0 then print "ecsetup> mark failed for ",pv," ...\n"  exit #end

l = UFCA.getInt(ecDLVDT) a = UFCA.getInt(ecDCamA) b = UFCA.getInt(ecDCamB) 
print "ecsetup> set ",pvL," to ",l,", and ",pvA," to ",a,", and ",pvB," to ",b,"...",instrum,":setup marked...applying start...\n"

pvAK = UFCA.getPVName(ecDCamAK) p = UFCA.putDouble(ecDCamAK, 75.5) pk = UFCA.getDouble(ecDCamAK)
pvAP = UFCA.getPVName(ecDCamAP) p = UFCA.putInt(ecDCamAP, P) pp = UFCA.getInt(ecDCamAP)
pvAI = UFCA.getPVName(ecDCamAI) p = UFCA.putInt(ecDCamAI, I) pi = UFCA.getInt(ecDCamAI)
pvAD = UFCA.getPVName(ecDCamAD) p = UFCA.putInt(ecDCamAD, D) pd = UFCA.getInt(ecDCamAD)
print "ecsetup> set ",pvAK," to ",pk,", and ",pvAP," to ",pp," and ",pvAI," to ",pi," and ",pvAD," to ",pd,"\n"

pvBK = UFCA.getPVName(ecDCamBK) p = UFCA.putDouble(ecDCamBK, 75.5) pk = UFCA.getDouble(ecDCamBK)
pvBP = UFCA.getPVName(ecDCamBP) p = UFCA.putInt(ecDCamBP, P) pp = UFCA.getInt(ecDCamBP)
pvBI = UFCA.getPVName(ecDCamBI) p = UFCA.putInt(ecDCamBI, I) pi = UFCA.getInt(ecDCamBI)
pvBD = UFCA.getPVName(ecDCamBD) p = UFCA.putInt(ecDCamBD, D) pd = UFCA.getInt(ecDCamBD)
print "ecsetup> set ",pvBK," to ",pk,", and ",pvBP," to ",pp," and ",pvBI," to ",pi," and ",pvBD," to ",pd,"\n"

p = UFCA.putInt(apply, Start)
c = UFCA.getInt(ecDC)
pv = UFCA.getPVName(ecDC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(apply)
cnt = 10
while ca == Busy and --cnt > 0
  print "ecsetup> ",pv,": ",c,", ",pva,": ",ca,"\n"
  time.sleep 1
  c = UFCA.getInt(ecDC) ca = UFCA.getInt(applyC)
#end
print "ecsetup> ",pv,": ",c," ",pva,": ",ca,"\n"
exit

