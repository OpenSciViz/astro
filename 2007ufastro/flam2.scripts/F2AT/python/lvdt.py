#!/bin/env python
rcsId = '$Name:  $ $Id: lvdt.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
# globals
instrum= "flam"
Idle = 0
Clear = 1
Busy = 2
Start = 3

def sethandler sig 
  trap sig  :
    print "sig = ",sig,"\n"
    if  sig == "SIGTERM" || sig == "SIGINT" || sig == "SIGABRT" || sig == "SIGTSTP" 
      print "terminating...\n"  exit 
    #end
  #end
#end

def cmdLVDT onoff 
  if onoff == "off" then p = UFCA.putInt(ecDLVDT, 0) else p = UFCA.putInt(ecDLVDT, 1) #end
  m = UFCA.getInt(ecDM)
  if m <= 0 then print "lvdt> mark failed for ",pv," ...\n" exit #end
  l = UFCA.getInt(ecDLVDT)
  print "lvdt> set ",pvL," to ",l,", ",instrum,":setup marked...applying start...\n"
  p = UFCA.putInt(apply, Start)
  c = UFCA.getInt(ecDC)
  pv = UFCA.getPVName(ecDC)
  ca = UFCA.getInt(applyC)
  pva = UFCA.getPVName(apply)
  cnt = 10
  while ca == Busy and --cnt > 0
    print "lvdt> ",pv,": ",c,", ",pva,": ",ca,"\n"
    time.sleep 1
    c = UFCA.getInt(ecDC) ca = UFCA.getInt(applyC)
  #end
  print "lvdt> ",pv,": ",c," ",pva,": ",ca,"\n"
#end

def offLVDT time 
  cmdLVDT "off" 
  print "time.sleep for ",time/60," min.\n"
  time.sleep time
#end

def onLVDT time 
  cmdLVDT "on" 
  s = 0
  while ++s <= time 
    volts = UFCA.getDouble(ecDLVDTV)
    t = Time.new
    print "lvdt> ",t," , ",pvLstat," , ",volts,"\n"
    time.sleep 1
  #end
#end

#end
print "lvdt> ",instrum,"\n"
sethandler("SIGINT") sethandler("SIGABRT") sethandler("SIGTSTP") sethandler("SIGTERM")

to = 1.0 # timeout
# globals:
apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
ecD = UFCA.connectPV(instrum+":ec:setup.DIR", to)
ecDC = UFCA.connectPV(instrum+":ec:setupC", to)
ecDM = UFCA.connectPV(instrum+":ec:setup.marked", to)

ecDLVDT = UFCA.connectPV(instrum+":ec:setup.LVDTOnOff", to)
ecDLVDTV = UFCA.connectPV(instrum+":sad:LVDTVLTS.VAL", to)

pvL = UFCA.getPVName(ecDLVDT)
pvLstat = UFCA.getPVName(ecDLVDTV)

pv = UFCA.getPVName(apply)
print "lvdt> clear: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)

time = 300 # 5 minutes

# do this until terminated..
while true
  3.times do
    onLVDT time  # turn on lvdt and monitor reading time seconds before returning
    offLVDT 300  # tunrn off the lvdt and time.sleep for 5 min. before returning
  #end
  time += 300 # increase duration of sample
#end

exit

