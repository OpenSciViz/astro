#!/bin/env python
rcsId = '$Name:  $ $Id: dbtest.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

PVString = 0
PVInt = 1
PVDouble = 6
#
#db = instrum+":cc:heartbeat.val"
print "PV == db, val == val\n"
to = 0.5
chan = UFCA.connectPV(db, to)
inpv = UFCA.getPVName(chan)
typ = UFCA.getType(chan, to)
print "inpv type == typ\n"
if typ == PVString  : i = UFCA.getString(chan, to) #end
if typ == PVInt  : i = UFCA.getInt(chan, to) #end
if typ == PVDouble  : i = UFCA.getDouble(chan, to) #end
print "inpv == i\n"
if val eq ""  :
  exit
#end
#
@dbrec = split /./,db
print "dbrec[0]\n"
valchan = UFCA.connectPV(dbrec[0], to)
print "dbrec[0] chid: valchan\n"
outpv = UFCA.getPVName(valchan)
print "outpv chid: valchan\n"
if typ == PVInt  :
  UFCA.putInt(chan, val, to)
  i = UFCA.getInt(chan, to)
  o = UFCA.getInt(valchan, to)
#end
if typ == PVDouble  :
  UFCA.putDouble(chan, val, to)
  i = UFCA.getDouble(chan, to)
  o = UFCA.getDouble(valchan, to)
#end
if typ == PVString  :
  UFCA.putString(chan, val, to)
  i = UFCA.getString(chan, to)
  o = UFCA.getString(valchan, to)
#end

print "input inpv is i ==> output outpv is o\n"

exit

