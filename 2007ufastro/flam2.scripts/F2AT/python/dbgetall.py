#!/bin/env python
rcsId = '$Name:  $ $Id: dbgetall.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

PVString = 0
PVInt = 1
PVDouble = 6
#
sub dbget :
  to = 0.5
  chan = UFCA.connectPV(db, to)
  if chan == 0  : exit #end
  pv = UFCA.getPVName(chan)
  typ = UFCA.getType(chan, to)
  styp = UFCA.getTypeStr(chan, to)
#  print "pv type == typ\n"
  if typ == PVString  : i = UFCA.getString(chan, to) #end
  if typ == PVInt  : i = UFCA.getInt(chan, to) #end
  if typ == PVDouble  : i = UFCA.getDouble(chan, to) #end
  print "pv == i, type == styp\n"
#end
# main:
print "getting full list of db fields, this will take a moment..\n"
dball = `sh -c 'ufflam2epicsd -lvv 2>&1' | grep pvname | cut -d ',' -f2`
#print "dball\n"
@dball = split / /,dball
# first element seems to be empty line (blank?)
foreach db (@dball) :
#  print "db ...\n"
  dbget(db)
#end
#
exit
