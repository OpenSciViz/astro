#!/bin/env python
rcsId = '$Name:  $ $Id: obssetup.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# main
Idle = 0
Clear = 1
Busy = 2
Start = 3
clear= 0
to = 1.0

instrum = flam
expt = 2
nrd = 4 #mce4 cds read 4-64?
frmcnt = 1 #frames 1 (sci.) or 2 (eng.)
bias = "null" # if bias override

    print "pbssetup.pl [-h[elp]] [instrum] [frmcnt/rdout] [frmcnt/rdout] [numrd] [biasoverride]\n"
    exit
  #end
  else :
  #end
  #end
  #end
  #end
  #end
#end
print "obssetup> instrum expt: expt frmcnt: frmcnt nrd: nrd bias: bias\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
setupC = UFCA.connectPV(instrum+":obsSetupC", to)
vsetup = UFCA.connectPV(instrum+":obsSetup.VAL", to)
exptime = UFCA.connectPV(instrum+":obsSetup.ExpTime", to)
vexptime = UFCA.connectPV(instrum+":obsSetup.VALExpTime", to)
numreads = UFCA.connectPV(instrum+":obsSetup.NumReads", to)
vnumreads = UFCA.connectPV(instrum+":obsSetup.VALNumReads", to)
readout = UFCA.connectPV(instrum+":obsSetup.ReadoutMode", to)
vreadout = UFCA.connectPV(instrum+":obsSetup.VALReadoutMode", to)
biasmode = UFCA.connectPV(instrum+":obsSetup.BiasMode", to)
vbiasmode = UFCA.connectPV(instrum+":obsSetup.VALBiasMode", to)
wbiasmode = UFCA.connectPV(instrum+":obsSetup.WheelBiasMode", to)
vwbiasmode = UFCA.connectPV(instrum+":obsSetup.VALWheelBiasMode", to)
ovrdbias = UFCA.connectPV(instrum+":obsSetup.OverrideBias", to)
vovrdbias = UFCA.connectPV(instrum+":obsSetup.VALOverrideBias", to)

p = UFCA.putInt(apply, Clear)
ac = UFCA.getInt(applyC)
wbi = UFCA.getInt(wbiasmode)
print "obssetup> current instrum wheelbias: wbi\n"
print "obssetup> clear car: c\n"
p = UFCA.putInt(exptime, expt)
p = UFCA.putInt(numreads, nrd)
p = UFCA.putString(readout, frmcnt)
if bias != "null"  :
  p = UFCA.putString(biasmode, bias)
  p = UFCA.putInt(ovrdbias, 1)
#end
p = UFCA.putInt(apply, Start)
ac = UFCA.getInt(applyC)
c = UFCA.getInt(setupC)
print "obssetup> obssetup car: c, apply car: ac\n"
cnt = 3
while c == Busy and ac == Busy  and cnt-- > 0  :
  print "obssetup> cnt, obssetup car: c, apply car: ac\n"
  time.sleep 1
  c = UFCA.getInt(setupC)
  ac = UFCA.getInt(applyC)
#end
exit
