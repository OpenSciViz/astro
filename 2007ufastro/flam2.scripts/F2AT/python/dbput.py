#!/bin/env python
rcsId = '$Name:  $ $Id: dbput.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

PVString = 0
PVInt = 1
PVDouble = 6
to = 0.5
#
#db = instrum+":cc:heartbeat.val"
val = 0
print "PV == db, val == val\n"
chan = UFCA.connectPV(db, to)
pv = UFCA.getPVName(chan)
typ = UFCA.getType(chan, to)
if typ == PVInt  :
  i = UFCA.getInt(chan, to)
  UFCA.putInt(chan, val, to)
  o = UFCA.getInt(chan, to)
#end
if typ == PVDouble  :
  i = UFCA.getDouble(chan, to)
  UFCA.putDouble(chan, val, to)
  o = UFCA.getDouble(chan, to)
#end
if typ == PVString  :
  i = UFCA.getString(chan, to)
  UFCA.putString(chan, val, to)
  o = UFCA.getString(chan, to)
#end

print "initial pv was i ==> new pv is o\n"

exit

