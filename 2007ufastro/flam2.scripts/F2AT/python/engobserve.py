#!/bin/env python
rcsId = '$Name:  $ $Id: engobserve.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
#dbprint()
Start = 3
Clear = 1
Idle = 0
to = 1.0
frmcnt = 1
#end
if frmcnt > 1  : 
  print "engobserve> EngMode frmcnt == frmcnt\n"
#end
else :
  print "engobserve> SciMode frmcnt == frmcnt\n"
#end
obsC = UFCA.connectPV(instrum+":eng:observeC", to)
obsI = UFCA.connectPV(instrum+":eng:observeC.ival", to)
g = UFCA.getAsString(obsC)
if g ne Idle  :
  print instrum+":eng:observeC not Idle, force Idle? [y/n]"
  a = getc
  if a eq 'n'  : exit #end
  p = UFCA.putInt(obsI, Idle)
#end
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyC = UFCA.connectPV(instrum+":eng:applyC", to)
dhslabel = UFCA.connectPV(instrum+":eng:observe.DHSLabel", to)
edtinit = UFCA.connectPV(instrum+":eng:observe.EDTInit", to)
ds9 = UFCA.connectPV(instrum+":eng:observe.DS9", to)
lut = UFCA.connectPV(instrum+":eng:observe.LUT", to)
fits = UFCA.connectPV(instrum+":eng:observe.FITSFile", to)
frmmode = UFCA.connectPV(instrum+":eng:observe.FrameMode", to)
sim = UFCA.connectPV(instrum+":eng:observe.SimMode", to)
exptime = UFCA.connectPV(instrum+":eng:observe.ExpTime", to)
p = UFCA.putDouble(exptime, 2.0)
p = UFCA.putString(dhslabel,"RichardElston")
p = UFCA.putString(fits,"RichardElston.fits")
p = UFCA.putString(lut, "true")
p = UFCA.putInt(frmmode, frmcnt)
p = UFCA.putInt(ds9, 2)
p = UFCA.putInt(apply, Clear)
p = UFCA.putInt(apply, Start)
exit

############################## subs #####################
sub dbprint :
  my cc = `ufflam2epicsd -lvv |grep 'cc:setup'|grep -v 'setupC'|grep -v 'O,'|cut -d' ' -f4`
  my @dball = split /\n/,cc
  foreach db (@dball) :
    print "db\n"
  #end
  dc = `ufflam2epicsd -lvv | grep 'dc:setup'| grep -v 'setupC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,dc
  foreach db (@dball) :
    print "db\n"
  #end
  ec = `ufflam2epicsd -lvv | grep 'ec:setup'| grep -v 'setupC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,ec
  foreach db (@dball) :
    print "db\n"
  #end
  eng = `ufflam2epicsd -lvv | grep 'eng:observe'| grep -v 'observeC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,eng
  foreach db (@dball) :
    print "db\n"
  #end
#end

