#!/bin/env python
rcsId = '$Name:  $ $Id: signals.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if



sub sighandler :
  print "sighandler> recv'd sig: sig\n"
  if sig eq "INT"  : 
    my u = "n"
    print "exit? [n]: "
    # note that getc returns "\n" if one simply hits enter key
    # but if one hits any other key followed by enter, getc returns the single key char
    u = getc #print "you entered: \"u\"\n"
    if u eq "y") : exit #end
  #end
#end

my signame = "ZERO"    
foreach signame (split(' ', Config:sig_name#end)) :
  print "set sighandler for sig: signame\n"
  if signame ne "ZERO"  : SIG:signame#end = \&sighandler #end
#end

my true = 1
while true  :
  print "waiting for signal...\n"
  time.sleep 1
#end

