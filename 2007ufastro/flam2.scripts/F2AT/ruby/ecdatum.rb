#!/usr/local/bin/ruby
$rcsId = '$Name:  $ $Id: ecdatum.rb 14 2008-06-11 01:49:45Z hon $';
require 'UFCA';
######################### main #########################
instrum= "flam"
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

P = 30
I = 2
D = 0

$terminate = false; # global

def sethandler( sig )
  trap( sig ) {
    print "sig = ",sig,"\n";
    if( sig == "SIGINT" || sig == "SIGABRT" || sig == "SIGTSTP" )
      print "ecdatum> abort? [y/n]: "; reply = $stdin.gets.chomp
      if( reply == "y" ) then $terminate = true; end #print "your reply: ", reply, "\n"; end
    end
  }
end

if( ARGV.length > 0 )
  instrum = argval = ARGV.shift
  if argval == "-h" || argval == "-help" then print "ecdatum.rb [-h[elp]] [instrum]\n"; exit; end
  if( ARGV.length > 0 ) then onoff = ARGV.shift; end
end
print "ecdatum> ",instrum,"\n"
sethandler("SIGINT"); sethandler("SIGABRT"); sethandler("SIGTSTP")

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
ecD = UFCA.connectPV(instrum+":ec:datum.DIR", to)
ecDC = UFCA.connectPV(instrum+":ec:datumC", to)
ecDM = UFCA.connectPV(instrum+":ec:datum.marked", to)

ecDCamA = UFCA.connectPV(instrum+":ec:datum.CamAOnOff", to)
ecDCamAK = UFCA.connectPV(instrum+":ec:datum.CamAKelvin", to)
ecDCamAP = UFCA.connectPV(instrum+":ec:datum.CamAP", to)
ecDCamAI = UFCA.connectPV(instrum+":ec:datum.CamAI", to)
ecDCamAD = UFCA.connectPV(instrum+":ec:datum.CamAD", to)

ecDCamB = UFCA.connectPV(instrum+":ec:datum.CamBOnOff", to)
ecDCamBK = UFCA.connectPV(instrum+":ec:datum.CamBKelvin", to)
ecDCamBP = UFCA.connectPV(instrum+":ec:datum.CamBP", to)
ecDCamBI = UFCA.connectPV(instrum+":ec:datum.CamBI", to)
ecDCamBD = UFCA.connectPV(instrum+":ec:datum.CamBD", to)

pv = UFCA.getPVName(apply)
print "ecdatum> clear: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
#p = UFCA.putInt(sysD, Mark)
pvA = UFCA.getPVName(ecDCamA); p = UFCA.putInt(ecDCamA, 1)
pvB = UFCA.getPVName(ecDCamB); p = UFCA.putInt(ecDCamB, 1)

m = UFCA.getInt(ecDM)
if m <= 0 then print "ecdatum> mark failed for ",pv," ...\n";  exit; end
print "ecdatum> set ",pvA,", and ",pvB," to 1. \n"

pvAK = UFCA.getPVName(ecDCamAK); p = UFCA.putDouble(ecDCamAK, 75.5); pk = UFCA.getDouble(ecDCamAK)
pvAP = UFCA.getPVName(ecDCamAP); p = UFCA.putInt(ecDCamAP, P); pp = UFCA.getInt(ecDCamAP)
pvAI = UFCA.getPVName(ecDCamAI); p = UFCA.putInt(ecDCamAI, I); pi = UFCA.getInt(ecDCamAI)
pvAD = UFCA.getPVName(ecDCamAD); p = UFCA.putInt(ecDCamAD, D); pd = UFCA.getInt(ecDCamAD)
print "ecdatum> set ",pvAK," to ",pk,", and ",pvAP," to ",pp," and ",pvAI," to ",pi," and ",pvAD," to ",pd,"\n"

pvBK = UFCA.getPVName(ecDCamBK); p = UFCA.putDouble(ecDCamBK, 75.5); pk = UFCA.getDouble(ecDCamBK)
pvBP = UFCA.getPVName(ecDCamBP); p = UFCA.putInt(ecDCamBP, P); pp = UFCA.getInt(ecDCamBP)
pvBI = UFCA.getPVName(ecDCamBI); p = UFCA.putInt(ecDCamBI, I); pi = UFCA.getInt(ecDCamBI)
pvBD = UFCA.getPVName(ecDCamBD); p = UFCA.putInt(ecDCamBD, D); pd = UFCA.getInt(ecDCamBD)
print "ecdatum> set ",pvBK," to ",pk,", and ",pvBP," to ",pp," and ",pvBI," to ",pi," and ",pvBD," to ",pd,"\n"

print "ecdatum> set ",instrum,":datum marked, applying start...\n"

p = UFCA.putInt(apply, Start)
c = UFCA.getInt(ecDC)
pv = UFCA.getPVName(ecDC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(apply)
cnt = 10
while ca == Busy && --cnt > 0
  print "ecdatum> ",pv,": ",c,", ",pva,": ",ca,"\n";
  sleep 1
  c = UFCA.getInt(ecDC); ca = UFCA.getInt(applyC)
end
print "ecdatum> ",pv,": ",c," ",pva,": ",ca,"\n"
exit

