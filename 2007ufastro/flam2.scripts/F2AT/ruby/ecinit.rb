#!/usr/local/bin/ruby
rcsId = '$Name:  $ $Id: ecinit.rb 14 2008-06-11 01:49:45Z hon $'
#
# load the Swig generated Epics channel access module:
require 'UFCA'
#
######################### main #########################
#
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0
$terminate = false; # global

def sethandler( sig )
  trap(sig) {
    print "sig = ",sig,"\n";
    if( sig == "SIGINT" || sig == "SIGABRT" || sig == "SIGTSTP" )
      print "ecinit> abort? [y/n]: "; reply = $stdin.gets.chomp
      if( reply == "y" ) then $terminate = true; end #print "your reply: ", reply, "\n"; end
    end
  }
end

instrum= "flam"
host = `hostname`; host = host.chop #host = host.chomp

if ARGV.length > 0 #then
#if( ARGV.length > 0 ) 
  instrum = argval = ARGV.shift
#  if argval  == "-h" || argval == "-help" then puts "ecinit.rb [-h[elp]] [instrum] [agenthost]"; exit; end
  if( argval  == "-h" || argval == "-help" ) then puts "ecinit.rb [-h[elp]] [instrum] [agenthost]"; exit; end
#  if argval == "-h" || argval == "-help" then raise "ecinit.rb [-h[elp]] [instrum] [agenthost]\n"; end
  if ARGV.length > 0 then host = ARGV.shift; end
end

print "ecinit> ",instrum," device agent connections on ",host,"\n"

sethandler("SIGINT"); sethandler("SIGABRT"); sethandler("SIGTSTP")

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
ecI = UFCA.connectPV(instrum+":ec:init.DIR", to)
ecIM = UFCA.connectPV(instrum+":ec:init.MARK", to)
ecIC = UFCA.connectPV(instrum+":ec:initC", to)
ecIhost = UFCA.connectPV(instrum+":ec:init.host", to)
pv = UFCA.getPVName(apply)
print "ecinit> clear system: ",pv,"\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
pv = UFCA.getPVName(ecIhost)
p = UFCA.putString(ecIhost, host)
m = UFCA.getInt(ecIM)
if m <= 0 then print "ecinit> mark failed for",pv,"\n"; end
print "ecinit> set ",pv," to host ",host," , ",instrum,":init marked, applying start...","\n"
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(ecIC)
pv = UFCA.getPVName(ecIC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(applyC)
cnt = 10
while c == Busy || ca == Busy && --cnt > 0
  print "ecinit> ",pv,": ", c, ", ",pva,": ",ca,"\n" # " terminate is ", $terminate,"\n"
  sleep 1
  if( $terminate ) then puts "ecinit> aborting..."; exit; end
  c = UFCA.getInt(ecIC)
  ca = UFCA.getInt(applyC)
end
print "ecinit> ",pv,": ", c, ", ",pva,": ",ca,"\n"
exit
