#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dcsetup.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
######################### main #########################

$instrum= "flam";
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;
$allbias = 0.5;
$allpreamp = 7.0;
$ct = 10;
$cnt = 1;
$exp = 2;
$pbc = 5;

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "dcdatum.pl [-h[elp]] [instrum]\n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $allbias = shift; }
  if( @ARGV > 0 ) { $allpreamp = shift; }
}

print "dcsetup> $instrum\n";
$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$dcCI = UFCA::connectPV("$instrum:dc:setupC.IVAL", $to);
$dcC = UFCA::connectPV("$instrum:dc:setupC.VAL", $to);
$dc = UFCA::connectPV("$instrum:dc:setup.VAL", $to);

$dcCDS = UFCA::connectPV("$instrum:dc:setup.CDSReads", $to);
$dcCT = UFCA::connectPV("$instrum:dc:setup.CycleType", $to);
$dcExpCnt = UFCA::connectPV("$instrum:dc:setup.ExpCnt", $to);

# these are all ints
$dcMSec = UFCA::connectPV("$instrum:dc:setup.MilliSec", $to);
$dcPBC = UFCA::connectPV("$instrum:dc:setup.PixelBaseClock", $to);
$dcPost = UFCA::connectPV("$instrum:dc:setup.Postsets", $to);
$dcPre = UFCA::connectPV("$instrum:dc:setup.Presets", $to);
$dcSec = UFCA::connectPV("$instrum:dc:setup.Seconds", $to);
$dcto = UFCA::connectPV("$instrum:dc:setup.timeout", $to);
$vdcCDS = UFCA::connectPV("$instrum:dc:setup.VALCDSReads", $to);
$vdcCT = UFCA::connectPV("$instrum:dc:setup.VALCycleType", $to);
$vdcExpCnt = UFCA::connectPV("$instrum:dc:setup.VALExpCnt", $to);
$vdcMSec = UFCA::connectPV("$instrum:dc:setup.VALMilliSec", $to);
$vdcPBC = UFCA::connectPV("$instrum:dc:setup.VALPixelBaseClock", $to);
$vdcPost = UFCA::connectPV("$instrum:dc:setup.VALPostsets", $to);
$vdcPre = UFCA::connectPV("$instrum:dc:setup.VALPresets", $to);
$vdcSec = UFCA::connectPV("$instrum:dc:setup.VALSeconds", $to);
$vdcto = UFCA::connectPV("$instrum:dc:setup.VALtimeout", $to);


# these are all doubles
$dcbg = UFCA::connectPV("$instrum:dc:setup.BiasGATE", $to); push @bias, $dcbg;
$dcbp = UFCA::connectPV("$instrum:dc:setup.BiasPWR", $to); push @bias, $dcbp;
$dcbvc = UFCA::connectPV("$instrum:dc:setup.BiasVCC", $to); push @bias, $dcbvc;
$dcbvr = UFCA::connectPV("$instrum:dc:setup.BiasVRESET", $to); push @bias, $dcbvr;
$vdcbg = UFCA::connectPV("$instrum:dc:setup.VALBiasGATE", $to); push @vbias, $vdcbg;
$vdcbp = UFCA::connectPV("$instrum:dc:setup.VALBiasPWR", $to); push @vbias, $vdcbp;
$vdcbvc = UFCA::connectPV("$instrum:dc:setup.VALBiasVCC", $to); push @vbias, $vdcbvc;
$vdcbvr = UFCA::connectPV("$instrum:dc:setup.VALBiasVRESET", $to); push @vbias, $vdcbvr;

# these are all doubles
$dcpa01 = UFCA::connectPV("$instrum:dc:setup.PreAmp01", $to); push @preamp, $dcpa01;
$dcpa02 = UFCA::connectPV("$instrum:dc:setup.PreAmp02", $to); push @preamp, $dcpa02;
$dcpa03 = UFCA::connectPV("$instrum:dc:setup.PreAmp03", $to); push @preamp, $dcpa03;
$dcpa04 = UFCA::connectPV("$instrum:dc:setup.PreAmp04", $to); push @preamp, $dcpa04;
$dcpa05 = UFCA::connectPV("$instrum:dc:setup.PreAmp05", $to); push @preamp, $dcpa05;
$dcpa06 = UFCA::connectPV("$instrum:dc:setup.PreAmp06", $to); push @preamp, $dcpa06;
$dcpa07 = UFCA::connectPV("$instrum:dc:setup.PreAmp07", $to); push @preamp, $dcpa07;
$dcpa08 = UFCA::connectPV("$instrum:dc:setup.PreAmp08", $to); push @preamp, $dcpa08;
$dcpa09 = UFCA::connectPV("$instrum:dc:setup.PreAmp09", $to); push @preamp, $dcpa09;
$dcpa10 = UFCA::connectPV("$instrum:dc:setup.PreAmp10", $to); push @preamp, $dcpa10;
$dcpa11 = UFCA::connectPV("$instrum:dc:setup.PreAmp11", $to); push @preamp, $dcpa11;
$dcpa12 = UFCA::connectPV("$instrum:dc:setup.PreAmp12", $to); push @preamp, $dcpa12;
$dcpa13 = UFCA::connectPV("$instrum:dc:setup.PreAmp13", $to); push @preamp, $dcpa13;
$dcpa14 = UFCA::connectPV("$instrum:dc:setup.PreAmp14", $to); push @preamp, $dcpa14;
$dcpa15 = UFCA::connectPV("$instrum:dc:setup.PreAmp15", $to); push @preamp, $dcpa15;
$dcpa16 = UFCA::connectPV("$instrum:dc:setup.PreAmp16", $to); push @preamp, $dcpa16;
$dcpa17 = UFCA::connectPV("$instrum:dc:setup.PreAmp17", $to); push @preamp, $dcpa17;
$dcpa18 = UFCA::connectPV("$instrum:dc:setup.PreAmp18", $to); push @preamp, $dcpa18;
$dcpa19 = UFCA::connectPV("$instrum:dc:setup.PreAmp19", $to); push @preamp, $dcpa19;
$dcpa20 = UFCA::connectPV("$instrum:dc:setup.PreAmp20", $to); push @preamp, $dcpa20;
$dcpa21 = UFCA::connectPV("$instrum:dc:setup.PreAmp21", $to); push @preamp, $dcpa21;
$dcpa22 = UFCA::connectPV("$instrum:dc:setup.PreAmp22", $to); push @preamp, $dcpa22;
$dcpa23 = UFCA::connectPV("$instrum:dc:setup.PreAmp23", $to); push @preamp, $dcpa23;
$dcpa24 = UFCA::connectPV("$instrum:dc:setup.PreAmp24", $to); push @preamp, $dcpa24;
$dcpa25 = UFCA::connectPV("$instrum:dc:setup.PreAmp25", $to); push @preamp, $dcpa25;
$dcpa26 = UFCA::connectPV("$instrum:dc:setup.PreAmp26", $to); push @preamp, $dcpa26;
$dcpa27 = UFCA::connectPV("$instrum:dc:setup.PreAmp27", $to); push @preamp, $dcpa27;
$dcpa28 = UFCA::connectPV("$instrum:dc:setup.PreAmp28", $to); push @preamp, $dcpa28;
$dcpa29 = UFCA::connectPV("$instrum:dc:setup.PreAmp29", $to); push @preamp, $dcpa29;
$dcpa30 = UFCA::connectPV("$instrum:dc:setup.PreAmp30", $to); push @preamp, $dcpa30;
$dcpa31 = UFCA::connectPV("$instrum:dc:setup.PreAmp31", $to); push @preamp, $dcpa31;
$dcpa32 = UFCA::connectPV("$instrum:dc:setup.PreAmp32", $to); push @preamp, $dcpa32;
$vdcpa01 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp01", $to); push @vpreamp, $vdcpa01;
$vdcpa02 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp02", $to); push @vpreamp, $vdcpa02;
$vdcpa03 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp03", $to); push @vpreamp, $vdcpa03;
$vdcpa04 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp04", $to); push @vpreamp, $vdcpa04;
$vdcpa05 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp05", $to); push @vpreamp, $vdcpa05;
$vdcpa06 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp06", $to); push @vpreamp, $vdcpa06;
$vdcpa07 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp07", $to); push @vpreamp, $vdcpa07;
$vdcpa08 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp08", $to); push @vpreamp, $vdcpa08;
$vdcpa09 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp09", $to); push @vpreamp, $vdcpa09;
$vdcpa10 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp10", $to); push @vpreamp, $vdcpa10;
$vdcpa11 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp11", $to); push @vpreamp, $vdcpa11;
$vdcpa12 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp12", $to); push @vpreamp, $vdcpa12;
$vdcpa13 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp13", $to); push @vpreamp, $vdcpa13;
$vdcpa14 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp14", $to); push @vpreamp, $vdcpa14;
$vdcpa15 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp15", $to); push @vpreamp, $vdcpa15;
$vdcpa16 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp16", $to); push @vpreamp, $vdcpa16;
$vdcpa17 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp17", $to); push @vpreamp, $vdcpa17;
$vdcpa18 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp18", $to); push @vpreamp, $vdcpa18;
$vdcpa19 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp19", $to); push @vpreamp, $vdcpa19;
$vdcpa20 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp20", $to); push @vpreamp, $vdcpa20;
$vdcpa21 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp21", $to); push @vpreamp, $vdcpa21;
$vdcpa22 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp22", $to); push @vpreamp, $vdcpa22;
$vdcpa23 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp23", $to); push @vpreamp, $vdcpa23;
$vdcpa24 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp24", $to); push @vpreamp, $vdcpa24;
$vdcpa25 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp25", $to); push @vpreamp, $vdcpa25;
$vdcpa26 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp26", $to); push @vpreamp, $vdcpa26;
$vdcpa27 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp27", $to); push @vpreamp, $vdcpa27;
$vdcpa28 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp28", $to); push @vpreamp, $vdcpa28;
$vdcpa29 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp29", $to); push @vpreamp, $vdcpa29;
$vdcpa30 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp30", $to); push @vpreamp, $vdcpa30;
$vdcpa31 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp31", $to); push @vpreamp, $vdcpa31;
$vdcpa32 = UFCA::connectPV("$instrum:dc:setup.VALPreAmp32", $to); push @vpreamp, $vdcpa32;

$p = UFCA::putInt($dcCT, $ct);
$p = UFCA::putInt($dcSec, $exp);
$p = UFCA::putInt($dcExpCnt, $cnt);

foreach $bi (@bias) { $p = UFCA::putDouble($bi, $allbias); }
foreach $pa (@preamp) { $p = UFCA::putDouble($pa, $allpreamp); }

$p = UFCA::putInt($apply, $Start);

sleep 1;

foreach $bi (@vbias) { $pd = UFCA::getDouble($bi); $pv = UFCA::getPVName($bi); print "$pv == $pd\n"; }
foreach $pa (@vpreamp) { $pd = UFCA::getDouble($pa); $pv = UFCA::getPVName($pa); print "$pv == $pd\n"; }

exit;
