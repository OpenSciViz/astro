#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dbput.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
$to = 0.5;
#
#$db = "flam:cc:heartbeat.val";
$db = shift;
$val = 0;
if( @ARGV > 0 ) { $val = shift; }
print "PV == $db, val == $val\n";
$chan = UFCA::connectPV($db, $to);
$pv = UFCA::getPVName($chan);
$typ = UFCA::getType($chan, $to);
if( $typ == $PVInt ) {
  $i = UFCA::getInt($chan, $to);
  UFCA::putInt($chan, $val, $to);
  $o = UFCA::getInt($chan, $to);
}
if( $typ == $PVDouble ) {
  $i = UFCA::getDouble($chan, $to);
  UFCA::putDouble($chan, $val, $to);
  $o = UFCA::getDouble($chan, $to);
}
if( $typ == $PVString ) {
  $i = UFCA::getString($chan, $to);
  UFCA::putString($chan, $val, $to);
  $o = UFCA::getString($chan, $to);
}

print "initial $pv was $i ==> new $pv is $o\n";

exit;

