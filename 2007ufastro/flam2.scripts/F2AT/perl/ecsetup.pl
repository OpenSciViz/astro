#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: ecsetup.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$init = UFCA::connectPV("flam:ec:init.dir", $to);
$initR = UFCA::connectPV("flam:ec:initC", $to);
$setupR = UFCA::connectPV("flam:ec:setupC", $to);
$camP = UFCA::connectPV("flam:ec:setup.CamP", $to);
$camI = UFCA::connectPV("flam:ec:setup.CamI", $to);
$camD = UFCA::connectPV("flam:ec:setup.CamD", $to);
$camR = UFCA::connectPV("flam:ec:setup.CamRange", $to);
$camK = UFCA::connectPV("flam:ec:setup.CamKelvin", $to);
$mosP = UFCA::connectPV("flam:ec:setup.MOSP", $to);
$mosI = UFCA::connectPV("flam:ec:setup.MOSI", $to);
$mosD = UFCA::connectPV("flam:ec:setup.MOSD", $to);
$mosR = UFCA::connectPV("flam:ec:setup.MOSRange", $to);
$mosK = UFCA::connectPV("flam:ec:setup.MOSKelvin", $to);
$tmo = UFCA::connectPV("flam:ec:setupC.timeout", $to);
$sadmos = UFCA::connectPV("flam:sad:MOSKelvin", $to);
$sadcam = UFCA::connectPV("flam:sad:CamKelvin", $to);
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  print "$pv type == $styp, val == $val\n";
  return $val;
}

sub ecinit {
  setPV($initR, $Idle);
  setPV($init, $Preset);
  setPV($applyR, $Idle);
  setPV($apply, $Start);
  my $done = 0;
  while ( $done == 0 ) {
    my $ir = getPV($initR);
    my $ar = getPV($applyR);
    if( $ir == $Error || $ar == $Error ) { $done = -1; }
    if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
    sleep 1;
  }
}

sub ecsetup {
  my $k1 = 71.1;
  my $k2 = 72.2;
  setPV($setupR, $Idle);
  setPV($mosP, 500);
  setPV($mosI, 100);
  setPV($mosD, 500);
  setPV($mosR, 3);
  setPV($mosD, $k1);
  setPV($camP, 500);
  setPV($camI, 100);
  setPV($camD, 500);
  setPV($camR, 3);
  setPV($camD, $k2);
  setPV($applyR, $Idle);
  setPV($apply, $Start);
  my $done = 0;
  while ( $done == 0 ) {
    my $ir = getPV($initR);
    my $ar = getPV($applyR);
    if( $ir == $Error || $ar == $Error ) { $done = -1; }
    if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
    sleep 1;
  }
}
# 
main:
$init = false;
getPV($sadmos);
getPV($sadcam);
if( $init ) { ecinit(); }

# setup:
$k1 = 71.1; $r1 = 3;
$k2 = 72.2; $r2 = 3;
@pid1 = (500, 100, 500);
@pid2 = @pid1;
setPV($setupR, $Idle);
setPV($mosP, $pid1[0]);
setPV($mosI, $pid1[1]);
setPV($mosD, $pid1[2]);
setPV($mosR, $r1);
setPV($mosD, $k1);
#
setPV($camP, $pid2[0]);
setPV($camI, $pid2[1]);
setPV($camD, $pid2[2]);
setPV($camR, $r2);
setPV($camD, $k2);
setPV($applyR, $Idle);
setPV($apply, $Start);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}
#
exit;
