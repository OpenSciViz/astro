#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: ecstop.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$init = UFCA::connectPV("flam:ec:init.dir", $to);
$initR = UFCA::connectPV("flam:ec:initC", $to);
$datumR = UFCA::connectPV("flam:ec:datumC", $to);
$camP = UFCA::connectPV("flam:ec:datum.CamP", $to);
$camI = UFCA::connectPV("flam:ec:datum.CamI", $to);
$camD = UFCA::connectPV("flam:ec:datum.CamD", $to);
$camR = UFCA::connectPV("flam:ec:datum.CamRange", $to);
$camK = UFCA::connectPV("flam:ec:datum.CamKelvin", $to);
$mosP = UFCA::connectPV("flam:ec:datum.MOSP", $to);
$mosI = UFCA::connectPV("flam:ec:datum.MOSI", $to);
$mosD = UFCA::connectPV("flam:ec:datum.MOSD", $to);
$mosR = UFCA::connectPV("flam:ec:datum.MOSRange", $to);
$mosK = UFCA::connectPV("flam:ec:datum.MOSKelvin", $to);
$tmo = UFCA::connectPV("flam:ec:datumC.timeout", $to);
$sadmos = UFCA::connectPV("flam:sad:MOSKelvin", $to);
$sadcam = UFCA::connectPV("flam:sad:CamKelvin", $to);
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  print "$pv type == $styp, val == $val\n";
  return $val;
}
# main:
getPV($sadmos);
getPV($sadcam);
# init:
setPV($initR, $Idle);
setPV($init, $Preset);
setPV($applyR, $Idle);
setPV($apply, $Start);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}
# datum:
$k1 = 71.1;
$k2 = 72.2;
setPV($datumR, $Idle);
setPV($mosP, 500);
setPV($mosI, 100);
setPV($mosD, 500);
setPV($mosR, 3);
setPV($mosD, $k1);
setPV($camP, 500);
setPV($camI, 100);
setPV($camD, 500);
setPV($camR, 3);
setPV($camD, $k2);
setPV($applyR, $Idle);
setPV($apply, $Start);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}
#
exit;
