#!/usr/bin/perl -w
use Config;
my $rcsId = '$Name:  $ $Id: signals.pl 14 2008-06-11 01:49:45Z hon $';

defined $Config{sig_name} || die "No signal names in Config?";

sub sighandler {
  my $sig = shift;
  print "sighandler> recv'd sig: $sig\n";
  if( $sig eq "INT" ) { 
    my $u = "n";
    print "exit? [n]: ";
    # note that getc returns "\n" if one simply hits enter key
    # but if one hits any other key followed by enter, getc returns the single key char
    $u = getc; #print "you entered: \"$u\"\n";
    if( $u eq "y") { exit; }
  }
}

my $signame = "ZERO";    
foreach $signame (split(' ', $Config{sig_name})) {
  print "set sighandler for sig: $signame\n";
  if( $signame ne "ZERO" ) { $SIG{$signame} = \&sighandler; }
}

my $true = 1;
while( $true ) {
  print "waiting for signal...\n";
  sleep 1;
}

