#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: ccabort.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$init = UFCA::connectPV("flam:cc:init.dir", $to);
$initR = UFCA::connectPV("flam:cc:initC", $to);
$datumR = UFCA::connectPV("flam:cc:datumC", $to);
$sadwin = UFCA::connectPV("flam:sad:WindowSteps", $to);
$saddck = UFCA::connectPV("flam:sad:DeckerSteps", $to);
$sadmos = UFCA::connectPV("flam:sad:MOSSteps", $to);
$sadf1 = UFCA::connectPV("flam:sad:Filter1Steps", $to);
$sadlyo = UFCA::connectPV("flam:sad:LyotSteps", $to);
$sadf2 = UFCA::connectPV("flam:sad:Filter2Steps", $to);
$sadgrsm = UFCA::connectPV("flam:sad:GrismSteps", $to);
$sadfoc = UFCA::connectPV("flam:sad:FocusSteps", $to);
# inputs:
#$dall = UFCA::connectPV("flam:cc:datum.All", $to);
$dck = UFCA::connectPV("flam:cc:datum.Decker", $to);
$dckbl = UFCA::connectPV("flam:cc:datum.DeckerBacklash", $to);
$dckdir = UFCA::connectPV("flam:cc:datum.DeckerDirection", $to);
$dckvel = UFCA::connectPV("flam:cc:datum.DeckerVel", $to);
$f1 = UFCA::connectPV("flam:cc:datum.Filter1", $to);
$f1bl = UFCA::connectPV("flam:cc:datum.Filter1Backlash", $to);
$f1dir = UFCA::connectPV("flam:cc:datum.Filter1Direction", $to);
$f1vel = UFCA::connectPV("flam:cc:datum.Filter1Vel", $to);
$f2 = UFCA::connectPV("flam:cc:datum.Filter2", $to);
$f2bl = UFCA::connectPV("flam:cc:datum.Filter2Backlash", $to);
$f2dir = UFCA::connectPV("flam:cc:datum.Filter2Direction", $to);
$f2vel = UFCA::connectPV("flam:cc:datum.Filter2Vel", $to);
$foc = UFCA::connectPV("flam:cc:datum.Focus", $to);
$fucbl = UFCA::connectPV("flam:cc:datum.FocusBacklash", $to);
$focdir = UFCA::connectPV("flam:cc:datum.FocusDirection", $to);
$focvel = UFCA::connectPV("flam:cc:datum.FocusVel", $to);
$grsm = UFCA::connectPV("flam:cc:datum.Grism", $to);
$grsmbl = UFCA::connectPV("flam:cc:datum.GrismBacklash", $to);
$grsmdir = UFCA::connectPV("flam:cc:datum.GrismDirection", $to);
$grsmvel = UFCA::connectPV("flam:cc:datum.GrismVel", $to);
$lyot = UFCA::connectPV("flam:cc:datum.Lyot", $to);
$lyotbl = UFCA::connectPV("flam:cc:datum.LyotBacklash", $to);
$lyotdir = UFCA::connectPV("flam:cc:datum.LyotDirection", $to);
$lyotvel = UFCA::connectPV("flam:cc:datum.LyotVel", $to);
$mos = UFCA::connectPV("flam:cc:datum.MOS", $to);
$mosbl = UFCA::connectPV("flam:cc:datum.MOSBacklash", $to);
$mosdir = UFCA::connectPV("flam:cc:datum.MOSDirection", $to);
$mosvel = UFCA::connectPV("flam:cc:datum.MOSVel", $to);
$win = UFCA::connectPV("flam:cc:datum.Window", $to);
$winbl = UFCA::connectPV("flam:cc:datum.WindowBacklash", $to);
$windir = UFCA::connectPV("flam:cc:datum.WindowDirection", $to);
$winvel = UFCA::connectPV("flam:cc:datum.WindowVel", $to);
$dir = UFCA::connectPV("flam:cc:datum.dir", $to);
$icid = UFCA::connectPV("flam:cc:datum.icid", $to);
$mess = UFCA::connectPV("flam:cc:datum.mess", $to);
$ocid = UFCA::connectPV("flam:cc:datum.ocid", $to);
$tmo = UFCA::connectPV("flam:cc:datum.timeout", $to);
$val = UFCA::connectPV("flam:cc:datum.val", $to);
#
sub setPV {
  my $ch = shift @_;
  my $val = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $i, $o;
  #print "PV == $pv, type == $styp, val == $val\n";
  if( $typ == $PVInt ) {
    $i = UFCA::getInt($ch, $to); UFCA::putInt($ch, $val, $to); $o = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $i = UFCA::getDouble($ch, $to); UFCA::putDouble($ch, $val, $to); $o = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $i = UFCA::getString($ch, $to); UFCA::putString($ch, $val, $to); $o = UFCA::getString($ch, $to);
  }
  print "initial $pv was $i ==> new $pv is $o\n";
}
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  print "$pv type == $styp, val == $val\n";
  return $val;
}
# main:
getPV($sad);
$val = $Preset;
# init:
setPV($initR, $Idle);
setPV($init, $Preset);
setPV($applyR, $Idle);
setPV($apply, $Start);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}
# datum:
setPV($datumR, $Idle);
setPV($dck, $Preset);
setPV($dckbl, 20);
setPV($dckdir, "+");
setPV($dckvel, 100);
setPV($f1, $Preset);
setPV($f1bl, 20);
setPV($f1dir, "+");
setPV($f1vel, 100);
$f2 = UFCA::connectPV("flam:cc:datum.Filter2", $to);
$f2bl = UFCA::connectPV("flam:cc:datum.Filter2Backlash", $to);
$f2dir = UFCA::connectPV("flam:cc:datum.Filter2Direction", $to);
$f2vel = UFCA::connectPV("flam:cc:datum.Filter2Vel", $to);
setPV($f2, $Preset);
setPV($f2bl, 20);
setPV($f2dir, "+");
setPV($f2vel, 100);
$foc = UFCA::connectPV("flam:cc:datum.Focus", $to);
$focbl = UFCA::connectPV("flam:cc:datum.FocusBacklash", $to);
$focdir = UFCA::connectPV("flam:cc:datum.FocusDirection", $to);
$focvel = UFCA::connectPV("flam:cc:datum.FocusVel", $to);
setPV($foc, $Preset);
setPV($focbl, 20);
setPV($focdir, "+");
setPV($focvel, 100);
$grsm = UFCA::connectPV("flam:cc:datum.Grism", $to);
$grsmbl = UFCA::connectPV("flam:cc:datum.GrismBacklash", $to);
$grsmdir = UFCA::connectPV("flam:cc:datum.GrismDirection", $to);
$grsmvel = UFCA::connectPV("flam:cc:datum.GrismVel", $to);
setPV($grsm, $Preset);
setPV($grsmbl, 20);
setPV($grsmdir, "+");
setPV($grsmvel, 100);
$lyot = UFCA::connectPV("flam:cc:datum.Lyot", $to);
$lyotbl = UFCA::connectPV("flam:cc:datum.LyotBacklash", $to);
$lyotdir = UFCA::connectPV("flam:cc:datum.LyotDirection", $to);
$lyotvel = UFCA::connectPV("flam:cc:datum.LyotVel", $to);
setPV($lyot, $Preset);
setPV($lyotbl, 20);
setPV($lyotdir, "+");
setPV($lyotvel, 100);
$mos = UFCA::connectPV("flam:cc:datum.MOS", $to);
$mosbl = UFCA::connectPV("flam:cc:datum.MOSBacklash", $to);
$mosdir = UFCA::connectPV("flam:cc:datum.MOSDirection", $to);
$mosvel = UFCA::connectPV("flam:cc:datum.MOSVel", $to);
setPV($mos, $Preset);
setPV($mosbl, 20);
setPV($mosdir, "+");
setPV($mosvel, 100);
$win = UFCA::connectPV("flam:cc:datum.Window", $to);
$winbl = UFCA::connectPV("flam:cc:datum.WindowBacklash", $to);
$windir = UFCA::connectPV("flam:cc:datum.WindowDirection", $to);
$winvel = UFCA::connectPV("flam:cc:datum.WindowVel", $to);
setPV($win, $Preset);
setPV($winbl, 20);
setPV($windir, "+");
setPV($winvel, 100);
#$dir = UFCA::connectPV("flam:cc:datum.dir", $to);
#$icid = UFCA::connectPV("flam:cc:datum.icid", $to);
#$mess = UFCA::connectPV("flam:cc:datum.mess", $to);
#$ocid = UFCA::connectPV("flam:cc:datum.ocid", $to);
#$tmo = UFCA::connectPV("flam:cc:datum.timeout", $to);
#$val = UFCA::connectPV("flam:cc:datum.val", $to);
setPV($applyR, $Idle);
setPV($apply, $Start);
$done = 0;
while ( $done == 0 ) {
  $ir = getPV($initR);
  $ar = getPV($applyR);
  if( $ir == $Error || $ar == $Error ) { $done = -1; }
  if( $ir == $Idle && $ar == $Idle ) { $done = 1; }
  sleep 1;
}

exit;
