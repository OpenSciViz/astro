#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: mosbarcode.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$setupR = UFCA::connectPV("flam:cc:setupC", $to);
$mosR = UFCA::connectPV("flam:cc:setupMOSC", $to);
$init = UFCA::connectPV("flam:cc:init.dir", $to);
$initR = UFCA::connectPV("flam:cc:initC", $to);
# sad .val:
$b0 = UFCA::connectPV("flam:sad:MOSPlateBarCode", $to);
$bc1 = UFCA::connectPV("flam:sad:MOSCIRC1", $to);
$bc2 = UFCA::connectPV("flam:sad:MOSCIRC2", $to);
$b1 = UFCA::connectPV("flam:sad:MOSPLT01", $to);
$b2 = UFCA::connectPV("flam:sad:MOSPLT02", $to);
$b3 = UFCA::connectPV("flam:sad:MOSPLT03", $to);
$b4 = UFCA::connectPV("flam:sad:MOSPLT04", $to);
$b5 = UFCA::connectPV("flam:sad:MOSPLT05", $to);
$b6 = UFCA::connectPV("flam:sad:MOSPLT06", $to);
$b7 = UFCA::connectPV("flam:sad:MOSPLT07", $to);
$b8 = UFCA::connectPV("flam:sad:MOSPLT08", $to);
$b9 = UFCA::connectPV("flam:sad:MOSPLT09", $to);
$b10 = UFCA::connectPV("flam:sad:MOSPLT10", $to);
@barcdsad = ($bc1, $bc2, $b1, $b2, $b3, $b4, $b5, $b6, $b7, $b8, $b9, $b10);
# sad .inp:
$bc1i = UFCA::connectPV("flam:sad:MOSCIRC1.inp", $to);
$bc2i = UFCA::connectPV("flam:sad:MOSCIRC2.inp", $to);
$b1i = UFCA::connectPV("flam:sad:MOSPLT01.inp", $to);
$b2i = UFCA::connectPV("flam:sad:MOSPLT02.inp", $to);
$b3i = UFCA::connectPV("flam:sad:MOSPLT03.inp", $to);
$b4i = UFCA::connectPV("flam:sad:MOSPLT04.inp", $to);
$b5i = UFCA::connectPV("flam:sad:MOSPLT05.inp", $to);
$b6i = UFCA::connectPV("flam:sad:MOSPLT06.inp", $to);
$b7i = UFCA::connectPV("flam:sad:MOSPLT07.inp", $to);
$b8i = UFCA::connectPV("flam:sad:MOSPLT08.inp", $to);
$b9i = UFCA::connectPV("flam:sad:MOSPLT09.inp", $to);
$b10i = UFCA::connectPV("flam:sad:MOSPos10.inp", $to);
@barcdisad = ($bc1i, $bc2i, $b1i, $b2i, $b3i, $b4i, $b5i, $b6i, $b7i, $b8i, $b9i, $b10i);
print "connected to Barcode SAD I/O...\n";
#
#
sub setPV {
  my $ch = shift @_;
  my $val = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $i, $o;
  print "PV == $pv, type == $styp, val == $val\n";
  if( $typ == $PVInt ) {
    $i = UFCA::getInt($ch, $to); UFCA::putInt($ch, $val, $to); $o = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $i = UFCA::getDouble($ch, $to); UFCA::putDouble($ch, $val, $to); $o = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $i = UFCA::getString($ch, $to); UFCA::putString($ch, $val, $to); $o = UFCA::getString($ch, $to);
  }

  #print "initial $pv was $i ==> new $pv is $o\n";
}
#
sub getPV {
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  #print "$pv type == $styp, val == $val\n";

  return $val;
}

sub monPV {
  my $ch1 = shift @_;
  my $ch2 = shift @_;
  my $pv1 = UFCA::getPVName($ch1);
  my $pv2 = UFCA::getPVName($ch2);
  UFCA::subscribe($ch1, $to);
  UFCA::subscribe($ch2, $to);
  my $m1 = 0; $m2 = 0;
  while( $m1 == 0 || $m2 == 0 ) {
    $m1 = UFCA::eventMon($ch1); $m2 = UFCA::eventMon($ch2);
    sleep 1;
  }
  $val1 = getPV($ch1);
  $val2 = getPV($ch2);
  print "$pv1 == $val1; $pv2 == $val2\n";
}

sub prompt {
  my $bch = shift @_;
  my $pv = UFCA::getPVName($bch);
  my $pv0 = UFCA::getPVName($b0);
  my $b = UFCA::getString($b0, $to);
#  print "Barcode reading for $pv0 == $b\n";
  my $ac = getPV($applyR); my $anm = UFCA::getPVName($applyR);
  my $sc = getPV($setupR); my $snm = UFCA::getPVName($setupR);
  my $mosc = getPV($mosR); my $mnm = UFCA::getPVName($mosR);
  my $cnt = 0;
  while( $ac != $Idle || $sc != $Idle || $mosc != $Idle ) {
    #if( $ac == $Error || $sc == $Error || $mosc != $Error ) {
    #  print "System has error, exiting...\n"; exit;
    #}
    $b = UFCA::getString($b0, $to);
    print "Barcode reading for $pv0 == $b\n";
    print "System busy ... ";
    #print "$anm == $ac, $snm == $sc, $mnm == $mosc\n";
    if( $mosc != $Idle ) {
      print "Waiting for barcode scan ...\n";
    }
    elsif( $cnt > 10 ) {
      print "$cnt Force Idle? [no]";
      my $idle = getc;
      if( $idle eq "y" ) {
        putPV($applyR, $Idle); putPV($setupR, $Idle); putPV($mosR, $Idle);
      }
    }
    $ac = getPV($applyR);
    $sc = getPV($setupR);
    $mosc = getPV($mosR);
    $cnt++;
    sleep 1;
  }
  #print "$anm == $ac, $snm == $sc, $mnm == $mosc\n";
  $b = UFCA::getString($bch, $to);
  if( $pv ne $pv0 ) {
    print "\nMOSBarCode> Scanned: $pv == $b, type enter to proceed: ";
  }
  else {
    print "\nMOSBarCode> type enter to proceed: ";
  }
  getc;
}

# main:
for( $i = 0; $i < 9; $i++ ) {
  $n = 1 + $i;
  print "$n\. ";
  getPV($barcdisad[$i]);
  getPV($barcdsad[$i]);
}

$val = $Preset;
# init:
setPV($initR, $Idle);
setPV($init, $Preset);
setPV($applyR, $Idle);
setPV($apply, $Start);
#monPV($applyR, $initR);
$pv =  UFCA::getPVName($b0);
print "prompt arg: $pv\n";
prompt($b0);

$datum = UFCA::connectPV("flam:cc:datum.MOS", $to);
$datumR = UFCA::connectPV("flam:cc:datumC", $to);
$dbl = UFCA::connectPV("flam:cc:datum.MOSBacklash", $to);
$ddir = UFCA::connectPV("flam:cc:datum.MOSDirection", $to);
$dvel = UFCA::connectPV("flam:cc:datum.MOSVel", $to);

# datum the MOS wheel:
setPV($datum, $Preset);
setPV($dbl, 0);
setPV($ddir, 0);
setPV($dvel, 100);
setPV($datumR, $Idle);
setPV($applyR, $Idle);
setPV($apply, $Start);
#monPV($applyR, $datumR);
print "prompt arg: $pv\n";
prompt($b0);

# step to each barcode position and process barcode capture:
$acc = UFCA::connectPV("flam:cc:setup.MOSBarCdAcc", $to);
$dec = UFCA::connectPV("flam:cc:setup.MOSBarCdDec", $to);
$bl = UFCA::connectPV("flam:cc:setup.MOSBarCdBacklash", $to);
$ivel = UFCA::connectPV("flam:cc:setup.MOSBarCdInitVel", $to);
$runc = UFCA::connectPV("flam:cc:setup.MOSBarCdRunc", $to);
$vel = UFCA::connectPV("flam:cc:setup.MOSBarCdSlewVel", $to);
$dir = UFCA::connectPV("flam:cc:setup.MOSBarCdDirection", $to);
$step = UFCA::connectPV("flam:cc:setup.MOSBarCdStep", $to);
$sad = UFCA::connectPV("flam:sad:MOSSteps", $to);
setPV($ivel, 100);
setPV($vel, 100);
setPV($acc, 200);
setPV($bl, 0);
setPV($runc, 50);
setPV($dir, "+");

@barcdsteps = (100, 152, 195, 225, 350, 445, 495, 570, 635, 670, 690, 710, 730);
@barcdposnames = ("MOSCIRC1", "MOSCIRC2", "MOSPLT01", "MOSPLT02", "MOSPLT03", "MOSPLT04",
		  "MOSPLT51", "MOSPLT06", "MOSPLT07", "MOSPLT08", "MOSPLT09", "MOSPLT10" );
$prev = 0;
for( $i = 0; $i < 9; $i++ ) {
  $ch = $barcdsad[$i];
  $chi = $barcdisad[$i];
  $bs = $barcdsteps[$i] - $prev;
  $prev = $barcdsteps[$i];
  $pv = UFCA::getPVName($ch);
  print "$i\. Barcode step == $bs for $pv\n";
  setPV($chi, "Waiting for scan");
  setPV($mosR, $Idle);
  setPV($setupR, $Idle);
  setPV($applyR, $Idle);
# indicate steps to barcode position:
  setPV($step, $bs);
  setPV($apply, $Start);
#  monPV($stepR, $sad);
  print "prompt arg: $pv\n";
  prompt($ch);
}

exit;

