#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: cctest.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
# apply/cad directives:
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
# channel connection timeout:
$to = 0.5;
#
$instrum = "flam";
$mech = "all";
#
if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "instrumsetup.pl [-h[elp]] [instrum] [all/or: window decker mos filt1 lyot filt2 grizm focus] \n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $mech = shift; }
}
$apply = UFCA::connectPV("$instrum:eng:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:eng:applyC", $to);
$test = UFCA::connectPV("$instrum:cc:test.DIR", $to);
$testC = UFCA::connectPV("$instrum:cc:testC", $to);
$all = UFCA::connectPV("$instrum:cc:test.All", $to);
$dck = UFCA::connectPV("$instrum:cc:test.Decker", $to);
$sdck = UFCA::connectPV("$instrum:sad:DeckerSteps", $to);
$f1 = UFCA::connectPV("$instrum:cc:test.Filter1", $to);
$sf1 = UFCA::connectPV("$instrum:sad:Filter1Steps", $to);
$f2 = UFCA::connectPV("$instrum:cc:test.Filter2", $to);
$sf2 = UFCA::connectPV("$instrum:sad:Filter2Steps", $to);
$foc = UFCA::connectPV("$instrum:cc:test.Focus", $to);
$sfoc = UFCA::connectPV("$instrum:sad:FocusSteps", $to);
$grsm = UFCA::connectPV("$instrum:cc:test.Grism", $to);
$sgrsm = UFCA::connectPV("$instrum:sad:GrismSteps", $to);
$lyot = UFCA::connectPV("$instrum:cc:test.Lyot", $to);
$slyot = UFCA::connectPV("$instrum:sad:LyotSteps", $to);
$mos = UFCA::connectPV("$instrum:cc:test.MOS", $to);
$smos = UFCA::connectPV("$instrum:sad:MOSSteps", $to);
$win = UFCA::connectPV("$instrum:cc:test.Window", $to);
$swin = UFCA::connectPV("$instrum:sad:WindowSteps", $to);
#$icid = UFCA::connectPV("$instrum:cc:test.icid", $to);
#$mess = UFCA::connectPV("$instrum:cc:test.mess", $to);
#$ocid = UFCA::connectPV("$instrum:cc:test.ocid", $to);
$tmo = UFCA::connectPV("$instrum:cc:test.timeout", $to);
$val = UFCA::connectPV("$instrum:cc:test.val", $to);
#
# check current state of setup CAR:
$c = UFCA::getInt($testC); $pv = UFCA::getPVName($testC); print "$pv == $c\n";
if( $c != $Idle ) { die "CCTest not Idle!\n"; }

# clear:
$p = UFCA::putInt($apply, $Clear);
$rall = rindex $mech, "all";
$rwin = rindex $mech, "win";
$rdck = rindex $mech, "deck";
$rmos = rindex $mech, "mos";
$rf1 = rindex $mech, "f1";
$rlyot = rindex $mech, "lyot";
$rf2 = rindex $mech, "f2";
$rgrsm = rindex $mech, "grism";
$rfoc = rindex $mech, "foc";
if( $rall >= 0 )  { $p = UFCA::putInt($all, $Preset); }
if( $rwin >= 0 )  { $p = UFCA::putInt($win, $Preset); }
if( $rdck >= 0 )  { $p = UFCA::putInt($dck, $Preset); }
if( $rmos >= 0 )  { $p = UFCA::putInt($mos, $Preset); }
if( $rf1 >= 0 )    { $p = UFCA::putInt($f1, $Preset); }
if( $rlyot >= 0 ) { $p = UFCA::putInt($lyot, $Preset); }
if( $rf2 >= 0 )   { $p = UFCA::putInt($f2, $Preset); }
if( $rgrsm >= 0 ) { $p = UFCA::putInt($grsm, $Preset); }
if( $rfoc >= 0 )  { $p = UFCA::putInt($foc, $Preset); }
$p = UFCA::putInt($apply, $Start);

waitCAR($testC);

printCCSAD();

exit;

#################################### subs ############################
sub waitCAR {
  my $car = shift;
  my $c = UFCA::getInt($car);
  my $pv = UFCA::getPVName($car);
  my $ca = UFCA::getInt($applyC);
  my $pva = UFCA::getPVName($applyC);
  my $cnt = 10;
  while(  $c == $Busy && $cnt-- > 0 ) {
    print "waitCAR> $pv: $c, $pva: $ca\n";
    sleep 1;
    $c = UFCA::getInt($car);
    $ca = UFCA::getInt($applyC);
  }
  $cnt = 10;
  while(  $ca == $Busy && $cnt-- > 0 ) {
    print "waitCAR> $pv: $c, $pva: $ca\n";
    sleep 1;
    $c = UFCA::getInt($car);
    $ca = UFCA::getInt($applyC);
  }
  print "waitCAR> $pv: $c, $pva: $ca\n";
}

sub printCCSAD {
  my $s = UFCA::getInt($swin);
  print "StepCnt -- Window: $s, ";
  my $s = UFCA::getInt($sdck);
  print "Decker: $s, ";
  my $s = UFCA::getInt($smos);
  print "MOS: $s, ";
  my $s = UFCA::getInt($sf1);
  print "Filt1: $s, ";
  my $s = UFCA::getInt($slyot);
  print "Lyot: $s, ";
  my $s = UFCA::getInt($sf2);
  print "Filt2: $s, ";
  my $s = UFCA::getInt($sgrsm);
  print "Grism: $s, ";
  my $s = UFCA::getInt($sfoc);
  print "DetFocus: $s\n";
}
