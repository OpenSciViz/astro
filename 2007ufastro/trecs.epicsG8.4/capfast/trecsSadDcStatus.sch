[schematic2]
uniq 64
[tools]
[detail]
w 1960 267 100 0 n#63 esirs.esirs#47.SIML 1984 256 1984 256 junction
w 1987 312 100 2 n#63 hwin.hwin#60.in 1984 256 1984 320 esirs.esirs#47.INP
w 1988 1499 100 2 n#59 hwin.hwin#58.in 1984 1504 1984 1504 esirs.esirs#50.INP
w 1988 1083 100 2 n#57 hwin.hwin#56.in 1984 1088 1984 1088 esirs.esirs#49.INP
w 1988 667 100 2 n#55 hwin.hwin#54.in 1984 672 1984 672 esirs.esirs#48.INP
w 1988 1915 100 2 n#53 hwin.hwin#52.in 1984 1920 1984 1920 esirs.esirs#51.INP
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-Recs DC Status
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/12
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 trecsSadDcStatus.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2000/11/12
[cell use]
use esirs 160 -249 100 0 esirs#62
xform 0 368 -96
p 96 -544 100 0 0 FTVL:LONG
p 352 -448 100 0 0 SNAM:ufSetDatumCnt
p 272 -256 100 1024 1 name:$(top)dcDatumCnt
use esirs 1248 423 100 0 esirs#43
xform 0 1456 576
p 1312 384 100 0 1 SCAN:Passive
p 1408 416 100 1024 -1 name:$(top)dcName
use esirs 1248 839 100 0 esirs#42
xform 0 1456 992
p 1312 800 100 0 1 SCAN:Passive
p 1408 832 100 1024 -1 name:$(top)dcHeartbeat
use esirs 1248 1255 100 0 esirs#41
xform 0 1456 1408
p 1312 1216 100 0 1 SCAN:Passive
p 1408 1248 100 1024 -1 name:$(top)dcHealth
use esirs 544 423 100 0 esirs#40
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)hSetpointReq
use esirs 544 71 100 0 esirs#37
xform 0 752 224
p 608 32 100 0 1 SCAN:Passive
p 704 64 100 1024 -1 name:$(top)detiD
use esirs 1248 1671 100 0 esirs#36
xform 0 1456 1824
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)detType
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)hPowerReq
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)exposedRQ
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)exposed
use esirs -192 71 100 0 esirs#30
xform 0 16 224
p -128 32 100 0 1 SCAN:Passive
p -32 64 100 1024 -1 name:$(top)elapsed
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)dataLabel
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)bunit
use esirs -192 1255 100 0 esirs#27
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)beamswitchHsk
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)bgCheckFlag
use esirs 1248 71 100 0 esirs#46
xform 0 1456 224
p 1312 32 100 0 1 SCAN:Passive
p 1408 64 100 1024 -1 name:$(top)dcState
use esirs 1984 71 100 0 esirs#47
xform 0 2192 224
p 2048 32 100 0 1 SCAN:1 second
p 2160 64 100 1024 -1 name:$(top)chopDutyCycle
use esirs 1984 423 100 0 esirs#48
xform 0 2192 576
p 2048 384 100 0 1 SCAN:1 second
p 2144 416 100 1024 -1 name:$(top)nodDwell
use esirs 1984 839 100 0 esirs#49
xform 0 2192 992
p 2048 800 100 0 1 SCAN:1 second
p 2144 832 100 1024 -1 name:$(top)nodDelay
use esirs 1984 1255 100 0 esirs#50
xform 0 2192 1408
p 2048 1216 100 0 1 SCAN:1 second
p 2144 1248 100 1024 -1 name:$(top)chopDelay
use esirs 1984 1671 100 0 esirs#51
xform 0 2192 1824
p 2048 1632 100 0 1 SCAN:1 second
p 2144 1664 100 1024 -1 name:$(top)chopFreq
use hwin 1792 1879 100 0 hwin#52
xform 0 1888 1920
p 1696 1952 100 0 -1 val(in):$(dc)physOutG.VALD
use hwin 1792 631 100 0 hwin#54
xform 0 1888 672
p 1712 704 100 0 -1 val(in):$(dc)physOutG.VALF
use hwin 1792 1047 100 0 hwin#56
xform 0 1888 1088
p 1712 1120 100 0 -1 val(in):$(dc)physOutG.VALG
use hwin 1792 1463 100 0 hwin#58
xform 0 1888 1504
p 1712 1536 100 0 -1 val(in):$(dc)physOutG.VALE
use hwin 1792 215 100 0 hwin#60
xform 0 1888 256
p 1712 288 100 0 -1 val(in):$(dc)physOutG.VALH
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadDcStatus.sch,v 0.0 2003/04/25 15:21:16 hon Developmental $
