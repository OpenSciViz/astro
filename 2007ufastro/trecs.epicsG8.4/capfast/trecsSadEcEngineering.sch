[schematic2]
uniq 46
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS EC Engineering
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2528 -240 100 1792 trecsSadEcEngineering.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2001/01/25
[cell use]
use esirs 1248 1671 100 0 esirs#40
xform 0 1456 1824
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)tSStrap
use esirs 1248 1255 100 0 esirs#39
xform 0 1456 1408
p 1312 1216 100 0 1 SCAN:Passive
p 1392 1248 100 1024 -1 name:$(top)tSEdge
use esirs 1248 839 100 0 esirs#38
xform 0 1456 992
p 1312 800 100 0 1 SCAN:Passive
p 1392 832 100 1024 -1 name:$(top)tSMiddle
use esirs 544 407 100 0 esirs#35
xform 0 752 560
p 608 368 100 0 1 SCAN:Passive
p 704 400 100 1024 -1 name:$(top)cryostatPressure
use esirs 544 823 100 0 esirs#34
xform 0 752 976
p 608 784 100 0 1 SCAN:Passive
p 704 816 100 1024 -1 name:$(top)tSWindow
use esirs 544 1239 100 0 esirs#33
xform 0 752 1392
p 608 1200 100 0 1 SCAN:Passive
p 752 1232 100 1024 -1 name:$(top)tSPassiveShield
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)tSFinger
use esirs -192 423 100 0 esirs#30
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -16 416 100 1024 -1 name:$(top)tSDetector
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -16 832 100 1024 -1 name:$(top)tSColdhead2
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -16 1248 100 1024 -1 name:$(top)tSColdhead1
use esirs -192 1671 100 0 esirs#27
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p 0 1664 100 1024 -1 name:$(top)tSActiveShield
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadEcEngineering.sch,v 0.0 2003/04/25 15:21:16 hon Developmental $
