[schematic2]
uniq 49
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS CC Status
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 trecsSadCcStatus.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2000/11/12
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Removed EC Records
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2001/01/25
[cell use]
use esirs 352 1671 100 0 esirs#48
xform 0 560 1824
p 288 1376 100 0 0 FTVL:LONG
p 544 1472 100 0 0 SNAM:ufSetDatumCnt
p 464 1664 100 256 1 name:$(top)ccDatumCnt
use esirs -192 1655 100 0 esirs#40
xform 0 16 1808
p -128 1616 100 0 1 SCAN:Passive
p -32 1648 100 1024 -1 name:$(top)ccHealth
use esirs -192 1239 100 0 esirs#39
xform 0 16 1392
p -128 1200 100 0 1 SCAN:Passive
p -32 1232 100 1024 -1 name:$(top)ccHeartbeat
use esirs -192 823 100 0 esirs#38
xform 0 16 976
p -128 784 100 0 1 SCAN:Passive
p -32 816 100 1024 -1 name:$(top)ccName
use esirs -192 407 100 0 esirs#37
xform 0 16 560
p -128 368 100 0 1 SCAN:Passive
p -32 400 100 1024 -1 name:$(top)ccState
use esirs -192 -9 100 0 esirs#36
xform 0 16 144
p -128 -48 100 0 1 SCAN:Passive
p -32 -16 100 1024 -1 name:$(top)wfsBeam
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#47
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadCcStatus.sch,v 0.0 2003/04/25 15:21:15 hon Developmental $
