;Motor Parameter File
; $Name:  $ $Id: mot_param.txt 14 2008-06-11 01:49:45Z hon $
;cc agent host IP number
000.000.000.000
;cc agent port number
52005
;Number of Motors
9
;# IS  TS  A   D   HC DC AN HS  FHS HD  BL  EN       Name
 1 40  80  10  10  0 15  A  80  40  1   10  sectWhl  Sector Wheel
 2 40  40  255 255 0 100 B  80  20  1   40  winChngr Window Changer
 3 40  80  10  10  0 15  C  80  40  0  -30  aprtrWhl Aperture Wheel
 4 40  80  10  10  0 15  D  80  40  0  -10  fltrWhl1 Filter Wheel 1
 5 40  80  10  10  0 15  E  80  40  1   20  lyotWhl  Lyot Wheel
 6 40  80  10  10  0 15  F  80  40  1   20  fltrWhl2 Filter Wheel 2
 7 40  80  10  10  0 30  G  80  40  1   40  pplImg   Pupil Imaging Wheel
 8 40  200 100 100 0 15  H  200 40  1  100  slitWhl  Slit Wheel
 9 20  40  255 255 0 30  I  40  40  1  500  grating  Grating Wheel
;
;#  : Motor Number
;IS : Initial Speed
;TS : Terminal Speed
;A  : Acceleration
;D  : Deceleration
;HC : Hold Current
;DC : Drive Current
;AN : Axis Name (one character)
;HS : Homing Speed
;FHS: Final Homing Speed
;HD : Homing Direction 
;BL : Back-Lash correction param.
;EN : Epics Name
;Name : Motor name
;
; NOTE: There can be any number of comments between the above params
;       and the following positions,
;       but a line with keyword positions (in capitals)
;	must appear once (and only once in file) just before first ;Mot_# line.
;
;POSITIONS:
;Mot_#   #_Positions for Sector Wheel
1  5 sectWhl
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 268     1.00         2.0     28.0      Poly_115
 2 556     1.00         2.0     28.0      Black_Plate
 3 841     1.00         2.0     28.0      Open
 4 1126    0.60         2.0     28.0      Poly_105
 5 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Window Changer
2  6 winChngr
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 0       0.50         2.0     28.0      Block
 2 1289    0.97         0.5     18.3      KRS-5
 3 2578    0.95         0.2     27.5      ZnSe
 4 3862    0.70         0.6     50.0      KBr (uncoated)
 5 5150    0.95         0.2     27.5      KBrC (coated)
 6 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Aperture Wheel
3  6 aprtrWhl
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 21      1.00         2.0     28.0      Grid_Mask
 2 167     1.00         2.0     28.0      Matched
 3 313     1.00         2.0     28.0      Oversized
 4 460     0.97         0.5     18.3      Window_Imager
 5 607     1.00         2.0     28.0      Spot_Mask
 6 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Filter Wheel 1
4  14 fltrWhl1
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 -72     1.00         2.0     28.0      Open
 2 15      0.71        17.60    25.38     Qw-20.8um
 3 102     0.95         7.39     8.08     Si1-7.9um
 4 189     0.77         8.39     8.81     PAH1-8.6um
 5 276     0.95         8.35     9.13     Si2-8.8um
 6 363     1.00         2.0     28.0      Block
 7 451     0.92         9.22    10.15     Si3-9.7um
 8 538     0.83        11.00    11.60     PAH2-11.3um
 9 625     0.97         9.87    10.89     Si4-10.4um
10 712     0.68         8.93     9.07     ArIII-9.0um
11 799     0.88        11.09    12.22     Si5-11.7um
12 886     0.86        11.74    12.92     Si6-12.3um
13 973     1.00         2.0     28.0      Align-Spot
14 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Lyot Wheel
5  14 lyotWhl
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 809     1.00         2.0     28.0      Grid_Mask
 2 896     1.00         2.0     28.0      Spot_Mask
 3 982     1.00         2.0     28.0      Ciardi
 4 1069    0.97         2.0     28.0      Open
 5 1156    1.00         2.0     28.0      Quakham_Mask
 6 1243    0.60         2.0     28.0      Polystyrene
 7 1331    0.96         2.0     28.0      Circ-2
 8 1418    0.94         2.0     28.0      Circ-4
 9 1506    1.00         2.0     28.0      Circ+2
10 1592    1.03         2.0     28.0      Circ+4
11 1680    1.00         2.0     28.0      Circ+6
12 1767    1.00         2.0     28.0      Circ+8
13 1849    1.00         2.0     28.0      Block
14 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Filter Wheel 2
6  14 fltrWhl2
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 530     1.00         2.0     28.0      Open
 2 1574    0.85         1.98     2.39     K
 3 1487    0.97         3.56     4.14     L
 4 1400    0.94         4.39     4.97     M
 5 1313    0.59        12.71    12.91     NeII-12.8um
 6 1226    0.63        13.00    13.20     NeII_ref2-13.1um
 7 1139    0.68        10.44    10.60     SIV-10.5um
 8 1052    0.64        17.61    19.11     Qs-18.3um
 9 965     0.65        17.20    18.16     Qone-17.8um
10 878     0.64        23.59    25.55     Ql-24.5um
11 791     0.85         7.74    13.03     N
12 704     1.00         2.0     28.0      Block
13 617     1.00         2.0     28.0      Align-Spot
14  0      1.00         2.0     28.0      Datum
;Mot_#   #_Positions Pupil Imaging Wheel
7  5 pplImg
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 35      1.00         2.0     28.0      Open-1
 2 249     0.97         0.5     18.3      Pupil_Imager
 3 462     1.00         2.0     28.0      Open-2
 4 675     1.00         2.0     28.0      Open-3
 5 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions for Slit Wheel
8  9 slitWhl
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 -2316   1.00         2.0     28.0      Open
 2 195     1.00         2.0     28.0      1.32 (")
 3 2661    1.00         2.0     28.0      0.72 (")
 4 5189    1.00         2.0     28.0      0.66 (")
 5 7662    1.00         2.0     28.0      0.36 (")
 6 10190   1.00         2.0     28.0      0.31 (")
 7 12634   1.00         2.0     28.0      0.26 (")
 8 15104   1.00         2.0     28.0      0.21 (")
 9 0       1.00         2.0     28.0      Datum
;Mot_#   #_Positions Grating Wheel
9  7 grating
;# Offset  Throughput  LambdaLo LambdaHi  Name (Comment)
 1 -1006   0.99         2.0     28.0      Mirror
 2 1216    0.90         9.70    9.80      LowRes-10
 3 2392    0.99         2.0     28.0      LR_Ref_Mirror
 4 3519    0.90        19.57    19.83     LowRes-20
 5 5716    0.90        10.04    10.05     HighRes-10
 6 6972    0.99         2.0     28.0      HR_Ref_Mirror
 7 0       1.00         2.0     28.0      Datum (@ 0)
