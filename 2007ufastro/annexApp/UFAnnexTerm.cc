#if !defined(__UFAnnexTerm_cc__)
#define __UFAnnexTerm_cc__ "$Name:  $ $Id: UFAnnexTerm.cc 14 2008-06-11 01:49:45Z hon $";
const char rcsId[] = __UFAnnexTerm_cc__;

#include "pthread.h"
#include "unistd.h"
#include "stdio.h"
#include "fcntl.h"
#include "termio.h"
#include "sys/time.h"

#include "iostream"
#include "string"

#include "UFPosixRuntime.h"
#include "UFClientSocket.h"

static UFClientSocket annex;
static pthread_mutex_t annexRM, annexSM;

static unsigned char inputAnnex[1024*1024]; /* read buff == socket buffer max size? */
static bool newAnnexOutput = false;
static unsigned char outputAnnex = 0; /* send out each character as it's typed */

static void setNewAnnexOutput(char c) {
  pthread_mutex_lock(&annexSM); // prevent user input until char is read from terminal
  outputAnnex = c;
  newAnnexOutput = true; // reset flag
  pthread_mutex_unlock(&annexSM);
}

// recv from annex socket & display text
static void* annexRecvThread(void* p) {
  int termout = 1; // standard output file descriptor == 1
  struct termio _orig, _outraw;
  ioctl( termout, TCGETA , &_orig);
  _outraw = _orig;
  _outraw.c_cflag = CLOCAL | CREAD;
  _outraw.c_lflag &= ~ICANON;
  _outraw.c_lflag &= ~ECHO; 
  _outraw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  _outraw.c_cc[VTIME] = 0; // block if nothing is available (infinite timeout)
  ioctl( termout, TCSETAF, &_outraw ); // set termout to raw

  int nb = 0;
  clog << "annexRecvThread> started..."<<endl;
  while( (nb = annex.available()) >= 0 ) {
    //clog << "annexRecvThread> nb= "<<nb<<endl;
    if( nb > 0 ) {
      memset(inputAnnex, 0, sizeof(inputAnnex));
      nb = annex.recv(inputAnnex, nb);
      if(nb <= 0 ) {
	clog<<"annexRecvThread> socket recv error? exit..."<<endl;
	exit(1);
      }
      //clog<<"annexRecvThread> socket recv nb= "<<nb<<endl;
      write(termout, inputAnnex, nb);
    }
    else
      UFPosixRuntime::yield(); 
  }
  return p;
}

static void* annexSendThread(void* p) {
  int nb = 0;
  clog << "annexSendThread> started..."<<endl;
  while( annex.writable() > 0 ) {
    if( newAnnexOutput ) { // main thread read new user input and should have set (annex) newoutput flag
      pthread_mutex_lock(&annexSM); // prevent user input until char is read from terminal
      nb = annex.send(&outputAnnex, 1);
      newAnnexOutput = false; // reset flag
      pthread_mutex_unlock(&annexSM);
    }
    UFPosixRuntime::yield();
  }
  return p;
}

int readable(int fd) {
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(fd, &readfds);
 
  struct timeval poll = { 0, 0 }; // don't block

  int fdcnt;
  do {
    struct timeval to = poll; // since timeval may be modified by call
    errno = 0;
    fdcnt = select(FD_SETSIZE, &readfds, 0, 0, &to);
  } while( errno == EINTR );

  if( fdcnt <= 0 ) {
    return 0;
  }
  return FD_ISSET(fd, &readfds);
}

int main(int argc, char** argv) {
  string host("192.168.111.101");
  int port = 7008;
  if( argc > 1 ) port = atoi(argv[1]);

  int sfd = annex.connect(host, port);

  if( sfd <= 0 ) {
    clog << "annexterm> failed to connect to: "<<host<<":"<<port<<endl;
    exit(sfd);
  }
  else
    clog << "annexterm> connected to: "<<host<<" : "<<port<<endl;

  int termin = 0; // standard input file descriptor == 0
  struct termio _orig, _inraw;
  ioctl(termin, TCGETA , &_orig);
  _inraw = _orig;
  _inraw.c_cflag = CLOCAL | CREAD;
  _inraw.c_lflag &= ~ICANON;
  _inraw.c_lflag &= ~ECHO; 
  _inraw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  _inraw.c_cc[VTIME] = 0; // block if nothing is available (infinite timeout)
  ioctl( termin, TCSETAF, &_inraw ); // set termin to raw
 
  pthread_mutex_init(&annexRM, 0);
  pthread_mutex_init(&annexSM, 0);

  pthread_t ar = UFPosixRuntime::newThread(annexRecvThread, (void*) 0);

  if( ar <= 0 ) {
    clog << "failed to create read thread"<<endl;
    exit(1);
  }
  UFPosixRuntime::yield();
    
  pthread_t aw = UFPosixRuntime::newThread(annexSendThread, (void*) 0);
  if( aw <= 0 ) {
    clog << "failed to create read thread"<<endl;
    exit(2);
  }
  UFPosixRuntime::yield();
  char nl = 10;
  char cr = 13;

  while( true ) {
    // if there is new input from the terminal, fetch it and notify the annexWrite thread
    char c = (char) getchar();
    if( c == nl ) c = cr; // force any newline inot a carriage return
    setNewAnnexOutput(c); // let annexWrite know about the new chart
    UFPosixRuntime::yield();
  }
  return 0;
}

#endif //  __UFAnnexTerm_cc__   
      
