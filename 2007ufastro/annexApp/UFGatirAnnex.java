import java.io.*;
import java.util.*;
import java.net.*;

public class UFGatirAnnex implements Runnable {
  public static final String _GatirEOT_ = ">>";
  public DataOutputStream dout = null;
  public DataInputStream din = null;

  // ctor
  public UFGatirAnnex(String annexIP, int port) {
    try {
    Socket s = new Socket(annexIP, port);
      dout = new DataOutputStream(s.getOutputStream());
      System.out.println("Connected to " + annexIP + " on port " + port);
      din = new DataInputStream(s.getInputStream());
    } // end of socket i/o try
    catch (IOException e) {
      System.err.println("socket i/o error " + e);
    }
    finally {
      if ( din == null || dout == null ) {
        System.err.println("No Annex connection, abort!");
        System.exit(-1);
      }
    }
  } // ctor

  // Runnable interface method override
  public void run() {
    // first check for (boot/command) prompt
    BufferedReader userInp = new BufferedReader(new InputStreamReader(System.in));
    String cmd = "Error";
    boolean booted = false;
    String prompt = "> "; 
    while (true) {
      // get user cmd input
      if( !booted ) {
	prompt = "UFGatirAnnex>To check for boot prompt, enter b/boot (otherwise, enter command): "; 
        System.err.print(prompt); 
      }
      try {
        cmd = userInp.readLine(); // read in user command from stdin
      }
      catch (IOException e) {
        System.err.println("user input error " + e);
	  System.exit(-2);
      }
      //System.err.println("accepted cmd= :"+cmd+": ...");
      if( cmd.equals("b") || cmd.equals("boot") ) {
	prompt = recvGatir();
        if( prompt == null || prompt.length() == 0 || prompt.equals("Error") )
	  booted = false;
	else
	  booted = true;  
      }
      else // assume booted?
	booted = true;

      if( booted ) {
        int nb = sendGatir(cmd);
        prompt = recvGatir();
        System.err.print(prompt);
        String nextpage = prompt;
        if( prompt == null || prompt == "Error" || prompt.length() == 0 ) {
          System.err.println("UFGatirAnnex::run> ? no prompt...\n");
          while( nextpage.indexOf("Hit") >= 0 || nextpage.indexOf("hit") >= 0 &&
	       nextpage.indexOf("Continue") >= 0 || nextpage.indexOf("continue") >= 0 ) {
	    // this is a special case where we need to send a char back to complete the input
	    byte b = 33; // "!" ASCII
	    sendGatir1(b); // anything will do?
            nextpage = recvGatir();
            System.err.print(nextpage); 
          }
	}
      }
    } // end while
  } // run

  public void start() {
    new Thread(this).start(); // start the run() method
  }

  public String recvGatir() {
    //System.err.println("recvGatir> read available input, until _GatirEOT_ is received...");
    String gatir_prompt = "";
    byte[] eot = new byte[2]; eot[0] = eot[1] = 32;
    String gatirEOT = new String(eot);
    int charcnt;
    try {
      do {
	charcnt = din.available();
	// 1 sec sleep should allow socket to recv more input
	if( charcnt == 0 ) {
	  //System.err.println("recvGatir> No available input, sleep awhile & iterate");
	  Thread.sleep(1000);
	}
	else { // always read just 1 byte
	  // byte[] binp = new byte[charcnt];
	  byte[] binp = new byte[1];
	  din.read(binp);
	  /*
          if( charcnt >= 2 ) 
	    eot[0] = binp[binp.length-2];
	  else
	    eot[0] = eot[1];
          eot[1] = binp[binp.length-1] ;
	  */
          eot[0] = eot[1];
          eot[1] = binp[0];
	  // append
          gatir_prompt += new String(binp);
	  //System.err.println("recvGatir> received: "+gatir_prompt);
	  // check for termination
          gatirEOT = new String(eot);
	  //System.err.println("recvGatir> gatirEOT= :"+gatirEOT+":");
	}
      } while( !gatirEOT.equals(_GatirEOT_) );
    }
    catch (IOException e) {
      System.err.println("socket i/o error " + e);
    }
    catch (Throwable e) {
      System.err.println("I can't go to sleep!");
    }
    System.err.println("recvGatir> "+gatir_prompt);
    return gatir_prompt;
  }

  // this always appends kevin's CR transmission terminator
  public int sendGatir(String cmd) {
    int charcnt = cmd.length();
    //System.err.println("Send cmd: "+cmd+" charcnt= "+charcnt);
    byte[] ch = cmd.getBytes();
    byte cr = 13;
    int i = 0;
    try {
      while( charcnt-- > 0 ) {
        dout.writeByte(ch[i++]);
      }
      dout.writeByte(cr); // make sure to send termination
    }
    catch (IOException e) {
      System.err.println("socket i/o error " + e);
    }
    int n = cmd.length() - charcnt;
    //System.err.println("sendGatir> wrote "+n+" char of cmd: "+cmd);
    return n;
  }

  // this explicitly send one byte
  public void sendGatir1(byte b) {
    //System.err.println("Send 1 byte: "+b);
    try {
      dout.writeByte(b); // nomatter what it is
    }
    catch (IOException e) {
      System.err.println("socket i/o error " + e);
    }
  }

  // run as an application
  public static void main(String[] args) {
    String annexPrivateIP = "192.168.111.101";
    int port = 7007;
    if( args.length > 0 ) {
      Integer portNo = Integer.valueOf(args[0]);
      port = portNo.intValue();
    }
    UFGatirAnnex g = new UFGatirAnnex(annexPrivateIP, port);
    g.start();
  } // end of main
}  // end of class annex
