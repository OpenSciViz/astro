#if !defined(__ufbootc_cc__)
#define __ufbootc_cc__ "$Name:  $ $Id: ufbootc.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufbootc_cc__;

#include "UFClientSocket.h"
__UFSocket_H__(ufbootc_cc);

#include "UFTimeStamp.h"
__UFTimeStamp_H__(ufbootc_cc);

#include "UFRuntime.h"
__UFRuntime_H__(ufbootc_cc);

#include "cstdio"
#include "cstring"
const bool _verbose = true;

int main(int argc, char** argv, char** envp) {
  // unit test of (x)inetd tcp-mux server
  // /usr/local/sbin/{ufbootc,ufstartup}
  // preumably this is exec'd by the (x)inetd listening on port 3720
  // and so it is expected to associate file desc. 0, 1, and 2 with
  // client connection:
  string host =  UFRuntime::hostname();
  string clname= "UFFLAMBoot@" + host;
  UFClientSocket clsoc;
  if( clsoc.connect(host, 52222, false) < 0 ) {
    clog<<"ufbootc> failed to connect to ufinetboot"<<endl;
    return 1;
  }
  clog<<"ufbootc> connected to ufinetboot: "<<clsoc.description()<<endl;
  // client connected:
  int ns = clsoc.send(clname); // send boot request
  if( ns <= 0 ) {
    clog<<"ufbootc> failed to send boot request."<<endl;
    clsoc.close();
    return 2;
  }
  else {
    clog<<"ufbootc> sent boot request, ns= "<<ns<<", "<<clname<<endl;
  }
  // expect single string reply:
  string reply;
  int nr = clsoc.recv(reply);
  if( nr <= 0 ) {
    clog<<"ufbootc> failed to recv boot request reply"<<endl;
    clsoc.close();
    return 3;
  }
  clog<<"ufbootc> boot reply, nr: "<<nr<<" -- "<<reply<<endl;
  clsoc.close();
  return 0;
}
#endif // __ufbootc_cc__
