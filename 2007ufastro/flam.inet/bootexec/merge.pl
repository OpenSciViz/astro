#!/bin/perl -w
$rscId = '$Name:  $ $Id: merge.pl 14 2008-06-11 01:49:45Z hon $';
$flam2file = "flam2.fits";
$dhsfile = "dhsflam2.fits";
open D, "<$dhsfile" or die "unable to open $dhsfile\n";
open T, "<$flam2file" or die "unable to open $flam2file\n";
$cnt = 0;
while ( <D> ) {
  $cnt++;
  $dhsline = $_;
  chomp $dhsline;
  $flam2line = <T>;
  if( !defined($flam2line) ) { $flam2line = " ";  }
  chomp $flam2line;
  if( $cnt < 10 ) { print "00"; }
  elsif( $cnt < 100 ) { print "0"; }
  if( length($dhsline) > 3 ) { print "$cnt $dhsline \t $flam2line\n"; }
  else { print "$cnt $dhsline    \t $flam2line\n"; }
}
close D;
close T;
exit;
