#!/bin/tcsh -f
#set rcsId = "$Name:  $ $Id: ufsimagents,v 0.24 2006/02/23 21:14:55 drashkin Exp $"
#
set argc = `echo $argv | /usr/bin/wc -w`
# indicate whether or not to bring up the (non vme) portable cas epics db:
set pcasdb = "no"
# indicate whether or not to bring up the instrument executive agent:
set executive = "no"
foreach i ( $argv )
  if( "$i" == "-nodb" || "$i" == "-vme" ) set pcasdb = "no"
  if( "$i" == "-all" || "$i" == "-ex" ) set executive = "yes"
end
set instrum = flam
set dhssys = `hostname`
mkdir -p /data/${instrum} >& /dev/null
#
# note that this script should be used by the FLAM Executive daemon (ufg${instrum}) for starting/stopping, i.e.
# performing "soft reboots" of the system
# start a specific agent via first cmd line parameter: "ufsimagents ufagentname ...usual params..."
# start all agents, including ${instrum} executive via cmd line parameter: "ufsimagents -all ...usual params..."
if( ! $?UFINSTALL ) setenv UFINSTALL /share/local/uf
#
source $UFINSTALL/.ufcshrc
# linux and cygwin static binaries work, but dynamic/shared do not:
setenv UFBIN ${UFINSTALL}/bin
set procls = '/bin/ps -ef'
set osname = `/bin/uname | cut -d'_' -f1`
if( "$osname" == "Linux" ) then
  setenv UFBIN ${UFINSTALL}/sbin
  set procls = '/bin/ps -efw'
endif
#
set slp = 1
# just list agents this can start:
if( "$1" == "-h" || "$1" == "-help" ) then
  echo '"ufsimagents [-all]" -- starts all agents with default runtime options'
  echo '"ufsimagents -l/ls/list" -- list installed agents'
#  echo '"ufsimagents -ex" -- start all agents except the executive'
  echo '"ufsimagents ufagentname" -- start only specified agent'
  exit
endif
if( "$1" == "-l" || "$1" == "-ls" || "$1" == "-list" ) then
  \ls -lqF $UFBIN/ufflam2epicsd
  \ls -lqF $UFBIN/ufgedtd
  \ls -lqF $UFBIN/ufdhsput
  \ls -lqF $UFBIN/ufgls218d
  \ls -lqF $UFBIN/ufgls33xd
  \ls -lqF $UFBIN/ufgpf26xvacd
  \ls -lqF $UFBIN/ufgmotord
  \ls -lqF $UFBIN/ufgsymbarcd
  \ls -lqF $UFBIN/ufglvdtd
  \ls -lqF $UFBIN/ufgisplcd
  \ls -lqF $UFBIN/ufgmce4d
  \ls -lqF $UFBIN/ufg${instrum}2d
  exit
endif
set epics = "-epics $instrum"
if( "$1" == "-noepics" ) then
  set epics = "-noepics"
endif
set jd = `date "+%Y:%j:%H:%M"`
set agent = "unknown"
set params = ""
# presumably all agent/daeomn names start with uf and end with d:
if( "$1" == "ufflam2epicsd" ) then 
  set agent = ${UFBIN}/"$1"
  set params = "$argv[2-]"
endif
if( "$1" == "ufgedtd" ) then 
  mkdir -p /data/${instrum}
  set agent = ${UFBIN}/"$1"
  set params = "$argv[2-]"
endif
if( "$1" == "ufgls218d" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7002 $argv[2-]"
endif
if( "$1" == "ufgls33xd" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7003 $argv[2-]"
endif
if( "$1" == "ufgpf26xvacd" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7004 $argv[2-]"
endif
if( "$1" == "ufgmotord" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7024 $argv[2-]"
#  set params = " $argv[2-]"
endif
if( "$1" == "ufgsymbarcd") then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7005 $argv[2-]"
endif
if( "$1" == "ufglvdtd" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7006 $argv[2-]"
endif
if( "$1" == "ufgisplcd" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7007 $argv[2-]"
endif
if( "$1" == "ufdhsput" ) then 
  $UFBIN/dhsClientBoot start
  set agent = ${UFBIN}/"$1"
  set params = -dhs ${dhssys} "$argv[2-]"
endif
if( "$1" == "ufgmce4d" ) then
  set agent = ${UFBIN}/"$1"
  set params = "-tshost ${instrum}perle -tsport 7008 $argv[2-]"
endif
#echo $agent $params
# is there is no leading dash, assume this is not an option, rather
# just a typo or mispelled arg
set arg = `echo $1 | /bin/egrep -v '\-'`
if( "$agent" == "unknown" && "$arg" != "" ) then
  echo Sorry, "$1" '(' "$arg" ')' is "$agent" and/or ambiguous...
  exit
endif
pgrep -l $agent:t
if( $status == 0 ) then
  echo Sorry, "$agent" is already up, please use ufstop first...
  exit
endif
if( -x $agent ) then
  $agent $epics $params >& /usr/tmp/$1.${jd} &
  sleep 1
  echo UF agents currently running:
  $procls | /bin/grep $UFINSTALL | /bin/grep uf | /bin/grep d | /bin/grep -v jei
  exit
else 
  if( "$agent" != "unknown" ) then
    echo Sorry, "$agent" is not executable...
    exit
  endif
endif
# if we get here start all or all but executive
# presumably all agent/daeomn names start with uf and end with d:
if( -e /tmp/.ufSimAgents ) then
  echo Sorry, lockfile /tmp/.ufSimAgents already exists, please use ufstop first...
  exit
endif
touch /tmp/.ufSimAgents
pgrep caRepeater
if( $status != 0 ) then
  echo start caRepeater
  set log = /usr/tmp/caRepeater.${jd}
  caRepeater >& $log &
endif
if( "$pcasdb" == "no" ) echo 'assuming epics db is (external/vme) or not desired...'
if( "$pcasdb" == "yes" ) then
# bring up the epics portable cas db (instrum. seq. and sad and eng. db):
  echo start ufflam2epicsd
  set log = /usr/tmp/ufflam2epicsd.${jd}
  $UFBIN/ufflam2epicsd $argv[1-] >& $log &
# give epics runtime a chance to init itself before starting all other agents
  set status = 1
  while ( $status != 0 ) 
    /bin/grep started $log >& /dev/null
    sleep $slp
  end
endif
# frame acquisition daemon (in sim mode, ignore the EDT card):
set log = /usr/tmp/ufgedtd.${jd}
#echo start ufgedtd -v $epics $argv[1-] 
#$UFBIN/ufgedtd -v $epics >& $log &
echo start ufgedtd -sim -v $epics $argv[1-] 
$UFBIN/ufgedtd -sim -v $epics $argv[1-] >& $log &
set status = 1
while ( $status != 0 ) 
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# lakeshore 332 (array temp. controller) daemon:
echo start ufgls33xd -sim $epics $argv[1-]
set log = /usr/tmp/ufgls33xd.${jd}
$UFBIN/ufgls33xd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7003 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# lakeshore 218 daemon:
echo start ufgls218d -sim $epics $argv[1-]
set log = /usr/tmp/ufgls218d.${jd}
$UFBIN/ufgls218d -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7002 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# pfeiffer vacuum monitor daemon:
echo start ufgpf26xvacd -sim $epics $argv[1-]
set log = /usr/tmp/ufgpf26xvacd.${jd}
$UFBIN/ufgpf26xvacd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7004 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# potescap motor indexor daemon:
echo start ufgmotord -sim $epics $argv[1-]
set log = /usr/tmp/ufgmotord.${jd}
#$UFBIN/ufgmotord -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7024 -motors 'A:7024 B:7023 C:7022 D:7021 E:7020 F:7019 G:7018' >& $log &
$UFBIN/ufgmotord -sim $epics $argv[1-] >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# symbol barcode daemon:
echo start ufgsymbarcd -sim $epics $argv[1-]
set log = /usr/tmp/ufgsymbarcd.${jd}
$UFBIN/ufgsymbarcd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7005 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# schaevitz lvdt daemon:
echo start ufglvdtd -sim $epics $argv[1-]
set log = /usr/tmp/ufglvdtd.${jd}
$UFBIN/ufglvdtd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7006 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
# automation plc (gis) daemon:
echo start ufgisplcd -sim $epics $argv[1-]
set log = /usr/tmp/ufgisplcd.${jd}
$UFBIN/ufgisplcd -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7007 >& $log &
#
# detector controller (mce4) daemon:
# this should come up after all other device agents because it does a FITS connect (to all other device agents) on startup:
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end
echo start ufgmce4d -sim $epics $argv[1-]
set log = /usr/tmp/ufgmce4d.${jd}
$UFBIN/ufgmce4d -sim $epics $argv[1-] -tshost ${instrum}perle -tsport 7008 >& $log &
set status = 1
while ( $status != 0 )
  /bin/grep listen $log >& /dev/null
  sleep $slp
end

#run temperature logging script
echo start temperature logger
$UFBIN/../scripts/starttemperatureloggers.pl $epics
#run script to initialize epics db values
echo start epics db initialization script
$UFBIN/../scripts/epicsstartupvalues.pl $epics

# optionally start the ${instrum} executive:
# note that the ${instrum} executive communicates with the baytech rpc-3
# and handles requests for powerOn-Off of each/all instrument hardware component(s)
# so it is responsible for turning the cryo-cooler "coldhead" on or off:
set log = /usr/tmp/ufg${instrum}2d.${jd}
#p/bin/grep -lf ufg${instrum}2d
#if( $status != 0 ) then
#  $UFBIN/ufg${instrum}2d $epics $argv[1-] -baytech irbaytech -tsport 7001 >& $log &
#endif
#set status = 1
#while ( $status != 0 )
#  /bin/grep listen $log >& /dev/null
#  sleep $slp
#end
#
# gemini dhs client daemon is last -- it should connect to ufgedtd as replication client
# and it also connects to all device agents for fits (nod-sad route)
$UFBIN/dhsClientBoot start
sleep $slp
#echo start ufdhsput -dhs ${dhssys} $argv[2-] \(ufgdhsd\)
echo start ufdhsput \(ufgdhsd\)
set log = /usr/tmp/ufdhsput.${jd}
#$UFBIN/ufdhsput $epics -dhs ${dhssys} $argv[2-] >& $log &
#$UFBIN/ufdhsput -dhs ${dhssys} $argv[2-] >& $log &
$UFBIN/ufdhsput >& $log &
#
echo UF agents started:
$procls | /bin/grep $UFINSTALL | /bin/grep uf | /bin/grep d | /bin/grep -v jei
# purge older files from local archive, since this can take a while, do it last:
#ufpurge /data/${instrum}
exit
