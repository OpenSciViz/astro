const char fitsF2Prm[] = "SIMPLE  =                    T / Fits standard
BITPIX  =                   32 / Bits per pixel
NAXIS   =                    2 / Number of axes
NAXIS   =                    0 / number of data axes
EXTEND  =                    T / FITS dataset may contain extensions
COMMENT   Data Information
COMMENT   --------------------------------------------------------------------
FILENAME= '02nov08.0058.fits'  / Filename of original file up to 69 characters
OBSERVAT= 'Gemini-South'       / Observatory where data taken
OBSID   = 'GS-2002B-DS-1-52'   / Observation ID
TELESCOP= 'Gemini-South'       / Telescope where data taken
COMMENT   
COMMENT   TCS info follows that we want in the header
COMMENT
COMMENT   TCS Keywords with Numeric Keyvalues
COMMENT   -------------------------------------------------------------------
AIRMASS =       0.000000000000 / Airmass of Object at start of exposure
TELFOCUS=       0.000000000000 / Focus Position Absolute (mm) at start
HA      =       0.000000000000 / Hour Angle at start of exposure
HUMID   =       0.000000000000 / Humidity (% rel) at start of exposure
INSTRAA =       0.000000000000 / Instrument alignment angle (deg)
INSTRPA =       0.000000000000 / Instrument PA (deg)
MJD     =       0.000000000000 / Modified Julian Date
DEC_OFF =       0.000000000000 / Offset in Dec from base (arcsec)
RA_OFF  =       0.000000000000 / Offset in RA from base  (arcsec)
ROTATOR =       0.000000000000 / Rotator mechanical angle (deg)
ROTERROR=       0.000000000000 / Rotator mechanical angle error (deg)
RA_BASE =       0.000000000000 / Target's Base RA  (deg)
DEC_BASE=       0.000000000000 / Target's Base DEC (deg)
EPOCH   =       0.000000000000 / Target's Epoch (years)
EQUINOX =       0.000000000000 / Target's equinox (years)
USRFOC  =       0.000000000000 / User's offset from nominal focus (mm)
ZD      =       0.000000000000 / Zenith Distance at start of exposure (deg)
COMMENT
COMMENT   TCS Keywords with String Keyvalues
COMMENT   ------------------------------------------------------------------
AZIMUTH = '00:00:00'           / Azimuth (deg:min:sec) at start
AZERROR = '00:00:00'           / Azimuth error (deg:min:sec) at start
BEAM    = 'A       '           / Nod beam (A | B | C)
EL      = '90:00:00'           / Elevation (deg:min:sec) at start
ELERROR = '00:00:00'           / Elevation error (deg:min:sec) at start
FRAMEPA = 'Unavailable'        / Reference frame of instrument PA
LOCTIME = '00:00:00.0'         / Local time (hh:mm:ss.s)
LST     = '00:00:00.0'         / Siderial Time at start of observation
OBJECT  = 'CDF-8s  '           / Name of the object observed
TARGFRM = 'FK5     '           / Target's coord frame (FK4;FK5;AAPT;AZEL_TOPO)
TARGSYS = 'FK5     '           / Target's FITS coord frame (FK4;FK5;GAPPT)
RA_TEL  = '00:00:00.00'        / Telescope's RA  pointing origin (hh:mm:ss.ss)
DEC_TEL = '90:00:00.00'        / Telescope's Dec pointing origin (hh:mm:ss.ss)
UTC     = '00:00:00.0'         / UTC time at start of exposure (hh:mm:ss.s)
UTCDATE = '2003-07-10'         / UTC date from tcs at start of exposure
COMMENT
COMMENT   F-2 Instrument Keywords
COMMENT   --------------------------------------------------------------------
COMMENT   
COMMENT   --- Observing Parameters ---
COMMENT   ----------------------------
BEAMMODE= 'f/16    '           / f/16 or MCAO
CAMMODE = 'imaging '           / Imaging or spectroscopy
OBSMODE = 'stare   '           / Stare or nod --- not too sure need this
OBSTYPE = 'image   '           / Image | spectrum | flat | dark
COMMENT 
COMMENT   --- Acquisition Parameters ---
COMMENT   ------------------------------
EXPTIME =                  3.0 / Exposure time in Seconds
NCOADDS =                    1 / Integer number frames coadded on MCE4
NODDLY  =                  5.0 / Dead time in seconds to allow for nod
NETEXPTM=                  3.0 / Net exposure time if NREADS > 1
NREADS  =                    1 / Number of reads to perform if not doing CDS
OFFSTDLY=                  5.0 / Dead time in seconds for telescope offset
COMMENT
COMMENT   --- Optical Configuration ----
COMMENT   ------------------------------
DECKER  = 'OPEN    '           / Named Decker position
MOSPOSNM= 'OPEN    '           / Named Mos position
MOSID   = 'none    '           / Mosplate name if not on an open or long slit
SLITID  = 'none    '           / Slit width name if not on an open or mosplate
FILTER  = 'J       '           / Named Filter position
LYOT    = 'f/16    '           / Named Lyot position
GRISM   = 'OPEN    '           / Named Grism position
COMMENT 
COMMENT   --- Observation Progress ---
COMMENT   ----------------------------
NODBEAM = 'A       '           / Telescope beam
COMMENT
COMMENT   --- Instrument Status ---
COMMENT   -------------------------
INHEALTH= 'GOOD    '           / Instrument Health
INPORT  =                    1 / ISS Port number on which it is installed
INSTRUME= 'FLAMINGOS-2'        / Instrument Name
INSTATE = 'IDLE    '           / Instrument State
COMMENT
COMMENT   ---- WCS INFO ------
COMMENT   --------------------
PIXSCALE=               0.1879 / Plate scale at detector; arcsec/pixel
WCSDIM	=                    2 / Number of dimensions
CTYPE1  = 'RA---TAN'           / X-axis type (or DEC--TAN or TBD)
CTYPE2  = 'DEC--TAN'           / Y-axis type (or RA---TAN or TBD)
CRVAL1  =                  0.0 / Ref pixel value in degrees or dispersion units
CRVAL2  =                  0.0 / Ref pixel value in degrees or dispersoin units
CRPIX1  =               1024.5 / Pixel at which reference applies
CRPIX2  =               1024.5 / Pixel at which reference applies
CD1_1   =                  0.0 / CD matrix values TBD
CD1_2   =                  0.0 /
CD2_1   =                  0.0 /
CD2_2   =                  0.0 /
COMMENT
COMMENT   --- Detector Configuration ---
COMMENT   ------------------------------
PRERSETS=                   10 / Number of pre-resets
PSTRSETS=                 1000 / Number of post-resets
PBC     =                   10 / Pixel base clock period
EDTIMOUT=                   30 / Number of seconds before EDT card timesout
LUTIMOUT=                   30 / Number of seconds before LUT apply timesout
COMMENT
COMMENT   --- Readout Status ----
COMMENT   -----------------------
BUNIT   = 'ADU     '           / Raw data units
DHSLABEL= 'a string'           / Data label from DHS
UTEND   = '00:00:00.00'        / UT Time at end of exposure
UTSTART = '00:00:00.00'        / UT TIme at start of exposure
COMMENT
COMMENT   --- Detector Bias Values ---
COMMENT   ------------------------------
BIAS    =                  1.0 / Applied bias in Volts
COMMENT
COMMENT   --- Detector Controller Status ---
DETID   = 'Det serial number'  / Hawaii-II Serial Number
DETTYPE = 'Hawaii-II'          / Detector Type (HgCdTe)
DCHEALTH= 'GOOD    '           / MCE4 health
DCNAME  = 'MCE4 version'       / Put MCE4 code version here
DCSTATE = 'IDLE    '           / MCE4 state
COMMENT
COMMENT   --- Component Controller ---
COMMENT   ----------------------------
CCHEALTH= 'GOOD    '           / Component Controller health
CCNAME  = 'dont know'          / Components Controller name
CCSTATE = 'IDLE    '           / Components Controller state
WFSBEAM = 'dont know'          / Wavefront sensor status
COMMENT
COMMENT   --- Instrument Temperatures and Pressures ---
COMMENT   ---------------------------------------------
TSMOS   =                77.20 / Mos dewar coldhead temperature
TSMOSWS =                77.20 / Mos dewar work surface temperature
TSMOSPAR=                77.20 / Mos dewar spare temperature
TSCAM   =                77.20 / Camera dewar coldhead temperature
TSCAMWS1=                77.20 / Camera dewar 1st work surface temperature
TSCAMWS2=                77.20 / Camera dewar 2nd work surface temperature
TSDETFAN=                77.20 / Detector fanout board temperature
TSCAMSPR=                77.20 / Camera dewar spare temperature sensor
MOSPRES =               1.0E-8 / Mos dewar pressure in Torr
CAMPRES =               1.0E-8 / Camera dewar pressure in Torr
COMMENT
COMMENT   --- Environment Controller ---
COMMENT   ------------------------------
ECHEALTH= 'GOOD    '           / Environment controller health
ECNAME  = 'dont know'          / Environment controller name
ECSTATE = 'IDLE    '           / Environment controller state





















END
























";
