#if !defined(__UFImgHandler_h__)
#define __UFImgHandler_h__ "$Name:  $ $Id: UFImgHandler.h,v 0.2 2005/03/29 20:25:34 hon Exp $"
#define __UFImgHandler_H__(arg) const char arg##ImgHandler_h__rcsId[] = __UFImgHandler_h__;

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"

#include "iostream"
#include "string"
#include "UFgd.h"
#include "UFBytes.h"
#include "UFFrameConfig.h"
#include "UFFlamObsConf.h"
#include "UFInts.h"
#include "UFEdtDMA.h"

class UFImgHandler {
public:
  inline UFImgHandler(UFFrameConfig* fc= 0) : _ufc(0)
    { if( fc ) _ufc = new UFFrameConfig(*fc); }
  inline UFImgHandler(UFFlamObsConf* oc= 0) : _ufc(0) 
    { if( oc ) _ufc = new UFFrameConfig(oc->width(), oc->height()); }
  inline ~UFImgHandler() {}

  // generate unique? filename:
  inline static string mkname(const string& filehint, int index0, int cnt, bool hidden= false) {
    return UFEdtDMA::mkname(filehint, index0, cnt, hidden);
  }
  // use system open/write for output fits file:
  inline static int writeFits(const string& filenm, char* imgfits, int total_sz) {
    return UFEdtDMA::writeTo(filenm, imgfits, total_sz); 
  }

  // return a newly allocated buffer with fits header prepended to image:
  static char* fitsBuf(UFBytes* ufb, int& total_sz, int fits_sz= 0, char* fitsHdr= 0);
  static char* fitsBuf(UFInts* ufi, int& total_sz, int fits_sz= 0, char* fitsHdr= 0);

  // parse fits header for integer value keyword:
  static int fitsInt(const string& key, char* fitsHdr);
 
  // scale image
  static int* scale(int* data, int ws, int hs, int s);

  // 8bit compressed formats
  static void writePngJpeg(char* fits, int sz, const string& filenm, bool png= true, bool jpeg= false);

  // send to ds9
  static void display(const string& filenm, int dpy= 2);
  static void display(char* fits, int sz, int dpy= 2);

  static int _scale;

protected:
  UFFrameConfig* _ufc;
};

#endif // __UFImgHandler_h__
