#if !defined(__ufgtake_cc__)
#define __ufgtake_cc__ "$Name:  $ $Id: ufgtake.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgtake_cc__;

#include "UFRuntime.h"
#include "UFFrameConfig.h"
#include "UFEdtDMA.h"
#include "UFPSem.h"
#include "UFgd.h"
#include "UFDs9.h"
#include "uffits.h"
// allow (postacq) to put to Epics SAD via pipe:
#include "UFGemDeviceAgent.h"

#include "sys/stat.h"

// for use by UFEdtDMA::takeAndStore():
static UFDs9 _ds9;
static int _cnt= 0;
static int _scale= 1;
static bool _verbose= false;
static int _ds9Disp= 0;
static bool _png= false;
static bool _jpeg= false;
static bool _frmconf= false;
static UFFrameConfig* _ufc= 0;
static string* _epics= 0;
static FILE* _capipe= 0;

static void _sigHandler(int signum) {
  switch(signum) {
    case SIGABRT: clog<<"ufgtake> caught signal "<<signum<<" SIGABRT"<<endl; break;
    case SIGALRM: clog<<"ufgtake> caught signal "<<signum<<" SIGALRM"<<endl; break;
    case SIGCHLD: clog<<"ufgtake> caught signal "<<signum<<" SIGCHLD"<<endl; break;
    case SIGCONT: clog<<"ufgtake> caught signal "<<signum<<" SIGCONT"<<endl; break;
    case SIGFPE: clog<<"ufgtake> caught signal "<<signum<<" SIGFPE"<<endl; break;
    case SIGINT: clog<<"ufgtake> caught signal "<<signum<<" SIGINT"<<endl; exit(0); break;
    case SIGPIPE: clog<<"ufgtake> caught signal "<<signum<<" SIGPIPE"<<endl; break;
    case SIGPWR: clog<<"ufgtake> caught signal "<<signum<<" SIGPWR"<<endl; break;
    case SIGTERM: clog<<"ufgtake> caught signal "<<signum<<" SIGTERM"<<endl; exit(0); break;
    case SIGURG: clog<<"ufgtake> caught signal "<<signum<<" SIGURG"<<endl; break;
    case SIGUSR1: clog<<"ufgtake> caught signal "<<signum<<" SIGG"<<endl; break;
    case SIGUSR2: clog<<"ufgtake> caught signal "<<signum<<" SIGUSR2"<<endl; break;
    case SIGVTALRM: clog<<"ufgtake> caught signal "<<signum<<" SIGVTALRM"<<endl; break;
    case SIGXFSZ: clog<<"ufgtake> caught signal "<<signum<<" SIGXFSZ"<<endl; break;
    default: clog<<"ufgtake> caught signal "<<signum<<" SIG???"<<endl; break;
  }
}
 
int fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

int* scale(int* data, int ws, int hs, int s) {
  int* sdata = new int[ws * hs]; 
  for( int y = 0; y < hs; ++y ) {
    for( int x = 0; x < ws; ++x ) {
      float pix = 0; 
      for( int i = 0; i < s; ++i ) {
        for( int j = 0; j < s; ++j ) {
	  pix += data[s*y + i + s*x + j];
	}
      }
      sdata[y*hs + x] = (int) pix/s/s;
    }
  }
  return sdata; 
}

void writeImg(char* fits, int sz, const char* filenm, bool png= true, bool jpeg= false) {
  int* data= 0;
  int* sdata = data;
  char* ep = fits;
  char end[4]; memset(end, 0, sizeof(end));
  char naxis[7]; memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;
  int ws= w/_scale, hs= h/_scale; // reduce _scale 
  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  int fits_sz= 0; // allow 10x2880 (10 headers)?
  do {
    memcpy(end, ep, ne);
    s = end;
    memcpy(naxis, ep, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, ep);
    if( key == "NAXIS2" )
      h = fitsInt(key, ep);
    ep += 80;
    fits_sz += 80;
  } while( s != "END" && fits_sz < 28800 );
  if( fits_sz >= 28800 && s != "END" ) {
    clog<<"ufgtake::writeImg> sorry, improper or no FITS header?"<<endl;
    return;
  }
  data = (int*)(ep + 80); // fits data is Big-Endian:
  if( w <= 0 || h <= 0 ) {
    clog<<"ufgtake::writeImg> sorry FITS header lacks NAXIS1/2"<<endl;
    return;
  }
  ws = w/_scale; hs = h/_scale;
  clog<<"ufgtake::writeImg>writing output files for img w= "<<ws<<", h= "<<hs<<endl;
  UFgd gd(ws, hs);
  if( _scale == 1 ) {
    gd.apply8bitLutB(data, w, h);
  }
  else {
    sdata = scale(data, ws, hs, _scale);
    gd.apply8bitLutB(data, w, h);
    delete [] sdata;
  }

  string ss;
  if( filenm && jpeg ) {
    char* cf = (char*)filenm;
    while( *cf == '.' || *cf == ' ' || *cf == '\t' ) ++cf; // elim. leading whites or dots
    string ss = cf;
    strstream s;
    size_t fpos = ss.find(".fits");
    if( fpos == string::npos ) {
      if( _scale != 1 )
        s << ss <<"."<< ws <<"x"<<hs<<".jpg"<<ends;
      else
        s << ss << ".jpg"<<ends;
    }
    else {
      if( _scale != 1 )
        s << ss.substr(0, fpos) <<"."<< ws <<"x"<<hs<<".jpg"<<ends;
      else
        s << ss.substr(0, fpos) << ".jpg"<<ends;
    }
    gd.writeJpeg(s.str()); delete s.str();
  }
  if( filenm && png ) {
    char* cf = (char*)filenm;
    while( *cf == '.' || *cf == ' ' || *cf == '\t' ) ++cf; // elim. leading whites or dots
    string ss = cf;
    strstream s;
    size_t fpos = ss.find(".fits");
    if( fpos == string::npos ) {
      if( _scale != 1 )
        s << ss <<"."<< ws <<"x"<<hs<<".png"<<ends;
      else
        s << ss << ".png"<<ends;
    }
    else {
      if( _scale != 1 )
        s << ss.substr(0, fpos) <<"."<< ws <<"x"<<hs<<".png"<<ends;
      else
        s << ss.substr(0, fpos) << ".png"<<ends;
    }
    gd.writePng(s.str()); delete s.str();
  }
}

void postacq(unsigned char* acqbuf, char* fits, int sz, const char* filenm) {
  ++_cnt;
  if( _epics != 0 ) {
    string sadrec = *_epics + ":sad:" + "EDTFRAME";
    UFGemDeviceAgent::sendEpicsSAD(sadrec, _cnt, _capipe);
  }
  if( _ds9Disp ) {
    //int stat = _ds9.display(fits, sz, _ds9Disp); // pipe the data
    int stat = _ds9.display(filenm, _ds9Disp); // just tell ds9 to load the file
    if( stat < 0 )
      clog<<"postproc> Ds9 failed?"<<endl;
  }
  // and while ds9 displays it, and the user gazes fondly at it,
  // write to disk:
  if( _jpeg || _png )
    writeImg(fits, sz, filenm, _png, _jpeg);

  /*
  if( _frmconf && _ufc!= 0 ) {
    _ufc->setDMACnt(_cnt);
    _ufc->setImageCnt(_cnt);
    _ufc->writeTo(1); // send frmconf to stdout
  }
  */
}

int main(int argc, char** argv) {
  int cnt= 1, timeOut= 0, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0;
  UFRuntime::Argv args(argc, argv);
  string arg;
  string usage = "ufgtake -h[elp] [-v[v]] [-nosig -nosem -noswap] [-epics [instrum]] [-exptime [sec.] [-stdout] [-ref [filename]] [-index start index] [-cnt frmcnt] [-timeout sec.] [-lut [lutfile]] [-lutLE FullPathtoLittleEndianLUT] [-fits FullPathtoFITSHeaderFile] [-file FullPathtoFileHint] [-rename] [-lock /tmp/lockfile] [-ds9 [1-9]] [-jpeg [scale]] [-png [scale]] [-frmconf]";

  if( argc <= 1 ) {
    clog<<usage<<endl;
    return 0;
  }
  arg = UFRuntime::argVal("-h", args);
  if( arg == "true" ) {
    clog<<usage<<endl;
    return 0;
  }
  arg = UFRuntime::argVal("-help", args);
  if( arg == "true" ) {
    clog<<usage<<endl;
    return 0;
  }
  
  // start signal thread unless indicated otherwise
  bool sighand = true;
  arg = UFRuntime::argVal("-nosighandler", args);
  if( arg != "false" ) 
    sighand = false;
 
  arg = UFRuntime::argVal("-nosig", args);
  if( arg != "false" )
    sighand = false;
 
  arg = UFRuntime::argVal("-nosem", args);
  if( arg != "false" )
    UFEdtDMA::_nosem = true;
 
  arg = UFRuntime::argVal("-noswap", args);
  if( arg != "false" )
    UFEdtDMA::_noswap = true;

  if( sighand )
    UFPosixRuntime::sigWaitThread(_sigHandler);

  // this also examines args (and sets via ref. parms)
  UFEdtDMA::Conf *ecp = UFEdtDMA::init(args, cnt, timeOut, lut, index, filehint, fits, fits_sz);
  if( ecp == 0 )
    return -1;;

  arg = UFRuntime::argVal("-v", args);
  if( arg == "true" )
    _verbose = true;

  arg = UFRuntime::argVal("-vv", args);
  if( arg == "true" ) {
    _verbose = true;
    UFPSem::_verbose = true;
    UFPosixRuntime::_verbose = true;
  }

  arg = UFRuntime::argVal("-exptime", args);
  if( arg == "true" ) {
    UFEdtDMA::_simExpTime = 1;
    if( _verbose ) clog<<"ufgtake> sim. exposure time: "<<UFEdtDMA::_simExpTime<<endl;
  }
  else if( arg != "false" ) {
    UFEdtDMA::_simExpTime = atoi(arg.c_str());
    if( _verbose ) clog<<"ufgtake> sim. exposure time: "<<UFEdtDMA::_simExpTime<<endl;
  }

  arg = UFRuntime::argVal("-ds9", args);
  if( arg == "true" ) {
    _ds9Disp = 1;
    if( _verbose ) clog<<"ufgtake> display to ds9: "<<_ds9Disp<<endl;
  }
  else if( arg != "false" ) {
    _ds9Disp = atoi(arg.c_str());
    if( _verbose ) clog<<"ufgtake> display to ds9: "<<_ds9Disp<<endl;
  }

  arg = UFRuntime::argVal("-png", args);
  if( arg != "false" ) {
    _png = true;
    if( arg != "true" ) // scale also requested
      _scale = atoi(arg.c_str());
    if( _verbose ) clog<<"ufgtake> write PNG (portable network graphics) file(s), _scale: "<<_scale<<endl;
  }
  arg = UFRuntime::argVal("-jpg", args);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale also requested
      _scale = atoi(arg.c_str());
    if( _verbose ) clog<<"ufgtake> write JPEG file(s), _scale: "<<_scale<<endl;
  }
  arg = UFRuntime::argVal("-jpeg", args);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale also requested
      _scale = atoi(arg.c_str());
    if( _verbose ) clog<<"ufgtake> write JPEG file(s), _scale: "<<_scale<<endl;
  }
  arg = UFRuntime::argVal("-frmconf", args);
  if( arg == "true" ) {
    if( _verbose ) clog<<"ufgtake> write UFFrameConfigs to stdout..."<<endl;
    _frmconf = true;
    _ufc = new UFFrameConfig(ecp->w, ecp->h);
  }
  
  arg = UFRuntime::argVal("-epics", args); // frame cnt
  if( arg != "false"  ) {
    _epics = new string("flam");
    if( arg != "true"  ) *_epics = arg;
    clog<<"ufgtake> postacq will set Epics SAD: "<<*_epics<<endl;
  }

  if( fits == 0 ) {
    if( ecp->w == 2048 ) {
      if( _verbose ) clog<<"ufgtake> set fits header to default for current EDT config.(FLAMINGOS)"<<endl;
      fits = (char*)fits2048;
      fits_sz = strlen(fits);
    }
    else {
      if( _verbose ) clog<<"ufgtake> set fits header to default for current EDT config.(TReCS)"<<endl;
      fits = (char*)fits320x240;
      fits_sz = strlen(fits);
    }
  }

  UFEdtDMA::setDpySeq(1); // call postacquisition function modula 1

  // enter frame acquisition/take loop:
  clog<<"ufgtake> acquire frame(s): "<<cnt<<", time: "<<UFRuntime::currentTime()<<endl;
  float clock0 = 1.0*clock();
  _cnt = 0; _capipe= 0;
  if( _epics != 0 ) {
    string sadrec = *_epics + ":sad:" + "EDTFRAME";
    clog<<"ufgtake> DC agent will clear EDTFRAME cnt on START, and ufgtake postacq func. will increment SAD: "<<sadrec<<endl;
    //UFGemDeviceAgent::sendEpicsSAD(sadrec, _cnt, _capipe); // clear sad
  }
  cnt = UFEdtDMA::takeAndStore(cnt, index, filehint, lut, fits, fits_sz, postacq);
  float frmrate = cnt / ((1.0*clock()-clock0) / CLOCKS_PER_SEC);
  clog<<"ufgtake> acquired frame(s): "<<cnt<<", time: "<<UFRuntime::currentTime()<<endl;
  clog<<"ufgtake> frame rate (Hz): "<<frmrate<<endl;

  return 0;
}

#endif // __ufgtake_cc__
