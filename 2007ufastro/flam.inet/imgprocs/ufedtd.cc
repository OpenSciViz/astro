#include "UFEdtDMAgent.h"
 
int main(int argc, char** argv, char** envp) {
  UFRuntime::Argv args(argc, argv);

  string instrum = "false";

  instrum = UFRuntime::argVal("-flam", args);
  if( instrum == "true" )
    instrum = "flam";
  else
    instrum = UFRuntime::argVal("-flamingos", args);
  if( instrum == "true" )
    instrum = "flam";

  instrum = UFRuntime::argVal("-trecs", args);
  if( instrum == "true" )
    instrum = "trecs";

  instrum = UFRuntime::argVal("-canari", args);
  if( instrum == "true" )
    instrum = "canaricam";
  else
    instrum = UFRuntime::argVal("-canaricam", args);
  if( instrum == "true" )
    instrum = "canaricam";

  // if instrument name is supplied, force initcam for it?
  // ...

  // else get current cam config. and set instrument name accordingly
  if( instrum == "false" ) {
    int w, h, elem = UFEdtDMA::dimensions(w, h);
    if( elem == 320*240 )
      instrum = "trecs";
    else
      instrum = "flam";
    //clog<<"ufedtd> w, h: "<<w << ", "<<h<<", assume instrument: "<<instrum<<endl;
    char* uf = getenv("UFINSTALL");
    if( elem <= 0 ) {
      if( uf == 0 ) {
	clog<<"ufedtd> Please configure the EDT Card Device Driver as you like..."<<endl;
        return(1);
      }
      string conf = uf; conf += "/etc/test_2048.cfg";
      clog<<"ufedtd> forcing Flamingos(2) EDT conf: "<<conf<<endl;
      string cmd = "/opt/EDTpdv/initcam -f "; cmd += conf;
      system(conf.c_str());
    }
  }     

  try {
    UFEdtDMAgent::main(instrum, argc, argv, envp);
  }
  catch( std::exception& e ) {
    clog<<"ufedtd> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufedtd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
