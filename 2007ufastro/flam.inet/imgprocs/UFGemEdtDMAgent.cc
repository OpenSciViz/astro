
#if !defined(__UFGemEdtDMAgent_cc__)
#define __UFGemEdtDMAgent_cc__ "$Name:  $ $Id: UFGemEdtDMAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemEdtDMAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "strings.h"

#include "UFGemEdtDMAgent.h"
#include "UFEdtDMAConfig.h"
#include "UFRuntime.h"
#include "UFgd.h"
#include "UFImgHandler.h"
#include "uffits.h"

// global statics
// for use with external test card (since it transmits continously)
// flamingos exposures times will always be greater than this?
float UFGemEdtDMAgent::_exptime= 0.0; // test/sim exposure time
bool UFGemEdtDMAgent::_SimAcq= false;
bool UFGemEdtDMAgent::_obsdone= false;

// make these available to static funcs:
//string UFGemEdtDMAgent::_instrum;
string UFGemEdtDMAgent::_edtacq;
string UFGemEdtDMAgent::_edtfrm;
string UFGemEdtDMAgent::_edttot;
string UFGemEdtDMAgent::_car;
//string UFGemEdtDMAgent::_dhslabel;
string UFGemEdtDMAgent::_simFITS; // simulation FITS input file
map< UFStrings*, UFInts* > UFGemEdtDMAgent::_simFrms;
pthread_mutex_t UFGemEdtDMAgent::_theFrmMutex;
pthread_mutex_t UFGemEdtDMAgent::_theEdtMutex;
pthread_t UFGemEdtDMAgent::_dmaThrdId= 0;

// buffer info. (one-time init., no mutex required)
int UFGemEdtDMAgent::_maxBuff= 3;
map < string, int > *UFGemEdtDMAgent::_nameIdxFrmBuff= 0; // index # of named frame buff.
// use theFrmMutex when accessing this:
deque < UFInts* > *UFGemEdtDMAgent::_dataFrmBuff= 0; // each named buff is a UFInt object

UFFlamObsConf* UFGemEdtDMAgent::_theObsConf= 0;
UFDs9 UFGemEdtDMAgent::_ds9;
int UFGemEdtDMAgent::_totalcnt= 0; // for observation
int UFGemEdtDMAgent::_edttotal= 0; // for all observations thus far

// globals below need to be cleared for each new dma thread?
// abort discards, stop saves data.?
// take options stored as Argv:
UFRuntime::Argv UFGemEdtDMAgent::_acqArgs;

// post acq. activities
int UFGemEdtDMAgent::_postcnt= 0;
int UFGemEdtDMAgent::_ds9Disp= 0; // # image frames to expect in the ds9 display (0 - 4)
bool UFGemEdtDMAgent::_jpeg= false;
bool UFGemEdtDMAgent:: _png= false;
int UFGemEdtDMAgent::_scale= 1;

void UFGemEdtDMAgent::_clearAll() {
  // globals below need to be cleared for each new dma thread?
  UFRndRobinServ::_replicate = 0; // _replicate now inherited from RndRobinServ

  // abort discards, stop saves data.?
  // take options stored as Argv:
  UFGemEdtDMAgent::_acqArgs.clear();

  // post acq. activities
  UFGemEdtDMAgent::_postcnt= 0;
  UFGemEdtDMAgent::_ds9Disp= 0; // # image frames to expect in the ds9 display (0 - 4)
  UFGemEdtDMAgent::_jpeg= false;
  UFGemEdtDMAgent:: _png= false;
  UFGemEdtDMAgent::_scale= 1;
}

void UFGemEdtDMAgent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    _heart = ""; //_epics + ":ec:heartbeat.edt";
    //_StatRecs.push_back(_epics + ":sad:EDTFRAME.INP");
    _StatRecs.push_back(_epics + ":sad:EDTFRAME.VAL");
    //_StatRecs.push_back(_epics + ":sad:EDTTOTAL.INP");
    _StatRecs.push_back(_epics + ":sad:EDTTOTAL.VAL");
    //_StatRecs.push_back(_epics + ":sad:EDTACTN.INP");
    _StatRecs.push_back(_epics + ":sad:EDTACTN.VAL");
    _edtfrm = _statRec = _StatRecs[0];
    _edttot = _StatRecs[1];
    _edtacq = _StatRecs[2];
  }
  else {
    _heart = "";
    _edtacq = _edttot = _edtfrm = _statRec = "";
  }
}

// ctors:
UFGemEdtDMAgent::UFGemEdtDMAgent(const string& name, int argc,
			   char** argv, char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  _instrum = name;
  UFRndRobinServ::threadedServ();
  ::pthread_mutex_init(&_theFrmMutex, 0);
  ::pthread_mutex_init(&_theEdtMutex, 0);

  _allocNamedBuffs(name);
}

// static funcs:
int UFGemEdtDMAgent::main(const string& name, int argc, char** argv, char** envp) {
  UFGemEdtDMAgent ufdma(name, argc, argv, envp); // ctor binds to listen socket
  // enter event loop:
  ufdma.exec((void*)&ufdma);
  return 0;
}

void UFGemEdtDMAgent::_sighandler(int sig) {
  //if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_sighandler> sig: "<<sig<<endl;
  switch(sig) {
  case SIGTERM:
    //if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::_sighandler> (SIGTERM handled like SIGINT ) sig: "<<sig<<endl;
  case SIGINT: {
    //if( UFDeviceAgent::_verbose )
       clog<<"UFGemEdtDMAgent::_sighandler> (SIGTERM/SIGINT) sig: "<<sig<<endl;
    if( _dmaThrdId ) {
      ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
      _dmaThrdId = 0;
    }
    UFEdtDMA::terminate();
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  break;
  case SIGPIPE:
    clog<<"UFGemEdtDMAgent::_sighandler> (ignoring SIGPIPE) sig: "<<sig<<endl;
    break;
  default:
    //if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::_sighandler> (invoke default handler) sig: "<<sig<<endl;
      UFRndRobinServ::sighandlerDefault(sig); // handle all other signals
    break;
  }
}

void UFGemEdtDMAgent::_cancelhandler(void* p) {
  UFGemEdtDMAgent* dma = static_cast< UFGemEdtDMAgent* > (p);
  if( !_epics.empty() && _epics != "false" ) {
    if(!_edtacq.empty() && !_obsdone ) {
      sendEpics(_edtacq, "Aborted/Stopped");
      clog<<"UFGemEdtDMAgent::_cancelhandler> "<<_edtacq<<" ==> Aborted/Stopped"<<endl; 
    }
  }
  // always unlock mutexes on cancellation of thread
  ::pthread_mutex_unlock(&dma->_theFrmMutex);
  ::pthread_mutex_unlock(&dma->_theEdtMutex);
}

// acqbuf is raw pixel data, fitsbuf is (lut sorted) image with simple fits header, but may be littleEndian (if host is):
void UFGemEdtDMAgent::_postacq(unsigned char* acqbuf, char* fitsbuf, int sz, const char* filenm) {
  ++_postcnt; ++_edttotal;

  if( !_epics.empty() && _epics != "false" ) {
    if( !_edtacq.empty() ) {
      clog<<"UFGemEdtDMAgent::_postacq> "<<_edtacq<<" ==> PostProcessing"<<endl;
      sendEpics(_edtacq, "PostProcessing");
    }
    if( !_edtfrm.empty() ) {
      clog<<"UFGemEdtDMAgent::_edtfrm> "<<_edtacq<<" ==> "<<_postcnt<<endl;
      sendEpics(_edtfrm, UFRuntime::concat("", _postcnt));
    }
    if( !_edttot.empty() ) {
      clog<<"UFGemEdtDMAgent::_edttot> "<<_edtacq<<" ==> "<<_edttotal<<endl;
      sendEpics(_edttot, UFRuntime::concat("", _edttotal));
    }
  }

  if( acqbuf == 0 || fitsbuf == 0 )
    return;

  if( UFDeviceAgent::_verbose ) 
    clog<<"UFGemEdtDMAgent::_postacq> acqbuf, fitsbuf: "<< (size_t)acqbuf <<", "<< (size_t)fitsbuf<<", sz: "<<sz<<endl;

  // also insert acquired frame into frame buffer replcation queue
  int* imgbuf = (int*) (&fitsbuf[2880]);
  if( UFRndRobinServ::_replicate ) {
    clog<<"UFGemEdtDMAgent::_postacq> send to replication connection (dhsclient)?"<<endl;
    _replFrm((int*) imgbuf);
  }
  else if( UFDeviceAgent::_verbose ) {
    clog<<"UFGemEdtDMAgent::_postacq> No replication connection (dhsclient)?"<<endl;
  }

  //if( _dataFrmBuff->size() > 0 ) // set history data full & qlook frames
    _histFrm((int*) imgbuf);

  // and while the above is being handled,
  // write jpeg and/or png to disk:
  if( _jpeg || _png ) {
    //if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::_postacq> _jpeg, _png: "<<_jpeg<<", "<<_png<<endl;
    _writeImg(fitsbuf, sz, filenm, _png, _jpeg);
  }

  // presumably this is followed by another frame acq, or completion to idle..
  if( !_epics.empty() && _epics != "false" ) {
    if( !_edtacq.empty() ) {
      clog<<"UFGemEdtDMAgent::_postacq> "<<_edtacq<<" ==> Acquiring"<<endl;
      sendEpics(_edtacq, "Acquiring"); 
    }
  }

 if( imgbuf != 0 && sz > 0 ) {
   if( UFRuntime::littleEndian() ) {
      UFEdtDMA::little2BigEnd((unsigned char*)imgbuf, (unsigned char*)imgbuf, _edtcfg);
   }
    if( _ds9Disp ) {
      int stat = -1;
      if( filenm != 0 ) {
        clog<<"UFGemEdtDMAgent::_postacq> DS9 fits file:"<<filenm<<endl;
        // write out the file
        double fitsunit = 36.0*80.0;
        // number of bytes to pad out to integral 2880:
        int pad = (int) ::floor((sz/(fitsunit) - ::floor(sz/fitsunit))*(fitsunit)); 
	UFEdtDMA::writeTo(filenm, (char*)fitsbuf, sz, pad);
        stat = _ds9.display(filenm, _ds9Disp);
      }
      else {
        clog<<"UFGemEdtDMAgent::_postacq> DS9 fits pipe buffer..."<<endl;
        stat  = _ds9.display(fitsbuf, sz, _ds9Disp);
      }
      if( stat < 0 )
        clog<<"_postacq> Ds9 display failed?"<<endl;
    }
  }

  if( threaded() ) {
    // this helps when using the external simulator (since it sends continuously)
    clog<<"UFGemEdtDMAgent::_postacq> dma thread _exptime: "<<_exptime<<" (sec.)"<<endl;
    if( _exptime > 0.001 && UFDeviceAgent::_sim )
      UFPosixRuntime::sleep(_exptime); // allow other threads some cpu
    else
      UFPosixRuntime::sleep(0.1); // allow other threads some cpu
  }
} // _postacq

void UFGemEdtDMAgent::startup() {
  // start signal handler thread
  sigWaitThread(UFGemEdtDMAgent::_sighandler); // create a thread devoted to all signals of interest
  //clog << "UFGemEdtDMAgent::startup> established signal handler (sigwait) thread."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  init();
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemEdtDMAgent::startup> listening on port= " <<listenport<<endl;
  //clog<<", with server soc: "<<_theServer.description()<<endl;
  return;  
}

UFTermServ* UFGemEdtDMAgent::init() {
  // save edtconfig to a file
  //pid_t p = getpid();
  strstream s;
  s<<"/usr/tmp/.edtconf"<<ends; //."<<p<<".txt"<<ends;
  mode_t msk = umask(0);
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  umask(msk);
  if( fd <= 0 ) {
    clog<<"UFGemEdtDMAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return 0;
  }
  delete s.str();

  int w= 0, h= 0;
  UFEdtDMA::dimensions(w, h);

  strstream ss;
  ss<<name()<<"> Initial/current Edt dev. conf is: "<<w<<"x"<<h<<endl;
  char *edtstr = ss.str();
  int nc = ::write(fd, edtstr, strlen(edtstr));
  if( nc <= 0 ) {
    clog<<"UFGemEdtDMAgent> edt config log failed..."<<endl;
  }
  ::close(fd);

  if( _sim ) {
    _theObsConf = 0; //_readMEF(_simFITS, _simFrms);
  }

  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemEdtDMAgent::init> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemEdtDMAgent::init> Ecpics CA child proc. failed to start"<<endl;
  }

  return 0;
} // init

UFFlamObsConf* UFGemEdtDMAgent::_readMEF(const string& filenm, map< UFStrings*, UFInts* >& data) {
  int fd = ::open(filenm.c_str(), O_RDONLY);
  if( fd < 0 ) 
    return 0;

  UFFlamObsConf* oc = new UFFlamObsConf(_exptime, filenm);
  // parse the MEF to create an obsconfig & load all headers & frames into data:
  // ...
  // ...
  ::close(fd);
  return oc;
}

// this should always return the service/agent listen port #
int UFGemEdtDMAgent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  port = 52001; // force it to this for now
  _config = new UFEdtDMAConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = -1;
  _config->_tshost = "";

  string arg = findArg("-v");
  if( arg == "true" ) {
    UFDeviceAgent::_verbose = true;
    //UFEdtDMA::_verbose = UFDeviceAgent::_verbose = UFRndRobinServ::_verbose = true;
    //UFPosixRuntime::_verbose = UFDeviceAgent::_verbose = true;
  }
  arg = findArg("-vv");
  if( arg == "true" ) {
    UFPosixRuntime::_verbose = UFEdtDMA::_verbose = UFDeviceAgent::_verbose = UFRndRobinServ::_verbose = UFPSem::_verbose = true;
  }

  arg = findArg("-nosem");
  if( arg == "true" )
    UFEdtDMA::_nosem = true;

  arg = findArg("-noswap");
  if( arg == "true" )
    UFEdtDMA::_noswap = true;

  arg = findArg("-mmap");
  if( arg == "true" )
    UFEdtDMA::_usemmap = true;

  arg = findArg("-sim");
  if( arg == "true" ) {
    _sim = true;
    _SimAcq = _sim;
  }

  arg = findArg("-exp");
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  if( UFDeviceAgent::_verbose || UFEdtDMA::_verbose )
    clog<<"UFGemEdtDMAgent::options> default (if sim) exposure time (sec.): "<<_exptime<<endl;
  /*
  if( _config->_tsport >= 0 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52001;
  }
  */
  if( UFDeviceAgent::_verbose ) {
    if( threaded() )
      clog<<"UFGemEdtDMAgent::options> this service is threaded..."<<endl;
    clog<<"UFGemEdtDMAgent::options> set port to GemEdtDMAgent port "<<port<<endl;
  }
  arg = findArg("-initcam");
  if( arg != "false" ) {
    if( arg == "true" ) {// default is MCE4 config. for F2
      string info = UFEdtDMAConfig::initF2mce();
      clog<<info<<endl;
    }
    else { // assume non default option is 'test' mode
      string info = UFEdtDMAConfig::initF2test();
      clog<<info<<endl;
    }
  }

  return port;
} // options

int UFGemEdtDMAgent::_setAcqArgs(UFDeviceAgent::CmdInfo* act) {
  _acqArgs.clear();
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    // since these will be passed directly to UFEdtDMA::init(),
    // which expects the F1 ufgtake syntax -exp time -cnt 0/1 -fle -lut etc.
    // need to force lowercase and '-' prepend...
    string cmdname = act->cmd_name[i]; UFRuntime::lowerCase(cmdname);
    if( cmdname.find("-") != 0 ) cmdname = "-" + cmdname;
    string cmdimpl = act->cmd_impl[i];
    _acqArgs.push_back(cmdname); _acqArgs.push_back(cmdimpl);
    clog<<"UFGemEdtDMAgent::_setAcqArgs> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
  }

  return (int)_acqArgs.size();
}

string UFGemEdtDMAgent::newClient(UFSocket* clsoc, const string& agentname) {
  string clname = "";
  if( clsoc == 0 ) {
    clog<<"UFGemEdtDMAgent:::newClient> "<<agentname<<" new client connection null?."<<endl;
    return clname;
  }
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFGemEdtDMAgent:::newClient> "<<agentname<<" exchanging greeting with "<<clname<<endl;
  clname = greetClient(clsoc, agentname);  
  /*
  try {
    clname = greetClient(clsoc, agentname);  
  }
  catch(std::exception& e) {
    clog<<"UFGemEdtDMAgent::exec> stdlib exception occured in newClient accept: "<<e.what()<<endl;
  }
  catch(...) {
    clog<<"UFGemEdtDMAgent::exec> unknown exception occured in newClient accept..."<<endl;
  }
  */
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFGemEdtDMAgent:::newClient> "<<agentname<<" exchanged greeting with "<<clname<<endl;

  // replication client support:
  if( clname.find("replica") != string::npos || clname.find("Replica") != string::npos ||
      clname.find("REPLICA") != string::npos ) {
    if( UFRndRobinServ::_replicate != 0 ) {
       clog<<"UFGemEdtDMAgent::newClient> resetting _replicate socket!"<<endl;
       // close and destroy old socket?
       UFRndRobinServ::_replicate->close(); delete UFRndRobinServ::_replicate;
    }
    clog<<"UFGemEdtDMAgent::newClient> set _replicate socket."<<endl;
    UFRndRobinServ::_replicate = clsoc;
  }

  return clname;
} // newClient

/*
int UFGemEdtDMAgent::pendingReqs(vector <UFSocket*>& clients) {
  clients.clear();
  UFSocket::AvailTable bytcnts;
  if( _theConnections == 0 ) {
    clog<<"UFGemEdtDMAgent::pendingReqs> no connection table?"<<endl;
    return -1;
  }
  if( _theConnections->size() == 0 ) {
    clog<<"UFGemEdtDMAgent::pendingReqs> empty connection table?"<<endl;
    return 0;
  }
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::pendingReqs> # of clients in connection table to check: "<<_theConnections->size()<<endl;
  int acnt = UFSocket::availableList(*_theConnections, bytcnts, _theConMutex);
  //if( UFDeviceAgent::_verbose )
  clog<<"UFGemEdtDMAgent::pendingReqs> # of clients in connection table to (double) check: "<<acnt<<" out of: "<<_theConnections->size()<<endl;
  if( acnt <= 0 ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::pendingReqs> no clients have pending/available i/o, acnt: "<<acnt<<", of "<<_theConnections->size()<<endl;
    return acnt;
  }
  UFSocket::AvailTable::iterator i = bytcnts.begin();
  for( ; i != bytcnts.end(); ++i ) {
    UFSocket* socp = i->first; 
    int nb = i->second;
    if( nb > 0 && socp != 0 ) {
      clients.push_back(socp);
    }
    else if( nb < 0 && socp != 0 ) { // stale connection? close it...
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemEdtDMAgent::pendingReqs> connection stale?, closing! acnt: "<<acnt<<endl;
	int concnt = UFSocket::closeAndClear(*_theConnections, socp, _theConMutex);
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemEdtDMAgent::pendingReqs> current connection cnt= "
	    <<_theConnections->size()<<", avail. cnt= "<<concnt<<endl;
    }
    else if( socp != 0 ) { // available timed-out?
      if( UFDeviceAgent::_verbose )
        clog<<"UFGemEdtDMAgent::pendingReqs> nb: "<<nb<<", nothing yet on "<<socp->description()<<endl;
    }
  }

  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::pendingReqs> # of clients with (valid) pendingReqs: "
        <<clients.size()<<", out of: "<<_theConnections->size()<<endl;

  return (int) clients.size();   
} 
*/

void UFGemEdtDMAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0 ) {
    mlsleep(_Update); // no connections, no queued reqs., go to sleep
  }
  else {
    //UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
    UFRndRobinServ::hibernate();
  }
}

// the key virtual function(s) to override,
// presumably any allocated (UFStrings reply) memory is freed by the calling layer....
int UFGemEdtDMAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  bool reply_expected= true;
  int stat = -1;
  // allocate empty reply vectot:
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;
  string reply, simInstrum = name();

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdparams = act->cmd_params[i];
    clog<<"UFGemEdtDMAgent::action> "<<cmdname<<" :: "<<cmdparams<<endl;
  }
  
  UFFlamObsConf* ufobs = UFEdtDMAConfig::obsconf(act); 
  if( ufobs ) {
    _totalcnt = ufobs->totImgCnt();
    if( _theObsConf == 0 ) {
       clog<<"UFGemEdtDMAgent::action> (first) new obsconf: "<<ufobs->name()<<", _totalcnt: "<<_totalcnt<<endl;
      _theObsConf = ufobs;
    }
    else { 
      clog<<"UFGemEdtDMAgent::action> (delete old, and set) new obsconf: "<<ufobs->name()<<", _totalcnt: "<<_totalcnt<<endl;
      delete _theObsConf;
      // free old obs. conf. done in dma thread when it clears its dataque on obs. completion (and after repl)?
      // note that if/when we switch to AcqCtrl, should never free its obsconf!
      _theObsConf = ufobs; // new obs conf.
    }
    if( reply_expected ) {
      reply = ufobs->name();
      act->cmd_reply.push_back(reply);
      string heading = "UFGemEdtDMAgent::action> obsconfig";
      clog<<"UFGemEdtDMAgent::action> reply: "<<heading<<", "<<reply;
      replies.push_back(new UFStrings(heading, &reply));
    }
  }
  else {
    clog<<"UFGemEdtDMAgent::action> No OBSCONF/OBSETUP in this cmd bundle..."<<endl;
    if( _theObsConf == 0 )
      clog<<"UFGemEdtDMAgent::action> No current ObsConf..."<<endl;
    else
      clog<<"UFGemEdtDMAgent::action> Current ObsConf: "<<_theObsConf->name()
	  <<", exptime: "<<_theObsConf->expTime()<<", expcnt: "<<_theObsConf->totImgCnt()<<endl;
  }
  
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    //if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<endl;

    int nr = _config->validCmd(cmdname); //, cmdimpl);
    if( nr < 0 ) {
      clog<<"UFGemEdtDMAgent::action> Assume cmd should be passed to EdtDMA class: "<<cmdname<<" :: "<<cmdimpl<<endl;
      continue;
    }
    act->time_submitted = currentTime();
    _active = act;

    // check if this bundle is from the epics db (or the dc agent, or some other client)
    if( cmdname.find("car") == 0 || cmdname.find("Car") == 0 || cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      _car = _carRec = cmdimpl;
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemEdtDMAgent::action> CAR: "<<_car<<endl;
      continue;
    }

    // check if this bundle is from the epics db (or the dc agent, or some other client)
    if( cmdname.find("sad") == 0 || cmdname.find("Sad") == 0 || cmdname.find("SAD") == 0 ) {
      reply_expected= false;
      _edtfrm = _statRec = cmdimpl;
      //if( UFDeviceAgent::_verbose )
        clog<<"UFGemEdtDMAgent::action> SAD: "<<_edtfrm<<endl;
      continue;
    }

    // check if sim, don't create dma thread
    if( cmdname.find("sim") != string::npos ||
	cmdname.find("Sim") != string::npos ||
	cmdname.find("SIM") != string::npos) {
	_sim = true;
        if( cmdimpl.find("flam") != string::npos || cmdimpl.find("Flam") != string::npos || cmdimpl.find("FLAM") != string::npos ) 
	  simInstrum = "flam";
    }

    if( cmdname.find("shutdown") != string::npos || cmdname.find("Shutdown") != string::npos || 
	cmdname.find("SHUTDOWN") != string::npos ) {
      if( _dmaThrdId ) {
        ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dmaThrdId = 0;
      }
      UFRndRobinServ::_shutdown = true; //UFRndRobinServ::shutdown();
      if( reply_expected ) {
        reply = "Ok, shutting down: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemEdtDMAgent::action> shutdown", &reply));
      }
      if( !_epics.empty() && _epics != "false" && !_car.empty() )
        setCARIdle(_car);

      break;
    }

    if( cmdname.find("acq") != string::npos || cmdname.find("Acq") != string::npos || 
	cmdname.find("ACQ") != string::npos ) {
      clog<<"UFGemEdtDMAgent::action> ACQ directive indicated: "<<cmdname<<" ==> "<<cmdimpl<<endl;
      if( _theObsConf == 0 ) {
	clog<<"UFGemEdtDMAgent::action> But No current ObsConf? ignoring..."<<endl;
	continue;
      }
      // send/forward obsconf to replication client: (acq start/stop/abort directive in name())
      if( UFRndRobinServ::_replicate )  {
	if( UFRndRobinServ::_replicate->validConnection() ) {
          clog<<"UFGemEdtDMAgent::action> sending obsconf to replication client..."<<endl;
          clog<<"UFGemEdtDMAgent::action> typ: "<<_theObsConf->typeId()<<", elem: "
	      <<_theObsConf->elements()<<", name: "<<_theObsConf->name()<<endl;
	  int ns = UFRndRobinServ::_replicate->send(_theObsConf);
          clog<<"UFGemEdtDMAgent::action> sent nb: "<<ns<<endl;
	  if( reply_expected && replies.size() <= 0 ) {
            string heading = "UFGemEdtDMAgent::action> obsconfig";
            clog<<"UFGemEdtDMAgent::action> reply: "<<heading<<", "<<reply;
            replies.push_back(new UFStrings(heading, &reply));
	  }
	}
	else {
	  clog<<"UFGemEdtDMAgent::action> invalid replication socket, unable to forward directive"<<endl;
          UFRndRobinServ::_replicate = 0; // _replicate now inherited from RndRobinServ
        }
      }
      if( cmdimpl.find("abort") != string::npos || cmdimpl.find("Abort") != string::npos ||
	  cmdimpl.find("ABORT") != string::npos ) {
	// abort acquisition
	clog<<"UFGemEdtDMAgent::action> ACQ abort."<<endl;
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId )
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dmaThrdId = 0; stat = 0;
        if( reply_expected ) {
  	  reply = "Ok, aborted DMA: " + cmdname + "::" + cmdimpl;
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemEdtDMAgent::action> acq::abort", &reply));
	}
	break;
      } // acq::abort
      if( cmdimpl.find("stop") != string::npos || cmdimpl.find("Stop") != string::npos || 
	  cmdimpl.find("STOP") != string::npos ) {
	clog<<"UFGemEdtDMAgent::action> ACQ stop."<<endl;
	// get dma status cmdimpl == fits/other
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId ) 
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dmaThrdId = 0; stat = 0;
        if( reply_expected ) {
 	  reply = "Ok, stopped DMA: " + cmdname + "::" + cmdimpl;
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemEdtDMAgent::action> acq::stop", &reply));
	}
	break;
      } // acq::stop
      if( cmdimpl.find("start") != string::npos || cmdimpl.find("Start") != string::npos ||
	  cmdimpl.find("START") != string::npos ) {
	clog<<"UFGemEdtDMAgent::action> ACQ start."<<endl;
        // start acquisition
        stat = _setAcqArgs(act);
        int w, h;
        bool idle = UFEdtDMA::takeIdle(w, h);
        //if( UFDeviceAgent::_verbose )
	  clog<<"UFGemEdtDMAgent::action> takeIdle: "<<idle<<"; w, h = "<<w<<", "<<h<<endl;
        if( !idle ) { // 
          clog<<"UFGemEdtDMAgent::action> "<<cmdimpl<<" :: "<<cmdimpl<<" rejected, Waiting on Image"<<endl;
          if( reply_expected ) {
            strstream s;
            s<<cmdimpl<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
            reply = s.str(); delete s.str();
            act->cmd_reply.push_back(reply);
            replies.push_back(new UFStrings("UFGemEdtDMAgent::action> acq::start", &reply));
	  }
	  break;
        } // acq start
        else if( _dmaThrdId > 0 ) { 
	  int m = ::pthread_mutex_trylock(&_theEdtMutex);
	  if( m != 0 && errno == EBUSY ) // edt dma in process, reject request
          clog<<"UFGemEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" rejected, dma (thread) still in progress"<<endl;
          if( reply_expected ) {
            strstream s;
            s<<cmdname<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
            reply = s.str(); delete s.str();
            act->cmd_reply.push_back(reply);
            replies.push_back(new UFStrings("UFGemEdtDMAgent::action> acq::start", &reply));
	  }
	  break;
        } // not idle, reject dma req.

        // create & run dma thread
	// dmaThread should lock and unlock the EdtMutex before exiting..
	// if we get here presumably we are convinced about current idle state
	// and can start a new acq...
	// be sure to unlock the mutex, if we left it locked above...
	::pthread_mutex_unlock(&_theEdtMutex);
        _dmaThrdId = newThread(_dmaThread, this); stat = (int)_dmaThrdId;
 	reply = "Ok, started DMA thread for: " + cmdname + "::" + cmdimpl;
	clog<<"UFGemEdtDMAgent::action> acq::start "<<reply<<endl;
        if( reply_expected ) {
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemEdtDMAgent::action> acq::start", &reply));
	}
	break;
      } // acq start
    } // end acq
    else if( cmdname.find("fram") != string::npos || cmdname.find("Fram") != string::npos ||
	     cmdname.find("FRAM") != string::npos ) { // fetch frame from cmdimpl == buffer name or index
      clog<<"UFGemEdtDMAgent::action> Frame/Image req."<<endl;
      const char* c = cmdimpl.c_str();
      int frmidx = 0;
      if( isdigit(c[0]) ) {// specify frame by index (index 0 should be most recent full image) or by name 
	frmidx = atoi(c);
      }
      else if( cmdimpl == "flam:FullImage" || cmdimpl == "flam:1024Image" ||
	       cmdimpl == "flam:512Image") {
	frmidx = (*_nameIdxFrmBuff)[cmdimpl];
      }
      else {
	frmidx = _maxBuff-1;
      }
      if( frmidx >= _maxBuff ) frmidx = _maxBuff-1; 
      if( frmidx < 0 ) frmidx = 0;
      UFInts* frm = (*_dataFrmBuff)[frmidx];
      //int m = ::pthread_mutex_trylock(&_theFrmMutex); // fails if not owned by this thread
      int m = ::pthread_mutex_lock(&_theFrmMutex); // blocks...
      if( m != 0 ) { // failed to get the lock
        if( reply_expected ) {
          reply = "UFGemEdtDMAgent::action> unable to access frame buffer, mutex still locked...";
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFGemEdtDMAgent::action> frame", &reply));
        }
        break;
      }
      else if( reply_expected ) {
        strstream s;
        s<<"UFGemEdtDMAgent::action> sending "<<frm->name()<<", pixcnt: "<<frm->numVals()<<ends;
        reply = s.str(); delete s.str();
        //if( UFDeviceAgent::_verbose )
          clog<<reply<<endl;
        replies.push_back(new UFStrings("UFGemEdtDMAgent::action> frame", &reply));
        // make a deep copy to be sent back to client (and deleted by UFDeviceAgent::exec)
        UFInts* img = new UFInts(frm->name(), frm->valInts(), frm->numVals());
        ::pthread_mutex_unlock(&_theFrmMutex);
        int cnt = takeCnt(), tot = takeTotCnt();
        img->setSeq(cnt, tot); // (ab)use the seq. cnt in header for frame info.
        replies.push_back(img); // replies are sent back via exec virtual
        int npix = img->elements();
        clog<<"UFGemEdtDMAgent::action> frame: "<<img->timeStamp()<<", "<<img->name()<<", npix: "<<npix<<endl;
        clog<<"UFGemEdtDMAgent::action> pixmin: "<<img->minVal()<<", pixmax: "<< img->maxVal()<<endl;
        clog<<"UFGemEdtDMAgent::action> first, last pixels: "<<(*img)[0]<<", "<<(*img)[npix - 1]<<endl;
        clog<<"UFGemEdtDMAgent::action> central pixels: "<<(*img)[npix/2-1]<<", "<<(*img)[npix/2]<<", "<<(*img)[npix/2+1]<<endl;
	return (int) replies.size(); // reply w/o conf & image objects
      }
    } // end frame req. action
    /*
    else if( reply_expected &&
	     cmdname.find("conf") != string::npos ||
	     cmdname.find("Conf") != string::npos ||
	     cmdname.find("CONF") != string::npos ||
	     cmdname.find("obs") != string::npos ||
	     cmdname.find("Obs") != string::npos ||
	     cmdname.find("OBS") != string::npos ) { // reply with obsconf
      clog<<"UFGemEdtDMAgent::action> ObsConf req."<<endl;
      UFFlamObsConf* oc = new UFFlamObsConf(*_theObsConf);
      clog<<"UFGemEdtDMAgent::action> sending (copy of) current obsconf"<<oc->name()<<endl;
      replies.push_back(oc);
      // don't return here, ACQ may follow ObsConf in cmd bundle...
      //return (int) replies.size(); // reply will get deleted by RndRobin logic!
    } // end conf
    */
    else if( reply_expected &&
	     cmdname.find("stat") != string::npos || cmdname.find("Stat") != string::npos ||
	     cmdname.find("STAT") != string::npos ) { // get dma status cmdimpl == edt, fits/other
      if( cmdimpl.find("edt") != string::npos || cmdimpl.find("Edt") != string::npos ||
	  cmdimpl.find("EDT") != string::npos ||
	  cmdimpl.find("frm") != string::npos || cmdimpl.find("Frm") != string::npos ||
	  cmdimpl.find("FRM") != string::npos ) { // edt config (initcam) & take sem. counter vals
        reply = UFEdtDMA::status();
	replies.push_back(new UFStrings("UFGemEdtDMAgent::action> stat::edt", &reply));
        stat = reply.length();
      }
      else if( cmdimpl.find("fits") != string::npos || cmdimpl.find("Fits") != string::npos ||
	       cmdimpl.find("FITS") != string::npos ) {
        act->cmd_reply.push_back(reply);
	replies.push_back(_config->statusFITS(this));
      }
      else {
        act->cmd_reply.push_back(reply);
        replies.push_back(_config->status(this));
      }
    } // end stat
    else if( ufobs == 0 &&
	     cmdname.find("file") != string::npos ||
	     cmdname.find("File") != string::npos ||
	     cmdname.find("FILE") != string::npos ) {
      // query (cmdimpl == '?') or set fits file pathname
      if( reply_expected ) {
        clog<<"UFGemEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" client has requested current filename?"<<endl;
	strstream s;
        s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
        reply = s.str(); delete s.str();
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemEdtDMAgent::action> archv", &reply));
      }
    } // end archv
    else if( ufobs == 0 ) {
      if( reply_expected ) {
        clog<<"UFGemEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" client req. not supported."<<endl;
	strstream s;
        s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
        reply = s.str(); delete s.str();
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFGemEdtDMAgent::action> unknown", &reply));
      }
    }
  } // end for loop of cmd bundle

  if( stat < 0 )
    act->status_cmd = "failed";
  else if( act->cmd_reply.empty() )
    act->status_cmd = "failed/rejected";
  else
    act->status_cmd = "succeeded";
  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size();
  }

  clog<<"UFGemEdtDMAgent::action> reply count: "<<replies.size()<<endl;
  return 0;
} 

int UFGemEdtDMAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemEdtDMAgent::dmaThread(UFGemEdtDMAgent& dma) {
  string usage = "[-v] [-nosem] [-stdout] [-index start index] [-cnt frmcnt] [-timeout sec.] [-lut [lutfile]] [-fits fitsfile] [-file hint] [-rename] [-lock lockfile] [-ds9] [-jpeg [scale]] [-png [scale]] [-frmconf]";

  // is agent in very verbose mode?
  string arg = dma.findArg("-vv");
  if( arg == "true" ) {
    UFEdtDMA::_verbose = UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  }
  arg = UFRuntime::argVal("-exp", dma._acqArgs);
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  arg = UFRuntime::argVal("-exptime", dma._acqArgs);
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  if( UFDeviceAgent::_verbose || UFEdtDMA::_verbose )
    clog<<"UFGemEdtDMAgent::dmaThread> exposure time (sec): "<<_exptime<<endl;

  /*
  if( UFDeviceAgent::_verbose ) {
    for( int i = 0; i < (int)dma._acqArgs.size(); ++i )
      clog<<"UFGemEdtDMAgent::dmaThread> arg# "<<i<<" : "<<dma._acqArgs[i]<<endl;
  }
  */

  _ds9Disp = 0;
  arg = UFRuntime::argVal("-ds9", dma._acqArgs);
  if( !arg.empty() && arg != "false" ) {
    _ds9Disp = 1;
    const char* cs = arg.c_str();
    if( isdigit(cs[0]) )
      _ds9Disp = atoi(cs);
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::dmaThread> display to ds9 frame(s): "<<_ds9Disp<<endl;
  }

  _png = false;
  arg = UFRuntime::argVal("-png", dma._acqArgs);
  if( arg != "false" ) {
    _png = true;
    if( arg != "true" ) // scale png
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemEdtDMAgent::dmaThread> write PNG (portable network graphics) file(s), _scale: "<<_scale<<endl;
  }

  _jpeg = false;
  arg = UFRuntime::argVal("-jpg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemEdtDMAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }
  arg = dma.UFRuntime::argVal("-jpeg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg requested
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFGemEdtDMAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }

  int ecnt= 1, timeOut= 0, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0; 
  // this also examines acq args:
  UFEdtDMA::init(dma._acqArgs, ecnt, timeOut, lut, index, filehint, fits, fits_sz);
  if( fits == 0 ) {
    if( width() == 2048 ) {
      if( UFDeviceAgent::_verbose ) clog<<"UFGemEdtDMAgent::dmaThread> set fits header to default for current EDT config.(FLAMINGOS)"<<endl;
      fits = (char*)fits2048;
      fits_sz = strlen(fits);
    }
    else {
      if( UFDeviceAgent::_verbose ) clog<<"UFGemEdtDMAgent::dmaThread> set fits header to default for current EDT config.(TReCS)"<<endl;
      fits = (char*)fits320x240;
      fits_sz = strlen(fits);
    }
  }
  UFEdtDMA::setDpySeq(1); // call postacquisition function modula 1
  // enter frame acquisition/take loop:
  // postacq func. should lock the FrmMutex, copy the frame into the _dmeFrameBuff UFInt*
  // and setSeq(takeCnt(), takeTotal())... 
  dma._postcnt = 0; // clear the global counte

  // clear the ThrdId
  dma._dmaThrdId = 0;
 
  //if( UFDeviceAgent::_verbose )
  clog<<"UFGemEdtDMAgent::dmaThread> "<<UFRuntime::currentTime()<<" Start DMA acq. of frame(s): "<<ecnt
	<<", filehint: "<<filehint<<endl;

  _obsdone = false;
  int cnt= 0;
  if( !_SimAcq )
    cnt = UFEdtDMA::takeAndStore(ecnt, index, filehint, lut, fits, fits_sz, _postacq);
  else
    cnt = UFEdtDMA::takeAndStore(ecnt, index, filehint, lut, fits, fits_sz, _postacq, _simacq);

  //if( UFDeviceAgent::_verbose )
  clog<<"UFGemEdtDMAgent::dmaThread> "<<UFRuntime::currentTime()<<" Completed DMA acq. of frame(s): "<<cnt
      <<", filehint: "<<filehint<<endl;

  _obsdone = true;

  return;
}

// allow dma event loop to be run in its own (cancellable) thread:
void* UFGemEdtDMAgent::_dmaThread(void* p) {
  if( p == 0 )
    return p;

  UFGemEdtDMAgent* dma = static_cast< UFGemEdtDMAgent* > (p);
  // lock edt dma loop
  int m = ::pthread_mutex_trylock(&dma->_theEdtMutex);
  if( m != 0 && errno == EBUSY ) { // edt dma in process?
    clog<<"UFGemEdtDMAgent::_dmaThread> edt mutex lock failed ? dma in progress"<<endl;
    return p;
  }
  clog<<"UFGemEdtDMAgent::_dmaThread> edt mutex locked; starting new dma..."<<endl;
  int oldstate, oldtype;
  ::pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
  ::pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
// since linux(2.4-6 kernel) & solaris(8-10?) "pthread.h" declare pthread_cleanup_push macro(s)
// if macro, cannot use global scope '::' notation:
  pthread_cleanup_push(UFGemEdtDMAgent::_cancelhandler, p);
  // clear acq. dma related globals (of any lingering values from prior thread?)
  //_clearAll();

  // if commanded from epics clear the current obs. edt frame count sad
  if( !_epics.empty() && _epics != "false" ) {
    clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edtacq<<" ==> Acquiring"<<endl;
    if( !_edtacq.empty() ) sendEpics(_edtacq, "Acquiring"); 
    clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edtfrm<<" ==> 0"<<endl;
    if( !_edtfrm.empty() ) sendEpics(_edtfrm, "0");
  } 

  // enter dma event loop:
  dma->dmaThread(*dma);

  // if commanded from epics, and _car/_sad set, indicate command completion:
  if( !_epics.empty() && _epics != "false" ) {
    if( !_edtacq.empty() && _obsdone) {
      clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edtacq<<" ==> Idle"<<endl;
      sendEpics(_edtacq, "Idle"); 
    }
    if( !_edtacq.empty() && !_obsdone) {
      clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edtacq<<" ==> Aborted/Stopped"<<endl;
      sendEpics(_edtacq, "Aborted/Stopped");
    } 
    if( !_edtfrm.empty() ) {
      clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edtfrm<<" ==> "<<dma->_postcnt<<endl;
      sendEpics(_edtfrm, UFRuntime::concat("", dma->_postcnt));
    }
    if( !_edttot.empty() ) {
      clog<<"UFGemEdtDMAgent::_dmaThread> "<<_edttot<<" ==> "<<dma->_edttotal<<endl;
      sendEpics(_edttot, UFRuntime::concat("", dma->_edttotal));
    }
    if( !_car.empty() ) {
      clog<<"UFGemEdtDMAgent::_dmaThread> _car Idle: "<<_car<<endl;
      setCARIdle(_car);
    }
  }
  
  // clear the ThrdId
  dma->_dmaThrdId = 0;

  // upon return from edt dma loop, unlock
  ::pthread_mutex_unlock(&dma->_theEdtMutex);
  clog<<"UFGemEdtDMAgent::_dmaThread> dma completed, edt mutex unlocked..."<<endl;

  // if macro, cannot use global scope '::' notation:
  pthread_cleanup_pop(0);

  int exitstat;
  ::pthread_exit(&exitstat);

  return p;
} // _dmaThread

// helpers
int UFGemEdtDMAgent::_allocNamedBuffs(const string& instrum) {
  //clog<<"UFGemEdtDMAgent::_allocNamedBuffs> instrum: "<< instrum<<endl;
  if( _nameIdxFrmBuff == 0 )
    _nameIdxFrmBuff = new map < string, int >;
  if( _dataFrmBuff == 0 )
    _dataFrmBuff = new deque < UFInts* >;

  vector< string > names;
  _maxBuff = _buffNames(instrum, names);

  string bufname;
  int elem = width() * height(); // if failed, use defaults
  vector< int > elems;
  for( int i = 0; i<_maxBuff; ++i )  elems.push_back(elem);
  if( elem == 2048*2048 ) { // flamingos1or2
    elems[1] = 1024*1024; elems[2] = 512*512;
  }
  for( int i = 0; i<_maxBuff; ++i ) {
    const int* imgdata = (const int*) new int[elems[i]];
    memset((int*)imgdata, 0, sizeof(int)*elems[i]); // clear buff
    bufname = names[i];
    (*_nameIdxFrmBuff)[bufname] = i;
    UFInts* ufi = new UFInts(bufname, imgdata, elems[i]); // shallow ctor
    //(*_dataFrmBuff)[i] = ufi;
    _dataFrmBuff->push_back(ufi);
    clog<<"UFGemEdtDMAgent::_allocNamedBuffs> i, elem, name: "<<(*_nameIdxFrmBuff)[bufname]<<", "<<elems[i]<<", "<<bufname<<endl;
  }
  return _maxBuff;  
}

int UFGemEdtDMAgent::_buffNames(const string& instrum, vector< string >& names) {
  names.clear();
  if( instrum.find("flam") != string::npos || 
      instrum.find("Flam") != string::npos || 
      instrum.find("FLAM")!= string::npos  ) {
    names.push_back(instrum+":FullImage");
    names.push_back(instrum+":1024Image");
    names.push_back(instrum+":512Image");
    return (int) names.size();
  }

  // if not flamingos, must be trecs or canaricam
  // according to varosi's FrameProcessThread.cc:
  names.push_back(instrum+":src1");        //0 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":ref1");        //1 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":ref2");        //2 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":src2");        //3 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":accum(src1)"); //4
  names.push_back(instrum+":accum(ref1)"); //5
  names.push_back(instrum+":accum(src2)"); //6
  names.push_back(instrum+":accum(ref2)"); //7
  names.push_back(instrum+":dif1");        //8
  names.push_back(instrum+":dif2");        //9
  names.push_back(instrum+":accum(dif1)"); //10
  names.push_back(instrum+":accum(dif2)"); //11
  names.push_back(instrum+":sig");         //12
  names.push_back(instrum+":accum(sig)");  //13

  return (int) names.size();
}

int UFGemEdtDMAgent::_replFrm(int* acqbuf) {
  // replication service, always send frame config followed by frame
  int w, h, numvals = UFEdtDMA::dimensions(w, h); 
  int cnt = UFEdtDMA::takeCnt(), dcnt = UFEdtDMA::doneCnt(), tcnt = UFEdtDMA::takeTotCnt();
  //if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_replFrm> w= "<<w<<", cnt= "<<cnt<<", h= "<<h<<", dcnt= "<<dcnt<<", tcnt= "<<tcnt
	<<", _replicate: "<<UFRndRobinServ::_replicate->description()<<endl;

  string t = currentTime();
  if( UFRndRobinServ::_replicate ) {
    if( !UFRndRobinServ::_replicate->validConnection() ) {
      clog<<"UFGemEdtDMAgent::_replFrm> replication socket is stale?"<<_replicate->description()<<endl;
      //UFRndRobinServ::_replicate->close(); // assume signal handler already closed it.
      //delete UFRndRobinServ::_replicate;
      //UFRndRobinServ::_replicate = 0; // _replicate now inherited from RndRobinServ
      return 0;
    }
  }
  bool sendthread = false; // threaded();
  if( !sendthread ) { // synchronous send can re-use existing buf (shallor ctor)
    // sending synchronously avoids copying the frame to a new buffer...
    UFInts acqfrm("EdtAcqBuf", (const int*) acqbuf, numvals);
    acqfrm.setSeq(_postcnt, _totalcnt);
    UFRndRobinServ::_replicate->send(acqfrm);
    return 1;
  }
  // must allocate off heap and pass ptr to new thread
  // this will presumably be freed by the sendThread
  std::vector < UFProtocol* >* ufv = new (nothrow) std::vector < UFProtocol* >;
  if( ufv == 0 ) {
    clog<<"UFGemEdtDMAgent::_replFrm> replication sendThread protocol vec. cannot be allocated."<<endl;
    return 0;
  }
  UFInts* acqfrm = new UFInts("EdtAcqBuf", (int*) acqbuf, numvals); // deep ctor
  if( acqfrm == 0 ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFGemEdtDMAgent::_replFrm> replication sendThread protocol Image. cannot be allocated."<<endl;
    delete ufv;
    return 0;
  }
  acqfrm->setSeq(_postcnt, _totalcnt);
  /*
  UFFrameConfig* frmconf = new UFFrameConfig(w, h);
  if( frmconf == 0 ) {
    clog<<"UFGemEdtDMAgent::_replFrm> replication sendThread protocol FrameConf cannot be allocated."<<endl;
    return 0;
  }
  frmconf->setDMACnt(dcnt); frmconf->setImageCnt(cnt); frmconf->setFrameObsSeqNo(cnt); frmconf->setFrameObsSeqTot(tcnt);
  ufv->push_back(frmconf);
  clog<<"UFGemEdtDMAgent::_replFrm> "<<t<<" send FrameConfig and ImgFrame to replication client"<<endl;
  */
  ufv->push_back(acqfrm);
  //if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_replFrm> "<<t<<" send ImgFrame to replication client"<<endl;
  int rcnt = (int) ufv->size();
  pthread_t sthrd = UFRndRobinServ::_replicate->sendThread(ufv); // will free ufv and its contents
  //if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_replFrm> "<<t<<" spawned thread to send FrameImage: "<<sthrd<<endl;
  return rcnt;
}

int UFGemEdtDMAgent::_histFrm(int* acqbuf) {
  int w, h, elem = UFEdtDMA::dimensions(w, h); 
  int cnt = UFEdtDMA::takeCnt(), dcnt = UFEdtDMA::doneCnt(), tcnt = UFEdtDMA::takeTotCnt();
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_histFrm> w= "<<w<<", h= "<<h
	<<", cnt= "<<cnt<<", dcnt= "<<dcnt<<", tcnt= "<<tcnt<<endl;

  // Flamingos frames go into 3 buffers:

  if( (elem < 2048*2048 ) ) {
    clog<<"UFGemEdtDMAgent::_histFrm> only supporting flamingos 2048x2048 frames now..."<<endl;
    return 0;
  }

  if( _dataFrmBuff == 0 ) {
    clog<<"UFGemEdtDMAgent::_histFrm> no history/qlook frames allocated?"<<endl;
    return -1;
  }
  if( _dataFrmBuff->size() < 3 ) {
    clog<<"UFGemEdtDMAgent::_histFrm> expected 3 qlook frames allocated, have: "<<_dataFrmBuff->size()<<endl;
  }
 
  //if( UFDeviceAgent::_verbose )
  // clog<<"UFGemEdtDMAgent::_histFrm> Flamingos..."<<endl;
  // lock the mutex and copy image frame:
  int m = ::pthread_mutex_trylock(&_theFrmMutex); // should fail if not owned by this thread
  if( m != 0 ) { // failed to get the lock
    clog<<"UFGemEdtDMAgent::_histFrm> unable to access frame buffer, mutex still locked..."<<endl;
    return 0;
  }
  //if( UFDeviceAgent::_verbose )
  //  clog<<"UFGemEdtDMAgent::_histFrm> mutex locked ..."<<endl;

  UFInts* ufi = (*_dataFrmBuff)[0]; // full image
  int elem1 = ufi->numVals();
  if( elem != elem1 ) {
    clog<<"UFGemEdtDMAgent::_histFrm> full frame size and EDT conf. mismatch, edt: "<<elem<<", frame buff: "<<elem1<<endl;
    return -2;
  }
    
  int* img2048 = ufi->valInts(); //(int*) ufi->valData();

  ufi = (*_dataFrmBuff)[1]; // 1/4 image
  int elem4 = ufi->numVals();
  int* img1024 = ufi->valInts(); //(int*) ufi->valData();

  ufi = (*_dataFrmBuff)[2]; // 1/16 image
  int elem16 = ufi->numVals();
  int* img512 = ufi->valInts(); //(int*) ufi->valData();
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_histFrm> full pixcnt= "<<elem1<<", 1/4= "<<elem4<<", 1/16= "<<elem16<<endl;

  // replace old (full) image with new:
  //for( int i = 0; i < elem; ++i )
  //  img2048[i] = acqbuf[i];
  memcpy((char*)img2048, (char*)acqbuf, elem*sizeof(int));

  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_histFrm> eval 2x2 avg. (1024x1024 pixcnt)..."<<endl;
  // 2x2 avg:
  int w2 = w/2, h2 = h/2;
  //double sum1024= 0.0;
  int i, j, k, kk0= 0, kk1= 0;
  for( i= 0, k= 0; i < h2-1; ++i ) {
    for( j = 0; j < w2-1; ++j, ++k ) {
      kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
	img1024[k] = (img2048[kk0] + img2048[kk0 + 1] + img2048[kk1] + img2048[kk1 + 1]) / 4;
    }
    kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
    img1024[k++] = (img2048[kk0] + img2048[kk1]) / 2;
  }
  //if( UFDeviceAgent::_verbose )
  //  clog<<"UFGemEdtDMAgent::_histFrm> eval 2x2 avg."<<endl;

  // 4x4 avg:        
  if( UFDeviceAgent::_verbose )
    clog<<"UFGemEdtDMAgent::_histFrm> eval 4x4 avg. (512x512 pixcnt)..."<<endl;
  int w4 = w2/2, h4 = h2/2;
  //double sum512= 0.0;
  for( i= 0, k= 0; i < h4-1; ++i ) {
    for( j = 0; j < w4-1; ++j, ++k ) {
      int kk0 = i*w2 + 2*j, kk1 = (i+1)*w2 + 2*j;
	img512[k] = (img1024[kk0] + img1024[kk0 + 1] + img1024[kk1] + img1024[kk1 + 1]) / 4;
    }
    kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
    img512[k++] = (img1024[kk0] + img1024[kk1]) / 2;
  }

  //if( UFDeviceAgent::_verbose )
  //  clog<<"UFGemEdtDMAgent::_histFrm> eval 4x4 avg."<<endl;

  m = ::pthread_mutex_unlock(&_theFrmMutex);
  if( m != 0 ) // failed to unlock
    clog<<"UFGemEdtDMAgent::_histFrm> failed mutex unlocked? ..."<<endl;
  //else if( UFDeviceAgent::_verbose )
  // clog<<"UFGemEdtDMAgent::_histFrm> mutex unlocked, copied new frame elem: "<<elem<<" ..."<<endl;

  return (int) _dataFrmBuff->size();
}

// simulation mode
void UFGemEdtDMAgent::_simacq(unsigned char* acqbuf, char* fitsbuf, int sz, const char* filenm) {
  int elem = 320*240; // TReCS/CanariCam
  if( sz >= 2048*2048*(int)sizeof(int) )
    elem = 2048*2048; // Flamingos1/2
  else if( sz >= 1024*1024*(int)sizeof(int) )
    elem = 1024*1024; // test?
  else if( sz >= 512*512*(int)sizeof(int) )
    elem = 512*512; // test?

  int* simbuf = (int*) acqbuf;
  clog<<"UFGemEdtDMAgent::_simacq> _postcnt: "<<_postcnt<<", sz: "<<sz<<", elem: "<<elem<<", acqbuf[0]: "<<simbuf[0]<<endl;
  for( int i = 0; i < elem; ++i ) {
    simbuf[i] = i + _postcnt;
  }
  clog<<"UFGemEdtDMAgent::_simacq> _postcnt: "<<_postcnt<<", sz: "<<sz<<", elem: "<<elem<<", acqbuf[elem-1]: "<<simbuf[elem-1]<<endl;
  clog<<"UFGemEdtDMAgent::_simacq> central pixels: "<<simbuf[elem/2-1]<<", "<<simbuf[elem/2]<<", "<<simbuf[elem/2+1]<<endl;
  return;
}

void UFGemEdtDMAgent::_writeImg(char* fits, int sz, const char* filenm, bool png, bool jpeg) {
  if( _scale == 0 ) _scale = 1; // compiler/thread bug?
  UFImgHandler::_scale = _scale;
  UFImgHandler::writePngJpeg(fits, sz, filenm, png, jpeg);
}

#endif // UFGemEdtDMAgent
