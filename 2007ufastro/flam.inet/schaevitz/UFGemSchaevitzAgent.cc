#if !defined(__UFGemSchaevitzAgent_cc__)
#define __UFGemSchaevitzAgent_cc__ "$Name:  $ $Id: UFGemSchaevitzAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemSchaevitzAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemSchaevitzAgent.h"
#include "UFSADFITS.h"

string UFGemSchaevitzAgent::_currentVolt; // measured voltage
string UFGemSchaevitzAgent::_currentDisp; // displacement in mm

string UFGemSchaevitzAgent::getCurrent(string& v, string& d) { 
  v =_currentVolt; d = _currentDisp;
  return v + ", " + d;
}

// helper for ctors:
void UFGemSchaevitzAgent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //    _confGenSub = _epics + ":ec:configG";
    _confGenSub = "";
    //_heart = _epics + ":ec:heartbeat.lvdt";
    _heart = _epics + ":ec:LVDTheartbeat.VAL";
    // presumably these are == 0 when lvdt is not powered on, < 0 when com. error
    //for( int i = 0; i < (int) UFSADFITS::_eclvdtsad->size(); ++i )
    //_StatRecs.push_back((*UFSADFITS::_eclvdtsad)[i] + ".inp");
    //_StatRecs.push_back(instrum + ":sad:LVDTVLTS.INP"); // current voltage reading
    _StatRecs.push_back(instrum + ":sad:LVDTVLTS.VAL"); // current voltage reading
    //_StatRecs.push_back(instrum + ":sad:LVDTDISP.INP"); // (LED) displayed voltage reading
    _StatRecs.push_back(instrum + ":sad:LVDTDISP.VAL"); // (LED) displayed voltage reading
    _statRec = _StatRecs[0];
  }
  else {
    _epics = "false";
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
}

// ctors:
UFGemSchaevitzAgent::UFGemSchaevitzAgent(int argc, char** argv, char** env) : UFGemDeviceAgent(argc, argv, env) {
  // use ancillary function to retrieve LVDT readings
  _flush = 0.25; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFGemSchaevitzAgent::UFGemSchaevitzAgent(const string& name,
					     int argc, char** argv, char** env) : UFGemDeviceAgent(name, argc, argv, env) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.25; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

// static funcs:
int UFGemSchaevitzAgent::main(int argc, char** argv, char** env) {
  UFGemSchaevitzAgent ufs("UFGemSchaevitzAgent", argc, argv, env); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemSchaevitzAgent::options(string& servlist) {
  UFDeviceAgent::_config = _config = new UFLVDTConfig(); // needs to be proper device subclass (GP345)
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  // g-p defaults for ufinstrum
  // although Perle ports 1-i-N default to tcp port 10000 + i
  // we reconfigure the Perle to the original Annex tcp ports 7000 + i
  _config->_tsport = _config->_tsport = 7006;
  _config->_tshost = _config->_tshost = "flamperle"; // "192.168.111.125";

  string arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" ) {
    _config->_tsport = atoi(arg.c_str());
    // power outlet # should match Perle port # (6)
    UFLVDTConfig::_outlet = _config->_tsport - 7000;
  }
  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());


  arg = findArg("-on");
  if( arg != "false" )
    UFLVDTConfig::_powered = true;
  arg = findArg("-off");
  if( arg != "false" )
    UFLVDTConfig::_powered = false;

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    UFTermServ::_verbose = _verbose;
    //UFSocket::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52006;
  }
  if( _verbose ) {
    clog<<"UFGemSchaevitzAgent::options> _epics: "<< _epics<<endl;
    clog<<"UFGemSchaevitzAgent::options> set port to GemSchaevitz port "<<port<<endl;
  }
  return port;
}

// override base class startup here to avoid connect to term. serv
void UFGemSchaevitzAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemSchaevitzAgent::startup> established signal handler."<<endl;

  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm or perle should initialize
  // a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemSchaevitzAgent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemSchaevitzAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemSchaevitzAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemSchaevitzAgentt::init> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemSchaevitzAgentt::init> Ecpics CA child proc. failed to start"<<endl;
  }
  bool powered =  UFLVDTConfig::_powered; // on startup this should false, but cmd option may set true
  string lvdt; // cmd & reply strings 
  time_t clck = time(0);
  if( clck % 2 == 0 )
    lvdt = "+222.00";
  else
    lvdt = "+111.99";
  if( !_sim ) {
    if( _verbose )
	clog << "UFGemSchaevitzAgent::init> connect to PerleConsole: "<<tshost<<" on port= " <<tsport<<endl;
    // connect to Baytech and turn on LVDT outlet, then connect to Perle terminal server LVDT port:
    ts = _config->connect(tshost, tsport);
    if( ts == 0 || _config->_devIO == 0 )
      clog<<"UFGemSchaevitzAgent> failed connection to Perle: "<<tshost<<" on LVDT port= " <<tsport<<endl;
    else
      clog<<"UFGemSchaevitzAgentt::init> connected to Perle: "<<tshost<<" on LVDT port= " <<tsport<<endl;
  }
  if( ts == 0 ) {
    _sim = true;
    clog<<"UFGemSchaevitzAgentt::init> simulate connection to Perle -> LVDT..."<<endl;
    return ts; // _config->_devIO;
  }

  // test connectivity to schaevitz lvdt:
  string lvcmd = UFLVDTConfig::setInputDefault();
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.schaevitzlvdt."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemSchaevitzAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  */

  clog<<"UFGemSchaevitzAgentt::init> turned on Schaevitz LVDT,  set defaults..."<<endl;
  /*
  string btech = UFLVDTConfig::pwrOn();
  if( btech.empty() ) {
    clog<<"UFGemSchaevitzAgentt::init> Schaevitz LVDT PowerOn failed..."<<endl;
    //_config->_devIO->close();
    //delete _config->_devIO; _config->_devIO = 0;
    return _config->_devIO;
  }
  else {
    UFRuntime::rmJunk(btech); 
    size_t nlpos = btech.rfind("\n");
    while( nlpos != string::npos ) { btech.erase(nlpos, 1); nlpos = btech.rfind("\n"); }
    clog<<"UFGemSchaevitzAgentt::init> Schaevitz LVDT Baytech outlet: "<<UFLVDTConfig::_outlet<<btech<<endl;
  }
  */
 
  int nc = _config->_devIO->submit(lvcmd, lvdt, _flush);
  if( nc <= 0 ) {
    clog<<"UFGemSchaevitzAgentt::init> Schaevitz reading failed..."<<endl;
    //_config->_devIO->close();
    //delete _config->_devIO;  _config->_devIO = 0;
    //exit(-1);
    return _config->_devIO;; 
  }
  UFRuntime::rmJunk(lvdt); 
  size_t nlpos = lvdt.rfind("\n");
  while( nlpos != string::npos ) { lvdt.erase(nlpos, 1); nlpos = lvdt.rfind("\n"); }
  clog<<"UFGemSchaevitzAgentt::init> set input default reply: "<<lvdt<<endl;
  lvcmd = UFLVDTConfig::getMeasVal();
  nc = _config->_devIO->submit(lvcmd, lvdt, _flush);
  if( nc <= 0 ) {
    clog<<"UFGemSchaevitzAgentt::init> Schaevitz reading failed..."<<endl;
    return 0; 
  }
  UFRuntime::rmJunk(lvdt); 
  nlpos = lvdt.rfind("\n");
  while( nlpos != string::npos ) { lvdt.erase(nlpos, 1); nlpos = lvdt.rfind("\n"); }

  clog<<"UFGemSchaevitzAgent> measured val: "<<lvdt<<endl;
  if( !_statRec.empty() ) sendEpics(_statRec, lvdt);
  _DevHistory.push_back(lvdt);
 
  if( powered ) { // -on indicated, leave it on?
    clog<<"UFGemSchaevitzAgent::init> leave Schaevitz LVDT Powered On..."<<endl;
    return _config->_devIO;
  }

  clog<<"UFGemSchaevitzAgent::init> leave Schaevitz LVDT Powered Off..."<<endl;
  string btech = UFLVDTConfig::pwrOff();
  if( btech.empty() ) {
    clog<<"UFGemSchaevitzAgentt::init> Schaevitz LVDT PowerOff failed..."<<endl;
    //_config->_devIO->close();
    //delete _config->_devIO;
    return _config->_devIO;;
  }
  else {
    UFRuntime::rmJunk(btech);
    nlpos = btech.rfind("\n");
    while( nlpos != string::npos ) { btech.erase(nlpos, 1); nlpos = btech.rfind("\n"); }
    clog<<"UFGemSchaevitzAgent> (powerOff) Schaevitz LVDT Baytech outlet: "<<btech<<endl;
  }
  /*
  int nc = ::write(fd, lvdt.c_str(), lvdt.length());
  if( nc <= 0 ) {
    clog<<"UFGemSchaevitzAgent> status log failed..."<<endl;
  }
  ::close(fd);
  */
  return _config->_devIO;
} // init

void* UFGemSchaevitzAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  // since we cannot assume the epics db is available at startup,
  // attempt this one-time init each time we are invoked:
  /*
  if( _epics != "false" && _StatList.size() == 0 ) {
    string conf = _confGenSub + ".VALC";
    clog<<"UFGemSchaevitzAgent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    int nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemSchaevitzAgent::ancillary> epics initialization succeeded."<<endl;
      _heart = _StatList[2]; // ?heartbeat rec. name is 3nd entry in this list
    }
    else {
      clog<<"UFGemSchaevitzAgent::ancillary> epics initialization failed."<<endl;
    }
    conf = _confGenSub + ".VALA";
    clog<<"UFGemSchaevitzAgent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemSchaevitzAgent::ancillary> epics initialization succeeded."<<endl;
      _statRec = _StatList[2]; // SB status rec. name is 3d entry in this list
    }
  }
  */
  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe && _epics != "false" && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str();
    if( _verbose )
      clog<<"UFGemSchaevitzAgent::ancillary> heart= "<<val<<", fd: "<<fileno(_capipe)<<endl;
    updateEpics(_heart, val);
    delete s.str();
  }

  if( !UFLVDTConfig::_powered )
    if( _verbose )
      clog<<"UFGemSchaevitzAgent::ancillary> lvdt not ON now: "<<currentTime()<<endl;

  string lvdtrd = UFLVDTConfig::getMeasVal(); // lvdt address rdout cmd
  string replylvdt = _currentVolt; // whatever
  // _sim:
  clck = time(0);
  if( clck % 5 == 0 && UFLVDTConfig::_powered ) {
    string btstat = UFLVDTConfig::refresh();
    clog<<"UFGemSchaevitzAgent> refresh: "<<btstat<<endl;
  }

  if( clck % 5 == 0 )
    replylvdt = "+2.222";
  else
    replylvdt = "+1.111";

  // assume blocking i/o for now...
  //if( clck % 5 == 0 &&  !_sim && _config && _config->_devIO != 0 && UFLVDTConfig::_powered ) {
  if( !_sim && _config && _config->_devIO != 0 && UFLVDTConfig::_powered ) {
    if( _config->_devIO == 0 ) {
      clog<<"UFGemSchaevitzAgent> no connection to Perle LVDT port?"<<endl;
      exit(-1);
    }
    // clog<<"UFGemSchaevitzAgent> Schaevitz reading cmd: "<<lvdtrd<<endl;
    int nc = _config->_devIO->submit(lvdtrd, replylvdt, _flush); // expect single line reply
    if( nc <= 0 ) {
      clog<<"UFGemSchaevitzAgent> Schaevitz (1st try) reading failed, try again..."<<endl;
      //clog<<"UFGemSchaevitzAgent> Schaevitz (1st try) reading failed, try reconnect..."<<endl;
      //_config->_devIO->reconnect();
      nc = _config->_devIO->submit(lvdtrd, replylvdt, _flush); // try again
      if( nc <= 0 )
        clog<<"UFGemSchaevitzAgent> Schaevitz reading failed 2nd try...and try later"<<endl;
      return p;
    }
  } // !_sim and powered on

  UFRuntime::rmJunk(replylvdt);
  size_t nlpos = replylvdt.rfind("\n");
  while( nlpos != string::npos ) { replylvdt.erase(nlpos, 1); nlpos = replylvdt.rfind("\n"); }
  string t = UFRuntime::currentTime();
  if( clck % 5 == 0 ) {
    clog<<"UFGemSchaevitzAgent::ancillary> "<<t<<" Voltage reading: "<<replylvdt;
    if( _sim )
      clog<<" (sim)"<<endl;
    else if( !UFLVDTConfig::_powered  )
      clog<<" (poweredOff)"<<endl;
    else
      clog<<endl;
  }
  if( replylvdt == "" || replylvdt == " " ) {
    clog<<"UFGemSchaevitzAgent::ancillary> no lvdt value now: "<<currentTime()<<endl;
    return p;
  }
  if( replylvdt.find("unknown") != string::npos ) {
    clog<<"UFGemSchaevitzAgent::ancillary> lvdt value: "<<replylvdt<<endl;
    return p;
  }

  _currentVolt = replylvdt; // either sim or real reply
  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();
  _DevHistory.push_back(replylvdt);

  replylvdt == "";
  // assume blocking i/o for now...
  //if( clck % 5 == 0 &&  !_sim && _config && _config->_devIO != 0 && UFLVDTConfig::_powered ) {
  if( !_sim && _config && _config->_devIO != 0 && UFLVDTConfig::_powered ) {
    lvdtrd = UFLVDTConfig::getDispVal(); // lvdt display rdout
    //clog<<"UFGemSchaevitzAgent> Schaevitz reading cmd: "<<lvdtrd<<endl;
    int nc = _config->_devIO->submit(lvdtrd, replylvdt, _flush); // expect single line reply
    if( nc <= 0 ) {
      clog<<"UFGemSchaevitzAgent> Schaevitz (1st try) reading failed, try reconnect?..."<<endl;
      //_config->_devIO->reconnect();
      if( _config->_devIO == 0 ) {
        clog<<"UFGemSchaevitzAgent> failed connection to annex/iocom."<<endl;
        exit(-1);
      }
      nc = _config->_devIO->submit(lvdtrd, replylvdt, _flush); // try again
      if( nc <= 0 ) {
        clog<<"UFGemSchaevitzAgent> Schaevitz reading failed 2nd try... quite and try later"<<endl;
      return p;
    }
    }
  } // !_sim

  UFRuntime::rmJunk(replylvdt);
  nlpos = replylvdt.rfind("\n");
  while( nlpos != string::npos ) { replylvdt.erase(nlpos, 1); nlpos = replylvdt.rfind("\n"); }
  string tled = UFRuntime::currentTime();
  if( clck % 5 == 0 ) {
    clog<<"UFGemSchaevitzAgent::ancillary> "<<tled<<" Voltage displayed (LED): "<<replylvdt;
    if( _sim )
      clog<<" (sim)"<<endl;
    else if( !UFLVDTConfig::_powered  )
      clog<<" (poweredOff)"<<endl;
    else
      clog<<endl;
  }
  if( replylvdt == "" || replylvdt == " " ) {
    clog<<"UFGemSchaevitzAgent::ancillary> no lvdt value now: "<<currentTime()<<endl;
    return p;
  }

  if( clck % 5 == 0 && replylvdt.find("unknown") != string::npos ) {
    clog<<"UFGemSchaevitzAgent::ancillary> lvdt value: "<<replylvdt<<endl;
    return p;
  }
  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();

  _DevHistory.push_back(replylvdt);
  _currentDisp = replylvdt; // either sim or realy reply 

  // update the status value(s):
  if( _capipe && _epics != "" && _epics != "false" && _statRec != "" ) {
    ///if( _verbose &&  clck % 5 == 0 )
    if( clck % 5 == 0 )
      clog<<"UFGemSchaevitzAgent::ancillary> vac: "<<_currentVolt<<", "<<_currentDisp<<", fd: "<<fileno(_capipe)<<endl;
    sendEpics(_StatRecs[0], _currentVolt);
    sendEpics(_StatRecs[1], _currentDisp);
  }
   
  return p;
} //ancillary

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// the Gemini/Epics version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error/alarm condition should be indicated
int UFGemSchaevitzAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find("f2:") != string::npos || act->clientinfo.find("flam:") != string::npos || 
      act->clientinfo.find("foo:") != string::npos || act->clientinfo.find("flam2:") != string::npos ||
      act->clientinfo.find("fu:") != string::npos ) { // must be epics client
    reply_expected = false;
    clog<<"UFGemSchaevitzAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemSchaevitzAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemSchaevitzAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string pwrstat, replylvdt = "unknown";
  string car, sad, errmsg, hist, raw;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= _sim;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];
    //if( _verbose ) {
      clog<<"UFGemSchaevitzAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemSchaevitzAgent::action> cmdimpl: "<<cmdimpl<<endl;
      // }
    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return up processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int)replies.size();
      }
      else {
        replies.push_back(_config->status(this)); 
	return (int)replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("sad") == 0 ||
	cmdname.find("Sad") == 0 ||
        cmdname.find("SAD") == 0 ) {
      reply_expected= false;
      sad = cmdimpl;
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> SAD: "<<sad<<endl;
      continue;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      raw = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> Raw: "<<raw<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("on") != string::npos ||
	     cmdname.find("On") != string::npos || 
	     cmdname.find("ON") != string::npos ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
      pwrstat = UFLVDTConfig::pwrOn();  // this really should be parsed for any error condition
      stat = pwrstat.length();
      if( stat > 0  ) act->cmd_reply.push_back(pwrstat);
    }
     else if( cmdname.find("off") != string::npos ||
	     cmdname.find("Off") != string::npos || 
	     cmdname.find("OFF") != string::npos ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
      pwrstat = UFLVDTConfig::pwrOff(); // this really should be parsed for any error condition
      stat = pwrstat.length();
      if( stat > 0  ) act->cmd_reply.push_back(pwrstat);
    }
   else if( cmdname.find("sim") != 0 ||
	     cmdname.find("Sim") != 0 || 
	     cmdname.find("SIM") != 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSchaevitzAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemSchaevitzAgent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
        return (int)replies.size();
      }
      else {
	delete rep; rep = 0;
        return 0;      
      }
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    if( !sim && _config != 0 && _config->_devIO && raw != "" ) {
      int nr = _config->validCmd(raw);
      if( nr > 0 ) { // valid device cmd
        //if( _verbose )
	clog<<"UFGemSchaevitzAgent::action> submit raw: "<<raw<<endl;
        stat = _config->_devIO->submit(raw, replylvdt, _flush); // expect single line reply
        //if( _verbose )
	clog<<"UFGemSchaevitzAgent::action> reply: "<<replylvdt<<endl;
        if( stat > 0 ) {
          if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(replylvdt);
          act->cmd_reply.push_back(replylvdt);
	}
      }
      else { // default to history, but get new reading first
        string rd = UFLVDTConfig::getMeasVal();
	if( _config != 0 && _config->_devIO ) 
          stat = _config->_devIO->submit(rd, replylvdt, _flush); // expect single line reply
        if( stat > 0 ) {
	  if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(replylvdt);
	}
        int last = (int)_DevHistory.size() - 1;
	int cnt = atoi(cmdimpl.c_str());
	if( cnt <= 0 ) cnt = 1;
	if( cnt > last+1 ) cnt = last+1;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
    }
    else if( hist != "" ) {
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(cmdimpl.c_str());
      if( cnt <= 0 ) cnt = 1;
      if( cnt > last+1 ) cnt = last+1;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      // simulate an error condition?
      errmsg += cmdimpl;
      replylvdt = "Sim OK, sleeping 1/2 sec...";
      act->cmd_reply.push_back(replylvdt);
      UFPosixRuntime::sleep(0.5);
      if( car != "" && _epics != "false" && _capipe ) sendEpics(car, errmsg);
      if( _verbose ) {
        clog<<"UFGemSchaevitzAgent::action> device reply: "<<replylvdt<<endl;
	clog<<"UFGemSchaevitzAgent::action> simulating error condition..."<<endl;
      }
      delete rep; rep = 0;
      return 0;
    }
    else if( sim ) {
      time_t clck = time(0);
      if( clck % 2 == 0 )
        replylvdt = "+2.222";
      else
        replylvdt = "+1.111";
      act->cmd_reply.push_back(replylvdt);
    }
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( car != "" && _epics != "false" && _capipe ) setCARError(car, &errmsg);
    if( sad != "" && _epics != "false" && _capipe ) sendEpics(sad, replylvdt);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( car != "" && _epics != "false" && _capipe ) setCARIdle(car);
    if( sad != "" && _epics != "false" && _capipe ) sendEpics(sad, replylvdt);
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size();
  }

  delete rep; rep = 0;
  return 0;
} // action command

int UFGemSchaevitzAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemSchaevitzAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update);
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

#endif // UFGemSchaevitzAgent
