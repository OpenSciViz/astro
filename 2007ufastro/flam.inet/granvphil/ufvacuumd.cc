#if !defined(__ufvacuumd_cc__)
#define __ufvacuumd_cc__ "$Name:  $ $Id: ufvacuumd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufvacuumd_cc__;

#include "UFGranvilPhilAgent.h"

int main(int argc, char** argv) {
  return UFGranvilPhilAgent::main(argc, argv);
}
      
#endif // __ufvacuumd_cc__
