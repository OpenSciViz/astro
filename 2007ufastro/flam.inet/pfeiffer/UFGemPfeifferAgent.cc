#if !defined(__UFGemPfeifferAgent_cc__)
#define __UFGemPfeifferAgent_cc__ "$Name:  $ $Id: UFGemPfeifferAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemPfeifferAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemPfeifferAgent.h"
#include "UFPFConfig.h"
#include "UFSADFITS.h"

string UFGemPfeifferAgent::_currentvalA;
string UFGemPfeifferAgent::_currentvalB;

// helper for ctors:
void UFGemPfeifferAgent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //    _confGenSub = _epics + ":ec:configG";
    _confGenSub = "";
    //_heart = _epics + ":ec:heartbeat.vacuum";
    _heart = _epics + ":ec:Vacheartbeat.VAL";
     for( int i = 0; i < (int) UFSADFITS::_ecpfvacsad->size(); ++i )
      //_StatRecs.push_back((*UFSADFITS::_ecpfvacsad)[i] + ".INP");
      _StatRecs.push_back((*UFSADFITS::_ecpfvacsad)[i] + ".VAL");
    _statRec = _StatRecs[0];
  }
  else {
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
}

// ctors:
UFGemPfeifferAgent::UFGemPfeifferAgent(int argc, char** argv, char** env) : UFGemDeviceAgent(argc, argv, env) {
  // use ancillary function to retrieve LS340 readings
  _flush = 0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFGemPfeifferAgent::UFGemPfeifferAgent(const string& name,
				       int argc, char** argv, char** env) : UFGemDeviceAgent(name, argc, argv, env) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

// static funcs:
int UFGemPfeifferAgent::main(int argc, char** argv, char** env) {
  UFGemPfeifferAgent ufs("UFGemPfeifferAgent", argc, argv, env); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemPfeifferAgent::options(string& servlist) {
  UFDeviceAgent::_config = _config = new UFPFConfig(); // needs to be proper device subclass (GP345)
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  // g-p defaults for ufinstrum
  _config->_tsport = _config->_tsport = 7004;
  _config->_tshost = _config->_tshost = "flamperle"; // "192.168.111.125";

  string arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    UFTermServ::_verbose = _verbose;
    //UFSocket::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52004;
  }
  if( _verbose ) {
    clog<<"UFGemPfeifferAgent::options> _epics: "<< _epics<<endl;
    clog<<"UFGemPfeifferAgent::options> set port to GemPfeiffer port "<<port<<endl;
  }
  return port;
}

// override base class startup here to avoid connect to term. serv
// if no motors are specified, assume simulation is desired,
// then do server listen stuff
void UFGemPfeifferAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemPfeifferAgent::startup> established signal handler."<<endl;

  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemPfeifferAgent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemPfeifferAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemPfeifferAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemPfeifferAgent> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemPfeifferAgent> Ecpics CA child proc. failed to start"<<endl;
  }
  if( !_sim ) {
    if( _verbose )
	clog << "UFGemPfeifferAgent::init> connect to PerleConsole: "<<tshost<<" on port= " <<tsport<<endl;
    // connect to terminal server:
    ts = _config->connect(tshost, tsport);
    if( ts == 0 || _config->_devIO == 0 ) {
      clog<<"UFGemPfeifferAgent> failed connection to annex/iocom."<<endl;
      exit(-1);
    }
  }
  // test connectivity to pfeiffer:
  string pf26x; // cmd & reply strings 
  time_t clck = time(0);
  if( clck % 2 == 0 )
    //pf26x = "4, 7.6100E+02,0, 7.1500E+00";
    pf26x = "0, 2.9100E-07,0, 6.5000E+02";
  else
    //pf26x = "0, 0.0000E+00,0, 0.0000E-00";
    pf26x = "0, 2.9400E-07,0, 6.5100E+02";

  string pfrd = "PRX"; // should be function in UFDeviceConfig subclass
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.pfeiffer262."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemPfeifferAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  */
  if( _sim ) {
    clog<<"UFGemPfeifferAgent> simulating connection to Pfeiffer 262."<<endl;
  }
  else {
    clog<<"UFGemPfeifferAgent> checking availiblity of Pfeiffer 262."<<endl;
    int nc = _config->_devIO->submit(pfrd, pf26x, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemPfeifferAgent> Pfeiffer reading failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
  }
  UFRuntime::rmJunk(pf26x);
  size_t nlpos = pf26x.rfind("\n");
  while( nlpos != string::npos ) { pf26x.erase(nlpos, 1); nlpos = pf26x.rfind("\n"); }
  clog<<"UFGemPfeifferAgent> reply: "<<pf26x<<endl;
  _DevHistory.push_back(pf26x);
  /*
  int nc = ::write(fd, pf26x.c_str(), pf26x.length());
  if( nc <= 0 ) {
    clog<<"UFGemPfeifferAgent> status log failed..."<<endl;
  }
  ::close(fd);
  */
  return _config->_devIO;
} //init

void* UFGemPfeifferAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  // since we cannot assume the epics db is available at startup,
  // attempt this one-time init each time we are invoked:
  /*
  if( _epics != "false" && _StatList.size() == 0 ) {
    string conf = _confGenSub + ".VALC";
    clog<<"UFGemPfeifferAgent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    int nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemPfeifferAgent::ancillary> epics initialization succeeded."<<endl;
      _heart = _StatList[2]; //  PF heartbeat rec. name is 3nd entry in this list
    }
    else {
      clog<<"UFGemPfeifferAgent::ancillary> epics initialization failed."<<endl;
    }
    conf = _confGenSub + ".VALA";
    clog<<"UFGemPfeifferAgent::ancillary> get _epics: "<<_epics<<", conf: "<<conf<<endl;
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr > 0 ) {
      clog<<"UFGemPfeifferAgent::ancillary> epics initialization succeeded."<<endl;
      _statRec = _StatList[2]; // PF status rec. name is 3d entry in this list
    }
  }
  */

  string pfrd = "PRX"; // should be function in UFDeviceConfig subclass
  string reply26x = "unknown@" + UFRuntime::currentTime();
  time_t clck = time(0);
  if( _sim ) {
    if( clck % 2 == 0 )
      reply26x = "  0, 2.9100E-07,0, 6.5000E+02";
    else
      reply26x = "  0, 2.9400E-07,0, 6.5100E+02";
  }

  // assume blocking i/o for now...
  if( !_sim && _config && _config->_devIO ) {
    int nc =_config->_devIO->submit(pfrd, reply26x, _flush); // expect single line reply
    if( nc <= 0 ) {
      clog<<"UFGemPfeifferAgent> Pfeiffer (1st try) reading failed, try reconnect?..."<<endl;
      //_config->_devIO->reconnect();
      if( _config->_devIO == 0 ) {
        clog<<"UFGemPfeifferAgent> failed connection to annex/iocom."<<endl;
        exit(-1);
      }
      nc =_config->_devIO->submit(pfrd, reply26x, _flush); // expect single line reply
      if( nc <= 0 )
        clog<<"UFGemPfeifferAgent> Pfeiffer reading failed 2nd try... quite and try later"<<endl;
    }
  } // !_sim

  UFRuntime::rmJunk(reply26x);
  // elim. leading, trailing, & mult. white-spaces in: "4, 7.6100E+02,0, 7.1500E+00 ";
  size_t nlpos = reply26x.rfind("\n");
  while( nlpos != string::npos ) { reply26x.erase(nlpos, 1); nlpos = reply26x.rfind("\n"); }
  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();
  _DevHistory.push_back(reply26x);

  //if( _verbose )
    clog<<"UFGemPfeifferAgent::ancillary> reading: "<<reply26x<<endl;

  // update the heartbeat:
  clck = time(0);
  if( _capipe && _epics != "false" && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str();
    if( _verbose )
      clog<<"UFGemPfeifferAgent::ancillary> heart= "<<val<<", fd: "<<fileno(_capipe)<<endl;
    updateEpics(_heart, val);
    delete s.str();
  }
  if( reply26x == "" || reply26x == " " ) {
    //clog<<"UFGemPfeifferAgent::ancillary> no vac. value now: "<<currentTime()<<endl;
    return p;
  }
  if( reply26x.find("unknown") != string::npos ) {
    //clog<<"UFGemPfeifferAgent::ancillary> vac. value: "<<reply26x<<endl;
    return p;
  }
  // reply must be parsed and formatted
  /*
  char* crep = (char*)reply26x.c_str();
  while( *crep == ' ' || *crep == '\t' || *crep == '\n' || *crep == '\r' ) ++crep;
  reply26x = crep; // eliminate whites
  */
  string delim = ",";
  int dlen = delim.length();
  int pos0= dlen + reply26x.find(delim);
  int pos1= dlen + reply26x.find(delim, pos0);
  int posr= dlen + reply26x.rfind(delim);
  /*
  int len = 0, rlen = reply26x.length();
  crep = (char*)reply26x.c_str();
  while( *crep++ != 0 && --rlen > 0 ) if( *crep != '\n' || *crep != '\r' ) ++len;
  */
  // presumably there are 2 pressure values
  string vacA= reply26x.substr(pos0, pos1-pos0-1);
  //string vacB= reply26x.substr(posr, len-posr); // suppress terminator(s)
  string vacB= reply26x.substr(posr); // suppress terminator(s)
  UFRuntime::rmJunk(vacA); UFRuntime::rmJunk(vacB);
  _currentvalA = vacA; _currentvalB = vacB; 

  // update the status value(s):
  if( _capipe && _epics != "false" && _statRec != "" ) {
    //if( _verbose )
      clog<<"UFGemPfeifferAgent::ancillary> vac: "<<vacA<<", "<<vacB<<endl; //<<", fd: "<<fileno(_capipe)<<endl;
    sendEpics(_StatRecs[0], vacB); // camera cryostat?
    sendEpics(_StatRecs[1], vacA); // mos cryostat?
  }
   
  return p;
} //ancillary

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// the Gemini/Epics version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error/alarm condition should be indicated
int UFGemPfeifferAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find(":") != string::npos ) { // must be epics client
    reply_expected = false;
    clog<<"UFGemPfeifferAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemPfeifferAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemPfeifferAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car, errmsg, hist = "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= _sim;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];
    if( _verbose ) {
      clog<<"UFGemPfeifferAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemPfeifferAgent::action> cmdimpl: "<<cmdimpl<<endl;
    }
    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int)replies.size();
      }
      else {
        replies.push_back(_config->status(this)); 
	return (int)replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemPfeifferAgent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemPfeifferAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      //sim = false;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemPfeifferAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") != 0 ||
	     cmdname.find("Sim") != 0 || 
	     cmdname.find("SIM") != 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemPfeifferAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemPfeifferAgent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
        return (int)replies.size();
      }
      else {
	delete rep; rep = 0;
        return 0;      
      }
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    string reply26x;
    if( !sim && _config != 0 && _config->_devIO ) {
      int nr = _config->validCmd(cmdimpl);
      if( nr > 0 ) { // valid device cmd
        if( _verbose ) clog<<"UFGemPfeifferAgent::action> submit raw: "<<cmdimpl<<endl;
        stat = _config->_devIO->submit(cmdimpl, reply26x, _flush); // expect single line reply
        if( _verbose ) clog<<"UFGemPfeifferAgent::action> reply: "<<reply26x<<endl;
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply26x);
        act->cmd_reply.push_back(reply26x);
      }
      else { // default to history, but get new reading first
        string pfrd = "PRX"; // should be function in UFDeviceConfig
	if( _config != 0 && _config->_devIO ) 
          stat = _config->_devIO->submit(pfrd, reply26x, _flush); // expect single line reply
        if( stat >= 0 ) {
	  if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply26x);
	}
        int last = (int)_DevHistory.size() - 1;
	int cnt = atoi(cmdimpl.c_str());
	if( cnt <= 0 ) cnt = 1;
	if( cnt > last+1 ) cnt = last+1;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
    }
    else if( hist != "" ) {
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(cmdimpl.c_str());
      if( cnt <= 0 ) cnt = 1;
      if( cnt > last+1 ) cnt = last+1;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      errmsg += cmdimpl;
      reply26x = "Sim OK, sleeping 1/2 sec...";
      act->cmd_reply.push_back(reply26x);
      UFPosixRuntime::sleep(0.5);
      if( car != "" && _epics != "false" && _capipe ) setCARError(car, &errmsg);
      if( _verbose ) {
        clog<<"UFGemPfeifferAgent::action> device reply: "<<reply26x<<endl;
	clog<<"UFGemPfeifferAgent::action> simulating error condition..."<<endl;
      }
      delete rep; rep = 0;
      return 0;
    }
    else if( sim ) {
      time_t clck = time(0);
      if( clck % 2 == 0 )
        reply26x = "4, 7.6100E+02,0, 3.2300E-02";
      else
        reply26x = "0, 0.0000E+00,0, 0.0000E-00";
      act->cmd_reply.push_back(reply26x);
    }
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( car != "" && _epics != "false" && _capipe ) setCARError(car, &errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( car != "" && _epics != "false" && _capipe ) setCARIdle(car);
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size();
  }

  delete rep; rep = 0;
  return 0;
} 

int UFGemPfeifferAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemPfeifferAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update);
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

#endif // UFGemPfeifferAgent
