#if !defined(__UFGemPfeifferAgent_h__)
#define __UFGemPfeifferAgent_h__ "$Name:  $ $Id: UFGemPfeifferAgent.h,v 0.3 2005/06/07 15:49:05 hon Exp $"
#define __UFGemPfeifferAgent_H__(arg) const char arg##UFGemPfeifferAgent_h__rcsId[] = __UFGemPfeifferAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFPFConfig.h"

class UFGemPfeifferAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemPfeifferAgent(int argc, char** argv, char** envv);
  UFGemPfeifferAgent(const string& name, int argc, char** argv, char** envv);
  inline virtual ~UFGemPfeifferAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);
  // override UFGemDevAgent virtual:
  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // in adition to establishing the iocomm/annex connection to
  // the motors, open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline string getCurrent(string& valA, string& valB)
    { valA =_currentvalA; valB = _currentvalB; return valA + ", " + valB; }


protected:
  static string _currentvalA, _currentvalB;
};

#endif // __UFGemPfeifferAgent_h__
      
