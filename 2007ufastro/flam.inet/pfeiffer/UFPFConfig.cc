#if !defined(__UFPFConfig_cc__)
#define __UFPFConfig_cc__ "$Name:  $ $Id: UFPFConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPFConfig_cc__;

#include "UFPFConfig.h"
#include "UFGemPfeifferAgent.h"
#include "UFFITSheader.h"

UFPFConfig::UFPFConfig(const string& name) : UFDeviceConfig(name) {}

UFPFConfig::UFPFConfig(const string& name, const string& tshost, int tsport) : UFDeviceConfig(name, tshost, tsport) {}

string UFPFConfig::terminator() { return "\r\n"; }

// connect calls create which then connects
UFTermServ* UFPFConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }
  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);
  return _devIO;
}

vector< string >& UFPFConfig::queryCmds() {
  if( _queries.size() > 0 ) 
    return _queries;

  _queries.push_back("PR1"); // pressure reading sensor 1
  _queries.push_back("PR2"); // pressure reading sensor 2
  _queries.push_back("PRX"); // pressure reading sensors 1 & 2

  return _queries;
}

vector< string >& UFPFConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("COM"); // continuous mode
  _actions.push_back("RES"); // reset
  _actions.push_back("RST"); // r232 test
  _actions.push_back("SEN"); // sensor on/off

  return _actions;
}

int UFPFConfig::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      return 1;
    }
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }

  clog<<"UFPFConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFPFConfig::status(UFDeviceAgent* da) {
  UFGemPfeifferAgent* dapfp = dynamic_cast< UFGemPfeifferAgent* > (da);
  if( dapfp->_verbose )
    clog<<"UFLSConfig::status> "<<dapfp->name()<<endl;

  string a, b;
  string g = dapfp->getCurrent(a, b); 
  string vaca= "MOSVaccum == ";
  vaca += a;
  vaca += " Torr";
  vector< string > v;
  v.push_back(vaca);
  string vacb= "CamVaccum == ";
  vacb += b;
  vacb += " Torr";
  v.push_back(vacb);

  return new UFStrings(dapfp->name(), v);
}

UFStrings* UFPFConfig::statusFITS(UFDeviceAgent* da) {
  UFGemPfeifferAgent* dapfp = dynamic_cast< UFGemPfeifferAgent* > (da);
  if( dapfp->_verbose )
    clog<<"UFLSConfig::status> "<<dapfp->name()<<endl;

  string a, b;
  string g = dapfp->getCurrent(a, b); 

  map< string, string > valhash, comments;
  valhash["MOSVacuu"] = a;
  comments["MOSVacuu"] = "Torr";
  valhash["CamVacuu"] = b;
  comments["CamVacuu"] = "Torr";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(dapfp->name());

  return ufs;
}

#endif // __UFPFConfig_cc__
