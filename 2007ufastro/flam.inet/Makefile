# RCS: 
# $Name:  $ $Id: Makefile 14 2008-06-11 01:49:45Z hon $
#
# Macros:
SHELL := /bin/tcsh -f

DOMAIN := $(shell domainname)


ifndef GCCID
  GCCID = $(shell ../.gccv)
endif

include ../.makerules

#reset RCSMASTER & DEV: 
RCSDEV := $(shell \pwd)
ifeq ($(UNAME),Linux)
# RCSMASTER := $(shell \ls -l | \grep RCS | \cut -d'>' -f2 | \cut -c2 | sed 's^/RCS*^^')
  RCSMASTER := $(shell /bin/ls -l --time-style=locale | grep RCS | awk '{print $$11}' | sed 's^/RCS*^^')
else
  RCSMASTER := $(shell /bin/ls -l | grep RCS | awk '{print $$11}' | sed 's^/RCS*^^')
endif

STDLIBA := $(shell find $(GCCLIB) -follow -type f -name "*std*.a" |tail -1)
STDLIBSO := $(shell find $(GCCLIB) -follow -type f -name "*std*.so" |tail -1)

RCSLIST := ReadMe Makefile

FLAMINETDIR := \
automation \
granvphil \
pfeiffer \
lakeshore \
portescap \
symbol \
schaevitz \
ufmce \
bootexec \
imgprocs \
flamdhs 

UFRELEASE := UFFLAM2

# Targets:
all: show init install
	@echo made all

show:
	-@echo JDATE= $(JDATE)
	-@echo RCSMASTER= $(RCSMASTER)
	-@echo JAR= $(JAR)
	-@echo LIB= $(LIB)
	-@echo EXE= $(EXE)
	-@echo STARGET= $(STARGET)
	-@echo TARGET= $(TARGET)
	-@echo JTARGET= $(JTARGET)
	-@echo COMPILER_ID= $(COMPILER_ID)
	-@echo OS= $(OS)
	-@echo OS_VERSION= $(OS_VERSION)
	-@echo VX_TARGET= $(VX_TARGET)
	-@echo RCSLIST = $(RCSLIST)
	-@echo PUBHDR = $(PUBHDR)
	-@echo PUBLIB = $(PUBLIB)
	-@echo PUBSLIB = $(PUBSLIB)
	-@echo PUBEXE = $(PUBEXE) 
	-@echo PUBBIN = $(PUBBIN)
	-@echo PUBSBIN = $(PUBSBIN)
	-@echo UFINSTALL = $(UFINSTALL)
	-@echo INSTALL_HDR = $(INSTALL_HDR)
	-@echo RELEASE = $(RELEASE)
	-@echo UFRELEASE = $(UFRELEASE)
	-@echo FLAMINETDIR = $(FLAMINETDIR)
	-@echo GCCIDM = $(GCCIDM)
	-@echo GCCLIB = $(GCCLIB)
	-@echo STDLIBA = $(STDLIBA)
	-@echo STDLIBSO = $(STDLIBSO)
	-@echo DOMAIN = $(DOMAIN)

# force source of .ufcshrc before make
init: initco
	source ../.ufcshrc; $(MAKE) -i -k .init

.init:
	$(foreach i, $(FLAMINETDIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET); popd;)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) init; popd;)

initco: 
	if( -e RCS ) co $(COFLAGS) RCS/*,v
	$(foreach i, $(FLAMINETDIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(TARGET)/ ) mkdir -p $i/$(TARGET); \
	if ( ! -e $i/$(TARGET)/.depend ) touch $i/$(TARGET)/.depend; \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET);)
	$(foreach i, $(FLAMINETDIR), pushd $i; rm RCS; ln -s $(RCSMASTER)/$i/RCS; co $(COFLAGS) RCS/*,v; popd;)

relco: 
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/*,v
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/.[A-z]*,v
	$(foreach i, $(FLAMINETDIR), \
	if ( ! -e $i/$(STARGET)/ ) mkdir -p $i/$(STARGET); \
	if ( ! -e $i/$(JTARGET)/ ) mkdir -p $i/$(JTARGET); \
	if ( ! -e $i/RCS ) ln -s $(RCSMASTER)/$i/RCS $i/RCS;)
	$(foreach i, $(FLAMINETDIR), pushd $i; if( -e RCS ) co -q -r$(UFRELEASE) RCS/*,v; \
	if( -e RCS ) co -q -r$(UFRELEASE) RCS/.[A-z]*,v; popd;)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k init; popd;)

sync:
	-@echo sync: full checkin and checkout ...
	ci -q RCS/*,v >& /dev/null ; co $(COFLAGS) RCS/*,v
	ci -q RCS/.[A-z]*,v >& /dev/null ; co $(COFLAGS) RCS/.[A-z]*,v
	$(foreach i, $(FLAMINETDIR), \
	pushd $i; if( -e RCS ) co $(COFLAGS) RCS/*,v; if( -e RCS ) co $(COFLAGS) RCS/.[A-z]*,v; $(MAKE) -i -k sync; popd;)
	-@echo sync: full checkin and checkout completed

syncinit:
	-@echo sync: full checkout ...
	co $(COFLAGS) RCS/*,v
	co $(COFLAGS) RCS/.[A-z]*,v
	$(foreach i, $(FLAMINETDIR), \
	pushd $i; if( -e RCS ) co $(COFLAGS) RCS/*,v; if( -e RCS ) co $(COFLAGS) RCS/.[A-z]*,v; $(MAKE) -i -k init; popd;)
	-@echo sync: full checkout completed

syncdiff:
	-@echo sync: full checkout ...
	$(foreach i, $shell(\ls RCS/*,v), rcsdiff $i >& /dev/null; if( $$? != 0 ) co $(COFLAGS) $i;)
	$(foreach i, $shell(\ls RCS/.[A-z]*,v), rcsdiff $i >& /dev/null; if( $$? != 0 ) co $(COFLAGS) $i;)
	$(foreach i, $(FLAMINETDIR), \
	pushd $i; $(MAKE) -i -k syncdiff; popd;)
	-@echo sync: full checkout completed

# only public headers, libraries, and executables, meant for public consumption
# should be "installed", nominally into /usr/local/...
# force source of .ufcshrc
install:
	source ../.ufcshrc; $(MAKE) -i -k .install

.install: pubheader publib pubexe pubjar
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k install; popd;)
	cd bootexec; make pubinit
	cd flamdhs; make pubinit

clean: .init
	source ../.ufcshrc; $(MAKE) -i -k .clean

.clean:
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k clean; popd;)

pubheader:
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k $@; popd;)

publib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k $@; popd;)

pubexe: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) -i -k $@; popd;)

pubjar:
	@if( ! -e $(INSTALL_JAR)/ ) mkdir -p $(INSTALL_JAR)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

stripall: pubheader strippublib strippubexe
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

strippublib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

strippubexe: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

strip: pubheader stripslib stripsbin
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

stripslib: 
	@if( ! -e $(INSTALL_LIB)/ ) mkdir -p $(INSTALL_LIB)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

stripsbin: 
	@if( ! -e $(INSTALL_EXE)/ ) mkdir -p $(INSTALL_EXE)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

jar:
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

shlib: pubheader
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

exe: shlib publib
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $@; popd;)

rcsclean:
	$(foreach i, $(FLAMINETDIR), pushd $i; rcsclean; popd;)
#	rcsclean

rcsinit:
	if( -o RCS ) $(RM) ./RCS/* ./RCS/.[A-z]*
	ci -l0.0 -N"Init" -s"Dev." -m"initial checkin" -t-"initial checkin" $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; rcsclean; popd;)

rcscheckin:
	ci -l -s"Developmental" -m"inremental checkin" $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcstest:
	ci -u -sTestable -m"current & testable" $(RCSLIST)
	rcs -N"Test": $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcsinstall:
	ci -u -sInstalled -m"current & installed" $(RCSLIST)
	rcs -N"Install": $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

rcsrelease: rcscheckin
	ci -u -sRelease -m"tested & released" $(RCSLIST)
	rcs -N"$(UFRELEASE)": $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

releaseco: rcsrelease
	rcsclean; co -q -r$(UFRELEASE) $(RCSLIST)
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)

release: releaseco
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE $(MAKEFLAGS)) $@; popd;)
	@echo completeded installation of release $(UFRELEASE)

depend: pubheader
	$(foreach i, $(FLAMINETDIR), pushd $i; $(MAKE) $(MAKEFLAGS) $@; popd;)
