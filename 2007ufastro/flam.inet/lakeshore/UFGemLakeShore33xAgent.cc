#if !defined(__UFGemLakeShore33xAgent_cc__)
#define __UFGemLakeShore33xAgent_cc__ "$Name:  $ $Id: UFGemLakeShore33xAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemLakeShore33xAgent_cc__;

#include "UFGemLakeShore33xAgent.h"
#include "UFLS33xConfig.h"
#include "UFSADFITS.h"

// indicate that no set pint has been established yet via -1.0 initial val:
float UFGemLakeShore33xAgent::_setpvalA = -1.0;
float UFGemLakeShore33xAgent::_setpvalB = -1.0;
// assign ctrl loop 1 to input A, kelvin units, powerup enabled, hearter output as power
string UFGemLakeShore33xAgent::_ctrlA = "CSET 1, A, 1, 1, 2"; 
// assign ctrl loop 2 to input B, kelvin units, powerup enabled, hearter output as power
string UFGemLakeShore33xAgent::_ctrlB = "CSET 2, B, 1, 1, 2";
string UFGemLakeShore33xAgent::_currentvalA; // cold plate?
string UFGemLakeShore33xAgent::_currentvalB; // detector array?
string UFGemLakeShore33xAgent::_rangeA = "CMODE 1, 1;RANGE 2"; // dcold plate range == medium
string UFGemLakeShore33xAgent::_rangeB = "CMODE 2, 1;RANGE 1"; // det. array range == 0 off, 1 on only
string UFGemLakeShore33xAgent::_setpvalACmd = "SETP 1, 75.5";
string UFGemLakeShore33xAgent::_setpvalBCmd = "SETP 2, 75.5";
string UFGemLakeShore33xAgent::_setpidACmd = "CMODE 1, 1;PID 1, 30, 5, 0";
string UFGemLakeShore33xAgent::_setpidBCmd = "CMODE 2, 1;PID 2, 30, 5, 0";
string UFGemLakeShore33xAgent::_manualACmd = "MOUT 1, 58.0";
string UFGemLakeShore33xAgent::_manualBCmd = "MOUT 2, 58.0";
string UFGemLakeShore33xAgent::_setPointRec;  
string UFGemLakeShore33xAgent::_simkelvin1;  
string UFGemLakeShore33xAgent::_simkelvin2;  
time_t UFGemLakeShore33xAgent::_timeOut = 10; // seconds
time_t UFGemLakeShore33xAgent::_clock = 0; // seconds
bool UFGemLakeShore33xAgent::_setcrv = false; // set F2 temp. calibration curve
bool UFGemLakeShore33xAgent::_setintyp = false; // set F2 temp. calibration amps/volts input


//public:
// this virtual should be called from options()...
void UFGemLakeShore33xAgent::setDefaults(const string& instrum, bool initsad) {
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //_confGenSub = _epics + ":ec:configG";
    _confGenSub = "";
    //_heart = _epics + ":ec:heartbeat.ctrltemp";
    _heart = _epics + ":ec:L332heartbeat.VAL";
    
    for( int i = 0; i < (int) UFSADFITS::_ecls33xsad->size(); ++i ) {
      //string ls332sad = (*UFSADFITS::_ecls33xsad)[i] + ".INP";
      string ls332sad = (*UFSADFITS::_ecls33xsad)[i] + ".VAL";
      //if( ls332sad.find("CAMBENCA") != string::npos ) _StatRecs.push_back(ls332sad); // chan. A
      //if( ls332sad.find("CAMDETCB") != string::npos ) _StatRecs.push_back(ls332sad); // chan. B
      if( ls332sad.find("CAMABENC") != string::npos ) _StatRecs.push_back(ls332sad); // chan. A
      if( ls332sad.find("CAMBDETC") != string::npos ) _StatRecs.push_back(ls332sad); // chan. B
    }
  }
  else {
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
  _simkelvin1 = "";
}

// ctors:
UFGemLakeShore33xAgent::UFGemLakeShore33xAgent(int argc, char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve LS33x readings
  _flush = 0.05; // 0.1; // 0.35; // inherited value is not quite rigth for the 33x
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //setDefaults();
}

UFGemLakeShore33xAgent::UFGemLakeShore33xAgent(const string& name, int argc,
				               char** argv) : UFGemDeviceAgent(name, argc, argv) {
  _flush = 0.05; //  0.1; //0.35;
  // use ancillary function to retrieve LS33x readings
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //setDefaults();
}

// static funcs:
int UFGemLakeShore33xAgent::main(int argc, char** argv) {
  UFGemLakeShore33xAgent ufs("UFGemLakeShore33xAgent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemLakeShore33xAgent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  if( clname.find("trecs:") != string::npos )
    _epics = "trecs";
  else if( clname.find("miri:") != string::npos )  
    _epics = "miri";
  else if( clname.find("flam:") != string::npos )  
    _epics = "flam";

  if( !( _epics == "trecs" || _epics == "miri" || _epics == "flam") )
    return clname;
  /*
  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string conf =  _epics + genSub + ".VALA";
  string periods =  _epics + genSub  + ".VALB";
  string hearts =  _epics + genSub + ".VALC";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemLakeShore33xAgent::newClient> Epics client (I think) fetching rec. list from:"
        <<conf<<endl;

  // this is a deferred (post boot, one time) init.
  if( _StatList.size() != 0 ) { // need only do this on first epics connect...
    int nr = getEpicsOnBoot(_epics, hearts, _StatList);
    if( nr > 0 && _StatList.size() > 0 ) {
      _heart = _StatList[0]; // assume 33x is first in list
      if( _verbose ) {
        clog<<"UFGemLakeShore33xAgent::newClient> Epics heartbeat list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore33xAgent::newClient> "<<_StatList[i]<<endl;
      }
    }

    nr = getEpicsOnBoot(_epics, periods, _StatList);
    if( nr > 0 && _StatList.size() > 0 ) {
      _Update = atof(_StatList[0].c_str()); // assume minimum is first in list
      if( _verbose ) {
        clog<<"UFGemLakeShore33xAgent::newClient> Epics update periods list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore33xAgent::newClient> "<<_StatList[i]<<endl;
      }
    }
    // and lastly, the config info should be preserved in StatList:
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr <= 0 || _StatList.size() <= 0 )
      clog<<"UFGemLakeShore33xAgent::newClient> Epics connection failed. "<<endl;
    else {
      _statRec = _StatList[0];
      if( _verbose ) {
        clog<<"UFGemLakeShore33xAgent::newClient> Epics client recs. list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore33xAgent::newClient> "<<_StatList[i]<<endl;
      }
    }
  }
  */

  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemLakeShore33xAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemLakeShore33xAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemLakeShore33xAgent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist); // note this calls its setDefaults
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  _config = new UFLS33xConfig(); // needs to be proper device subclass (LS33x)
  // 33x defaults for trecs:
  _config->_tsport = 7003;
  _config->_tshost = "flamperle";

  string arg = findArg("-sim"); // _sim mode
  if( arg == "true" ) {
    _sim = true;
    _simkelvin1 = "+301.11";
    _simkelvin2 = "+299.99";
  }
  else if( arg == "cold" ) {
    _sim = true;
    _simkelvin1 = "+70.77";
    _simkelvin2 = "+80.88";
  }
  else if( arg != "false" ) {
    _sim = true;
    _simkelvin1 = arg;
    _simkelvin2 = arg;
  }

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-crv"); 
  if( arg != "false" )
    _setcrv = true;;

  arg = findArg("-setcrv"); 
  if( arg != "false" )
    _setcrv = true;

  arg = findArg("-intyp"); 
  if( arg != "false" )
    _setintyp = true;
 
  arg = findArg("-setintyp"); 
  if( arg != "false" )
    _setintyp = true;

  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52003;
  }
  if( _verbose ) 
    clog<<"UFGemLakeShore33xAgent::options> set port to GemLakeShore33x port "<<port<<endl;

  return port;
}

void UFGemLakeShore33xAgent::startup() {
  setSignalHandler(UFGemLakeShore33xAgent::sighandler);
  clog << "UFGemLakeShore33xAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config == 0 || _sim ) {
    clog<<"UFGemLakeShore33xAgent::startup> simulation... "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemLakeShore33xAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemLakeShore33xAgent::init(const string& tshost, int tsport) {
  // connect to the device:
  UFTermServ* ts = 0;
  if( !_sim ) {
    ts =_config->connect(tshost, tsport);
    if( _config->_devIO == 0 ) {
      clog<<"UFGemLakeShore33xAgent> failed to connect to device port..."<<endl;
      return 0;
    }
    if( _setintyp )
      UFLS33xConfig::setInTypFlam2Detector();
    if( _setcrv )
      UFLS33xConfig::setCrvFlam2Detector();
  }
  // test connectivity to lakeshore:
  string lakeshore; // lakeshore cmd & reply strings
  lakeshore = "+298.76";
  if( _simkelvin1 != "" ) 
    lakeshore = _simkelvin1;
  
  string kelvinA = "KRDG?A"; // should be function in UFDeviceConfig
  string kelvinB = "KRDG?B"; // should be function in UFDeviceConfig
  string cmode, cset, pid, rangeonoff;
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.lakeshore33x."<<p<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemLakeShore33xAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return ts;
  }
  delete s.str();
  */
  if( !_sim ) {
    clog<<"UFGemLakeShore33xAgent> checking availiblity of lakeshore 33x channels A, B."<<endl;
    int nc = _config->_devIO->submit(kelvinA, lakeshore, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemLakeShore33xAgent> lakeshore33x querry kelvin A failed..."<<endl;
      delete _config->_devIO; _config->_devIO= 0;
      return 0;
    }
    nc = _config->_devIO->submit(kelvinB, lakeshore, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemLakeShore33xAgent> lakeshore33x querry kelvin B failed..."<<endl;
      delete _config->_devIO; _config->_devIO= 0;
      return 0;
    }
    clog<<"UFGemLakeShore33xAgent> reply: "<<lakeshore<<endl;
    // also set the desired control loop ID for all subsequent control commands?
    /*
    cmode = "cmode 1,1";
    nc = _config->_devIO->submit(cmode, lakeshore, _flush);
    cmode = "cmode? 1";
    nc = _config->_devIO->submit(cmode, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> control mode: "<<lakeshore<<endl;
    pid = _setpidACmd;
    nc = _config->_devIO->submit(pid, _flush);
    pid = "pid?1";
    nc = _config->_devIO->submit(pid, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> pid1: "<<lakeshore<<endl;
    nc = _config->_devIO->submit(_manualACmd, _flush);
    // also set the desired control loop parameters:
    cset = "cset 1,A,1,0,1"; // no reply expected, 2nd to last arg is power-up enable off
    nc = _config->_devIO->submit(cset, _flush);
    cset = "cset?1";
    nc = _config->_devIO->submit(cset, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> control loop set: "<<lakeshore<<endl;
    // also set the desired control loop on/off
    rangeonoff = "range 0"; // no reply expected, insure control loop is off for A (cold plate)
    nc = _config->_devIO->submit(range, _flush);
    rangeonoff = "range?";
    nc = _config->_devIO->submit(range, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> control loop set: "<<lakeshore<<endl;
 
    cmode = "cmode 2,1"; // no reply expected
    nc = _config->_devIO->submit(cmode, _flush);
    cmode = "cmode? 2";
    nc = _config->_devIO->submit(cmode, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> set control mode: "<<lakeshore<<endl;
    pid = _setpidBCmd;
    nc = _config->_devIO->submit(pid, _flush);
    pid = "pid?2";
    nc = _config->_devIO->submit(pid, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> pid2: "<<lakeshore<<endl;
    nc = _config->_devIO->submit(_manualBCmd, _flush);
    // also set the desired control loop parameters:
    cset = "cset 2,B,1,0,1"; // no reply expected, 2nd to last arg is power-up enable off
    nc = _config->_devIO->submit(cset, _flush);
    cset = "cset?2";
    nc = _config->_devIO->submit(cset, lakeshore, _flush);
    clog<<"UFGemLakeShore33xAgent> control set: "<<lakeshore<<endl;
    // also set the desired control loop on/off
    rangeonoff = "range 1"; // no reply expected, insure control loop is on for B (detector)
    nc = _config->_devIO->submit(range, _flush);
    rangeonoff = "range?";
    nc = _config->_devIO->submit(range, lakeshore, _flush);
    */
  }
  else {
    clog<<"UFGemLakeShore33xAgent> simulate 33x two channels: "<<_simkelvin1<<", "<<_simkelvin2<<endl;
  }
  /* 
  int nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
  if( nc <= 0 ) {
    clog<<"UFGemLakeShore33xAgent> status log failed..."<<endl;
  }
  ::close(fd);
  */

  if( _epics != "false" && _capipe == 0 ) {
    for( size_t i = 0; i < _StatRecs.size(); ++i )
      clog<<"UFGemLakeShore33xAgent::init> SAD: "<<_StatRecs[i]<<endl;

    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemLakeShore33xAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemLakeShore33xAgent> Ecpics CA child proc. failed to start"<<endl;
    
  }

  return ts;
}

string UFGemLakeShore33xAgent::_simsetp() {
  if( _setpvalA < 0.0 ) // not yet init?
    _setpvalA = atoi(_simkelvin1.c_str());
  if( _setpvalB < 0.0 ) // not yet init?
    _setpvalB = atoi(_simkelvin2.c_str());

  double setpnt = _setpvalA;
  char v[8], v1[8], v2[8];
  ::memset(v, 0, sizeof(v)); ::memset(v1, 0, sizeof(v1)); ::memset(v2, 0, sizeof(v2));
  // presumably simulation mode allows any non-physical setp?
  if( setpnt < 10 ) {
   ::sprintf(v, "%4.2f", setpnt); 
    ::sprintf(v1, "%4.2f", (setpnt+0.04)); ::sprintf(v2, "%4.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 100 ) {
    ::sprintf(v, "%5.2f", setpnt); 
    ::sprintf(v1, "%5.2f", (setpnt+0.04)); ::sprintf(v2, "%5.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 1000 ) {
    ::sprintf(v, "%6.2f", setpnt); 
    ::sprintf(v1, "%6.2f", (setpnt+0.04)); ::sprintf(v2, "%6.2f", (setpnt-0.04)); 
  }

  setpnt = _setpvalB;
  if( setpnt < 10 ) {
   ::sprintf(v, "%4.2f", setpnt); 
    ::sprintf(v1, "%4.2f", (setpnt+0.04)); ::sprintf(v2, "%4.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 100 ) {
    ::sprintf(v, "%5.2f", setpnt); 
    ::sprintf(v1, "%5.2f", (setpnt+0.04)); ::sprintf(v2, "%5.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 1000 ) {
    ::sprintf(v, "%6.2f", setpnt); 
    ::sprintf(v1, "%6.2f", (setpnt+0.04)); ::sprintf(v2, "%6.2f", (setpnt-0.04)); 
  }

 strstream s1, s2;
  s1 << "+" << v1 << ends;
  s2 << "+" << v2 << ends;
  string reply33x = _simkelvin1 = s1.str(); delete s1.str();
  _simkelvin2 = s2.str(); delete s2.str();
  reply33x += ";"; reply33x += _simkelvin2;
  //if( _verbose ) 
     // clog<<"UFGemLakeShore33xAgent::ancillary> (sim setpnt) "<< reply33x <<endl;

  return reply33x;
}

void* UFGemLakeShore33xAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  string replyAB= "+301.11;+299.99", replyA = "+301.11", replyB= "+299.99";
  std::map< string, std::pair<string, string> > sadmap;

  if( _simkelvin1 != "" ) 
    replyA = _simkelvin1;
  if( _simkelvin2 != "" ) 
    replyB = _simkelvin2;

  if( _sim ) {
    replyAB = _simsetp();
  }
  else if(_config && _config->_devIO ) { // not simulation
    //_config->_devIO->submit("KRDG?A", replyA, _flush); // expect single line reply
    //_config->_devIO->submit("KRDG?B", replyB, _flush); // expect single line reply
    replyAB = UFLS33xConfig::updateSAD(_epics, sadmap); //replyA + ";" + replyB;
    size_t semic = replyAB.find(";");
    if( semic != string::npos ) {
      replyA = replyAB.substr(0, semic); replyB = replyAB.substr(1+semic);
    }
  }

  if( _verbose )
    clog<<"UFGemLakeShore33xAgent::ancillary> new reading: "<<replyAB<<endl;

  // check the max history count, and decrement by two if limit has been reached
  // this insures that the modula 2 logic above continues properly
  if( _DevHistory.size() > _DevHistSz ) { // should be configurable size
    _DevHistory.pop_front();  _DevHistory.pop_front();
  }

  // insert value into history
  _DevHistory.push_back(replyAB);

  /*  
  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemLakeShore33xAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemLakeShore33xAgent> Ecpics CA child proc. failed to start"<<endl;
  }
  */
  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
  }

  // reply must be parsed and formatted into vector
  // assuming semicolon delim: "+304.17;+304.410"
  // since 33x only has 2 channels, don't bother with loop:
  /*
  int pos= reply33x.find(";");
  string a= reply33x.substr(0, pos);
  string b= reply33x.substr(1+pos);
  */
  // presumably A is the array's temperature, and it gets special treatment
  _currentvalA = replyA;
  _currentvalB = replyB;

  if( _verbose ) {
  clog<<"UFGemLakeShore33xAgent::ancillary>_statRec: "<<_StatRecs[0]<<", valA: "<<replyA<<endl; 
  clog<<"UFGemLakeShore33xAgent::ancillary>_statRec: "<<_StatRecs[1]<<", valB: "<<replyB<<endl; 
  }

  if( _sim && _capipe ) {
    // assume 332A values are in [0] and B in [1]
    string val = replyA; val = val.substr(1+val.find("?"));
    sendEpics(_StatRecs[0], val);
    val = replyB; val = val.substr(1+val.find("?"));
    sendEpics(_StatRecs[1], val);
  }

  // for set point "command completion"
  if( _capipe && _setPointRec != "" ) { // && _setpval > 0 ) {
    double currentval = atof(_currentvalA.c_str());
    double diffA = fabs(currentval - _setpvalA);
    if( _verbose )
      clog<<"UFGemLakeShore33xAgent::ancillary> setpval, currentval (A), diff: "<<_setpvalA<<", "
          <<currentval<<", "<<diffA<<endl;

    currentval = atof(_currentvalB.c_str());
    double diffB = fabs(currentval - _setpvalB);
    if( _verbose )
      clog<<"UFGemLakeShore33xAgent::ancillary> setpval, currentval (A), diff: "<<_setpvalB<<", "
          <<currentval<<", "<<diffB<<endl;

    if( diffA <= 0.05 && diffB <= 0.05) {
      //sendEpics(_setPointRec, "OK");
      setCARIdle(_setPointRec);
      _setPointRec = ""; // clear this for next iteration
      clog<<"UFGemLakeShore33xAgent::ancillary> OK. setpval, currentvals, diffs: "<<_setpvalA<<", "
          <<_setpvalB<<", "<<", "<<diffA<<", "<<diffB<<endl;
    }
    else if( (clck - _clock) > _timeOut ) {
      setCARError(_setPointRec);
      _setPointRec = ""; // clear this for next iteration
      clog<<"UFGemLakeShore33xAgent::ancillary> TimedOut! setpval, currentvals, diffs: "<<_setpvalA<<", "
          <<_setpvalB<<", "<<", "<<diffA<<", "<<diffB<<endl;
    }
  } 
  
  return p;
}

void UFGemLakeShore33xAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update); 
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// this version must always return null to client, and
// instead send the command completion status to the designated CAR
// if no CAR is desginated, an error /alarm condition should be indicated
int UFGemLakeShore33xAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat = 0;
  bool reply_client= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_client = false;
    clog<<"UFGemLakeShore33xAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemLakeShore33xAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemLakeShore33xAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car= "", Asad= "", Bsad= "",  errmsg= "", hist= "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim = _sim;
    bool reqsetp = false;
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];

    //if( _verbose ) {
      clog<<"UFGemLakeShore33xAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemLakeShore33xAgent::action> cmdimpl: "<<cmdimpl<<endl;
      //}

    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
	replies.push_back(_config->statusFITS(this)); 
        return (int) replies.size(); 
      }
      else {
	replies.push_back(_config->status(this)); 
        return (int) replies.size(); 
      }
    }
    else if( cmdname.find("car") == 0 || cmdname.find("Car") == 0 || cmdname.find("CAR") == 0 ) {
      reply_client= false;
      _carRec = car = cmdimpl;
      if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("sad") == 0 || cmdname.find("Sad") == 0 || cmdname.find("SAD") == 0 ) {
      reply_client= false;
      if( cmdname.find("B") != string::npos )
	Bsad = cmdimpl;
      else
        Asad = cmdimpl;
      if( _verbose )
	clog<<"UFGemLakeShore33xAgent::action> SAD A: "<<Asad<<", B: "<<Bsad<<endl;
      continue;
    }
    else if( cmdname.find("caput") == 0 ||
	     cmdname.find("Caput") == 0 ||
	     cmdname.find("CaPut") == 0 || 
             cmdname.find("CAPUT") == 0 ) {
      if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> "<<cmdname<<" "<<cmdimpl<<endl;
      if( _epics != "false" && _capipe ) sendEpics(cmdimpl);
      delete rep; rep = 0;
      return 0;      
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore33xAgent::action> History: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") == 0 ||
	     cmdname.find("Sim") == 0 || 
	     cmdname.find("SIM") == 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore33xAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    // if a sim or non-sim setpoint req... make a note of it
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore33xAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
      if( cmdimpl.find("SETP") != string::npos || cmdimpl.find("SetP") != string::npos 
          || cmdimpl.find("setp") != string::npos && cmdimpl.find("?") == string::npos ) {
        // was not a query
	reqsetp = true;
	if( _verbose )
          clog<<"UFGemLakeShore33xAgent::action> req. setp: "<<cmdimpl<<endl;
        _setPointRec = car;
        int vpos = (int) cmdimpl.find(","); // "SETP 1, 7.0" where 1 is "loop Id"
        if( vpos != (int)string::npos ) {
          string loopId = cmdimpl.substr(vpos-1,1);
          _clock = time(0); // to be used for temp. settle timeout
	  if( loopId == "1" ) { // assume A chan. is assigned to loop 1:
            double oldsetp = _setpvalA;
            string setpstr = cmdimpl.substr(1+vpos);
            double setp = atof(setpstr.c_str());
            clog<<"UFGemLakeShore33xAgent::action> req. setp: "<<setpstr<<" == "<<setp<<endl;
            if( setp > UFLS33xConfig::_MaxSetPnt ) {
              clog<<"UFGemLakeShore33xAgent::action> req. setpval exceeds max allowed: "<<UFLS33xConfig::_MaxSetPnt<<endl;
              if( oldsetp < 0 ) {
                _setpvalA = UFLS33xConfig::_NominalSetPnt;
                clog<<"UFGemLakeShore33xAgent::action> changing req.: "<<setp<<", to nominal: "<<_setpvalA;
              }
              else { // use old value
               _setpvalA = oldsetp;
               clog<<"UFGemLakeShore33xAgent::action> changing req.: "<<setp<<", to prev./old value: "<<_setpvalA<<endl;
              }
            }
	    else {
            _setpvalA = setp;
	    }
            clog<<"UFGemLakeShore33xAgent::action> req. setpval, oldval: "<<_setpvalA<<", "<<oldsetp<<endl;
	    if( !Asad.empty() && !_epics.empty() && _epics != "false" && _capipe ) sendEpics(Asad, _setpvalA);
	  }
	  else { // assum B chan i loop 2
            double oldsetp = _setpvalB;
            string setpstr = cmdimpl.substr(1+vpos);
            double setp = atof(setpstr.c_str());
            clog<<"UFGemLakeShore33xAgent::action> req. setp: "<<setpstr<<" == "<<setp<<endl;
            if( setp > UFLS33xConfig::_MaxSetPnt ) {
              clog<<"UFGemLakeShore33xAgent::action> req. setpval exceeds max allowed: "<<UFLS33xConfig::_MaxSetPnt<<endl;
              if( oldsetp < 0 ) {
                _setpvalB = UFLS33xConfig::_NominalSetPnt;
                clog<<"UFGemLakeShore33xAgent::action> changing req.: "<<setp<<", to nominal: "<<_setpvalB;
              }
              else { // use old value
               _setpvalB = oldsetp;
               clog<<"UFGemLakeShore33xAgent::action> changing req.: "<<setp<<", to prev./old value: "<<_setpvalB<<endl;
              }
            }
	    else {
            _setpvalB = setp;
	    }
            clog<<"UFGemLakeShore33xAgent::action> req. setpval, oldval: "<<_setpvalB<<", "<<oldsetp<<endl;
	    if( !Bsad.empty() && !_epics.empty() && _epics != "false" && _capipe ) sendEpics(Bsad, _setpvalB);
	  }
        }
        if( _verbose )
          clog<<"UFGemLakeShore33xAgent::action> setpval, currentvals: "<<_setpvalA
              <<", A: "<<_currentvalA<<", B: "<<_currentvalB<<endl;
      }
    }
    else {
      clog<<"UFGemLakeShore33xAgent::action> ?improperly formatted request?"<<endl;
      delete rep; rep = 0;
      return 0;      
    }

    string reply33x= "";
    int nr = _config->validCmd(cmdimpl);
    if( nr == UFLS33xConfig::_BadSetPnt ) { // if this is first time ever, set whatever was determined above
      strstream s;
      s << "SETP 1, "<<_setpvalA<<ends;
      cmdimpl = s.str(); delete s.str();
      reply33x = "Err: Bad SetP! Using: " + cmdimpl;
      act->cmd_reply.push_back(reply33x);
      clog<<"UFGemLakeShore33xAgent::action> "<<reply33x<<endl;
      nr = 0; // proceed with default
    }
    else if( nr == UFLS33xConfig::_BadPID ) { // if this is first time ever, set whatever was determined above
      reply33x = "Err: Bad PID! Using: " + cmdimpl;
      clog<<"UFGemLakeShore33xAgent::action> "<<reply33x<<endl;
      act->cmd_reply.push_back(reply33x);
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    if( hist != "" ) { // history
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(hist.c_str());
      if( cnt <= 0 ) 
        cnt = 1;
      else if( cnt > last )
	cnt = 1+last;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( !sim && _config != 0 && _config->_devIO ) {
      if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> submit raw: "<<cmdimpl<<endl;
      if( nr < 0 ) { // assume history requested
        int last = (int)_DevHistory.size() - 1;
        int cnt = atoi(cmdimpl.c_str());
        if( cnt <= 0 )
	  cnt = 1;
	else if( cnt > last )
	  cnt = 1+last;
        else if( cmdimpl.find("all") != string::npos || 
		 cmdimpl.find("All") != string::npos ||
		 cmdimpl.find("All") != string::npos )
	  cnt = last;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
      else if( nr == 0 ) { // no reply expected
	if( reply33x == "" ) reply33x = "OK: " + cmdimpl;
        stat = _config->_devIO->submit(cmdimpl, _flush);
        if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> reply33x: "<<reply33x<<endl;
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply33x);
        if( stat < 0 ) {
	  errmsg = "Failed: " + cmdimpl;
	  //if( car != "" ) setCARError(car, &errmsg); // set error
	  if( _epics != "false" && _capipe &&  car != "" ) sendEpics(car, errmsg);
          if( _verbose )
	  clog<<"UFGemLakeShore33xAgent::action> cmd error condition..."<<endl;
          delete rep; rep = 0;
	  return 0;
	}
      }
      else if( nr != UFLS33xConfig::_BadSetPnt) {
        stat = _config->_devIO->submit(cmdimpl, reply33x, _flush); // expect single line reply?
        if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> reply33x: "<<reply33x<<endl;
        if( stat < 0 ) {
	  //if( car != "" ) setCARError(car, &errmsg); // set error
	  if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
          if( _verbose )
	  clog<<"UFGemLakeShore33xAgent::action> cmd error condition..."<<endl;
          delete rep; rep = 0;
	  return 0;
	}
      }
    } // non-sim
    else if( sim ) {
      errmsg += cmdimpl;
      // "+301.11;+302.22"  LS33x format
      //int asim = 300*rand() / RAND_MAX;
      //int bsim = 300*rand() / RAND_MAX;
      //strstream s;
      //s<<asim<<".11;"<<bsim<<".22"<<ends;
      //reply33x = s.str(); delete s.str();
      int chan= 0; // 0 for both; 1 for a; 2 for b
      const char* cs = cmdimpl.c_str();
      size_t qpos1 = cmdimpl.find("?");
      if(qpos1 != string::npos && cs[1+qpos1] == 'a' || cs[1+qpos1] == 'A')
        chan = 1;
      if(qpos1 != string::npos && cs[1+qpos1] == 'b' || cs[1+qpos1] == 'B')
        chan = 2;
      size_t qpos2 = cmdimpl.find("?", 1+qpos1);
      if(qpos2 != string::npos) // 2 ? indicates query for both chan
        chan = 0;

      reply33x = "+301.11;+302.22"; // chan a & chan b
      if( reqsetp ) 
        reply33x = _simsetp();

      if( _simkelvin1 != "" )
        reply33x = _simkelvin1;

      if( _DevHistory.size() % 2 == 0 ) {
        if( _simkelvin2 != "" ) 
          reply33x = _simkelvin2;
        else
          reply33x = "+299.11;+298.22";
      }

      size_t qposa = reply33x.find(";");
      size_t qposb = 1+qposa;

      switch(chan) {
      case 2: // b
        reply33x = reply33x.substr(qposb);
	break;
      case 1: // a
        reply33x = reply33x.substr(0,qposa);
	break;
      case 0: // both
      default:
	break;
      }

      if( _verbose ) clog<<"UFGemLakeShore33xAgent::action> sim. reply33x: "<<reply33x<<endl;
      // simulate/force an error state transition:
      if( cmdimpl.find("err") != string::npos || 
	  cmdimpl.find("Err") != string::npos || 
	  cmdimpl.find("ERR") != string::npos  ) {
        //if( car != "" ) setCARError(car, &errmsg); // set error
	if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
        if( _verbose )
	  clog<<"UFGemLakeShore33xAgent::action> sim. error condition..."<<endl;
        delete rep; rep = 0;
	return 0;
      }
    } // sim

    if( _DevHistory.size() >= _DevHistSz ) { 
      _DevHistory.pop_front();
      _DevHistory.pop_front();
    }
    _DevHistory.push_back(reply33x);
    if( hist == "" && reply33x != "" ) act->cmd_reply.push_back(reply33x);
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    if( _verbose )
      clog<<"UFGemLakeShore33xAgent::action> cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    if( car != "" && _capipe && car != "" ) sendEpicsCAR(car, 3, &errmsg);setCARError(car, &errmsg);
    //if( car != "" && _capipe && car != "" ) sendEpicsCAR(car, 3, &errmsg);
    if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    if( _verbose )
      clog<<"UFGemLakeShore33xAgent::action> cmd. complete, set CAR to idle... "<<endl;
    act->status_cmd = "succeeded";
    if( car != "" && _capipe && car != ""  ) setCARIdle(car);
    //if( car != "" && _capipe && car != ""   ) sendEpicsCAR(car, 0);
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client ) {
    delete rep; rep = 0;
    return 0;
  }
  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int)replies.size();; // this should be freed by the calling func...
} 

int UFGemLakeShore33xAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFGemLakeShore33xAgent
