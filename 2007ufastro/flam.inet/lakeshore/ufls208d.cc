#if !defined(__ufls208d_cc__)
#define __ufls208d_cc__ "$Name:  $ $Id: ufls208d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufls208d_cc__;

#include "UFLakeShore208Agent.h"

int main(int argc, char** argv) {
  return UFLakeShore208Agent::main(argc, argv);
}
      
#endif // __ufls208d_cc__
