#if !defined(__UFLS208Config_cc__)
#define __UFLS208Config_cc__ "$Name:  $ $Id: UFLS208Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLS208Config_cc__;

#include "UFLS208Config.h"
#include "UFLakeShore208Agent.h"
#include "UFFITSheader.h"

UFLS208Config::UFLS208Config(const string& name) : UFDeviceConfig(name) {}

UFLS208Config::UFLS208Config(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// LS208 terminator is carriage-retuen + new-line:
string UFLS208Config::terminator() { return "\r\n"; }

// LS208 prefix is none:
//string UFLS208Config::prefix() { return ""; }

vector< string >& UFLS208Config::queryCmds() {
  return _queries;
}

vector< string >& UFLS208Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("YC");
  _actions.push_back("WS");
  return _actions;
}

int UFLS208Config::validCmd(const string& c) {
  if( c.find("YC") != string::npos )
    return 0;
  else if( c.find("WS") != string::npos )
    return 1;
  
  vector< string >& cv = queryCmds();
  int i= 0, cnt= (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }
  clog<<"UFLS208Config::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFLS208Config::status(UFDeviceAgent* da) {
  UFLakeShore208Agent* da208 = dynamic_cast< UFLakeShore208Agent* > (da);
  if( da208->_verbose )
    clog<<"UFLS218Config::status> "<<da208->name()<<endl;

  string ls208 = da208->getCurrent();
  //string line = "1,KKK.K 2,KKK.K 3,KKK.K 4,KKK.K 5,KKK.K 6,KKK.K 7,KKK.K 8,KKK.K\n";
  // parse this comma separated list and make a UFStrings...
  vector< string > vals;
  string s;
  int pos = 0;
  int comma = ls208.find(",", pos);
  s = "Chan-1";
  s += " == " + ls208.substr(pos, comma-2) + " Kelvin";
  vals.push_back(s);
 
  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-2";
  s += " == " + ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-3"; s += " == " + ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-4"; s += " == " +  ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-5"; s += " == " + ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-6"; s += " == " + ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  s = "Chan-7"; s += ls208.substr(pos, comma-pos-2) + " Kelvin";
  vals.push_back(s);

  pos = 1 + comma;
  //comma = ls208.find(",", pos);
  // last value is not followed by comma
  s = "Chan-8"; s += " == " + ls208.substr(pos) + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  return new UFStrings(da208->name(), vals);
}

UFStrings* UFLS208Config::statusFITS(UFDeviceAgent* da) {
  UFLakeShore208Agent* da208 = dynamic_cast< UFLakeShore208Agent* > (da);
  if( da208->_verbose )
    clog<<"UFLS218Config::status> "<<da208->name()<<endl;

  string ls208 = da208->getCurrent();
  // parse this comma separated list and make a UFStrings...
  map< string, string > valhash, comments;
  int pos = 0;
  int comma = ls208.find(",", pos);
  valhash["Chan-1"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-1"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-2"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-2"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-3"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-3"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-4"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-4"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-5"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-5"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-6"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-6"] = "Kelvin";

  pos = 1 + comma;
  comma = ls208.find(",", pos);
  valhash["Chan-7"] = ls208.substr(pos, comma-pos-2);
  comments["Chan-7"] = "Kelvin";

  pos = 1 + comma;
  //comma = ls208.find(",", pos);
  valhash["Chan-8"] = ls208.substr(pos); // last value is not followed by comma
  comments["Chan-8"] = "Kelvin";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da208->name());

  return ufs;
}

#endif // __UFLS208Config_cc__
