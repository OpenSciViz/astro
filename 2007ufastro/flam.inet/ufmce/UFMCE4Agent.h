#if !defined(__UFMCE4Agent_h__)
#define __UFMCE4Agent_h__ "$Name:  $ $Id: UFMCE4Agent.h,v 0.1 2004/05/20 18:23:29 hon beta $"
#define __UFMCE4Agent_H__(arg) const char arg##UFMCE4Agent_h__rcsId[] = __UFMCE4Agent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFDeviceAgent.h"
#include "UFTermServ.h"
#include "UFClientSocket.h"

class UFMCE4Agent: public UFDeviceAgent {
public:
  static bool _verbose;
  static int main(int argc, char** argv, char** envp);
  UFMCE4Agent(int argc, char** argv, char** envp);
  UFMCE4Agent(const string& name, int argc, char** argv, char** envp);
  inline virtual ~UFMCE4Agent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  void startup();
  virtual int options(string& servlist);
  virtual UFTermServ* init(const string& host= "192.168.111.100", int port= 7004);
  void* ancillary(void* p); 

  // some (raw) MCE4 actions return replies, some do not:
  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  // all queries should reply
  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

protected:
  static UFClientSocket* _frmsoc;
  static string _frmhost;
  static int _frmport;
};

#endif // __UFMCE4Agent_h__
      
