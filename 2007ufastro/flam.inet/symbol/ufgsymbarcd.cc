#if !defined(__ufgsymbarcd_cc__)
#define __ufgsymbarcd_cc__ "$Name:  $ $Id: ufgsymbarcd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgsymbarcd_cc__;

#include "UFGemSymbolAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFGemSymbolAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgsymbarcd> exception in stdlib: "<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgsymbarcd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgsymbarcd_cc__
