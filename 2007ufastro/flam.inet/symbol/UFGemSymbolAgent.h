#if !defined(__UFGemSymbolAgent_h__)
#define __UFGemSymbolAgent_h__ "$Name:  $ $Id: UFGemSymbolAgent.h,v 0.4 2005/06/07 15:52:17 hon Exp $"
#define __UFGemSymbolAgent_H__(arg) const char arg##UFGemSymbolAgent_h__rcsId[] = __UFGemSymbolAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFSBConfig.h"

class UFGemSymbolAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemSymbolAgent(int argc, char** argv, char** envv);
  UFGemSymbolAgent(const string& name, int argc, char** argv, char** envv);
  inline virtual ~UFGemSymbolAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);
  // override UFGemDevAgent virtual:
  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // in adition to establishing the iocomm/annex connection to
  // the motors, open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);
  int getCurrent(vector< string >& vals);

protected:
  // current barcode reading, and to which sad it should be posted
  static string _current, _postsad, _postcar, _simtrigger;

};

#endif // __UFGemSymbolAgent_h__
      
