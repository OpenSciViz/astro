#!/bin/ksh
export LD_LIBRARY_PATH=$UFINSTALL/lib:/usr/local/lib:/gemini/dhs/lib/solaris-debug-mt:/gemini/dhs/external/lib/solaris-debug-mt:/usr/local/epics/lib:/opt/EDTpdv
export SYBASE=/gemini/external/sybase/sybase
export DHS_BASE=/gemini/dhs
export OCS_BASE=$DHS_BASE/external/ocs
export IMP_STARTUP=$DHS_BASE/imp_startup
#export IMP_SCRATCH=/tmp/imp_scratch.$USER
export IMP_SCRATCH=/tmp/imp_scratch
export DEFAULT_CONFIG_DIR=$DHS_BASE/conf/`hostname`
# despite setting this env, something appears to be hard-coded to /var/dhs/master.log
# perhaps in the GemBootStart script itself?
#export DHS_LOG_DIR=/var/tmp/dhs
export PATH=./:~/bin:$UFINSTALL/bin:/opt/SUNWspro/bin:/usr/local/bin:$SYBASE/bin:$DHS_BASE/bin/solaris:$DHS_BASE/scripts:$DHS_BASE/external/bin/solaris:$DHS_BASE/external/scripts:$OCS_BASE/bin/solaris:$SYBASE/bin:/bin:/usr/bin:/sbin:/usr/sbin
