#if !defined(__ufnewdhslabel_cc__)
#define __ufnewdhslabel_cc__ "$Name:  $ $Id: ufnewdhslabel.cc 14 2008-06-11 01:49:45Z hon $";

// to support OCS like testing:
// this should attempt to open a dhs connection and request a new datalabel and close the connection.
// however, if another dhsclient is already running on this machine, the dhs open will fail.
// on failure this should then attempt to connect to the dhsclient currently running on this
// hos and request that it provide a new dhs label...


#include "UFRuntime.h"
#include "UFClientApp.h"
#include "UFFrameClient.h"
#include "UFFlamObsConf.h"
#include "UFDHSFlam.h"
#include "UFFITSClient.h"

// DHS data server name at gemini summit(s):
//static string _dataserver = "dataServerSS"; // gemini south summit
static string _dataserver = "dataServerNS"; // gemini north summit
//static string _dataserver = "dataServerNB"; // gemini north base
//static string _dataserver = "dataServerSB"; // gemini south base
static string _dhshost, _agenthost;
static bool _verbose = false;

string ufdhslabel(const string& fifoname) {
  string label= "Test";
  // this attempts to use fifo/named-pipe:
  struct stat fifostat;
  int exists = ::stat(fifoname.c_str(), &fifostat);
  if( exists != 0 )
    return label;

  // once opened for read, ufdhsput can open it for write
  // send USER1 signal to ufdhsput to request a new dhs label:
  char buff[BUFSIZ]; ::memset(buff, 0, sizeof(buff));
  FILE* fp = ::popen("/usr/bin/pgrep ufgdhsd", "r"); // get the pid
  if( fp == 0 ) // or ...
    fp = ::popen("/usr/bin/pgrep ufdhsput", "r"); // get the pid

  if( fp == 0 ) 
    return label;

  ::fgets(buff, BUFSIZ, fp);
  ::pclose(fp);

  pid_t sendsig = (pid_t) ::atoi(buff);
  int sent = ::kill(sendsig, SIGUSR1);
  if( sent < 0 ) // failed to send the signal...
    return label;
  //clog<<"ufdhslabel> sent sig to pid: "<<sendsig<<endl;

  // this will block until ufdhsput has opened fifo for write....
  int fd = ::open(fifoname.c_str(), O_RDONLY); // ufdhsput will write the new labe to this fifo
  if( fd < 0 ) 
    return label;

  //clog<<"ufdhslabel> opened fifo: "<<fifoname<<endl;
  ::memset(buff, 0, sizeof(buff));
  int na= UFRuntime::available(fd);
  if( na < 0 ) // something wrong with fifo 
    return label;
 
  const int _DATALABELSIZ_ = 13;

  while( na < _DATALABELSIZ_ ) {
    na = UFRuntime::available(fd);
    UFPosixRuntime::sleep(0.25);
  } // until na == na0 > 0, i.e. write is complete
  //clog<<"ufdhslabel> "<<na<<" bytes avail on fifo: "<<fifoname<<endl;
  int nb = ::read(fd, buff, na);
  if( na != nb )
    clog<<"ufdhslabel> fifo read error"<<buff<<endl;

  ::close(fd); 
  label = buff;
  //clog<<"ufdhslabel> read label: "<<label<<" from on fifo: "<<fifoname<<endl;
  return label;
}

string ufdhslabel(UFClientApp& ufc, const string& dhshost, bool ocsinit) {
  string label= "";
  // this attempst an agent socket transaction:
  bool block = false;
  if( _verbose )
    clog<<"ufdhslabel> connect to UFFlamAgent @ "<<dhshost<<endl;
  if( ufc.connect(dhshost, 52010, block) >= 0 ) {
    vector< string > req; req.push_back("dhsbd");
    if( !ocsinit )
      req.push_back("label"); // new dataset label via dhs bulk data cmd/req.
    else
      req.push_back("ocs"); // new dataset label via dhs bulk data cmd/req. AND init prim. hdr like ocs
    UFTimeStamp greet(ufc.name());
    ufc.send(greet); ufc.recv(greet); // connection greeting/handshake
    UFStrings ufs(ufc.name(), req);
    ufc.send(ufs);
    ufc.recv(ufs);
    int elem = ufs.elements();
    label = ufs[elem-1];
    //cout<<label<<endl;
    ufc.close();
  }
  else if( _verbose ) {
    clog<<"ufdhslabel> Failed to connect to UFDHSAgent @ "<<dhshost<<endl;
  }

  return label;
}

string ufdhslabel(UFDHSFlam& dhs, bool ocsinit) {
  string label= "";
  if( _verbose )
    clog<<"ufdhslabel> connect to DHS @ "<<_dhshost<<" -- "<<_dataserver<<endl;
  DHS_STATUS dstat = dhs.open(_dhshost, _dataserver);
  if( dstat == DHS_S_SUCCESS ) {
    label = dhs.newlabel();
    cout<<label<<endl;
    if( ocsinit )
      dhs.initAttributes(label, dhs, _agenthost);
    dhs.close();
    return label;
  }
  else if( _verbose ) {
    clog<<"ufnewdhslabel> Failed to connect to DHS: "<<_dhshost<<endl;
  }
  return label;
}

void usage() {
  clog<<"ufnewdhslabel attempts to connect to the DHS service and request genereation of a new dataset label."<<endl;
  clog<<"ufnewdhslabel will write the label to stdout on success. all other messages are written to stderr."<<endl;
  clog<<"ufnewdhslabel can also be used for unit tests of the dhs subsystem by creating obs. config.s and starting observations."<<endl;
  clog<<"ufnewdhslabel [-v[v]] [-dhs] [-ocs] [-label label] [-frmstart] [-chops -savesets  -nods -nodsets] [-agent[s]] [-ns -nb]"<<endl;
  clog<<"ufnewdhslabel -v[v] -- verbosity"<<endl;
  clog<<"ufnewdhslabel -dhs <name/IP> -- hostname or IP address of DHS server"<<endl;
  clog<<"ufnewdhslabel -ocs -- attempt ocs-like init. of primary header"<<endl;
  clog<<"ufnewdhslabel -frmstart -- send the obs  config. to the acqframe agent and start the (simulated) observation"<<endl;
  clog<<"ufnewdhslabel -chops <c> -savesets <nc> -nods <n> -nodsets <nc> --  obs. config. parameter of observation"<<endl;
  clog<<"ufnewdhslabel -agent[s] <name/IP> -- hostname or IP address of Flam agent(s) host"<<endl;
  exit(0);
}

int main(int argc, char** argv, char** envp) {
  string name = UFRuntime::hostname(); name = "ufnewdhslabel@" + name;
  UFClientApp ufc(name, argc, argv, envp);
  string arg = ufc.findArg("-h");
  if( arg == "true" )
    usage();
  arg = ufc.findArg("-help");
  if( arg == "true" )
    usage();

  string label = "";
  arg = ufc.findArg("-label");
  if( arg != "true" && arg != "false" )
    label = arg;

  arg = ufc.findArg("-v");
  if( arg == "true" )
    _verbose = true;

  arg = ufc.findArg("-vv");
  if( arg == "true" ) {
    _verbose = true;
    UFDHSwrap::_verbose = true;
  }

  arg = ufc.findArg("-nowait");
  if( arg == "true" )
    UFDHSwrap::_wait = false;

  bool ocsinit = false;
  arg = ufc.findArg("-ocs");
  if( arg == "true" ) {
    ocsinit = true;
    clog<<"ufnewdhslabel> ocs option -- init primary header attributes of new dataset..."<<endl;
  }

  int nods= 0;
  arg = ufc.findArg("-nods");
  if( arg != "true" && arg != "false" ) {
    nods = atoi(arg.c_str()); 
  }

  UFFrameClient* ufrm= 0;
  UFFlamObsConf* obscfg= 0;
  _agenthost = UFRuntime::hostname();
  string frmhost = _agenthost;
  arg = ufc.findArg("-frmstart");
  if( arg != "false" ) {
    //clog<<"ufnewdhslabel> acq frame server start option..."<<endl;
    obscfg = new UFFlamObsConf(1.0);
    if( arg != "true" )
      _agenthost = frmhost = arg;
    ufrm = new UFFrameClient(frmhost);
  }
  arg = ufc.findArg("-agent");
  if( arg != "false" )
    _agenthost = arg;
  arg = ufc.findArg("-agents");
  if( arg != "false" )
    _agenthost = arg;

  string fifoname= "/tmp/.ufdhslabel";
  arg = ufc.findArg("-fifo");
  if( arg != "false" && arg != "true" )
    fifoname = arg;

  // connect to the dhs (gemini server or uf dhsclien):
  arg = ufc.findArg("-ns");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNS";

  arg = ufc.findArg("-nb");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNB";

  string clhost = UFRuntime::hostname();
  string clhostIP = UFSocket::ipAddrOf(clhost);
  _dhshost = clhostIP;
  arg = ufc.findArg("-dhs");
  if( arg != "false" && arg != "true" ) {
    //_dhshost = arg;
    _dhshost = UFSocket::ipAddrOf(arg);
    if( arg.find("flam") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
    if( arg.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  if( _dhshost == clhostIP ) {
    if( clhost.find("flam") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
    if( clhost.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  // a dhs app. is threaded:
  UFPosixRuntime::sigWaitThread();

  // only sparc dhs libraries exist...
#if defined(sparc)
  if( label == "" ) {
    UFDHSFlam dhs;
    label = ufdhslabel(dhs, ocsinit);
  }
#endif

  // if above failed (or was never compiled!)
  if( label == "" )
    label = ufdhslabel(fifoname);

  if( label == "" )
    label = ufdhslabel(ufc, _dhshost, ocsinit);

  cout<<label<<endl;

  if( label.length() > 0 && obscfg && ufrm ) {
    obscfg->relabel(label);
    ufrm->startObs(*obscfg, frmhost, 52001);
    delete obscfg; delete ufrm;
  }

  return 0;
}

#endif // __ufnewdhslabel_cc__
