#if !defined(__UFDHSConfig_h__)
#define __UFDHSConfig_h__ "$Name:  $ $Id: UFDHSConfig.h,v 0.4 2004/06/15 19:01:04 hon beta $"
#define __UFDHSConfig_H__(arg) const char arg##UFDHSConfig_h__rcsId[] = __UFDHSConfig_h__;

#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFTermServ.h"
#include "UFObsConfig.h"
#include "UFDHSFlam.h"

//using namespace std;
#include "string"

// forward declarations for friendships
class UFDHSAgent;

class UFDHSConfig : public UFDeviceConfig {
public:
  UFDHSConfig(const string& name= "UnknownDHS@DefaultConfig");
  inline virtual ~UFDHSConfig() {}

  virtual UFStrings* status(UFDeviceAgent*);
  virtual UFStrings* statusFITS(UFDeviceAgent*);

  // allowed cmds
  virtual vector< string >& UFDHSConfig::queryCmds();
  virtual vector< string >& UFDHSConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& name);
  virtual int validCmd(const string& name, const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  // if start of new observation requested, and/or obsconf parameters
  // have been provided, create new obsconf and return, otherwise
  // return 0:
  static UFObsConfig* obsconf(const UFDeviceAgent::CmdInfo* info); 


protected:
  // allow Agent access
  friend class UFDHSgent;
};

#endif // __UFDHSConfig_h__
