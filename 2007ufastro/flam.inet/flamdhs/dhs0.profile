#!/bin/ksh
# $Name:  $ $Id: dhs0.profile,v 0.3 2003/02/12 22:47:35 hon beta $
#
export DHS_BASE=/gemini/dhs/dhs
export OCS_BASE=$DHS_BASE/external/ocs
export SYBASE=/gemini/external/sybase/sybase
export IMP_STARTUP=$DHS_BASE/imp_startup
export IMP_SCRATCH=/tmp/imp_scratch
export DEFAULT_CONFIG_DIR=$DHS_BASE/conf/`hostname`
# despite setting this env, something appears to be hard-coded to /var/dhs/master.log
# perhaps in the GemBootStart script itself?
#export DHS_LOG_DIR=/var/tmp/dhs
export PATH=./:~/bin:${UFINSTALL}/bin:/opt/SUNWspro/bin:/usr/local/bin:${SYBASE}/bin:${DHS_BASE}/bin/solaris:${DHS_BASE}/scripts:${DHS_BASE}/external/bin/solaris:${DHS_BASE}/external/scripts:${OCS_BASE}/bin/solaris:${SYBASE}/bin:/bin:/usr/bin:/sbin:/usr/sbin
export LD_LIBRARY_PATH=$UFINSTALL/lib:/usr/local/lib:${DHS_BASE}/lib/solaris-debug-mt:{DHS_BASE}/external/lib/solaris-debug-mt:${EPICS}/base/lib/solaris:/opt/EDTpdv
