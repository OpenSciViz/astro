#if !defined(__ufgdhsd_cc__)
#define __ufgdhsd_cc__ "$Name:  $ $Id: ufgdhsd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgdhsd_cc__;

#include "UFFlamDHSAgent.h"

int main(int argc, char** argv, char** env) {
  string agentname = "ufgdhsd";
  try {
    UFFlamDHSAgent::main(agentname, argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgdhsd> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgdhsd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}

#endif // __ufgdhsd_cc__
