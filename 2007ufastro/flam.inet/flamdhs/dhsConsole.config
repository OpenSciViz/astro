#
#***********************************************************************
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#
# (c) 1997 				(c) 1997
# National Research Council		Conseil national de recherches
# Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
# All rights reserved			Tous droits reserves
# 					
# NRC disclaims any warranties,		Le CNRC denie toute garantie
# expressed, implied, or statu-		enoncee, implicite ou legale,
# tory, of any kind with respect	de quelque nature que se soit,
# to the software, including		concernant le logiciel, y com-
# without limitation any war-		pris sans restriction toute
# ranty of merchantability or		garantie de valeur marchande
# fitness for a particular pur-		ou de pertinence pour un usage
# pose.  NRC shall not be liable	particulier.  Le CNRC ne
# in any event for any damages,		pourra en aucun cas etre tenu
# whether direct or indirect,		responsable de tout dommage,
# special or general, consequen-	direct ou indirect, particul-
# tial or incidental, arising		ier ou general, accessoire ou
# from the use of the software.		fortuit, resultant de l'utili-
# 					sation du logiciel.
#
#***********************************************************************
#
# FILENAME
# dhsConsole/config/dhsConsole.config
#
# PURPOSE:
# Configuration file for the DHS Console (DHG)
#
#INDENT-OFF*
# $Log: dhsConsole.config,v $
# Revision 0.0  2004/07/09 16:40:42  hon
# *** empty log message ***
#
# Revision 1.22  1999/06/24 17:47:40  jaeger
# Removed "req%d:AutoRetrieve" record.
#
# Revision 1.21  1999/06/21 20:59:18  jaeger
# Version number.
#
# Revision 1.20  1999/06/18 03:08:32  jaeger
# Updated the version number.
#
# Revision 1.19  1999/06/16 23:12:35  jaeger
# Added a warning message about the "max" values in the status record (channel)
# configurations.
#
# Revision 1.18  1999/06/16 22:50:14  jaeger
# "Geminized" the aliases given to the records.  Added the full compliment
# of SDP and OLP records, even though they are not used.
#
# Revision 1.17  1999/01/26 06:42:45  jaeger
# Updated version number.
#
# Revision 1.16  1998/08/05 06:15:22  jaeger
# updated version number.
#
# Revision 1.15  1998/07/20 19:46:05  jaeger
#  Removed the "Problem with EPICS" resource.
#
# Revision 1.14  1998/06/25 20:20:10  jaeger
# Updated version number.
#
# Revision 1.13  1998/06/19 22:57:15  jaeger
# Added stuff for configuring status records.
#
# Revision 1.12  1998/06/15 16:39:25  jaeger
# Update version number.
#
# Revision 1.11  1998/05/25 20:11:25  jaeger
# Changed the version information.
#
# Revision 1.10  1998/05/20 21:05:32  jaeger
# Added Summit/Base specification of status records.
#
# Revision 1.9  1998/05/15 18:13:42  jaeger
# Fixed the statusRouter information.
#
# Revision 1.8  1998/05/15 17:51:07  jaeger
# Added dataserver and statusRouter configuration information.
#
# Revision 1.7  1998/05/04 21:07:42  jaeger
# Added STO subystem and removed the DHS subsystem (no longer needed).
#
# Revision 1.6  1998/04/15 22:44:09  jaeger
# Converted configuration file to be usable by the nes configGen class,
# now looks like all of the other DHS configuration files.
#
# Revision 1.5  1998/04/14 19:41:29  jaeger
# updated resource list.
#
# Revision 1.4  1998/03/16 23:25:02  jaeger
# Added "cmd" to specify the name of the command server
#
# Revision 1.3  1998/03/06 18:08:43  jaeger
# Changed store config info to resource config info.
#
# Revision 1.2  1997/12/04 17:38:42  jaeger
# Changes made match those made by the config class. Added comments.
#
# Revision 1.1  1997/10/30 22:09:31  jaeger
# Initial revision
#
#INDENT-ON*
#
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#***********************************************************************
#


#
#  Application identification
#
# Keyword       application 
#
# -------       -----------
#
 
identity        dhsConsoleNS


#
#  Version information
#
# Keyword       description	
#
# -------       ----------------
#
 
version		"dhsConsole 0.23 (c) 1998 NRC/CNRC"


#
# Command server identification information.
#
# Server        Server's IMP    Host
# Keyword       Identity        Address
# -------       ------------    -------
#
 
commandServer    commandServerNS irflam2a


#
# Data Sserver identification information.
#
# Server        Server's IMP    Host
# Keyword       Identity        Address
# -------       ------------    -------
#
 
dataServer    	dataServerNS	  irflam2a


#
# Command server identification information.
#
# Server        Server's IMP    Host	Gemini Location	Gemini Site
# Keyword       Identity        Address	("N"orth or
#					 "S"outth)	("S"ummit, "B"ase)
# -------       ------------    -------	---------------	------------------
#
 
statusRouter    statusServerNS  irflam2a	N		S	



#
#  EPICS channel information.
#
#  WARNING: The "max" values specified here must match those used by the
#           DHS Status server, DHS Storage server, DHS On-line Data
#           Processing server, and the DHS Synchronous Data Processing
#           server.
#
# keyword subsystem channel name		alias			max
#
# ------- --------- ------------		-----			-----
#

channel	    -	    logMessage			dhsLog			-

channel     -       resourceArraySize       	resArraySize		-
channel     -       resource%d:inUse        	res%dInUse		12
channel     -       resource%d:fullname     	res%dFullname		12
channel     -       resource%d:max          	res%dMax		12
channel     -       resource%d:name         	res%dName		12
channel     -       resource%d:type         	res%dType		12
channel     -       resource%d:used         	res%dUsed		12
channel     -       resource%d:units        	res%dUnits		12

channel	    *	    health			health			-
channel	    *	    healthDesc			desc			-
channel	    *	    state			state			-
channel	    *	    debug			debug			-
channel	    *	    simulate			sim			-

channel	    -	    health			dhsHealth		-
channel	    -	    healthDesc			dhsDesc			-
channel	    -	    state			dhsState		-
channel	    -	    debug			dhsDebug		-
channel	    -	    simulate			dhsSim			-

channel	    DTS	    numIncomplete		inc			-


#
#  DHS subsystem information.
#
# keyword       subsystem
#
# -------       ---------
#
 
subsystem       CMD
subsystem       DTS
#subsystem       HIS
subsystem       QLS
subsystem       STA


#
#  List of resources to be monitored.
#
# keyword       descriptive
#		   name
# -------       ---------
#
 
resource	"Temp Store"
resource	"Perm Store"
resource	"irflam2a /tmp"
resource	"dhsDB_NS db"
#resource	"History Store"
# RCS: $Name:  $ $Id: dhsConsole.config,v 0.0 2004/07/09 16:40:42 hon beta $
