#if !defined(__UFTReCSDHSAgent_h__)
#define __UFTReCSDHSAgent_h__ "$Name:  $ $Id: UFTReCSDHSAgent.h,v 0.2 2002/11/07 21:10:58 hon beta $"
#define __UFTReCSDHSAgent_H__(arg) const char arg##UFTReCSDHSAgent_h__rcsId[] = __UFTReCSDHSAgent_h__;

#include "iostream.h"
#include "deque"
#include "map"
#include "string"
#include "vector"

#include "UFGemDeviceAgent.h"
#include "UFTermServ.h"
#include "UFDHSTReCS.h"

class UFTReCSDHSAgent: public UFGemDeviceAgent, public UFDHSTReCS {
public:
  // instrument name should indicate nature of frame buffs.
  // trecs has 14, flamingos just 2.
  UFTReCSDHSAgent(const string& name, int argc, char** argv, char** envp);
  inline virtual ~UFTReCSDHSAgent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual void* exec(void* p);
  virtual void startup();
  virtual UFTermServ* init(); // this is dubious since there is no serial device here...
  virtual int options(string& servlist);
  virtual int query(const string& q, vector<string>& reply);

  // dhs put event loop(s):
  static void dhsThreads(UFTReCSDHSAgent& dha);
  // start dhs event loop(s) in new pthread
  static void* _dhsThreads(void* p= 0);

  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& replies);

  // main should start signal handler thread then dhs and agent::exec threads
  static int main(const string& name, int argc, char** argv, char** envp);

protected:
  // handlers
  static void _sighandler(int sig);
  //  static void _cancelhandler(void* p= 0);

  static pthread_t _dhsThrdId;
  // dhs and agent(main) threads may share addresses, if so, protect
  // them with this mutex:
  static pthread_mutex_t _theDhsMutex;
  static int _frmport, _chops, _nods, _savesets, _nodsets;
  static string _Instrum; 
  static UFDHSTReCS* _dhs; // reference to self as DHSwrap for use in agent(main) and dhs threads?
  static bool _reconnect;
  static UFSocket* _replicate;

  // generate new data label and optionally init. the dataset's primary header
  // (behave like the ocs).
  string _newdatalabel(bool ocsheader= false);
};

#endif // __UFTReCSDHSAgent_h__
      
