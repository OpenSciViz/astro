#if !defined(__ufgisplcd_cc__)
#define __ufgisplcd_cc__ "$Name:  $ $Id: ufgisplcd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgisplcd_cc__;

#include "UFGemAutoPLCAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFGemAutoPLCAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgisplcd> exception in stdlib: "<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgisplcd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgisplcd_cc__
