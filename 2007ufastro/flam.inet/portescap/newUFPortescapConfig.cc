#if !defined(__UFPortescapConfig_cc__)
#define __UFPortescapConfig_cc__ "$Name:  $ $Id: newUFPortescapConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPortescapConfig_cc__;

//#include "uftrecsmech.h" // TReCS mechanism names & named positions
#include "ufflam1mech.h" // Flamingos1 mechanism names & named positions
#include "ufflam2mech.h" // Flamingos2 mechanism names & named positions

//#include "UFPortescapConfig.h"
#include "newUFPortescapConfig.h"
#include "UFGemPortescapAgent.h"
#include "UFFITSheader.h"

#include "ctype.h"

// statics
bool UFPortescapConfig::_verbose;
//int UFPortescapConfig::_instrumId= UFDeviceAgent::TReCS;
int UFPortescapConfig::_instrumId= UFDeviceAgent::Flamingos2;
double UFPortescapConfig::_leftLim= 0;
double UFPortescapConfig::_MinLim= -5000;
double UFPortescapConfig::_rightLim= 0;
double UFPortescapConfig::_MaxLim= 5000;
vector <string> UFPortescapConfig::_Indexors;
string UFPortescapConfig::_Motors;

// hold-run current settings  
map< string, string> UFPortescapConfig::_HoldRunC;

// datum history (count) -- has indexor been datumed? how many times?
// 0 means never, > 0 means home command datum count
// < 0 means origin command executed rather than home command
// if the datum count is 0 for an indexor and the current indexor step 
// count is also 0, do not assume mechanism is datumed -- accept
// a datum command but interpose an intial step of 10 counts
// in the opposite direction of the requested home direction;
// otherwise treat a datum request as a no-op?
map< string, int> UFPortescapConfig::_DatumCnt;

// prior cmds:
map< string, string> UFPortescapConfig::_HoldRunCcmd;
map< string, string> UFPortescapConfig::_SlewVcmd;
map< string, string> UFPortescapConfig::_AccelDecelcmd;
map< string, string> UFPortescapConfig::_InitVcmd;
map< string, string> UFPortescapConfig::_Settlecmd;
map< string, string> UFPortescapConfig::_Jogcmd;


int UFPortescapConfig::cntDelim(const string& cmds, string delim) {
  // count commas or any other designated deliminator -- number f concatenated commands
  int nc = 0;
  if( delim == "" ) delim = ",";
  size_t pos = cmds.find(delim, 0);
  while( pos != string::npos ) {
    ++nc;
    pos = cmds.find(delim, 1+pos);
  }
  return nc;
}

bool UFPortescapConfig::isMotionCmd(const string& cmd, string& indexor) {
  // return true if cmd contains +/- M or F portescap indexor motion request
  const char* cs = cmd.c_str();
  while( !isalpha(*cs) && *cs != 0 ) ++cs;
  // assume first non white-space, non numeric character is indexor
  char idxr[2]; memset(idxr, 0, sizeof(idxr)); idxr[0] = *cs;
  if( isalpha(*cs ) ) {
    indexor = idxr;
  }
  else {
    indexor = "";
    clog<<"UFPortescapConfig::isMotionCmd> no indexor name indicated in command: "<<cmd<<endl;
    return false;
  }

  size_t pos = cmd.find("+"), rpos = cmd.rfind("+");
  if( pos != string::npos ) return true;

  pos = cmd.find("-");
  if( pos != string::npos ) return true;

  pos = cmd.find("M");
  if( pos != string::npos ) { // could be indexor name
    rpos = cmd.rfind("M");
    if( rpos != string::npos && rpos != pos ) return true;
  }

  pos = cmd.find("F");
  if( pos != string::npos ) { // could be indexor name
    rpos = cmd.rfind("F");
    if( rpos != string::npos && rpos != pos ) return true;
  }

  return false;
}

// helper func. inspects comma separated list and return hashmaps key=indexor, val= cmd
// of non-motion and motion commands
int UFPortescapConfig::splitCmdlist(const string& cmds, map< string, string >& motion, map< string, string >& nonmotion) {
  motion.clear(); nonmotion.clear();
  string cmd, indexor;
  bool move;
  size_t comma = cmds.find(",");
  if( comma == 0 ) {
    clog<<"UFPortescapConfig::splitCmdlist> ill formed command! (starts with a comma?): "<<cmds<<endl;
    return -1;
  }
  if( comma == string::npos ) { // assume single command
    move = isMotionCmd(cmds, indexor);

    if( move )
      motion[indexor] = cmds;
    else if( indexor != "" ) // null indexor?
      nonmotion[indexor] = cmds;
    else // null indexor?
      nonmotion["null"] = cmds;
      
    return 1;
  }
  // at least one comma was found, continue to find all others
  vector< size_t > commapos;
  commapos.push_back(comma);
  size_t i= 0, offset= 0;
  while( comma = cmd.find(",", 1+comma) != string::npos )
    commapos.push_back(comma);

  for( i= 0; i < commapos.size(); ++i ) {
    comma = commapos[i];
    cmd = cmds.substr(offset, comma);
    move = isMotionCmd(cmd, indexor);
    if( move ) {
      if( motion.count(indexor) ) // comma separate this list
        motion[indexor] = nonmotion[indexor] + "," +cmd;
      else
        motion[indexor] = cmd;
    }
    else if( indexor != "" ) {
      if( nonmotion.count(indexor) ) // comma separate this list
        nonmotion[indexor] = nonmotion[indexor] + "," +cmd;
      else
	nonmotion[indexor] = cmd;
    }
    else { // null indexor?
      if( nonmotion.count("null" ) ) // comma separate the null indexor list
        nonmotion[indexor] = nonmotion[indexor] + "," +cmd;
      else
        nonmotion["null"] = cmd;
    }

    offset = 1+comma;
  }
  // don't forget the last one:
  cmd = cmds.substr(offset);
  move = isMotionCmd(cmd, indexor);
  if( move ) {
    if( motion.count(indexor) ) // comma separate this list
      motion[indexor] = nonmotion[indexor] + "," +cmd;
    else
      motion[indexor] = cmd;
  }
  else if( indexor != "" ) { // null indexor?
    if( nonmotion.count(indexor) ) // comma separate this list
      nonmotion[indexor] = nonmotion[indexor] + "," +cmd;
    else
      nonmotion[indexor] = cmd;
  }
  else { // null indexor?
    if( nonmotion.count("null" ) ) // comma separate the null indexor list
      nonmotion[indexor] = nonmotion[indexor] + "," +cmd;
    else
      nonmotion["null"] = cmd;
  }
  
  return (int)max(motion.size(), nonmotion.size()); 
}

// fetch current step counts for indicated indexors
int UFPortescapConfig::queryStepCnts(map< string, double >& curstepcnts) {
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::queryStepCnts> no device connection?"<<endl;
    return -1;
  }
  UFPortescapTermServ* pts = dynamic_cast< UFPortescapTermServ* > ( _devIO );
  if( pts == 0 ) {
    clog<<"UFPortescapConfig::queryStepCnts> not a portescap device connection?"<<endl;
    return -2;
  }
  int retval= 0;
  map< string, double >::iterator it = curstepcnts.begin();
  for( ; it != curstepcnts.end() ; ++it ) {
    double val = INT_MIN;;
    string indexor = it->first;
    clog<<"UFPortescapConfig::queryStepCnts> indexor: "<<indexor<<endl;
    string reply= "", stepstat = indexor + "Z";
    pts->submit(stepstat, reply);
    size_t zpos = reply.find("Z");
    if( zpos == string::npos ) {
      clog<<"UFPortescapConfig::queryStepCnts> bad step stat return from indexor: "<<indexor<<", returned: "<<reply<<endl;
    }
    else {
      string sval = reply.substr(1+zpos);
      val = atof(sval.c_str());
    }
    curstepcnts[indexor] = val;
    ++retval;
  }
  return retval;
}

// new helper functions for agent action virtual
// sequenced cmds must be performed via ancillay func via exec or sim Motion funcs.
// immmediate cmds can be sent to device directly from agent action func.
// in this case the string values is the deliminated set of motion parameters,
// and/or status reqs, optionally followed by a single/atomic motion request
// set immed. and seq cmds. from cmdinfo*
// immediate commands must be Raw cmdnames and only one raw motion is allowed per indexor
// any negative return value should indicate an illegal (rejected) bundle)
// return number of indexors indicated in bundle with immediate cmds (from epics client expect 1)
int UFPortescapConfig::parseCmds(UFDeviceAgent::CmdInfo* bundle, map< string, string >& cars, map< string, string >& sads,
				 map< string, string >& immedcmds, map< string, string >& seqcmds) {
  immedcmds.clear(); seqcmds.clear();
  string cmdname, cmdimpl, motor, car, sad, raw;
  for( int i = 0; i < (int)bundle->cmd_name.size(); ++i ) {
    cmdname = bundle->cmd_name[i];
    cmdimpl = bundle->cmd_impl[i];
     if( _verbose ) 
        clog<<"UFPortescapConfig::parseCmds> "<<cmdname<<", "<<cmdimpl<<endl;

    // first look for SAD indication, need to know motor indexor associated with sad sir channel name...
    int spos = cmdname.find("sad");
    if( spos == (int)string::npos ) 
      spos = cmdname.find("Sad");
    if( spos == (int)string::npos ) 
      spos = cmdname.find("SAD");
    if( spos != (int)string::npos ) {
      sad = cmdimpl;
      if( spos > 0 ) { // assume motor indexor name is at pos 0 of sad?
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
        char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
        motor = indexor;
  	clog << "UFPortescapConfig::parseCmds> sad: "<<sad<<", parsing for: "<<motor<<endl;
      }
      continue;
    }

    // next look for CAR indication
    int cpos = cmdname.find("car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("Car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("CAR");

    if( cpos != (int)string::npos ) {
      car = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0 of Car?
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
        char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
        motor = indexor;
  	clog << "UFPortescapConfig::parseCmds> car: "<<car<<", parsing for: "<<motor<<endl;
      }
      continue;
    }
    // look for new F2 command: "IF2-N" where I is motor/indexor and N should indicate 1-4 or more seq. cmds
    // of the nature: "IFsteps 0/1::0, I+-steps::final, IU+-steps::final, I..."
    int f2pos = cmdname.find("F2-");
    if( f2pos == (int)string::npos ) 
      f2pos = cmdname.find("f2-");

    if( f2pos != (int)string::npos ) {
      if( f2pos > 0 ) { // assume motor indexor name is at pos 0 of F2-?
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
        char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
        motor = indexor;
      }
      int upos = cmdimpl.find("U");
      if( upos != (int)string::npos && upos > 0 ) // elim. U
	cmdimpl = cmdimpl.substr(0, upos) + cmdimpl.substr(1+upos);
      if( seqcmds.count(motor) ) {
	clog << "UFPortescapConfig::parseCmds> F2- supplement seqcmds for: "<<motor<<endl;
	seqcmds[motor] = seqcmds[motor] + "," + cmdimpl;
      }
      else {
  	clog << "UFPortescapConfig::parseCmds> F2- setting seqcmds for: "<<motor<<endl;
        seqcmds[motor] = cmdimpl;
      }
      continue;
    }

    // treat raw cmds like F2-N commands: i.e. like "IRaw-N" where I is motor/indexor and N should indicate 1 or more
    // (non-motion) commands of the nature: "IIinitVel, IRunHold , ISlewVel, I^, I@" although the abort is really a 
    // special case. should only allow one (raw) motion command
    int rpos = cmdname.find("raw");
    if( rpos == (int)string::npos )
      rpos = cmdname.find("Raw");
    if( rpos == (int)string::npos )
      rpos = cmdname.find("RAW");
    if( rpos != (int)string::npos ) {
      raw = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0 of Car?
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
        char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
        motor = indexor;
      }
      if( immedcmds.count(motor) && raw.find("F")  ) {
	clog << "UFPortescapConfig::parseCmds> supplement immedcmds for: "<<motor<<endl;
	immedcmds[motor] = immedcmds[motor] + "," + raw;
      }
      else {
	clog << "UFPortescapConfig::parseCmds> setting immedcmds for: "<<motor<<endl;
        immedcmds[motor] = raw;
      }
    }
  } // end for loop over bundle

  // now check the lists. assume for now the seqcmds provided by F2-N are ok
  // the immediate commands lists should not allow more than one motion...
  map< string, string >::iterator it = immedcmds.begin();
  for( ; it != immedcmds.end(); ++it ) {
    string indexor = it->first, cmds = it->second;
    clog<<"UFPortescapConfig::parseCmds> immediate cmds indexor: "<<indexor<<", cmds: "<<cmds<<endl;
    if( indexor == "null" )
      continue;
    // double check that all commands in commo separated list are meant for this indexor:
    map< string, string > motion, nonmotion;
    splitCmdlist(cmds, motion, nonmotion);
    if( nonmotion.count(indexor) ) {
      string nomo = nonmotion[indexor];
      clog<<"UFPortescapConfig::parseCmds> immediate nonmotions cmds indexor: "<<indexor<<", cmds: "<<nomo<<endl;
    }
    if( nonmotion.count(indexor) ) {
      string mo = motion[indexor];
      clog<<"UFPortescapConfig::parseCmds> immediate motion cmds indexor: "<<indexor<<", cmds: "<<mo<<endl;
      int nc = cntDelim(mo);
      if( nc > 1 ) {
	clog<<"UFPortescapConfig::parseCmds> immediate commands with more than 1 motion are illegal now!"<<endl;
	return -nc;
      }
    }  
  }
  return(int) immedcmds.size();
}

// submit immediates
int UFPortescapConfig::submitImmedCmds(map< string, string >& immedcmds, map< string, double >& curstepcnts) {
  return 0;
}

// once the action func has processed any/all immmediate cmds, it should use this
// to start any/all sequenced motions (aided by helpers below):
int UFPortescapConfig::startSeqCmds(map< string, string >& seqcmds, map< string, double >& curstepcnts) {
  return 0;
}

// start indexor motion return starting position step count
double UFPortescapConfig::startIndexorMotion(const string& indexor, const string& cmd) {
  return 0.0;
}

// query indexor for motion status -- return motion? true or false and latest/current stetp count
bool UFPortescapConfig::statMotion(const string& indexor, double& stepcnt) {
  return false;
}

// new helper functions for agent action virtual
// sequenced must be performed via ancillay func.
// the map key should ne either the mechanism or indexor name,
// the string value it the delimnated list of motion commands and expected
// intermediate/ final step counts 
/*
int UFPortescapConfig::sequenceCmds(std::map< string, string >& seqcmds, map< string, double >& curstepcnts) {
  seqcmds.clear(); return 0;
}

// immmediate cmds can be sent to device directly from agent action func.
// in this case the string values is the deliminated set of motion parameters,
// and/or status reqs, optionally followed by a single/atomic motion request
int UFPortescapConfig::immedCmds(std::map< string, string >& immedcmds, map< string, double >& curstepcnt) {
  immedcmds.clear(); return 0;
}
*/

string UFPortescapConfig::setMotionParms(const string& indexor, string& ivel, string& vel,
				  string& acc, string& holdrunc, string& res) {
  string reply, rval;
  if( ivel.empty() ) ivel= indexor + "I 100";
  _devIO->submit(ivel, reply); rval += reply;
  if( vel.empty() ) vel= indexor + "V 100";
  _devIO->submit(vel, reply); rval += reply;
  if( acc.empty() ) acc= indexor + "K 200";
  _devIO->submit(acc, reply); rval += reply;
  if( holdrunc.empty() ) holdrunc= indexor + "Y 0 50";
  _devIO->submit(holdrunc, reply); rval += reply;
  if( res.empty() ) res= indexor + "H 0";
  _devIO->submit(res, reply); rval += reply;
  return rval;
}

// note that this is not really a "jog" function, which the
// portescaps only support via its jog inputs (wires?)
bool UFPortescapConfig::jogMotor(const string& motor, const string& cmd, double curpos, string& jogcmd) {
  bool homecmd = (cmd.rfind("F") != string::npos && cmd.rfind("F") != 0);
  double hdif = ::fabs(curpos) - 0.01;
  // if this motor has never been datumed or origined its current position
  // may be ambiguous, so we must perform an inital step in the oposite direction
  // of the first home command to deal with the pathelogical behavior of
  // of some indexors when asked to home when already at home
  // consequently this jog action will insure that we will be in
  // the datum state when we issue the home command
  if( !homecmd || hdif > 0.01 ) {
    // no need to jiggle it (i.e. this is not a home req. and/or we are not at home...)
    jogcmd = "";
    return false;
  }
  // to insure that homing subsequent to this jog takes the shorter route,
  // always jog-step in the opposite direction of the following home, syntax of
  // homing motor A at speed 100 in direction 0: "AF100 0" or "AF100"
  // for direction 1: "AF100 1"
  // according to manual if indexor is in non-datum state (home switch low)
  // home dir == 0 motor will move in negative direction so we must jog in
  // positive (+) direction. conversely if dir == 1, jog inneg. (-) dirction:
  const char* cc = cmd.c_str();
  const char* sp0 = ::index(cc, ' ');
  const char* spr = ::rindex(cc, ' ');
  char* dir = 0;
  if( spr > sp0 )
    dir = ::rindex(spr, '1');

  if( dir ) { // 1
    jogcmd = motor;
    jogcmd += "-5";
  }
  else { // 0
    jogcmd = motor;
    jogcmd += "+5";
  }
  return true;
}

void UFPortescapConfig::setIndexorNames(int instrumId, const string& motors) {
  clog<<"UFPortescapConfig::setIndexorNames> indexors: "<<motors<<endl;
  bool flam2= false;
  switch( instrumId ) {
  case UFDeviceAgent::Flamingos1:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "a b c d e";
    break;
  case UFDeviceAgent::CanariCam:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "A B C D E F G H I J K L";
    break;
  case UFDeviceAgent::TReCS:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "A B C D E F G H I";
    break;
  case UFDeviceAgent::Flamingos2:  // default
  default:
    flam2= true;
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "A B C D E F G H";
    break;
  }

  // assuming space separated list of 1 character motor names:
  _Indexors.clear();
  int indxcnt = (1 + _Motors.length()) / 2;
  char* cm = (char*) _Motors.c_str(); // null-terminated c-string
  // nominal hold-run current settings
  string hrcval =  "0 15";
  char sport[5]; memset(sport, 0, sizeof(sport));
  char idxor[2]; memset(idxor, 0, sizeof(idxor));
  // reset portmap here if one or more indexors are not to be used:
  //const char* indexors = _Motors.c_str();
  // flam2 default is A:7024, B:7023, C:7022, D:7021, E:7020, F:7019, G:7018, H:7017
  // insure that subsetting is correct:
  string indexor = idxor;
  for( int i = 0; i < indxcnt; ++i, cm += 2 ) {
    idxor[0] = *cm;
    indexor = idxor;
    if( flam2 ) { // defaults for flam2 non-party (direct) line perle port config:
      switch( idxor[0] ) {
      case 'A': indexor += ":"; sprintf(sport, "%d", 7024); indexor += sport; break;
      case 'B': indexor += ":"; sprintf(sport, "%d", 7023); indexor += sport; break;
      case 'C': indexor += ":"; sprintf(sport, "%d", 7022); indexor += sport; break;
      case 'D': indexor += ":"; sprintf(sport, "%d", 7021); indexor += sport; break;
      case 'E': indexor += ":"; sprintf(sport, "%d", 7020); indexor += sport; break;
      case 'F': indexor += ":"; sprintf(sport, "%d", 7019); indexor += sport; break;
      case 'G': indexor += ":"; sprintf(sport, "%d", 7018); indexor += sport; break;
      case 'H': indexor += ":"; sprintf(sport, "%d", 7017); indexor += sport; break;
      default: break;
      }
    }
    _Indexors.push_back(indexor);
    //if( _verbose ) clog<<"UFPortescapConfig::setIndexorNames> "<<indexor<<endl; 
    _HoldRunC[indexor] = hrcval;
    // while we're at it, init/clear nominal motion setting command 'history'
    _HoldRunCcmd[indexor] = "";
    _SlewVcmd[indexor] = "";
    _AccelDecelcmd[indexor] = "";
    _InitVcmd[indexor] = "";
    _Settlecmd[indexor] = "";
    _Jogcmd[indexor] = "";
    _DatumCnt[indexor] = 0;
  }
}

// ctors
UFPortescapConfig::UFPortescapConfig(const string& name) : UFDeviceConfig(name) {
  _tsport = -1; 
  _tshost = "";
}

UFPortescapConfig::UFPortescapConfig(const string& name,
				     const string& tshost,
				     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// Portescap terminator is new-line (default):
//string UFPortescapConfig::terminator() { return "\n"; }

// Portescap prefix is none (default):
//string UFPortescapConfig::prefix() { return ""; }

// support setting party-line or direct-line indexor conf, may rely on _tsport being set in agent options:
int UFPortescapConfig::parsePorts(int& tsport, map< string, int >& portmap) {
  if( _Indexors.empty() ) {
    clog<<"UFPortescapConfig::parsePorts> empty indexor name list..."<<endl;
    return 0;
  }
  //clog<<"UFPortescapConfig::parsePorts> indexor name list size: "<<_Indexors.size()<<endl;
  for( size_t i = 0; i <_Indexors.size(); ++i ) {
    string idxr = _Indexors[i].substr(0, 1);
    if( _Indexors[i].find(":") != string::npos ) {
      string sprt = _Indexors[i].substr(2);
      const char* cs = sprt.c_str();
      //if( _verbose ) clog<<"UFPortescapConfig::parsePorts> "<< idxr << " : " << sprt <<endl;
      if( isdigit(*cs) ) { // this must be the first or only port #
        portmap[idxr] = ::atoi(cs);
	if( i == 0 ) tsport = portmap[idxr]; // force the first on the list?
      }
      else {
         portmap[idxr] = tsport + i;
      }
      // and truncate indexor name to eliminate port $
      _Indexors[i] = _Indexors[i].substr(0, 1);
    }
    else { // assume first as been provided in options?
      portmap[idxr] = tsport + i;
    }
  }
  if( _verbose) {
    map< string, int >::iterator it = portmap.begin();
    while( it != portmap.end() ) {
      clog<<"UFPortescapConfig::parsePorts> "<<it->first<<" "<<it->second<<endl;
      ++it;
    }
  }
  return (int) portmap.size();
}

// these are wrappers for the UFPortescapTermServ funcs:
int UFPortescapConfig::parsePorts(const string& indexorports, map< string, int >& portmap, const string& start) {
  int np = UFPortescapTermServ::parsePorts(indexorports, portmap, start);
  if( np <= 0 )
    return np;

  //if( _verbose ) {
    map< string, int >::iterator it = portmap.begin();
    while( it != portmap.end() ) {
      clog<<"UFPortescapConfig::parsePorts> "<<it->first<<" "<<it->second<<endl;
      ++it;
    }
  //} 
  return np;
}

// wrapper... if parsePorts return 1, assume party-line:
int UFPortescapConfig::partyLine(int port) {
  return UFPortescapTermServ::partyLine(port, _Indexors);
}

// wrapper...if multiple ports are indicated, assume direct lines:
int UFPortescapConfig::directLines(map< string, int >& portmap) {
  return UFPortescapTermServ::directLines(portmap);
}

// connect calles create which then connects
UFTermServ* UFPortescapConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFPortescapTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }
  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);
  string sot = prefix();
  _devIO->resetSOTS(sot);
  return _devIO;
}

// get the current position of each indexor
// and generate a status report
UFStrings* UFPortescapConfig::status(UFDeviceAgent* da) {
  char **names, **posnames;
  //  int cnt = mechNamesTReCS(&names, &posnames);
  int cnt = mechNamesFLAM2(&names, &posnames);
  cnt = (int) _Indexors.size(); // since we may not be using all motors
  vector < string > list, comments;
  string indexor, name;
  for( int i = 0; i < cnt; ++i ) {
    indexor = _Indexors[i];
    name = names[i];
    comments.push_back(name);
    double steps;
    da->getValue(indexor, steps);
    //char* nearest = posNameNearTReCS(name.c_str(), steps);
    char* nearest = posNameNearFLAM2(name.c_str(), steps);
    strstream s;
    s<<name<< " ("<<indexor<<")"<<" == "<<steps<<", "<<nearest; 
    //s<<"@"<<stepsFromHomeTReCS(name.c_str(), nearest); 
    s<<"@"<<stepsFromHomeFLAM2(name.c_str(), nearest); 
    s<<ends;
    list.push_back(s.str()); delete s.str();
  }
  return new UFStrings(da->name(), list);
}

UFStrings* UFPortescapConfig::statusFITS(UFDeviceAgent* da) {
  char **names, **posnames;
  int cnt= 0;
  char* (*posName)(const char*, double);
  double (*stepsFromHome)(const char*, char*);
  switch( da->instrumentId() ) {
    case UFDeviceAgent::Flamingos1: cnt = mechNamesFlam1(&names);
                                    posName = posNameNearFlam1;
                                    stepsFromHome = stepsFromHomeFlam1;
    break;
    default: cnt = mechNamesFLAM2(&names, &posnames); //mechNamesTReCS(&names, &posnames);
             posName = posNameNearFLAM2;
             stepsFromHome = stepsFromHomeFLAM2;
    break;
  }  
  cnt = (int) _Indexors.size(); // since we may not be using all motors
  map < string, string > valhash, comments;
  string indexor;
  for( int i = 0; i < cnt; ++i ) {
    indexor = _Indexors[i];
    string name = names[i];
    string posname = posnames[i];
    if( _verbose )
      clog<<"UFPortescapConfig> name, posname: "<<name<<", "<<posname<<endl;
    comments[name] = name + " (" + indexor + ") ";
    comments[posname] = posname + " (" + indexor + ") ";
    double steps;
    da->getValue(indexor, steps); // current step count of this indexor
    char* nearest = posName(name.c_str(), steps); // named position nearest this
    double namesteps = stepsFromHome(name.c_str(), nearest);
    double diff = steps - namesteps;
    strstream s;
    s<<nearest; // nearest named position
    if( diff < -0.01 )
      s<<" - "<< fabs(diff); 
    else if( diff > 0.01 )
      s<<" + "<< diff;

    s<<ends;
    string val = s.str();  delete s.str();
    comments[name] += val;
    comments[posname] = "Steps from Datum";
    valhash[name] = val;
    strstream ss;
    ss<<steps<<ends; 
    valhash[posname] = ss.str(); delete ss.str();
  }
  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

//assume reply should be 1 line except in certain cases...
int UFPortescapConfig::numReplyLinesExpected(const string& cmdimpl) {
  if (cmdimpl.rfind("X") != string::npos && cmdimpl.rfind("X") != 0)
    return 2;

  return 1;
}

// if the command is not a motion settings cmd, always return true
// check existing setting(s) (i.e. previous accepted command setting(s))
// and if the new request differs, return true; otherwise return false
bool UFPortescapConfig::newSettingCmd(const string& indexor, const string& cmdimpl) {
  string setting;
  if( cmdimpl.rfind("B") != string::npos && cmdimpl.rfind("B") != 0 ) { // jog
    setting = _Jogcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _Jogcmd[indexor] = cmdimpl;
  }
  if( cmdimpl.rfind("E") != string::npos && cmdimpl.rfind("E") != 0 ) { // settle
    setting = _Settlecmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _Settlecmd[indexor] = cmdimpl;
  }
  if( cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0 ) { // initial vel.
    setting = _InitVcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _InitVcmd[indexor] = cmdimpl;
  }
  if( cmdimpl.rfind("K") != string::npos && cmdimpl.rfind("K") != 0 ) { // accel/decl. 
    setting = _AccelDecelcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _AccelDecelcmd[indexor] = cmdimpl;
  }  
  if(cmdimpl.rfind("V") != string::npos && cmdimpl.rfind("V") != 0 ) { // slew
    setting = _SlewVcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _SlewVcmd[indexor] = cmdimpl;
  }
  if( cmdimpl.rfind("Y") != string::npos && cmdimpl.rfind("Y") != 0 ) { // hold & run currents
    setting = _HoldRunCcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _HoldRunCcmd[indexor] = cmdimpl;
  }
  return true;
}

vector< string >& UFPortescapConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("@"); // abort/stop
  _actions.push_back("["); // status
  _actions.push_back("+"); // + step
  _actions.push_back("-"); // - step
  _actions.push_back("B"); // jog speed
  _actions.push_back("E"); // settle time
  _actions.push_back("F"); // home
  _actions.push_back("M"); // continous (infinite) step
  _actions.push_back("N"); // near-home (our pseudo/bogus virtual home kludge)
  _actions.push_back("o"); // origin
  _actions.push_back("O"); // origin
  _actions.push_back("I"); // set initial velocity
  _actions.push_back("K"); // set accel/decel.
  _actions.push_back("S"); // save indexor settings to non-volatile mem.
  _actions.push_back("V"); // set slew velocity
  _actions.push_back("X"); // get indexor parameters (multi-line reply)
  _actions.push_back("Y"); // set hold & run current

  return _actions;
}

vector< string >& UFPortescapConfig::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("Z"); // query position
  _queries.push_back("^"); // query motion

  return _queries;
}


int UFPortescapConfig::validCmd(const string& c) {
  if( c.length() <= 1 )
   return -1; // indexor ID must precede cmd

  if( c.length() > 12 ) { // cmd too long
    clog<<"UFPortescapConfig::validCmd> cmd too long: "<<c<<endl;
    return -1; 
  }

  const char* cc = c.c_str();
  while( *cc == ' ' || *cc == '\t' ) 
    ++cc; // ignore leading white spaces

  // first check if indexor/motor name is present
  int i;
  string motname;
  for( i = 0; i < (int)_Indexors.size(); ++i ) {
    motname = _Indexors[i];
    const char* mcc = motname.c_str();
    //clog<<"UFPortescapConfig::validCmd> motor: "<<motname<<", c: "<<c<<endl;
    if( *mcc == *cc )
      break; // and check value of i
  }

  if( i >= (int)_Indexors.size() )  { // name not found...
    clog<<"UFPortescapConfig::validCmd> bad indexor name: "<<cc<<endl;
    return -1;
  }

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  i = 0;
  int cnt = (int)av.size();
  string mc = cc; // motor cmd stripped of any leading white-space
  while( i < cnt ) {
    //if( _verbose )
     // clog<<"UFPortescapConfig::validCmd> av: "<<av[i]<<", mc: "<<mc<<endl;
    if( mc.find(av[i++]) != string::npos ) {
      size_t hrc = mc.find("Y"); // hold-run-current cmd?
      if( hrc != string::npos && hrc > 0 ) { // save this
	if( _verbose )
	  clog<<"UFPortescapConfig::validCmd> old _HoldRunC: "<<_HoldRunC[motname]<<endl;
	_HoldRunC[motname] = mc.substr(hrc);
	if( _verbose )
	  clog<<"UFPortescapConfig::validCmd> reset _HoldRunC: "<<_HoldRunC[motname]<<endl;
      }
      return 1; 
    }
  }
  vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    //if( _verbose )
     // clog<<"UFPortescapConfig::validCmd> qv: "<<qv[i]<<", mc: "<<mc<<endl;
    if( mc.find(qv[i++]) != string::npos )
      return 1;
  }
  
  clog<<"UFPortescapConfig::validCmd> cmd unknown: "<<c<<endl; 
  return -1;
}

#endif // __UFPortescapConfig_cc__
