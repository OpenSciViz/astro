#if !defined(__UFPortescapConfig_h__)
#define __UFPortescapConfig_h__ "$Name:  $ $Id: UFPortescapConfig.h,v 0.14 2006/08/16 20:01:34 hon Exp $"
#define __UFPortescapConfig_H__(arg) const char arg##UFPortescapConfig_h__rcsId[] = __UFPortescapConfig_h__;

#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFPortescapTermServ.h"
#include "ufF2mechMotion.h"

using namespace std;
#include "string"
#include "deque"
#include "map"

// forward declarations for friendships
class UFPortescapAgent;
class UFGemPortescapAgent;

class UFPortescapConfig : public UFDeviceConfig {
public:
  UFPortescapConfig(const string& name= "UnknownPortescap@DefaultConfig");
  UFPortescapConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFPortescapConfig() {}

  // new helper functions for agent action virtual
  // sequenced cmds must be performed via ancillay func.
  // the map key should be either the mechanism or indexor name,
  // the string value is the delimnated list of motion commands and expected
  // intermediate/final step counts (via  mufF2mechMotion.h and ufflam2mech.h
  // set seq. cmds from cmdinfo*
  int sequenceCmds(UFDeviceAgent::CmdInfo* cmds, map< string, string >& seqcmds);

  // immmediate cmds can be sent to device directly from agent action func.
  // in this case the string values is the deliminated set of motion parameters,
  // and/or status reqs, optionally followed by a single/atomic motion request
  // set immed. cmds. from cmdinfo*
  int immedCmds(UFDeviceAgent::CmdInfo* cmds, map< string, string >& immedcmds);
  // use _devIO to submit the immediate cmds
  int submitImmedCmds(UFDeviceAgent::CmdInfo* cmds, map< string, string >& immedcmds);

  // once the action func has processed any/all immmediate cmds, it should use this
  // to start any/all sequenced motions (aided by helpers below):
  int startSeq(UFDeviceAgent::CmdInfo* cmds, map< string, string >& seqcmds);

  // helper func. parses cmd sequences and separates parms (vel, acc, run/hold current etc.),
  // from motion cmds. and expected final step counts for motions
  int parseSeq(map< string, string >& seqcmds, map< string, string >& parmcmds,
	       map< string, string >& motioncmds, map< string, int >& seqsteps);

  // helper uses above parsed strings to set cmd parm and motion (de)queues
  int setMotionSeq(map< string, string >& seqcmds, map< string, deque< int > >& seqparms,
		   map< string, deque< string > >& seqmotion, map< string, deque< int > >& seqsteps);

  // this is used by execMotion and execSim to evaluate the progress of each sequenced motion,
  // when this indicates a motion has completed, the next motion should be submitted, etc.
  int motionStat(map< string, deque< int > >& prevsteps,
		 map< string, deque< int > >& currentsteps, map< string, deque< bool > >& completed);


  static void setIndexorNames(int instrumId, const string& motors= "");
  static bool newSettingCmd(const string& indexor, const string cmdimpl);
  static int numReplyLinesExpected(const string cmdimpl);

  // to support party-line or direct-line indexors:
  static int parsePorts(const string indexorports, map< string, int >& portmap, const string& start= "A");
  // wrappers to support party-line or direct-line indexors:
  // if _Indexors look like: "A:portA B:portB ... N:portN", or not, use tsport 
  static int parsePorts(int& tsport, map< string, int >& portmap);
  // if parsePorts return 1, assume party-line:
  static int partyLine(int port);
  // if multiple ports are indicated, assume direct lines:
  static int directLines(map< string, int >& portmap);

  // override these virtuals:
  // device i/o behavior
  virtual UFTermServ* connect(const string& host= "", int port= 0);

  virtual UFStrings* status(UFDeviceAgent*);
  virtual UFStrings* statusFITS(UFDeviceAgent*);

  // device cmds
  virtual vector< string >& UFPortescapConfig::queryCmds();
  virtual vector< string >& UFPortescapConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  static bool jogMotor(const string& motor, const string& cmd, double curpos, string& jogcmd);

  static bool _verbose;
  static int _instrumId;

  // keep last hold-run current settings
  static map< string, string > _HoldRunC;
  static map< string, int> _DatumCnt;
  static string setMotionParms(const string& indexor, string& ivel, string& vel, string& acc,
			     string& holdrunc, string& res);

  // support 2 limit switches in simulation mode (use the same two vals for all indexors)
  // on cmd 'M -/+vel' step to current-_leftLim or current+_rightLim
  static double _leftLim, _MinLim, _rightLim, _MaxLim;

protected:
  static vector <string> _Indexors; // names of All indexors 
  static string _Motors; // names as single cmd-line string
  // keep track of prior settings cmd:
  static map< string, string> _HoldRunCcmd;
  static map< string, string> _SlewVcmd;
  static map< string, string> _AccelDecelcmd;
  static map< string, string> _InitVcmd;
  static map< string, string> _Settlecmd;
  static map< string, string> _Jogcmd;

  // allow Agents access to static _Indexors
  friend class UFPortescapAgent;
  friend class UFGemPortescapAgent;
};

#endif // __UFPortescapConfig_h__
