#if !defined(__UFGemPortescapAgent_cc__)
#define __UFGemPortescapAgent_cc__ "$Name:  $ $Id: UFGemPortescapAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemPortescapAgent_cc__;

// useful gdb cmds:
//b 'UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo *)'
//r -epics flam -tshost flamperle -tsport 7024 -motors "A B C D E F G"
//b UFGemPortescapAgent::_execMovement

#include "UFGemPortescapAgent.h"
#include "UFPortescapConfig.h"
#include "UFSADFITS.h"
#include "ufflam2mech.h"
#include "UFStringTokenizer.h"

#include "algorithm"

// statics 
const int _initialPosition= 1000; // for simulation mode (require datum)
static map<string, bool> _setIdleOrErr;
static map<string, string> _indexorCAR;

// globals
string UFGemPortescapAgent::_SADDatumCnt;
bool UFGemPortescapAgent::_resetHRC= false;
int UFGemPortescapAgent::_instrumId;
string UFGemPortescapAgent::_currentval;
string UFGemPortescapAgent::_reqval;
time_t UFGemPortescapAgent::_timeOut = 180; // seconds
time_t UFGemPortescapAgent::_clock = 0; // seconds
double UFGemPortescapAgent::_NearHome = 10.0;
double UFGemPortescapAgent::_simSpeed = 2.0; // must be > 1

map<string, string> UFGemPortescapAgent::_Status; // use this instead of _StatList for EDB/CA names
map<string, string> UFGemPortescapAgent::_ErrPosition; // error message

map<string, string> UFGemPortescapAgent::_HomeThenStep; // compound motion seq.: first home then step
map<string, string> UFGemPortescapAgent::_HomeStepHome; // compound motion: home then step then home step parms
map<string, string> UFGemPortescapAgent::_HomeHome; // compound motion: home then step then home 2nd home parms
map<string, string> UFGemPortescapAgent::_InitVelCmd; // last/latest initial velocity command setting

map<string, double> UFGemPortescapAgent::_DesPosition; // desired/requested
map<string, double> UFGemPortescapAgent::_CurPosition; // current position (steps from home)
map<string, double> UFGemPortescapAgent::_Motion; // in-motion desired position (steps from home)
map<string, double> UFGemPortescapAgent::_Homing; // homing desired position should always be 0
map<string, double> UFGemPortescapAgent::_HomingFinal; // home-step-home
map<string, double> UFGemPortescapAgent::_NearHoming; // homing desired position should always be 0
map<string, double> UFGemPortescapAgent::_LimitSeeking; // seeking limit desired position should always be +-Limit

map<string, vector<string> > UFGemPortescapAgent::_ParamSads; // indexor motion paramater sads

string UFGemPortescapAgent::_guessSADchan(const string& indexor) {
  // note there are now 8 F2 indexors
  string sad;
  if( indexor == "A" ) sad = _StatRecs[0];
  if( indexor == "B" ) sad = _StatRecs[1];
  if( indexor == "C" ) sad = _StatRecs[2];
  if( indexor == "D" ) sad = _StatRecs[3];
  if( indexor == "E" ) sad = _StatRecs[4];
  if( indexor == "F" ) sad = _StatRecs[5];
  if( indexor == "G" ) sad = _StatRecs[6];
  if( indexor == "H" ) sad = _StatRecs[7];
  //if( _verbose )
  clog<<"UFGemPortescapAgent::_guessSADchan> indexor: "<<indexor<<", sad: "<<sad<<endl;
  return sad;
}

// public:
// this virtual should be called from options()...
void UFGemPortescapAgent::setDefaults(const string& instrum, bool initsad) {
  // clear status rec/chan container and (re)set
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;

  if( _verbose )
    clog<<"UFGemPortescapAgent::setDefaults> _epics: "<<_epics<<", initsad: "<<initsad<<endl;

  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //_confGenSub = _epics + ":cc:configG";
    //_heart = _epics + ":cc:heartbeat.INP";
    _heart = _epics + ":cc:heartbeat.VAL";
    // for( int i = 0; i < (int) UFSADFITS::_ccsad->size(); ++i )
    //  _StatRecs.push_back((*UFSADFITS::_ccsad)[i] + ".INP");
    std::map< string, string > _mechNameOfIndexor;
    UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();
    UFFLAM2MechPos* mech;
    for(int m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
      string indexor = infoFLAM2MechOf(m, &mech); // sets mech ptr
      string mname = mech->name;
      //string sadchan = _epics + ":sad:" + mname + "Steps.INP";
      string sadchan = _epics + ":sad:" + mname + "Steps.VAL";
      //MOS steps == MOSBarCd steps
      if (mname.find("BarCd") == string::npos) { // don't place BarCd sad into this list 
	_StatRecs.push_back(sadchan);
        //if( _verbose )
	  clog<<"UFGemPortescapAgent::setDefaults> indexor: "<<indexor<<", "<<mname<<", sad: "<<sadchan<<endl;
      }
      string sadmname (mname);
      transform (sadmname.begin (), sadmname.end (), sadmname.begin (), (int(*)(int)) toupper);
      if (sadmname.size() > 6) { // one of the filter wheels
	sadmname = sadmname.substr(0,4) + sadmname[sadmname.size()-1];
      }
      //sadchan = _epics+":sad:"+sadmname+"IV.INP";
      sadchan = _epics+":sad:"+sadmname+"IV.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"SV.INP";
      sadchan = _epics+":sad:"+sadmname+"SV.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"AC.INP";
      sadchan = _epics+":sad:"+sadmname+"AC.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"DC.INP";
      sadchan = _epics+":sad:"+sadmname+"DC.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"HC.INP";
      sadchan = _epics+":sad:"+sadmname+"HC.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"RC.INP";
      sadchan = _epics+":sad:"+sadmname+"RC.VAL";
      _ParamSads[indexor].push_back(sadchan);
      //sadchan = _epics+":sad:"+sadmname+"DV.INP";
      sadchan = _epics+":sad:"+sadmname+"DV.VAL";
      _ParamSads[indexor].push_back(sadchan);
    }    
    //_SADDatumCnt = _epics + ":cc:DATUMCNT.INP"; //_StatRecs[0];
    _SADDatumCnt = _epics + ":cc:DATUMCNT.VAL"; //_StatRecs[0];
    _statRec = "";
    _carRec = ""; // ":cc:datumC" or "cc:setupC" or "cc:abortC", etc.
  }
  else {
    //_confGenSub = "";
    _heart = "";
    _SADDatumCnt = "";
    _statRec = "";
    _carRec = "";
  }
} // setDefaults

// ctors:
UFGemPortescapAgent::UFGemPortescapAgent(int argc,
					 char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve Portescap readings
  _flush = 0.02; // 0.05; // 0.35; 
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //setDefaults();
}

UFGemPortescapAgent::UFGemPortescapAgent(const string& name,
					 int argc,
				         char** argv) : UFGemDeviceAgent(name, argc, argv) {
  _flush = 0.02; //  0.05; 
  // use ancillary function to retrieve LS340 readings
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  //setDefaults();
}

int UFGemPortescapAgent::main(int argc, char** argv) {
  UFGemPortescapAgent ufs("UFGemPortescapAgent", argc, argv); // ctor binds to listen socket
  //UFProtocol::_verbose = true;
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemPortescapAgent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  // extra security we can do without for now...
  /*
  if( clname.find("flam:") != string::npos )
    UFGemDeviceAgent::_epics = _epics = "flam";
  else if( clname.find("foo:") != string::npos )  
     UFGemDeviceAgent::_epics = _epics = "foo";

  if( !( _epics == "flam" || _epics == "foo") )
    return clname;
  */

  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string linecnt =  _epics + genSub + ".VALA";
  string positions =  _epics + genSub  + ".VALB";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemPortescapAgent::newClient> Epics client." <<clname<<endl;

  /* don't care right now
  int nr = getEpicsOnBoot(_epics, linecnt, _StatList);
  if( nr > 0 && _StatList.size() > 0 ) {
    linecnt = atof(_StatList[0].c_str());
  }

  nr = getEpicsOnBoot(_epics, positions, _StatList);
  */

  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemPortescapAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemPortescapAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemPortescapAgent::options(string& servlist) {
  //int instrumId= UFDeviceAgent::TReCS;
  string arg;
  int instrumId= UFDeviceAgent::Flamingos2;
  int port = UFGemDeviceAgent::options(servlist);

  arg = findArg("-v");
  if( arg != "false" )
    UFPortescapConfig::_verbose = _verbose = true;

  arg = findArg("-vv");
  if( arg != "false" )
    UFPortescapConfig::_verbose = _verbose = true;

  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  _config = new UFPortescapConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = 7005;
  //_config->_tshost = "ufannex"; // "192.168.111.101";
  _config->_tshost = "flamperle"; // "192.168.111.102"; "192.168.111.125"

  arg = findArg("-trecs"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::TReCS;
  arg = findArg("-flam"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos2;
  arg = findArg("-flam1"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos1;
  arg = findArg("-flam2"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos2;
  arg = findArg("-canari"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::CanariCam;

  // be sure to set indexor names before parsing ports:
  UFPortescapConfig::setIndexorNames(instrumId);
  arg = findArg("-motor"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-motors"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-indexor"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-indexors"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);

  // parse ports to determine if party-line or direct-line mode is in use:
  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" ) {
    int np = 0;
    map< string, int > portmap;
    if( _instrumId == UFDeviceAgent::Flamingos1 )
      np = UFPortescapConfig::parsePorts(arg, portmap, "a");
    else // assume default "A"
      np = UFPortescapConfig::parsePorts(arg, portmap);
    clog<<"UFGemPortescapAgent::options> nports: "<<np<<endl;
    if( np > 1 || instrumId == UFDeviceAgent::Flamingos2 ) {
      UFPortescapConfig::directLines(portmap);
    }
    else {
      _config->_tsport = atoi(arg.c_str());
      UFPortescapConfig::partyLine(_config->_tsport);
    }
  }
  else if( arg == "false" ) { // perhaps provided by indexor list ala: "A:7010 B:2011 ... N:70NN" 
    map< string, int > portmap;
    int np = UFPortescapConfig::parsePorts(_config->_tsport, portmap);
    if( np > 1 || instrumId == UFDeviceAgent::Flamingos2 ) {
      UFPortescapConfig::directLines(portmap);
    }
    else {
      _config->_tsport = atoi(arg.c_str());
      UFPortescapConfig::partyLine(_config->_tsport);
    }
  }

  arg = findArg("-speed");
  if( arg != "false" && arg != "true" )
    _simSpeed = atof(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;


  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  arg = findArg("-reset");
  if( arg != "false" )
    _resetHRC = true;
 
  if( _config->_tsport > 7000 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52005;
  }

  if( _verbose ) 
    clog<<"UFGemPortescapAgent::options> set port: "<<port<<", epics: "<<_epics<<endl;

  return port;
}

void UFGemPortescapAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  if( _verbose )
    clog << "UFGemPortescapAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  /*
  if( _config->_devIO == 0 ) {
    clog<<"UFGemPortescapAgent::startup> no Device config object?"<<endl;
    init();
  }
  else
  */
  time_t t = time(0);
  init(_config->_tshost, _config->_tsport);
  UFSocketInfo socinfo = _theServer.listen(listenport);
  t = time(0) - t;
  clog << "UFGemPortescapAgent::startup> Ok, startup completed (in "<<t
       <<" sec.), listening on port= " <<listenport<<endl;

  return;
}

UFTermServ* UFGemPortescapAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  // send initial status info to epics db if possible
  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemPortescapAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemPortescapAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  if( _verbose )
    clog<<"UFPortescapAgent> indexor names: "<<UFPortescapConfig::_Motors<<endl;
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.portescap."<<p<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemPortescapAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return ts;
  }
  delete s.str();
  */
  if( _sim ) {
    string motor;
    for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
      motor = UFPortescapConfig::_Indexors[i];
      _HomeThenStep[motor] = "false"; // compound motion seq.: first home then step
      _HomeStepHome[motor] = "false"; // compound motion: home then step then home
      _HomeHome[motor] = "false"; // compound motion: home then step then home
      //clog<<"UFGemPortescapAgent::init> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
      //clog<<"UFGemPortescapAgent::init> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
      _CurPosition[motor] = _initialPosition;
      strstream s; s<<_CurPosition[motor]<<ends; _currentval = s.str(); delete s.str();
      clog<<"Initial indexor position: "<<motor<<" == "<<_CurPosition[motor]<<endl;
      if( !_epics.empty() && _epics != "false" ) sendEpics(_StatRecs[i], _currentval);
    }
    /*
    int  nc = ::write(fd, "UFGemPortescapAgent> simulation.", strlen("UFGemPortescapAgent> simulation."));
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> sim log failed."<<endl;
    }
    ::close(fd);
    */
    return 0;
  }
  
  // connect to the device:
  clog<<"UFGemPortescapAgent::init> connect to device port(s)..."<<endl;
  ts = _config->connect(tshost, tsport);
  if( ts == 0 || _config->_devIO == 0 ) {
    clog<<"UFGemPortescapAgent::init> failed to connect to device port..."<<endl;
    return 0;
  }
  // insure that _devIO points to portescap extension class:
  UFPortescapTermServ* pts = dynamic_cast<UFPortescapTermServ*> ( ts );
  if( pts == 0 ) {
    clog<<"UFGemPortescapAgent::init> connect to device port did not return portescap extension?"<<endl;
    return 0;
  }

 ts = _config->_devIO = pts; 

  if( _verbose )
    clog<<"UFGemPortescapAgent> checking availiblity of portescap indexors"<<endl;

  // test connectivity to indexors, save transaction to a file
  string querystat, portescap; // portescap cmd & reply strings 
  string fullstatus = " [ 1990 23 "; // should be function in UFPortescapConfig
  string motor;
  for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    _setIdleOrErr[motor] = false;
    _HomeThenStep[motor] = "false"; // compound motion seq.: first home then step
    _HomeStepHome[motor] = "false"; // compound motion: home then step then home
    _HomeHome[motor] = "false"; // compound motion: home then step then home
    //clog<<"UFGemPortescapAgent::init> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
    //clog<<"UFGemPortescapAgent::init> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
    //clog<<"UFGemPortescapAgent::init> _HomeHome for "<<motor<<" = "<<_HomeHome[motor]<<endl;
    // perform hard abort/clear/escape of any currently executing indexor nonsense
    //_config->_devIO->escape(motor, 0.3); 
    // get current position
    querystat = motor + "Z";    
    int nc = _config->_devIO->submit(querystat, portescap, _flush);
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor "<<motor<<" position status failed..."<<endl;
      continue;
    }
    rmJunk(portescap);
    string spos = portescap.substr(1+portescap.rfind("Z"));
    double pos = atof(spos.c_str());
    _CurPosition[motor] = pos;
    clog<<"Initial indexor position: "<<motor<<" == "<<_CurPosition[motor]<<endl;
    strstream s; s<<_CurPosition[motor]<<ends; _currentval = s.str(); delete s.str();
    if( !_epics.empty() &&  _epics != "false" )
      sendEpics(_StatRecs[i], _currentval);
    /*
    // get full status block
    querystat = motor + fullstatus;
    nc = _config->_devIO->submit(querystat, portescap, _flush); 
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status failed..."<<endl;
      break;
    }
    clog<<"UFPortescapAgent> indexor full status succeeded, nc: "<<nc<<endl;
    */
    /*
    nc = ::write(fd, querystat.c_str(), querystat.length());
    nc = ::write(fd, portescap.c_str(), portescap.length());
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status log failed..."<<endl;
    }
    */
  }
  //::close(fd); // closing it forces io flush

  return ts;
}

void* UFGemPortescapAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemPortescapAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemPortescapAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
    if( _verbose && (clck % 2 == 0) )
      clog<<"ancillary> sent Epics heartbeat: "<<clck<<", _heart: "<<_heart<<endl;
  }

  // any motion to monitor?
  if( _Motion.empty() && _LimitSeeking.empty() &&
      _Homing.empty() && _HomingFinal.empty() && _NearHoming.empty() ) { // none
    if( _verbose && (clck % 10 == 0) )
      clog<<"ancillary> All Motion lists are empty, clock: "<<clck<<endl;
    return p;
  }

  if( _sim || _config->_devIO == 0 ) {
    if( !_NearHoming.empty() ) { // motor near homing is simulated
      //if( _verbose )
        clog<<"ancillary> near homing "<<endl;
       _simMovement(_NearHoming);
    }
    if( !_Motion.empty() ) { // motor stepping is simulated
      //if( _verbose )
        clog<<"ancillary> stepping "<<endl;
      _simMovement(_Motion);
    }
    if( !_LimitSeeking.empty() ) { // motor limit seeking is simulated
      //if( _verbose )
        clog<<"ancillary> seeking limit "<<endl;
      _simMovement(_LimitSeeking);
    }
    if( !_Homing.empty() ) { // motor homing is simulated
      //if( _verbose )
        clog<<"ancillary> homing "<<endl;
      _simMovement(_Homing);
    }
    if( !_HomingFinal.empty() ) { // final motor homing is simulated
      //if( _verbose )
        clog<<"ancillary> final homing "<<endl;
      _simMovement(_HomingFinal);
    }
    return p;
  }

  // this will check _HomeThenStep compound request, and
  // will submit a step motion cmd if necessary to complete
  // the request
  if( !_NearHoming.empty() ) {
    if( _verbose )
      clog<<"ancillary> near homing "<<endl;
    _execMovement(_NearHoming);
  }
  if( !_Motion.empty() ) {
    if( _verbose )
      clog<<"ancillary> stepping "<<endl;
   _execMovement(_Motion);
  }
  if( !_LimitSeeking.empty() ) {
    if( _verbose )
      clog<<"ancillary> seeking limit "<<endl;
   _execMovement(_LimitSeeking);
  }
  if( !_Homing.empty() ) {
    if( _verbose )
      clog<<"ancillary> homing "<<endl;
    _execMovement(_Homing);
  }
  if( !_HomingFinal.empty() ) {
    if( _verbose )
      clog<<"ancillary> final homing "<<endl;
    _execMovement(_HomingFinal);
  }
  return p;
}

void UFGemPortescapAgent::datumCompleted(int errcnt, string& errmsg) {
  // was this a succesfull datum?
  double sum= 0.0;
  int datumcnt= MAXINT, cnt= MAXINT;
  strstream s;
  map<string, double>::iterator it = _CurPosition.begin();
  while( it != _CurPosition.end() ) {
    cnt = UFPortescapConfig::_DatumCnt[it->first];
    if( cnt <= datumcnt ) datumcnt = cnt;
    if( _verbose )
      clog<<"UFGemPortescapAgent::datumCompleted> datumcnt: "<<it->first<<", "<<cnt<<", "<<datumcnt<<endl;
    sum += it->second;
    ++it;
  }
  s << datumcnt << ends;
  string sd = s.str(); delete s.str();
  if( _verbose )
    clog<<"UFGemPortescapAgent::datumCompleted> datumcnt: "<<datumcnt<<", sum: "<<sum
        <<", sad: "<<_SADDatumCnt<<", car: "<<_carRec<<endl;
  bool datumed = (sum > -0.1 && sum < 0.1);
  if( errcnt <= 0 ) {
    if( _carRec != "" )
      setCARIdle(_carRec);
    if( datumed )
      sendEpics(_SADDatumCnt, sd);
  }
  else if( _carRec != "" ) {
    setCARError(_carRec, &errmsg);
  }
  return;
}

// the real thing:
void UFGemPortescapAgent::_execMovement(map<string, double>& moving) {
  if( moving.empty() )
    return;

  vector <string> erase;
  string errmsg, reply, qstopped = " ^ ", qsteps = " Z "; // steps from home/origin
  map<string, double>::iterator i = moving.begin();
  int errcnt= 0;
  do {
    reply = "";
    string indexor = i->first;
    // since this function is used for all motion (stepping or homing, or home-then-step, or home-step-home)
    bool stepping = !_Motion.empty();
    if( stepping ) { // is this indexor stepping?
      map<string, double>::iterator fnd = _Motion.find(indexor);
      if( fnd == _Motion.end() )
	stepping = false;
    }
    bool seeking = !_LimitSeeking.empty();
    if( seeking ) { // is this indexor stepping?
      map<string, double>::iterator fnd = _LimitSeeking.find(indexor);
      if( fnd == _LimitSeeking.end() )
	seeking = false;
    }
    bool nearhoming = !_NearHoming.empty(); // (diff < 0.1);
    if( nearhoming ) { // is this indexor nearhoming?
      map<string, double>::iterator fnd = _NearHoming.find(indexor);
      if( fnd == _NearHoming.end() )
	nearhoming = false;
    }
    // it is possible to (initially) find indexor in _Homing and _HomingFinal
    // so we need to be carefull what is tested here... 
    bool homing = !_Homing.empty(); // (des > -0.1 && des < 0.1);
    bool homingF = !_HomingFinal.empty(); // (des > -0.1 && des < 0.1);
    if( homing ) { // check this indexor homing?
      map<string, double>::iterator fnd = _Homing.find(indexor);
      if( fnd == _Homing.end() ) // not this indexor
	homing = false;
      else
	homingF = false; // force this to false
    }
    if( homingF ) { // check this indexor homingFinal?
      map<string, double>::iterator fnd = _HomingFinal.find(indexor);
      if( fnd == _HomingFinal.end() ) // not this indexor
	homingF = false;
    }
    //if( _verbose )
      clog<<"UFGemPortescapAgent::_execMovement> indexor: "<<indexor
	  <<", stepping: "<<stepping<<", seeking: "<<seeking
	  <<", homing: "<<homing<<", homingFinal: "<<homingF<<endl;

    errmsg = _ErrPosition[indexor];
    string query = indexor + qsteps;
    _config->_devIO->submit(query, reply, _flush); // expect single line reply
    //if( _verbose )
      clog<<"UFGemPortescapAgent::_execMovement> steps reading: "<<reply<<endl;
    if( reply.empty() ) {
      clog<<"UFGemPortescapAgent::_execMovement> no reply from "<<indexor<<endl;
      continue;
    }
    size_t pos = reply.find(qsteps);
    if( pos == string::npos ) pos = 0; // assume it?
    pos += qsteps.length(); 
    _currentval = reply.substr(pos);

    // send current value to epics
    if( !_epics.empty() && _epics != "false" ) {
      string gsad = _guessSADchan(indexor);
      _statRec = _Status[indexor];
      if( _statRec.empty() ) {
        clog<<"UFGemPortescapAgent::_execMovement> indexor: "<<indexor<<" has No client supplied sad, using: "<<_statRec<<endl;
	_statRec = _Status[indexor] = gsad;
      }
      sendEpics(_statRec, _currentval);
      if( _statRec != gsad ) {
        clog<<"UFGemPortescapAgent::_execMovement> indexor: "<<indexor<<" client supplied alternate sad: "<<_statRec<<endl;
        clog<<"UFGemPortescapAgent::_execMovement> indexor: "<<indexor<<" also send status to sad: "<<gsad<<endl;
        sendEpics(gsad, _currentval);
      }
      //if( _verbose )
        clog<<"UFGemPortescapAgent::_execMovement>> "<<_statRec<<" == "<<_currentval<<endl;
    }
    else {
      clog<<"UFGemPortescapAgent::_execMovement>> No epics? "<<_epics<<endl;
    }
	
    // check for completed motion:
    double steps = atof(_currentval.c_str());
    double prev_steps = _CurPosition[indexor];
    // if homing _DesPosition[indexor] == 0.0 
    // in positive direction, steps will simply keep increasing
    double pdiff = ::fabs(steps - prev_steps);
    if( pdiff > 0.11 ) // reset current   
      _CurPosition[indexor] = steps;
    double des = _DesPosition[indexor];
    double diff = ::fabs(steps - des);
    // if homing _DesPosition[indexor] == 0.0 
    // in positive direction, steps will simply keep increasing, (diff > pdiff)
    bool checkForCompletion = false;
    if( des > -0.1 && des < 0.1 && pdiff > diff) // homed?
      checkForCompletion = true;
    else if( diff <= 0.1 || pdiff <= 0.1 ) // stepped or near-homed?
      checkForCompletion = true;

    if( checkForCompletion ) { // check if movement is complete (indexor stationary) 
      query = indexor + qstopped;
      _config->_devIO->submit(query, reply, _flush); // expect single line reply
      if( _verbose )
        clog<<"UFGemPortescapAgent::_execMovement> (check completion) motion query: "<<reply<<endl;

      if( reply.empty() ) {
        clog<<"UFGemPortescapAgent::_execMovement> no reply from "<<indexor<<endl;
        continue;
      }
      pos = query.length() + reply.find(query);
      const char* ms = (reply.substr(pos)).c_str();
      int mstat = atoi(ms);
      if( _verbose ) {
        clog<<"UFGemPortescapAgent::_execMovement> _HomeThenStep for "<<indexor<<" = "<<_HomeThenStep[indexor]<<endl;
        clog<<"UFGemPortescapAgent::_execMovement> _HomeStepHome for "<<indexor<<" = "<<_HomeStepHome[indexor]<<endl;
        clog<<"UFGemPortescapAgent::_execMovement> _HomeHome for "<<indexor<<" = "<<_HomeHome[indexor]<<endl;
      }
      // if this part of a compund motion, which type?
      size_t snpos = _HomeThenStep[indexor].find("-");
      size_t sppos = _HomeThenStep[indexor].find("+");
      bool homethenstep = false, homestephome = false;
      if( snpos != string::npos || sppos != string::npos ) {
	homethenstep = true;
      }
      else {
        snpos = _HomeStepHome[indexor].find("-");
        sppos = _HomeStepHome[indexor].find("+");
        if( snpos != string::npos || sppos != string::npos )
	  homestephome = true;
        // or final home
        sppos = _HomeHome[indexor].find("F");
        if( snpos != string::npos || sppos != string::npos )
	  homestephome = true;
      }

      if( mstat == 0 ) { // completed motion, was it real/near homing or stepping?
        erase.push_back(indexor);
	// check position one last time and...
        query = indexor + qsteps;
        if( _verbose )
	  clog<<"UFGemPortescapAgent::_execMovement> (motionless?) steps reading "<<query<<endl;

	//sleep(0.1);
        reply = "";
        _config->_devIO->submit(query, reply, _flush); // expect single line reply
        if( _verbose )
          clog<<"UFGemPortescapAgent::_execMovement> (really completed?) motion query: "<<reply<<endl;
        if( reply.empty() ) {
          clog<<"UFGemPortescapAgent::_execMovement> no reply from "<<indexor<<endl;
          continue;
        }
        pos = reply.find(qsteps);
        if( pos == string::npos ) pos = 0; // assume it?
        pos += qsteps.length();
        _currentval = reply.substr(pos);
        if( _statRec != "" ) {
          sendEpics(_statRec, _currentval);
          if( _verbose )
            clog<<"UFGemPortescapAgent::_execMovement> "<<_statRec<<" == "<<_currentval<<endl;
        }
        steps = atof(_currentval.c_str());
        prev_steps = _CurPosition[indexor];
        pdiff = ::fabs(steps - prev_steps);
        _CurPosition[indexor] = steps;

        bool athome= false;
	if( homing || homingF || nearhoming ) {
	  if( homing || homingF ) { // must send Origin cmd
            string orign = indexor + "O";
            _config->_devIO->submit(orign, reply, _flush); // expect single line reply
            if( _verbose )
              clog<<"UFGemPortescapAgent::_execMovement> homed, sent origin: "<<reply<<endl;
    	    _DesPosition[indexor] = _CurPosition[indexor] = 0.0;
	    _currentval = "0.0"; // orgined
	    // send epics home/origin position value: 
            if( _statRec != "" ) {
	      sendEpics(_statRec, _currentval);
              if( _verbose )
                clog<<"UFGemPortescapAgent::_execMovement> "<<_statRec<<" == "<<_currentval<<endl;
            }
            athome = true;
	    // set the datum cnt for this indexor:
	    int datumcnt = UFPortescapConfig::_DatumCnt[indexor];
            if( datumcnt < 0 )
	      datumcnt = 1;
	    else
	      ++datumcnt;
	    UFPortescapConfig::_DatumCnt[indexor] = datumcnt;

	    if( _resetHRC ) {
	      string holdrunc = indexor + " 0 0";
              _config->_devIO->submit(holdrunc, reply, _flush); // expect single line reply
              if( _verbose )
                clog<<"UFGemPortescapAgent::_execMovement> reset hold-run-current: "<<reply<<endl;
	      holdrunc = indexor + " " + UFPortescapConfig::_HoldRunC[indexor];
              _config->_devIO->submit(holdrunc, reply, _flush); // expect single line reply
              if( _verbose )
                clog<<"UFGemPortescapAgent::_execMovement> reset hold-run-current: "<<reply<<endl;
            }
	  }
	  else if( nearhoming ) {
    	    _DesPosition[indexor] = _NearHoming[indexor]; // _NearHome;
            if( _verbose )
              clog<<"UFGemPortescapAgent::_execMovement> nearhoming indexor, curr. pos.: "
		  <<indexor<<", "<<_CurPosition[indexor]
		  <<", des. pos.: "<<_DesPosition[indexor]
                  <<", _NearHoming[indexor]= "<<_NearHoming[indexor]
                  <<", _NearHome= "<<_NearHome<<endl;
	  }
          if( _indexorCAR[indexor] != ""&& !homethenstep && !homestephome ) { // nearhome only
	    //sendEpics(_statRec, "OK");
            if( _verbose )
  	      clog<<"UFGemPortescapAgent::_execMovement> "<<_carRec<<" Idle."<<endl;
	    if( _indexorCAR[indexor] != _carRec ) {
	      setCARIdle(_indexorCAR[indexor]); _setIdleOrErr[indexor] = true;
	    }
	  }
	} // set athome
	  
	if( homing || homingF || nearhoming ) { // finished (first) real or near homing
	  // homethenstep applies to nearhome cmd as well as real home cmd
	  // homestephome only applies to real home
	  if( (athome || nearhoming) && !homingF && (homethenstep || homestephome) ) {
	    // (first) real/near homing completed, next send step
	    string stepcmd;
            if( homethenstep )
	      stepcmd = _HomeThenStep[indexor];
	    else if( homestephome )
	      stepcmd = _HomeStepHome[indexor];

            if( stepcmd != "" && stepcmd != "false" ) {
	      _config->_devIO->submit(stepcmd, reply, _flush);
              if( homethenstep ) {
                _HomeThenStep[indexor] = "false"; // reset map
                if( _verbose )
                 clog<<"UFGemPortescapAgent::_execMovement> home then step, sent step: "<<reply<<endl;
              }	
              if( homestephome ) {
                _HomeStepHome[indexor] = "false"; // reset map
                if( _verbose )
                  clog<<"UFGemPortescapAgent::_execMovement> home step home, sent step: "<<reply<<endl;
	      }
              string ss;
              double steps = 0.0;
              if( sppos != string::npos ) { // step forwards
	        ss = stepcmd.substr(1+sppos);
                steps += atof(ss.c_str());
              }
              if( snpos != string::npos ) { // step backwards
	        ss = stepcmd.substr(1+snpos);
                steps -= atof(ss.c_str());
              }
              // put motor into motion list
              _Motion[indexor] = _DesPosition[indexor] += steps;
	    }
	    else {
	      clog<<"UFGemPortescapAgent::_execMovement> home then step, steps empty?"<<endl;
	    }
	  } // athome or at nearhome
	} // finished first homing
	else if( homestephome && !homingF ) { // near or real home followed by stepping part done, now home
	  // put motor into final homing list, if not already there
	  _HomingFinal[indexor] = _DesPosition[indexor] = 0.0;
          string home2 = _HomeHome[indexor];
          if( home2 != "false" && home2 != "" ) {
	    // since home firmware uses initial velocity, insure it is set here
	    size_t hvpos = home2.rfind("F");
	    string hvel = "100";
	    if( hvpos != string::npos ) {
	      hvel = home2.substr(1+hvpos);
	      char* cs = (char*) hvel.c_str();
	      while( *cs == ' ' ) ++cs; // eliminate whites
	      hvel = cs;
	      hvpos = hvel.find(" ");
	      if( hvpos != string::npos )
		hvel = hvel.substr(0, hvpos);
	    }
	    else {
	      clog<<"UFGemPortescapAgent::_execMovement> home2 lacks velocity? "<<home2<<"use "<<hvel<<endl;
	    }
	    string setvel = indexor + 'I' + hvel;
	    //if( _verbose )
	      clog<<"UFGemPortescapAgent::_execMovement> Will NOT set final home/initial velocity: "<<setvel<<endl;
	    //_config->_devIO->submit(setvel, reply, _flush);
	    //if( _verbose )
	      clog<<"UFGemPortescapAgent::_execMovement> final home: "<<home2<<endl;
	    _config->_devIO->submit(home2, reply, _flush);
            _HomeThenStep[indexor] = "false"; // remove indexor from map
            _HomeStepHome[indexor] = "false"; // remove indexor from map
            _HomeHome[indexor] = "false"; // remove indexor from map
	  }
	  else {
	    clog<<"UFGemPortescapAgent::_execMovement> home2 empty?"<<endl;
	  }
        }
	else { // (only/final) home or step done, send OK
          _HomeThenStep[indexor] = "false"; // remove indexor from map
          _HomeStepHome[indexor] = "false"; // remove indexor from map
          _HomeHome[indexor] = "false"; // remove indexor from map
	  // restore initial velocity setting 
	  string setvel = _InitVelCmd[indexor];
          //if( _verbose )
            clog<<"UFGemPortescapAgent::_execMovement> "<<indexor<<" completed, desired: "<<_DesPosition[indexor]<<" current: "<<_CurPosition[indexor]<<endl;
          if( !setvel.empty() ) {
	    if( _verbose )
              clog<<"UFGemPortescapAgent::_execMovement> (re)setvel: "<<setvel<<endl;
	    _config->_devIO->submit(setvel, reply, _flush);
          }
          if(_indexorCAR[indexor] != "" && _carRec != _indexorCAR[indexor] ) {
            if( _statRec != "" )
	      sendEpics(_statRec, _currentval);
	    //sendEpics(_statRec, "OK"); 
            //if( _verbose )
	      clog<<"UFGemPortescapAgent::_execMovement> "<<_carRec<<" Idle."<<endl;
	    setCARIdle(_indexorCAR[indexor]); _setIdleOrErr[indexor] = true;
	  }
        }
      } // mstat == 0
      else if( pdiff <= 0.1 ) { // msta != 0, but not moving either!
	// mechanism is stuck?
        // send Error Message, abort homing and remove from homing list?
	errmsg += " Stuck?"; ++errcnt;
 	if( _carRec != _indexorCAR[indexor] && _indexorCAR[indexor]!= "" ) {
	  //sendEpics(_statRec, errmsg);
	  clog<<"UFGemPortescapAgent::_execMovement> "<<_carRec<<" Error: "<<errmsg<<endl;
	  setCARError(_indexorCAR[indexor], &errmsg); _setIdleOrErr[indexor] = true;
	}
	erase.push_back(indexor);
        if( homethenstep ) // remove from HomeThenStep
 	  _HomeThenStep[indexor] = "false";
        if( homestephome ) { // remove from HomeStepHome
 	  _HomeStepHome[indexor] = "false";
 	  _HomeHome[indexor] = "false";
	}
        clog<<"UFGemPortescapAgent::_execMovement> mechanism stuck?: "<<indexor<<endl;
      } // pdiff
    } // check for completion
    /*
    if( mstat == 0 || pdiff <= 0.1 ) { 
      if( !homethenstep && !homestephome && !nearhoming ) // step only
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else if( athome && !homethenstep && !homestephome ) // home only
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else if( athome && homingF ) // done homestephome
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else ( !athome && homethenstep ) // done homestep
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      if( _verbose )
        clog<<"UFGemPortescapAgent::_execMovement> "<<_statRec<<" == "<<_currentval<<endl;
    }
    */
  } while( ++i != moving.end() );

  // remove all motion completed indexors from given map (homing or homingFinal or motion)
  // this must be down outside of the loop, otherwise the stlmap gets confused.
  for( int i = 0; i < (int)erase.size(); ++i ) {
    //if( _verbose )
      clog<<"_execMovement> remove from motion list: "<<erase[i]<<endl;
    moving.erase(erase[i]); 
  }

  if( moving.empty() && !_epics.empty() && _epics != "false") // all done, if this was a datum, report to sad
    datumCompleted(errcnt, errmsg);
    
  return;
} // _execMovement

// simuated motion (not very realistic) 
void UFGemPortescapAgent::_simMovement(map<string, double>& moving) {
  if( moving.empty() )
    return;

  vector< string > erase;
  string errmsg, reply, qmotion = " ^ ", qsteps = " Z 0 "; // steps from home/origin
  map<string, double>::iterator i = moving.begin();
  int errcnt= 0;
  do { 
    string indexor = i->first;
    if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor
	  <<", current: "<<_CurPosition[indexor]<<", desired: "<<_DesPosition[indexor]<<endl;

    // this function is used for all simulated motion (stepping or seeking or homing,
    // or home-then-step, or home-step-home)
    bool stepping = !_Motion.empty();
    if( stepping ) { // is this indexor stepping?
      map<string, double>::iterator fnd = _Motion.find(indexor);
      if( fnd == _Motion.end() )
	stepping = false;
    }
    bool seeking = !_LimitSeeking.empty();
    if( seeking ) { // is this indexor seeking limits?
      map<string, double>::iterator fnd = _LimitSeeking.find(indexor);
      if( fnd == _LimitSeeking.end() )
	seeking = false;
    }
    bool nearhoming = !_NearHoming.empty(); // (diff < 0.1);
    if( nearhoming ) { // is this indexor nearhoming?
      map<string, double>::iterator fnd = _NearHoming.find(indexor);
      if( fnd == _NearHoming.end() )
	nearhoming = false;
    }
    // it is possible to (initially) find indexor in _Homing and _HomingFinal
    // so we need to be carefull what is tested here... 
    bool homing = !_Homing.empty(); // (des > -0.1 && des < 0.1);
    bool homingF = !_HomingFinal.empty(); // (des > -0.1 && des < 0.1);
    if( homing ) { // check this indexor homing?
      map<string, double>::iterator fnd = _Homing.find(indexor);
      if( fnd == _Homing.end() ) // not this indexor
	homing = false;
      else
	homingF = false; // force this to false
    }
    if( homingF ) { // check this indexor homingFinal?
      map<string, double>::iterator fnd = _HomingFinal.find(indexor);
      if( fnd == _HomingFinal.end() ) // not this indexor
	homingF = false;
    }
    //if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor
	  <<", stepping: "<<stepping<<", seeking: "<<seeking
	  <<", homing: "<<homing<<", homingFinal: "<<homingF<<endl;

    _statRec = _Status[indexor];
    errmsg = _ErrPosition[indexor];

    // move it part way between current position and desired:
    double val= 0.0;
    double steps = 0.1 + ::fabs(_DesPosition[indexor] - _CurPosition[indexor]) / _simSpeed;
    if( homingF ) // go slower
      steps = 0.5 * steps;

    if( _CurPosition[indexor] - _DesPosition[indexor] > 0 ) 
      val = _CurPosition[indexor] - steps;
    else
      val = _CurPosition[indexor] + steps;

    //if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor
	  <<", steps: "<<steps<<", new position: "<<val<<endl;

    strstream s;
    s << indexor << qsteps << val << ends;
    reply = s.str(); delete s.str(); // simulate indexor position query/reading reply
    if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> steps reading: "<<reply<<endl;

    int pos = qsteps.length() + reply.find(qsteps);
    _currentval = reply.substr(pos);
    steps = atof(_currentval.c_str());
    double prev_steps = _CurPosition[indexor];
    double pdiff = ::fabs(steps - prev_steps);
    if( pdiff > 0.1 ) // reset current position   
      _CurPosition[indexor] = steps;

    if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> "<<_currentval<<", _statRec: "<<_statRec<<endl;

    // send current value to epics
    if( !_epics.empty() && _epics != "false" ) {
      string gsad = _guessSADchan(indexor);
      _statRec = _Status[indexor];
      if( _statRec.empty() ) {
        clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor<<" has No client supplied sad, using: "<<_statRec<<endl;
	_statRec = _Status[indexor] = gsad;
      }
      sendEpics(_statRec, _currentval);
      if( _statRec != gsad ) {
        clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor<<" client supplied alternate: "<<_statRec<<endl;
        clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor<<" also send status to sad: "<<gsad<<endl;
        sendEpics(gsad, _currentval);
      }
      //if( _verbose )
        clog<<"UFGemPortescapAgent::_simMovement>> "<<_statRec<<" == "<<_currentval<<endl;
    }
    else {
      clog<<"UFGemPortescapAgent::_simMovement>> No epics? "<<_epics<<endl;
    }

    bool athome = false;
    if( ::fabs(_CurPosition[indexor]-_DesPosition[indexor]) <= 0.9 ) { // must be home? 
      //_CurPosition[indexor] = _DesPosition[indexor];
      athome = true;
      // set the datum cnt for this indexor:
      int datumcnt = UFPortescapConfig::_DatumCnt[indexor];
      if( datumcnt < 0 )
	datumcnt = 1;
      else
	++datumcnt;
      UFPortescapConfig::_DatumCnt[indexor] = datumcnt;
      if( _verbose )
        clog<<"UFGemPortescapAgent::_simMovement> athome "<<indexor<<" datumcnt == "<<datumcnt<<endl;
    }

    double des = _DesPosition[indexor];
    double diff = ::fabs(steps - des);
    // since this function can be used for either motion/stepping or nearhoming
    // there can be some ambiguity here...
    size_t snpos = _HomeThenStep[indexor].find("-");
    size_t sppos = _HomeThenStep[indexor].find("+");
    bool homethenstep = false, homestephome = false;
    string stepcmd = "";
    if( snpos != string::npos || sppos != string::npos ) {
      homethenstep = true;
      stepcmd = _HomeThenStep[indexor];
      clog<<"UFGemPortescapAgent::_simMovement> "<<indexor<<", stepcmd: "<<stepcmd<<endl;
    }
    else {
      snpos = _HomeStepHome[indexor].find("-");
      sppos = _HomeStepHome[indexor].find("+");
      if( snpos != string::npos || sppos != string::npos ) {
        homestephome = true;
        stepcmd = _HomeStepHome[indexor];
        if( _verbose )
          clog<<"UFGemPortescapAgent::_simMovement> "<<indexor<<", stepcmd: "<<stepcmd<<endl;
      }
    }

    // once current motion is complete, should it be followed by more motion?
    if( (diff <= 0.1 || pdiff <= 0.1) ) {
      // motion is complete, should it be followed by stepping or homing?
      val = _CurPosition[indexor] = _DesPosition[indexor];
      erase.push_back(indexor);
      strstream s;
      s << indexor << qsteps << val << ends;
      reply = s.str(); delete s.str(); // simulate final reading
      int pos = qsteps.length() + reply.find(qsteps);
      _currentval = reply.substr(pos);
      if( (homing || nearhoming) && (homethenstep || homestephome) && stepcmd.length() > 1 ) { 
        // step for HomeThenStep or HomeStepHome
	string ss; 
        double steps = 0.0;
        if( sppos != string::npos ) { // step forwards
	  ss = stepcmd.substr(1+sppos);
          steps += atof(ss.c_str());
        }
        if( snpos != string::npos ) { // setp backwards
	  ss = stepcmd.substr(1+snpos);
          steps -= atof(ss.c_str());
        }
	// put motor into motion list
        if( _verbose )
	  clog<<"UFGemPortescapAgent::_simMovement> "<<indexor<<", stepcmd: "<<stepcmd<<", steps= "<<steps<<endl;
        _Motion[indexor] = _DesPosition[indexor] += steps;
      }
      else if( stepping && homestephome  ) { // put motor into final homing list, if not already there
        _HomingFinal[indexor] = _DesPosition[indexor] = 0.0;
      }
      else { // (final or only) motion done
        if( _verbose )
          clog<<"UFGemPortescapAgent::_simMovement> motion completed, final reading: "<<_currentval
	      <<", _statRec: "<<_statRec<<endl;
        _HomeThenStep[indexor] = "false"; // remove indexor from homethestep
        _HomeStepHome[indexor] = "false"; // remove indexor from homestephome
        if( !_statRec.empty() ) {
	  clog<<"UFGemPortescapAgent::_simMovement> "<<_statRec<<" == "<<_currentval<<endl;
	  sendEpics(_statRec, _currentval); //sendEpics(_statRec, "OK"); 
        }
        if(_indexorCAR[indexor] != "" && _carRec != _indexorCAR[indexor] ) {
	  //sendEpics(_statRec, "OK"); 
	  clog<<"UFGemPortescapAgent::_simMovement> "<<_carRec<<" Idle."<<endl;
	  setCARIdle(_indexorCAR[indexor]); _setIdleOrErr[indexor] = true;
	}
      }
    }
  } while( ++i != moving.end() );

  // remove all motion completed indexors from given map (motion, homing, or homingF)
  for( int i = 0; i < (int)erase.size(); ++i ) {
    //if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> remove from motion list: "<<erase[i]<<endl;
    moving.erase(erase[i]);
  }

  if( moving.empty() && !_epics.empty() && _epics != "false") // all done, if this was a datum, report to sad
    datumCompleted(errcnt, errmsg);

  return;
} // _simMovement

void UFGemPortescapAgent::hibernate() {
  //clog<<"UFGemPortescapAgent::hibernate> ..."<<endl; 
  if( _Motion.size() > 0 || _LimitSeeking.size() > 0 || 
      _Homing.size() > 0 || _HomingFinal.size() > 0 || _NearHoming.size() > 0 ) {
    //clog<<"UFGemPortescapAgent::hibernate> something is moving, check status..."<<endl; 
    return; // don't hibernate, must monitor motion as best as can
  }
  /*
  if( _immed.size() == 0 && _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0 ) {
    //clog<<"UFGemPortescapAgent::hibernate> no connections, no immed. or queued reqs., go to sleep..."<<endl; 
    //mlsleep(_Update); // no connections, no queued reqs., go to sleep
    UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
  }
  else {
    //clog<<"UFGemPortescapAgent::hibernate> no connections or immed. requests, go to sleep..."<<endl; 
    //mlsleep(_Update); // no queued reqs., go to sleep
    UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
  }
  */
  UFRndRobinServ::hibernate();
}

int UFGemPortescapAgent::_reqHomeThenMore(UFDeviceAgent::CmdInfo* act) {
  map<string, int> home, nearhome;
  map<string, int> step;
  map<string, string> stepcmd;
  map<string, string> home2cmd;
  string cmdname, cmdimpl, motor, car, sad;
  for( int i = 0; i < (int) UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    home[motor] = nearhome[motor] = step[motor] = 0;
    stepcmd[motor] = home2cmd[motor] = "";
  }

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    cmdname = act->cmd_name[i];
    cmdimpl = act->cmd_impl[i];
     if( _verbose ) 
	clog<<"_reqHomeThenMore> "<<cmdname<<", "<<cmdimpl<<endl;

    // first look for SAD indication, need to know motor indexor associated with sad sir channel name...
    int spos = cmdname.find("sad");
    if( spos == (int)string::npos ) 
      spos = cmdname.find("Sad");
    if( spos == (int)string::npos ) 
      spos = cmdname.find("SAD");
    if( spos != (int)string::npos ) {
      sad = cmdimpl;
      continue;
    }

    // next look for CAR indication
    int cpos = cmdname.find("car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("Car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("CAR");

    motor = ""; // which motor is this?
    if( cpos != (int)string::npos ) {
      car = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0 of Car?
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
	char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
	motor = indexor;
      }
      continue;
    }
    if( motor == "" ) { // otherwise try first char of cmdimpl:
      char* c = (char*)cmdimpl.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
      char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
      motor = indexor;
    }
    if( cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0 ) { // home req.
      int hcnt = 1 + home[motor];
      home[motor] = hcnt; //
      if( hcnt > 1 ) home2cmd[motor] = cmdimpl;
    }
    else if( cmdimpl.rfind("N") != string::npos && cmdimpl.rfind("N") != 0 ) { // near-home req.
      nearhome[motor] = 1; // any value > 0 will do
      string nhs = cmdimpl.substr(1+cmdimpl.rfind("N"));
      double nh = atof(nhs.c_str());
      if( _verbose )
	clog<<"NearHome req. to: "<<nh<<" ("<<cmdimpl<<")"<<endl;
      if( nh < 1.0 && nh >= 0.0 )
	nh = 1.0;
      if( nh > -1.0 && nh <= 0.0 )
	nh = -1.0;
      double diff = ::fabs(_NearHome - nh);
      if( diff > 0.1 )
	_NearHome = nh;
      if( _verbose )
	clog<<"_NearHome set to: "<<_NearHome<<endl;
    }
    else if( cmdimpl.find("+") != string::npos || cmdimpl.find("-") != string::npos ) { // step req
      step[motor] = 1; // any value > 0 will do
      stepcmd[motor] = cmdimpl;
    }
  } 

  int cnt= 0;
  for( int i = 0; i < (int) UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    if( (home[motor] > 0 || nearhome[motor] > 0 ) && step[motor] > 0 ) {
      ++cnt; 
      if( home[motor] == 1 || nearhome[motor] > 0 ) { // home-step for real or virtual home then step
        _HomeThenStep[motor] = stepcmd[motor]; // steps raw cmd
        if( _verbose )
          clog<<"_reqHomeThenSMore> Motor: "<<motor<<" => "<<_HomeThenStep[motor]<<endl;
      }
      else if( home[motor] > 1 ) { // home-step-home only for real home
        if( stepcmd[motor] != "" )
	  _HomeStepHome[motor] = stepcmd[motor]; // steps raw cmd
        if( home2cmd[motor] != "" )
	  _HomeHome[motor] = home2cmd[motor]; // steps raw cmd
        if( _verbose )
	  clog<<"_reqHomeThenMore> (then home finally) Motor: "<<motor<<" => "<<_HomeStepHome[motor]<<", "<<_HomeHome[motor]<<endl;
      }
    }
  }
  return cnt;
} // _reqHomeThenMore

// the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// to portescap indexor.
// this version must always return null to (epics) clients, and
// instead send the command completion status to designated CARs,
// if no CAR is desginated, an error /alarm condition should be indicated
// new signature for action:
int  UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  delete repv; repv = 0;
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_client= true;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ||
      act->clientinfo.find("flam:") != string::npos ||
      act->clientinfo.find("fu:") != string::npos ||
      act->clientinfo.find("foo:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    if( _verbose )
      clog<<"UFGemPortescapAgent::action> No replies will be sent to Gemini/Epics client: "
          <<act->clientinfo<<endl;
  }
 // if( _verbose ) {
    size_t cnt = act->cmd_name.size();
    clog<<"UFGemPortescapAgent::action> cnt: "<<cnt<<", client: "<<act->clientinfo<<", time: "<<act->cmd_time<<endl;
    //for( size_t i = 0; i<cnt; ++i ) 
      //clog<<"UFGemPortescapAgent::action> elem: "<<i<<" "<<act->cmd_name[i]<<" ; "<<act->cmd_impl[i]<<endl;
  //}

  // check the entire bundle for a home-then-step, or home-step-home sequence, and make a
  // note of which motors, if any, were specified
  int nhm = _reqHomeThenMore(act);

  // epics will send a CmdInfo bundle for just one motor indexor at a time...
  // this bit of logic will fail if bundle contains commands for more than one indexor! 
  string motor, car, sad, errmsg;
  bool motion= false, seeklimit= false, homing= false, homingF= false, nearhoming= false;

  map< string, bool > submittedhome; // if this bundle has compound home requests for more than one index 
  for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    submittedhome[motor] = false;
  }

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    motor = "";
    bool raw= false, sim = _sim; // command-line option can force all actions to be simulated
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    //if( _verbose ) {
      clog<<"UFGemPortescapAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemPortescapAgent::action> cmdimpl: "<<cmdimpl<<endl;
    //}

    if( cmdname.find("raw") != string::npos || cmdname.find("Raw") != string::npos ||
        cmdname.find("RAW") != string::npos ) {
	clog<<"UFGemPortescapAgent::action> setting raw command mode..."<<endl;
       raw = true;
    }

    // in principle this could be a one-time init from the cc:configG
    // however, support for multiple epics DBs is possible by simply
    // resetting the CAR _Status name for each request...
    // also note that the one character indexor name is expected
    // at position index 0 of the CAR cmd directive!

    int cpos = cmdname.find("car");
    if( cpos == (int)string::npos ) cpos = cmdname.find("Car");
    if( cpos == (int)string::npos ) cpos = cmdname.find("CAR");
    if( cpos != (int)string::npos ) {
      // only epics clients send car keyword, no socket reply is expected by epics
      reply_client= false;
      // note that _execMoition and _simMotion may send OK and set this to true...
      car = _carRec = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
	char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
	motor = indexor;
      }
      if( motor == "" ) { // try first char of cmdimpl:
        char* c = (char*)cmdimpl.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
        char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
        motor = indexor;
      }
      //if( _verbose )
	clog<<"UFGemPortescapAgent::action> motor indexor: "<<motor<<", CAR: "<<car<<endl;
      continue;
    }
    // next look for SAD indication
    int spos = cmdname.find("sad");
    if( spos == (int)string::npos ) spos = cmdname.find("Sad");
    if( spos == (int)string::npos ) spos = cmdname.find("SAD");
    if( spos != (int)string::npos ) {
      reply_client = false; // don't reply to epics socket, use sad channel
      sad = cmdimpl;  
     //if( _verbose )
	clog<<"UFGemPortescapAgent::action> SAD: "<<sad<<endl;
      continue;
    }

    int errIdx = cmdname.find("!!");
    if( errIdx != (int)string::npos ) 
      errmsg = cmdname.substr(2+errIdx);

    // if this is a fits or such type status text requext from non-epics client:   
    if( !raw && reply_client && 
	(cmdname.find("stat") != string::npos || 
	 cmdname.find("Stat") != string::npos || cmdname.rfind("STAT") != string::npos)  ) {
      clog<<"UFGemPortescapAgent::action> Status text/FITS request..."<<endl;
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  )
        return _config->statusFITS(this); 
      else
        return _config->status(this); 
    }

    // now check for raw/real or sim commands
    int rpos = cmdname.find("raw");
    if( rpos == (int)string::npos ) rpos = cmdname.find("Raw");
    if( rpos == (int)string::npos ) rpos = cmdname.find("RAW");
    int simpos = cmdname.find("sim");
    if( simpos == (int)string::npos ) simpos = cmdname.find("Sim");
    if( simpos == (int)string::npos ) simpos = cmdname.find("SIM");
    if( rpos == (int)string::npos && simpos == (int)string::npos &&
        spos == (int)string::npos && cpos == (int)string::npos ) {
      // quit
      errmsg += "Badly formatted request? " + cmdname + " " + cmdimpl;
      clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
      if( car != "" ) { setCARError(car, &errmsg); _setIdleOrErr[motor] = true; } // set error
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }
    else if( motor == "" && rpos != (int)string::npos ) { // one last effort to id motor indexor
      char* c = (char*)cmdimpl.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
      char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
      motor = indexor;
      //if( _verbose )
	clog<<"UFGemPortescapAgent::action> motor indexor: "<<motor<<", CAR: "<<car<<endl;
    }

    // if we have an indexor/motor ID now:
    if( motor != "" ) {
      if( car != "" )
        _indexorCAR[motor] = car;
       if( sad != "" )
	 _Status[motor] = sad; // assume client has indicated correct sad channel for motor indexor!
    }
    /*
    else if( !reply_client && _epics != "" && _epics != "false" ) { // should attempt to guess sad ?
      _Status[motor] = _guessSADchan(motor);
      clog<<"UFGemPortescapAgent::action> guess SAD for indexor: "<<motor<<" => "<<_Status[motor]<<endl;
    }
    */
    int nr = _config->validCmd(cmdimpl);
    if( nr < 0 ) { // invalid cmd
      errmsg += "?Invalid Portescap req? " + cmdname + " " + cmdimpl;
      clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
      if( car != "" ) { setCARError(car, &errmsg); _setIdleOrErr[motor] = true; } // set error
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }
 
    act->time_submitted = currentTime();
    _active = act;
    size_t sppos = cmdimpl.find("+");
    size_t snpos = cmdimpl.find("-");
    double steps = 0.0;

    if( cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0 ) {
      // this is to set initial velocity, make a note of it
      _InitVelCmd[motor] = cmdimpl;
    }
 
    // this rejection logic implicitly assumes this bundle is devoted to just 1 indexor...
    if( cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0 ) {
      snpos = sppos = string::npos;
      // this is a home req, which is OK if currently homing,
      // but not OK if stepping (+/-) or M seeking limit
      map<string, double>::iterator s = _Motion.find(motor);
      if( s != _Motion.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject home req., still stepping: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator ls = _LimitSeeking.find(motor);
      if( ls != _LimitSeeking.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject home req., still limit seeking: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      _DesPosition[motor] = 0.0;
/*
      double hdif = ::fabs(_CurPosition[motor] - _DesPosition[motor]);
      // if this motor has been datumed or origined at least once and it is currently
      // in the datum position, treat this as an OK no-op, else reject it...
      if( hdif < 0.1 && UFPortescapConfig::_DatumCnt[motor] != 0 ) {
        clog<<"UFGemPortescapAgent::action> "<<motor<<" is at datum, assume No-Op..."<<endl;
        act->cmd_reply.push_back("No-Op Home");
        if( car != "" ) sendEpics(car, "OK");
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
*/	
      // this logic assumes the compound motion cmd bundles are only sent for single motors...
      if( !homing ) {
        homing = true;
        _Homing.insert(map<string, double>::value_type(motor, 0.0));
      }
      else if( !homingF) { // _HomingFinal must be set later (by _exec or _sim Movement?
        homingF = true;
        //_HomingFinal.insert(map<string, double>::value_type(motor, 0.0));
      }
      clog<<"UFGemPortescapAgent::action> motor/indexor: "<<motor<<" Findhome: "<<_DesPosition[motor]<<endl;
    }
    if( cmdimpl.rfind("N") != string::npos && cmdimpl.rfind("N") != 0 ) {
      snpos = sppos = string::npos;
     // this is a near-home req, which is OK if currently near-homing? but not OK if stepping
      map<string, double>::iterator s = _Motion.find(motor);
      if( s != _Motion.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject near-home req. motor still stepping: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator ls = _LimitSeeking.find(motor);
      if( ls != _LimitSeeking.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject near-home req., still limit seeking: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      _NearHoming.insert(map<string, double>::value_type(motor, _NearHome));
      _DesPosition[motor] = _NearHome;
      nearhoming = true;
      clog<<"UFGemPortescapAgent::action> motor/indexor: "<<motor<<" Nearhome: "<<_DesPosition[motor]<<endl;
    }
    if( cmdimpl.rfind("M") != string::npos && cmdimpl.rfind("M") != 0 ) {
      // this is a Move req. (presumabley seek limit), which is not OK if stepping or homing
      map<string, double>::iterator s = _Motion.find(motor);
      if( s != _Motion.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject seek lim. req. motor still stepping: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator h = _Homing.find(motor);
      if( h != _Homing.end() ) { // motor is already/still homing, reject this
        errmsg += " Reject move/seek lim. req. motor still homing: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      seeklimit = true;
      if( snpos != string::npos ) { // "-" limit
        steps = _CurPosition[motor] + UFPortescapConfig::_MinLim;
        _LimitSeeking[motor] = UFPortescapConfig::_leftLim = steps;
        _DesPosition[motor] = UFPortescapConfig::_MinLim;
      }
      else { // "+" limit
        steps = _CurPosition[motor] + UFPortescapConfig::_MaxLim;
        _LimitSeeking[motor] = UFPortescapConfig::_rightLim = steps;
        _DesPosition[motor] = UFPortescapConfig::_MaxLim;
      }
      clog<<"UFGemPortescapAgent::action> motor/indexor: "<<motor<<" Seeklimit: "<<_DesPosition[motor]<<endl;
    }

    string reply= cmdimpl;
    if( _LimitSeeking.find(motor) != _LimitSeeking.end() )
      clog<<"UFGemPortescapAgent::action> _LimitSeeking for "<<motor<<" = "<<_LimitSeeking[motor]<<endl;
    if( _HomeThenStep.find(motor) != _HomeThenStep.end() )
      clog<<"UFGemPortescapAgent::action> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
    if( _HomeStepHome.find(motor) != _HomeStepHome.end() )
      clog<<"UFGemPortescapAgent::action> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
    if( _HomeHome.find(motor) != _HomeHome.end() )
      clog<<"UFGemPortescapAgent::action> _HomeHome for "<<motor<<" = "<<_HomeHome[motor]<<endl;

    bool homethenstep = false, homestephome = false;
    if( nhm > 0 ) {
      homethenstep = (_HomeThenStep[motor] != "false");
      homestephome = (_HomeStepHome[motor] != "false");
    }
    if( (homethenstep || homestephome) && (sppos != string::npos || snpos!= string::npos) ) {
      // step request following the home request is handled by the ancillary function
      //reply += " Ok";
      _setIdleOrErr[motor] = false;  // epics clients need "OK" sent to CAR upon succesfull command completion
      act->cmd_reply.push_back(reply);
      continue;
    }
    if( !seeklimit && !homethenstep && !homestephome && (sppos != string::npos || snpos!= string::npos) ) {
    //if( !homethenstep && !homestephome || (sppos != string::npos || snpos!= string::npos) ) {
      // this is a step only or home only req. bundle?
      map<string, double>::iterator s = _Motion.find(motor);
      if( s != _Motion.end() ) { // motor is already/still in (stepping) motion, reject this
        errmsg += " Reject step req. motor in motion: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator h = _Homing.find(motor);
      if( h != _Homing.end() ) { // motor is already/still in (homing) motion, reject this
        errmsg += " Reject step req. motor homing: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator nh = _NearHoming.find(motor);
      if( nh != _NearHoming.end() ) { // motor is already/still in (homing) motion, reject this
        errmsg += " Reject step req. motor near-homing: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      string ss;
      if( sppos != string::npos ) { // step forwards
	ss = cmdimpl.substr(1+sppos);
        steps += atof(ss.c_str());
      }
      if( snpos != string::npos ) { // step backwards
	ss = cmdimpl.substr(1+snpos);
        steps -= atof(ss.c_str());
      }
      // put motor into motion list and clear its Ok message
      // if this is an M (seek limits) command, motion has already been set...
      if(steps > 0.1 || steps < -0.1 ) { // valid step cmd
        motion = true;
        _Motion.insert(map<string, double>::value_type(motor, steps));
        _DesPosition[motor] = _DesPosition[motor] + steps;
        _setIdleOrErr[motor] = false;  // epics clients need to set CAR Idle upon succesfull command completion
        clog<<"UFGemPortescapAgent::action> step indexor: "<<motor<<endl;
      }
    }
    // finally, no motion parameter (re)set should be submitted ????
    // at this point or the portescap rs422 bus will become locked (busy).
    // the only possible exception to this would be a home request when we
    // are already busy homing, in which case we could treat it as a no-op.
    // this really should be a function in the Config class:
    /*
    bool busbusy = ( motion || homing || nearhoming || homethenstep || homestephome );
    if( busbusy ) {
      if( (cmdimpl.rfind("B") != string::npos && cmdimpl.rfind("B") != 0) ||
	  (cmdimpl.rfind("E") != string::npos && cmdimpl.rfind("E") != 0) ||
          (cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0) ||
	  (cmdimpl.rfind("K") != string::npos && cmdimpl.rfind("K") != 0) ||
	  (cmdimpl.rfind("V") != string::npos && cmdimpl.rfind("V") != 0) ||
	  (cmdimpl.rfind("Y") != string::npos && cmdimpl.rfind("Y") != 0) )
	// assume this cmd bundle is bogus and reject thw whole thing
        errmsg += "RS422 Bus busy (Portescap), reject: " + cmdname + " " + cmdimpl;
        if( _verbose )
	  clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        //if( car != "" ) setCARError(car, &errmsg); // set error
        if( car != "" ) sendEpics(car, errmsg);
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }
    */

    map<string, string>::iterator ep = _ErrPosition.find(motor);
    if( ep != _ErrPosition.end() ) // replace errmsg
      _ErrPosition.erase(ep);
    _ErrPosition.insert(map<string, string>::value_type(motor, errmsg));

    map<string, double>::iterator mm = _Motion.find(motor);
    map<string, double>::iterator hm = _Homing.find(motor);
    map<string, double>::iterator hmF = _HomingFinal.find(motor);
    map<string, double>::iterator nrhm = _NearHoming.find(motor);
    map<string, double>::iterator ls = _LimitSeeking.find(motor);

    if( simpos != (int)string::npos || _sim ) { // sim request
      sim = true;
      bool abort = false, origin= false;
      reply = "sim: " + cmdimpl;
      clog<<"UFGemPortescapAgent::action> simulation: "<<reply<<endl;
      // simulate position & motion queries:
      strstream s;
      if( cmdimpl.find("Z") != string::npos ) {
	// return current position
	s << cmdimpl << " " << _CurPosition[motor] <<ends;
	reply = s.str(); delete s.str();
        strstream sp;
        sp<<_CurPosition[motor]<<ends; 
 	clog<<"UFGemPortescapAgent::action> indexor reply: "<<reply<<", sad: "<<sad<<", car: "<<car<<endl;     
        if( car != "" ) { setCARIdle(car); _setIdleOrErr[motor] = true; }
        //sendEpics(car, sp.str()); sendEpics(car,"OK");
        if( sad != "" ) sendEpics(sad, sp.str());
	delete sp.str();
      }
      else if (cmdimpl.find("X") != string::npos) {
	s << cmdimpl << " " << "XX Y= " << "25/25" << " E= " << "50" << " K= " << "255/255" << " H=vr"<< " na="<< motor << "\r\n" 
	  << "I= " << "100 ( 0 /0)" << " V= " << "600 ( 0 /0) (rl= 0 )" << ends;
	reply = s.str(); delete s.str();
	_parseXCmdReply(motor,reply.substr(cmdimpl.length()));
      }
      else if( cmdimpl.find("^") != string::npos ) {
	// return 0 if stationary, otherwise current position
        if( mm != _Motion.end() || nrhm != _NearHoming.end() ||
	    hm != _Homing.end() || hmF != _HomingFinal.end() || ls != _LimitSeeking.end() )
	  // motor is already/still in motion
	  s << cmdimpl << " " << _CurPosition[motor] <<ends;
	else
	  s << cmdimpl << " " << 0 <<ends;
	reply = s.str(); delete s.str();
      }
      else if( ( cmdimpl.find("O") != string::npos && cmdimpl.find("O") > 0) ||
	       (cmdimpl.find("o") != string::npos && cmdimpl.find("o") > 0) ) {
	// clear the step counts (origin)
        origin = true;
        if( mm != _Motion.end() || nrhm != _NearHoming.end() ||
	    hm != _Homing.end() || hmF != _HomingFinal.end() )
	  // motor is still in motion
	  s << cmdimpl << " " << _CurPosition[motor] <<ends;
	else {
          _CurPosition[motor] = 0; _currentval = "0.0";
	  s << cmdimpl << " " << 0 <<ends;
        }
	reply = s.str(); delete s.str();
	// and return current position
        strstream sp;
        sp<<_CurPosition[motor]<<ends; 
        if( car != "" ) sendEpics(car, sp.str()); delete sp.str();
      }
      else if( cmdimpl.find("@") != string::npos ) { // stop/abort command:
        abort = true;
	s << cmdimpl << " (abort) " << _CurPosition[motor] <<ends;
        // clear HomeThenStep
 	_HomeThenStep[motor] = "false";
        // clear HomeStepHome
 	_HomeStepHome[motor] = "false";
 	_HomeHome[motor] = "false";
        if( !_Motion.empty() && mm != _Motion.end() ) {// motor is still in motion
	  _Motion.erase(mm); // remove it from motion list
	}
        if( !_Homing.empty() && hm != _Homing.end() ) {// motor is still in motion
	  _Homing.erase(hm); // remove it from homing list
	}
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) {// motor is still in motion
	  _HomingFinal.erase(hmF); // remove it from homing list
	}
        if( !_NearHoming.empty() && nrhm != _NearHoming.end() ) {// motor is still in motion
	  _NearHoming.erase(nrhm); // remove it from homing list
	}
        if( !_LimitSeeking.empty() && ls != _LimitSeeking.end() ) {// motor is still in motion
	  _LimitSeeking.erase(ls); // remove it from homing list
	}
	reply = s.str(); delete s.str();
      }
      //if( _verbose )
	clog<<"UFGemPortescapAgent::action> Sim: "<<reply<<endl;
    } // end sim request

    // if not sim. use the annex/iocomm/perl port to submit the action command and get reply:
    // (should be calling Config functions here)
    // if a command bundle includes a home followed by a step for a given
    // motor, only perform the home. must defer submitting the step req.
    // until home completes
    else if( _config->_devIO != 0 ) {
      bool abort= false, aborthome= false, abortlimit= false, origin= false;
      if( cmdimpl.find("@") != string::npos ) {
	abort = true;
        if( !_Homing.empty() && hm != _Homing.end() ) aborthome = true;
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) aborthome = true;
        //if( !_LimitSeeking.empty() && ls != _LimitSeeking.end() ) abortlimit = true;
      }
      else if( ( cmdimpl.find("O") != string::npos && cmdimpl.find("O") > 0) ||
	       (cmdimpl.find("o") != string::npos && cmdimpl.find("o") > 0) ) {
	// clear the step counts (origin)
        origin = true;
	//if( _verbose )
	clog<<"UFGemPortescapAgent::action> Origin indexor/motor: "<<motor<<endl;
      }
      // if cmdimpl is a near-home replace pseudo command with the appropriate +/- request
      bool nearH = _nearHome(motor, cmdimpl);
      bool realH = (cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0);
      bool submittedH = submittedhome[motor];
      if( nearH ) 
        clog<<"UFGemPortescapAgent::action> nearH: "<<nearH<<", submittedHome for "<<motor<<": "<<submittedH<<endl;
      if( submittedH && (nearH || (cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0) ) ) {
	if( _verbose )
	  clog<<"UFGemPortescapAgent::action> already submitted home or near/not-home: "<<motor << endl;
	continue; // already submitted (first) home req. for this motor
      }
      if( aborthome || abortlimit ) { // use escape
        //if( _verbose )
	  clog<<"UFGemPortescapAgent::action> aborthome/limit, no device reply expected for escape."<<endl;
        stat = _config->_devIO->escape(motor, 0.3); 
        if( _verbose )
	  clog<<"UFGemPortescapAgent::action> aborthome/limit, stat: "<<stat<<endl;
	reply = cmdimpl;
      }
      else { // reply expected
	if( realH ) { // reset initial velocity according to requested home speed to make them congruent (while homing)
	  // since home firmware uses initial velocity, insure it is set here
	  size_t hvpos = cmdimpl.rfind("F");
	  string hvel = "100";
	  if( hvpos != string::npos ) {
	    hvel = cmdimpl.substr(1+hvpos);
	    char* cs = (char*) hvel.c_str();
	    while( *cs == ' ' ) ++cs; // eliminate whites
	    hvel = cs;
	    hvpos = hvel.find(" ");
	    if( hvpos != string::npos )
	      hvel = hvel.substr(0, hvpos);
	  }
	  else {
	    clog<<"UFGemPortescapAgent::action> home lacks velocity? "<<cmdimpl<<"use "<<hvel<<endl;
	  }
	  string setvel = motor + 'I' + hvel;
	  //if( _verbose )
	    clog<<"UFGemPortescapAgent::action> Will NOTset home/initial velocity: "<<setvel<<endl;
	  //_config->_devIO->submit(setvel, reply, _flush);
	} // realH homing resets init. vel.
 
        // if this motor has never been datumed or origined and/or the indexor motion params
        // have not yet been (re)set, its current position and/or vel, runc, holdc, etc. config.
	// may be ambiguous, so we perform an inital config. and optionally step in the opposite direction
        // of the first home command to deal with the pathelogical behavior of
        // of some indexors when asked to home when already at home?
        bool sendcmd = UFPortescapConfig::newSettingCmd(motor, cmdimpl);
        if( sendcmd ) { // send command to indexor and get reply
	  /*
          string jogcmd;
          double curpos = _CurPosition[motor];
          UFPortescapConfig::jogMotor(motor, cmdimpl, curpos, jogcmd);
          if( !jogcmd.empty() ) { // first jog the indexor
            stat = _config->_devIO->submit(jogcmd, reply, _flush); 
            if( _verbose )
	      clog<<"UFGemPortescapAgent::action> device reply: "<<reply<<endl;
	  }
	  */
	  int numLines = UFPortescapConfig::numReplyLinesExpected(cmdimpl);
	  if (numLines > 1)
	    stat = _config->_devIO->submitMultiline(cmdimpl, reply, _flush, numLines); 
	  else
	    stat = _config->_devIO->submit(cmdimpl,reply,_flush);
          if( _verbose )
	    clog<<"UFGemPortescapAgent::action> sent config. cmd: "<<cmdimpl<<", device reply: "<<reply<<endl;
          //if( !sad.empty() ) _Status[motor] = sad;
          clog<<"UFGemPortescapAgent::action> sad: "<<sad<<", _Status[]"<<_Status[motor]<<endl;
	}
        else {
	  clog<<"UFGemPortescapAgent::action> (existing setting ok) cmd does not need to be sent: "<<cmdimpl<<endl;
	}
        if( origin ) { // this was an origin command that presumably suceeded
	  if( stat < 0 ) clog<<"UFGemPortescapAgent::action> exception on Origin command? assume success..."<<endl;
	  // clear and return current position
	  _CurPosition[motor] = 0; _currentval = "0.0";
          strstream sp;
          sp<<_CurPosition[motor]<<ends; 
          if( sad != "" ) sendEpics(sad, sp.str()); delete sp.str();
          if( car != "" ) { setCARIdle(car); _setIdleOrErr[motor] = true; }
          //if( _indexorCAR[motor] != "" ) { setCARIdle(_indexorCAR[motor]); _setIdleOrErr[motor] = true; }
	}
      }
      if( nearH || realH ) {
        submittedhome[motor] = true;
	if( _verbose )
         clog<<"set submittedH for "<<motor<<": nearH= "<<nearH<< endl;
	//break; // submitted near home or home, ancillary takes care of all other request in this bundle?
	continue; // submitted near home or home, ancillary takes care of all other request in this bundle?
      }
      if( _DevHistory.size() >= _DevHistSz )
	_DevHistory.pop_front();

      _DevHistory.push_back(reply);

      if( stat < 0 ) {
	errmsg += "Device communication failed";
        if( car != "" ) { setCARError(car, &errmsg); _setIdleOrErr[motor] = true; } // set error
        if( _verbose )
          clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        if( !_Motion.empty() && mm != _Motion.end() ) // remove motor from motion list
          _Motion.erase(mm);
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // freed by the calling func...
      } // end failed dev. comm.
      if( abort || aborthome || abortlimit ) {
        // motion was aborted
        // clear  HomeThenStep
 	_HomeThenStep[motor] = "false";
        // clear  HomeStepHome
 	_HomeStepHome[motor] = "false";
 	_HomeHome[motor] = "false";
        if( !_Motion.empty() && mm != _Motion.end() ) 
          _Motion.erase(mm); // remove from motion list
        if( !_Homing.empty() && hm != _Homing.end() ) 
	  _Homing.erase(hm); // remove from motion list
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) 
	  _HomingFinal.erase(hmF); // remove from motion list
        if( !_NearHoming.empty() && nrhm != _NearHoming.end() ) 
	  _NearHoming.erase(nrhm); // remove from motion list
        if( !_LimitSeeking.empty() && ls != _LimitSeeking.end() ) 
	  _LimitSeeking.erase(ls); // remove from motion list
      }
      if( stat > 0 && car != "" || sad != "" ) { // device replied:
        string s = reply.substr(cmdimpl.length());
        // currently only origin or abort or position request by epics client requires immediate care:
	bool reqpos = (reply.find("Z") != string::npos || reply.find("z") != string::npos);
	// need to update _currentval if command was Z
	if (reqpos) {
	  _currentval = s;
	  _CurPosition[motor] = atof(s.c_str());
	}

        if( origin || abort || (reqpos  && s.length() > 0) ) {
	  clog<<"UFGemPortescapAgent::action> indexor reply: "<<reply<<", sad: "<<sad<<", car: "<<car<<endl;
          //if( sad != "" ) sendEpics(sad, _currentval);
          if( car != "" ) { setCARIdle(car); _setIdleOrErr[motor] = true; }
          //if( _indexorCAR[motor] != "" ) { setCARIdle(_indexorCAR[motor]); _setIdleOrErr[motor] = true; }
	}
	if (reply.find("X") != string::npos)
	  _parseXCmdReply(motor,s);
      } // reply sent back to epics
    } // non sim.
    act->cmd_reply.push_back(reply);
  } // end for loop of cmd bundle

  // if the client is an epics db (car != "" ) send ok or error
  // completion of all req. in this bundle.
  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    //if( _verbose )
      clog<<"UFGemPortescapAgent::action> one or more cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    if( car != "" ) setCARError(car, &errmsg); _setIdleOrErr[motor] = true;
  }
  else {
    //if( _verbose ) 
      clog<<"UFGemPortescapAgent::action> motor: "<<motor<<", CAR: "<<car<<", SAD: "<<sad<<endl;
    
    act->status_cmd = "succeeded";
    if( car != "" && !_setIdleOrErr[motor] && !motion && !homing && !homingF && !nearhoming ) {
      //if( _verbose ) {
        clog<<"UFGemPortescapAgent::action> cmd. complete, set CAR to idle, OkOrErr flag: "<<_setIdleOrErr[motor]<<endl;
        clog<<"presumably this OK/Idle is only being sent in response to a non-motion request (parameter reset, abort, etc)."<<endl;
	//}
      //if( sad != "" ) sendEpics(sad, _currentval);
      setCARIdle(car); _setIdleOrErr[motor] = true;
    }
  }
 
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client )
    return (UFStrings*) 0;

  return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
} // action

bool UFGemPortescapAgent::_nearHome(const string& motor, string& cmdimpl) {
  if( _verbose )
    clog<<"_nearHome> NearHome= "<<_NearHome<<", cmdimpl=> "<<cmdimpl<<endl;

  if( cmdimpl.rfind("N") == string::npos || cmdimpl.rfind("N") == 0 )
    return false;

  double diff = ::fabs(_CurPosition[motor] - _NearHome);
  strstream s;
  if( _CurPosition[motor] >= _NearHome )
    s<<motor<<"-"<<(int)diff<<ends;
  else
    s<<motor<<"+"<<(int)diff<<ends;

  cmdimpl = s.str(); delete s.str();
  //if( _verbose )
    clog<<"_nearHome> motor: "<<motor<<", _curPos= "<<_CurPosition[motor]<<", NearHome= "<<_NearHome<<", cmdimpl=> "<<cmdimpl<<endl;

  return true;
}

int UFGemPortescapAgent::_parseXCmdReply(const string& motor, const string& xReply) {
  //need to parse 'X' reply and populate sad recs
  string s = xReply;
  if (s.find("\n") == string::npos || s.find("\n")+1 >= s.length() ) {
    clog << "UFGemPortescapAgent::_parseXCmdReply> Badly formatted reply from X command: "<<s << endl;	    
  } else {
    string firstline = s.substr(0,s.find("\n"));
    string secondline = s.substr(s.find("\n")+1);
    string totalline = firstline + " " + secondline;
    totalline = totalline.substr(totalline.find("Y= "));
    //clog << "UFGemPortescapAgent::_parseXCmdReply> firstline: " << firstline << " secondline: " << secondline << " End." <<endl;

    UFStringTokenizer ufst(totalline);
    UFStringTokenizer curst(ufst[1],'/');
    UFStringTokenizer accst(ufst[5],'/');
    if (ufst.size() < 15 || curst.size() < 2 || accst.size() < 2) {
      clog << "UFGemPortescapAgent::_parseXCmdReply> Badly formatted reply from X command: "<<totalline<<endl;
      return -1;
    }
    string acc = accst[0];
    clog << "UFGemPortescapAgent::_parseXCmdReply> acc: " << acc<<endl;
    string dec = accst[1];
    string secnum = ufst[3];
    string hc = curst[0];
    string rc = curst[1];
    unsigned int i=0;
    while (i < ufst.size() && ufst[i] != string("I=")) i++;
    if (i >= ufst.size()) {
      clog << "UFGemPortescapAgent::_parseXCmdReply> Badly formatted reply from X command: "<<totalline<<endl;
      return -1;
    }
    string initVel = ufst[i+1];
    while (i < ufst.size() && ufst[i] != string("V=")) i++;
    if (i+1 >= ufst.size()) {
      clog << "UFGemPortescapAgent::_parseXCmdReply> Badly formatted reply from X command: "<<totalline<<endl;
      return -1;
    }
    string slewVel = ufst[i+1];
    sendEpics(_ParamSads[motor][0],initVel);
    sendEpics(_ParamSads[motor][1],slewVel);
    sendEpics(_ParamSads[motor][2],acc);
    sendEpics(_ParamSads[motor][3],dec);
    sendEpics(_ParamSads[motor][4],hc);
    sendEpics(_ParamSads[motor][5],rc);
    //clog << "UFGemPortescapAgent::_parseXCmdReply> acc: " <<acc<< " dec: "<<dec<<" hc: "<<hc<<" rc: "<<rc<<" initVel: "<<initVel<<" slewVel: "<<slewVel<<endl;
  }
  return 1;
}

int UFGemPortescapAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFGemPortescapAgent
