#if !defined(__UFFLAM2MECH_H__)
#define __UFFLAM2MECH_H__
/* for use by C & C++ to describe mechanism names & positions */
/* assume Epics-like string length limits */

#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "ctype.h"
/*
#if defined(CYGWIN)
#include "libguile/values.h"
#else
#include "values.h"
#endif
*/
#include "limits.h"
#include "math.h"

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

/* public indices into UFFLAM2Mech->mech[0:7] struct array: */
#define UFF2WindowIdx 0
#define UFF2MOSIdx 1
#define UFF2MOSBarCdIdx 2
#define UFF2DeckerIdx 3
#define UFF2Filter1Idx 4
#define UFF2LyotIdx 5
#define UFF2Filter2Idx 6
#define UFF2GrismIdx 7
#define UFF2FocusIdx 8


/*
 * Filter1, Filter2, and Grism wheel --
 *         Measured with 'F 600 0'
 * Lyot Wheel --
 *         Mesaured with 'F 200 1'
 * Limit switch wiring normally open when away from home
 * ( 0 corresponds to positive steps, 
 *   1 corresponds to negative steps)          
 */
#define WINDOW_OFFSET 0
#define MOSWHEEL_OFFSET 0
#define DECKER_OFFSET 0
#define FILTER1_OFFSET -110
#define LYOT_OFFSET 11
#define FILTER2_OFFSET 358
#define GRISM_OFFSET 90
#define FOCUS_OFFSET 0

static const int __UFEPICSSLEN_ = 40; 
static const int __UFMAXPOSFLAM2_ = 40; 
static const int __UFMAXINDXFLAM2_ = 9; /* mechanism/indexor count MOS counts as two */
static const char* _nonexist = "NonExistent";

typedef struct {
  int posCnt, stepCount360; /* number of (<= max) positions (including home) in use */
  char *name; /* name[__UFEPICSSLEN_]; */ /* Gemini keyword for mech. named pos */
  char *posname; /* posname[__UFEPICSSLEN_]; */ /* Gemini keyward for pos name step cnt */
  char **positions; /* positions[__UFMAXPOSFLAM2_][__UFEPICSSLEN_]; */
  float *steps; /* steps[__UFMAXPOSFLAM2_]; + steps from home */
} UFFLAM2MechPos;

typedef struct __UFFLAM2Mech_ {
  int mechCnt;
  char **indexor; /* indexor[__UFMAXINDXFLAM2_][__UFEPICSSLEN_]; */
  int *perlport; 
  UFFLAM2MechPos *mech; /* mech[__UFMAXINDXFLAM2_]; */
} UFFLAM2Mech;

/* non-reentrant non-thread-safe: */
static UFFLAM2Mech* getTheUFFLAM2Mech() {
  /* mos wheel and mos barcode entries share indexor B */
  /* default Perle port configuration */
  static int _perlports[]= { 24, 23, 23, 22, 21, 20, 19, 18, 17 };
  /* default indexor name configuration */
  static char* _indexors[]= { "A", "B", "B", "C", "D", "E", "F", "G", "H" };

  /* force names to match Gemini DHS conf. (unless truncation accomplishes same effect)...
     note Gemini expects separate keywords for the position name and set cnt */

  /* window cover uses limit switches ~ 15k steps apart;
     seek + limit for datum/open: M+200; seek - limit for park/closed: M-200 */
  static char* _nameWC = "Window"; /* "WindowCover";  */
  static char* _posWC = "WindowPos";
  static char* _positionsWC[] = { "Datum", "Open", "Park", "Closed" }; 
  static float _stepWC[] = { 0, 0, -15000, -15000 };
  static UFFLAM2MechPos wc;

  static char* _nameMW = "MOS"; /* "MOSWheel"; */
  static char* _posMW = "MOSPos"; 
  static char* _positionsMW[] = { "Imaging", "mos1", "2pix-slit", "mos2", "3pix-slit", "mos3", "circle1", "mos4", "4pix-slit", "mos5", "6pix-slit", "mos6", "circle2", "mos7", "8pix-slit", "mos8", "1pix-slit", "mos9", "Park", "Datum" }; 
  static float _stepMW[] = { 0 + MOSWHEEL_OFFSET, 1417 + MOSWHEEL_OFFSET, 1958 + MOSWHEEL_OFFSET, 2500 + MOSWHEEL_OFFSET, 3042 + MOSWHEEL_OFFSET, 3583 + MOSWHEEL_OFFSET, 5000 + MOSWHEEL_OFFSET, 6417 + MOSWHEEL_OFFSET, 6958 + MOSWHEEL_OFFSET, 7500 + MOSWHEEL_OFFSET, 8042 + MOSWHEEL_OFFSET, 8583 + MOSWHEEL_OFFSET, 10000 + MOSWHEEL_OFFSET, 11417 + MOSWHEEL_OFFSET, 11958 + MOSWHEEL_OFFSET, 12500 + MOSWHEEL_OFFSET, 13042 + MOSWHEEL_OFFSET, 13583 + MOSWHEEL_OFFSET, 0, 0 };
  static UFFLAM2MechPos mw;

  static char* _nameMB = "MOSBarCd"; /* "MOSWheel BarCodes" */
  static char* _posMB = "MOSBCPos"; 
  /* use the test rig mockup bar codes for now... */
  /*
  static char* _positionsMB[] = { "mos1", "mos2", "mos3", "mos4", "mos5", "mos6", "mos7", "mos8", "mos9", "mos10", "circ1", "circ2", "Park", "Datum" };
  static float _stepMB[] = { 150, 250, 350, 450, 550, 650, 750, 850, 950, 1050,
			     1150, 1250, 150, 0 };
  */
  /* these names <= 8 char allow them to be FITS keywords... */
  static char* _positionsMB[] = { "MOSCIRC1", "MOSCIRC2", "MOSPLT01", "MOSPLT02", "MOSPLT03", "MOSPLT04", "MOSPLT05", "MOSPLT06", "MOSPLT07", "MOSPLT08", "MOSPLT09", "MOSPLT10", "MOSPark", "MOSDatum" };
  static float _stepMB[] = { 100, 152, 185, 225, 350, 445, 495, 570, 635, 725,
			     1150, 1250, 1000, 0 };
  static UFFLAM2MechPos mb;

  static char* _nameDW = "Decker"; /* "DeckerWheel"; */
  static char* _posDW = "DeckerPos";
  static char* _positionsDW[] = { "Imaging", "Long_slit", "MOS", "Park", "Datum" }; 
  static float   _stepDW[] = { 0 + DECKER_OFFSET, 1667 + DECKER_OFFSET, 3333 + DECKER_OFFSET, 0, 0 };
  static UFFLAM2MechPos dw;

  static char* _nameFW1 = "Filter1"; /* "Filter1Wheel"; */
  static char* _posFW1 = "Filt1Pos"; 
#if defined(IRLAB)
  static char* _positionsFW1[] = { "Open", "Datum_Offset", "J", "H", "Ks", "J-lo", "Park", "Datum" }; 
  static float _stepFW1[] = { 0 + FILTER1_OFFSET, 67500, 13800 + FILTER1_OFFSET, 27600 + FILTER1_OFFSET, 41400 + FILTER1_OFFSET, 55200 + FILTER1_OFFSET, 13800+FILTER1_OFFSET, 0};
#else
  static char* _positionsFW1[] = { "Open", "Datum_Offset", "J", "H", "Ks", "J-lo", "Park", "Datum" }; 
  static float _stepFW1[] = { 0 + FILTER1_OFFSET, 67500, 13800 + FILTER1_OFFSET, 27600 + FILTER1_OFFSET, 41400 + FILTER1_OFFSET, 55200 + FILTER1_OFFSET, 13800+FILTER1_OFFSET, 0};
#endif
  static UFFLAM2MechPos fw1;

  static char* _nameLW = "Lyot"; /* "LyotWheel";  */
  static char* _posLW = "LyotPos"; 
#if defined(IRLAB)
  static char* _positionsLW[] = { "f/16", "Datum_Offset", "MCAO_over", "MCAO_under", "Hartmann1", "Hartmann2", "Park", "Datum" }; 
  static float   _stepLW[] = { 0 + LYOT_OFFSET, 2725, 575 + LYOT_OFFSET, 1150 + LYOT_OFFSET, 1725 + LYOT_OFFSET, 2300 + LYOT_OFFSET, 0+LYOT_OFFSET, 0 };
#else
  static char* _positionsLW[] = { "f/16", "Datum_Offset", "MCAO_over", "MCAO_under", "Hartmann1", "Hartmann2", "Park", "Datum" }; 
  static float   _stepLW[] = { 0 + LYOT_OFFSET, 2725, 575 + LYOT_OFFSET, 1150 + LYOT_OFFSET, 1725 + LYOT_OFFSET, 2300 + LYOT_OFFSET, 0+LYOT_OFFSET, 0 };
#endif
  static UFFLAM2MechPos lw;

  static char* _nameFW2 = "Filter2"; /* "Filter2Wheel"; */
  static char* _posFW2 = "Filt2Pos"; 
#if defined(IRLAB)
  static char* _positionsFW2[] = { "Open", "Datum_Offset", "JH", "HK_good", "HK_bad", "Dark", "Park", "Datum" }; 
  static float _stepFW2[] = { 0 + FILTER2_OFFSET, 67500, 13800 + FILTER2_OFFSET, 27600 + FILTER2_OFFSET, 41400 + FILTER2_OFFSET, 55200 + FILTER2_OFFSET, 55200+FILTER2_OFFSET,  0 };
#else
  static char* _positionsFW2[] = { "Open", "Datum_Offset", "JH", "HK_good", "HK_bad", "Dark", "Park", "Datum" }; 
  static float _stepFW2[] = { 0 + FILTER2_OFFSET, 67500, 13800 + FILTER2_OFFSET, 27600 + FILTER2_OFFSET, 41400 + FILTER2_OFFSET, 55200 + FILTER2_OFFSET, 55200+FILTER2_OFFSET,  0 };
#endif
  static UFFLAM2MechPos fw2;

  static char* _nameGW = "Grism"; /* "GrismWheel";  */
  static char* _posGW = "GrismPos";
#if defined(IRLAB)
  static char* _positionsGW[] = { "Open", "Datum_Offset", "JH", "Dark", "OpenR3K", "HK", "Park", "Datum" }; 
  static float   _stepGW[] = { 0 + GRISM_OFFSET, 67500, 13800 + GRISM_OFFSET, 27600 + GRISM_OFFSET, 41400 + GRISM_OFFSET, 55200 + GRISM_OFFSET, 0+GRISM_OFFSET, 0};
#else
  static char* _positionsGW[] = { "Open", "Datum_Offset", "JH", "Dark", "OpenR3K", "HK", "Park", "Datum" }; 
  static float   _stepGW[] = { 0 + GRISM_OFFSET, 67500, 13800 + GRISM_OFFSET, 27600 + GRISM_OFFSET, 41400 + GRISM_OFFSET, 55200 + GRISM_OFFSET, 0+GRISM_OFFSET, 0};
#endif
  static UFFLAM2MechPos gw;

  /* focus mech (like the window) uses two limit switches, but park and datum can be congruent
     and not necessarily useful for observing. there may be up to 3 different focus positions
     used in oberving modes, each at some TBD steps between limits. this is due to the expected
     shift in the focal plane associated with different opitcal configs. */
  static char* _nameFoc = "Focus"; /* "DetectorFocus"; */
  static char* _posFoc = "FocusPos";
  /* two identical MCAO positions simplify Lyot correlation */
  static char* _positionsFoc[] = { "f/16",  "MCAO_under", "MCAO_over", "Park", "Datum" }; 
  static float   _stepFoc[] = { 1000 + FOCUS_OFFSET, 1500 + FOCUS_OFFSET, 1500 + FOCUS_OFFSET, 0, 0 };
  static UFFLAM2MechPos foc;

  static UFFLAM2Mech _TheFLAM2Mech;
  static UFFLAM2MechPos* _mech= 0; /* compile time init. */

  if( _mech != 0 ) {
    return &_TheFLAM2Mech;
  }  
 
  _mech = (UFFLAM2MechPos*) calloc(__UFMAXINDXFLAM2_, sizeof(UFFLAM2MechPos));

  wc.name = _nameWC;
  wc.posname = _posWC;
  wc.positions = _positionsWC;
  wc.steps = _stepWC;
  wc.posCnt = _ArrayLen__(_stepWC);
  wc.stepCount360 = 69000; 

  dw.name = _nameDW;
  dw.posname = _posDW;
  dw.positions = _positionsDW;
  dw.steps = _stepDW;
  dw.posCnt = _ArrayLen__(_stepDW);
  dw.stepCount360 = 69000;

  mw.name = _nameMW;
  mw.posname = _posMW;
  mw.positions = _positionsMW;
  mw.steps = _stepMW;
  mw.posCnt = _ArrayLen__(_stepMW);
  mw.stepCount360 = 69000;
  mb.name = _nameMB;
  mb.posname = _posMB;
  mb.positions = _positionsMB;
  mb.steps = _stepMB;
  mb.posCnt = _ArrayLen__(_stepMB);
  mb.stepCount360 = 69000;

  fw1.name = _nameFW1;
  fw1.posname = _posFW1;
  fw1.positions = _positionsFW1;
  fw1.steps = _stepFW1;
  fw1.posCnt = _ArrayLen__(_stepFW1);
  fw1.stepCount360 = 69000;

  fw2.name = _nameFW2;
  fw2.posname = _posFW2;
  fw2.positions = _positionsFW2;
  fw2.steps = _stepFW2;
  fw2.posCnt = _ArrayLen__(_stepFW2);
  fw2.stepCount360 = 69000;

  lw.name = _nameLW;
  lw.posname = _posLW;
  lw.positions = _positionsLW;
  lw.steps = _stepLW;
  lw.posCnt = _ArrayLen__(_stepLW);
  lw.stepCount360 = 2875;

  gw.name = _nameGW;
  gw.posname = _posGW;
  gw.positions = _positionsGW;
  gw.steps = _stepGW;
  gw.posCnt = _ArrayLen__(_stepGW);
  gw.stepCount360 = 69000;

  foc.name = _nameFoc;
  foc.posname = _posFoc;
  foc.positions = _positionsFoc;
  foc.steps = _stepFoc;
  foc.posCnt = _ArrayLen__(_stepFoc);
  foc.stepCount360 = 69000;

  _TheFLAM2Mech.mechCnt = __UFMAXINDXFLAM2_;
  _TheFLAM2Mech.perlport = _perlports; 
  _TheFLAM2Mech.indexor = _indexors; 
  _TheFLAM2Mech.mech = _mech;

  _TheFLAM2Mech.mech[UFF2WindowIdx] = wc;
  _TheFLAM2Mech.mech[UFF2MOSIdx] = mw;
  _TheFLAM2Mech.mech[UFF2MOSBarCdIdx] = mb;
  _TheFLAM2Mech.mech[UFF2DeckerIdx] = dw;
  _TheFLAM2Mech.mech[UFF2Filter1Idx] = fw1;
  _TheFLAM2Mech.mech[UFF2LyotIdx] = lw;
  _TheFLAM2Mech.mech[UFF2Filter2Idx] = fw2;
  _TheFLAM2Mech.mech[UFF2GrismIdx] = gw;
  _TheFLAM2Mech.mech[UFF2FocusIdx] = foc;

  return &_TheFLAM2Mech;
}

/* this assumes that char** names never been initialized
   and is passed via &names: */
static int mechNamesFLAM2(char*** names, char*** posnames) {
  static char **_names= 0, **_posnames= 0;
  UFFLAM2Mech* _TheFLAM2Mech= getTheUFFLAM2Mech();
  int i;
  if( _names == 0 ) {
    _names = (char**) calloc(__UFMAXINDXFLAM2_, sizeof(char*));
    _posnames = (char**) calloc(__UFMAXINDXFLAM2_, sizeof(char*));
    for( i = 0; i < _TheFLAM2Mech->mechCnt; ++i ) {
      _names[i] = _TheFLAM2Mech->mech[i].name;
      _posnames[i] = _TheFLAM2Mech->mech[i].posname;
    }
  }

  *names = _names;
  *posnames = _posnames;
  return _TheFLAM2Mech->mechCnt;
}

static char* infoFLAM2MechOf(int idx, UFFLAM2MechPos** mech) {
  UFFLAM2Mech* _TheFLAM2Mech= getTheUFFLAM2Mech();
  static UFFLAM2MechPos f2m; 
  static char indexor[2];
  memset(&f2m, 0, sizeof(f2m)); /* sizeof(UFFLAM2MechPos)); */
  memset(indexor, 0, sizeof(indexor));
  if( idx > UFF2FocusIdx || idx < 0 ) {
    *mech = 0;
    return 0;
  }

  f2m = _TheFLAM2Mech->mech[idx];
  *mech = &f2m;
  strcpy(indexor, _TheFLAM2Mech->indexor[idx]);
  return &indexor[0];
}

static char* indexorOfMechName(const char* name) {
  int idx = 0;  
  UFFLAM2Mech* _TheFLAM2Mech= getTheUFFLAM2Mech();
  UFFLAM2MechPos* mech= 0;
  char* indexor = infoFLAM2MechOf(idx, &mech);
  /* first try exact match */
  while( idx < _TheFLAM2Mech->mechCnt ) {
    if( strcmp(mech->name, name) == 0 ) return indexor;
    indexor = infoFLAM2MechOf(++idx, &mech);
  }

  /* then try substring match */
  idx = 0;
  while( idx < _TheFLAM2Mech->mechCnt ) {
    if( strstr(mech->name, name) != 0 ) return indexor;
    indexor = infoFLAM2MechOf(++idx, &mech);
  }
  return 0;
}

static char* mechNameOfIndexor(const char* idxor) {
  int idx = 0;  
  UFFLAM2Mech* _TheFLAM2Mech= getTheUFFLAM2Mech();
  UFFLAM2MechPos* mech= 0;
  char* indexor = infoFLAM2MechOf(idx, &mech);
  while( idx < _TheFLAM2Mech->mechCnt ) {
    if( strcmp(idxor, indexor) == 0 ) return mech->name;
    indexor = infoFLAM2MechOf(++idx, &mech);
  }
  return 0;
}

/* set comma separated list of indexor:mechname in one pre-allocated string:
   A:Window,B:MOSWheel,...*/
static int setIndexorMechNameList(char* list) {
  char indexors[] = "ABCDEFGH\0";
  int i= 0, n = strlen(indexors);
  char *mechname= 0, idxor[2]; memset(idxor, 0, 2);
  for( i = 0; i < n; ++i ) {
    idxor[0] = indexors[i];
    strcpy(list++,idxor); strncpy(list++,":",1);
    mechname = mechNameOfIndexor(idxor);
    if( i < n - 1 ) strncpy(list++,",",1);
  }
  return n;
}

/* return position name steps value */
static double stepsFromHomeFLAM2(const char* mechName, char* posName) {
  int m= 0, p= 0;
  unsigned int i = 0;
  char* s= 0;
  char buff1[256];
  char buff2[256];
  UFFLAM2MechPos mech;
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();

  if( mechName == 0 )
    return INT_MIN;

  for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    mech = _TheFLAM2Mech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  if( s == 0 )
    return INT_MIN;
  /* printf("stepsFromHomeFLAM2> %s\n", s); */

  /* check for exact match first */
  for( p = 0; p < mech.posCnt; ++p ) {
    if( strcmp(mech.positions[p], posName) == 0 ) 
      break;
  }
  if( p == mech.posCnt) { /* try case-insensitive match*/
    memset(buff1,0,256);    
    memcpy(buff1,posName,(256 < strlen(posName) ? 256 : strlen(posName)));
    for (i=0; i<strlen(buff1); i++)
      buff1[i] = toupper(buff1[i]);
    for (p =0; p<mech.posCnt; ++p) {
      memset(buff2,0,256);
      memcpy(buff2,mech.positions[p],(256 < strlen(mech.positions[p]) ? 256 : strlen(mech.positions[p])));
      for (i=0; i<strlen(buff2); i++)
	buff2[i] = toupper(buff2[i]);
      if ( strcmp(buff1, buff2) == 0)
	break;
    }
  }
  if (p == mech.posCnt) return INT_MIN;
/*
  if( p == mech.posCnt ) { *//* try substr */
/*
    for( p = 0; p < mech.posCnt; ++p ) {
      if( (s = strstr(mech.positions[p], posName)) != 0 ) 
        break;
    }
  }
  if (p == mech.posCnt) { *//* try case-insensitive substr*/
/*
    memset(buff1,0,256);    
    memcpy(buff1,posName,(256 < strlen(posName) ? 256 : strlen(posName)));
    for (i=0; i<strlen(buff1); i++)
      buff1[i] = toupper(buff1[i]);
    for (p =0; p<mech.posCnt; ++p) {
      memset(buff2,0,256);
      memcpy(buff2,mech.positions[p],(256 < strlen(mech.positions[p]) ? 256 : strlen(mech.positions[p])));
      for (i=0; i<strlen(buff2); i++)
	buff2[i] = toupper(buff2[i]);
      if ( (s = strstr(buff1, buff2)) != 0)
	break;
    }
  }
*/
  if( s == 0 )
    return INT_MIN;

  return mech.steps[p];
};

/* return position name associated with nearest match to steps value */
static char* posNameNearFLAM2(const char* mechName, double stepsFromHome) {
  static char* s= 0;
  int m= 0, p= 0;
  double diffmin= INT_MAX, diffs[__UFMAXPOSFLAM2_];
  UFFLAM2MechPos mech;
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();

  if( mechName == 0 )
    return 0;

  /* try exact match */
  for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    mech = _TheFLAM2Mech->mech[m];
    if( strcmp(mech.name, mechName) == 0 ) {
      s = (char*)mechName; 
      break;
    }
  }
  if( s == 0 ) { /* try substring */
    for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
      mech = _TheFLAM2Mech->mech[m];
      if( (s = strstr(mech.name, mechName)) != 0 ) 
        break;
    }
  }
  if( s == 0 )
    return (char*)_nonexist;

  for( p = 0; p < mech.posCnt; ++p ) {
    diffs[p] = fabs(stepsFromHome - mech.steps[p]);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
    diffs[p] = fabs(stepsFromHome - mech.steps[p]+mech.stepCount360);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
    diffs[p] = fabs(stepsFromHome - mech.steps[p]-mech.stepCount360);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
  }
  if( s == 0 )
    return (char*)_nonexist;

  return s;
}

/* set list of named positions for specified mechanism via comma separated single string.
   posnames should be pre-allocated (via char s[BUFSIZ]  */ 
static int setNamedPositionList(const char* mechName, char* posnames) {
  static char* s= 0;
  char *mechname = (char*) mechName;
  int m= 0, p= 0;
  UFFLAM2MechPos mech;
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();

  if( mechName == 0 || posnames == 0 )
    return -1;

  if( strlen(mechName) == 1 ) /* indexor */
    mechname = mechNameOfIndexor(mechName);

  if( mechname == 0 )
    return -1;
    
  /* try exact match */
  for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    mech = _TheFLAM2Mech->mech[m];
    if( strcmp(mech.name, mechname) == 0 ) {
      s = (char*)mechname;
      break;
    }
  }
  if( s == 0 ) { /* try substring */
    for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
      mech = _TheFLAM2Mech->mech[m];
      if( (s = strstr(mech.name, mechname)) != 0 )
        break;
    }
  }
  if( s == 0 )
    return -1;

  for( p = 0; p < mech.posCnt; ++p ) {
    strcpy(posnames,mech.positions[p]); posnames += strlen(mech.positions[p]);
    if( p < mech.posCnt - 1 ) strcpy(posnames++,",");
  }

  return mech.posCnt; 
}

/* print named positions */ 
static int printNamedPositions(const char* mechName) {
  static char* s= 0;
  char *mechname = (char*) mechName;
  int m= 0, p= 0;
  /*
  double diffmin= INT_MAX, diffs[__UFMAXPOSFLAM2_];
  */
  UFFLAM2MechPos mech;
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();

  if( mechName == 0 )
    return -1;

  if( strlen(mechName) == 1 ) /* indexor */
    mechname = mechNameOfIndexor(mechName);

  if( mechname == 0 )
    return -1;
    
  /* try exact match */
  for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    mech = _TheFLAM2Mech->mech[m];
    if( strcmp(mech.name, mechname) == 0 ) {
      s = (char*)mechname;
      break;
    }
  }
  if( s == 0 ) { /* try substring */
    for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
      mech = _TheFLAM2Mech->mech[m];
      if( (s = strstr(mech.name, mechname)) != 0 )
        break;
    }
  }
  if( s == 0 )
    return -1;

  printf("Named Positions for: %s\n", s);
  for( p = 0; p < mech.posCnt; ++p ) {
    printf("%s\t%d\n", mech.positions[p], (int)mech.steps[p]);
  }

  return mech.posCnt; 
}

static int printAllNamedPositions() {
  int m= 0, cnt= 0;
  char s[BUFSIZ];
  UFFLAM2MechPos mech;
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();
  for( m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    memset(s, 0, BUFSIZ);
    mech = _TheFLAM2Mech->mech[m];
    cnt += setNamedPositionList(mech.name, s);
    printf("Named Positions for: %s == %s\n", mech.name, s);
  }
  return cnt;
}

/* return steps from home associated with park positions */
static double parkFLAM2(const char* mechName) {
  /* insure mechName is legit. */
  char **names, **posnames, *park= "Park";
  int i, stat= -1, nm = mechNamesFLAM2(&names, &posnames);
  double steps= 0;
  char* indexor= 0;

  if( mechName == 0 )
    return stat;

  indexor = (char*) indexorOfMechName(mechName);
  if( indexor == 0 )
    return stat;

  for( i= 0; i < nm; ++i ) {
    stat = strcmp(mechName, names[i]);
    if( stat == 0 ) {
      stat = i;
      break;
    }
  }
  if( stat <= 0 )
    return stat;

  steps = stepsFromHomeFLAM2(mechName, park);
  park = posNameNearFLAM2(mechName, steps); /* and get the park pos. name back */

  return steps;
}

/* to avoid warning about any function not being used, use once here */
static void _stub(const char* mechName) {
  if( mechName == 0 )
    return;

  setIndexorMechNameList(0);
  setNamedPositionList(0, 0);
  printNamedPositions(0);
  printAllNamedPositions();
  parkFLAM2(0);

  _stub(0);
}

#endif /* __UFFLAM2MECH_H__ */
