package ufjei;

/**
 * Title:        JAVA Engineering Console
 * Description:
 * Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
 * Company:      University of Florida
 * @author David Rashkin, Latif Albusairi and Tom Kisko
 * @version 1.0
 */


//===============================================================================
/**
 * Handles a single item for the records tree??
 */
public class EPICSRecsNode {
  public String type;
  public String text;


//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   *@param type String: Type of file??
   *@param text String: Text displayed next to file in the tree
   */
  public EPICSRecsNode(String type, String text) {
    this.type = type;
    this.text = text;
  } //end of EPICSRecsNode


//-------------------------------------------------------------------------------
  /**
   *Returns the class-field: text
   */
  public String toString() {
    return text;
  } //end of toString

} //end of class EPICSRecsNode
