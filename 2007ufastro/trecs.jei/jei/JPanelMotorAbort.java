package ufjei;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


//===============================================================================
/**
 * Abort Panel
 */
public class JPanelMotorAbort extends JPanel implements ActionListener {
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorAbort.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  public JButton b[] ;


//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param mot_param jeiMotorParameters: motor parameters object
   */
  public JPanelMotorAbort(jeiMotorParameters mot_param) {
    b = new JButton [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    ///gridLayout.setVgap(5);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JButton ("Abort");
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       b[i].setEnabled(true) ;
       b[i].setName("DO_NOT_ABORT");
       add (b[i]);
    }
  } //end of JPanelMotorAbort


//-------------------------------------------------------------------------------
  /**
   * Action Performed??
   *@param e TBD
   */
  public void actionPerformed(ActionEvent e) {
    String mot_num_str = e.getActionCommand();
    int mot_num = Integer.parseInt(mot_num_str)+1;
    String pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "abort.A";
    String cmd = "PUT " + pv_name + " ABORT ;abort motor movement";
    jeiCmd command = new jeiCmd();
    command.execute(cmd);
    pv_name = jeiMotorParameters.getPVmotorPrefix(mot_num) + "motorApply.DIR";
    cmd = "PUT " + pv_name + " 3 ;apply the abort";
    command = new jeiCmd();
    command.execute(cmd);

    JPanelMotorHighLevel.jPanelNamedLocation.NamedLocation[mot_num-1].setBackground(Color.white);

  } //end of actionPerformed

} //end of class JPanelMotorAbort


