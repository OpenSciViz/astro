package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;                // Object serialization streams.
import java.net.*;
import javax.swing.event.*;


//===============================================================================
/**
 *Scripts tabbed pane
 */
public class JPanelScripts extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelScripts.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  String script_file_name = "T-ReCS.s";
  public static boolean recording = false;
  public static boolean logging = false;
  public static String log_file_name = null;
  public static TextFile log_file = null;
  public Vector scripts = new Vector();
  Script selectedScript;
  String selectedCmd;
  String moving_motors;
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JList jListScripts = new JList(scripts);
  JList jListCmds = new JList();
  JScrollPane scrollScripts = new JScrollPane(jListScripts);
  JScrollPane scrollCmds = new JScrollPane(jListCmds);
  ScriptDialogNewScript dialogScript = new ScriptDialogNewScript(null, "Enter the script name", true);
  ScriptDialogNewCmd dialogCmd = new ScriptDialogNewCmd(null, "Enter the new cmd information", true);
  JLabel jLabel2 = new JLabel();
  GridLayout gridLayout1 = new GridLayout();
  JPanel jPanel3 = new JPanel();
  JButton jButtonExecuteScript = new JButton();
  JButton jButtonAddScript = new JButton();
  JButton jButtonRenameScript = new JButton();
  JButton jButtonMoveScriptUp = new JButton();
  JButton jButtonMoveScriptDown = new JButton();
  JButton jButtonLoadScript = new JButton();
  JButton jButtonSaveScript = new JButton();
  JButton jButtonDeleteScript = new JButton();
  JButton jButtonExecuteCmd = new JButton();
  JButton jButtonAddCmd = new JButton();
  JButton jButtonEditCmd = new JButton();
  JButton jButtonMoveCmdDown = new JButton();
  JButton jButtonMoveCmdUp = new JButton();
  JButton jButtonDeleteCmd = new JButton();
  GridLayout gridLayout2 = new GridLayout();
  JPanel jPanel4 = new JPanel();
  GridLayout gridLayout3 = new GridLayout();
  String MotorIP = new String();
  int PortNumber;
  JButton recordButton = new JButton();
  public static JCheckBox execWhileRecording = new JCheckBox();

  GridBagLayout gridBagLayout1 = new GridBagLayout();
  GridBagLayout gridBagLayout2 = new GridBagLayout();


//-------------------------------------------------------------------------------
  /**
   *Construct the Scripts panel frame
   */
  public JPanelScripts() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      jeiError.show("jbInit exception :    " + e);
      //System.out.println("jbInit exception :");
      //System.out.println("    " + e);
      //e.printStackTrace();
    }
  } //end of JPanelScripts


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    selectedScript = null;
    selectedCmd = null;
    moving_motors = "";
    jListScripts.setPrototypeCellValue("ABCEDEFGHIJKLMNOPQ");
    jListScripts.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jListScripts.setVisibleRowCount(6);
    jListScripts.addListSelectionListener (new ListSelectionListener() {
      public void valueChanged (ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          selectedScript = (Script)jListScripts.getSelectedValue();
          if (selectedScript != null) {
            jListCmds.setListData(selectedScript.cmds);
            if (selectedScript.cmds.size() > 0) jListCmds.setSelectedIndex(0);
          }
          repaint();
        }
      }
    });
    jListCmds.setPrototypeCellValue("ABCEDEFGHIJKLMNOPQ");
    jListCmds.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    jListCmds.setVisibleRowCount(6);
    jListCmds.addListSelectionListener (new ListSelectionListener() {
      public void valueChanged (ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
          selectedCmd = (String)jListCmds.getSelectedValue();
          repaint();
        }
      }
    });
    this.setLayout(gridLayout1);
    this.setSize(new Dimension(618, 400));
    jLabel1.setForeground(Color.blue);
    jLabel1.setText("Scripts in " + script_file_name);
    dialogScript.setLocation(200,200);
    dialogCmd.setLocation(200,200);
    jLabel2.setForeground(Color.blue);
    jLabel2.setText("Commands in the Selected Script");
    jPanel1.setLayout(gridBagLayout1);
    jPanel2.setLayout(gridBagLayout2);
    jButtonExecuteScript.setText("Execute Script");
    jButtonExecuteScript.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonExecuteScript_actionPerformed(e);
      }
    });
    jButtonRenameScript.setText("Rename Script");
    jButtonRenameScript.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonRenameScript_actionPerformed(e);
      }
    });
    jButtonAddScript.setText("Add Script");
    jButtonAddScript.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonAddScript_actionPerformed(e);
      }
    });
    jButtonDeleteScript.setText("Delete Script");
    jButtonDeleteScript.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonDeleteScript_actionPerformed(e);
      }
    });
    jButtonMoveScriptUp.setText("Move Script Up");
    jButtonMoveScriptUp.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonMoveScriptUp_actionPerformed(e);
      }
    });
    jButtonMoveScriptDown.setText("Move Script Down");
    jButtonMoveScriptDown.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonMoveScriptDown_actionPerformed(e);
      }
    });
    jButtonLoadScript.setText("Load Script File");
    jButtonLoadScript.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonLoadScript_actionPerformed(e);
      }
    });
    jButtonSaveScript.setText("Save Script File");
    jButtonSaveScript.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonSaveScript_actionPerformed(e);
      }
    });
    jButtonExecuteCmd.setText("Execute Cmd");
    jButtonExecuteCmd.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonExecuteCmd_actionPerformed(e);
      }
    });
    jButtonEditCmd.setText("Edit Cmd");
    jButtonEditCmd.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonEditCmd_actionPerformed(e);
      }
    });
    jButtonAddCmd.setText("Add Cmd");
    jButtonAddCmd.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonAddCmd_actionPerformed(e);
      }
    });
    jButtonDeleteCmd.setText("Delete Cmd");
    jButtonDeleteCmd.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonDeleteCmd_actionPerformed(e);
      }
    });
    jButtonMoveCmdDown.setText("Move Cmd Down");
    jButtonMoveCmdDown.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonMoveCmdDown_actionPerformed(e);
      }
    });
    jButtonMoveCmdUp.setText("Move Cmd Up");
    jButtonMoveCmdUp.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButtonMoveCmdUp_actionPerformed(e);
      }
    });
    jPanel3.setLayout(gridLayout2);
    gridLayout2.setColumns(2);
    gridLayout2.setHgap(4);
    gridLayout2.setRows(0);
    gridLayout2.setVgap(4);
    jPanel4.setLayout(gridLayout3);
    gridLayout3.setColumns(2);
    gridLayout3.setHgap(4);
    gridLayout3.setRows(0);
    gridLayout3.setVgap(4);
    recordButton.setText("Record Cmds");
    recordButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        recordButton_actionPerformed(e);
      }
    });
    execWhileRecording.setText("Execute cmds while recording");
    this.add(jPanel1, null);
    jPanel1.add(jLabel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 9), 257, 0));
    jPanel1.add(scrollScripts, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 0, 9), 35, 89));
    jPanel1.add(jPanel3, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(6, 12, 78, 22), 9, 11));
    jPanel3.add(jButtonExecuteScript, null);
    jPanel3.add(jButtonRenameScript, null);
    jPanel3.add(jButtonAddScript, null);
    jPanel3.add(jButtonDeleteScript, null);
    jPanel3.add(jButtonMoveScriptUp, null);
    jPanel3.add(jButtonMoveScriptDown, null);
    jPanel3.add(jButtonLoadScript, null);
    jPanel3.add(jButtonSaveScript, null);
    this.add(jPanel2, null);
    jPanel2.add(jLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(5, 5, 0, 9), 111, 0));
    jPanel2.add(scrollCmds, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 0, 9), 35, 89));
    jPanel2.add(jPanel4, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
            ,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 43, 25), 17, 16));
    jPanel4.add(jButtonExecuteCmd, null);
    jPanel4.add(jButtonEditCmd, null);
    jPanel4.add(jButtonAddCmd, null);
    jPanel4.add(jButtonDeleteCmd, null);
    jPanel4.add(jButtonMoveCmdUp, null);
    jPanel4.add(jButtonMoveCmdDown, null);
    jPanel4.add(recordButton, null);
    jPanel4.add(execWhileRecording, null);
    load(script_file_name);
    jListScripts.setSelectedIndex(0);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *File | Exit action performed
   *@param e not used
   */
  public void fileExit_actionPerformed(ActionEvent e) {
    System.exit(0);
  } //end of fileExit_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *Help | About action performed
   *@param e not used
   */
  public void helpAbout_actionPerformed(ActionEvent e) {

  } //end of helpAbout_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *Overridden so we can exit on System Close
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    //super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      fileExit_actionPerformed(null);
    }
  } //end of processWindowEvent


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#execute script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonExecuteScript_actionPerformed(ActionEvent e) {
    Script script = (Script)jListScripts.getSelectedValue();
    String cmd;
    for (int j = 0; j < script.cmds.size(); j++) {   // For each cmd in the script
      cmd = (String)script.cmds.elementAt(j);
      execute(cmd);
    }
  } //end of jButtonExecuteScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#rename script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonRenameScript_actionPerformed(ActionEvent e) {
    Script s = (Script)jListScripts.getSelectedValue();
    if (s != null) {
      dialogScript.nameToDisplay(s.name);
      dialogScript.setVisible(true);
      if (dialogScript.ok) {
        s.name = dialogScript.script.name;
        jListScripts.repaint();
      }
    }
  } //end of jButtonRenameScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#add script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonAddScript_actionPerformed(ActionEvent e) {
    dialogScript.nameToDisplay("");
    dialogScript.setVisible(true);
    if (dialogScript.ok) {
      int i = jListScripts.getSelectedIndex();
      scripts.insertElementAt(dialogScript.script, i + 1);
      jListScripts.setListData(scripts);
      jListScripts.setSelectedIndex(i + 1);
      selectedScript = dialogScript.script;
      jListScripts.ensureIndexIsVisible(i + 1);
    }
  } //end of jButtonAddScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#delete script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonDeleteScript_actionPerformed(ActionEvent e) {
    int i = jListScripts.getSelectedIndex();
    if (i >= 0) {
      scripts.remove(i);
      jListScripts.setListData(scripts);
      if (i == scripts.size()) i--;
      if (scripts.size() > 0) jListScripts.setSelectedIndex(i);
      selectedScript = (Script)jListScripts.getSelectedValue();
    }
  } //end of jButtonDeleteScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#move script up button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveScriptUp_actionPerformed(ActionEvent e) {
    int i = jListScripts.getSelectedIndex();
    if (i >= 1) {
      Object o1 = scripts.get(i);
      Object o2 = scripts.get(i - 1);
      scripts.setElementAt(o1, i - 1);
      scripts.setElementAt(o2, i);
      jListScripts.setListData(scripts);
      jListScripts.setSelectedIndex(i - 1);
      selectedScript = (Script)o1;
      jListScripts.ensureIndexIsVisible(i - 1);
    }
  } //end of jButtonMoveScriptUp_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#move script down button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveScriptDown_actionPerformed(ActionEvent e) {
    int i = jListScripts.getSelectedIndex();
    if (i >= 0 && i < scripts.size() - 1) {
      Object o1 = scripts.get(i);
      Object o2 = scripts.get(i + 1);
      scripts.setElementAt(o1, i + 1);
      scripts.setElementAt(o2, i);
      jListScripts.setListData(scripts);
      jListScripts.setSelectedIndex(i + 1);
      selectedScript = (Script)o1;
      jListScripts.ensureIndexIsVisible(i + 1);
    }
  } //end of jButtonMoveScriptDown_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#load script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonLoadScript_actionPerformed(ActionEvent e) {
    load();
  } //end of jButtonLoadScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#save script button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonSaveScript_actionPerformed(ActionEvent e) {
    save();
  } //end of jButtonSaveScript_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#execute command button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonExecuteCmd_actionPerformed(ActionEvent e) {
    String cmd = (String)jListCmds.getSelectedValue();
    execute(cmd);
  } //end of jButtonExecuteCmd_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#edit command button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonEditCmd_actionPerformed(ActionEvent e) {
    String cmd = (String)jListCmds.getSelectedValue();
    if (cmd != null) {
      dialogCmd.cmdToDisplay(cmd);
      dialogCmd.setVisible(true);
      if (dialogCmd.ok) {
        int i = jListCmds.getSelectedIndex();
        selectedScript.cmds.setElementAt((Object)dialogCmd.cmd, i);
        jListCmds.setListData(selectedScript.cmds);
      }
    }
  } //end of jButtonEditCmd_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#add command button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonAddCmd_actionPerformed(ActionEvent e) {
    dialogCmd.clear();
    dialogCmd.setVisible(true);
    if (dialogCmd.ok) {
      add_new_cmd(dialogCmd.cmd);
    }
  } //end of jButtonAddCmd_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#add new command button action performed
   *@param e ActionEvent: TBD
   */
  public void add_new_cmd(String cmd) {
    int i = jListCmds.getSelectedIndex();
    selectedScript.cmds.insertElementAt(cmd, i + 1);
    jListCmds.setListData(selectedScript.cmds);
    jListCmds.setSelectedIndex(i + 1);
    selectedCmd = dialogCmd.cmd;
    jListCmds.ensureIndexIsVisible(i + 1);
  } //end of add_new_cmd


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#delete command button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonDeleteCmd_actionPerformed(ActionEvent e) {
    int i = jListCmds.getSelectedIndex();
    if (i >= 0) {
      selectedScript.cmds.remove(i);
      jListCmds.setListData(selectedScript.cmds);
      if (i == selectedScript.cmds.size()) i--;
      if (selectedScript.cmds.size() > 0) jListCmds.setSelectedIndex(i);
      selectedCmd = (String)jListCmds.getSelectedValue();
    }
  } //end of jButtonDeleteCmd_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#move command up button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveCmdUp_actionPerformed(ActionEvent e) {
    int i = jListCmds.getSelectedIndex();
    if (i >= 1) {
      Object o1 = selectedScript.cmds.get(i);
      Object o2 = selectedScript.cmds.get(i - 1);
      selectedScript.cmds.setElementAt(o1, i - 1);
      selectedScript.cmds.setElementAt(o2, i);
      jListCmds.setListData(selectedScript.cmds);
      jListCmds.setSelectedIndex(i - 1);
      selectedCmd = (String)o1;
      jListCmds.ensureIndexIsVisible(i - 1);
    }
  } //end of jButtonMoveCmdUp_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#move command down button action performed
   *@param e ActionEvent: TBD
   */
  void jButtonMoveCmdDown_actionPerformed(ActionEvent e) {
    int i = jListCmds.getSelectedIndex();
    if (i >= 0 && i < selectedScript.cmds.size() - 1) {
      Object o1 = selectedScript.cmds.get(i);
      Object o2 = selectedScript.cmds.get(i + 1);
      selectedScript.cmds.setElementAt(o1, i + 1);
      selectedScript.cmds.setElementAt(o2, i);
      jListCmds.setListData(selectedScript.cmds);
      jListCmds.setSelectedIndex(i + 1);
      selectedCmd = (String)o1;
      jListCmds.ensureIndexIsVisible(i + 1);
    }
  } //end of jButtonMoveCmdDown_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *JPanelScripts#record button action performed
   *@param e ActionEvent: TBD
   */
  void recordButton_actionPerformed(ActionEvent e) {
     if (recording) {
       recording = false;
       recordButton.setText("Record Cmds");
       recordButton.setForeground(Color.black);
       jeiFrame.statusBar.setText("");
     }
     else {
       recording = true;
       recordButton.setText("Stop Recording");
       recordButton.setForeground(Color.red);
       jeiFrame.statusBar.setText(">>> RECORDING <<<");
     }
  } //end of recordButton_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *Save the script file
   *brings up a chooser dialog
   */
  public void save() {
    Script script;
    JFileChooser chooser = new JFileChooser(jei.data_path);
    ExtensionFilter script_type = new ExtensionFilter (
          "T-ReCS script files", new String[] {".s"});
    chooser.setFileFilter (script_type); // Initial filter setting
    chooser.setSelectedFile(new File(script_file_name));
    chooser.setDialogTitle("Save script to");
    chooser.setApproveButtonText("Save");
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.CANCEL_OPTION ) return;
    if(returnVal == JFileChooser.APPROVE_OPTION) {
      script_file_name = chooser.getSelectedFile().getName();
      if (!script_file_name.endsWith(".s")) script_file_name = script_file_name + ".s";
    }
    TextFile out = new TextFile(jei.data_path + script_file_name, TextFile.OUT) ;
    for (int i = 0; i < scripts.size(); i++) {  // For each script
      script = (Script)scripts.elementAt(i);
      out.println("~" + script.name);   // Write ~ and the script name
      for (int j = 0; j < script.cmds.size(); j++) {   // For each cmd in the script
        out.println((String)script.cmds.elementAt(j)); // Write the cmd
      }
    }
    out.close();                 // And close the stream.
    jLabel1.setText("Scripts in " + script_file_name);
  } //end of save


//-------------------------------------------------------------------------------
  /**
   *Load a script file
   *brings up a chooser dialog
   */
  public void load() {
    JFileChooser chooser = new JFileChooser(jei.data_path);
    ExtensionFilter script_type = new ExtensionFilter (
          "T-ReCS cfg files", new String[] {".s"});
    chooser.setFileFilter (script_type); // Initial filter setting
    chooser.setSelectedFile(new File(script_file_name));
    chooser.setDialogTitle("Load script file from");
    chooser.setApproveButtonText("Load");
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.CANCEL_OPTION ) return;
    if(returnVal == JFileChooser.APPROVE_OPTION) {
      script_file_name = chooser.getSelectedFile().getName();
      if (!script_file_name.endsWith(".s")) script_file_name = script_file_name + ".s";
    }
    load(script_file_name);
  } //end of load


//-------------------------------------------------------------------------------
  /**
   *Load a script file with given name
   *@param scripts_file_name String: script file name
   */
  public void load(String scripts_file_name) {
    jLabel1.setText("Scripts in " + scripts_file_name);
    try {
      TextFile in = new TextFile(jei.data_path + script_file_name, TextFile.IN);
//      BufferedReader in = new BufferedReader(new FileReader(jei.data_path + script_file_name));
      Vector scripts_loaded = new Vector();
      String s;
      Vector cmds = null;
      boolean ok = true;
      if (!in.ok()) jeiError.show("error opening" + jei.data_path + script_file_name);
      else {
        s = in.readLine();
        if (!in.ok()) jeiError.show("error reading" + jei.data_path + script_file_name);
        while (s != null && in.ok()) {
          if (s.length() < 1 || s.charAt(0) != '~') {
            jeiError.show(script_file_name + " is not a valid script file.");
            /*JOptionPane.showMessageDialog(null,script_file_name +
                " is not a valid script file.",  "Error", JOptionPane.ERROR_MESSAGE);*/
            s = null;
            ok = false;
          }
          else { // we have a valid ~ script_name line, read the cmds for the script
            String script_name = new String(s.substring(1));
            cmds = new Vector();
            boolean done = false;
            while (!done && in.ok()) {  // read cmds until a ~ or eof
              s = in.readLine();
              if (!in.ok()) jeiError.show("error reading " + jei.data_path + script_file_name);
              if (s == null || s.length() >= 1 && s.charAt(0) == '~') {
                done = true; // we found a new script name or eof
              }
              else { // presume the line s is a cmd
                cmds.add(s);
              }
            }
            Script script = new Script(script_name, cmds);
            scripts_loaded.add(script);
            // execute the script if named autoexec
            if (script_name.equals("autoexec")) {
              String cmd;
              for (int j = 0; j < script.cmds.size(); j++) {   // For each cmd in the script
                cmd = (String)script.cmds.elementAt(j);
                execute(cmd);
              }
            }
          }
        }
        in.close();                    // Close the stream.
        repaint();
      }

      if (ok) {
        scripts = scripts_loaded;
        jListScripts.setListData(scripts);
        jListScripts.setSelectedIndex(0);
        repaint();
      }
    }
    // Print out exceptions.  We should really display them in a dialog...
    catch (Exception e) {
      JOptionPane.showMessageDialog(null, e,  "Error", JOptionPane.ERROR_MESSAGE);
    }
  } //end of load


//-------------------------------------------------------------------------------
  /**
   *Log method??
   *@param s String: string added to log
   */
  void log(String s) {
    if (logging) {
      /// tbd - add timestamp to log file entries
      log_file.println(s);
      System.out.println(s);
    }
  } //end of log


//-------------------------------------------------------------------------------
  /**
   *Execute given command
   *@param cmd String: command to execute
   */
  void execute(String cmd) {
    String response;
    if (cmd != null) {
      if (logging) log(cmd);
      StringTokenizer cmdTokens = new StringTokenizer(cmd) ;
      String cmd_verb = cmdTokens.nextToken().toUpperCase();
      if (cmd_verb.equals("DELAY")) { // delay command
        int ms = Integer.parseInt(cmdTokens.nextToken());
        try {
          Thread.sleep(ms);
        }
        catch (Exception _e) {}
        //System.out.println(cmd + " : Finished");
      }
      else if (cmd_verb.equals("PAUSE")) { // pause command
        String msg;
        if (cmdTokens.hasMoreTokens()) msg = cmdTokens.nextToken("\n");
        else msg = "Pausing due to a PAUSE command in a script. Press OK to continue.";
        JOptionPane.showMessageDialog(null, msg, "PAUSE", JOptionPane.INFORMATION_MESSAGE);
      }
      else if (cmd_verb.equals("LOG_START")) { // log_start command
        if (logging) log_file.close();
        log_file_name = cmdTokens.nextToken();
        log_file = new TextFile(jei.data_path + log_file_name, TextFile.OUT) ;
        if (log_file.ok()) logging = true;
        else logging = false;
        if (logging) log(cmd);
      }
      else if (cmd_verb.equals("LOG_STOP")) { // log_stop command
        logging = false;
        log_file.close();                 // And close the stream.
      }
      else {
        jeiCmd command = new jeiCmd();
        command.execute(cmd);
      }
    }
  } //end of execute

} //end of class JPanelScripts


//===============================================================================
/**
 *Object containing commands read from the script file??
 */
class Script implements Serializable {  // This class represents an arbitrary Script
  String name;
  Vector cmds;


//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  Script() {
    name = "";
    cmds = new Vector();
  } //end of Script


//-------------------------------------------------------------------------------
  /**
   *Constructs given name and commands
   *@param _name String: name of the script
   *@param _cmds Vector: commands for the script to use
   */
  Script(String _name, Vector _cmds) {
    name = _name;
    cmds = new Vector(_cmds);
  } //end of Script


//-------------------------------------------------------------------------------
  /**
   *Constructs given a Script object
   *@param script Script: script object
   */
  Script(Script script) {
    name = script.name;
    cmds = new Vector(script.cmds);
  } //end of Script


//-------------------------------------------------------------------------------
  /**
   *Returns name of script
   */
  public String toString() {
    if (name.length() == 0) return name;
    else if (name.charAt(0) == ';') return name;
    return "    " + name;
  } //end of toString

} //end of class Script


//===============================================================================
/**
 *Dialog for making new script
 */
class ScriptDialogNewScript extends JDialog {
  public boolean ok;
  public Script script;
  JPanel panel1 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JTextField jTextField1 = new JTextField();
  JLabel jLabel2 = new JLabel();
  JButton jButton1 = new JButton();
  JButton jButton2 = new JButton();
  JTextArea jTextArea1 = new JTextArea();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JPanel jPanel3 = new JPanel();


//-------------------------------------------------------------------------------
  /**
   *Constructs given a parent frame, dialog title and modal boolean
   *@param frame Frame: parent frame
   *@param title String: dialog title
   *@param modal boolean: TBD??
   */
  public ScriptDialogNewScript(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try  {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  } //end of ScriptDialogNewScript


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public ScriptDialogNewScript() {
    this(null, "", false);
  } //end of ScriptDialogNewScript


//-------------------------------------------------------------------------------
  /**
   *Set default name in the text field
   *@param _name String: name to set
   */
  public void nameToDisplay(String _name) {
    ok = false;
    jTextField1.setText(_name);
    jTextField1.grabFocus();
  } //end of nameToDisplay


//-------------------------------------------------------------------------------
  /**
   *??
   *@param script Script: script object
   */
  public void set_fields(Script script) {
    ok = false;
    jTextField1.setText(script.name);
    jTextField1.grabFocus();
  } //end of set_fields


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  void jbInit() throws Exception {
    jLabel1.setText("Script name");
    panel1.setLayout(new BorderLayout());
    jTextField1.setColumns(40);
    jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {

      public void keyPressed(KeyEvent e) {
        jTextField1_keyPressed(e);
      }
    });
    jButton1.setText("OK");
    jButton1.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButton1_actionPerformed(e);
      }
    });
    jButton2.setText("Cancel");
    jButton2.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        jButton2_actionPerformed(e);
      }
    });
    jTextArea1.setLineWrap(true);
    jTextArea1.setColumns(40);
    jTextArea1.setWrapStyleWord(true);
    jTextArea1.setRows(2);
    jTextArea1.setBackground(Color.lightGray);
    jTextArea1.setText("Start the name with a semicolon, to make it a section title " +
        "in the script list.\nName it autoexec to make it automatically execute when loaded.");
    jTextArea1.setForeground(Color.blue);
    jTextArea1.setEditable(false);
    jTextArea1.setFont(new java.awt.Font("Dialog", 0, 12));
    //jTextArea1.setFont(new java.awt.Font("Dialog", 0, 12));
    getContentPane().add(panel1);
    panel1.add(jPanel1, BorderLayout.NORTH);
      jPanel1.add(jLabel1);
      jPanel1.add(jTextField1);
    panel1.add(jPanel2, BorderLayout.CENTER);
      jPanel2.add(jButton1);
      jPanel2.add(jButton2);
    panel1.add(jPanel3, BorderLayout.SOUTH);
      jPanel3.add(jTextArea1);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewScript#button 1 action performed
   *@param e not used
   */
  void jButton1_actionPerformed(ActionEvent e) {
    ok = true;
    script = new Script();
    script.name = jTextField1.getText();
    this.setVisible(false);
  } //end of jButton1_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewScript#button 2 action performed
   *@param e not used
   */
  void jButton2_actionPerformed(ActionEvent e) {
    ok = false;
    this.setVisible(false);
  } //end of jButton2_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewScript#text field 1#key pressed action performed
   *@param e not used
   */
  void jTextField1_keyPressed(KeyEvent e) {
     //System.out.println("pressed " + e.getKeyChar());
     if (e.getKeyChar() == '\n') jButton1_actionPerformed(null);
  } //end of jTextField1_keyPressed

} //end of class ScriptDialogNewScript


//===============================================================================
/**
 *Dialog for making new command
 */
class ScriptDialogNewCmd extends JDialog {
  public boolean ok;
  public String cmd;
  JPanel panel1 = new JPanel();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JPanel jPanel3 = new JPanel();
  JLabel jLabel1 = new JLabel();
  JTextField jTextField1 = new JTextField();
  JButton jButtonOK = new JButton();
  JButton jButtonCancel = new JButton();
  JLabel jLabel2 = new JLabel();
  JTextArea jTextArea1 = new JTextArea();


//-------------------------------------------------------------------------------
  /**
   *Constructs given a parent frame, title, and modal??
   *@param frame Frame: parent frame
   *@param title String: title of ??
   *@param modal boolean: TBD??
   */
  public ScriptDialogNewCmd(Frame frame, String title, boolean modal) {
    super(frame, title, modal);
    try  {
      jbInit();
      pack();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  } //end of ScriptDialogNewCmd


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public ScriptDialogNewCmd() {
    this(null, "", false);
  } //end of ScriptDialogNewCmd


//-------------------------------------------------------------------------------
  /**
   *Clears the text field
   */
  public void clear() {
    ok = false;
    jTextField1.setText("");
    jTextField1.grabFocus();
  } //end of clear


//-------------------------------------------------------------------------------
  /**
   *Sets the text in the text field
   *@param cmd String: command string to be put in the text field
   */
  public void cmdToDisplay(String cmd) {
    ok = false;
    jTextField1.setText(cmd);
    jTextField1.grabFocus();
  } //end of cmdToDisplay


//-------------------------------------------------------------------------------
  /**
   *Sets the text in the text field
   *@param cmd String: command string to be put in the text field
   */
  public void set_fields(String cmd) {
    ok = false;
    jTextField1.setText(cmd);
    jTextField1.grabFocus();
  } //end of set_fields


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  void jbInit() throws Exception {
    jLabel1.setText(" ");
    panel1.setLayout(new BorderLayout());
    jTextField1.setColumns(40);
    jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {

      public void keyPressed(KeyEvent e) {
        jTextField1_keyPressed(e);
      }
    });
    jButtonOK.setText("OK");
    jButtonOK.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonOK_actionPerformed(e);
      }
    });
    jButtonCancel.setText("Cancel");
    jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(ActionEvent e) {
        jButtonCancel_actionPerformed(e);
      }
    });
    jLabel2.setText("New Command");
    jTextArea1.setColumns(70);
    jTextArea1.setBackground(Color.lightGray);
    jTextArea1.setLineWrap(true);
    jTextArea1.setText("Commands:\n  ;comment line\n  PUT rec value\n  GET rec\n  OPATH" +
        "; opens optical path window\n  DELAY ms\n  PAUSE optional_message\n  LOG_START " +
            "log_file_name\n  LOG_STOP\n  .master_scrn_field : value\n  ADD_REC rec && " +
            "description && EPICS_recs_window_name\nNotes:\n  rec means EPICS_record_name or " +
            "EPICS_record_name.field_name\n  Semicolon comments may be appended to any command");
    getContentPane().add(panel1);
    jPanel1.setLayout(new FlowLayout());
    jPanel2.setLayout(new FlowLayout());
    jPanel3.setLayout(new FlowLayout());
    panel1.add(jPanel1, BorderLayout.NORTH);
      jPanel1.add(jLabel1);
      jPanel1.add(jLabel2);
      jPanel1.add(jTextField1);
    panel1.add(jPanel2, BorderLayout.CENTER);
      jPanel2.add(jTextArea1);
    panel1.add(jPanel3, BorderLayout.SOUTH);
      jPanel3.add(jButtonOK);
      jPanel3.add(jButtonCancel);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewCmd#ok button action performed
   *@param e not used
   */
  void jButtonOK_actionPerformed(ActionEvent e) {
    ok = true;
    cmd = new String();
    cmd = jTextField1.getText();
    setVisible(false);
  } //end of jButtonOK_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewCmd#cancel button action performed
   *@param e not used
   */
  void jButtonCancel_actionPerformed(ActionEvent e) {
    ok = false;
    setVisible(false);
  } //end of jButtonCancel_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *ScriptDialogNewCmd#text field 1# key pressed action performed
   *@param e TBD??
   */
  void jTextField1_keyPressed(KeyEvent e) {
     //System.out.println("pressed " + e.getKeyChar());
     if (e.getKeyChar() == '\n') jButtonOK_actionPerformed(null);
  } //end of jTextField1_keyPressed

} //end of class ScriptDialogNewCmd

