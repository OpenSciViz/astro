package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;

/**
 *Title:      Jei save window
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     David Rashkin
 *Company:    University of Florida
 *Description:T-ReCS is the Mid IR instrument for the Gemini South telescope. This is the T-ReCS Java Engineering Interface.
 */


//===============================================================================
/**
 *
 */
public class jeiFrameSaveBox extends JDialog {
  public static final String rcsID = "$Name:  $ $Id: jeiFrameSaveBox.java,v 0.1 2003/03/12 00:00:02 varosi beta $";
  private String tempFileName =     "tmp_ctrl_params.txt";
  private String motorFileName =    "mot_param_v3.txt";
  private String detectorFileName = "detector_params.txt";
  JPanel panel1 = new JPanel();
  JPanel panel2 = new JPanel();
  JPanel insetsPanel1 = new JPanel();
  JPanel insetsPanel2 = new JPanel();
  JPanel insetsPanel3 = new JPanel();
  JButton button1 = new JButton();
  JButton button2 = new JButton();
  JCheckBox checkBox1 = new JCheckBox("Motor Parameters",true);
  JCheckBox checkBox2 = new JCheckBox("Temperature Parameters",true);
  JCheckBox checkBox3 = new JCheckBox("Detector Parameters",true);
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();


//-------------------------------------------------------------------------------
  /**
   *default Constructor from a parent frame
   *@param parent the parent Frame
   */
  public jeiFrameSaveBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    pack();
  } //end of jeiFrameSaveBox


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    this.setTitle("Save");
    setResizable(false);
    panel1.setLayout(borderLayout1);
    panel2.setLayout(borderLayout2);
    insetsPanel1.setLayout(flowLayout1);
    insetsPanel2.setLayout(flowLayout1);
    insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
    gridLayout1.setRows(4);
    gridLayout1.setColumns(1);
    insetsPanel3.setLayout(gridLayout1);
    insetsPanel3.setBorder(new EmptyBorder(10, 60, 10, 10));
    button1.setText("OK");
    button1.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
             button1_actionPerformed(e);
         }
    });
    button2.setText("CANCEL");
    button2.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
             button2_actionPerformed(e);
         }
    });
    panel2.add(insetsPanel2, BorderLayout.WEST);
    this.getContentPane().add(panel1, null);
    insetsPanel3.add(new JLabel("Files to save..."), null);
    insetsPanel3.add(checkBox1, null);
    insetsPanel3.add(checkBox2, null);
    insetsPanel3.add(checkBox3, null);
    panel2.add(insetsPanel3, BorderLayout.CENTER);
    insetsPanel1.add(button1, null);
    insetsPanel1.add(button2, null);
    panel1.add(insetsPanel1, BorderLayout.SOUTH);
    panel1.add(panel2, BorderLayout.NORTH);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**method used when writing text files
   *copies all lines containg the character ';'
   *(comment lines) from infile to outfile
   *@param infile BufferedReader
   *@param outfile PrintWriter
   */
  public String deCommentAndWrite (BufferedReader infile, PrintWriter outfile ) {
     String l = null;
     try {l = infile.readLine();}
     catch (Exception e) { System.out.println (e.toString());}
       while ((l!=null) && (l.indexOf(";") != -1))
       {
          try { outfile.println(l);
                l = infile.readLine();}
          catch (Exception e) { System.out.println (e.toString());}
       }
     return l;
  } //end of deCommentAndWrite


//-------------------------------------------------------------------------------
  /**
   *
   */
  public void button1_actionPerformed (ActionEvent e) {
     String tildaFileName = "";
     if (checkBox3.isSelected()) { // save detector parameters
       try {
         tildaFileName = detectorFileName.substring(0,detectorFileName.length()-3) +
                         "~" + detectorFileName.substring(detectorFileName.length()-2);
         File tempFile = new File (tildaFileName);
         if (tempFile.exists())
           if (!tempFile.delete())
             JOptionPane.showMessageDialog(null,"Cannot delete temporary file '" + tildaFileName + "'.  Make Sure file permissions are set correctly");
         BufferedReader inFile = new BufferedReader (new FileReader(detectorFileName));
         PrintWriter outFile = new PrintWriter (new FileWriter(tildaFileName));
         String line;
         for (int i=0; i<10; i++) {             // ten user un-editable parameters
           outFile.println(deCommentAndWrite(inFile,outFile));
         }

         for (int i=0; i<7; i++) {
           deCommentAndWrite(inFile,outFile);
           outFile.println(jeiFrame.jPanelDetector.textGroup[i].getText());
         }
         outFile.close();
         inFile.close();
         File oldFile = new File (detectorFileName);
         if (!oldFile.delete())
           JOptionPane.showMessageDialog(null,"Cannot delete file '" + detectorFileName + "'.  Make Sure file permissions are set correctly");
         File changeName = new File (tildaFileName);
         if (!changeName.renameTo(new File(detectorFileName)))
           JOptionPane.showMessageDialog(null,"Cannot rename temporary file '"+ tildaFileName +"'.  Make Sure file permissions are set correctly");
       }
       catch (Exception ee) {}
     }
       if (checkBox2.isSelected()) { // save temperature parameters
       try {
         tildaFileName = tempFileName.substring(0,tempFileName.length()-3) +
                         "~" + tempFileName.substring(tempFileName.length()-2);
         File tempFile = new File (tildaFileName);
         if (tempFile.exists())
           if (!tempFile.delete())
             JOptionPane.showMessageDialog(null,"Cannot delete temporary file '" + tildaFileName + "'.  Make Sure file permissions are set correctly");
         BufferedReader inFile = new BufferedReader (new FileReader(tempFileName));
         PrintWriter outFile = new PrintWriter (new FileWriter(tildaFileName));
         String line;
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new set point value
         outFile.println(jeiFrame.jPanelTemperature.set_Point.getText());
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new P value
         outFile.println(jeiFrame.jPanelTemperature.p_Text.getText());
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new I value
         outFile.println(jeiFrame.jPanelTemperature.i_Text.getText());
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new D value
         outFile.println(jeiFrame.jPanelTemperature.d_Text.getText());
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new heater position value
         outFile.println("" + jeiFrame.jPanelTemperature.heaterComboBox.getSelectedIndex());
         // Write out all comments
         deCommentAndWrite(inFile,outFile);
         // Write out new graph parameter values
         outFile.println(jeiFrame.jPanelTemperature.jGraphPanel.timeMin + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.timeMax + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.tempMin + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.tempMax + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.xTickDelta + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.xLabelDelta + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.yTickDelta + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.yLabelDelta + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.pollPeriod + " " +
                         jeiFrame.jPanelTemperature.jGraphPanel.maxDataPoints);
         // The rest of the file is not editable under program control
         while ((line = deCommentAndWrite(inFile,outFile)) != null)
           outFile.println(line);

         outFile.close();
         inFile.close();
         File oldFile = new File (tempFileName);
         if (!oldFile.delete())
           JOptionPane.showMessageDialog(null,"Cannot delete file '" + tempFileName + "'.  Make Sure file permissions are set correctly");
         File changeName = new File (tildaFileName);
         if (!changeName.renameTo(new File(tempFileName)))
           JOptionPane.showMessageDialog(null,"Cannot rename temporary file '" + tildaFileName + "'.  Make Sure file permissions are set correctly");
       }
       catch (Exception ee) {}
      }
      if (checkBox1.isSelected()) { // save motor parameters
        try {
         tildaFileName = motorFileName.substring(0,motorFileName.length()-3) +
                         "~" + motorFileName.substring(motorFileName.length()-2);
         File motorFile = new File (tildaFileName);
         if (motorFile.exists())
           if (!motorFile.delete())
             JOptionPane.showMessageDialog(null,"Cannot delete temporary file '" + tildaFileName + "'.  Make Sure file permissions are set correctly");
         BufferedReader inFile = new BufferedReader (new FileReader(motorFileName));
         PrintWriter outFile = new PrintWriter (new FileWriter(tildaFileName));
         String line;
         // Write out all comments and number of motors
         outFile.println(deCommentAndWrite(inFile,outFile));
         // Write out new motor parameter values
         for (int i=0; i<jeiFrame.jeiMotorParameters.getNumberMotors(); i++) {
           //;Mot_#  IS  TS  A  D  HC  DC  AN  Name
           line = (i+1) + " " +
                    jeiFrame.jPanelParameters.jPanelInitialSpeed.InitialSpeed[i].getText() +  " " +
                    jeiFrame.jPanelParameters.jPanelTerminalSpeed.TerminalSpeed[i].getText() + " " +
                    jeiFrame.jPanelParameters.jPanelAcceleration.Acceleration[i].getText() + " " +
                    jeiFrame.jPanelParameters.jPanelDeceleration.Deceleration[i].getText() + " " +
                    "0" + " " +
                    jeiFrame.jPanelParameters.jPanelDriveCurrent.DriveCurrent[i].getText() + " " +
                    (i) + " " +
                    jeiFrame.jeiMotorParameters.getMotorName(i+1);
           deCommentAndWrite(inFile,outFile);
           outFile.println(line);
         }
         // The rest of the file is not editable under program control
         while ((line = deCommentAndWrite(inFile,outFile)) != null)
           outFile.println(line);
         outFile.close();
         inFile.close();
         File oldFile = new File (motorFileName);
         if (!oldFile.delete())
           JOptionPane.showMessageDialog(null,"Cannot delete file '" + motorFileName + "'.  Make Sure file permissions are set correctly");
         File changeName = new File (tildaFileName);
         if (!changeName.renameTo(new File(motorFileName)))
           JOptionPane.showMessageDialog(null,"Cannot rename temporary file '" + tildaFileName + "'.  Make Sure file permissions are set correctly");
       }
       catch (Exception ee) {System.out.println(ee);}

      }
      cancel();
  } //end of button1_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *
   */
  public void button2_actionPerformed (ActionEvent e) {
      cancel();
  } //end of button2_actionPerformed


//-------------------------------------------------------------------------------
  /**
   *
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
    super.processWindowEvent(e);
  } //end of processWindowEvent


//-------------------------------------------------------------------------------
  /**
   *
   */
  void cancel() {
    dispose();
  } //end of cancel

} //end of class jeiFrameSaveBox

