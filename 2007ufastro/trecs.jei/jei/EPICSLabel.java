
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Company:      University of Florida
//Description:

package ufjei;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import jca.*;
import jca.dbr.*;
import jca.event.*;


//===============================================================================
/**
 * Handles Labels related to EPICS
 */
public class EPICSLabel extends JLabel implements MonitorListener {
  public static final String rcsID = "$Name:  $ $Id: EPICSLabel.java,v 0.6 2003/09/04 22:52:33 varosi beta $";
  String monitorRec;
  String prefix;
  Monitor outMon = null;
  boolean showRec;
  JButton button = null;

//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  public EPICSLabel(String rec, String prefix) {
    try  {
      jbInit(rec,prefix);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   * Constructor used to associate this EPICSLabel with
   * a JButton
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  public EPICSLabel(String rec, String prefix, JButton button) {
    try  {
      jbInit(rec,prefix,button);
    }
    catch(Exception ex) {
      jeiError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  private void jbInit(String Rec, String prefix, JButton button) throws Exception {
    this.monitorRec = Rec;
    this.prefix = prefix;
    this.button = button;
    if (monitorRec.trim().equals("")) return;
    // setup monitor(s)
    int mask = Monitor.VALUE;
    try {
      PV pv = new PV(monitorRec);
      Ca.pendIO(jeiFrame.TIMEOUT);
      outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
      Ca.pendIO(jeiFrame.TIMEOUT);
      this.setForeground(new JTextField().getForeground());
    }
    catch (Exception ex) {
      this.setBackground(Color.red);
    }
    this.setHorizontalAlignment(JLabel.LEFT);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  private void jbInit(String Rec, String prefix) throws Exception {
      jbInit(Rec, prefix, null);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(MonitorEvent event) {
    try {
      DBR_DBString dbstr = new DBR_DBString();
      event.pv().get(dbstr);
      Ca.pendIO(jeiFrame.TIMEOUT);

      if (event.pv().name().equals(this.monitorRec)) {
	  this.setText(prefix + "  " + dbstr.valueAt(0) + "   ");
	  if (button != null) {
	      String dbval = dbstr.valueAt(0).trim();
	      if( prefix.indexOf("CAR") >= 0 ) {
		  if( dbval.equals("BUSY") )
		      button.setEnabled(false);
		  else
		      button.setEnabled(true);
	      }
	      else if (checkforBadSocket(dbval)) {
		  button.setBackground(Color.red);
		  button.setForeground(Color.white);
		  button.setText("Reconnect");
	      }
	  }
          if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
          if( this.getText().indexOf("ERR") >= 0 ) {
	      Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	  }
      }
    }
    catch (Exception ex) {
       jeiError.show("Error in monitor changed for EPICSLabel: " + monitorRec + " " + ex.toString());
    }
  } //end of monitorChanged
    
//-------------------------------------------------------------------------------

  boolean checkforBadSocket( String EPICSmessage ) {	
    if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
      return true;
    else
      return false;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      try {
        if (outMon != null) outMon.clear();
      }
      catch (Exception ee) {
        jeiError.show(e.toString());
      }
      ///dispose();
    }
    ///super.processWindowEvent(e);
  } //end of processWindowEvent

} //end of class EPICSTextField

