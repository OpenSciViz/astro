package ufjei;

import jca.*;
import jca.event.*;
import jca.dbr.*;


/**
 * Title:        JAVA Engineering Console
 * Description:
 * Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
 * Company:      University of Florida
 * @author Latif Albusairi, David Rashkin and Tom Kisko
 * @version 1.0
 */


//===============================================================================
/**
 *Handles errors related to EPICS
 */
public class EPICSErrors implements MonitorListener {

  public static final String rcsID = "$Name:  $ $Id: EPICSErrors.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  private Monitor mon;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   *Establishes monitor for the global instrument CAR
   */
  public EPICSErrors() {
    try {
      PV pv = new PV ("trecs:cc:applyC.???");
      Ca.pendIO(jeiFrame.TIMEOUT);
      mon = pv.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE | Monitor.ALARM | Monitor.LOG);
      Ca.pendIO(jeiFrame.TIMEOUT);
    }
    catch (Exception e) {
      jeiError.show("Cannot monitor the error record.  EPICS errors will not be reported" + e.toString());
    }
  } //end of EPICSErrors


  /**
   *Event handling method
   *Reports all EPICS errors to the user by way of jeiError
   *@param me MonitorEvent: TBD
   */
  public void monitorChanged (MonitorEvent me) {
    DBR_DBString type = new DBR_DBString();
    DBR_DBString msg = new DBR_DBString();
    try {
      me.pv().get(type);
      Ca.pendIO(jeiFrame.TIMEOUT);
      if (type.valueAt(0).equals("ERROR")) {
        PV pv = new PV("trecs:cc:applyC.???");
        Ca.pendIO(jeiFrame.TIMEOUT);
        pv.get(msg);
        Ca.pendIO(jeiFrame.TIMEOUT);
        jeiError.show("EPICS reported the following error: "+msg.valueAt(0));
      }
    }
    catch (Exception e) {
      jeiError.show("cannot do channel access: " + e.toString());
    }
  } //end of monitorChanged

} //end of class EPICSErrors

