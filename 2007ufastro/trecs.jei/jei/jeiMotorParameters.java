//
//
// Constructors:
// jeiMotorParameters (String filename) ;
//    Uses default hardcoded values
// jeiMotorParameters (String initial_speed, String terminal_speed, String acceleration,
//                     String deceleration,  String hold_current, String drive_current,
//                     char axis_name, String motor_name, String num_locations,
//                     String offsets [][], String location_names[][] ) ;
//

package ufjei;

import java.io.*;
import java.util.*;
import jca.*;
import jca.dbr.*;
import jca.event.*;


//===============================================================================
/**
 * jeiMotorParameters is an object to hold the prameters for a motor
 */
public class jeiMotorParameters {
  public static final String rcsID = "$Name:  $ $Id: jeiMotorParameters.java,v 0.0 2002/06/03 17:44:36 hon beta $";

  //---------------------------------
  // Class Fields
  //---------------------------------
  private static int NumberMotors ;
  private static String InitialSpeed[] ;
  private static String TerminalSpeed[] ;
  private static String Acceleration[];
  private static String Deceleration[];
  private static String HoldCurrent[];
  private static String DriveCurrent[] ;
  private static String AxisName[];
  private static String MotorName[];
  private static int    NumLocations[];
  private static int    LocationOffsets[][];
  private static String LocationNames[][] ;
  private static float  LocationTransmissions[][];
  private static float  LocationLambdaLow[][];
  private static float  LocationLambdaHi[][];
  private static String PVNames[];


//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   *@param filename String: filename found in "jeiFrame.java"
   */
  public jeiMotorParameters (String filename) {
    int Num_motors ;
    String in_line ;
    try {
      // read data file and assign motor names and positions
      TextFile f = new TextFile(jei.data_path + filename, TextFile.IN);
      // skip over the first three lines which are applicable only to the
      // EPICS database itself
      f.nextUncommentedLine();
      f.nextUncommentedLine();
      Num_motors = Integer.parseInt(f.nextUncommentedLine());
      AllocMemory(Num_motors);
      PVNames = new String[Num_motors];
      for (int i=0; i<Num_motors; i++) {
        //Mot_#  IS  TS  A  D  HC  DC  AN HS FHD HD BL EN Name
        StringTokenizer st = new StringTokenizer(f.nextUncommentedLine()," ");
        int mot_num = Integer.parseInt(st.nextToken());
        //skip over the stuff (params for epics) that we don't care about
        for (int j=0; j<11; j++) st.nextToken();
        PVNames[i] = EPICS.prefix + "cc:" + st.nextToken();
        String mot_name = st.nextToken();
        while (st.hasMoreTokens()) mot_name += " "+st.nextToken();
        setMotorName(mot_num, mot_name);
      }
      // now assign positions and offsets for each motor
      for (int i=0; i<Num_motors; i++) {
        StringTokenizer st = new StringTokenizer(f.nextUncommentedLine()," ");
        int mot_num = Integer.parseInt(st.nextToken());
        int num_positions = Integer.parseInt(st.nextToken());
        initNamedPositions(mot_num, num_positions);
        for (int j=0; j<num_positions; j++) {
          StringTokenizer tr = new StringTokenizer(f.nextUncommentedLine()," ");
          int pos_num = Integer.parseInt(tr.nextToken());
          String offset = tr.nextToken();
	  LocationTransmissions[mot_num][pos_num] = Float.parseFloat(tr.nextToken());
	  LocationLambdaLow[mot_num][pos_num] = Float.parseFloat(tr.nextToken());
	  LocationLambdaHi[mot_num][pos_num] = Float.parseFloat(tr.nextToken());
          String offset_name = tr.nextToken();
          while (tr.hasMoreTokens()) offset_name += " "+tr.nextToken();
          setLocationOffset(mot_num, pos_num, Integer.parseInt(offset));
          setNamedLocations(mot_num, pos_num, offset_name);
        }
      }
      f.close();
    }
    catch (Exception eee) { jeiError.show("Possible error in motor parameter file: "+ eee.toString());}
  } //end of jeiMotorParamters


  //---------------------------------
  // Methods
  //---------------------------------
  //
  //  Allcoate memory once we know the number of motors.

//-------------------------------------------------------------------------------
  /**
   * Allocates Memory
   *@param num_motors int: Number of motors
   */
  private static void AllocMemory(int num_motors) {
    NumberMotors = num_motors;
    //InitialSpeed = new String [NumberMotors + 1] ;
    //TerminalSpeed = new String [NumberMotors + 1] ;
    //Acceleration = new String [NumberMotors + 1];
    //Deceleration = new String [NumberMotors + 1];
    //HoldCurrent = new String [NumberMotors + 1];
    //DriveCurrent = new String [NumberMotors + 1];
    //AxisName = new String [NumberMotors + 1];
    MotorName = new String [NumberMotors + 1] ;
    NumLocations = new int [NumberMotors + 1];
    LocationOffsets = new int[NumberMotors+1][]  ;
    LocationNames = new String [NumberMotors+1][]  ;
    LocationTransmissions = new float [NumberMotors+1][]  ;
    LocationLambdaLow = new float [NumberMotors+1][]  ;
    LocationLambdaHi = new float [NumberMotors+1][]  ;
  } //end of AllocMemory


//-------------------------------------------------------------------------------
  /**
   *Initializes the named position info
   *@param mot_num int: index number of the motor??
   *@param num_positions int: index number of the motor position??
   */
  private static void initNamedPositions (int mot_num, int num_positions) {
    NumLocations[mot_num] = num_positions;
    LocationOffsets[mot_num] =  new int [NumLocations[mot_num]+1] ;
    LocationNames[mot_num] = new String [NumLocations[mot_num]+1] ;
    LocationTransmissions[mot_num] =  new float [NumLocations[mot_num]+1] ;
    LocationLambdaLow[mot_num] =  new float [NumLocations[mot_num]+1] ;
    LocationLambdaHi[mot_num] =  new float [NumLocations[mot_num]+1] ;
  } //end of initNamedPositions


//-------------------------------------------------------------------------------
  /**
   *Get the number of motors
   */
  public static int getNumberMotors () {
    return NumberMotors ;
  } //end of getNumberMotors


//-------------------------------------------------------------------------------
  /**
   * Set the number of motors
   *@param num_motors int: quantity of motors??
   */
  private static void setNumberMotors (int num_motors) {
    NumberMotors = num_motors ;
  } //end of setNumberMotors


//-------------------------------------------------------------------------------
  /**
   *Get and set the motor name given its number
   *@param mot_num int: motor number
   */
  public static String getMotorName (int mot_num) {
    return MotorName[mot_num] ;
  } //end of getMotorName


//-------------------------------------------------------------------------------
  /**
   * Sets the motor name with the given nubmer
   *@param mot_num int: motor number
   *@param mot_name String: motor name
   */
  private static void setMotorName (int mot_num, String mot_name) {
    MotorName[mot_num] =  mot_name;
  } //end of setMotorName


//-------------------------------------------------------------------------------
  /**
   *Get the motor number given its axis name
   *@param axisName axis name
   */
  public static int getMotorNumber (String axisName) {
    int i = 0;
    while (i < AxisName.length) {
      if (AxisName[i] != null)
        if (AxisName[i].compareTo(axisName) == 0)
          return i;
      i++;
    }
    return -1;
  } //end of getMotorNumber


//-------------------------------------------------------------------------------
  /**
   *Get the name of a locations
   *@param mot_num int: motor number
   *@param location_num int: location number
   */
  public static String getNamedLocations(int mot_num, int location_num) {
    return LocationNames[mot_num][location_num]  ;
  } //end of getNamedLocations


//-------------------------------------------------------------------------------
  /**
   * Sets the name of the location given the number and motor number
   *@param mot_num int: motor number
   *@param location_num int: location number
   *@param name String: location name
   */
  private static void setNamedLocations(int mot_num, int location_num, String name) {
    LocationNames[mot_num][location_num] = name ;
  } //end of setNamedLocations


//-------------------------------------------------------------------------------
  /**
   *Get and set the offset from home for a given location number
   *@param mot_num int: motor number
   *@param location_num int location number
   */
  public static int getLocationOffset (int mot_num, int location_num) {
    return LocationOffsets[mot_num][location_num] ;
  } //end of getLocationsOffset


//-------------------------------------------------------------------------------
  /**
   * Set the location offset
   *@param mot_num int: motor number
   *@param location_num int: location number
   *@param offset int: offset
   */
  private static void setLocationOffset (int mot_num, int location_num, int offset) {
    LocationOffsets[mot_num][location_num] = offset;
  } //end of setLocationOffset


//-------------------------------------------------------------------------------
  /**
   *Get and set the number of locations
   *@param mot_num int: motor number
   */
  public static int getNumLocations (int mot_num) {
    return NumLocations[mot_num] ;
  } //end of getNumLocations


//-------------------------------------------------------------------------------
  /**
   * Set the number of locations??
   *@param mot_num int: motor number
   *@param num_location int: number of locations??
   */
  private static void setNumLocations(int mot_num, int num_location) {
    NumLocations[mot_num] = num_location;
  } //end of setNumLocations


//-------------------------------------------------------------------------------
  /**
   *(EPICS) get a motor's steps_to_go PV name
   *@param mot_num int: motor number
   */
  public static String getPVmotorPrefix (int mot_num) {
    // note -- mot_num > 0
    return PVNames[mot_num-1]+ ":";
  } //end of getPVmotorPrefix
} //end of class jeiMotorParameters
