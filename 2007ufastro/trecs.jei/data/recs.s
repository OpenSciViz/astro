~is - Instrument Sequencer
ADD_REC trecs:dataMode.A&& in, STRING, <save|discard>, Commanded data disposition mode&&is - Instrument Sequencer - 0
ADD_REC trecs:dataMode.VALA&& out, STRING, <save|discard>, Current data disposition&&is - Instrument Sequencer - 0
ADD_REC trecs:debug.A&& in, STRING, <NONE|MIN|FULL>, Commanded debuggin output level&&is - Instrument Sequencer - 0
ADD_REC trecs:debug.VALA&& out, STRING, <NONE|MIN|FULL>, Current debugging output level&&is - Instrument Sequencer - 0
ADD_REC trecs:init.A&& in, STRING, <NONE|VSM|FAST|FULL>, Commanded simulation mode&&is - Instrument Sequencer - 0
ADD_REC trecs:init.VALA&& out, STRING, <NONE|VSM|FAST|FULL>, Current simulation mode&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.A&& in, STRING, imaging|spectroscopy, Commanded camera mode&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.B&& in, STRING, <field|window|pupil>, Commanded imaging mode&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.C&& in, STRING, TBD, Commanded aperture name&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.D&& in, STRING, TBD, Commanded filter name&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.E&& in, STRING, loRes10|hiRes10|loRes20, Commanded grating name&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.F&& in, STRING, TBD, Commanded central wavelength&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.G&& in, STRING, TBD, Commanded lens name&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.H&& in, STRING, TBD, Commanded Lyot stop name&&is - Instrument Sequencer - 0
ADD_REC trecs:instrumentSetup.I&& in, STRING, TBD, Commanded sector name&&is - Instrument Sequencer - 0
~is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.J&& in, STRING, TBD, Commanded slit width&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.K&& in, STRING, TBD, Commanded window setting&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.L&& in, STRING, TRUE|FALSE, Override automatic aperture selection&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.M&& in, STRING, TRUE|FALSE, Override automatic filter selection&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.N&& in, STRING, TRUE|FALSE, Override automatic lens selection&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.O&& in, STRING, TRUE|FALSE, Override automatic window selection&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALA&& out, STRING, imaging|spectroscopy, Current camera mode&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALB&& out, STRING, <field|window|pupil>, Current imaging mode&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALC&& out, STRING, [0..1], Optical throughput&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALE&& out, STRING, TBD, Current filter name&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALF&& out, STRING, TBD, Current central wavelength&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALG&& out, STRING, TBD, Current aperture name&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALH&& out, STRING, TBD, Current cold clamp name&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALI&& out, STRING, TBD, Current filter 1 name&&is - Instrument Sequencer - 1
ADD_REC trecs:instrumentSetup.VALJ&& out, STRING, TBD, Current filter 2 name&&is - Instrument Sequencer - 1
~is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALK&& out, STRING, loRes10|hiRes10|loRes20, Current grating name&&is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALL&& out, STRING, TBD, Current Lyot stop name&&is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALM&& out, STRING, TBD, Current pupil name&&is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALN&& out, STRING, TBD, Current sector name&&is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALO&& out, STRING, TBD, Current slit width&&is - Instrument Sequencer - 2
ADD_REC trecs:instrumentSetup.VALP&& out, STRING, TBD, Current window setting&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.A&& in, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.B&& in, STRING, TBD[TBD], Commnaded source photon collectin time&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.C&& in, STRING, <nominal|high>, Commanded flux background setting&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.D&& in, STRING, <nominal|high>, Commanded flux noise setting&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.VALA&& out, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.VALB&& out, STRING, TBD[TBD], Current source photon collecting time&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.VALC&& out, STRING, <nominal|high>, Current flux background setting&&is - Instrument Sequencer - 2
ADD_REC trecs:observationSetup.VALD&& out, STRING, <nominal|high>, Current flux noise setting&&is - Instrument Sequencer - 2
ADD_REC trecs:observe.A&& in, TBD, TBD, Commanded data label&&is - Instrument Sequencer - 2
~is - Instrument Sequencer - 3
ADD_REC trecs:observe.VALA&& out, TBD, TBD, Current data label&&is - Instrument Sequencer - 3
ADD_REC trecs:setDhsInfo.A&& in, STRING, TBD, Commanded DHS quick-look stream ID&&is - Instrument Sequencer - 3
ADD_REC trecs:setDhsInfo.VALA&& out, TBD, TBD, Current DHS quick-look stream ID&&is - Instrument Sequencer - 3
~ec - Environment control and monitoring - temperature and vacuum
ADD_REC trecs:ec:tempMonG.A&& in, STRING, TBA, cold1 upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.B&& in, STRING, TBA, cold2 upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.C&& in, STRING, TBA, active upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.D&& in, STRING, TBA, passive upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.E&& in, STRING, TBA, window upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.F&& in, STRING, TBA, strap upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.G&& in, STRING, TBA, edge upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.H&& in, STRING, TBA, middle upper limit override&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.J&& in, STRING, TBA, input array&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.VALA&& out, STRING, TBA, cold1&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.VALB&& out, STRING, TBA, cold2&&ec - Environment control and monitoring - temperature and vacuum - 0
ADD_REC trecs:ec:tempMonG.VALC&& out, STRING, TBA, active&&ec - Environment control and monitoring - temperature and vacuum - 0
~ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALD&& out, STRING, TBA, passive&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALE&& out, STRING, TBA, window&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALF&& out, STRING, TBA, strap&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALG&& out, STRING, TBA, edge&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALH&& out, STRING, TBA, middle&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALI&& out, STRING, TRUE|FALSE, cold1 OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALJ&& out, STRING, TRUE|FALSE, cold2 OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALK&& out, STRING, TRUE|FALSE, active OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALL&& out, STRING, TRUE|FALSE, passive OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALM&& out, STRING, TRUE|FALSE, window OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALN&& out, STRING, TRUE|FALSE, strap OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALO&& out, STRING, TRUE|FALSE, edge OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:tempMonG.VALP&& out, STRING, TRUE|FALSE, middle OK&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:pressMonG.A&& in, STRING, TBA, pressure upper&&ec - Environment control and monitoring - temperature and vacuum - 1
ADD_REC trecs:ec:pressMonG.J&& in, STRING, TBD, input array&&ec - Environment control and monitoring - temperature and vacuum - 1
~ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:pressMonG.VALA&& out, STRING, TBA, cryostat pressure&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:pressMonG.VALB&& out, STRING, TBA, cryostat pressure OK to cool instrument&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.A&& in, STRING, n/a, set point(from tempSetG.VALC)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.B&& in, STRING, TBA, array upper&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.C&& in, STRING, TBA, array lower&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.D&& in, STRING, TBA, finger upper&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.VALA&& out, STRING, TBA, Detector (current)(to sad:tSDetector.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.VALB&& out, STRING, TBA, finger(to sad:tSFinger.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.VALC&& out, STRING, TBA, Detector OK(to sad:DetectorOK.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:arrayG.VALD&& out, STRING, TBA, finger OK(to sad:FingerOK.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:tempSetG.A&& in, LONG, <0|2|3>, command mode(from ec:simLevelWrite.OUT)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:tempSetG.B&& in, DOUBLE, n/a, set point(from ec:setPointWrite.OUT)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:tempSetG.C&& in, LONG, n/a, heater power(from ec:powerWrite.OUT)&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:tempSetG.D&& in, DOUBLE, n/a, P value&&ec - Environment control and monitoring - temperature and vacuum - 2
ADD_REC trecs:ec:tempSetG.E&& in, DOUBLE, n/a, I value&&ec - Environment control and monitoring - temperature and vacuum - 2
~ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.F&& in, DOUBLE, n/a, D value&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.G&& in, LONG, n/a, auto tune&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.H&& in, STRING, <FULL|MIN|NONE>, n/a(from ec:debug.OUT)&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALA&& out, LONG, <O|2|3>, command mode&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALB&& out, LONG, <-1|Position Integer>, socket number&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALC&& out, STRING, n/a, set point(to ec:arrayG.INPA)&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALD&& out, STRING, n/a, heater power&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALE&& out, STRING, n/a, P value&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALF&& out, STRING, n/a, I value&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALG&& out, STRING, n/a, D value&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:tempSetG.VALH&& out, STRING, n/a, auto tune&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:configG.VALA&& out, STRING, <no defined range>, status gensubs&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:configG.VALB&& out, STRING, <no defined range>, status update&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:configG.VALC&& out, STRING, <no defined range>, heartbeat records&&ec - Environment control and monitoring - temperature and vacuum - 3
ADD_REC trecs:ec:apply.A&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from ec:init.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 3
~ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:apply.B&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from ec:tempSet.VAL)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:apply.VALA&& out, STRING, <START|CLEAR>, n/a(to ec:init.DIR)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:apply.VALB&& out, STRING, <START|CLEAR>, n/a(to ec:tempSet.DIR)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:init.MESS&& out, STRING, <ERROR MESSAGE TBA>, n/a(to ec:apply.INMA)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:init.VAL&& out, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(to ec:apply.INPA)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:init.VALA&& out, STRING, <O|2|3>, n/a(to ec:heartbeatCalc.INPE)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:init.VALB&& out, STRING, <O|2|3>, n/a(to ec:simLevelWrite.DOL)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.A&& in, STRING, n/a, set point&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.B&& in, STRING, n/a, heater power&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.MESS&& out, STRING, <ERROR MESSAGE TBA>, n/a(to ec:apply.INMB)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.VAL&& out, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(to ec:apply.INPB)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.VALA&& out, STRING, n/a, n/a(to ec:setPointWrite.DOL)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.VALB&& out, STRING, n/a, n/a(to ec:powerWrite.DOL)&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.VALG&& out, STRING, n/a, set point&&ec - Environment control and monitoring - temperature and vacuum - 4
ADD_REC trecs:ec:tempSet.VALH&& out, STRING, n/a, heater power&&ec - Environment control and monitoring - temperature and vacuum - 4
~ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:applyC.IVAL&& in, STRING, <0|1|2|3>, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:applyC.IMSS&& in, STRING, <no defined range>, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:applyC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:applyC.OMSS&& out, STRING, TBD, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:debug.DOL&& in, STRING, TBD, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:debug.OUT&& out, STRING, TBD, TBD&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls340Heartbeat.INP&& in, STRING, n/a, n/a&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls340Heartbeat.VAL&& out, STRING, n/a, n/a(to ec:heartbeat.SLNK)&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls218Heartbeat.INP&& in, STRING, n/a, n/a&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls218Heartbeat.VAL&& out, STRING, n/a, n/a(to ec:heartbeat.SLNK)&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls354Heartbeat.INP&& in, STRING, n/a, n/a&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:ls354Heartbeat.VAL&& out, STRING, n/a, n/a(to ec:heartbeat.SLNK)&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:heartbeat.SLNK&& in, STRING, n/a, n/a(from the 3 longins above)&&ec - Environment control and monitoring - temperature and vacuum - 5
ADD_REC trecs:ec:heartbeat.OUT&& out, STRING, n/a, n/a(to sad:ecHeartbeat.VAL&&ec - Environment control and monitoring - temperature and vacuum - 5
~motor - Motor control
~sectWhl - Sector Wheel
ADD_REC trecs:cc:sectWhl:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:test.VAL)&&sectWhl - Sector Wheel - 0
~sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:sectWhl:test.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:init.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:sectWhl:init.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:datum.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:sectWhl:datum.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:namedPos.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:sectWhl:namedPos.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:steps.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INME&& in, STRING, <any string>, n/a(from cc:sectWhl:steps.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:abort.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:sectWhl:abort.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:sectWhl:stop.VAL)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:sectWhl:stop.MESS)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:test.DIR)&&sectWhl - Sector Wheel - 1
ADD_REC trecs:cc:sectWhl:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:init.DIR)&&sectWhl - Sector Wheel - 1
~sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:datum.DIR)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:namedPos.DIR)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:steps.DIR)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:abort.DIR)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:sectWhl:stop.DIR)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:test.DIR&& in, STRING, TBD, TBD&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:test.A&& in, STRING, <1>, test directive&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:test.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMA)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:test.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPA)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:test.VALA&& out, LONG, <1>, n/a(internal)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:init.DIR&& in, STRING, TBD, TBD&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:init.A&& in, STRING, <O|2|3>, init directive&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:init.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMB)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:init.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPB)&&sectWhl - Sector Wheel - 2
ADD_REC trecs:cc:sectWhl:init.VALA&& out, LONG, <O|2|3>, TBD&&sectWhl - Sector Wheel - 2
~sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:datum.DIR&& in, STRING, TBD, TBD&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:datum.A&& in, STRING, n/a, datum directive&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:datum.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMC)&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:datum.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPC)&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:datum.VALA&& out, LONG, n/a, n/a&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.DIR&& in , STRING, TBD, TBD&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.A&& in, STRING, n/a, Position number&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMD)&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPD)&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.VALA&& out, LONG, n/a, n/a&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:sectWhl:Pos.VAL)&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:stop.DIR&& in, STRING, TBD, TBD&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:stop.A&& in, STRING, n/a, stop message&&sectWhl - Sector Wheel - 3
ADD_REC trecs:cc:sectWhl:stop.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INME)&&sectWhl - Sector Wheel - 3
~sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:stop.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPE)&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:stop.VALA&& out, STRING, n/a, n/a&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:abort.DIR&& in, STRING, TBD, TBD&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:abort.A&& in, STRING, n/a, abort message&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:abort.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMF)&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:abort.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPF)&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:abort.VALA&& out, STRING, n/a, n/a&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:steps.DIR&& in, STING, TBD, TBD&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:steps.A&& in, STRING, n/a, steps&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:steps.MESS&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INMG)&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:steps.VAL&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.INPG)&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&sectWhl - Sector Wheel - 4
ADD_REC trecs:cc:sectWhl:motorG.A&& in, LONG, <1>, perform_test&&sectWhl - Sector Wheel - 4
~sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.C&& in, LONG, <0|2|3>, command mode&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.E&& in, LONG, <1>, datum_motor&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.I&& in, STRING, n/a, stop_mess&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.K&& in, STRING, n/a, abort_mess&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.M&& in, STRING, n/a, debug mode&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.T&& in, LONG, n/a, datum_direction&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.VALA&& out, LONG, <0|2|3>, command mode&&sectWhl - Sector Wheel - 5
ADD_REC trecs:cc:sectWhl:motorG.VALB&& out, LONG, n/a, socket number&&sectWhl - Sector Wheel - 5
~sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALC&& out, STRING, n/a, current position&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorC.IMSS&& in, STRING, <no defined range>, n/a&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&sectWhl - Sector Wheel - 6
ADD_REC trecs:cc:sectWhl:motorC.OMSS&& out, STRING, n/a, n/a&&sectWhl - Sector Wheel - 6
~winChngr - Window Changer
ADD_REC trecs:cc:winChngr:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:test.VAL)&&winChngr - Window Changer - 0
ADD_REC trecs:cc:winChngr:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:winChngr:test.MESS)&&winChngr - Window Changer - 0
ADD_REC trecs:cc:winChngr:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:init.VAL)&&winChngr - Window Changer - 0
~winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:winChngr:init.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:datum.VAL)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:winChngr:datum.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:namedPos.VAL)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:winChngr:namedPos.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:steps.VAL)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INME&& in, STRING, <any string>, n/a(from cc:winChngr:steps.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:abort.VAL)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:winChngr:abort.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:winChngr:stop.VAL)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:winChngr:stop.MESS)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:test.DIR)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:init.DIR)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:datum.DIR)&&winChngr - Window Changer - 1
ADD_REC trecs:cc:winChngr:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:namedPos.DIR)&&winChngr - Window Changer - 1
~winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:steps.DIR)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:abort.DIR)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:winChngr:stop.DIR)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:test.DIR&& in, STRING, TBD, TBD&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:test.A&& in, STRING, <1>, test directive&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:test.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMA)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:test.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPA)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:test.VALA&& out, LONG, <1>, n/a(internal)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:init.DIR&& in, STRING, TBD, TBD&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:init.A&& in, STRING, <O|2|3>, init directive&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:init.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMB)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:init.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPB)&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:init.VALA&& out, LONG, <O|2|3>, TBD&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:datum.DIR&& in, STRING, TBD, TBD&&winChngr - Window Changer - 2
ADD_REC trecs:cc:winChngr:datum.A&& in, STRING, n/a, datum directive&&winChngr - Window Changer - 2
~winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:datum.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMC)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:datum.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPC)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:datum.VALA&& out, LONG, n/a, n/a&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.DIR&& in , STRING, TBD, TBD&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.A&& in, STRING, n/a, Position number&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMD)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPD)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.VALA&& out, LONG, n/a, n/a&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.VALB&& out, DOUBLE, n/a, n/a&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:namedPos.VALC&& out, TBD, n/a, Position number(to sad:winChngr:Pos.VAL)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:stop.DIR&& in, STRING, TBD, TBD&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:stop.A&& in, STRING, n/a, stop message&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:stop.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INME)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:stop.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPE)&&winChngr - Window Changer - 3
ADD_REC trecs:cc:winChngr:stop.VALA&& out, STRING, n/a, n/a&&winChngr - Window Changer - 3
~winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:abort.DIR&& in, STRING, TBD, TBD&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:abort.A&& in, STRING, n/a, abort message&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:abort.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMF)&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:abort.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPF)&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:abort.VALA&& out, STRING, n/a, n/a&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:steps.DIR&& in, STING, TBD, TBD&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:steps.A&& in, STRING, n/a, steps&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:steps.MESS&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INMG)&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:steps.VAL&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.INPG)&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:steps.VALA&& out, DOUBLE, n/a, n/a&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:motorG.A&& in, LONG, <1>, perform_test&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:motorG.C&& in, LONG, <0|2|3>, command mode&&winChngr - Window Changer - 4
ADD_REC trecs:cc:winChngr:motorG.E&& in, LONG, <1>, datum_motor&&winChngr - Window Changer - 4
~winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.G&& in, DOUBLE, n/a, num_steps&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.I&& in, STRING, n/a, stop_mess&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.K&& in, STRING, n/a, abort_mess&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.M&& in, STRING, n/a, debug mode&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.N&& in, DOUBLE, n/a, init_velocity&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.O&& in, DOUBLE, n/a, slew_velocity&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.P&& in, DOUBLE, n/a, acceleration&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.Q&& in, DOUBLE, n/a, deceleration&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.R&& in, DOUBLE, n/a, drive_current&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.S&& in, DOUBLE, n/a, datum_speed&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.T&& in, LONG, n/a, datum_direction&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.VALA&& out, LONG, <0|2|3>, command mode&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.VALB&& out, LONG, n/a, socket number&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.VALC&& out, STRING, n/a, current position&&winChngr - Window Changer - 5
ADD_REC trecs:cc:winChngr:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&winChngr - Window Changer - 5
~winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALF&& out, DOUBLE, n/a, acceleration&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALG&& out, DOUBLE, n/a, deceleration&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALH&& out, DOUBLE, n/a, drive_current&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorG.VALJ&& out, LONG, n/a, datum_direction&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorC.IMSS&& in, STRING, <no defined range>, n/a&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&winChngr - Window Changer - 6
ADD_REC trecs:cc:winChngr:motorC.OMSS&& out, STRING, n/a, n/a&&winChngr - Window Changer - 6
~aprtrWhl - Aperture Wheel
ADD_REC trecs:cc:aprtrWhl:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:test.VAL)&&aprtrWhl - Aperture Wheel - 0
ADD_REC trecs:cc:aprtrWhl:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:aprtrWhl:test.MESS)&&aprtrWhl - Aperture Wheel - 0
ADD_REC trecs:cc:aprtrWhl:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:init.VAL)&&aprtrWhl - Aperture Wheel - 0
ADD_REC trecs:cc:aprtrWhl:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:aprtrWhl:init.MESS)&&aprtrWhl - Aperture Wheel - 0
ADD_REC trecs:cc:aprtrWhl:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:datum.VAL)&&aprtrWhl - Aperture Wheel - 0
~aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:aprtrWhl:datum.MESS)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:namedPos.VAL)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:aprtrWhl:namedPos.MESS)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:steps.VAL)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INME&& in, STRING, <any string>, n/a(from cc:aprtrWhl:steps.MESS)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:abort.VAL)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:aprtrWhl:abort.MESS)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:aprtrWhl:stop.VAL)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:aprtrWhl:stop.MESS)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:test.DIR)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:init.DIR)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:datum.DIR)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:namedPos.DIR)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:steps.DIR)&&aprtrWhl - Aperture Wheel - 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:abort.DIR)&&aprtrWhl - Aperture Wheel - 1
~aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:aprtrWhl:stop.DIR)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:test.DIR&& in, STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:test.A&& in, STRING, <1>, test directive&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:test.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMA)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:test.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPA)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:test.VALA&& out, LONG, <1>, n/a(internal)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:init.DIR&& in, STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:init.A&& in, STRING, <O|2|3>, init directive&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:init.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMB)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:init.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPB)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:init.VALA&& out, LONG, <O|2|3>, TBD&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:datum.DIR&& in, STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:datum.A&& in, STRING, n/a, datum directive&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:datum.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMC)&&aprtrWhl - Aperture Wheel - 2
ADD_REC trecs:cc:aprtrWhl:datum.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPC)&&aprtrWhl - Aperture Wheel - 2
~aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:datum.VALA&& out, LONG, n/a, n/a&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.DIR&& in , STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.A&& in, STRING, n/a, Position number&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMD)&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPD)&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.VALA&& out, LONG, n/a, n/a&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:aprtrWhl:Pos.VAL)&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:stop.DIR&& in, STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:stop.A&& in, STRING, n/a, stop message&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:stop.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INME)&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:stop.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPE)&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:stop.VALA&& out, STRING, n/a, n/a&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:abort.DIR&& in, STRING, TBD, TBD&&aprtrWhl - Aperture Wheel - 3
ADD_REC trecs:cc:aprtrWhl:abort.A&& in, STRING, n/a, abort message&&aprtrWhl - Aperture Wheel - 3
~aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:abort.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMF)&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:abort.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPF)&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:abort.VALA&& out, STRING, n/a, n/a&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:steps.DIR&& in, STING, TBD, TBD&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:steps.A&& in, STRING, n/a, steps&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:steps.MESS&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INMG)&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:steps.VAL&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.INPG)&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:motorG.A&& in, LONG, <1>, perform_test&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:motorG.C&& in, LONG, <0|2|3>, command mode&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:motorG.E&& in, LONG, <1>, datum_motor&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&aprtrWhl - Aperture Wheel - 4
ADD_REC trecs:cc:aprtrWhl:motorG.I&& in, STRING, n/a, stop_mess&&aprtrWhl - Aperture Wheel - 4
~aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.K&& in, STRING, n/a, abort_mess&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.M&& in, STRING, n/a, debug mode&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.T&& in, LONG, n/a, datum_direction&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALA&& out, LONG, <0|2|3>, command mode&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALB&& out, LONG, n/a, socket number&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALC&& out, STRING, n/a, current position&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&aprtrWhl - Aperture Wheel - 5
ADD_REC trecs:cc:aprtrWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&aprtrWhl - Aperture Wheel - 5
~aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorC.IMSS&& in, STRING, <no defined range>, n/a&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&aprtrWhl - Aperture Wheel - 6
ADD_REC trecs:cc:aprtrWhl:motorC.OMSS&& out, STRING, n/a, n/a&&aprtrWhl - Aperture Wheel - 6
~fltrwhl1 - Filter Wheel 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:test.VAL)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:fltrwhl1:test.MESS)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:init.VAL)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:fltrwhl1:init.MESS)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:datum.VAL)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:fltrwhl1:datum.MESS)&&fltrwhl1 - Filter Wheel 1 - 0
ADD_REC trecs:cc:fltrwhl1:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:namedPos.VAL)&&fltrwhl1 - Filter Wheel 1 - 0
~fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:fltrwhl1:namedPos.MESS)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:steps.VAL)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INME&& in, STRING, <any string>, n/a(from cc:fltrwhl1:steps.MESS)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:abort.VAL)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:fltrwhl1:abort.MESS)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl1:stop.VAL)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:fltrwhl1:stop.MESS)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:test.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:init.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:datum.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:namedPos.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:steps.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:abort.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl1:stop.DIR)&&fltrwhl1 - Filter Wheel 1 - 1
ADD_REC trecs:cc:fltrwhl1:test.DIR&& in, STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 1
~fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:test.A&& in, STRING, <1>, test directive&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:test.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMA)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:test.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPA)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:test.VALA&& out, LONG, <1>, n/a(internal)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:init.DIR&& in, STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:init.A&& in, STRING, <O|2|3>, init directive&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:init.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMB)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:init.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPB)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:init.VALA&& out, LONG, <O|2|3>, TBD&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:datum.DIR&& in, STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:datum.A&& in, STRING, n/a, datum directive&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:datum.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMC)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:datum.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPC)&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:datum.VALA&& out, LONG, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 2
ADD_REC trecs:cc:fltrwhl1:namedPos.DIR&& in , STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 2
~fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.A&& in, STRING, n/a, Position number&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMD)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPD)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.VALA&& out, LONG, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.VALB&& out, DOUBLE, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:namedPos.VALC&& out, TBD, n/a, Position number(to sad:fltrwhl1:Pos.VAL)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:stop.DIR&& in, STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:stop.A&& in, STRING, n/a, stop message&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:stop.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INME)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:stop.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPE)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:stop.VALA&& out, STRING, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:abort.DIR&& in, STRING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:abort.A&& in, STRING, n/a, abort message&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:abort.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMF)&&fltrwhl1 - Filter Wheel 1 - 3
ADD_REC trecs:cc:fltrwhl1:abort.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPF)&&fltrwhl1 - Filter Wheel 1 - 3
~fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:abort.VALA&& out, STRING, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:steps.DIR&& in, STING, TBD, TBD&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:steps.A&& in, STRING, n/a, steps&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:steps.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INMG)&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:steps.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl1:motorApply.INPG)&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:steps.VALA&& out, DOUBLE, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.A&& in, LONG, <1>, perform_test&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.C&& in, LONG, <0|2|3>, command mode&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.E&& in, LONG, <1>, datum_motor&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.G&& in, DOUBLE, n/a, num_steps&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.I&& in, STRING, n/a, stop_mess&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.K&& in, STRING, n/a, abort_mess&&fltrwhl1 - Filter Wheel 1 - 4
ADD_REC trecs:cc:fltrwhl1:motorG.M&& in, STRING, n/a, debug mode&&fltrwhl1 - Filter Wheel 1 - 4
~fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.N&& in, DOUBLE, n/a, init_velocity&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.O&& in, DOUBLE, n/a, slew_velocity&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.P&& in, DOUBLE, n/a, acceleration&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.Q&& in, DOUBLE, n/a, deceleration&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.R&& in, DOUBLE, n/a, drive_current&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.S&& in, DOUBLE, n/a, datum_speed&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.T&& in, LONG, n/a, datum_direction&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALA&& out, LONG, <0|2|3>, command mode&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALB&& out, LONG, n/a, socket number&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALC&& out, STRING, n/a, current position&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALF&& out, DOUBLE, n/a, acceleration&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALG&& out, DOUBLE, n/a, deceleration&&fltrwhl1 - Filter Wheel 1 - 5
ADD_REC trecs:cc:fltrwhl1:motorG.VALH&& out, DOUBLE, n/a, drive_current&&fltrwhl1 - Filter Wheel 1 - 5
~fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorG.VALJ&& out, LONG, n/a, datum_direction&&fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorC.IMSS&& in, STRING, <no defined range>, n/a&&fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&fltrwhl1 - Filter Wheel 1 - 6
ADD_REC trecs:cc:fltrwhl1:motorC.OMSS&& out, STRING, n/a, n/a&&fltrwhl1 - Filter Wheel 1 - 6
~lyotWhl - Lyot Wheel
ADD_REC trecs:cc:lyotWhl:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:test.VAL)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:lyotWhl:test.MESS)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:init.VAL)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:lyotWhl:init.MESS)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:datum.VAL)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:lyotWhl:datum.MESS)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:namedPos.VAL)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:lyotWhl:namedPos.MESS)&&lyotWhl - Lyot Wheel - 0
ADD_REC trecs:cc:lyotWhl:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:steps.VAL)&&lyotWhl - Lyot Wheel - 0
~lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.INME&& in, STRING, <any string>, n/a(from cc:lyotWhl:steps.MESS)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:abort.VAL)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:lyotWhl:abort.MESS)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:lyotWhl:stop.VAL)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:lyotWhl:stop.MESS)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:test.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:init.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:datum.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:namedPos.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:steps.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:abort.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:lyotWhl:stop.DIR)&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:test.DIR&& in, STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:test.A&& in, STRING, <1>, test directive&&lyotWhl - Lyot Wheel - 1
ADD_REC trecs:cc:lyotWhl:test.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMA)&&lyotWhl - Lyot Wheel - 1
~lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:test.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPA)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:test.VALA&& out, LONG, <1>, n/a(internal)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:init.DIR&& in, STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:init.A&& in, STRING, <O|2|3>, init directive&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:init.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMB)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:init.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPB)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:init.VALA&& out, LONG, <O|2|3>, TBD&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:datum.DIR&& in, STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:datum.A&& in, STRING, n/a, datum directive&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:datum.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMC)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:datum.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPC)&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:datum.VALA&& out, LONG, n/a, n/a&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:namedPos.DIR&& in , STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:namedPos.A&& in, STRING, n/a, Position number&&lyotWhl - Lyot Wheel - 2
ADD_REC trecs:cc:lyotWhl:namedPos.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMD)&&lyotWhl - Lyot Wheel - 2
~lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:namedPos.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPD)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:namedPos.VALA&& out, LONG, n/a, n/a&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:lyotWhl:Pos.VAL)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:stop.DIR&& in, STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:stop.A&& in, STRING, n/a, stop message&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:stop.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INME)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:stop.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPE)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:stop.VALA&& out, STRING, n/a, n/a&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:abort.DIR&& in, STRING, TBD, TBD&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:abort.A&& in, STRING, n/a, abort message&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:abort.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMF)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:abort.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPF)&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:abort.VALA&& out, STRING, n/a, n/a&&lyotWhl - Lyot Wheel - 3
ADD_REC trecs:cc:lyotWhl:steps.DIR&& in, STING, TBD, TBD&&lyotWhl - Lyot Wheel - 3
~lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:steps.A&& in, STRING, n/a, steps&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:steps.MESS&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INMG)&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:steps.VAL&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.INPG)&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.A&& in, LONG, <1>, perform_test&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.C&& in, LONG, <0|2|3>, command mode&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.E&& in, LONG, <1>, datum_motor&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.I&& in, STRING, n/a, stop_mess&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.K&& in, STRING, n/a, abort_mess&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.M&& in, STRING, n/a, debug mode&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&lyotWhl - Lyot Wheel - 4
ADD_REC trecs:cc:lyotWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&lyotWhl - Lyot Wheel - 4
~lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.T&& in, LONG, n/a, datum_direction&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALA&& out, LONG, <0|2|3>, command mode&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALB&& out, LONG, n/a, socket number&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALC&& out, STRING, n/a, current position&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&lyotWhl - Lyot Wheel - 5
ADD_REC trecs:cc:lyotWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&lyotWhl - Lyot Wheel - 5
~lyotWhl - Lyot Wheel - 6
ADD_REC trecs:cc:lyotWhl:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&lyotWhl - Lyot Wheel - 6
ADD_REC trecs:cc:lyotWhl:motorC.IMSS&& in, STRING, <no defined range>, n/a&&lyotWhl - Lyot Wheel - 6
ADD_REC trecs:cc:lyotWhl:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&lyotWhl - Lyot Wheel - 6
ADD_REC trecs:cc:lyotWhl:motorC.OMSS&& out, STRING, n/a, n/a&&lyotWhl - Lyot Wheel - 6
~fltrwhl2 - Filter Wheel 2
ADD_REC trecs:cc:fltrwhl2:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:test.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:fltrwhl2:test.MESS)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:init.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:fltrwhl2:init.MESS)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:datum.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:fltrwhl2:datum.MESS)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:namedPos.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:fltrwhl2:namedPos.MESS)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:steps.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INME&& in, STRING, <any string>, n/a(from cc:fltrwhl2:steps.MESS)&&fltrwhl2 - Filter Wheel 2 - 0
ADD_REC trecs:cc:fltrwhl2:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:abort.VAL)&&fltrwhl2 - Filter Wheel 2 - 0
~fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:fltrwhl2:abort.MESS)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:fltrwhl2:stop.VAL)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:fltrwhl2:stop.MESS)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:test.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:init.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:datum.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:namedPos.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:steps.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:abort.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:fltrwhl2:stop.DIR)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:test.DIR&& in, STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:test.A&& in, STRING, <1>, test directive&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:test.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMA)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:test.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPA)&&fltrwhl2 - Filter Wheel 2 - 1
ADD_REC trecs:cc:fltrwhl2:test.VALA&& out, LONG, <1>, n/a(internal)&&fltrwhl2 - Filter Wheel 2 - 1
~fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:init.DIR&& in, STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:init.A&& in, STRING, <O|2|3>, init directive&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:init.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMB)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:init.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPB)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:init.VALA&& out, LONG, <O|2|3>, TBD&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:datum.DIR&& in, STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:datum.A&& in, STRING, n/a, datum directive&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:datum.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMC)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:datum.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPC)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:datum.VALA&& out, LONG, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:namedPos.DIR&& in , STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:namedPos.A&& in, STRING, n/a, Position number&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:namedPos.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMD)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:namedPos.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPD)&&fltrwhl2 - Filter Wheel 2 - 2
ADD_REC trecs:cc:fltrwhl2:namedPos.VALA&& out, LONG, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 2
~fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:namedPos.VALB&& out, DOUBLE, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:namedPos.VALC&& out, TBD, n/a, Position number(to sad:fltrwhl2:Pos.VAL)&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:stop.DIR&& in, STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:stop.A&& in, STRING, n/a, stop message&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:stop.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INME)&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:stop.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPE)&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:stop.VALA&& out, STRING, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:abort.DIR&& in, STRING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:abort.A&& in, STRING, n/a, abort message&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:abort.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMF)&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:abort.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPF)&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:abort.VALA&& out, STRING, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:steps.DIR&& in, STING, TBD, TBD&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:steps.A&& in, STRING, n/a, steps&&fltrwhl2 - Filter Wheel 2 - 3
ADD_REC trecs:cc:fltrwhl2:steps.MESS&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INMG)&&fltrwhl2 - Filter Wheel 2 - 3
~fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:steps.VAL&& out, STRING, n/a, n/a(to cc:fltrwhl2:motorApply.INPG)&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:steps.VALA&& out, DOUBLE, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.A&& in, LONG, <1>, perform_test&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.C&& in, LONG, <0|2|3>, command mode&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.E&& in, LONG, <1>, datum_motor&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.G&& in, DOUBLE, n/a, num_steps&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.I&& in, STRING, n/a, stop_mess&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.K&& in, STRING, n/a, abort_mess&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.M&& in, STRING, n/a, debug mode&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.N&& in, DOUBLE, n/a, init_velocity&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.O&& in, DOUBLE, n/a, slew_velocity&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.P&& in, DOUBLE, n/a, acceleration&&fltrwhl2 - Filter Wheel 2 - 4
ADD_REC trecs:cc:fltrwhl2:motorG.Q&& in, DOUBLE, n/a, deceleration&&fltrwhl2 - Filter Wheel 2 - 4
~fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.R&& in, DOUBLE, n/a, drive_current&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.S&& in, DOUBLE, n/a, datum_speed&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.T&& in, LONG, n/a, datum_direction&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALA&& out, LONG, <0|2|3>, command mode&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALB&& out, LONG, n/a, socket number&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALC&& out, STRING, n/a, current position&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALF&& out, DOUBLE, n/a, acceleration&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALG&& out, DOUBLE, n/a, deceleration&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALH&& out, DOUBLE, n/a, drive_current&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorG.VALJ&& out, LONG, n/a, datum_direction&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&fltrwhl2 - Filter Wheel 2 - 5
ADD_REC trecs:cc:fltrwhl2:motorC.IMSS&& in, STRING, <no defined range>, n/a&&fltrwhl2 - Filter Wheel 2 - 5
~fltrwhl2 - Filter Wheel 2 - 6
ADD_REC trecs:cc:fltrwhl2:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&fltrwhl2 - Filter Wheel 2 - 6
ADD_REC trecs:cc:fltrwhl2:motorC.OMSS&& out, STRING, n/a, n/a&&fltrwhl2 - Filter Wheel 2 - 6
~pplImg - Pupil Wheel
ADD_REC trecs:cc:pplImg:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:test.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:pplImg:test.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:init.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:pplImg:init.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:datum.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:pplImg:datum.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:namedPos.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:pplImg:namedPos.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:steps.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INME&& in, STRING, <any string>, n/a(from cc:pplImg:steps.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:abort.VAL)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:pplImg:abort.MESS)&&pplImg - Pupil Wheel - 0
ADD_REC trecs:cc:pplImg:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:pplImg:stop.VAL)&&pplImg - Pupil Wheel - 0
~pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:pplImg:stop.MESS)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:test.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:init.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:datum.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:namedPos.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:steps.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:abort.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:pplImg:stop.DIR)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:test.DIR&& in, STRING, TBD, TBD&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:test.A&& in, STRING, <1>, test directive&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:test.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMA)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:test.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPA)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:test.VALA&& out, LONG, <1>, n/a(internal)&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:init.DIR&& in, STRING, TBD, TBD&&pplImg - Pupil Wheel - 1
ADD_REC trecs:cc:pplImg:init.A&& in, STRING, <O|2|3>, init directive&&pplImg - Pupil Wheel - 1
~pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:init.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMB)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:init.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPB)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:init.VALA&& out, LONG, <O|2|3>, TBD&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:datum.DIR&& in, STRING, TBD, TBD&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:datum.A&& in, STRING, n/a, datum directive&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:datum.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMC)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:datum.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPC)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:datum.VALA&& out, LONG, n/a, n/a&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.DIR&& in , STRING, TBD, TBD&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.A&& in, STRING, n/a, Position number&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMD)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPD)&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.VALA&& out, LONG, n/a, n/a&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.VALB&& out, DOUBLE, n/a, n/a&&pplImg - Pupil Wheel - 2
ADD_REC trecs:cc:pplImg:namedPos.VALC&& out, TBD, n/a, Position number(to sad:pplImg:Pos.VAL)&&pplImg - Pupil Wheel - 2
~pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:stop.DIR&& in, STRING, TBD, TBD&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:stop.A&& in, STRING, n/a, stop message&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:stop.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INME)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:stop.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPE)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:stop.VALA&& out, STRING, n/a, n/a&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:abort.DIR&& in, STRING, TBD, TBD&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:abort.A&& in, STRING, n/a, abort message&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:abort.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMF)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:abort.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPF)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:abort.VALA&& out, STRING, n/a, n/a&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:steps.DIR&& in, STING, TBD, TBD&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:steps.A&& in, STRING, n/a, steps&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:steps.MESS&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INMG)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:steps.VAL&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.INPG)&&pplImg - Pupil Wheel - 3
ADD_REC trecs:cc:pplImg:steps.VALA&& out, DOUBLE, n/a, n/a&&pplImg - Pupil Wheel - 3
~pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.A&& in, LONG, <1>, perform_test&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.C&& in, LONG, <0|2|3>, command mode&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.E&& in, LONG, <1>, datum_motor&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.G&& in, DOUBLE, n/a, num_steps&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.I&& in, STRING, n/a, stop_mess&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.K&& in, STRING, n/a, abort_mess&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.M&& in, STRING, n/a, debug mode&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.N&& in, DOUBLE, n/a, init_velocity&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.O&& in, DOUBLE, n/a, slew_velocity&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.P&& in, DOUBLE, n/a, acceleration&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.Q&& in, DOUBLE, n/a, deceleration&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.R&& in, DOUBLE, n/a, drive_current&&pplImg - Pupil Wheel - 4
ADD_REC trecs:cc:pplImg:motorG.S&& in, DOUBLE, n/a, datum_speed&&pplImg - Pupil Wheel - 4
~pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.T&& in, LONG, n/a, datum_direction&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALA&& out, LONG, <0|2|3>, command mode&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALB&& out, LONG, n/a, socket number&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALC&& out, STRING, n/a, current position&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALF&& out, DOUBLE, n/a, acceleration&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALG&& out, DOUBLE, n/a, deceleration&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALH&& out, DOUBLE, n/a, drive_current&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorG.VALJ&& out, LONG, n/a, datum_direction&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorC.IMSS&& in, STRING, <no defined range>, n/a&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&pplImg - Pupil Wheel - 5
ADD_REC trecs:cc:pplImg:motorC.OMSS&& out, STRING, n/a, n/a&&pplImg - Pupil Wheel - 5
~pplImg - Pupil Wheel - 6
~slitWhl - Slit Wheel
ADD_REC trecs:cc:slitWhl:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:test.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:slitWhl:test.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:init.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:slitWhl:init.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:datum.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:slitWhl:datum.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:namedPos.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:slitWhl:namedPos.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:steps.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INME&& in, STRING, <any string>, n/a(from cc:slitWhl:steps.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:abort.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:slitWhl:abort.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:slitWhl:stop.VAL)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:slitWhl:stop.MESS)&&slitWhl - Slit Wheel - 0
ADD_REC trecs:cc:slitWhl:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:test.DIR)&&slitWhl - Slit Wheel - 0
~slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:init.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:datum.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:namedPos.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:steps.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:abort.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:slitWhl:stop.DIR)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:test.DIR&& in, STRING, TBD, TBD&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:test.A&& in, STRING, <1>, test directive&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:test.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMA)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:test.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPA)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:test.VALA&& out, LONG, <1>, n/a(internal)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:init.DIR&& in, STRING, TBD, TBD&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:init.A&& in, STRING, <O|2|3>, init directive&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:init.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMB)&&slitWhl - Slit Wheel - 1
ADD_REC trecs:cc:slitWhl:init.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPB)&&slitWhl - Slit Wheel - 1
~slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:init.VALA&& out, LONG, <O|2|3>, TBD&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:datum.DIR&& in, STRING, TBD, TBD&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:datum.A&& in, STRING, n/a, datum directive&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:datum.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMC)&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:datum.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPC)&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:datum.VALA&& out, LONG, n/a, n/a&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.DIR&& in , STRING, TBD, TBD&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.A&& in, STRING, n/a, Position number&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMD)&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPD)&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.VALA&& out, LONG, n/a, n/a&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:slitWhl:Pos.VAL)&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:stop.DIR&& in, STRING, TBD, TBD&&slitWhl - Slit Wheel - 2
ADD_REC trecs:cc:slitWhl:stop.A&& in, STRING, n/a, stop message&&slitWhl - Slit Wheel - 2
~slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:stop.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INME)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:stop.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPE)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:stop.VALA&& out, STRING, n/a, n/a&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:abort.DIR&& in, STRING, TBD, TBD&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:abort.A&& in, STRING, n/a, abort message&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:abort.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMF)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:abort.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPF)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:abort.VALA&& out, STRING, n/a, n/a&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:steps.DIR&& in, STING, TBD, TBD&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:steps.A&& in, STRING, n/a, steps&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:steps.MESS&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INMG)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:steps.VAL&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.INPG)&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&slitWhl - Slit Wheel - 3
ADD_REC trecs:cc:slitWhl:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&slitWhl - Slit Wheel - 3
~slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.A&& in, LONG, <1>, perform_test&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.C&& in, LONG, <0|2|3>, command mode&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.E&& in, LONG, <1>, datum_motor&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.I&& in, STRING, n/a, stop_mess&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.K&& in, STRING, n/a, abort_mess&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.M&& in, STRING, n/a, debug mode&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.T&& in, LONG, n/a, datum_direction&&slitWhl - Slit Wheel - 4
ADD_REC trecs:cc:slitWhl:motorG.VALA&& out, LONG, <0|2|3>, command mode&&slitWhl - Slit Wheel - 4
~slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALB&& out, LONG, n/a, socket number&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALC&& out, STRING, n/a, current position&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorC.IMSS&& in, STRING, <no defined range>, n/a&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&slitWhl - Slit Wheel - 5
ADD_REC trecs:cc:slitWhl:motorC.OMSS&& out, STRING, n/a, n/a&&slitWhl - Slit Wheel - 5
~grating - Grating Turret
ADD_REC trecs:cc:grating:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:test.VAL)&&grating - Grating Turret - 0
ADD_REC trecs:cc:grating:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:grating:test.MESS)&&grating - Grating Turret - 0
~grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:init.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:grating:init.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:datum.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:grating:datum.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:namedPos.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:grating:namedPos.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:steps.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INME&& in, STRING, <any string>, n/a(from cc:grating:steps.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:abort.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:grating:abort.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:grating:stop.VAL)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:grating:stop.MESS)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:grating:test.DIR)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:grating:init.DIR)&&grating - Grating Turret - 1
ADD_REC trecs:cc:grating:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:grating:datum.DIR)&&grating - Grating Turret - 1
~grating - Grating Turret - 2
ADD_REC trecs:cc:grating:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:grating:namedPos.DIR)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:grating:steps.DIR)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:grating:abort.DIR)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:grating:stop.DIR)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:test.DIR&& in, STRING, TBD, TBD&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:test.A&& in, STRING, <1>, test directive&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:test.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMA)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:test.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPA)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:test.VALA&& out, LONG, <1>, n/a(internal)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:init.DIR&& in, STRING, TBD, TBD&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:init.A&& in, STRING, <O|2|3>, init directive&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:init.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMB)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:init.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPB)&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:init.VALA&& out, LONG, <O|2|3>, TBD&&grating - Grating Turret - 2
ADD_REC trecs:cc:grating:datum.DIR&& in, STRING, TBD, TBD&&grating - Grating Turret - 2
~grating - Grating Turret - 3
ADD_REC trecs:cc:grating:datum.A&& in, STRING, n/a, datum directive&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:datum.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMC)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:datum.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPC)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:datum.VALA&& out, LONG, n/a, n/a&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.DIR&& in , STRING, TBD, TBD&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.A&& in, STRING, n/a, Position number&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMD)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPD)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.VALA&& out, LONG, n/a, n/a&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.VALB&& out, DOUBLE, n/a, n/a&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:namedPos.VALC&& out, TBD, n/a, Position number(to sad:grating:Pos.VAL)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:stop.DIR&& in, STRING, TBD, TBD&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:stop.A&& in, STRING, n/a, stop message&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:stop.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INME)&&grating - Grating Turret - 3
ADD_REC trecs:cc:grating:stop.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPE)&&grating - Grating Turret - 3
~grating - Grating Turret - 4
ADD_REC trecs:cc:grating:stop.VALA&& out, STRING, n/a, n/a&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:abort.DIR&& in, STRING, TBD, TBD&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:abort.A&& in, STRING, n/a, abort message&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:abort.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMF)&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:abort.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPF)&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:abort.VALA&& out, STRING, n/a, n/a&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:steps.DIR&& in, STING, TBD, TBD&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:steps.A&& in, STRING, n/a, steps&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:steps.MESS&& out, STRING, n/a, n/a(to cc:grating:motorApply.INMG)&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:steps.VAL&& out, STRING, n/a, n/a(to cc:grating:motorApply.INPG)&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:steps.VALA&& out, DOUBLE, n/a, n/a&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:motorG.A&& in, LONG, <1>, perform_test&&grating - Grating Turret - 4
ADD_REC trecs:cc:grating:motorG.C&& in, LONG, <0|2|3>, command mode&&grating - Grating Turret - 4
~grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.E&& in, LONG, <1>, datum_motor&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.G&& in, DOUBLE, n/a, num_steps&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.I&& in, STRING, n/a, stop_mess&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.K&& in, STRING, n/a, abort_mess&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.M&& in, STRING, n/a, debug mode&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.N&& in, DOUBLE, n/a, init_velocity&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.O&& in, DOUBLE, n/a, slew_velocity&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.P&& in, DOUBLE, n/a, acceleration&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.Q&& in, DOUBLE, n/a, deceleration&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.R&& in, DOUBLE, n/a, drive_current&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.S&& in, DOUBLE, n/a, datum_speed&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.T&& in, LONG, n/a, datum_direction&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.VALA&& out, LONG, <0|2|3>, command mode&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.VALB&& out, LONG, n/a, socket number&&grating - Grating Turret - 5
ADD_REC trecs:cc:grating:motorG.VALC&& out, STRING, n/a, current position&&grating - Grating Turret - 5
~grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALF&& out, DOUBLE, n/a, acceleration&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALG&& out, DOUBLE, n/a, deceleration&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALH&& out, DOUBLE, n/a, drive_current&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorG.VALJ&& out, LONG, n/a, datum_direction&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorC.IMSS&& in, STRING, <no defined range>, n/a&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&grating - Grating Turret - 6
ADD_REC trecs:cc:grating:motorC.OMSS&& out, STRING, n/a, n/a&&grating - Grating Turret - 6
~coldClmp - Cold Clamp
ADD_REC trecs:cc:coldClmp:motorApply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:test.VAL)&&coldClmp - Cold Clamp - 0
ADD_REC trecs:cc:coldClmp:motorApply.INMA&& in, STRING, <any string>, n/a(from cc:coldClmp:test.MESS)&&coldClmp - Cold Clamp - 0
ADD_REC trecs:cc:coldClmp:motorApply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:init.VAL)&&coldClmp - Cold Clamp - 0
ADD_REC trecs:cc:coldClmp:motorApply.INMB&& in, STRING, <any string>, n/a(from cc:coldClmp:init.MESS)&&coldClmp - Cold Clamp - 0
~coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:datum.VAL)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INMC&& in, STRING, <any string>, n/a(from cc:coldClmp:datum.MESS)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:namedPos.VAL)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INMD&& in, STRING, <any string>, n/a(from cc:coldClmp:namedPos.MESS)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:steps.VAL)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INME&& in, STRING, <any string>, n/a(from cc:coldClmp:steps.MESS)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:abort.VAL)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INMF&& in, STRING, <any string>, n/a(from cc:coldClmp:abort.MESS)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:coldClmp:stop.VAL)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.INMG&& in, STRING, <any string>, n/a(from cc:coldClmp:stop.MESS)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:test.DIR)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:init.DIR)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:datum.DIR)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:namedPos.DIR)&&coldClmp - Cold Clamp - 1
ADD_REC trecs:cc:coldClmp:motorApply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:steps.DIR)&&coldClmp - Cold Clamp - 1
~coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:motorApply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:abort.DIR)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:motorApply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:coldClmp:stop.DIR)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:test.DIR&& in, STRING, TBD, TBD&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:test.A&& in, STRING, <1>, test directive&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:test.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMA)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:test.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPA)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:test.VALA&& out, LONG, <1>, n/a(internal)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:init.DIR&& in, STRING, TBD, TBD&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:init.A&& in, STRING, <O|2|3>, init directive&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:init.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMB)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:init.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPB)&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:init.VALA&& out, LONG, <O|2|3>, TBD&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:datum.DIR&& in, STRING, TBD, TBD&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:datum.A&& in, STRING, n/a, datum directive&&coldClmp - Cold Clamp - 2
ADD_REC trecs:cc:coldClmp:datum.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMC)&&coldClmp - Cold Clamp - 2
~coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:datum.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPC)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:datum.VALA&& out, LONG, n/a, n/a&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.DIR&& in , STRING, TBD, TBD&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.A&& in, STRING, n/a, Position number&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMD)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPD)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.VALA&& out, LONG, n/a, n/a&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.VALB&& out, DOUBLE, n/a, n/a&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:namedPos.VALC&& out, TBD, n/a, Position number(to sad:coldClmp:Pos.VAL)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:stop.DIR&& in, STRING, TBD, TBD&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:stop.A&& in, STRING, n/a, stop message&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:stop.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INME)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:stop.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPE)&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:stop.VALA&& out, STRING, n/a, n/a&&coldClmp - Cold Clamp - 3
ADD_REC trecs:cc:coldClmp:abort.DIR&& in, STRING, TBD, TBD&&coldClmp - Cold Clamp - 3
~coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:abort.A&& in, STRING, n/a, abort message&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:abort.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMF)&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:abort.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPF)&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:abort.VALA&& out, STRING, n/a, n/a&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:steps.DIR&& in, STING, TBD, TBD&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:steps.A&& in, STRING, n/a, steps&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:steps.MESS&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INMG)&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:steps.VAL&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.INPG)&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:steps.VALA&& out, DOUBLE, n/a, n/a&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:debug.DOL&& in, STRING, <FULL|NONE|MIN>, n/a&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:debug.OUT&& out, STRING, <FULL|NONE|MIN>, n/a&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:motorG.A&& in, LONG, <1>, perform_test&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:motorG.C&& in, LONG, <0|2|3>, command mode&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:motorG.E&& in, LONG, <1>, datum_motor&&coldClmp - Cold Clamp - 4
ADD_REC trecs:cc:coldClmp:motorG.G&& in, DOUBLE, n/a, num_steps&&coldClmp - Cold Clamp - 4
~coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.I&& in, STRING, n/a, stop_mess&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.K&& in, STRING, n/a, abort_mess&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.M&& in, STRING, n/a, debug mode&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.N&& in, DOUBLE, n/a, init_velocity&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.O&& in, DOUBLE, n/a, slew_velocity&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.P&& in, DOUBLE, n/a, acceleration&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.Q&& in, DOUBLE, n/a, deceleration&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.R&& in, DOUBLE, n/a, drive_current&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.S&& in, DOUBLE, n/a, datum_speed&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.T&& in, LONG, n/a, datum_direction&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.VALA&& out, LONG, <0|2|3>, command mode&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.VALB&& out, LONG, n/a, socket number&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.VALC&& out, STRING, n/a, current position&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&coldClmp - Cold Clamp - 5
ADD_REC trecs:cc:coldClmp:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&coldClmp - Cold Clamp - 5
~coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorG.VALF&& out, DOUBLE, n/a, acceleration&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorG.VALG&& out, DOUBLE, n/a, deceleration&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorG.VALH&& out, DOUBLE, n/a, drive_current&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorG.VALJ&& out, LONG, n/a, datum_direction&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorC.IVAL&& in, STRING, <0|1|2|3>, n/a&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorC.IMSS&& in, STRING, <no defined range>, n/a&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorC.VAL&& out, STRING, <IDLE|BUSY|PAUSED|ERR>, n/a&&coldClmp - Cold Clamp - 6
ADD_REC trecs:cc:coldClmp:motorC.OMSS&& out, STRING, n/a, n/a&&coldClmp - Cold Clamp - 6
~cc - Components Controller
ADD_REC trecs:cc:apply.INPA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:test.VAL)&&cc - Components Controller - 0
ADD_REC trecs:cc:apply.INMA&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:test.MESS)&&cc - Components Controller - 0
ADD_REC trecs:cc:apply.INPB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:init.VAL)&&cc - Components Controller - 0
ADD_REC trecs:cc:apply.INMB&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:init.MESS)&&cc - Components Controller - 0
ADD_REC trecs:cc:apply.INPC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:park.VAL)&&cc - Components Controller - 0
ADD_REC trecs:cc:apply.INMC&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:park.MESS)&&cc - Components Controller - 0
~cc - Components Controller - 1
ADD_REC trecs:cc:apply.INPD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:datum.VAL)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INMD&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:datum.MESS)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INPE&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:abort.VAL)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INME&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:abort.MESS)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INPF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:stop.VAL)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INMF&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:stop.MESS)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INPG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:apply.VAL)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.INMG&& in, STRING, <CAD.ACCEPT|CAD.REJECT>, n/a(from cc:apply.MESS)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALA&& out, STRING, <START|CLEAR>, n/a(to cc:test.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALB&& out, STRING, <START|CLEAR>, n/a(to cc:init.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALC&& out, STRING, <START|CLEAR>, n/a(to cc:park.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALD&& out, STRING, <START|CLEAR>, n/a(to cc:datum.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALE&& out, STRING, <START|CLEAR>, n/a(to cc:abort.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALF&& out, STRING, <START|CLEAR>, n/a(to cc:stop.DIR)&&cc - Components Controller - 1
ADD_REC trecs:cc:apply.VALG&& out, STRING, <START|CLEAR>, n/a(to cc:applyCMD.DIR)&&cc - Components Controller - 1
~cc - Components Controller - 2
ADD_REC trecs:cc:abort.A&& in, STRING, n/a, abort message&&cc - Components Controller - 2
ADD_REC trecs:cc:abort.DIR&& in, STRING, n/a, n/a(from cc:abort.VALE)&&cc - Components Controller - 2
ADD_REC trecs:cc:abort.VAL&& out, STRING, n/a, n/a(to cc:apply.INPE)&&cc - Components Controller - 2
ADD_REC trecs:cc:abort.MESS&& out, STRING, n/a, n/a(to cc:apply, INME)&&cc - Components Controller - 2
ADD_REC trecs:cc:abort.VALA&& out, STRING, n/a, abort message(to cc:abortS.A)&&cc - Components Controller - 2
ADD_REC trecs:cc:abort.STLK&& out, STRING, n/a, n/a(to cc:abortS.SLNK)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.J&& in, STRING, n/a, debug level&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALA&& out, STRING, n/a, debug level(to cc:sectWhl:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALB&& out, STRING, n/a, debug level(to cc:winChngr:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALC&& out, STRING, n/a, debug level(to cc:aprtrWhl:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALD&& out, STRING, n/a, debug level(to cc:fltrWhl1:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALE&& out, STRING, n/a, debug level(to cc:lyotWhl:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALF&& out, STRING, n/a, debug level(to cc:fltrWhl2:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALG&& out, STRING, n/a, debug level(to cc:pplImg:debug.VAL)&&cc - Components Controller - 2
ADD_REC trecs:cc:debug.VALH&& out, STRING, n/a, debug level(to cc:slitWhl:debug.VAL)&&cc - Components Controller - 2
~cc - Components Controller - 3
ADD_REC trecs:cc:debug.VALI&& out, STRING, n/a, debug level(to cc:grating:debug.VAL)&&cc - Components Controller - 3
ADD_REC trecs:cc:debug.VALJ&& out, STRING, n/a, debug level(to cc:coldClmp:debug.VAL)&&cc - Components Controller - 3
ADD_REC trecs:cc:init.DIR&& in, STRING, n/a, n/a(from cc:apply.VALB&&cc - Components Controller - 3
ADD_REC trecs:cc:init.A&& in, STRING, n/a, init directive&&cc - Components Controller - 3
ADD_REC trecs:cc:init.VAL&& out, STRING, n/a, n/a(to cc:apply.INPB)&&cc - Components Controller - 3
ADD_REC trecs:cc:init.MESS&& out, STRING, n/a, n/a(to cc:apply.INMB)&&cc - Components Controller - 3
ADD_REC trecs:cc:init.VALA&& out, STRING, n/a, init directive(to cc:initS.A)&&cc - Components Controller - 3
ADD_REC trecs:cc:init.VALB&& out, STRING, n/a, TBD(to cc:HeartbeatCALC.SDIS)&&cc - Components Controller - 3
ADD_REC trecs:cc:init.STLK&& out, STRING, n/a, n/a(to cc:initS.SLNK)&&cc - Components Controller - 3
ADD_REC trecs:cc:park.DIR&& in, STRING, n/a, n/a(from cc:apply.VALC)&&cc - Components Controller - 3
ADD_REC trecs:cc:park.A&& in, STRING, n/a, park directive&&cc - Components Controller - 3
ADD_REC trecs:cc:park.VAL&& out, STRING, n/a, n/a(to cc:apply.INPC)&&cc - Components Controller - 3
ADD_REC trecs:cc:park.MESS&& out, STRING, n/a, n/a(to cc:apply.INMC)&&cc - Components Controller - 3
ADD_REC trecs:cc:park.VALA&& out, STRING, n/a, n/a(to cc:parkS.A)&&cc - Components Controller - 3
ADD_REC trecs:cc:park.STLK&& out, STRING, n/a, n/a(to cc:parkS.SLNK)&&cc - Components Controller - 3
~cc - Components Controller - 4
ADD_REC trecs:cc:stop.DIR&& in, STRING, n/a, n/a(from cc:apply.VALF)&&cc - Components Controller - 4
ADD_REC trecs:cc:stop.A&& in, STRING, n/a, stop message(to cc:parkS.A)&&cc - Components Controller - 4
ADD_REC trecs:cc:stop.VAL&& out, STRING, n/a, n/a(to cc:apply.INPF)&&cc - Components Controller - 4
ADD_REC trecs:cc:stop.MESS&& out, STRING, n/a, n/a(to cc:apply, INMF)&&cc - Components Controller - 4
ADD_REC trecs:cc:stop.VALA&& out, STRING, n/a, stop mess&&cc - Components Controller - 4
ADD_REC trecs:cc:stop.STLK&& out, STRING, n/a, n/a(to cc:stopS.SLNK)&&cc - Components Controller - 4
ADD_REC trecs:cc:test.DIR&& in, STRING, n/a, n/a(from cc:apply.VALA)&&cc - Components Controller - 4
ADD_REC trecs:cc:test.A&& in, STRING, n/a, test directive&&cc - Components Controller - 4
ADD_REC trecs:cc:test.VAL&& out, STRING, n/a, n/a(to cc:apply.INPA)&&cc - Components Controller - 4
ADD_REC trecs:cc:test.MESS&& out, STRING, n/a, n/a(to cc:apply.INMA)&&cc - Components Controller - 4
ADD_REC trecs:cc:test.STLK&& out, STRING, n/a, n/a(to cc:testS.SLNK)&&cc - Components Controller - 4
ADD_REC trecs:cc:applyCMD.DIR&& in, STRING, n/a, n/a(from cc:apply.VALG)&&cc - Components Controller - 4
ADD_REC trecs:cc:applyCMD.INPA&& in, STRING, n/a, n/a(from cc:sectWhl:motorApply.VAL)&&cc - Components Controller - 4
ADD_REC trecs:cc:applyCMD.INMA&& in, STRING, n/a, n/a(from cc:sectWhl:motorApply.MESS)&&cc - Components Controller - 4
ADD_REC trecs:cc:applyCMD.INPB&& in, STRING, n/a, n/a(from cc:winChngr:motorApply.VAL)&&cc - Components Controller - 4
~cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMB&& in, STRING, n/a, n/a(from cc:winChngr:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPC&& in, STRING, n/a, n/a(from cc:aprtrWhl:motorApply.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMC&& in, STRING, n/a, n/a(from cc:aprtrWhl:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPD&& in, STRING, n/a, n/a(from cc:fltrWhl1:motorApply.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMD&& in, STRING, n/a, n/a(from cc:fltrWhl1:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPE&& in, STRING, n/a, n/a(from cc:lyotWhl:motorApply.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INME&& in, STRING, n/a, n/a(from cc:lyotWhl:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPF&& in, STRING, n/a, n/a(from cc:fltrWhl2:motorApply.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMF&& in, STRING, n/a, n/a(from cc:fltrWhl2:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPG&& in, STRING, n/a, n/a(from cc:pplImg:motorApply.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMG&& in, STRING, n/a, n/a(from cc:pplImg:motorApply.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INPH&& in, STRING, n/a, n/a(from cc:applyCMD2.VAL)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.INMH&& in, STRING, n/a, n/a(from cc:applyCMD2.MESS)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.VAL&& out, STRING, n/a, n/a(to cc:apply.INPG)&&cc - Components Controller - 5
ADD_REC trecs:cc:applyCMD.MESS&& out, STRING, n/a, n/a(to cc:apply.INMG)&&cc - Components Controller - 5
~cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALA&& out, STRING, n/a, n/a(to cc:sectWhl:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALB&& out, STRING, n/a, n/a(to cc:winChngr:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALC&& out, STRING, n/a, n/a(to cc:aprtrWhl:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALD&& out, STRING, n/a, n/a(to cc:fltrWhl1:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALE&& out, STRING, n/a, n/a(to cc:lyotWhl:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALF&& out, STRING, n/a, n/a(to cc:fltrWhl2:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALG&& out, STRING, n/a, n/a(to cc:pplImg:motorApply.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD.VALH&& out, STRING, n/a, n/a(to cc:applyCMD2.DIR)&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.DIR&& in, STRING, n/a, n/a(from cc:applyCMD.VALH&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INPA&& in, STRING, n/a, n/a(from cc:slitWhl:motorApply.VAL&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INMA&& in, STRING, n/a, n/a(from cc:slitWhl:motorApply.MESS&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INPB&& in, STRING, n/a, n/a(from cc:grating:motorApply.VAL&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INMB&& in, STRING, n/a, n/a(from cc:grating:motorApply.MESS&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INPC&& in, STRING, n/a, n/a(from cc:coldClmp:motorApply.VAL&&cc - Components Controller - 6
ADD_REC trecs:cc:applyCMD2.INMC&& in, STRING, n/a, n/a(from cc:coldClmp:motorApply.MESS&&cc - Components Controller - 6
~cc - Components Controller - 7
ADD_REC trecs:cc:applyCMD2.VAL&& out, STRING, n/a, n/a(to cc:applyCMD.INPH)&&cc - Components Controller - 7
ADD_REC trecs:cc:applyCMD2.MESS&& out, STRING, n/a, n/a(to cc:applyCMD.INMH)&&cc - Components Controller - 7
ADD_REC trecs:cc:applyCMD2.VALA&& out, STRING, n/a, n/a(to cc:slitWhl:motorApply.DIR&&cc - Components Controller - 7
ADD_REC trecs:cc:applyCMD2.VALB&& out, STRING, n/a, n/a(to cc:grating:motorApply.DIR&&cc - Components Controller - 7
ADD_REC trecs:cc:applyCMD2.VALC&& out, STRING, n/a, n/a(to cc:coldClmp:motorApply.DIR&&cc - Components Controller - 7
ADD_REC trecs:cc:datum.DIR&& in, STRING, n/a, n/a(from cc:apply.VALE)&&cc - Components Controller - 7
ADD_REC trecs:cc:datum.A&& in, STRING, n/a, datum directive&&cc - Components Controller - 7
ADD_REC trecs:cc:datum.VAL&& out, STRING, n/a, n/a(to cc:apply.INPD)&&cc - Components Controller - 7
ADD_REC trecs:cc:datum.MESS&& out, STRING, n/a, n/a(to cc:apply.INMD)&&cc - Components Controller - 7
ADD_REC trecs:cc:datum.STLK&& out, STRING, n/a, n/a(to cc:datumS.SLNK)&&cc - Components Controller - 7
ADD_REC trecs:cc:applyC.IVAL&& in, STRING, n/a, n/a(from cc:MrgCarG.VALB)&&cc - Components Controller - 7
ADD_REC trecs:cc:applyC.IMSS&& in, STRING, n/a, n/a(from cc:MrgCarG.VALA)&&cc - Components Controller - 7
ADD_REC trecs:cc:applyC.VAL&& out, STRING, n/a, n/a&&cc - Components Controller - 7
~Status Records
~Observing Parameters
ADD_REC trecs:sad:background&& String, -, BKND, Start, Current background flux condition: "nominal" || "high"&&Observing Parameters - 0
ADD_REC trecs:sad:cameraMode&& String, -, CAMMODE, Start, Current Camera Mode: "imaging" || "spectroscopy"&&Observing Parameters - 0
~Observing Parameters - 1
ADD_REC trecs:sad:dataMode&& String, -, -, -, Current Data Mode: "save" || "discard"&&Observing Parameters - 1
ADD_REC trecs:sad:obsID&& String, -, OBSID, Start, Current observation ID provided by OCS&&Observing Parameters - 1
ADD_REC trecs:sad:obsMode&& String, -, OBSMODE, Start, Current Observing Mode: "chop=nod" || "stare" || "chop" || "nod"&&Observing Parameters - 1
ADD_REC trecs:sad:obsType&& String, -, OBSTYPE, Start, Observation type (e.g. "image", "spectrum", "dark", "flat", ...)&&Observing Parameters - 1
ADD_REC trecs:sad:onSourceTime&& Float, Minutes, ONSRCTM, Start, Current total on-source exposure time&&Observing Parameters - 1
ADD_REC trecs:sad:photonTime&& Float, Minutes, PHOTONTM, Start, Current total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTime since only half the time is spent on-source.  In stare mode, this time is equal to onSourceTime&&Observing Parameters - 1
~Acquisition Parameters
ADD_REC trecs:sad:chopFreq&& Float, Hz, CHPFREQ, Start, Current secondary chopping frequency&&Acquisition Parameters - 0
ADD_REC trecs:sad:chopDelay&& Float, mS, CHPDDLY, Start, Current chopper "dead" time allowing for chopper transition.  This value is always greater than or equal to the requested chop delay&&Acquisition Parameters - 0
ADD_REC trecs:sad:frameTime&& Float, mS, FRMTM, Start, Current detector readout frame integration time&&Acquisition Parameters - 0
ADD_REC trecs:sad:nodDelay&& Float, Seconds, NODDLY, Start, Current "dead" time allowing for telescope nod.  This value is always greater than or equal to the requested nod delay&&Acquisition Parameters - 0
ADD_REC trecs:sad:nodDwell&& Float, Seconds, NODDWELL, Start, Current dwell time in each telescope beam&&Acquisition Parameters - 0
ADD_REC trecs:sad:saveFreq&& Float, Hz, SVFREQ, Start, Current save frequency&&Acquisition Parameters - 0
~Optical Configuration
ADD_REC trecs:sad:apertureName&& String, -, APERTURE, Start, Aperture wheel position name (mnemonic)&&Optical Configuration - 0
ADD_REC trecs:sad:filterName&& String, -, FILTER, Start, Filter wheel position name (mnemonic)&&Optical Configuration - 0
ADD_REC trecs:sad:gratingName&& String, -, GRATING, Start, Grating wheel position name (mnemonic)&&Optical Configuration - 0
~Optical Configuration - 1
ADD_REC trecs:sad:imagingMode&& String, -, IMGMODE, Start, Current Imaging Mode: "field" || "window" || "pupil"&&Optical Configuration - 1
ADD_REC trecs:sad:lensName&& String, -, LENS, Start, Lens wheel position name (mnemonic)&&Optical Configuration - 1
ADD_REC trecs:sad:lyotName&& String, -, LYOT, Start, Lyot wheel position name (mnemonic)&&Optical Configuration - 1
ADD_REC trecs:sad:sectorName&& String, -, SECTOR, Start, Sector wheel position name (mnemonic)&&Optical Configuration - 1
ADD_REC trecs:sad:slitName&& String, -, SLIT, Start, Slit wheel position name (mnemonic)&&Optical Configuration - 1
ADD_REC trecs:sad:windowName&& String, -, WINDOW, Start, Window wheel position name (mnemonic)&&Optical Configuration - 1
~Observation Progress
ADD_REC trecs:sad:backgroundAdcCurrent&& Integer, ADUs, -, , Current filling of detector well due to background in ADUs&&Observation Progress - 0
ADD_REC trecs:sad:backgroundAdcEnd&& Integer, ADUs, BKAEND, End, Filling of detector well due to background at end of observation in&&Observation Progress - 0
ADD_REC trecs:sad:backgroundAdcStart&& Integer, ADUs, BKASTART, Start, Filling of detector well due to background at beginning of observation in ADUs&&Observation Progress - 0
ADD_REC trecs:sad:backgroundWellCurrent&& Float, Percent, -, -, Current filling of detector well due to background (where 0% is empty well and 100% is full well)&&Observation Progress - 0
ADD_REC trecs:sad:backgroundWellEnd&& Float, Percent, BKWEND, End, Filling of detector well due to background at end of observation (where 0% is empty well and 100% is full well)&&Observation Progress - 0
ADD_REC trecs:sad:backgroundWellStart&& Float, Percent, BKWSTART, Start, Filling of detector well due to background at beginning of observation (where 0% is empty well and 100% is full well)&&Observation Progress - 0
ADD_REC trecs:sad:currentBeam&& String, -, -, -, Contains the current beam position of the telescope&&Observation Progress - 0
ADD_REC trecs:sad:currentNodCycle&& Integer, Cycles, -, -, Contains the current nod cycle for an ongoing observation&&Observation Progress - 0
ADD_REC trecs:sad:observationStatus&& String, -, -, -, Status of observation sequence&&Observation Progress - 0
~Observation Progress - 1
ADD_REC trecs:sad:sigmaPerFrameRead&& Float, ADUs, SIGFRM, End, Current sigma per frame readout in units of ADUs&&Observation Progress - 1
~Instrument status       
ADD_REC trecs:sad:detTempOK&& Boolean, -, -, -, Flag indicates whether the detector temperature has stabilized at the temperature controller setpoint (i.e. heaterSetpoint) to within the acceptable error band&&Instrument status        - 0
ADD_REC trecs:sad:health&& String, -, INHEALTH, Start, Instrument health&&Instrument status        - 0
ADD_REC trecs:sad:heartbeat&& Integer, -, -, -, Continuously changing variable used to detect whether the system is still alive&&Instrument status        - 0
ADD_REC trecs:sad:historyLog&& String, -, -, -, Detector Controller message log&&Instrument status        - 0
ADD_REC trecs:sad:issPort&& Integer, -, INPORT, Start, Number of ISS port on which the instrument is installed&&Instrument status        - 0
ADD_REC trecs:sad:name&& String, -, INSTRUME, Start, Instrument Name&&Instrument status        - 0
ADD_REC trecs:sad:state&& String, -, INSTATE, Start, Instrument State&&Instrument status        - 0
~Optical Configuration Defaults
ADD_REC trecs:sad:loRes10Blocker&& String, -, -, -, filterWheelPosition name of blocker for loRes10 spectroscopy mode&&Optical Configuration Defaults - 0
ADD_REC trecs:sad:hiRes10Blocker&& String, -, -, -, filterWheelPosition name of blocker for hiRes10 spectroscopy mode&&Optical Configuration Defaults - 0
ADD_REC trecs:sad:loRes10Blocker&& String, -, -, -, filterWheelPosition name of blocker for loRes20 spectroscopy mode&&Optical Configuration Defaults - 0
~Cryostat Window Parameters
ADD_REC trecs:sad:kBrHumidityLimit&& Float, Percent, -, -, Indicates maximum relative humidity permitted at the dewar window for which the KBr window can be used&&Cryostat Window Parameters - 0
ADD_REC trecs:sad:windowRelativeHumidity&& Float, Percent, -, -, Indicates the relative humidity corresponding to the cryostat window temperature (i.e. TSWindow) calculated using the dome environment relative humidity and temperature&&Cryostat Window Parameters - 0
~World Coordinate System Information
ADD_REC trecs:sad:wcsDec&& Double, Radians, -, -, Dec of the center of projection of detector surface&&World Coordinate System Information - 0
ADD_REC trecs:sad:wcsRA&& Double, Radians, -, -, RA of the center of projection of detector surface&&World Coordinate System Information - 0
~World Coordinate System Information - 1
ADD_REC trecs:sad:wcsTai&& Double, Gemini raw units , -, -, ??? &&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs11&& Double, mm, -, -, World Coordinate System transformation information. The 6 coefficients which define the transformation from sky coordinates into (X, Y) coordinates at the detector surface.  See tcs-ptw-008&&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs12&& Double, mm, -, -, &&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs13&& Double, mm, -, -, &&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs21&& Double, mm, -, -, &&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs22&& Double, mm, -, -, &&World Coordinate System Information - 1
ADD_REC trecs:sad:wcs23&& Double, mm, -, -, &&World Coordinate System Information - 1
~Miscellaneous Instrument Sequencer status information
ADD_REC trecs:sad:configurationTimeout&& Float, Seconds, -, -, Timeout value for instrument reconfiguration time&&Miscellaneous Instrument Sequencer status information - 0
~Detector configuration
ADD_REC trecs:sad:chopCoaddUc&& Integer, Chop cycles, CHPCOADD, Start, Number of chop cycles to coadd in hardware coadders before saving pair of chopped image buffers (i.e. a saveset) for permanent data archive.  This parameter is only used in "chop-nod" and "chop" mode&&Detector configuration - 0
ADD_REC trecs:sad:chopSettleUf&& Integer, Frame readouts, CHPSTTL, End, Number of frames to discard to allow for secondary chopper to settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the SCS.  This parameter is only used in "chop-nod" or "chop" mode&&Detector configuration - 0
ADD_REC trecs:sad:frameCoaddUf&& Integer, Frame readouts, FRMCOADD, Start, Number of Frames to coadd in hardware coadders: (a) per half-chop cycle in "chop" or "chop-nod" mode; (b) per saveset in "stare" or "nod"&&Detector configuration - 0
ADD_REC trecs:sad:nodSets&& Integer, -, NODSETS, End, Number of nod cycles per observation.  This parameter is only used in "chop-nod" or "nod" mode&&Detector configuration - 0
ADD_REC trecs:sad:nodSettleUc&& Integer, Chop cycles, NODSTTLC, End, Number of chop cycles to discard to allow time for telescope to complete beamswitch and settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the OCS.  This parameter is only used in "chop-nod" mode&&Detector configuration - 0
ADD_REC trecs:sad:nodSettleUF&& Integer, Frame readouts, NODSTTLF, End, Number of readout frames to discard to allow time for telescope to complete beamswitch and settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the OCS.  This parameter is only used in "nod" mode&&Detector configuration - 0
ADD_REC trecs:sad:pixelClocks&& Integer, -, PIXCLKS, Start, Number of hardware clocks per array pixel readout.  Because the frame readout time is an integral multiple of the array clocks, only discrete values of the frame readout time are obtainable&&Detector configuration - 0
~Detector configuration - 1
ADD_REC trecs:sad:postChopsUc&& Integer, Chop cycles, POSTCHPS, End, Number of chop cycles to discard at the termination of an observation to allow the secondary chopper to stop gracefully.  This parameter depends on the SCS and may not be required for Gemini&&Detector configuration - 1
ADD_REC trecs:sad:preChopsUc&& Integer, Chop cycles, PRECHPS, End, Number of chop cycles to discard at the initiation of an observation to allow the secondary chopper to stabilize.  This parameter depends on the SCS and may not be required for Gemini&&Detector configuration - 1
ADD_REC trecs:sad:saveSets&& Integer, -, SAVESETS, Start, Number of image buffers (or image buffer pairs if chopping) to save: (a) per beam position of the telescope in "chop-nod" or "nod" modes; (b) per observation in "chop" or "stare" mode&&Detector configuration - 1
ADD_REC trecs:sad:readoutMode&& Enumer-ation, -, DETMODE, Start, Detector readout mode: either single sample destructive read normally used in broadband imaging mode or a non-destructive read mode (in which multiple samples up the integration ramp are taken before detector reset) used in low flux conditions&&Detector configuration - 1
~Readout status
ADD_REC trecs:sad:acq&& Boolean, -, -, -, Flag to indicate that the Detector Controller is acquiring data.  While data is being acquired, both ACQ and RDOUT will be TRUE.  During a CHOP-NOD or NOD mode observation, RDOUT will go FALSE while ACQ remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, RDOUT will be reset to TRUE.  When an observation completes, both ACQ and RDOUT will be reset to FALSE&&Readout status - 0
ADD_REC trecs:sad:backgroundCheckFlag&& Boolean, -, -, -, Flag to indicate whether background flux is filling detector wells within acceptable limits (i.e. minDetWell and maxDetWell).&&Readout status - 0
ADD_REC trecs:sad:beamswitchHandshake&& Boolean, -, -, -, Indicates whether or not beamswitch handshaking is in effect&&Readout status - 0
ADD_REC trecs:sad:bunit&& String, ADU, BUNIT, Start, Units of quantity read from detector array&&Readout status - 0
ADD_REC trecs:sad:dataLabel&& String, , DHSLABEL, Start, Most recent DHS data label for data to be downloaded to the DHS&&Readout status - 0
ADD_REC trecs:sad:elapsed&& Float, Seconds, ELAPSED, End, Total elapsed time.  This is equal to photonTimeActual divided by photonEfficiency&&Readout status - 0
ADD_REC trecs:sad:exposed&& Float, Seconds, EXPTIME, End, Actual total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTimeActual since only half the time is spent on-source.  In stare mode, this time is equal to requested total photon collection time&&Readout status - 0
ADD_REC trecs:sad:exposedRQ&& Float, Seconds, EXPRQ, -, Requested total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTime since only half the time is spent on-source.  In stare mode, this time is equal to onSourceTime&&Readout status - 0
ADD_REC trecs:sad:prep&& Boolean, -, -, -, Flag to indicate that the Detector Controller is "preparing" to make an observation i.e. waiting for detector temperature to stabilize and taking a sample exposure to verify detector %well is within acceptable range&&Readout status - 0
ADD_REC trecs:sad:rdout&& Boolean, -, -, -, Flag to indicate that the Detector Controller is acquiring data.  While data is being acquired, both ACQ and RDOUT will be TRUE.  During a CHOP-NOD or NOD mode observation, RDOUT will go FALSE while ACQ remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, RDOUT will be reset to TRUE.  When an observation completes, both ACQ and RDOUT will be reset to FALSE&&Readout status - 0
ADD_REC trecs:sad:utend&& String, TBD, UTEND, End, UT at end of observation&&Readout status - 0
~Readout status - 1
ADD_REC trecs:sad:utnow&& String, TBD, -, -, UT time now&&Readout status - 1
ADD_REC trecs:sad:utstart&& String, TBD, UTSTART, Start, UT at beginning of observation&&Readout status - 1
~Acquisition Efficiency
ADD_REC trecs:sad:chopEfficiency&& Float, Percent, CHPEFF, Start, This is the chopper efficiency and is given by chop dwell time divided by half the chop period.  It accounts for chopper transition and setting&&Acquisition Efficiency - 0
ADD_REC trecs:sad:nodEfficiency&& Float, Percent, NODEFF, Start, This is the telescope beamswitch efficiency and is given by nod dwell time divided by half the nod period.  It accounts for the nod transition and setting times&&Acquisition Efficiency - 0
ADD_REC trecs:sad:onSrcEfficiency&& Float, Percent, SRCEFF, Start, This is the "on-source" photon collection efficiency.  In "chop", "chop-nod", or "nod" mode it is equal to half the photonEfficiency (since only half the time is spent on-source).  In "stare" mode it is equal to the photonEfficiency&&Acquisition Efficiency - 0
ADD_REC trecs:sad:photonEfficiency&& Float, Percent, PHOTEFF, Start, This is the photon collection efficiency and is given by the product of all the preceding efficiencies and the Detector Controller frameEfficiency&&Acquisition Efficiency - 0
ADD_REC trecs:sad:frameEfficiency&& Float, Percent, FRMEFF, Start, This is the current frame readout efficiency&&Acquisition Efficiency - 0
~Acquisition Parameter Override Flags
ADD_REC trecs:sad:frameTimeOverrideFlag&& Boolean, -, FTMOVRD, Start, Indicates whether or not manual override of frameTime is in effect&&Acquisition Parameter Override Flags - 0
ADD_REC trecs:sad:chopFreqOverrideFlag&& Boolean, -, CFROVRD, Start, Indicates whether or not manual override of chopFreq is in effect&&Acquisition Parameter Override Flags - 0
ADD_REC trecs:sad:chopDelayOverrideFlag&& Boolean, -, CDLOVRD, Start, Indicates whether or not manual override of chopDelay is in effect&&Acquisition Parameter Override Flags - 0
ADD_REC trecs:sad:nodDwellOverrideFlag&& Boolean, -, NDWOVRD, Start, Indicates whether or not manual override of nodDwell is in effect&&Acquisition Parameter Override Flags - 0
ADD_REC trecs:sad:nodDelayOverrideFlag&& Boolean, -, NDLOVRD, Start, Indicates whether or not manual override of nodDelay is in effect&&Acquisition Parameter Override Flags - 0
ADD_REC trecs:sad:saveFreqOverrideFlag&& Boolean, -, SVFOVRD, Start, Indicates whether or not manual override of saveFreq is in effect&&Acquisition Parameter Override Flags - 0
~Detector Operating Limits
ADD_REC trecs:sad:detTempErrorBand&& Float, Kelvins, -, -, Acceptable deviation of detector temperature from temperature controller heater setpoint value (i.e. heaterSetpoint) for valid observation&&Detector Operating Limits - 0
ADD_REC trecs:sad:maxDetWell&& Float, Percent, -, -, Maximum permitted value to which the detector well may be filled as a percentage of the detector full well&&Detector Operating Limits - 0
~Detector Operating Limits - 1
ADD_REC trecs:sad:minDetWell&& Float, Percent, -, -, Minimum permitted value to which the detector well may be filled as a percentage of the detector full well&&Detector Operating Limits - 1
~Detector Bias Values
ADD_REC trecs:sad:vcurg&& Float, Volts, VCURG, Start, Current "curG" bias voltage&&Detector Bias Values - 0
ADD_REC trecs:sad:vcurs&& Float, Volts, VCURS, Start, Current "curS" bias voltage&&Detector Bias Values - 0
ADD_REC trecs:sad:vdd&& Float, Volts, VDD, Start, Current "vdd" bias voltage&&Detector Bias Values - 0
ADD_REC trecs:sad:vdetsub&& Float, Volts, VDETSUB, Start, Current "vdi" bias voltage&&Detector Bias Values - 0
ADD_REC trecs:sad:vdi&& Float, Volts, VDI, Start, Current "vdi" bias voltage&&Detector Bias Values - 0
ADD_REC trecs:sad:vrst&& Float, Volts, VRST, Start, Current "vrst" bias voltage&&Detector Bias Values - 0
~Detector Temperature Settings
ADD_REC trecs:sad:hPowerReq&& String, -, -, -, Desired heater setpoint (i.e. control) power&&Detector Temperature Settings - 0
ADD_REC trecs:sad:hPowerDefault&& String, -, -, -, Heater power level for nominal background flux conditions&&Detector Temperature Settings - 0
ADD_REC trecs:sad:hPowerLowBackground&& String, -, -, -, Heater power level for low background flux conditions&&Detector Temperature Settings - 0
ADD_REC trecs:sad:hSetpointReq&& Float, Kelvins, -, -, Desired heater setpoint (i.e. control) temperature&&Detector Temperature Settings - 0
ADD_REC trecs:sad:hSetpointDefault&& Float, Kelvins, -, -, Heater temperature setpoint for nominal background flux conditions&&Detector Temperature Settings - 0
ADD_REC trecs:sad:hSetpointLowBackground&& Float, Kelvins, -, -, Heater temperature setpoint for low background flux conditions&&Detector Temperature Settings - 0
~Sub-System Status Information
ADD_REC trecs:sad:detiD&& String, -, DETID, Start, Detector array chip identifier&&Sub-System Status Information - 0
ADD_REC trecs:sad:detType&& String, -, DETTYPE, Start, Description of detector array type&&Sub-System Status Information - 0
~Sub-System Status Information - 1
ADD_REC trecs:sad:dcHealth&& String, -, CCHEALTH, Start, Detector Controller health&&Sub-System Status Information - 1
ADD_REC trecs:sad:dcHeartbeat&& Integer, -, -, -, Continuously changing variable used to detect whether the system is still alive&&Sub-System Status Information - 1
ADD_REC trecs:sad:dcName&& String, -, DCNAME, Start, Detector Controller name&&Sub-System Status Information - 1
ADD_REC trecs:sad:dcState&& String, -, DCSTATE, Start, Detector Controller state&&Sub-System Status Information - 1
~Wheel Positions
ADD_REC trecs:sad:aperturePos&& Long, -, -, -, Aperture wheel position &&Wheel Positions - 0
ADD_REC trecs:sad:coldClampPos&& Long, -, -, -, Cold Clamp wheel position&&Wheel Positions - 0
ADD_REC trecs:sad:filter1Pos&& Long, -, -, -, Filter wheel 1 position &&Wheel Positions - 0
ADD_REC trecs:sad:filter2Pos&& Long, -, -, -, Filter wheel 2 position&&Wheel Positions - 0
ADD_REC trecs:sad:gratingPos&& Long, -, -, -, Grating wheel position &&Wheel Positions - 0
ADD_REC trecs:sad:lyotPos&& Long, -, -, -, Lyot stop wheel position&&Wheel Positions - 0
ADD_REC trecs:sad:pupilPos&& Long, -, -, -, Pupil wheel position &&Wheel Positions - 0
ADD_REC trecs:sad:sectorPos&& Long, -, -, -, Sector wheel position &&Wheel Positions - 0
ADD_REC trecs:sad:slitPos&& Long, -, -, -, Slit wheel position &&Wheel Positions - 0
ADD_REC trecs:sad:windowPos&& Long, -, -, -, Window changer wheel position &&Wheel Positions - 0
~Motor Raw Positions
ADD_REC trecs:sad:apertureRawPos&& Long, Steps, -, -, Current aperture motor position &&Motor Raw Positions - 0
~Motor Raw Positions - 1
ADD_REC trecs:sad:coldClampRawPos&& Long, Steps, -, -, Current cold clamp motor position&&Motor Raw Positions - 1
ADD_REC trecs:sad:filter1RawPos&& Long, Steps, -, -, Current filter 1 motor position &&Motor Raw Positions - 1
ADD_REC trecs:sad:filter2RawPos&& Long, Steps, -, -, Current filter 2 motor position&&Motor Raw Positions - 1
ADD_REC trecs:sad:gratingRawPos&& Long, Steps, -, -, Current grating motor position &&Motor Raw Positions - 1
ADD_REC trecs:sad:lyotRawPos&& Long, Steps, -, -, Current lyot stop motor position&&Motor Raw Positions - 1
ADD_REC trecs:sad:pupiRawlPos&& Long, Steps, -, -, Current pupil motor position &&Motor Raw Positions - 1
ADD_REC trecs:sad:sectorRawPos&& Long, Steps, -, -, Current sector motor position &&Motor Raw Positions - 1
ADD_REC trecs:sad:slitRawPos&& Long, Steps, -, -, Current slit motor position &&Motor Raw Positions - 1
ADD_REC trecs:sad:windowRawPos&& Long, Steps, -, -, Current window changer motor position &&Motor Raw Positions - 1
~Heater Control Status
ADD_REC trecs:sad:heaterPower&& String, , -, , Current heater power level&&Heater Control Status - 0
ADD_REC trecs:sad:heaterSetpoint&& Float, Kelvins, -, , Current heater setpoint (i.e. control) temperature&&Heater Control Status - 0
ADD_REC trecs:sad:heaterHeartbeat&& Integer, , -, , Continuously changing variable used to detect whether the system is still alive&&Heater Control Status - 0
~Temperature Sensors
ADD_REC trecs:sad:tSActiveShield&& Float, Kelvins, TSACSHLD, , Current active shield temperature readout&&Temperature Sensors - 0
ADD_REC trecs:sad:tSActiveShieldHealth&& String, , -, , Active shield temperature sensor health&&Temperature Sensors - 0
ADD_REC trecs:sad:tSActiveShieldLimit&& Float, Kelvins, -, , Highest "healthy" active shield temperature&&Temperature Sensors - 0
~Temperature Sensors - 1
ADD_REC trecs:sad:tSActiveShieldNominal&& Float, Kelvins, -, , Nominal active shield temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead1&& Float, Kelvins, TSCH1, , Current 1st stage coldhead temperature readout&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead1Health&& String, , -, , 1st stage coldhead temperature sensor health&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead1Limit&& Float, Kelvins, -, , Highest "healthy" 1st stage coldhead temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead1Nominal&& Float, Kelvins, -, , Nominal 1st stage coldhead temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead2&& Float, Kelvins, TSCH2, , Current 2nd stage coldhead temperature readout&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead2Health&& String, , -, , 2nd stage coldhead temperature sensor health&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead2Limit&& Float, Kelvins, -, , Highest "healthy" 2nd stage coldhead temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSColdhead2Nominal&& Float, Kelvins, -, , Nominal 2nd stage coldhead temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetector&& Float, Kelvins, TSDET, , Current detector temperature readout&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetectorEnd&& Float, Kelvins, TSDEND, End, Detector temperature readout at end of observation&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetectorHealth&& String, , -, , Detector temperature sensor health&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetectorLimit&& Float, Kelvins, -, , Highest "healthy" detector temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetectorNominal&& Float, Kelvins, -, , Nominal detector temperature&&Temperature Sensors - 1
ADD_REC trecs:sad:tSDetectorStart&& Float, Kelvins, TSDSTART, Start, Detector temperature readout at start of observation&&Temperature Sensors - 1
~Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics1&& Float, Kelvins, TSOP1, , Current optics bench position 1 temperature readout&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics1Health&& String, , -, , Optics bench position 1 temperature sensor health&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics1Limit&& Float, Kelvins, -, , Highest "healthy" optics bench position 1 temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics1Nominal&& Float, Kelvins, -, , Nominal optics bench position 1 temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics2&& Float, Kelvins, TSOP2, , Current optics bench position 2 temperature readout&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics2Health&& String, , -, , Optics bench position 2 temperature sensor health&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics2Limit&& Float, Kelvins, -, , Highest "healthy" optics bench position 2 temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSOptics2Nominal`&& Float, Kelvins, -, , Nominal optics bench position 2 temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSPassiveShield&& Float, Kelvins, TSPASHLD, , Current passive shield temperature readout&&Temperature Sensors - 2
ADD_REC trecs:sad:tSPassiveShieldHealth&& String, , -, , Passive shield temperature sensor health&&Temperature Sensors - 2
ADD_REC trecs:sad:tSPassiveShieldLimit&& Float, Kelvins, -, , Highest "healthy" passive shield temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSPassiveShieldNominal&& Float, Kelvins, -, , Nominal passive shield temperature&&Temperature Sensors - 2
ADD_REC trecs:sad:tSWindow&& Float, Kelvins, TSWIN, , Current window temperature readout&&Temperature Sensors - 2
ADD_REC trecs:sad:tSWindowEnd&& Float, Kelvins, TSWEND, End, Window turret temperature readout at end of observation&&Temperature Sensors - 2
ADD_REC trecs:sad:tSWindowHealth&& String, , -, , Window turret temperature sensor health&&Temperature Sensors - 2
~Temperature Sensors - 3
ADD_REC trecs:sad:tSWindowLimit&& Float, Kelvins, -, , Highest "healthy" window turret temperature&&Temperature Sensors - 3
ADD_REC trecs:sad:tSWindowNominal&& Float, Kelvins, -, , Nominal window turret temperature&&Temperature Sensors - 3
ADD_REC trecs:sad:tSWindowStart&& Float, Kelvins, TSWSTART, Start, Window turret temperature readout at start of observation&&Temperature Sensors - 3
ADD_REC trecs:sad:cryostatPressure&& Float, Milli Torrs, -, , Cryostat pressure&&Temperature Sensors - 3
~Sub-System Status
ADD_REC trecs:sad:ccHealth&& String, , CCHEALTH, Start, Components Controller health&&Sub-System Status - 0
ADD_REC trecs:sad:ccHeartbeat&& Integer, , -, , Continuously changing variable used to detect whether the system is still alive&&Sub-System Status - 0
ADD_REC trecs:sad:ccName&& String, , CCNAME, , Components Controller name&&Sub-System Status - 0
ADD_REC trecs:sad:ccState&& String, , CCSTATE, , Components Controller state&&Sub-System Status - 0
ADD_REC trecs:sad:wfsBeam&& Boolean, , WFSBEAM, , Always FALSE for TReCS since no OIWFS&&Sub-System Status - 0
~Internal status records provided
ADD_REC trecs:sad:dc:acq&& bi, , , , Flags to indicate that the Detector Controller is acquiring data.  While data is being acquired, both acq and rdout will be TRUE.  During a CHOP-NOD or NOD mode observation, rdout will go FALSE while acq remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, rdout will be reset to TRUE.  When an observation completes, both acq and rdout will be reset to FALSE&&Internal status records provided - 0
ADD_REC trecs:sad:dc:rdout&& bi, , , , &&Internal status records provided - 0
ADD_REC trecs:sad:dc:prep&& bi, , , , Flag to indicate that the Detector Controller is "preparing" to make an observation i.e. waiting for detector temperature to stabilize and taking a sample exposure to verify detector %well is within acceptable range&&Internal status records provided - 0
~External status records required
ADD_REC trecs:sad:TBD&& TBD, TCS, TBD, , RA&&External status records required - 0
ADD_REC trecs:sad:TBD&& TBD, TCS, TBD, , DEC&&External status records required - 0
ADD_REC trecs:sad:TBD&& TBD, TBD, TBD, , Time&&External status records required - 0
~External status records required - 1
ADD_REC trecs:sad:TBD&& TBD, TBD, TBD, , Dome Humidity&&External status records required - 1
