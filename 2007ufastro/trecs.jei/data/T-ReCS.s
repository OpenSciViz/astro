~;latif's scripts
~miri_dc_connect
PUT miri:dc:hardware.A "0" ; in, STRING, TBD, command mode
PUT miri:dc:hardware.DIR "START" ; in, STRING, TBD, hardware directive(from dc:apply.OUTC or dc:adjHrdwrG.VALN)
PUT miri:dc:physical.A "0" ; in, LONG, TBD, command mode
PUT miri:dc:physical.DIR "START" ; in, STRING, TBD, physical directive(from dc:adjPhysG.VALM or dc:apply.OUTB)
PUT miri:dc:acqControl.E "0" ; in, STRING, TBD, sim mode
PUT miri:dc:acqControl.DIR "START" ; in, STRING, TBD, TBD
~miri_dc_config
PUT miri:dc:acqControl.A "CONFIGURE" ; in, STRING, TBD, command
PUT miri:dc:acqControl.DIR "START" ; in, STRING, TBD, command
~miri_dc_start
PUT miri:dc:acqControl.A "START" ; in, STRING, TBD, command
PUT miri:dc:acqControl.DIR "START" ; in, STRING, TBD, command
~miri_motor_connect
PUT miri:cc:apply.DIR "CLEAR"
DELAY 2000
PUT miri:cc:init.A "0" ; in, STRING, n/a, init directive
PUT miri:cc:apply.DIR "START" ; in, STRING, TBD, TBD
~miri_CCApply
PUT miri:cc:apply.DIR "START" ; in, STRING, TBD, TBD
~miri_origin_motors
PUT miri:cc:sectWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT miri:cc:winChngr:origin.A "1" ; in, STRING, <1>, test directive
PUT miri:cc:aprtrWhl:origin.A "1" ; in, STRING, <1>, test directive
PUT miri:cc:fltrwhl1:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT miri:cc:lyotWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT miri:cc:fltrwhl2:origin.A "1" ; in, STRING, <1>, test directive
PUT miri:cc:pplImg:origin.A "1" ; in, STRING, <1>, test directive
PUT miri:cc:slitWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT miri:cc:grating:origin.A "1" ; in, STRING, <1>, test directive
PUT miri:cc:coldClmp:origin.A "1" ; in, STRING, <1>, test directive
~trecs_dc_connect
PUT trecs:dc:hardware.A "0" ; in, STRING, TBD, command mode
PUT trecs:dc:hardware.DIR "START" ; in, STRING, TBD, hardware directive(from dc:apply.OUTC or dc:adjHrdwrG.VALN)
PUT trecs:dc:physical.A "0" ; in, LONG, TBD, command mode
PUT trecs:dc:physical.DIR "START" ; in, STRING, TBD, physical directive(from dc:adjPhysG.VALM or dc:apply.OUTB)
PUT trecs:dc:acqControl.E "0" ; in, STRING, TBD, sim mode
PUT trecs:dc:acqControl.DIR "START" ; in, STRING, TBD, TBD
~trecs_motor_connect
PUT trecs:cc:apply.DIR "CLEAR"
DELAY 2000
PUT trecs:cc:init.A "0" ; in, STRING, n/a, init directive
PUT trecs:cc:apply.DIR "START" ; in, STRING, TBD, TBD
~trecs_CCApply
PUT trecs:cc:apply.DIR "START" ; in, STRING, TBD, TBD
~trecs_origin_motors
PUT trecs:cc:sectWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT trecs:cc:winChngr:origin.A "1" ; in, STRING, <1>, test directive
PUT trecs:cc:aprtrWhl:origin.A "1" ; in, STRING, <1>, test directive
PUT trecs:cc:fltrwhl1:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT trecs:cc:lyotWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT trecs:cc:fltrwhl2:origin.A "1" ; in, STRING, <1>, test directive
PUT trecs:cc:pplImg:origin.A "1" ; in, STRING, <1>, test directive
PUT trecs:cc:slitWhl:origin.A "1" ; in, STRING, <O|2|3>, init directive
PUT trecs:cc:grating:origin.A "1" ; in, STRING, <1>, test directive
PUT trecs:cc:coldClmp:origin.A "1" ; in, STRING, <1>, test directive
~;Kisko's scripts
~autoexec
PUSH View Recent Commands
~test1
PUT  trecs:instrumentSetup.B imaging ;select item #, 0 based
PUT  trecs:observationSetup.A chop ;select item #, 0 based
~test add 
ADD_REC trecs:setDhsInfo.A&& in, STRING, TBD, Commanded DHS quick-look stream ID&&Custom List 2
ADD_REC trecs:setDhsInfo.VALA&& out, TBD, TBD, Current DHS quick-look stream ID&&Custom List 2
~test all cmds
LOG_START test.log
; test all cmds
PUT  trecs:observation.B 10 ;OnSource time
GET  trecs:observation.B 
DELAY 2000
PAUSE When the noise stops, press OK.
; the following cmd is normally part of a recorded "Do selected cfg"
.Imaging mode : field
ADD_REC trecs:sad:state && System status && my status screen
LOG_STOP
~;Matt's Scripts
~miri_error
PUT  miri:cc:sectWhl:namedPos.A 3 ;select item #, 0 based
PUT miri:cc:sectWhl:motorApply.DIR 3 ;apply the high level move
PUT  miri:cc:sectWhl:init.G 0 ;home
PUT  miri:cc:sectWhl:motorApply.DIR 3 ;apply the home
PUT  miri:cc:sectWhl:namedPos.A 0 ;select item #, 0 based
PUT miri:cc:sectWhl:motorApply.DIR 3 ;apply the high level move
~miritest
PUT  miri:cc:sectWhl:namedPos.A 2 ;select item #, 0 based
PUT miri:cc:sectWhl:motorApply.DIR 3 ;apply the high level move
PUT  miri:cc:sectWhl:init.G 0 ;home
PUT  miri:cc:sectWhl:init.G 0 ;datum
PUT  miri:cc:sectWhl:motorApply.DIR 3 ;Home -- note that a zero should have been put to the datum rec
PUT  miri:cc:sectWhl:init.G 0 ;datum
PUT  miri:cc:sectWhl:motorApply.DIR 3 ;Home -- note that a zero should have been put to the datum rec
PUT  miri:cc:sectWhl:motorApply.DIR 3 ;apply the home
~trecs_error
PUT  trecs:cc:winChngr:namedPos.A 3 ;select item #, 0 based
PUT trecs:cc:winChngr:motorApply.DIR 3 ;apply the high level move
PUT  trecs:cc:winChngr:init.G 0 ;home
PUT  trecs:cc:winChngr:motorApply.DIR 3 ;apply the home
PUT  trecs:cc:winChngr:namedPos.A 0 ;select item #, 0 based
PUT trecs:cc:winChngr:motorApply.DIR 3 ;apply the high level move
~error3
~add is1
ADD_REC trecs:dataMode.A&& in, STRING, <save|discard>, Commanded data disposition mode&&Custom List 1
ADD_REC trecs:dataMode.VALA&& out, STRING, <save|discard>, Current data disposition&&Custom List 1
ADD_REC trecs:debug.A&& in, STRING, <NONE|MIN|FULL>, Commanded debuggin output level&&Custom List 1
ADD_REC trecs:debug.VALA&& out, STRING, <NONE|MIN|FULL>, Current debugging output level&&Custom List 1
ADD_REC trecs:init.A&& in, STRING, <NONE|VSM|FAST|FULL>, Commanded simulation mode&&Custom List 1
ADD_REC trecs:init.VALA&& out, STRING, <NONE|VSM|FAST|FULL>, Current simulation mode&&Custom List 1
ADD_REC trecs:observationSetup.A&& in, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&Custom List 1
ADD_REC trecs:observationSetup.B&& in, STRING, TBD[TBD], Commnaded source photon collectin time&&Custom List 1
ADD_REC trecs:observationSetup.C&& in, STRING, <nominal|high>, Commanded flux background setting&&Custom List 1
ADD_REC trecs:observationSetup.D&& in, STRING, <nominal|high>, Commanded flux noise setting&&Custom List 1
ADD_REC trecs:observationSetup.VALA&& out, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&Custom List 1
ADD_REC trecs:observationSetup.VALB&& out, STRING, TBD[TBD], Current source photon collecting time&&Custom List 1
ADD_REC trecs:observationSetup.VALC&& out, STRING, <nominal|high>, Current flux background setting&&Custom List 1
ADD_REC trecs:observationSetup.VALD&& out, STRING, <nominal|high>, Current flux noise setting&&Custom List 1
ADD_REC trecs:observe.A&& in, TBD, TBD, Commanded data label&&Custom List 1
ADD_REC trecs:observe.VALA&& out, TBD, TBD, Current data label&&Custom List 1
ADD_REC trecs:setDhsInfo.A&& in, STRING, TBD, Commanded DHS quick-look stream ID&&Custom List 1
ADD_REC trecs:setDhsInfo.VALA&& out, TBD, TBD, Current DHS quick-look stream ID&&Custom List 1
~add is2
ADD_REC trecs:instrumentSetup.A&& in, STRING, imaging|spectroscopy, Commanded camera mode&&Custom List 1
ADD_REC trecs:instrumentSetup.B&& in, STRING, <field|window|pupil>, Commanded imaging mode&&Custom List 1
ADD_REC trecs:instrumentSetup.C&& in, STRING, TBD, Commanded aperture name&&Custom List 1
ADD_REC trecs:instrumentSetup.D&& in, STRING, TBD, Commanded filter name&&Custom List 1
ADD_REC trecs:instrumentSetup.E&& in, STRING, loRes10|hiRes10|loRes20, Commanded grating name&&Custom List 1
ADD_REC trecs:instrumentSetup.F&& in, STRING, TBD, Commanded central wavelength&&Custom List 1
ADD_REC trecs:instrumentSetup.G&& in, STRING, TBD, Commanded lens name&&Custom List 1
ADD_REC trecs:instrumentSetup.H&& in, STRING, TBD, Commanded Lyot stop name&&Custom List 1
ADD_REC trecs:instrumentSetup.I&& in, STRING, TBD, Commanded sector name&&Custom List 1
ADD_REC trecs:instrumentSetup.J&& in, STRING, TBD, Commanded slit width&&Custom List 1
ADD_REC trecs:instrumentSetup.K&& in, STRING, TBD, Commanded window setting&&Custom List 1
ADD_REC trecs:instrumentSetup.L&& in, STRING, TRUE|FALSE, Override automatic aperture selection&&Custom List 1
ADD_REC trecs:instrumentSetup.M&& in, STRING, TRUE|FALSE, Override automatic filter selection&&Custom List 1
ADD_REC trecs:instrumentSetup.N&& in, STRING, TRUE|FALSE, Override automatic lens selection&&Custom List 1
ADD_REC trecs:instrumentSetup.O&& in, STRING, TRUE|FALSE, Override automatic window selection&&Custom List 1
ADD_REC trecs:instrumentSetup.VALA&& out, STRING, imaging|spectroscopy, Current camera mode&&Custom List 1
ADD_REC trecs:instrumentSetup.VALB&& out, STRING, <field|window|pupil>, Current imaging mode&&Custom List 1
ADD_REC trecs:instrumentSetup.VALC&& out, STRING, [0..1], Optical throughput&&Custom List 1
ADD_REC trecs:instrumentSetup.VALE&& out, STRING, TBD, Current filter name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALF&& out, STRING, TBD, Current central wavelength&&Custom List 1
ADD_REC trecs:instrumentSetup.VALG&& out, STRING, TBD, Current aperture name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALH&& out, STRING, TBD, Current cold clamp name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALI&& out, STRING, TBD, Current filter 1 name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALJ&& out, STRING, TBD, Current filter 2 name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALK&& out, STRING, loRes10|hiRes10|loRes20, Current grating name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALL&& out, STRING, TBD, Current Lyot stop name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALM&& out, STRING, TBD, Current pupil name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALN&& out, STRING, TBD, Current sector name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALO&& out, STRING, TBD, Current slit width&&Custom List 1
ADD_REC trecs:instrumentSetup.VALP&& out, STRING, TBD, Current window setting&&Custom List 1
~heartbeat monitor for trecs
ADD_REC trecs:ec:heartbeat&& EC HEARTBEAT&&heartbeats
ADD_REC trecs:cc:heartbeat&& CC HEARTBEAT&&heartbeats
ADD_REC trecs:dc:heartbeat&& DC HEARTBEAT&&heartbeats
~heartbeat monitor for miri
ADD_REC miri:ec:heartbeat&& EC HEARTBEAT&& heartbeats
ADD_REC miri:cc:heartbeat&& CC HEARTBEAT&&heartbeats
ADD_REC miri:dc:heartbeat&& DC HEARTBEAT&&heartbeats
~;David's Scripts
~high level move 1
ADD_REC trecs:cc:sectWhl:namedPos.A&&pos #&&HighLevel SectorWheel
ADD_REC trecs:cc:sectWhl:namedPos.DIR&&DIR field&&HighLevel SectorWheel
ADD_REC trecs:cc:sectWhl:motorApply.DIR&&apply DIR&&HighLevel SectorWheel
ADD_REC trecs:cc:sectWhl:namedPos.VALC&&actual pos #&&HighLevel SectorWheel
ADD_REC trecs:cc:sectWhl:motorG.VALC&&Steps from home&&HighLevel SectorWheel
ADD_REC trecs:cc:sectWhl:motorC.VAL&&Motor Status&&HighLevel SectorWheel
