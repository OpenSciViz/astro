#if !defined(__UFBaytech_h__)
#define __UFBaytech_h__ "$Name:  $ $Id: UFBaytech.h,v 0.8 2006/10/19 22:12:52 hon Exp $";
#define __UFBaytech_H__(arg) const char arg##Baytech_h__rcsId[] = __UFBaytech_h__;

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientSocket.h" // Baytechs ethernet communications

// convenience class facilitates transaction with the baytech
// remote power (networked) switch. it has a telnet service
// with text based menus.

// this class provides functions for connecting to the baytech
// on the standard telnet tcp port 23; and submitting powerOff
// and powerOn commands to each of its 8 ports; and querying
// powerOn/Off status for each port; and querying RMS and peek
// current/amperage usage; and querying internal temperature.
class UFBaytech : public UFClientSocket {
public:
//  static bool _verbose;
  virtual ~UFBaytech();
  virtual int close();

  // return 0 on connection failure:
  static UFBaytech* create(const string& host, float flush= 0.25, int port= 23);
  virtual int connect(const string& host= "", float flush= 0.25, int port= 23, bool block= true);
  int reconnect(int port= 23);

  // perhaps this should go into base class:
  int recvReply(string& r, float flush= 0.25);

  // submit string (only), return immediately without fetching reply
  virtual int submit(const string& s, float flush= 0.25); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  virtual int submit(const string& s, string& r, float flush= 0.25); // flush output and sleep flush sec.

  // Baytech specific funcs
  string rmsAmps(const string& bt= "", float flush= 0.25);
  string maxAmps(const string& bt= "", float flush= 0.25);
  string celsius(const string& bt= "", float flush= 0.25);

  // chan  <=0 for all 8 channel logical AND, chan > 0 for individual chan: 
  string powerStat(int chan= -1, float flush= 0.25);
  bool powerStat(int chan, string& reply, float flush= 0.25);
  inline static string powerStatSim(int chan= -1, float flush= 0.25) { return statusSim(); }

  // turn on power to all or individual chan
  string powerOn(int chan= -1, float flush= 0.25);
  inline static string powerOnSim(int chan= -1, float flush= 0.25) { return statusSim(); }

  // turn off power to all or individual chan
  string powerOff(int chan= -1, float flush= 0.25);
  inline static string powerOffSim(int chan= -1, float flush= 0.25) { return statusSim(); }

  string status(float flush= 0.25); // just sending _eos string causes baytech to reply with a full status string
  static string statusSim(); // just return fixed string (screen captured)
  static int statparse(const string& btstat, vector< string >& statv); // parse each line of btstat 

  inline string getEOTR() { return _eotr; }
  inline string getSOTR() { return _sotr; }
  inline string getEOTS() { return _eots; }
  inline string getSOTS() { return _sots; }

  inline string resetEOTS(const string& eot= "\r") { _eots = eot; return _eots; }
  inline string resetEOTR(const string& eot= ">") { _eotr = eot; return _eotr; }
  inline string resetSOTS(const string& sot= "") { _sots = sot; return _sots; }
  inline string resetSOTR(const string& sot= "") { _sotr = sot; return _sotr; }
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
  static int main(int argc, char ** argv);

protected:
  string _btstat;
  static UFBaytech* _btinstance;
  string _eotr; // end-of-text in recv/reply usually either '\n' or '\r'
  string _sotr; // start-of-text in recv/reply usually empty '' (n/a) or '*'
  string _eots; // end-of-text in send/submit usually either '\n' or '\r'
  string _sots; // start-of-text in send/submit usually empty '' (n/a)  or '#'

  // prevent compiler from defining default or copy ctors:
  inline UFBaytech() : UFClientSocket() { resetEOTS(); resetEOTR(); resetSOTS(); resetSOTR(); }
  inline UFBaytech(const UFBaytech& rhs) : UFClientSocket((const UFClientSocket&) rhs)
    { resetEOTS(); resetEOTR(); resetSOTS(); resetSOTR(); }

  int _statparse(vector< string >& statv); // parse each line of _btstat 
  string _cleanReply(char* cs, int n);
};

#endif // __UFBaytech_h__   
