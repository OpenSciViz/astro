#if !defined(__UFSerialPort_h__)
#define __UFSerialPort_h__ "$Name:  $ $Id: UFSerialPort.h,v 0.3 2005/02/25 19:36:38 drashkin Exp $"
#define __UFSerialPort_H__(arg) const char arg##SerialPort_h__rcsId[] = __UFSerialPort_h__;

// common
#include "unistd.h"
#include "fcntl.h"
#include "sys/types.h"
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "sioLib.h"
#include "tyLib.h"
#include "fioLib.h"
#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#include "sys/uio.h"
#include "termio.h"
#elif defined(LINUX)
#include "termio.h"
#else
#include "sys/termios.h"
#endif

#include "cerrno"
#include "cstdio"
#include "cstring"
// this causes a compile error with sgi stl:
#include "string"
#include "iostream"

const int __DefaultBaud_ = 9600;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
const int __PortCnt_ = 4;
#elif defined(LINUX)
const int __PortCnt_ = 4;
//#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#else
const int __PortCnt_ = 2;
#endif

using namespace std ;
 
class UFSerialPort {
protected:
  static int _fd[__PortCnt_]; //
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
  static int _orig[__PortCnt_];
#elif defined(LINUX)
  static struct termio _orig[__PortCnt_] ;
  static struct termio _raw;
//#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#else
  static struct termio _orig[__PortCnt_], _raw;
#endif

protected:
  UFSerialPort(int _portIndex= 1); // comb
  static int _portIndex;
  //static string _portName;
  static string* _portName_ptr;

private:
  static UFSerialPort _theInstance;

public:
  inline virtual ~UFSerialPort() {}

  static UFSerialPort* getInstance();
  string portName(int portIndex) const;
  int portNum( const string& name) const ;
  inline int getPortCnt() const { return __PortCnt_; }
  inline int getFd(int portIndex = 0) const { return _fd[portIndex]; }

  int openAll(int baud = __DefaultBaud_);
  int open(const string& portName, int baud = __DefaultBaud_);
  int open(int portIndex, int baud = __DefaultBaud_);

  int closeAll();
  int close(const string& portName);
  int close(int portIndex= -1);

  int send(char c, int portIndex= 1);
  int send(const unsigned char* buf, int len, int portIndex= 1);
  int send(const unsigned char* buf, int len, const string& portName);
  int send(const string& s, int portIndex= 1);
  int send(const string& s, const string& portName);

  int recv(char& c, int portIndex= 1);
  int recv(unsigned char* buf, int len, int portIndex= 1);
  int recv(unsigned char* buf, int len, const string& portName);
  int recv(string& s, int portIndex= 1);
  int recv(string& s, const string& portName);

  int available(float wait= 0.01, int trycnt= 1, int portNo= 1);
  int hello(int portNo = -1); // -1 indicates all

  int exec(void* p = 0);

  static int main(int portIndex = 1);
  static int main(int argc, char** argv);
};

#endif // __UFSerialPort_h__
