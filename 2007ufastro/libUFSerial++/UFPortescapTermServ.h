#if !defined(__UFPortescapTermServ_h__)
#define __UFPortescapTermServ_h__ "$Name:  $ $Id: UFPortescapTermServ.h,v 0.6 2006/12/19 20:12:10 hon Exp $";
#define __UFPortescapTermServ_H__(arg) const char arg##PortescapTermServ_h__rcsId[] = __UFPortescapTermServ_h__;

#include "string"
#include "vector"
#include "map"
#include "stdio.h"

#include "UFTermServ.h"

class UFPortescapTermServ : virtual public UFTermServ {
public:
  inline UFPortescapTermServ() : UFTermServ() { 
    _party = false;
    //clog<<" UFPortescapTermServ:: UFPortescapTermServ> _party: "<<_party<<endl;
  }
  inline virtual ~UFPortescapTermServ() {}

  static int parsePorts(const string& indexorports, map< string, int >& portmap, const string& start= "A");
  static int parsePorts(const string& indexorports, map< string, int >& portmap, const vector< string >& indexors);
  // if parsePorts return 1, assume party-line:
  static int partyLine(int port, const vector< string >& indexors);
  // if multiple ports are indicated, assume direct lines:
  static int directLines(const map< string, int >& portmap);

  // return 0 on connection failure:
  static UFTermServ* create(const string& host, int port);
  virtual UFTermServ* reconnect();

  int recvOn(UFSerialPort* serial, string& r, string* eotr= 0);
  int recvOn(UFSocket* soc, string& r, string* eotr= 0);

  int recvOnMultiline(UFSerialPort* soc, string& r, int numEOTR= 1, string* eotr= 0);
  int recvOnMultiline(UFSocket* soc, string& r, int numEOTR= 1, string* eotr= 0);

  // the "normal" recv behavior -- the '@' abort cmd. may not not echo
  // back fully, no _eotr?
  virtual int submit(const string& s, string& r, float flsh= 0.0, int exact= 0); // flush output and sleep flush sec.
  // since recvOn is called implicitely within submit, submit must be told about any multiline responses to expect from the indexor.

  //submit with multi-line reply (indexors, for some god-forsaken reason, use \r\n as both an
  //end-of-line token and an end-of-transmission token)
  virtual int submitMultiline(const string& s, string& r, float flsh= 0.0, int numEOTRTokens=1);
  virtual int submitMultiline(const string& s, vector< string >& rv, float flsh= 0.0, int numEOTRTokens=1);

  // support escape (hard global abort) command
  virtual int escape(const string& s, float flsh= 0.0);

  // party line shares one socket/port, nonparty line allocates individual socket/ports to each indexor
  static bool _party;

  int handleBootPrompt(float flsh);

 protected:
  // map socket ports to indexor (psuedonames)
  static map< string, int >* _PortMap;
  static map< string, UFTermServ* >* _TSMap;
};

#endif //  __UFPortescapTermServ_h__   
      
