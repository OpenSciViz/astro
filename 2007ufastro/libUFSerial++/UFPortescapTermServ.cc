#if !defined(__UFPortescapTermServ_cc__)
#define __UFPortescapTermServ_cc__ "$Name:  $ $Id: UFPortescapTermServ.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFPortescapTermServ.h"
#include "UFRuntime.h"
#include "UFStringTokenizer.h"

bool UFPortescapTermServ::_party = false;
map< string, int >* UFPortescapTermServ::_PortMap= 0;
map< string, UFTermServ* >* UFPortescapTermServ::_TSMap= 0;

int UFPortescapTermServ::parsePorts(const string& indexorports, map< string, int >& portmap, const string& start) {
  // consider "P1,PN" and assume A,N for indexor names
  if( indexorports.length() <= 1 ) {
    clog<<"UFPortescapTermServ::parsePorts> bad indexorport list: "<<indexorports<<endl;
    return -1;
  }
  //clog<<"UFPortescapTermServ::parsePorts> indexorport list: "<<indexorports<<endl;
  portmap.clear();
  string strt = start;
  const string _Indxors = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
  const string _indxors = "abcdefghijklmnopqrstuvwxyz";
  size_t _Idxpos = _Indxors.find(strt);
  size_t _idxpos = _indxors.find(strt);

  vector< string > words;
  string comma = ",";
  if( indexorports.find(comma) != string::npos )
    UFRuntime::parseWords(indexorports, words, comma);
  else
    UFRuntime::parseWords(indexorports, words); // use space default
  
  string w = words[0];
  const char* cs = w.c_str();
  int port0 = -1;
  int portN = -1;
  if( isdigit(*cs) ) // this must be the first or only port #
    port0 = atoi(cs);

  size_t wc = words.size();
  //clog<<"UFPortescapTermServ::parsePorts> indexorport cnt: "<<wc<<", port0= "<<port0<<endl;
  if( wc <= 2 ) { // this must be the  port #s in a continuous range "first,last"
    w = words[wc-1];
    cs = w.c_str();
    portN = ::atoi(cs);
    //clog<<"UFPortescapTermServ::parsePorts> portN= "<<portN<<endl;
    if( portN < port0 ) {
      clog<<"UFPortescapTermServ::parsePorts> bad indexorport list: "<<indexorports<<endl;
      return -1;
    }
    for( int i = port0; i <= portN; ++i ) {
      if( _Idxpos != string::npos ) {
	string idxr = _Indxors.substr(_Idxpos++, 1);
	portmap[idxr] = i;
	//clog<<"UFPortescapTermServ::parsePorts> idxr: "<<idxr<<", port: "<<i<<endl;
      }
      else if( _idxpos != string::npos ) {
	string idxr = _indxors.substr(_idxpos++, 1);
	portmap[idxr] = i;
	//clog<<"UFPortescapTermServ::parsePorts> idxr: "<<idxr<<", port: "<<i<<endl;
      }
    }
    return (int)portmap.size();
  }
  // this must be a specific list of port #s
  for( size_t i = 0; i < wc; ++i ) {
    if( _Idxpos != string::npos ) {
      string idxr = _Indxors.substr(_Idxpos, 1);
      portmap[idxr] = i;
    }
    else if( _idxpos != string::npos ) {
      string idxr = _indxors.substr(_idxpos, 1);
      portmap[idxr] = i;
    }
  }

  return portmap.size();   
}

int UFPortescapTermServ::partyLine(int port, const vector< string >& indexors) {
  if( indexors.empty() )
    return 0;

  _party = true;
  if( _PortMap == 0 )
    _PortMap = new map< string, int >;

  size_t cnt = indexors.size();
  //clog<<"UFPortescapTermServ::partyLine> indexor cnt: "<<cnt<<endl;

  string indexor;
  for( size_t i = 0; i < cnt; ++i ) {
    indexor = indexors[i];
    //clog<<"UFPortescapTermServ::partyLine> indexorport list: "<<indexor<<", port: "<<port<<endl;
    (*_PortMap)[indexor] = port;
  }

  return (int) _PortMap->size();
}

int UFPortescapTermServ::directLines(const map< string, int >& portmap) {
  if( portmap.empty() )
    return 0;

  _party = false;
  if( _PortMap == 0 )
    _PortMap = new map< string, int >;

  map< string, int >::const_iterator it = portmap.begin();
  string indexor;
  int port;
  for( ; it != portmap.end(); ++it ) {
    indexor = it->first; port = it->second;
    //clog<<"UFPortescapTermServ::directLines> indexor: "<<indexor<<", port: "<<port<<endl;
    //_PortMap->insert(pair<string, int>(indexor, port));
    (*_PortMap)[indexor] = port;
  }
  return (int) _PortMap->size();
}


// public static func:

UFTermServ* UFPortescapTermServ::reconnect() {
  if( _instance == 0 )
    return 0;

  string host = _instance->peerHost();
  int port = _instance->peerPort();

  return UFPortescapTermServ::create(host, port);
}

// return 0 on connection failure:
UFTermServ* UFPortescapTermServ::create(const string& host, int port) {
/*
  if( _instance ) {
    ((UFClientSocket*)_instance)->close();
    ::fclose(_instance->_fs);
    delete _instance;
  }
*/
  if( _party ) {
    if( port <= 0 ) {
      map< string, int >::const_iterator it = _PortMap->begin();
      if( it == _PortMap->end()) {
        clog<<"UFPortescapTermServ::create> party-line port is unknown: "<<port<<endl;
        return 0;
      }
      string indexor = it->first;
      port = it->second;
      clog<<"UFPortescapTermServ::create> party-line port: "<<port<<", taken from indexor name:"<<indexor<<endl;
    }   
    clog<<"UFPortescapTermServ::create> connecting to shared party-line port: "<<port<<endl;
    _instance = new UFPortescapTermServ;

    int fd = ((UFTermServ*)_instance)->connect(host, port);
    if( fd < 0 ) {
      delete _instance; // dtor sets _instance to 0
    }
    else {
      _instance->_fs = fdopen(fd, "w");
    }
    //clog<<"UFPortescapTermServ::create> party-line connected..."<<endl;
    return _instance;
  }
  
  // noparty-line mode -- use separate socket ports for each indexor
  if( _PortMap->empty() ) {
    clog<<"UFPortescapTermServ::create> Non party-line ports unknown: "<<endl;
    return 0;
  }

  std::map< string, int >::const_iterator it = _PortMap->begin();
  size_t cnt = _PortMap->size();

  //clog<<"UFPortescapTermServ::create> allocating array of portservers: "<<cnt<<endl;
  _TSMap = new std::map< string, UFTermServ* >;
  UFPortescapTermServ* ptsAll = new UFPortescapTermServ[cnt]; // keep track of first object in allocation
  UFPortescapTermServ* pts = ptsAll;
  int fd = 0;
  for( int i= 0; it != _PortMap->end(); ++it, ++pts, ++i ) {
    string indexor = it->first;
    port = it->second;
    //clog<<"UFPortescapTermServ::create> initialize directline entry: "<<host<<", port: "<<port<<", indexor: "<<indexor<<endl;
    _instance = pts; // this must be set for full class hierachy creation
    (*_TSMap)[indexor] = pts; //_instance;
    //clog<<"UFPortescapTermServ::create> (nonparty directline) connect host: "<<host<<", port: "<<port<<", indexor: "<<indexor<<endl;
    fd = _instance->connect(host, port);
    if( fd > 0 ) { // use the first pointer in the list for "this" instance
      pts->_fs = fdopen(fd, "w"); // for flushing?
    }
    pts->resetEOTS("\r\n"); // directline mode requires different separator (or is it new indexor firmware)?
    pts->resetEOTR("\r\n"); // assume symmetrical echo...
    pts->handleBootPrompt(0.1); // deal with possibility power was recycled
  }

  clog<<"UFPortescapTermServ::create> completed creation of array of portservers: "<<cnt<<endl;

  // set to first in list...
  _instance = ptsAll; 
  return _instance;
}

int UFPortescapTermServ::recvOn(UFSerialPort* serial, string& r, string* eot) {
  int na= 0;
  bool done= false;
  string eotr = _eotr;
  if( eot )
    eotr = *eot;

  while( !done ) {
    na = _available(0.05, 3); 
    //clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
    if( na < 0 ) {
      clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
      return na;
    }
    /*
    else if( na == 0 && r.rfind("@") != string::npos ) { 
      // portescaps sometimes do not echo back abort command with  eotr '\n'?
      return r.length();
    }
    */
    else if( na == 0 ) {
      clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
      //na = 1; // block on 1 byte (char) read, using UFSocket::recv
      return na;
    }

    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    // clog<<"UFPortescapTemrServ::recvOn(serial)> expect to recv na: "<<na<<endl;
    int n = serial->recv((unsigned char*)input, na);
    if( n <= 0 ) {
      clog<<"UFPortescapTemrServ::recvOn(serial)> returned: "<<n<<endl;
      return n;
    }
    r += input;
    size_t eotpos = r.rfind(eotr);
    size_t rlen = r.length();
    size_t near = eotr.length(); // is eot at/near the end of reply string?
    //clog<<"UFPortescapTemrServ::recvOn(serial)> eotpos: "<<eotpos<<", rlen: "<<rlen<<", near: "<<near<<endl;
    if( eotpos != string::npos && ((rlen - eotpos) == near) ) { // terminator
	done = true;
    }
  }

  if( _verbose )
    clog<<"UFPortescapTermServ::recvOn(serial)> reply: "<<r<<endl;

  return r.length();
}

int UFPortescapTermServ::recvOn(UFSocket* soc, string& r, string* eot) {
  int na= 0;
  bool done= false;
  string eotr = _eotr;
  if( eot )
    eotr = *eot;

  if( _verbose )
    clog<<"UFPortescapTemrServ::recvOn(soc)> "<<soc->description()<<endl;
  while( !done ) {
    na = _available(0.05, 3); 
    //clog<<"UFPortescapTemrServ::recvOn(soc)> available returned: "<<na<<endl;
    if( na < 0 ) {
      clog<<"UFPortescapTermServ::recvOn(soc)> available returned: "<<na<<endl;
      return na;
    }
    /*
    else if( na == 0 && r.rfind("@") != string::npos ) { 
      // portescaps sometimes do not echo back abort command with  eotr '\n'?
      return r.length();
    }
    */
    else if( na == 0 ) {
      clog<<"UFPortescapTermServ::recvOn(soc)> available returned: "<<na<<endl;
      // na = 1; // block on 1 byte (char) read, using UFSocket::recv
      return na;
    }

    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    //clog<<"UFPortescapTemrServ::recvOn(soc)> expect to recv na: "<<na<<endl;
    int n = soc->recv((unsigned char*)input, na);
    //clog<<"UFPortescapTemrServ::recvOn(soc)> returned: "<<n<<endl;
    if( n <= 0 ) {
      clog<<"UFPortescapTemrServ::recvOn(soc)> returned: "<<n<<endl;
      return n;
    }
    r += input;
    //clog<<"UFPortescapTemrServ::recvOn(soc)> check for eotr: "<<eotr<<" in reply: "<<r<<n<<endl;
    size_t eotpos = r.rfind(eotr);
    size_t rlen = r.length();
    size_t near = eotr.length(); // is eot at/near the end of reply string?
    if( eotpos != string::npos && ((rlen - eotpos) == near) ) { // terminator
	done = true;
      //clog<<"UFPortescapTemrServ::recvOn(soc)> got eot..."<<endl;
    }
  }

  if( _verbose )
    clog<<"UFPortescapTermServ::recvOn(soc)> reply: "<<r<<endl;

  return r.length();
}


int UFPortescapTermServ::recvOnMultiline(UFSerialPort* serial, string& r, int numEOTR, string* eot) {
  int na= 0;
  bool done= false;
  string eotr = _eotr;
  if( eot )
    eotr = *eot;

  while( !done ) {
    na = _available(0.05, 3); 
    //clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
    if( na < 0 ) {
      clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
      return na;
    }
    /*
    else if( na == 0 && r.rfind("@") != string::npos ) { 
      // portescaps sometimes do not echo back abort command with  eotr '\n'?
      return r.length();
    }
    */
    else if( na == 0 ) {
      clog<<"UFPortescapTermServ::recvOn(serial)> available returned: "<<na<<endl;
      //na = 1; // block on 1 byte (char) read, using UFSocket::recv
      return na;
    }

    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    // clog<<"UFPortescapTemrServ::recvOn(serial)> expect to recv na: "<<na<<endl;
    int n = serial->recv((unsigned char*)input, na);
    if( n <= 0 ) {
      clog<<"UFPortescapTemrServ::recvOn(serial)> returned: "<<n<<endl;
      return n;
    }
    r += input;
    int numEotr = 0;
    size_t eotrpos = r.find(eotr);
    if (eotrpos != string::npos) numEotr++;
    while (eotrpos != string::npos && ((eotrpos+1) < r.length())) {
      eotrpos = r.find(eotr,eotrpos+1);
      if (eotrpos != string::npos) numEotr++;
    }
    size_t eotpos = r.rfind(eotr);
    size_t rlen = r.length();
    size_t near = eotr.length(); // is eot at/near the end of reply string?
    //clog<<"UFPortescapTemrServ::recvOn(serial)> eotpos: "<<eotpos<<", rlen: "<<rlen<<", near: "<<near<<endl;
    if( eotpos != string::npos && ((rlen - eotpos) == near) && (numEotr >= numEOTR) ) { // terminator
	done = true;
    }
  }

  if( _verbose )
    clog<<"UFPortescapTermServ::recvOn(serial)> reply: "<<r<<endl;

  return r.length();
}

int UFPortescapTermServ::recvOnMultiline(UFSocket* soc, string& r, int numEOTR, string* eot) {
  int na= 0;
  bool done= false;
  string eotr = _eotr;
  if( eot )
    eotr = *eot;

  if( _verbose )
    clog<<"UFPortescapTemrServ::recvOnMultiline(soc)> "<<soc->description()<<endl;
  while( !done ) {
    na = _available(0.05, 3); 
    //clog<<"UFPortescapTemrServ::recvOn(soc)> available returned: "<<na<<endl;
    if( na < 0 ) {
      clog<<"UFPortescapTermServ::recvOnMultiline(soc)> available returned: "<<na<<endl;
      return na;
    }
    /*
    else if( na == 0 && r.rfind("@") != string::npos ) { 
      // portescaps sometimes do not echo back abort command with  eotr '\n'?
      return r.length();
    }
    */
    else if( na == 0 ) {
      clog<<"UFPortescapTermServ::recvOnMultiline(soc)> available returned: "<<na<<endl;
      // na = 1; // block on 1 byte (char) read, using UFSocket::recv
      return na;
    }

    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    //clog<<"UFPortescapTemrServ::recvOn(soc)> expect to recv na: "<<na<<endl;
    int n = soc->recv((unsigned char*)input, na);
    //clog<<"UFPortescapTemrServ::recvOn(soc)> returned: "<<n<<endl;
    if( n <= 0 ) {
      clog<<"UFPortescapTemrServ::recvOnMultiline(soc)> returned: "<<n<<endl;
      return n;
    }
    r += input;
    //clog<<"UFPortescapTemrServ::recvOn(soc)> check for eotr: "<<eotr<<" in reply: "<<r<<n<<endl;
    int numEotr = 0;
    size_t eotrpos = r.find(eotr);
    if (eotrpos != string::npos) numEotr++;
    while (eotrpos != string::npos && ((eotrpos+1) < r.length())) {
      eotrpos = r.find(eotr,eotrpos+1);
      if (eotrpos != string::npos) numEotr++;
    }
    size_t eotpos = r.rfind(eotr);    
    size_t rlen = r.length();
    size_t near = eotr.length(); // is eot at/near the end of reply string?
    if( eotpos != string::npos && ((rlen - eotpos) == near) && (numEotr >= numEOTR) ) { // terminator
	done = true;
      //clog<<"UFPortescapTemrServ::recvOn(soc)> got eot..."<<endl;
    }
  }  

  if( _verbose )
    clog<<"UFPortescapTermServ::recvOnMultiline(soc)> reply: "<<r<<endl;

  return r.length();
}

// this works for partyline mode:
// submit string, recv reply and return reply as string
int UFPortescapTermServ::submit(const string& s, string& r, float flsh, int exact) {
  r = "";
  int ns = 0, nr= 0;
  string ss = s;
  UFRuntime::rmJunk(ss);
  if( ss.length() <= 1 ) {
    clog<<"UFPortescapTermServ::submit> input insufficient: \'"<<ss<<"\'"<<endl;
    return -1;
  }
  if( _party ) {
    //clog<<"UFPortescapTermServ::submit> party-line submit: "<<ss<<endl;
    ns = UFTermServ::submit(ss, flsh, exact);
    if( ns <= 0 )
      return ns;

    if( peerHost().find("localserial") == string::npos ) { // connected to the annec/iocomm term. server
      return recvOn((UFSocket*) this, r);
    }

    return recvOn((UFSerialPort*) this, r); // using localserial serial port
  }

  // non partyline: strip leading character indexor name from command
  // and send on associated socket-port
  if( _PortMap->empty() ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line _PortMap empty!"<<endl;
    return -1;
  }

  string indexor = ss.substr(0, 1);
  if( _verbose )
    clog<<"UFPortescapTermServ::submit> direct-line indexor: "<<indexor<<", ss: "<<ss<<endl;
  map< string, int >::const_iterator it = _PortMap->find(indexor);
  if( it == _PortMap->end() ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line _PortMap does not contain indexor: "<<indexor<<endl;
    return -2;
  }

  // set the _instance pointer to the specified port object
  _instance = (*_TSMap)[indexor];
  if( _instance == 0 ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line container does not contain indexor "<<indexor<<" instance"<<endl;
    return -2;
  }

  // for some reason these have been reset after create set them?
  _instance->resetEOTS("\r\n"); // directline mode requires different separator (or is it new indexor firmware)?
  _instance->resetEOTR("\r\n"); // assume symmetrical echo...
  string ds = ss.substr(1);
  UFRuntime::rmJunk(ds); // send cmd without indexor name...
  if( _verbose )
    clog<<"UFPortescapTermServ::submit> direct-line _PortMap indexor: "<<indexor<<", ds: "<<ds<<", _instance: "<<(size_t) _instance<<endl;

  ns = UFTermServ::submit(ds, flsh, exact); // should use the appropriate soc
  if( ns <= 0 )
    return ns;


  bool local = ( peerHost().find("localserial") != string::npos );
  if( !local ) { 
    // connected to the perle/annex/iocomm term. server
    nr = recvOn((UFSocket*) _instance, r);
  }
  else {  // using localserial serial port
    nr = recvOn((UFSerialPort*) _instance, r);
  }
  // and be sure to indicate which indexor replied:
  if( nr > 0 ) {
    UFRuntime::rmJunk(r);
    r = indexor + r;
  }
  //clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;

  /*
  int na= _available();
  if( na <= 0 )
    return nr;
   
  clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;
  clog<<"UFPortescapTermServ::submit> more available, na: "<<na<<endl;
  if( !local ) { 
    // connected to the perle/annex/iocomm term. server
    nr += recvOn((UFSocket*) _instance, r);
  }
  else {  // using localserial serial port
    nr += recvOn((UFSerialPort*) _instance, r);
  }
  // and be sure to indicate which indexor replied:
  if( nr > 0 )
    r = indexor + r;

  clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;
  */
  return nr;
}


int UFPortescapTermServ::submitMultiline(const string& s, vector< string >& rv, float flsh, int numEOTR) {
  string r = "";
  rv.clear();
  submitMultiline(s,r,flsh,numEOTR);
  UFStringTokenizer ufst(r,'\n');
  for (unsigned int i=0; i<ufst.size(); i++){
    string tmp = ufst[i];
    if (tmp[tmp.length()-1] == '\r')
      tmp = tmp.substr(0,tmp.length()-1);
    rv.push_back(tmp);
  }
  return r.length();
}
// this works for partyline mode:
// submit string, recv reply and return reply as string
int UFPortescapTermServ::submitMultiline(const string& s, string& r, float flsh, int numEOTR) {
  int ns = 0, nr= 0;
  string ss = s;
  UFRuntime::rmJunk(ss);
  if( ss.length() <= 1 ) {
    clog<<"UFPortescapTermServ::submit> input insufficient: \'"<<ss<<"\'"<<endl;
    return -1;
  }
  if( _party ) {
    //clog<<"UFPortescapTermServ::submit> party-line submit: "<<ss<<endl;
    ns = UFTermServ::submit(ss, flsh);
    if( ns <= 0 )
      return ns;

    if( peerHost().find("localserial") == string::npos ) { // connected to the annec/iocomm term. server
      return recvOnMultiline((UFSocket*) this, r, numEOTR);
    }

    return recvOnMultiline((UFSerialPort*) this, r, numEOTR); // using localserial serial port
  }

  // non partyline: strip leading character indexor name from command
  // and send on associated socket-port
  if( _PortMap->empty() ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line _PortMap empty!"<<endl;
    return -1;
  }

  string indexor = ss.substr(0, 1);
  if( _verbose )
    clog<<"UFPortescapTermServ::submit> direct-line indexor: "<<indexor<<", ss: "<<ss<<endl;
  map< string, int >::const_iterator it = _PortMap->find(indexor);
  if( it == _PortMap->end() ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line _PortMap does not contain indexor: "<<indexor<<endl;
    return -2;
  }

  // set the _instance pointer to the specified port object
  _instance = (*_TSMap)[indexor];
  if( _instance == 0 ) {
    clog<<"UFPortescapTermServ::submit> nonparty direct-line container does not contain indexor "<<indexor<<" instance"<<endl;
    return -2;
  }

  // for some reason these have been reset after create set them?
  _instance->resetEOTS("\r\n"); // directline mode requires different separator (or is it new indexor firmware)?
  _instance->resetEOTR("\r\n"); // assume symmetrical echo...
  string ds = ss.substr(1);
  UFRuntime::rmJunk(ds); // send cmd without indexor name...
  if( _verbose )
    clog<<"UFPortescapTermServ::submit> direct-line _PortMap indexor: "<<indexor<<", ds: "<<ds<<", _instance: "<<(size_t) _instance<<endl;

  ns = UFTermServ::submit(ds, flsh); // should use the appropriate soc
  if( ns <= 0 )
    return ns;


  bool local = ( peerHost().find("localserial") != string::npos );
  if( !local ) { 
    // connected to the perle/annex/iocomm term. server
    nr = recvOnMultiline((UFSocket*) _instance, r, numEOTR);
  }
  else {  // using localserial serial port
    nr = recvOnMultiline((UFSerialPort*) _instance, r, numEOTR);
  }
  // and be sure to indicate which indexor replied:
  if( nr > 0 ) {
    UFRuntime::rmJunk(r);
    r = indexor + r;
  }
  //clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;

  /*
  int na= _available();
  if( na <= 0 )
    return nr;
   
  clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;
  clog<<"UFPortescapTermServ::submit> more available, na: "<<na<<endl;
  if( !local ) { 
    // connected to the perle/annex/iocomm term. server
    nr += recvOn((UFSocket*) _instance, r);
  }
  else {  // using localserial serial port
    nr += recvOn((UFSerialPort*) _instance, r);
  }
  // and be sure to indicate which indexor replied:
  if( nr > 0 )
    r = indexor + r;

  clog<<"UFPortescapTermServ::submit> recv'd: "<<r<<endl;
  */
  return nr;
}


// if there has been a power-cycle in direct (nonparty) line mode, first
// send a space char and read-off the prompt
int UFPortescapTermServ::handleBootPrompt(float flsh) {
  string sp = " ";
  //clog<<"UFPortescapTermServ::handleBootPrompt> submit space "<<endl;
  int ns = UFTermServ::submit(sp, flsh); // should use base class submit on the appropriate soc (_instance)
  if( ns <= 0 )
    return ns;

  // in directline mode the boot prompt terminator is "#\n"
  string r = "";
  string eot = "#\r\n";
  if( peerHost().find("localserial") == string::npos ) { // connected to the annec/iocomm term. server
    int nr = recvOn((UFSocket*) this, r, &eot);
    //clog<<"UFPortescapTermServ::handleBootPrompt> reply: "<<r<<endl;
    return nr;
  }
  
  return recvOn((UFSerialPort*) this, r, &eot); // using localserial serial port
}


// submit escape char
int UFPortescapTermServ::escape(const string& s, float flsh) {
  char esc = 27;
  // raw byte send:
  if( _verbose )
    clog<<"UFPortescapTermServ::escape> "<<s<<endl;
  
  int ns= 0, ne= 0;
  bool local = ( peerHost().find("localserial") != string::npos );
  if( !local ) { // connected to the perle/annex/iocomm term. server
    if( _party && s != "" ) {
      ns += _instance->UFClientSocket::send((unsigned char*) s.c_str(), s.length());
      if( ns < 0 ) { // assume connection was lost and try to reconnect and resend
      // if socket is complient with X/Open standard, ::send can return negative of:
      // EMGSIZE, ENOTCONN, EPIPE, ENETDOWN, ENETUNREACH, ...
        clog<<"UFPortescapTermServ::escape> socket send failed, try to reconnect..."<<endl;
        if( _instance->reconnect() != 0 )
          ns = _instance->UFClientSocket::send((unsigned char*) s.c_str(), s.length());
	else
	  return -1;
      }
    }
    else if( s != "" && _PortMap ) {
      char* cs = (char*)s.c_str();
      while( *cs == ' ' || *cs == '\t' )
        ++cs;
      string indexor = cs;
      indexor = indexor.substr(0, 1);
      map< string, int >::const_iterator it = _PortMap->find(indexor);
      if( it == _PortMap->end() ) {
        clog<<"UFPortescapTermServ::escape> nonparty-line _PortMap does not contain indexor: "<<indexor<<endl;
        return -1;
      }
      // set the _instance pointer to the specified port object
      _instance = (*_TSMap)[indexor];
      if( _instance == 0 ) {
        clog<<"UFPortescapTermServ::escape> nonparty direct-line container does not contain indexor: "<<indexor<<endl;
        return -2;
      }
    }
    ns += ne = _instance->UFClientSocket::send((unsigned char*) &esc, 1);
  }
  else { // use localserial serial port
    if(_party && s != "" )
      ns += _instance->UFSerialPort::send(s);
    ns += _instance->UFSerialPort::send((unsigned char*) &esc, 1, _instance->peerPort());
  }

  // try flushing the socket/serial-port output stream
  // this is the only way i can think to do it:
  int stat = flush(flsh);
  if( stat != 0 ) clog<<"UFPortescapTermServ::escape> socket flush failed? ns= "<<ns<<endl;

  string r = "";
  if( ns <= 0 )
    return ns;

  if( _party ) // all done
    return ns;

  // is there an echo or any reply? evidently in direct (non party line) mode a '#'
  // is expected
  int nr= 0, na= 0;
  bool done= false;
  while( !done ) {
    na = _available(); // default behavior tries at least 3 times
    if( na <= 0 ) {
      clog<<"UFPortescapTemrServ::submit/reply> available() returned: "<<na<<endl;
      return na;
    }
    // single direct-line indexor returns '#', party-line is not described in manual 
    // portescaps sometimes do not echo back abort command with _eotr?
    /*
    else if( na == 0 ) {
      int cnt = 3;
      while( na == 0 && --cnt >= 0 ) { // try a few more times...
        UFPosixRuntime::sleep(0.1);
        na = _available();
      }
      if( na <= 0 )
	return na;
    }
    */
    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    if( !local ) // connected to the annec/iocomm term. server
      nr = _instance->UFClientSocket::recv((unsigned char*)input, na);
    else // use localserial serial port
      nr = _instance->UFSerialPort::recv((unsigned char*)input, na);
    if( nr <= 0 ) {
      clog<<"UFPortescapTemrServ::submit/reply> recv returned: "<<nr<<endl;
      return nr;
    }

    r += input;
    size_t eotpos = r.rfind(_eotr);
    size_t rlen = r.length();
    size_t near = _eotr.length(); // is eot at/near the end of reply string?
    if( eotpos != string::npos && ((rlen - eotpos) == near) ) // terminator
      done = true;
  }

  return nr;
}

#endif // __UFPortescapTermServ_cc__
