#include <iostream>
#include <string>
#include "UFClientSocket.h"

/**
 * This class is used to poll the TRIPP-LITE Smart UPS to determine the status
 * of the battery powered backup unit.  It communicates with the UPS by sending and
 * receiving HTML pages from the TRIPP-LITE web server. For an example of how to use
 * this class, see the static main function provided by this class.
 * @see main(int,char**)
 */
class UFSmartUPS : UFClientSocket { 
  
  /**
   * Inner struct used to store data received from the UPS web server. The parseMessage()
   * method is used to fill these values in once the _message member has been filled by a 
   * successful call to getResponse(). 
   * The only values that we really care about are inpVolt (if less than 120, means that
   * we're on backup power), and battCap (how much battery is left), but the others
   * may be useful in the future, which is why they are here (and why the parseMessage()
   * method fills these values in)
   * @see parseMessage() and _message
   */
  struct UPSParams {
    int inpVolt;     ///< Input Voltage 
                     /**< If input voltage falls below 120, that means 
		      *   that we are on backup power
		      */ 
    int battCap;     ///< Battery Capacity 
                     /**< Percentage of battery left.  When this reaches 10-20 percent, we
		      *   should trigger an alarm condition to gracefully shutdown all systems 
		      *   connected to the UPS.  This class only provides functionality to raise
		      *   an alarm condition, so the driver program must be responsible for 
		      *   noticing the alarm condition and issuing all necessry shutdown commands
		      */ 
    int load;        ///< Load 
                     /**< Amount of load on the smart UPS */
    double inpFreq;  ///< Input Frequency
                     /**< Should always be around 60Hz */ 
    double battVolt; ///< Battery Voltage
                     /**< Battery voltage level. */

    /// Contructor initializes all members to 0
    /** Insures that no members have misleading junk when a USPParams variable is 
     *  first instantiated
     */
    inline UPSParams() : inpVolt(0),battCap(0),load(0),inpFreq(0.0),battVolt(0.0) {} 
  };

public: 
  /// Variable used to store UPS parameters
  /** After sending a request to the web server and receiving a response, the 
   *  parseMessage() method should be called to fill in the data members
   *  @see requestPage(const char*, const char *), getResponse(), and parseMessage()
   */
  UPSParams _params;

private:
  string _message; ///< Storage variable for responses from UPS web server
                  ///< actual data recieved from UPS web server (should be in html format) after
                  ///< a successful call to getResponse()
                  ///< @see getResponse()
  bool _alarm;

public:
  /// Default Constructor
  /** Initializes data members and calls the UFClientSocket super contructor */
  UFSmartUPS();

  /// Receives a response from the UPS web server
  /** The response from the web server (should be in html format) is placed into the _message data
   *  member. The parseMessage() method should be called after calling getResponse() to extract 
   *  the data from the html and place it in the _params structure.
   *  @return The number of bytes read minus the length of the http (NOT HTML) header
   *  @see parseMessage(), _message and _params
   */
  int getResponse() ;

  /// Request a page from the UPS web server
  /** This method formulates an http get command and sends it to the UPS web server.  A call to
   *  this method should be followed by a call to the getResponse() method.
   *  @param pagename: The name (including path) of the page to get from the server
   *  @param hostname: The hostname or ipaddress of the UPS web server
   *  @return The result of calling the UFSocket::send(const & string) method
   *  @see getResponse()
   */
  int requestPage(const char * pagename, const char * hostname);

  /// Send logon information to the UPS web server
  /** This method formulates an http post command and sends it to the UPS web server. A call to this
   *  method should only be required if a previous call to loginRequired() returned true, and 
   *  should be followed by a call to the getResponse() method, which in turn should be followed
   *  by a call to the loginFailed() method.
   *  @param username: username used to log in to the UPS website
   *  @param password: password corresponding to username
   *  @param hostname: The hostname or ipaddress of the UPS web server
   *  @return The result of calling the UFSocket::send(const & string) method
   *  @see loginRequired(), loginFailed() and getResponse()
   */
  int postLogon(const char * username, const char * password, const char * hostname);

  /// Parse response from UPS web server which is stored in data member _message
  /** This method should be called only after successful calls to requestPage() and getResponse().
   *  After calling getResponse(), the data received from the UPS web server is stored in _message.
   *  parseMessage() will then inspect _message, extract the appropriate data,
   *  and fill in the _params data structure.  NB -- this method assumes that the UPS web server
   *  responds in a consistant manner.  If at any time in the future the format of the status.htm
   *  web page served by the UPS web server changes, this method must be altered to reflect those
   *  changes.
   *  @see getResponse(), requestPage(), _message and _params
   */
  void parseMessage() ;

  /// Inspects response from web server to determine is a login is required
  /** This method should be called after calling getResponse() and before calling parseMessage().
   *  After getResponse() fills in _message, this method scans the html to see if a login is
   *  required.  If so, the postLogon() method should be called to post the required login 
   *  information to the UPS web server.  Note that this sequence of calls should only be required
   *  once;  after logging in, the UPS web server should not require another for the duration of
   *  the session.
   *  @return True is a logon is required, false if a logon is not required
   *  @see getResponse(), parseMessage(), postLogon(), and _message
   */
  bool loginRequired();

  /// Inspects response from the web server to determine whether the login attempt succeeded
  /** This method should be called after calling the postLogon() and getResponse() methods.
   *  After getResponse() fills in _message, this method scans the html to see if the login
   *  attempt succeeded or failed.
   *  @return True if the previous login attempt failed, false if everything went ok.
   *  @see postLogon(), getResponse() and loginRequired()
   */
  bool loginFailed();

  /// Clears out _message and resets _alarm
  /** This is a convenience method used to re-initialize _message and _alarm
   *  @see _alarm and _message
   */
  void reset();

  /// Convenience method to print _message to stdout
  /**
   *  @see _message
   */
  inline void printMessage() { cout << _message << endl; }

  /// Convenience method to print all data memebers of _params to stdout
  /**
   *  @see _params
   */
  void printParams();

  /// Public method to report the status of _alarm
  /** This method simply returns the value of _alarm.  _alarm is automatically set to true when any 
   *  of the following conditions are met:  <br>
   *  1.) a call to requestPage() or postLogon() returns with a value ?, indicating a socket timeout.<br>
   *  2.) a call to getResponse() returns with a value ?, indicating a socket timeout.<br>
   *  3.) a call to parseMessage() results in _param.battCap having a value <= 20 (ie, battery capacity
   *  &nbsp;&nbsp;&nbsp;&nbsp;is at less than or equal to 20 percent)<br>
   *  @see requestPage(), postLogon(), parseMessage(), and _params
   *  @return True if there is an alarm condition, false if everything is ok.
   */
  inline bool isAlarm() { return _alarm; }

  /// static method used to connect to web server and retrieve the battery capacity
  /**
   *  @param host_or_IP Hostname or IP address of UPS web server
   *  @return Percentage of battery left, -1 on connection or login error, and -2 if isAlarm() reports
   *  an alarm condition
   *  @see isAlarm()
   */
  static int fetchBatteryHealth(const string & host_or_IP);

  /// static main method can be used as the main loop in a driver program
  /** Example usage: <br>
   *
   *  <CODE>
   *  #include "UFSmartUPS.h" <br>
   *  int main (int argc, char ** argv) { <br>
   *  &nbsp;&nbsp;return UFSmartUPS::main(argc,argv);<br>
   *  }<br></CODE>
   *
   *  For documentation purposes, the entire main method is reproduced below.
   *  This should provide insight into how to properly use this class, and what
   *  sequence of method calls are necessary:
   *<CODE><br>
   *
   *  char HOST_NAME[] = "192.168.111.40"; <br>
   *  int HTTP_PORT = 80; <br>
   *  UFSmartUPS us; <br>
   *  if ((us.connect(HOST_NAME,HTTP_PORT,false)) < 0) { <br>
   *  &nbsp;&nbsp;fprintf(stderr,"Uh oh, better exit\n"); <br>
   *  &nbsp;&nbsp;exit(1); <br>
   *  } <br>
   *  us.setTimeOut(10); <br>
   *  us.requestPage("/status.htm",HOST_NAME); <br>
   *  us.getResponse();  <br>
   *  if (us.loginRequired()) { //check to see if we need to send a login message <br>
   *  &nbsp;&nbsp;us.postLogon("admin","1234",HOST_NAME); <br>
   *  &nbsp;&nbsp;us.getResponse(); <br> 
   *  &nbsp;&nbsp;if (us.loginFailed()) { <br>
   *  &nbsp;&nbsp;&nbsp;&nbsp;fprintf(stderr,"Login failed.  Aborting...\n"); <br>
   *  &nbsp;&nbsp;&nbsp;&nbsp;return -1; <br>
   *  &nbsp;&nbsp;}     <br>
   *  } <br>
   *  while (true) { // enter polling loop <br>
   *  &nbsp;&nbsp;us.requestPage("/status.htm",HOST_NAME); <br>
   *  &nbsp;&nbsp;us.getResponse(); <br>
   *  &nbsp;&nbsp;us.parseMessage(); <br>
   *  &nbsp;&nbsp;us.printParams(); <br>
   *  &nbsp;&nbsp;if (us.isAlarm()) { <br>
   *  &nbsp;&nbsp;&nbsp;&nbsp;//need to issue shutdown commands ... <br>
   *  &nbsp;&nbsp;&nbsp;&nbsp;cout << "Alarm!!" << endl; <br>
   *  &nbsp;&nbsp;} <br>
   *  &nbsp;&nbsp;sleep(10);//sleep for 10 seconds <br>
   *  } <br>
   *</CODE>
   */
  static int main(int argc, char ** argv);
};
