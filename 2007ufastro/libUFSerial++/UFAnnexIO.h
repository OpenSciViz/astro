#if !defined(__UFAnnexIO_h__)
#define __UFAnnexIO_h__ "$Name:  $ $Id: UFAnnexIO.h,v 0.0 2002/06/03 17:42:20 hon beta $";
#define __UFAnnexIO_H__(arg) const char arg##AnnexIO_h__rcsId[] = __UFAnnexIO_h__;

#include "string"
#include "vector"

#include "UFClientSocket.h"

class UFAnnexIO : public UFClientSocket {
public:
  // for use by parent/other process:
  struct FifoInfo { int sendFd, recvFd; string sendName, recvName;
                    FifoInfo(); FifoInfo(const string& fifo_prefix); FifoInfo(const FifoInfo&); };

  // for use as stand-alone or child process/task that can use fifos:
  static int main(int argc, char** argv);
  static UFAnnexIO* create(const vector< char* >& args);
  
  // convenience function for executing annex child:
  static pid_t startChild(char* fifoname, UFAnnexIO::FifoInfo& info, int port= 7008);
  // for use by parent process, to open "other ends" of fifos:
  static UFAnnexIO::FifoInfo openFifos(const string& fifoname);

  // default ctor:
  UFAnnexIO(int portNo = 7008, const string& tcphost = "192.168.111.100");
  ~UFAnnexIO();

  // for use by threads:
  inline void setSendFifoName(const string& name) { _sendfifoname = name; }
  inline string getSendFifoName() { return _sendfifoname; }
  inline void setRecvFifoName(const string& name) { _recvfifoname = name; }
  inline string getRecvFifoName() { return _recvfifoname; }
  inline unsigned char* recvBuffPtr() { return _inputAnnex; }
  inline unsigned char* sendBuffPtr() { return _outputAnnex; }
  inline int rcvBufSize() { int snd, rcv; socBufSizes(snd, rcv); return rcv; }
  inline int sndBufSize() { int snd, rcv; socBufSizes(snd, rcv); return snd; }
  inline int setUsrInput(int ui) { _usrInput = ui; return ui; }
  inline int getUsrInput() { return _usrInput; }
  inline int setUsrOutput(int uo) { _usrOutput = uo; return uo; }
  inline int getUsrOutput() { return _usrOutput; }
  inline void setEot(const char* eot) { _eot = eot; }
  inline void setEot(const string& eot) { _eot = eot; }
  inline void setQuick(const string& s) { _quick = s; }
  inline string getQuick() { return _quick; }
  int sendQuick();
  int escape(const string& s, float flsh= 0.0); // s+esc
  inline void setSubmit(const string& s) { _submit = s; }
  inline string getSubmit() { return _submit; }
  inline void setFlush(float f) { _flush = f; }
  inline float getFlush() { return _flush; }
  int submit();

  // helper for O_NONBLOCK opens/writes (should put this into the runtime class)
  static int writeFully(int fd, unsigned char* buf, int sz);
  // fetch user input from stdin or pipe, and send to annex
  // currently translates new-lines to carriage-returns (default)
  // need to provide option to change the translation default (later)
  int fetchAndSend(int fd, int n);

  // create & start recv thread, then user enter event loop (read & send)
  // this is also the send thread, calls fetchAndSend
  static int mainThread(const vector< char* >& args, UFAnnexIO* instance);

  // posix thread functions must be declared static
  // this recv's the annex transmission & writes it out to the user via _userOutput
  // p should point to open file desc. of _userOutput
  static void* recvThread(void* p);

protected:
  // inherit portNo & hostname attributes
  unsigned char* _inputAnnex; /* [1024*1024]; recv buff == socket buffer max size? */
  unsigned char* _outputAnnex;  /* [1024*1024]; send buff == socket buffer max size? */
  // while I/O with the Annex is always via socket, user I/O could be
  // from a terminal or from another process (like IDL) via named pipe/fifo(s)
  int _usrInput, _usrOutput; // file desc.: default is 0, 1; and/or named pipe/fifo(s)
  string _sendfifoname, _recvfifoname, _quick, _submit;
  float _flush;
  //char _eot;
  string _eot;
  void UFAnnexIO::_telnetHandshake(int expect);
};

#endif //  __UFAnnexIO_h__   
      
