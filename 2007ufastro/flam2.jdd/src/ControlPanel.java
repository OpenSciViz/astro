package ufjdd;
/**
 * Title:        Java Data Display (JDD): ControlPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For controlling the single image display of contents of a frame buffer.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ControlPanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ControlPanel.java,v 1.4 2005/08/09 19:59:02 amarin Exp $";

    ImageBuffer imgBuffer;
    ImageDisplayPanel imgDisplay;
    AdjustPanel adjustPanel;
    DataAccessPanel dataAccess;
    JRadioButton radioButtonSingle = new JRadioButton("Single");
    JRadioButton radioButtonAccum = new JRadioButton("Accum");
    ButtonGroup radioButtons = new ButtonGroup();
    JComboBox bufferSelector;
    JComboBox bufferOperation;
    
    public ControlPanel( ImageDisplayPanel imgdp,  AdjustPanel adjustPanel, DataAccessPanel dataAccess )
    {
        super(new FlowLayout());
        this.imgDisplay = imgdp;
        this.dataAccess = dataAccess;
        this.adjustPanel = adjustPanel;
        
        setBackground(Color.GRAY);
        setPreferredSize(new Dimension( imgDisplay.width, 40 ));

        int numBufs = 1;
        String[] regBufferNames = new String[7];     //always 7 basic buffer names.
	int index = 0;
        
        for( int j = 0; j < numBufs; j++ ) {
	    String bufnam = "Buffer name";
	    if( bufnam.toLowerCase().indexOf("acc") < 0 ) regBufferNames[index++] = bufnam;
        }

        bufferSelector = new JComboBox( regBufferNames );

	String[] options = {"Buffer:","Buffer - REF","REF - Buffer","grab as REF","accum REF","stop accum"};
	bufferOperation = new JComboBox( options );

        add( bufferOperation );
        add( bufferSelector );
        add( radioButtonSingle );
        add( radioButtonAccum );

        radioButtons.add( radioButtonSingle );
        radioButtons.add( radioButtonAccum );
	radioButtons.setSelected( radioButtonSingle.getModel(), true );

        // Register action listeners for the radio buttons:

        radioButtonSingle.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { readFrameBuffer(); } });

        radioButtonAccum.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { readFrameBuffer(); } });

        bufferSelector.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { readFrameBuffer(); } });

	bufferOperation.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { doBufferOperation(); } });
    }
//--------------------------------------------------------------------------------------------

    public void doBufferOperation()
    {
	String oper = (String)bufferOperation.getSelectedItem();

	if( oper.indexOf("REF") >= 0 )
	    {
		if( oper.indexOf("grab") >= 0 )
		    {
			imgDisplay.grabReference();
			bufferOperation.setSelectedIndex(0);
		    }
		else if( oper.indexOf("Buffer") > oper.indexOf("REF") )
		    imgDisplay.subtractFromReference( true );
		else
		    imgDisplay.subtractReference( true );
	    }
	else if( oper.indexOf("stop") >= 0 )
	    {
		bufferOperation.setSelectedIndex(0);
	    }
	else imgDisplay.subtractReference( false );

	readFrameBuffer();
    }
//--------------------------------------------------------------------------------------------

    public void displayBuffer( String buffname )
    {
	if( buffname.toLowerCase().indexOf("acc") >= 0 ) {
	    String basicBuff = buffname.substring( buffname.indexOf("(")+1 );
	    basicBuff = basicBuff.substring( 0, basicBuff.indexOf(")") );
	    displayBuffer( basicBuff, true );
	}
	else displayBuffer( buffname, false );
    }

    public void displayBuffer( String buffname, boolean accum )
    {
	if( accum )
	    radioButtons.setSelected( radioButtonAccum.getModel(), true);
	else
	    radioButtons.setSelected( radioButtonSingle.getModel(), true);

	bufferSelector.setSelectedItem( buffname );
    }
//--------------------------------------------------------------------------------------------
    
    private void readFrameBuffer()
    {
        String bufferType = getRadioSelection( radioButtons );
        String bufferName = (String)bufferSelector.getSelectedItem();
        
        if( bufferType.equalsIgnoreCase("Accum") ) bufferName = "accum(" + bufferName + ")";

	imgBuffer = dataAccess.fetch( bufferName );
	imgDisplay.frmBuffName = bufferName;
	//dataAccess.setUniqBuffs(); //takes new value of frmBuffName and creates unique List of disp. buffers.
	imgDisplay.updateImage( imgBuffer );

	adjustPanel.updateMinMax();
	adjustPanel.scaleAndDraw();
    }

    // This method returns text value of selected radio button in a button group:

    private String getRadioSelection(ButtonGroup group) {
        for( Enumeration e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if( b.getModel() == group.getSelection() ) return b.getText();
        }
        return null;
    }
}
