package ufjdd;
/**
 * Title:        ZoomPanel.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javaUFProtocol.*;

public class ZoomPanel extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: ZoomPanel.java,v 1.7 2006/07/24 16:29:56 warner Exp $";
    
    protected ZoomImage zoomImage;
    protected AdjustZoomPanel adjustZoom;
    protected JLabel pixelValue;
    protected HistogramPanel histoPanel;
    protected ApertureStatsPanel apertureStats;
    protected SuperZoomImage superZoom;

    public JComboBox zoomFacSelect;
    public JComboBox smoothIterSel;
    public int xSize, ySize;

    public ZoomPanel( ColorMapDialog colorMapCntrl, NIRplotPanel plotPanel )
    {
	//this( 420, colorMapCntrl, plotPanel);
	this( 480, colorMapCntrl, plotPanel);
    }

    public ZoomPanel( int zoomSize, ColorMapDialog colorMapCntrl, NIRplotPanel plotPanel )
    {
	//String zooms[] = {"2","3","4","5","7","10","20","30","35","42","60","84"}; //divisors of 420.
	String zooms[] = {"0.25","0.5","1","2","3","4","5","6","8","10","12","15","16","20","24","30","32","40","48","60","80","96"}; //divisors of 480.
        zoomFacSelect = new JComboBox(zooms);
	zoomFacSelect.setMaximumRowCount(zooms.length);

	String smoothFacts[] = {"0","1","2","3","4","6","9","12","16","20","25"};
        smoothIterSel = new JComboBox(smoothFacts);
	smoothIterSel.setMaximumRowCount(14);

        pixelValue = new JLabel(" Pixel Data Values");
        pixelValue.setBorder( BorderFactory.createLoweredBevelBorder() );

	adjustZoom = new AdjustZoomPanel();
	histoPanel = new HistogramPanel( colorMapCntrl, 30, 110 );
	apertureStats = new ApertureStatsPanel();
        superZoom = new SuperZoomImage( 160, colorMapCntrl.colorModel, adjustZoom);

	zoomImage = new ZoomImage( zoomSize, smoothIterSel, pixelValue, histoPanel.histoBar, colorMapCntrl.colorModel, adjustZoom, apertureStats, plotPanel, superZoom );

	adjustZoom.setZoomImage( zoomImage );
        histoPanel.setZoomImage( zoomImage );

	JPanel controlPanel = new JPanel();
	controlPanel.add( new JLabel("ZOOM  Factor = ", JLabel.RIGHT) );
	controlPanel.add( zoomFacSelect );
	controlPanel.add( new JLabel("      Smooth  iterations = ", JLabel.RIGHT) );
	controlPanel.add( smoothIterSel );

	ySize = 2;
	controlPanel.setBounds( 2, ySize, zoomSize, 30 );
	ySize += 34;
	zoomImage.setBounds( 2, ySize, zoomSize, zoomSize );
	ySize += (zoomSize+2);
	int zh = zoomSize/2;
	pixelValue.setBounds( 2, ySize, zoomSize, 30 );
	ySize += 32;
	adjustZoom.setBounds( 2, ySize, zoomSize, 80 );
	ySize += 81;
	xSize = zoomSize + 14;
        histoPanel.setBounds(xSize, 2, histoPanel.xSize, histoPanel.ySize );
	superZoom.setBounds(xSize-10, histoPanel.ySize+3, superZoom.xySize, superZoom.xySize);
        //apertureStats.setBounds( xSize, histoPanel.ySize + 3, histoPanel.xSize, ySize - histoPanel.ySize -4 );
        apertureStats.setBounds( xSize-8, histoPanel.ySize + superZoom.xySize + 3, histoPanel.xSize+10, ySize - histoPanel.ySize - superZoom.xySize - 4 );
	xSize += (histoPanel.xSize+2);

	this.setLayout(null);
	this.add( controlPanel );
	this.add( zoomImage );
        this.add( pixelValue );
        this.add( adjustZoom );
        this.add( histoPanel );
        this.add(superZoom);
        this.add( apertureStats );
	this.setBorder( BorderFactory.createLineBorder(Color.green, 1) );
    }

    public void changeColorModel(IndexColorModel colorModel) {
        zoomImage.updateColorMap( colorModel );
	superZoom.updateColorMap(colorModel);
        histoPanel.colorBar.updateColorMap( colorModel );
    }
}
