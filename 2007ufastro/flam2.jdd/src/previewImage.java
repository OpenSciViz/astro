package ufjdd;
/**
 * Title:        PreviewImage.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

public class previewImage extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: previewImage.java,v 1.3 2006/03/03 23:33:28 warner Exp $";

    protected int previewData[][];
    public int xySize;
    protected byte pixBuffer[];
    protected Image pixImage = null;
    protected double oMin, oMax;
    protected IndexColorModel colorModel;
    protected ImageBuffer previewBuff;

    public previewImage( int previewSize, 
		      IndexColorModel colorModel)
    {
	this.xySize = previewSize;
        this.colorModel = colorModel;
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        this.setMinimumSize(new Dimension(xySize, xySize));

        pixBuffer = new byte[xySize*xySize];
    }
    
    public synchronized void updateImage(ImageBuffer previewBuff, IndexColorModel colorModel)
    {   
	if( previewBuff == null ) return;
        this.previewBuff=previewBuff;
	this.colorModel = colorModel;
    }
//--------------------------------------------------------------------------------
    
    private void previewExpand( byte array[][] ) {
        for( int i=0; i < array.length; i++ ) {
	    int zi = i;
            for( int j=0; j < array[i].length; j++ ) {
		int zj = j;
		byte data = array[i][j];
		int zindex = i * xySize + j;
                pixBuffer[zindex++] = data;
	    }
	}
    }

    public void updateSize(int size) {
	this.xySize = size;
	this.pixBuffer = new byte[xySize*xySize];
    }

    public void updateColorMap(IndexColorModel colorModel) {
        this.colorModel = colorModel;
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }
    

//--------------------------------------------------------------------------------

    public synchronized void applyLinearScale() {
	if( previewBuff == null ) return;
        oMin = previewBuff.min;
	oMax = previewBuff.max;
        applyLinearScale( oMin, oMax ); }
    
    public synchronized void applyLinearScale(double minv, double maxv)
    {
	if( previewBuff == null ) return;
	previewData = previewBuff.image;
        int ny = previewData.length;
	if (ny == 0) return;
	int nx = previewData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
        double range = maxv - minv;

        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double f = ( previewData[i][j] - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}
	
	previewExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public synchronized void applyLogScale(double threshold) {

        if( previewBuff == null ) return; 

        previewData = previewBuff.image;
        int ny = previewData.length;
        if (ny == 0) return;
        int nx = previewData[0].length;
        byte[][] pixScaled = new byte[ny][nx];
	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
        double maxv = Math.log( (double)previewBuff.max );
        double range = maxv - minv;

       for( int i=0; i < ny; i++ ) {
            for( int j=0; j < nx; j++ ) {

		double pval = (double)previewData[i][j];
                double f = 0.0;
		if (pval > threshold) f = (Math.log(pval) - minv)/range; 

                if( f <= 0 )
                    pixScaled[i][j] = 0;
                else if( f >= 1 )
                    pixScaled[i][j] = (byte)255;
                else
                    pixScaled[i][j] = (byte)Math.round(f*255);
            }
        }

	previewExpand(pixScaled);
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public synchronized void applyPowerScale(double threshold, double power) {

        if( previewBuff == null ) return;

        previewData = previewBuff.image;
        int ny = previewData.length;
        if (ny == 0) return;
        int nx = previewData[0].length;
        byte[][] pixScaled = new byte[ny][nx];
        if( threshold <= 0 ) threshold = 1;
        if( power <= 0 ) power = 0.5;
        if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
        double maxv = Math.pow( (double)previewBuff.max, power );
        double range = maxv - minv;

       for( int i=0; i < ny; i++ ) {
            for( int j=0; j < nx; j++ ) {

                double pval = (double)previewData[i][j];
                double f = 0.0;
                if (pval > threshold) f = (Math.pow(pval, power) - minv)/range;

                if( f <= 0 )
                    pixScaled[i][j] = 0;
                else if( f >= 1 )
                    pixScaled[i][j] = (byte)255;
                else
                    pixScaled[i][j] = (byte)Math.round(f*255);
            }
        }

        previewExpand(pixScaled);
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public void setZoomScale(ZoomPanel zoomPanel) {
        if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Linear")) {
           double min = zoomPanel.adjustZoom.newMin;
           double max = zoomPanel.adjustZoom.newMax;
           applyLinearScale(min, max);
        } else if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Log")) {
           double thresh = Double.parseDouble(zoomPanel.adjustZoom.thresholdTextField.getText());
           applyLogScale(thresh);
        } else if (zoomPanel.adjustZoom.scaleMode.equalsIgnoreCase("Power")) {
           double thresh = Double.parseDouble(zoomPanel.adjustZoom.thresholdTextField.getText());
           double power = Double.parseDouble(zoomPanel.adjustZoom.powerTextField.getText());
           applyPowerScale(thresh, power);
        }
    }

    public synchronized void applyZScale() {
	this.applyZScale("Zscale");
    }

    public synchronized void applyZScale(String zMode)
    {
        if( previewBuff == null ) {
            pixImage = null;
            repaint();
            return;
        }

	previewBuff.Zscale();
	double minv, maxv;
	if (zMode.equals("Zmax")) {
	    minv = (double)previewBuff.ZscaleRange[0];
	    maxv = previewBuff.max;
	} else if (zMode.equals("Zmin")) {
	    minv = previewBuff.min;
	    maxv = (double)previewBuff.ZscaleRange[1];
	} else {
	    minv = (double)previewBuff.ZscaleRange[0];
            maxv = (double)previewBuff.ZscaleRange[1];
	}

        previewData = previewBuff.image;
        int ny = previewData.length;
        if (ny == 0) return;
        int nx = previewData[0].length;
        byte[][] pixScaled = new byte[ny][nx];
        double range = maxv - minv;

        for(int i=0; i < ny; i++) {
	    for (int j = 0; j < nx; j++) {
		double f = ( previewData[i][j] - minv )/range;
                if( f <= 0 )
                    pixScaled[i][j] = 0;
                else if( f >= 1 )
                    pixScaled[i][j] = (byte)255;
                else
                    pixScaled[i][j] = (byte)Math.round(f*255);
            }
	}

	previewExpand(pixScaled);
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();

    }

//--------------------------------------------------------------------------------

    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        if( pixImage == null ) return;

	g.drawImage( pixImage, 0, 0, null );
    }
    
}
