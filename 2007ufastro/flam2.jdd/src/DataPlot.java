package ufjdd;

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javaUFLib.*;

public class DataPlot extends JFrame {
   static PlotPanel thePlot;
   int xdim, ydim;
   JPopupMenu menu;
   JMenuItem printItem;
   float[] x, y, oldX, oldY;
   String opts, oldOpts;
   JButton plotButton, quitButton, browseFiles, addText;
   JButton multiButton, optsButton, histButton;
   JRadioButton plotData, plotFiles, xLin, xLog, yLin, yLog;
   ButtonGroup plotType, xLinLog, yLinLog;
   JTextArea fileList, psymList, colorList;
   JTextField titleField, xtitleField, ytitleField, xcolField, ycolField;
   JTextField charSizeField, xMinField, xMaxField, yMinField, yMaxField;
   JTextField bgColorField, histField;
   int xticks=0, yticks=0, xminor=0, yminor=0;
   float xtickInt=0, ytickInt=0, xtickLen=0, ytickLen=0, symsize=0;
   float[] xtickVals, ytickVals, xmargin, ymargin, position;
   String[] xtickNames, ytickNames;
   String fontName="", axesColor="";
   public boolean autoOPlot = false;
   float[][] oxs, oys;
   String[] oOpts, psymOpts, colorOpts;
   int nplot = 0;

   public DataPlot() {
      this(540, 400, "DataPlot");
   }

   public DataPlot(String title) {
      this(540, 400, title);
   }

   public DataPlot(int xdim, int ydim) {
      this(xdim, ydim, "DataPlot");
   }

   public DataPlot(int xdim, int ydim, String title) {
      //super(title);
      this.xdim = xdim;
      this.ydim = ydim;
      this.opts = "";
      this.xtickVals = new float[1];
      this.ytickVals = new float[1];
      this.xmargin = new float[1];
      this.ymargin = new float[1];
      this.position = new float[1];
      this.xtickNames = new String[1];
      this.xtickNames[0] = "";
      this.ytickNames = new String[1];
      this.ytickNames[0] = "";
      setSize(xdim + 160, ydim + 128);

      addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
	   DataPlot.this.dispose();
        }
      });
      
      addComponentListener(new ComponentAdapter() {
	public void componentResized(ComponentEvent ev) {
	   DataPlot ufp = (DataPlot)ev.getSource();
	   ufp.thePlot.resizePlot(ufp.getWidth()-4, ufp.getHeight()-26);
        }
      });


      plotData = new JRadioButton("Initial Data", true);
      plotFiles = new JRadioButton("Plot File(s)");
      plotType = new ButtonGroup();
      plotType.add(plotData);
      plotType.add(plotFiles);
      fileList = new JTextArea(5, 14);
      browseFiles = new JButton("Browse");

      browseFiles.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   JFileChooser jfc = new JFileChooser();
	   jfc.setMultiSelectionEnabled(true);
	   int returnVal = jfc.showOpenDialog((Component)ev.getSource());
	   if (returnVal == JFileChooser.APPROVE_OPTION) {
	      File[] f = jfc.getSelectedFiles();
	      for (int j = 0; j < f.length; j++)
		fileList.append(f[j].getAbsolutePath() + "\n");
	   }
	}
      });

      titleField = new JTextField(10);
      xtitleField = new JTextField(9);
      ytitleField = new JTextField(9);
      psymList = new JTextArea(5,4);
      colorList = new JTextArea(5,8);
      xcolField = new JTextField(2);
      ycolField = new JTextField(2);

      this.thePlot = new PlotPanel(xdim, ydim, this);

      charSizeField = new JTextField(2);
      xMinField = new JTextField(5);
      xMaxField = new JTextField(5);
      yMinField = new JTextField(5);
      yMaxField = new JTextField(5);
      addText = new JButton("Add Text");
      xLin = new JRadioButton("Linear", true);
      xLog = new JRadioButton("Log");
      xLinLog = new ButtonGroup();
      xLinLog.add(xLin);
      xLinLog.add(xLog);
      yLin = new JRadioButton("Linear", true);
      yLog = new JRadioButton("Log");
      yLinLog = new ButtonGroup();
      yLinLog.add(yLin);
      yLinLog.add(yLog);
      bgColorField = new JTextField(8);
      multiButton = new JButton("Multiplot");

      multiButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   final JFrame multiFrame = new JFrame("Multiplot");
	   multiFrame.setSize(180, 70);
	   JPanel multiPanel = new JPanel();
	   multiPanel.setPreferredSize(new Dimension(180, 70));
	   multiPanel.add(new JLabel("Rows:"));
	   final JTextField rowsField = new JTextField(2);
	   multiPanel.add(rowsField);
	   multiPanel.add(Box.createRigidArea(new Dimension(20, 5)));
	   multiPanel.add(new JLabel("Cols:"));
	   final JTextField colsField = new JTextField(2);
	   multiPanel.add(colsField);
	   JButton apply = new JButton("Apply");
           apply.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ev) {
		int rows = 1, cols = 1;
		if (!rowsField.getText().equals(""))
		   rows = Integer.parseInt(rowsField.getText());
                if (!colsField.getText().equals(""))
                   cols = Integer.parseInt(colsField.getText());
		multi(0, cols, rows);
                multiFrame.dispose();
              }
           });
	   multiPanel.add(apply);
	   JButton cancel = new JButton("Cancel");
           cancel.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ev) {
		multiFrame.dispose();
	      }
	   });
	   multiPanel.add(cancel);
	   multiFrame.getContentPane().add(multiPanel);
	   multiFrame.pack();
	   multiFrame.setVisible(true);
	}
      });

      optsButton = new JButton("Options");
      optsButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   final JFrame optsFrame = new JFrame("Options");
	   optsFrame.setSize(440, 280);
	   JPanel optsPanel = new JPanel();
	   optsPanel.add(new JLabel("X ticks:"));
	   final JTextField optsXTicks = new JTextField(2);
	   optsXTicks.setText(""+xticks);
	   optsPanel.add(optsXTicks);
           optsPanel.add(new JLabel("Y ticks:"));
           final JTextField optsYTicks = new JTextField(2);
	   optsYTicks.setText(""+yticks);
           optsPanel.add(optsYTicks);
           optsPanel.add(new JLabel("X tick interval:"));
           final JTextField optsXTickInt = new JTextField(4);
	   optsXTickInt.setText(""+xtickInt);
           optsPanel.add(optsXTickInt);
           optsPanel.add(new JLabel("Y tick interval:"));
           final JTextField optsYTickInt = new JTextField(4);
	   optsYTickInt.setText(""+ytickInt);
           optsPanel.add(optsYTickInt);
           optsPanel.add(new JLabel("X tick values:"));
           final JTextArea optsXTickVals = new JTextArea(4,9);
	   for (int j = 0; j < xtickVals.length; j++)
	      optsXTickVals.append(""+xtickVals[j]+"\n");
           optsPanel.add(new JScrollPane(optsXTickVals));
           optsPanel.add(new JLabel("Y tick values:"));
           final JTextArea optsYTickVals = new JTextArea(4,9);
           for (int j = 0; j < ytickVals.length; j++)
              optsYTickVals.append(""+ytickVals[j]+"\n");
           optsPanel.add(new JScrollPane(optsYTickVals));
           optsPanel.add(new JLabel("X tick names:"));
           final JTextArea optsXTickNames = new JTextArea(4,9);
           for (int j = 0; j < xtickNames.length; j++)
              optsXTickNames.append(xtickNames[j]+"\n");
           optsPanel.add(new JScrollPane(optsXTickNames));
           optsPanel.add(new JLabel("Y tick names:"));
           final JTextArea optsYTickNames = new JTextArea(4,9);
           for (int j = 0; j < ytickNames.length; j++)
              optsYTickNames.append(ytickNames[j]+"\n");
           optsPanel.add(new JScrollPane(optsYTickNames));
           optsPanel.add(new JLabel("X tick length:"));
           final JTextField optsXTickLen = new JTextField(3);
	   optsXTickLen.setText(""+xtickLen);
           optsPanel.add(optsXTickLen);
           optsPanel.add(new JLabel("Y tick length:"));
           final JTextField optsYTickLen = new JTextField(3);
	   optsYTickLen.setText(""+ytickLen);
           optsPanel.add(optsYTickLen);
           optsPanel.add(new JLabel("X minor:"));
           final JTextField optsXMinor = new JTextField(2);
	   optsXMinor.setText(""+xminor);
           optsPanel.add(optsXMinor);
           optsPanel.add(new JLabel("Y minor:"));
           final JTextField optsYMinor = new JTextField(2);
	   optsYMinor.setText(""+yminor);
           optsPanel.add(optsYMinor);
	   optsPanel.add(new JLabel("Symbol Size:"));
	   final JTextField optsSymSize = new JTextField(3);
	   optsSymSize.setText(""+symsize);
	   optsPanel.add(optsSymSize);
	   optsPanel.add(new JLabel("Font:"));
	   final JTextField optsFont = new JTextField(8);
	   optsFont.setText(fontName);
	   optsPanel.add(optsFont);
           optsPanel.add(new JLabel("X margin: L"));
           final JTextField optsXMargin1 = new JTextField(3);
           final JTextField optsXMargin2 = new JTextField(3);
	   if (xmargin.length == 2) {
	      optsXMargin1.setText(""+xmargin[0]);
	      optsXMargin2.setText(""+xmargin[1]);
	   }
           optsPanel.add(optsXMargin1);
	   optsPanel.add(new JLabel("R"));
           optsPanel.add(optsXMargin2);
           optsPanel.add(new JLabel("Y margin: T"));
           final JTextField optsYMargin1 = new JTextField(3);
           final JTextField optsYMargin2 = new JTextField(3);
           if (ymargin.length == 2) {
              optsYMargin1.setText(""+ymargin[0]);
              optsYMargin2.setText(""+ymargin[1]);
           }
           optsPanel.add(optsYMargin1);
           optsPanel.add(new JLabel("B"));
           optsPanel.add(optsYMargin2);
	   optsPanel.add(new JLabel("Position: L"));
           final JTextField optsPos1 = new JTextField(3);
           final JTextField optsPos2 = new JTextField(3);
           final JTextField optsPos3 = new JTextField(3);
           final JTextField optsPos4 = new JTextField(3);
	   if (position.length == 4) {
	      optsPos1.setText(""+position[0]);
              optsPos2.setText(""+position[1]);
              optsPos3.setText(""+position[2]);
              optsPos4.setText(""+position[3]);
	   }
	   optsPanel.add(optsPos1);
	   optsPanel.add(new JLabel("T"));
           optsPanel.add(optsPos2);
           optsPanel.add(new JLabel("R"));
           optsPanel.add(optsPos3);
           optsPanel.add(new JLabel("B"));
           optsPanel.add(optsPos4);
	   optsPanel.add(new JLabel("Axes Color:"));
	   final JTextField optsAxesColor = new JTextField(9);
	   optsAxesColor.setText(axesColor);
	   optsPanel.add(optsAxesColor);
	   JButton setPrefs = new JButton("Set Options");
           setPrefs.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ev) {
		String temp;
		String[] temps;
		temp = optsXTicks.getText();
		if (!temp.equals("")) xticks = Integer.parseInt(temp);
                temp = optsYTicks.getText();
                if (!temp.equals("")) yticks = Integer.parseInt(temp);
                temp = optsXTickInt.getText();
                if (!temp.equals("")) xtickInt = Float.parseFloat(temp);
                temp = optsYTickInt.getText();
                if (!temp.equals("")) ytickInt = Float.parseFloat(temp);
                temp = optsXTickLen.getText();
                if (!temp.equals("")) xtickLen = Float.parseFloat(temp);
                temp = optsYTickLen.getText();
                if (!temp.equals("")) ytickLen = Float.parseFloat(temp);
                temp = optsXMinor.getText();
                if (!temp.equals("")) xminor = Integer.parseInt(temp);
                temp = optsYMinor.getText();
                if (!temp.equals("")) yminor = Integer.parseInt(temp);
		temp = optsSymSize.getText();
		if (!temp.equals("")) symsize = Float.parseFloat(temp);
		temp = optsFont.getText();
		if (!temp.equals("")) fontName = temp;
		temp = optsAxesColor.getText();
		if (!temp.equals("")) axesColor = temp;
		temps = optsXTickVals.getText().split("\n");
		if (temps.length != 0)
		   if (!(temps.length == 1 && temps[0].equals(""))) {
		      xtickVals = new float[temps.length];
		      for (int j = 0; j < temps.length; j++)
		        xtickVals[j] = Float.parseFloat(temps[j]);
		   } 
		if (temps.length != 0)
                   temps = optsYTickVals.getText().split("\n");
                   if (!(temps.length == 1 && temps[0].equals(""))) {
                      ytickVals = new float[temps.length];
                      for (int j = 0; j < temps.length; j++)
                        ytickVals[j] = Float.parseFloat(temps[j]);
                   }
		if (temps.length != 0)
                   temps = optsXTickNames.getText().split("\n");
                   if (!(temps.length == 1 && temps[0].equals(""))) {
                      xtickNames = temps; 
                   }
		if (temps.length != 0)
                   temps = optsYTickNames.getText().split("\n");
                   if (!(temps.length == 1 && temps[0].equals(""))) {
                      ytickNames = temps;
		   }
		if (!optsXMargin1.getText().equals("") && !optsXMargin2.getText().equals("")) {
		   xmargin = new float[2];
		   xmargin[0] = Float.parseFloat(optsXMargin1.getText());
		   xmargin[1] = Float.parseFloat(optsXMargin2.getText());
		} else xmargin = new float[1];
                if (!optsYMargin1.getText().equals("") && !optsYMargin2.getText().equals("")) {
                   ymargin = new float[2];
                   ymargin[0] = Float.parseFloat(optsYMargin1.getText());
                   ymargin[1] = Float.parseFloat(optsYMargin2.getText());
                } else ymargin = new float[1];
		if (!optsPos1.getText().equals("") && !optsPos2.getText().equals("") && !optsPos3.getText().equals("") && !optsPos4.getText().equals("")) {
		   position = new float[4];
		   position[0] = Float.parseFloat(optsPos1.getText());
                   position[1] = Float.parseFloat(optsPos2.getText());
                   position[2] = Float.parseFloat(optsPos3.getText());
                   position[3] = Float.parseFloat(optsPos4.getText());
		} else position = new float[1];
                optsFrame.dispose();
              }
           });
	   JButton cancelPrefs = new JButton("Cancel");
           cancelPrefs.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ev) {
                optsFrame.dispose();
              }
           });
	   optsPanel.add(setPrefs);
	   optsPanel.add(cancelPrefs);

	   optsPanel.setPreferredSize(new Dimension(440, 280));
	   optsFrame.getContentPane().add(optsPanel);
	   optsFrame.pack();
	   optsFrame.setVisible(true);
	}
      });

      histButton = new JButton("Histogram");
      histButton.setToolTipText("Click to create a Histogram.");

      histButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (plotData.isSelected()) {
              String tempOpts = readOpts();
              String[] colorOpts;
              String temp;
              temp = colorList.getText();
              if (!temp.trim().equals("")) {
		colorOpts = temp.trim().split("\n");
                if (colorOpts[0].indexOf(",") == -1) colorOpts[0] = colorOpts[0].replaceAll(" ",",");
                tempOpts+="*color="+colorOpts[0]+", ";
                int colorLen = colorOpts[0].split(",").length;
                if (colorLen < 3) {
		   for (int l = 0; l < 3-colorLen; l++) tempOpts+=colorOpts[0].substring(colorOpts[0].lastIndexOf(",")+1)+", ";
		}
              } else {
                colorOpts = new String[1];
                colorOpts[0] = "";
              }
	      int[] h = hist(tempOpts+opts);
	      if (autoOPlot) {
		for (int j = 0; j < oxs.length; j++) {
                   tempOpts = "";
                   if (colorOpts.length > j+1) {
		      if (!colorOpts[j+1].equals("")) {
			if (colorOpts[j+1].indexOf(",") == -1) colorOpts[j+1] = colorOpts[j+1].replaceAll(" ",",");
                        tempOpts+="*color="+colorOpts[j+1]+", ";
                        int colorLen = colorOpts[j+1].split(",").length;
                        if (colorLen < 3) {
			   for (int l = 0; l < 3-colorLen; l++) tempOpts+=colorOpts[j+1].substring(colorOpts[j+1].lastIndexOf(",")+1)+", ";
			}
                      }
                   }
		   overplot(oxs[j], oys[j], tempOpts+oOpts[j]);
		}
	      }
	   }
	   else {
	    int col = 0;
	    oldX = x;
	    oldY = y;
	    oldOpts = opts;
            String temp = histField.getText();
            if (!temp.trim().equals("")) col = Integer.parseInt(temp)-1;
            String file = fileList.getText().split("\n")[0];
	    readFile(file, -1, col);
	    final JFrame histFrame = new JFrame("Histogram");
	    histFrame.setSize(180, 100);
	    JPanel histPanel = new JPanel();
	    histPanel.setPreferredSize(new Dimension(180,100));
	    histPanel.add(new JLabel("Number of bins: "));
	    final JTextField nbins = new JTextField(3);
	    histPanel.add(nbins);
	    histPanel.add(new JLabel("Bin size: "));
	    final JTextField binsize = new JTextField(4);
	    histPanel.add(binsize);
	    final JCheckBox isFilled = new JCheckBox("Filled");
	    histPanel.add(isFilled);
	    JButton plotHist = new JButton("Plot");
	    histPanel.add(plotHist);
            plotHist.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ev) {
		String histOpts = "";
		if (!nbins.getText().trim().equals(""))
		   histOpts+="*nbins="+nbins.getText()+", ";
                if (!binsize.getText().trim().equals(""))
                   histOpts+="*binsize="+binsize.getText()+", ";
		if (isFilled.isSelected()) histOpts+="*fill, ";
                String temp = colorList.getText();
		String[] colorOpts;
                if (!temp.trim().equals("")) {
                   colorOpts = temp.trim().split("\n");
                   if (colorOpts[0].indexOf(",") == -1) colorOpts[0] = colorOpts[0].replaceAll(" ",",");
                   histOpts+="*color="+colorOpts[0]+", ";
                   int colorLen = colorOpts[0].split(",").length;
                   if (colorLen < 3) {
                      for (int l = 0; l < 3-colorLen; l++) histOpts+=colorOpts[0].substring(colorOpts[0].lastIndexOf(",")+1)+", ";
                   }
                }
                histFrame.dispose();
		int[] h = hist(y, histOpts + readOpts()); 
		x = oldX;
		y = oldY;
		opts = oldOpts;
              }
            });
	    JButton cancel = new JButton("Cancel");
            cancel.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ev) {
                histFrame.dispose();
		y = oldY;
		opts = oldOpts;
              }
            });
	    histPanel.add(cancel);
	    histFrame.getContentPane().add(histPanel);
	    histFrame.pack();
	    histFrame.setVisible(true);
	   }
	}
      });

      histField = new JTextField(2);

   }

   public void initOpts(String s) {
      this.opts = s;
   }

   public void showPlot() {
      this.setSize(xdim + 4, ydim + 26);
      this.validate();
      this.repaint();
      this.setVisible(true);
   }

   public void plot(String s) {
      this.setVisible(true);
      if (x == null) {
	x = new float[y.length];
	for (int j = 0; j < x.length; j++) x[j] = j;
      }
      thePlot.plot(x, y, s);
   }

   public void plot(float[] x, float[] y, String s) {
      this.setVisible(true);
      this.x = x;
      this.y = y;
      this.opts = s;
      thePlot.plot(x, y, readOpts() + readOPlotOpts(0) + s);
      this.nplot = 0;
   }

   public void plot(float[] y, String s) {
      this.setVisible(true);
      float[] x = new float[y.length];
      for (int j = 0; j < x.length; j++) x[j] = j;
      this.x = x;
      this.y = y;
      this.opts = s;
      thePlot.plot(x, y, readOpts() +readOPlotOpts(0) + s);
      this.nplot = 0;
   }

   public void plot(int[] x, String s) {
      float[] x2 = new float[x.length];
      for (int j = 0; j < x.length; j++) x2[j] = (float)x[j];
      this.plot(x2, s);
   }

   public void plot(int[] x, int[] y, String s) {
      float[] x2 = new float[x.length];
      float[] y2 = new float[y.length];
      for (int j = 0; j < x.length; j++) x2[j] = (float)x[j];
      for (int j = 0; j < y.length; j++) y2[j] = (float)y[j];
      this.plot(x2, y2, s);
   }

   public void plot(String file, int xcol, int ycol, String s) {
      this.setVisible(true);
      readFile(file, xcol, ycol);
      this.opts = s;
      thePlot.plot(x, y, readOpts() + readOPlotOpts(0) + s);
      this.nplot = 0;
   }

   public void plot(String file, int ycol, String s) {
      this.plot(file, -1, ycol, s);
   }

   public void usersym(int[] usymxs, int[] usymys) {
      thePlot.usersym(usymxs, usymys);
   }

   public void overplot(float[] x, float[] y, String s) {
      this.nplot++;
      thePlot.overplot(x, y, readOPlotOpts(this.nplot) + s);
   }

   public void xyouts(float xc, float yc, String text, String s) {
      thePlot.xyouts(xc, yc, text, s);
   }

   public void multi(int curr, int col, int row) {
      thePlot.multi(curr, col, row);
   }

   public void setOPlots(int num, float[] x, float[] y, String s) {
      this.oxs = new float[num][];
      this.oys = new float[num][];
      this.oOpts = new String[num];
      this.oxs[0] = x;
      this.oys[0] = y;
      this.oOpts[0] = s;
      this.autoOPlot = true;
   }

   public void setOPlots(float[] x, float[] y, String s, int n) {
      if (n < this.oxs.length) {
        this.oxs[n] = x;
        this.oys[n] = y;
        this.oOpts[n] = s;
      }
   }

   public int[] hist(String s) {
      this.setVisible(true);
      this.nplot = 0;
      return thePlot.hist(y, readOPlotOpts(0) + readOpts() + s);
   }

   public int[] hist(float[] x, String s) {
      this.setVisible(true);
      this.y = x;
      this.opts = s;
      this.nplot = 0;
      return thePlot.hist(x, readOPlotOpts(0) + readOpts() + s);
   }

   public int[] hist(float[][] x, String s) {
      this.setVisible(true);
      float[] y = new float[x.length*x[0].length];
      for (int j = 0; j < x.length; j++) {
	for (int l = 0; l < x[0].length; l++) y[j*x[0].length+l] = x[j][l];
      }
      this.y = y;
      this.opts = s;
      this.nplot = 0;
      return thePlot.hist(y, readOPlotOpts(0) + readOpts() + s);
   }

   public int[] hist(double[][] x, String s) {
      this.setVisible(true);
      float[] y = new float[x.length*x[0].length];
      for (int j = 0; j < x.length; j++) {
	for (int l = 0; l < x[0].length; l++) y[j*x[0].length+l] = (float)x[j][l];
      }
      this.y = y;
      this.opts = s;
      this.nplot = 0;
      return thePlot.hist(y, readOPlotOpts(0) + readOpts() + s);
   }

   public int[] hist(int[] x, String s) {
      this.setVisible(true);
      float[] y = new float[x.length];
      for (int j = 0; j < x.length; j++) y[j] = (float)x[j];
      this.y = y;
      this.opts = s;
      this.nplot = 0;
      return thePlot.hist(y, readOPlotOpts(0) + readOpts() + s);
   }

   public int[] hist(int[][] x, String s) {
      this.setVisible(true);
      float[] y = new float[x.length*x[0].length];
      for (int j = 0; j < x.length; j++) {
        for (int l = 0; l < x[0].length; l++)
	   y[j*x[0].length+l] = (float)x[j][l];
      }
      this.y = y;
      this.opts = s;
      this.nplot = 0;
      return thePlot.hist(y, readOPlotOpts(0) + readOpts() + s);
   }

   public float getYmax() {
      return thePlot.getYmax();
   }

   public void readFile(String file, int xcol, int ycol) {
      float[] x, y;
      Vector v = new Vector();
      String currLine = " ";
      String[] temp;
      int n;
      try {
        BufferedReader r = new BufferedReader(new FileReader(file));
        while (currLine != null) {
           currLine = r.readLine();
           if (currLine != null) v.add(currLine);
        }
      } catch(IOException e) {
        System.out.println("Error Reading From File.");
      }
      n = v.size();
      if (n == 0) {
        System.out.println("File is Empty");
        return;
      }
      x = new float[n];
      y = new float[n];
      for (int j = 0; j < n; j++) {
        currLine = (String)v.remove(0);
        currLine = currLine.replaceAll("\t", " ");
        while (currLine.indexOf("  ") != -1)
           currLine=currLine.replaceAll("  ", " ");
        temp = currLine.trim().split(" ");
        if (xcol != -1) x[j] = Float.parseFloat(temp[xcol]);
        else x[j] = j;
        y[j] = Float.parseFloat(temp[ycol]);
      }
      this.x = x;
      this.y = y;
   }

   public String readOpts() {
      String s = "";
      String temp;
      temp = titleField.getText();
      if (!temp.equals("")) {
	s+="*title="+temp+", ";
	this.setTitle("DataPlot: " + temp);
      }
      temp = xtitleField.getText();
      if (!temp.equals("")) s+="*xtitle="+temp+", ";
      temp = ytitleField.getText();
      if (!temp.equals("")) s+="*ytitle="+temp+", ";
      temp = charSizeField.getText();
      if (!temp.trim().equals("")) s+="*charsize="+temp+", ";
      temp = xMinField.getText();
      if (!temp.trim().equals("")) s+="*xminval="+temp+", ";
      temp = xMaxField.getText();
      if (!temp.trim().equals("")) s+="*xmaxval="+temp+", ";
      temp = yMinField.getText();
      if (!temp.trim().equals("")) s+="*yminval="+temp+", ";
      temp = yMaxField.getText();
      if (!temp.trim().equals("")) s+="*ymaxval="+temp+", ";
      if (xLog.isSelected()) s+="*xlog, ";
      if (yLog.isSelected()) s+="*ylog, ";
      if (xLin.isSelected()) s+="*xlinear, ";
      if (yLin.isSelected()) s+="*ylinear, ";
      temp = bgColorField.getText();
      if (!temp.trim().equals("")) {
	if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
	s+="*background="+temp+", ";
	int colorLen = temp.split(",").length;
	if (colorLen < 3) {
	   for (int l = 0; l < 3-colorLen; l++) s+=temp.substring(temp.lastIndexOf(",")+1)+", ";
	}
      }
      if (xticks != 0) s+="*xticks="+xticks+", ";
      if (yticks != 0) s+="*yticks="+yticks+", ";
      if (xminor != 0) s+="*xminor="+xminor+", ";
      if (yminor != 0) s+="*yminor="+yminor+", ";
      if (xtickInt != 0) s+="*xtickinterval="+xtickInt+", ";
      if (ytickInt != 0) s+="*ytickinterval="+ytickInt+", ";
      if (xtickLen != 0) s+="*xticklen="+xtickLen+", ";
      if (ytickLen != 0) s+="*yticklen="+ytickLen+", ";
      if (!fontName.equals("")) s+="*font="+fontName+", ";
      if (xtickVals.length != 0)
	if (!(xtickVals.length == 1 && xtickVals[0] == 0)) {
	   s+="*xtickv=[";
	   for (int j = 0; j < xtickVals.length-1; j++) s+=xtickVals[j]+",";
	   s+=xtickVals[xtickVals.length-1]+"], ";
        }
      if (ytickVals.length != 0)
        if (!(ytickVals.length == 1 && ytickVals[0] == 0)) {
           s+="*ytickv=[";
           for (int j = 0; j < ytickVals.length-1; j++) s+=ytickVals[j]+",";
           s+=ytickVals[ytickVals.length-1]+"], ";
        }
      if (xtickNames.length != 0)
        if (!(xtickNames.length == 1 && xtickNames[0].equals(""))) {
           s+="*xtickname=[";
           for (int j = 0; j < xtickNames.length-1; j++) s+=xtickNames[j]+",";
           s+=xtickNames[xtickNames.length-1]+"], ";
        }
      if (ytickNames.length != 0)
        if (!(ytickNames.length == 1 && ytickNames[0].equals(""))) {
           s+="*ytickname=[";
           for (int j = 0; j < ytickNames.length-1; j++) s+=ytickNames[j]+",";
           s+=ytickNames[ytickNames.length-1]+"], ";
        }
      if (xmargin.length == 2) s+="*xmargin=["+xmargin[0]+","+xmargin[1]+"], ";
      if (ymargin.length == 2) s+="*ymargin=["+ymargin[0]+","+ymargin[1]+"], ";
      if (position.length == 4) {
	s+="*position=["+position[0]+","+position[1]+","+position[2]+","+position[3]+"], ";
      }
      if (!axesColor.trim().equals("")) {
        if (axesColor.indexOf(",") == -1)
	   axesColor = axesColor.replaceAll(" ", ",");
        s+="*axescolor="+axesColor+", ";
      }

      return s;
   }

   public String readOPlotOpts(int j) {
      String oplotOpts = "";
      psymOpts = psymList.getText().trim().split("\n");
      colorOpts = colorList.getText().trim().split("\n");
      if (psymOpts.length > j) {
	if (!psymOpts[j].equals(""))
	   oplotOpts+="*psym="+psymOpts[j]+", ";
	if (symsize != 0)
	   oplotOpts+="*symsize="+symsize+", ";
      }
      if (colorOpts.length > j) {
	if (!colorOpts[j].equals("")) {
	   if (colorOpts[j].indexOf(",") == -1) colorOpts[j] = colorOpts[j].replaceAll(" ",",");
           oplotOpts+="*color="+colorOpts[j]+", ";
           int colorLen = colorOpts[j].split(",").length;
           if (colorLen < 3) {
	      for (int l = 0; l < 3-colorLen; l++) oplotOpts+=colorOpts[j].substring(colorOpts[j].lastIndexOf(",")+1)+", ";
	   }
	}
      }
      return oplotOpts;
   }

   public static void main(String[] args) {
      DataPlot p = new DataPlot();
      p.showPlot();
   }

} 
