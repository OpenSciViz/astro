package ufjdd;
/**
 * Title:        Java Data Display (JDD): DataAccessPanel.java (to access data in frame acq. server buffers)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Frank Varosi, Ziad Saleh, Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  Extends class UFLibPanel to use _socket attrib. and connectToServer() method.
 */
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;
import java.io.*;
import java.nio.channels.*;

import ufjca.*;

class F2DataAccessPanel extends DataAccessPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: F2DataAccessPanel.java,v 1.11 2006/12/13 19:48:02 warner Exp $";

    private UFCAToolkit.MonitorListener ml;

    public F2DataAccessPanel(ImagePanel[] imagePanels) {
	super(imagePanels);
	setMonitorListener();
	this.imagePanels = imagePanels;
	this.socTimeout = 0;
        frameConfig = getFrameConfig();
	this.initialize();
	changeMode(MODE_NOHOST);
	connectButton.setBackground(new JButton().getBackground());
    }

    public F2DataAccessPanel(String host, int port, String serverGreeting, ImagePanel[] imagePanels)
    {
	super( host, port, serverGreeting, imagePanels);
        setMonitorListener();
	this.imagePanels = imagePanels;
	this.serverName = "Data Acq. Server";
	this.socTimeout = 0;
	this.connectToServer();
	this.initialize();
        connectButton.setBackground(new JButton().getBackground());
	if (serverGreeting.toLowerCase().trim().equals("replication"))
	    changeMode(MODE_REPLICANT);
	else
	    changeMode(MODE_REGULAR);
    }
    
    public void changeMode (int newMode) {
	if (newMode < 1 || newMode > 4) {
	    System.err.println("DataAccessPanel.changeMode> Invalid mode: "+newMode);
	} else {
	    mode = newMode;
	    try {
		keepGoing = false;
		if (_socket != null)
		    _socket.close();
		_socket = null;
		if (ml != null) 
		    UFCAToolkit.removeMonitor(EPICSPrefix + "sad:EDTFRAME",ml);
		//UFCAToolkit.removeAllMonitors();
	    } catch (Exception e) {
		System.err.println("DataAccessPanel.changeMode> Error trying to close socket: "+e.toString());
	    }
	    if (mode == MODE_REPLICANT) {
		serverHandshake = "replication";
		if (connectToServer()) {
		    replicantThread = new Thread(){
			    public void run() {
				keepGoing = true;
				while (keepGoing) {
				    try {
					UFFrameConfig bfc = new UFFrameConfig(2048,2048);
					bfc.coAdds = 1;
					bfc.frameCoadds = 1;
					frameConfig = bfc;
					UFProtocol ufp = null;
					UFInts ufi = null;
				
					while (keepGoing && ufi == null) {
					    ufp = UFProtocol.createFrom(_socket);
					    if (ufp == null) {
						System.err.println("DataAccess.recvBuff> UFProtocol Read ERROR for buffer: " + bfc.name());
					    }
					    if (ufp.typeId() == UFProtocol.MsgTyp._Ints_)
						ufi = (UFInts)ufp;
					}
				
					System.out.println("DataAccess.Thread.run> New frame recvd.");
					System.out.println(ufi.numVals());
				
					ImageBuffer imgBuffer = new ImageBuffer(bfc, ufi);
					updateFrames(imgBuffer);
				    } catch (Exception e) {
					System.err.println("DataAccess.Thread.run> "+e.toString());
				    }
				}
			    }
			};
		    replicantThread.start();
		} else {
		    System.err.println("DataAccessPanel.changeMode> Error connecting to server!");
		}
	    } else  if (mode != MODE_NOHOST) {
		if (mode == MODE_REGULAR) {
		    serverHandshake = "normal";
		    connectToServer();
		}
		lastMonVal = "0";

		UFCAToolkit.addMonitor(EPICSPrefix + "sad:EDTFRAME",ml);
		System.out.println("DataAccessPanel.initialize> Created monitor for "+EPICSPrefix+"sad:EDTFRAME");
		if (epicsThread == null || !epicsThread.isAlive()) {
		    epicsThread = new Thread(){
			    public void run() { 
				while (true) {
				    UFCAToolkit.startMonitorLoop();
				    try {Thread.sleep(500);}
				    catch (Exception e) {}
				}
			    }
			};
		    epicsThread.start();
		}
	    }// fi
	}
    }

    public boolean connectToServer()
    {
	if( super.connectToServer() ) {
	    frameConfig = getFrameConfig();
	    return true;
	}
	else {
	    return false;
	}
    }

    public UFFrameConfig getFrameConfig() {

	UFFrameConfig ufc = new UFFrameConfig(2048,2048);
	ufc.coAdds = 1;
	ufc.frameCoadds = 1;
	frameConfig = ufc;
	return ufc;


//         // Send the agent a timestamp with the command FC to get Frame Configuration
//         UFTimeStamp uft = new UFTimeStamp("FC");
//         UFFrameConfig fc = null;
        
//         if( uft.sendTo(_socket) <= 0 ) {
//             System.err.println("DataAccess.getFrameConfig> UFTimeStamp send ERROR");
// 	    return null;
//         }

//         if( (fc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
//             System.err.println("DataAccess.getFrameConfig> UFFrameConfig recv ERROR");
// 	    return null;
//         }

// 	System.out.println("DataAccess.getFrameConfig> (width=" + fc.width + ",  height=" + fc.height
// 			   + ") ---> " + fc.name());
// 	frameConfig = fc;
// 	return fc;
    }

    public synchronized boolean request( String buffName )
    {

	String [] strs = {"-frame","flam:FullImage"};
        UFStrings ufs = new UFStrings("FLAMJDD",strs);


	if (_socket == null) connectToServer();
	if (_socket == null) { System.err.println("DataAccess.request> Null socket"); return false;}
        if( ufs.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.request(" + buffName + ")> UFStrings send ERROR");
            return false;
        }
	//flush socket output stream
	try {
	    _socket.getOutputStream().flush();
	    Thread.sleep(100);
	} catch (Exception e) {
	    System.err.println("DataAccess.request(" + buffName+")> Socket outputstream flush ERROR");
	    return false;
	}
	//get echo
	if (UFProtocol.createFrom(_socket) == null) {
	  System.err.println("DataAccess.request(" + buffName + ")> UFStrings echo recv ERROR");
	  return false;	    
	}
	else return true;



// 	if( buffName == null ) return false;
//         // Send the agent a timestamp with the desired Buffer Name:
//         UFTimeStamp uft = new UFTimeStamp(buffName);

//         if( uft.sendTo(_socket) <= 0 ) {
//             System.err.println("DataAccess.request(" + buffName + ")> UFTimeStamp send ERROR");
//             return false;
//         }
// 	else return true;
    }

    public synchronized ImageBuffer recvBuff()
    {

        UFFrameConfig bfc = new UFFrameConfig(2048,2048);
	bfc.coAdds = 1;
	bfc.frameCoadds = 1;
	frameConfig = bfc;
	UFProtocol ufp = null;
	UFInts ufi = null;
            
	while (ufi == null) {
	    ufp = UFProtocol.createFrom(_socket);
	    if (ufp == null) {
		System.err.println("DataAccess.recvBuff> UFProtocol Read ERROR for buffer: " + bfc.name());
		return null;
	    }
	    if (ufp.typeId() == UFProtocol.MsgTyp._Ints_) 
		ufi = (UFInts)ufp;
	}
	
	System.out.println(ufi.numVals());

	ImageBuffer imgBuffer = new ImageBuffer(bfc, ufi);
	return imgBuffer;


//         UFFrameConfig bfc = null;
//         try {
//             if( (bfc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
//                 System.err.println("DataAccess.recvBuff> UFFrameConfig recv ERROR");
//                 return null;
//             }
//         } catch( ClassCastException cce ) {
//             System.err.println("DataAccess.recvBuff> Class Cast Exception " + cce);
//             return null;
//         }
        
//         if( bfc.name().indexOf("ERROR") >= 0 ) {
//             System.out.println("DataAccess.recvBuff> " + bfc.name());
//             return null;
//         }
// 	else {
// 	    frameConfig = bfc;
// 	    UFInts ufi = null;
            
//             if( (ufi = (UFInts)UFProtocol.createFrom(_socket)) == null )
//                 System.err.println("DataAccess.recvBuff> UFInts Read ERROR for buffer: " + bfc.name());

//             ImageBuffer imgBuffer = new ImageBuffer(bfc, ufi);
//             return imgBuffer;
//         }
    }

    public void updateFrames(ImageBuffer newImgBuff) {

	if( pauseUpdates ) return;
	
	//erase previous display:
	/*if( imagePanels[0] != null ) {
	    imagePanels[0].imageDisplay.updateImage( (ImageBuffer)null );
	    imagePanels[0].imageDisplay.applyLinearScale();
	}*/

	if (mode == MODE_REGULAR) {
	// force reconnect to edtd due to suspected socket bug
	    //connectToServer();
	    newImgBuff = fetch("new");
	    if (newImgBuff != null) {
		
	    }
	} else if (mode == MODE_UFGTAKE) {
	    int xxx = Integer.parseInt(lastMonVal.trim()) - 1;
	    xxx += ufgtakeStartIndex;
	    String tmpStr  = xxx+"";
	    while (tmpStr.length() < 4) tmpStr = "0"+tmpStr;
	    String filename = ufgtakeDirectory + ufgtakePrefix +"."+ tmpStr+".fits";
	    String copyFilename = copyDir+ufgtakePrefix+"."+tmpStr+".fits";
	    //System.out.println("FILE: "+filename);
	    ProcessBuilder pb = new ProcessBuilder("df",filename);
	    String s = " ", out = "";
	    try {
		Process p = pb.start();
	    	BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    	while (s != null) {
	            s = br.readLine();
	            if (s != null) out = s;
	    	}
	    } catch(IOException e) {}
	    StringTokenizer st = new StringTokenizer(out);
	    while (st.hasMoreTokens()) {
		s = st.nextToken();
		if (s.indexOf("%") != -1) {
		   s = s.substring(0,s.length()-1);
		   int space = Integer.parseInt(s);
		   if (space >= 95) JOptionPane.showMessageDialog(null, "Warning: Disk is "+space+"% full!", "Warning", JOptionPane.WARNING_MESSAGE);
		}
	    }
	    int counter = 0;
	    while (!new File(filename).exists() || (new File(filename)).length() < 16000000L) {
		try {
		   Thread.sleep(100);
		   counter+=100;
		   //System.out.println("TIME: "+counter+" "+(new File(filename)).exists() + " "+(new File(filename)).length());
		   if (counter > 5000) break; 
		} catch(InterruptedException e) { break; }
	    }
	    if (!(new File(filename)).exists()) {
		System.out.println(filename+" does not exist!!!");
		return;
	    }
	    if (new File(copyDir).exists() && !copyDir.equals("/")) {
		//copy FITS file
		try {
        	    // Create channel on the source
        	    FileChannel srcChannel = new FileInputStream(filename).getChannel();
        	    // Create channel on the destination
        	    FileChannel dstChannel = new FileOutputStream(copyFilename).getChannel();
        	    // Copy file contents from source to destination
		    //System.out.println("SIZE: "+srcChannel.size());
        	    dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
        	    // Close the channels
        	    srcChannel.close();
		    dstChannel.close();
		    // Copy all log files
		    File ufgDirFile = new File(ufgtakeDirectory);
		    File[] logs = ufgDirFile.listFiles(new FileFilter() {
			public boolean accept(File f) {
			    if (f.getName().startsWith("log")) return true; else return false;
			}
		    });
		    for (int j = 0; j < logs.length; j++) {
			srcChannel = new FileInputStream(logs[j]).getChannel();
			dstChannel = new FileOutputStream(copyDir+logs[j].getName()).getChannel();
                    	dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
                    	srcChannel.close();
                    	dstChannel.close();
		    }

		} catch(IOException e) {
		   System.out.println("Copy failed to file "+copyFilename);
		}
	    }
            UFFITSheader imgFITShead = new UFFITSheader();
            UFInts data = (UFInts) imgFITShead.readFITSfile(filename);
            newImgBuff = new ImageBuffer(frameConfig, data);
	    if (newImgBuff != null) {
		if (InitDataDisplay.jddFullFrame == null) {
		    System.err.println("NULLER");		
		} else {
		    InitDataDisplay.jddFullFrame.setSrcFileName(filename);
		    InitDataDisplay.jddFullFrame.setTitleBarText();
		}
	    }
	}
/*
System.out.println(InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.xZoom+" "+InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.yZoom);
	int rxinit = InitDataDisplay.jddZoomFrame.zoomPanel.zoomImage.rxinit;
	int rxfin = InitDataDisplay.jddZoomFrame.zoomPanel.zoomImage.rxfin;
	int ryinit = InitDataDisplay.jddZoomFrame.zoomPanel.zoomImage.ryinit;
	int ryfin = InitDataDisplay.jddZoomFrame.zoomPanel.zoomImage.ryfin;
System.out.println(rxinit+" "+ryinit+" "+rxfin+" "+ryfin);
	if (rxinit < 20) InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.xZoom-=(int)(20/InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.zoomFactor);
        if (ryinit < 20) InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.yZoom-=(int)(20/InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.zoomFactor);
        if (rxfin > 460) InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.xZoom+=(int)(20/InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.zoomFactor);
        if (ryfin > 460) InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.yZoom+=(int)(20/InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.zoomFactor);
System.out.println(InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.xZoom+" "+InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.yZoom);
*/
	updateScaling(newImgBuff);
	//System.out.println(InitDataDisplay.jddZoomFrame.zoomPanel.zoomImage.rxinit);
    }

    public void setMonitorListener() {
        ml = new UFCAToolkit.MonitorListener(){
                public void reconnect(String newrec) {

                }
                public void monitorChanged(String val) {
                    //if (nohost) return;
                    try {
                        if (!lastMonVal.equals(val) && Integer.parseInt(val) != 0) {
                            lastMonVal = val;
                            updateFrames((ImageBuffer)null);
                        }
                        lastMonVal = val;
                    } catch (Exception e) {
                        System.err.println("DataAccessPanel.addMonitor.monitorChanged> "+e.toString());
                    }
                }
            };
    }
}
