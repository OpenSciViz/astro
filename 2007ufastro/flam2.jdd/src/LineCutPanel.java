package ufjdd;
/**
 * Title:        LineCutPanel.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh, Frank Varosi, Craig Warner, Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  For plotting image data pixel values along a user selected line cut thru image in ZoomPanel.
 */
import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class LineCutPanel {

    public static final
	String rcsID = "$Name:  $ $Id: LineCutPanel.java,v 1.9 2006/11/01 20:31:55 warner Exp $";

    boolean first;
    NIRplotPanel plotPanel;
    
    public LineCutPanel(NIRplotPanel plotPanel)
    {
        this.plotPanel = plotPanel;
	first = true;
    }
    
    public void show()
    {
        if( ! first ) {
	    plotPanel.setVisible(true);
	}
    }

    public void update(double[][] data, int x1, int y1, int x2, int y2, double scalefactor, String dname) {

        int xArange = Math.abs( x2 - x1 );
        int yArange = Math.abs( y2 - y1 ); 
        int range = Math.max( xArange, yArange );

	if (range < 2) return;

	if( x2 < x1 ) {
	   int temp = x1;
	   x1 = x2;
	   x2 = temp;
	   temp = y1;
	   y1 = y2;
	   y2 = temp;
	}

	int yinc = 1;
	if( y2 < y1 && yArange > xArange ) yinc = -1;

        float slope = (float)( y2 - y1 )/( x2 - x1 );
        float theta = (float)Math.atan( slope );
        
        //System.out.println("(x1,y1) = (" + x1 + "," + y1 + ") : (x2,y2) = ( " + x2 + "," + y2 + ")");
        //System.out.println("slope = " + slope + " theta = " + theta);

	float[] pixPlot = new float[range];
	float[] dataPlot = new float[range];
        int yiMax = data.length;
        int xiMax = data[0].length;

	if( xArange >= yArange ) {

	   for(int i=0; i < range; i++) {
	      int xin = x1 + i;
              float yd = slope*i;
              int yin = (int)Math.round( yd + y1 );
	      pixPlot[i] = (float)Math.sqrt( i*i + yd*yd );
	      if( yin < yiMax && yin >= 0 &&
		  xin < xiMax && xin >= 0 )
		  dataPlot[i] = (float)( data[yin][xin] * scalefactor );
	   }
	}
	else {
	   int yin = y1;
	   int xin = x1;

	   for(int i=0; i < range; i++) {
              yin += yinc;
	      float yd = yin - y1;
	      float xd = 0;
              if( xArange > 0 ) {
		  xd = yd/slope;
		  xin = (int)Math.round( x1 + xd );
	      }
              pixPlot[i] = (float)Math.sqrt( xd*xd + yd*yd );
	      if( yin < yiMax && yin >= 0 &&
		  xin < xiMax && xin >= 0 )
		  dataPlot[i] = (float)( data[yin][xin] * scalefactor );
           }
	}

	float maxDist = UFArrayOps.maxValue( pixPlot );

	double mean, stddev;
	try {
           mean = UFArrayOps.avgValue(dataPlot);
           stddev = UFArrayOps.stddev(dataPlot);
	} catch (ArrayIndexOutOfBoundsException e) {
	   mean = 0;
	   stddev = 0;
	}
/*
        plotPanel.plot( pixPlot, dataPlot,
		 "*xtitle=Distance( pixels ), *ytitle=DATA, *xminval=0, *psym=-4, *symsize=4, *title=" + dname + "  :  Mean = "+ UFLabel.truncFormat(mean) + "  :  Std. Dev. = "+UFLabel.truncFormat(stddev)+", *xminor=10, *xtickinterval=20, *xmaxval=" + maxDist );
*/
        plotPanel.plot( pixPlot, dataPlot, "*noendxlabels, *xtitle=Distance( pixels ), *ytitle=DATA, *xminval=0, *psym=-4 , *symsize=4, *title=" + dname + "  :  Mean = "+ UFLabel.truncFormat(mean) + " :  Std. Dev. = "+UFLabel.truncFormat(stddev)+", *xmaxval=" + maxDist );
	//Overplot a Gaussian
	float med = UFArrayOps.median(dataPlot);
	float[] medSubData = UFArrayOps.subArrays(dataPlot,med);
        float max = UFArrayOps.maxValue(medSubData);
	float min = UFArrayOps.minValue(medSubData);
        double sigma1 = max/UFArrayOps.stddev(UFArrayOps.extractValues(dataPlot, UFArrayOps.where(dataPlot, "<", med)));
	double sigmaMin = Math.abs(min)/UFArrayOps.stddev(UFArrayOps.extractValues(dataPlot, UFArrayOps.where(dataPlot, ">", med)));
	int b;
	float fwhm;
	float[] midpt;
	if (sigma1 > sigmaMin) {
	   b = UFArrayOps.whereMaxValue(medSubData);
	   midpt = UFImageOps.midpt1D(medSubData, b, max/2);
	   fwhm = midpt[1];
	   b = (int)(midpt[0]+0.5);
	} else {
	   b = UFArrayOps.whereMinValue(medSubData);
	   midpt = UFImageOps.midpt1D(UFArrayOps.multArrays(medSubData, -1.f), b, -1*min/2);
	   fwhm = midpt[1];
	   b = (int)(midpt[0]+0.5);
	}
	if (b >= 0 && b < pixPlot.length && (sigma1 > 8 || fwhm > 5 || sigmaMin > 8)) {
	   float rng = pixPlot[pixPlot.length-1]-pixPlot[0]; 
           int npg = 200;  //npts for Gaussian
           float[] x = new float[npg];
           float[] y = new float[npg];
           float nplot = (float)(npg-1);
           for (int j = 0; j < npg; j++) x[j] = j*rng/nplot;
	   float[] z = UFArrayOps.divArrays(UFArrayOps.subArrays(x, pixPlot[b]), (float)(fwhm/2.35));
	   for (int j = 0; j < npg; j++) {
	      if (sigma1 > sigmaMin) {
		y[j] = med+max*(float)Math.exp(-z[j]*z[j]/2);
	      } else {
		y[j] = med+min*(float)Math.exp(-z[j]*z[j]/2);
	      }
	   }
	   plotPanel.overplot(x,y,"*color=0,0,255");
	   plotPanel.xyouts(0.78f,0.11f,"FWHM = "+UFLabel.truncFormat(fwhm),"*charsize=12,*normal,*color=0,0,255");
	}
/*
	float[] cenPlotX = new float[3];
	float[] cenPlotY = new float[3];
	cenPlotX[0] = maxDist/2;
	cenPlotX[1] = maxDist/2;
	cenPlotX[2] = maxDist/2;
	cenPlotY[0] = UFArrayOps.minValue( dataPlot );
	cenPlotY[1] = 0;
	cenPlotY[2] = UFArrayOps.maxValue( dataPlot );
	float cran = cenPlotY[2] - cenPlotY[0];
	cenPlotY[0] -= cran;
	cenPlotY[2] += cran;
	plotPanel.overplot( cenPlotX, cenPlotY,"*psym=-4, *symsize=4");
*/
    }
}
