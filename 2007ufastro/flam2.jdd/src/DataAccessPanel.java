package ufjdd;
/**
 * Title:        Java Data Display (JDD): DataAccessPanel.java (to access data in frame acq. server buffers)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Frank Varosi, Ziad Saleh, Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  Extends class UFLibPanel to use _socket attrib. and connectToServer() method.
 */
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

class DataAccessPanel extends UFLibPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: DataAccessPanel.java,v 1.49 2006/07/24 20:32:20 warner Exp $";

    public static final int MODE_NOHOST = 1;
    public static final int MODE_REPLICANT = 2;
    public static final int MODE_UFGTAKE = 3;
    public static final int MODE_REGULAR = 4;

    ImagePanel[] imagePanels; //needed for updateFrames() method below.
    public UFFrameConfig frameConfig;
    public UFColorButton pauseUpdateButton = new UFColorButton("Pause  Updates");
    
    boolean pauseUpdates = false;

    public ChannelStatFrame csf;
    public boolean hasCsf = false;

    int mode;

    //Flam-2 specific vars
    public static String EPICSPrefix = "flam:";
    Thread replicantThread, epicsThread;
    boolean keepGoing;
    String lastMonVal = "0";
    String ufgtakeDirectory = "/";
    String ufgtakePrefix = "foo";
    int ufgtakeStartIndex = 0;
    public String copyDir = "";

    public DataAccessPanel(ImagePanel[] imagePanels) {
        super();
    }

    public DataAccessPanel(String host, int port, String serverGreeting, ImagePanel[] imagePanels)
    {
        super( host, port, serverGreeting );
    }

    public DataAccessPanel(String host, int port, ImagePanel[] imagePanels)
    {
	super( host, port, "CT" );
    }    

    public void initialize() {
        pauseUpdateButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
        pauseUpdateButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    if( pauseUpdateButton.getText().toUpperCase().indexOf("PAUSE") >= 0 ){
                        pauseUpdateButton.setText("Resume  Updates");
                        pauseUpdateButton.updateColorGradient(UFColorButton.COLOR_SCHEME_RED);
                        pauseUpdateButton.setForeground( Color.black );
                        pauseUpdates = true;
                    }
                    else {
                        pauseUpdateButton.setText("Pause  Updates");
                        pauseUpdateButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
                        pauseUpdateButton.setForeground( Color.black );
                        pauseUpdates = false;
                    }
                }
       });
    }

    public String getHost() { return _Host; }
    public int getPort() { return _Port; }

    public void changeMode (int newMode) {
    }

    public boolean connectToServer() {
	return super.connectToServer(); 
    }

    public UFFrameConfig getFrameConfig() {
	return null;
    }

    public synchronized boolean request( String buffName ) {
	return false;
    }

    public synchronized ImageBuffer recvBuff() {
	return null;
    }

    public synchronized ImageBuffer fetch( String buffName )
    {
	if( request( buffName ) )
	    return recvBuff();
	else
	    return null;
    }
    
    public void updateFrames(UFFrameConfig notifyFrameConfig) {
    }

    public void updateFrames(ImageBuffer newImgBuff) {
    }

    public void updateScaling(ImageBuffer newImgBuff) {
        if( newImgBuff != null ) {
           String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
           String scaleBoxSel = (String)(InitDataDisplay.jddFullFrame.scaleBox.getSelectedItem());
           imagePanels[0].imageDisplay.updateImage( newImgBuff );
           if (zModeBoxSel.indexOf("Auto") >= 0) {
              imagePanels[0].adjustPanel.updateMinMax();
              imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
           } else if (zModeBoxSel.indexOf("Manual") >= 0) {
              if (scaleBoxSel.indexOf("Linear") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(
                                            Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText()),
                                            Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText()) );
                //InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale(min, max);
                //if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                //   InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max) ;
              } else if (scaleBoxSel.indexOf("Log") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText()));
              } else if (scaleBoxSel.indexOf("Power") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(
                                            Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText()),
                                            Double.parseDouble(InitDataDisplay.jddFullFrame.zMin.getText()));
              }
           } else {
              System.out.println(scaleBoxSel);
              if (zModeBoxSel.indexOf("Zscale") >= 0) {
                imagePanels[0].imageDisplay.applyZScale();
              } else if (zModeBoxSel.indexOf("Zmax") >= 0) {
                imagePanels[0].imageDisplay.applyZScale("Zmax");
              } else if (zModeBoxSel.indexOf("Zmin") >= 0) {
                imagePanels[0].imageDisplay.applyZScale("Zmin");
              }
              imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText()), Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText()) );
           }
           if (hasCsf) csf.updateStats(imagePanels[0].imageDisplay.imageBuffer.pixels, true);
        }
        imagePanels[0].imageDisplay.setZoomImage();
    }

    public void setCsf(ChannelStatFrame csf) {
        this.csf = csf;
        this.hasCsf = true;
    }

    public void removeCsf() {
        this.hasCsf = false;
    }

    //Flam-2 specific methods
    public String getUfgtakeDirectory() { return ufgtakeDirectory; }
    public void setUfgtakeDirectory(String newDir) { ufgtakeDirectory = new String(newDir); }
    public String getUfgtakePrefix() { return ufgtakePrefix; }
    public void setUfgtakePrefix(String newFix) { ufgtakePrefix = new String(newFix); }
    public int getUfgtakeStartIndex() { return ufgtakeStartIndex; }
    public void setUfgtakeStartIndex(int newDex) { ufgtakeStartIndex = newDex; }
    public static String getEPICSPrefix() { return EPICSPrefix; }
    public static void setEPICSPrefix(String prefix) {
        if (!prefix.trim().endsWith(":")) prefix = prefix.trim()+":";
        EPICSPrefix = new String(prefix);
    }

    public void setMonitorListener() {
    }

}
