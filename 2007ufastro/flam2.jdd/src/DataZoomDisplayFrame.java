package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class DataZoomDisplayFrame extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: DataZoomDisplayFrame.java,v 1.46 2006/03/24 23:01:44 warner Exp $";

    GraphicsConfiguration gc;
    JPanel zoomDisplPanel, ufgtakePanel;
    DataAccessPanel fullDisplPanel;
    UFPlotPanel plotPanel;
    ZoomPanel zoomPanel;
    ChannelStatFrame csf;
    File loadDir = new File(".");
    PrefsPanel preferences;
    final ColorMapDialog colorMapDialog;
    
    public DataZoomDisplayFrame(GraphicsConfiguration gc, String tcshost, String tcsport,  
                                final ZoomPanel zoomPanel,
				final ColorMapDialog colorMapDialog,
				DataAccessPanel fullDisplPanel, final NIRplotPanel plotPanel,
				String args[])
    {
        super(gc);
	this.gc=gc;
		
        this.setTitle("CIRCE Java  Data  Display (version 2005/08/09)");
        //this.setSize( 1150, 640 );
	this.setSize(1210,700);
        this.setLocation( 0, 0 );
	this.setUndecorated(false);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.fullDisplPanel = fullDisplPanel;
        this.plotPanel = plotPanel;
        this.zoomPanel = zoomPanel;
        this.colorMapDialog = colorMapDialog;
	
	zoomDisplPanel = new JPanel();

        zoomPanel.setBounds( 5, 5, zoomPanel.xSize, zoomPanel.ySize );

        //fullDisplPanel.connectPanel.setBounds( 620, 510, 300, 70 );
	//fullDisplPanel.pauseUpdateButton.setBounds(960 , 520, 150, 50 );
	fullDisplPanel.connectPanel.setBounds(zoomPanel.xSize+33,570,300,70);
	fullDisplPanel.pauseUpdateButton.setBounds(zoomPanel.xSize+373,580,150,50);

        JButton colorMapButton = new JButton("Color  Map  Control");
        //colorMapButton.setBounds(890, 460, 200, 40);
	colorMapButton.setBounds(zoomPanel.xSize+303,520,200,40);

	colorMapButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                colorMapDialog.setVisible(true);
		colorMapDialog.setState( Frame.NORMAL );
            }
        });

	plotPanel.setLayout(new RatioLayout());
	//plotPanel.setBounds(595,50,540,400);
	plotPanel.setBounds(zoomPanel.xSize+8,50,540,460);
	plotPanel.setBackground(Color.white);
	plotPanel.setBorder(BorderFactory.createLineBorder(Color.green, 1));

        final UFColorButton apertureButton = new UFColorButton("Aperture", UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
        final UFColorButton lineButton  = new UFColorButton("Line", UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
        final UFColorButton radialButton = new UFColorButton("Radial", UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
        final UFColorButton statsButton = new UFColorButton("Stats", UFColorButton.COLOR_SCHEME_LIGHT_GRAY);

        UFColorButton offButton = new UFColorButton("OFF", UFColorButton.COLOR_SCHEME_CYAN);

        radialButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             radialButton.setForeground(Color.blue);
             apertureButton.setForeground(Color.black);
             statsButton.setForeground(Color.black);
             lineButton.setForeground(Color.black);
             radialButton.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
             apertureButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             statsButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             lineButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             zoomPanel.zoomImage.statsMode = false;
             zoomPanel.zoomImage.radialMode = true;
             zoomPanel.zoomImage.linecutMode = false;
             zoomPanel.zoomImage.apertureMode = false;
             zoomPanel.zoomImage.repaint();
             zoomPanel.zoomImage.calcRadialProfile();
             zoomPanel.zoomImage.radialProfilePanel.show();
           }
        });
        apertureButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             radialButton.setForeground(Color.black);
             apertureButton.setForeground(Color.blue);
             statsButton.setForeground(Color.black);
             lineButton.setForeground(Color.black);
             radialButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             apertureButton.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
             statsButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             lineButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             zoomPanel.zoomImage.statsMode = false;
             zoomPanel.zoomImage.radialMode = false;
             zoomPanel.zoomImage.linecutMode = false;
             zoomPanel.zoomImage.apertureMode = true;
             zoomPanel.zoomImage.repaint();
             zoomPanel.zoomImage.calcAperture();
             zoomPanel.zoomImage.aperturePanel.show();
           }
        });

        statsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             radialButton.setForeground(Color.black);
             apertureButton.setForeground(Color.black);
             statsButton.setForeground(Color.blue);
             lineButton.setForeground(Color.black);
             radialButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             apertureButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             statsButton.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
             lineButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             zoomPanel.zoomImage.statsMode = true;
             zoomPanel.zoomImage.radialMode = false;
             zoomPanel.zoomImage.linecutMode = false;
             zoomPanel.zoomImage.apertureMode = false;
             zoomPanel.zoomImage.repaint();
             zoomPanel.zoomImage.calcStats();
             zoomPanel.zoomImage.statsPanel.show();
           }
        });
        lineButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             radialButton.setForeground(Color.black);
             apertureButton.setForeground(Color.black);
             statsButton.setForeground(Color.black);
             lineButton.setForeground(Color.blue);
             radialButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             apertureButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             statsButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             lineButton.updateColorGradient(UFColorButton.COLOR_SCHEME_WHITE);
             zoomPanel.zoomImage.statsMode = false;
             zoomPanel.zoomImage.radialMode = false;
             zoomPanel.zoomImage.linecutMode = true;
             zoomPanel.zoomImage.apertureMode = false;
             zoomPanel.zoomImage.repaint();
             zoomPanel.zoomImage.calcLineCut();
             zoomPanel.zoomImage.linecutPanel.show();
           }
        });
        offButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
             radialButton.setForeground(Color.black);
             apertureButton.setForeground(Color.black);
             statsButton.setForeground(Color.black);
             lineButton.setForeground(Color.black);
             radialButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             apertureButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             statsButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             lineButton.updateColorGradient(UFColorButton.COLOR_SCHEME_LIGHT_GRAY);
             zoomPanel.zoomImage.statsMode = false;
             zoomPanel.zoomImage.radialMode = false;
             zoomPanel.zoomImage.linecutMode = false;
             zoomPanel.zoomImage.apertureMode = false;
             zoomPanel.zoomImage.repaint();
           }
        });

        JPanel dpButtonPanel;
	dpButtonPanel = new JPanel();
	dpButtonPanel.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
	//dpButtonPanel.setBounds(610,5,510,40);
	dpButtonPanel.setBounds(zoomPanel.xSize+23,5,510,40);
        dpButtonPanel.add(apertureButton  );
	dpButtonPanel.add(lineButton );
	dpButtonPanel.add(radialButton   );
	dpButtonPanel.add(statsButton  );
	dpButtonPanel.add(offButton );

        zoomDisplPanel.setLayout(null);
        zoomDisplPanel.add( plotPanel );
        zoomDisplPanel.add( dpButtonPanel );
        zoomDisplPanel.add( zoomPanel );
        zoomDisplPanel.add( colorMapButton );
        zoomDisplPanel.add( fullDisplPanel.connectPanel );
        zoomDisplPanel.add( fullDisplPanel.pauseUpdateButton );
        //zoomDisplPanel.setBounds( 0, 0, 1150-14, 730 );
	zoomDisplPanel.setBounds(0,0,1210-14,790);
	
	
        this.getContentPane().setLayout(null);
        this.getContentPane().add( zoomDisplPanel );
        
    }
    
//-------------------------------------------------------------------------------

    private void setupMenuBar() {
    }
    
//-------------------------------------------------------------------------------

    public void helpAbout_action(ActionEvent e) {
    jddAboutBox dlg = new jddAboutBox(this);
    Dimension dlgSize = dlg.getSize();
    Dimension frmSize = getSize();
    dlg.setLocation(100,100);
    dlg.setModal(true);
    dlg.setVisible(true);
    }
    
//-------------------------------------------------------------------------------

    void change_look(ActionEvent e) {
        if (e.getActionCommand()=="Motif Look")
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
        
        if (e.getActionCommand()=="Metal Look")
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
    }
//------------------------------------------------------------------------------------------------------------

    protected void processWindowEvent(WindowEvent wev) {
        if (wev.getID() == WindowEvent.WINDOW_CLOSING ) {
            Object[] exitOptions = {"Exit","Cancel"};
            int n = JOptionPane.showOptionDialog(this, "Are you sure you want to quit?", "Exit JDD?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
            if (n == 0) {
                System.out.println("JDD exiting...");
                System.exit(0);
            }
        }
        else
            super.processWindowEvent(wev);
    }

    public void setPrefs() {
        String home = UFExecCommand.getEnvVar("HOME");
        this.preferences = new PrefsPanel(home+"/.jdd", zoomPanel, colorMapDialog);
        this.preferences.setPrefs();
    }
}    
