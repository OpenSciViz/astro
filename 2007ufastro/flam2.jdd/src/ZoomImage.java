package ufjdd;
/**
 * Title:        ZoomImage.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Craig Warner
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

public class ZoomImage extends JPanel implements KeyListener {

    public static final
	String rcsID = "$Name:  $ $Id: ZoomImage.java,v 1.21 2006/12/06 20:19:36 warner Exp $";

    protected double zoomData[][];
    public int xySize;
    protected byte pixBuffer[];
    protected Image pixImage = null;
    protected int xInit = 0, yInit = 0;
    protected int xMark = 0, yMark = 0;
    protected int xLast = 0, yLast = 0;
    protected int mouseButton = 0;
    protected int sxinit, syinit, sxfin, syfin;   //screen coordinates of box for calcStats()
    protected int cxinit, cyinit, cxfin, cyfin;   //screen coordinates of Line for calcLineCut()
    protected int rxinit, ryinit, rxfin, ryfin;   //screen coordinates of Radial for calcradialProfile()
    protected int apxinit, apyinit, apxfin, apyfin, apbsize;   //screen coordinates of Radial for calcAperture()
    protected int xisav, yisav, xfsav, yfsav, absav;
    protected boolean dragTranslate = false, dragFinal = false, dragInit = false, dragResize = false;
    protected double oMin, oMax;
    protected int smoothIters = 0;
    protected int xStart, yStart;//, zoomFactor=1;
    protected float zoomFactor = 1;
    protected double scaleFactor;
    protected boolean useScaleFactor = false;
    protected boolean mouseInPanel = false;
    protected StatisticsPanel statsPanel;
    protected LineCutPanel linecutPanel;
    protected RadialProfilePanel radialProfilePanel;
    protected AperturePanel aperturePanel;
    protected boolean statsMode = false;
    protected boolean radialMode = false;
    protected boolean linecutMode = false;
    protected boolean apertureMode = false;
    protected boolean apertureDrag = false;
    protected Color transLucent = new Color(128, 255, 128, 49);
    protected Color apColor = new Color(255, 255, 128, 128);
    protected float[] centrd;
    protected float[] fwhm = new float[6];
    protected boolean doCentroid = false, plotCntrd = false;
    protected IndexColorModel colorModel;
    protected final HistogramBar histoBar;
    protected final AdjustZoomPanel adjustZoom;
    protected final JComboBox smoothIterSelect;
    protected final JLabel pixelDataVal;
    protected final ApertureStatsPanel apertureStats;
    protected ImageBuffer zoomBuffer;
    protected SuperZoomImage superZoom;
    protected ImageDisplayPanel imDispPanel = null;
    protected String lineCutStyle = "Hand Drawn"; 
    protected String centroidFollowing = "On";

    public ZoomImage( int zoomSize, JComboBox smoothIterSel, JLabel pixelValue, 
                      HistogramBar histoBar, IndexColorModel colorModel, 
		      AdjustZoomPanel adjZoom, ApertureStatsPanel aperStats, 
		      NIRplotPanel plotPanel, SuperZoomImage superZoom)
    {
	this.xySize = zoomSize;
        this.colorModel = colorModel;
        this.histoBar = histoBar;
	this.adjustZoom = adjZoom;
	this.pixelDataVal = pixelValue;
	this.apertureStats = aperStats;
	this.smoothIterSelect = smoothIterSel;
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        this.setMinimumSize(new Dimension(xySize, xySize));
        this.superZoom = superZoom;

	this.addKeyListener(this);
	
        radialProfilePanel = new RadialProfilePanel(plotPanel);
        linecutPanel = new LineCutPanel(plotPanel);
        statsPanel = new StatisticsPanel(plotPanel);
        aperturePanel = new AperturePanel(plotPanel);
        pixBuffer = new byte[xySize*xySize];
        
        smoothIterSelect.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    String selSmoothIter = (String)smoothIterSelect.getSelectedItem();
		    if( selSmoothIter.length() > 0 ) {
			smoothIters =  Integer.parseInt( selSmoothIter );
			if( zoomBuffer == null ) return;
			if( zoomBuffer.image == null ) return;
			smoothImage();
			adjustZoom.updateMinMax();
			adjustZoom.scaleAndDraw();
			apertureStats.showArrayStats( zoomData, zoomBuffer, scaleFactor );
		    }
		    else smoothIters = 0;
		}
	    });
                
        
        addMouseMotionListener(new MouseMotionListener() {

		public void mouseMoved(MouseEvent evt) {
		    xLast = evt.getX();
		    yLast = evt.getY();
		    showPixelValue( xLast, yLast );
		    if (mouseInPanel) updateSuperZoom(xLast, yLast);
		    requestFocusInWindow();
		}
            
		public void mouseDragged(MouseEvent evt) {
		    if( mouseButton == 1 ||  mouseButton == 3) {
			if( statsMode ) {
			    sxfin = evt.getX();
			    syfin = evt.getY();
			}
			else if( linecutMode ) {
			    cxfin = evt.getX();
			    cyfin = evt.getY();
			}
			else if( radialMode ) {
			    rxfin = evt.getX();
			    ryfin = evt.getY();
			}
			else if (apertureMode) {
			    apxfin = evt.getX();
			    apyfin = evt.getY();
			}
		    }
		    else if( mouseButton == 2 ) {

			int xOffset = evt.getX() - xInit;
			int yOffset = evt.getY() - yInit;
			int xi = xisav + xOffset;
			int yi = yisav + yOffset;
			int xf = xfsav + xOffset;
			int yf = yfsav + yOffset;

			if( dragFinal ) {
			    xi = xisav - xOffset;
			    yi = yisav - yOffset;
			}
			else if( dragInit ) {
			    xf = xfsav - xOffset;
			    yf = yfsav - yOffset;
			}
			else if( dragResize ) {
			    xi = xisav;
			    yi = yisav;
			    xf = xisav + xOffset;
			    yf = yisav;
			}

			if( statsMode ) {
			    sxinit = xi;
			    syinit = yi;
			    sxfin = xf;
			    syfin = yf;
			}
			else if( linecutMode ) {
			    cxinit = xi;
			    cyinit = yi;
			    cxfin = xf;
			    cyfin = yf;
			}
			else if( radialMode ) {
			    rxinit = xi;
			    ryinit = yi;
			    rxfin = xf;
			    ryfin = yf;
			}
			else if (apertureMode) {
			    if (apertureDrag) {
                                apxinit = xi;
                                apyinit = yi;
                                apxfin = xf;
                                apyfin = yf;
			    } else {
				apbsize=absav+xOffset;
				if (apbsize < 3) apbsize = 3;
			    }
                        }
		    }
		    repaint();
		}
	    });      

	addMouseListener(new MouseListener() {
            
		public void mousePressed(MouseEvent evt) {
		    requestFocusInWindow();
                
		    if( (evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0 ) {
			mouseButton = 1;
			if( statsMode ) {
			    sxinit = sxfin = evt.getX();
			    syinit = syfin = evt.getY();
			}
			else if( linecutMode ) {
			    cxinit = cxfin = evt.getX();
			    cyinit = cyfin = evt.getY();
			}
			else if( radialMode ) {
			    rxinit = rxfin = evt.getX();
			    ryinit = ryfin = evt.getY();
			}
			else if( apertureMode ) {
			    apxinit = apxfin = evt.getX();
			    apyinit = apyfin = evt.getY();
			    apbsize = 12;
			}
		    }
                
		    if( (evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0 ) {

			mouseButton = 2;
			xInit = evt.getX();
			yInit = evt.getY();

			if( statsMode ) {
			    xisav = sxinit;
			    yisav = syinit;
			    xfsav = sxfin;
			    yfsav = syfin;
			}
			else if( linecutMode ) {
			    xisav = cxinit;
			    yisav = cyinit;
			    xfsav = cxfin;
			    yfsav = cyfin;
			}
			else if( radialMode ) {
			    xisav = rxinit;
			    yisav = ryinit;
			    xfsav = rxfin;
			    yfsav = ryfin;
			}
                        else if( apertureMode ) {
                            xisav = apxinit;
                            yisav = apyinit;
                            xfsav = apxfin;
                            yfsav = apyfin;
			    absav = apbsize;
		            int x1 = Math.min( apxinit, apxfin );
            		    int y1 = Math.min( apyinit, apyfin );
                            int x2 = Math.max( apxinit, apxfin );
                            int y2 = Math.max( apyinit, apyfin );
			    if (xInit > x1 && xInit < x2 && yInit > y1 && yInit < y2) apertureDrag = true; else apertureDrag = false;
                        }
			else {
			   if (imDispPanel != null) {
			      imDispPanel.updateZoomImage((int)((xInit-xySize/2)/zoomFactor), (int)((yInit-xySize/2)/zoomFactor));
			   }
			}

			double distMid = distance( xInit, yInit, (xfsav+xisav)/2, (yfsav+yisav)/2 );
	                double rad = (double)Math.sqrt( (xisav-xfsav)*(xisav-xfsav) + (yisav-yfsav)*(yisav-yfsav) );
	                double dis = (double)Math.sqrt( (xisav-xInit)*(xisav-xInit) + (yisav-yInit)*(yisav-yInit) );
			dragTranslate = true;
			dragFinal = false;
			dragInit = false;
			dragResize = false;
			if( radialMode ) {
			   if( dis > rad ) {
			       dragTranslate = false;
			       dragResize = true;
			   }
			} else if( distance( xInit, yInit, xfsav, yfsav ) < distMid ) {
			    dragTranslate = false;
			    dragFinal = true;
			}
			else if( distance( xInit, yInit, xisav, yisav ) < distMid ) {
			    dragTranslate = false;
			    dragInit = true;
  		        }
		    }
                
		    if( (evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0 ) {
			mouseButton = 3;
			//if(evt.isPopupTrigger())
			//    popupMenu.show( evt.getComponent(), evt.getX(), evt.getY() );
		    }
		}
            
		public void mouseReleased(MouseEvent evt) {

		    if( (evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0 ||
		        (evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0 ) {
			if( statsMode ) {
			    sxfin = evt.getX();
			    syfin = evt.getY();
			    repaint();
			    calcStats();
			    statsPanel.show();
			}
			else if( linecutMode ) {
			    cxfin = evt.getX();
			    cyfin = evt.getY();
			    repaint();
			    calcLineCut();
			    linecutPanel.show();
			}
			else if( radialMode ) {
			    rxfin = evt.getX();
			    ryfin = evt.getY();
			    if (rxinit == rxfin) rxfin = (int)(rxinit + 12*zoomFactor);
			    if (ryinit == ryfin) ryfin = (int)(ryinit + 12*zoomFactor);
			    repaint();
			    calcRadialProfile();
			    radialProfilePanel.show();
			}
			else if( apertureMode ) {
                            apxfin = evt.getX();
                            apyfin = evt.getY();
                            repaint();
			    calcAperture();
			    aperturePanel.show();
			}
		    }
                
		    if( (evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0 ) {
			if( xInit == evt.getX() && 
			    yInit == evt.getY() ) {
			    xMark = xInit;
			    yMark = yInit;
			    showPixelValue( xMark, yMark );
			}
			else if( statsMode ) {
			    calcStats();
			    statsPanel.show();
			}
			else if( linecutMode ) {
			    calcLineCut();
			    linecutPanel.show();
			}
			else if( radialMode ) {
			    calcRadialProfile();
			    radialProfilePanel.show();
			}
		        else if (apertureMode) {
			    calcAperture();
			    aperturePanel.show();
			}
		    }
		}
            
		public void mouseClicked(MouseEvent evt) {}
		public void mouseEntered(MouseEvent evt) { mouseInPanel = true; }
		public void mouseExited(MouseEvent evt) { mouseInPanel = false; }
	    });
    }

    public double distance( int x1, int y1, int x2, int y2 ) {
	int xd = x2 - x1;
	int yd = y2 - y1;
	return Math.sqrt( xd*xd + yd*yd );
    }

    public void useScaleFactor( boolean useit )
    {
	useScaleFactor = useit;

	if( useScaleFactor && zoomBuffer != null )
	    this.scaleFactor = zoomBuffer.scaleFactor;
	else
	    this.scaleFactor = 1.0;

	if( zoomData != null ) apertureStats.showArrayStats( zoomData, zoomBuffer, scaleFactor );
    }

    public void updateSuperZoom(int zx, int zy) {
	int szSize = superZoom.xySize/4;
	if( zoomFactor > 0 && zoomData != null && zx >= 0 && zy >= 0 ) {
	    int szX = Math.min(xySize-szSize, Math.max(0, zx-szSize/2));
	    int szY = Math.min(xySize-szSize, Math.max(0, zy-szSize/2));
	    superZoomExpand(szX, szY);
	    superZoom.repaint();
	}
    }

    public void showPixelValue()
    {
	if( mouseInPanel )
	    showPixelValue( xLast, yLast );
	else
	    showPixelValue( xMark, yMark );
    }

    private void showPixelValue( int zx, int zy )
    {
	if( zoomFactor > 0 && zoomData != null && zx >= 0 && zy >= 0 ) {
	    int xPix = (int)(zx/zoomFactor);
	    int yPix = (int)(zy/zoomFactor);
	    int xLoc = xStart + xPix;
	    int yLoc = zoomBuffer.frameConfig.height - yStart - yPix -1;
	    double zdata = zoomData[yPix][xPix] * scaleFactor;
	    String dataVal = UFLabel.truncFormat( zdata ) + "  [" + zoomBuffer.getShortNameNum() + "]";
	    pixelDataVal.setText(" x=" + xLoc + ",  y=" + yLoc + ",  Data = " + dataVal );
	    pixelDataVal.setToolTipText(zoomBuffer.nameNum);
	}
    }

    public synchronized void updateImage(ImageBuffer zoomBuff, IndexColorModel colorModel)
    {
	this.zoomBuffer = zoomBuff;
        this.xStart = zoomBuff.xStart;
        this.yStart = zoomBuff.yStart;
        this.zoomFactor = zoomBuff.zoomFactor;

	if( useScaleFactor )
	    this.scaleFactor = zoomBuffer.scaleFactor;
	else
	    this.scaleFactor = 1.0;

	smoothImage();
	adjustZoom.updateMinMax();
	adjustZoom.scaleAndDraw();
	showPixelValue();
	apertureStats.showArrayStats( zoomData, zoomBuffer, scaleFactor );
	updateSuperZoom(xLast, yLast);
    }
    
    public void updateColorMap(IndexColorModel colorModel) {
        this.colorModel = colorModel;
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }
    
    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        if( pixImage == null ) return;

	g.drawImage( pixImage, 0, 0, null );

	if( statsMode ) {
	    // Paint a rectangle with a translucent color
	    g.setColor( transLucent );
	    int xi = Math.min( sxinit, sxfin );
	    int yi = Math.min( syinit, syfin );
	    int xs = Math.abs( sxfin - sxinit );
	    int ys = Math.abs( syfin - syinit );
	    g.fillRect( xi, yi, xs, ys );
	    // Paint a double rectangular outline
	    g.setColor( Color.WHITE );
	    g.drawRect( xi, yi, xs, ys );
	    g.setColor( Color.BLACK );
	    g.drawRect( xi+1, yi+1, xs-2, ys-2 );
	}
	else if( linecutMode ) {
	    g.setColor( Color.GREEN );
	    g.drawLine( cxinit, cyinit, cxfin, cyfin );
	}
	else if( radialMode ) {
	    int radius = (int)Math.sqrt( (rxinit-rxfin)*(rxinit-rxfin) + (ryinit-ryfin)*(ryinit-ryfin) );
	    g.setColor( Color.GREEN );
	    g.drawOval( rxinit-radius, ryinit-radius, 2*radius, 2*radius );
	}
	else if( apertureMode ) {
            // Paint a rectangle with a translucent color
            g.setColor( transLucent );
            int xi = Math.min( apxinit, apxfin );
            int yi = Math.min( apyinit, apyfin );
            int xs = Math.abs( apxfin - apxinit );
            int ys = Math.abs( apyfin - apyinit );
            g.fillOval( xi, yi, xs, ys );
            // Paint a double oval outline
            g.setColor( Color.WHITE );
            g.drawOval( xi, yi, xs, ys );
            g.setColor( Color.BLACK );
            g.drawOval( xi+1, yi+1, xs-2, ys-2 );
	    // Paint inner oval for background
	    g.setColor(Color.YELLOW);
	    g.drawOval(xi-2, yi-2, xs+4, ys+4);
	    // Paint outer oval for background
	    g.drawOval(xi-apbsize, yi-apbsize, xs+2*apbsize, ys+2*apbsize);
	}

	if( doCentroid ) {
	    if( centrd.length > 1 ) {
		int ax = (int)(2 * zoomFactor);
		int ay = ax;
		if (fwhm[0] > 0) {
		    ax = Math.max( Math.round( zoomFactor*fwhm[2]/2 ), ax );
		    ay = Math.max( Math.round( zoomFactor*fwhm[3]/2 ), ay );
		}
		int xc = (int)Math.round( centrd[0] * zoomFactor );
		int yc = (int)Math.round( centrd[1] * zoomFactor );
		int x1 = xc - ax;
		int x2 = xc + ax;
		int y1 = yc - ay;
		int y2 = yc + ay;
		//g.setColor( transLucent );
		//g.fillOval(x1, y1, (x2-x1), (y2-y1));
		g.setColor(Color.green);
		g.drawLine( xc, y1, xc, y2 );
		g.drawLine( x1, yc, x2, yc );
		g.setColor(Color.BLACK);
		g.drawLine( xc-1, y1, xc-1, y2 );
		g.drawLine( xc+1, y1, xc+1, y2 );
		g.drawLine( x1, yc-1, x2, yc-1 );
		g.drawLine( x1, yc+1, x2, yc+1 );
	    }
	}
    }

    // magnify the array by a zoomFactor and convert from 2-D array into 1-D pixBuffer:

    private void zoomExpand( byte array[][] ) {
        for( int i=0; i < array.length; i++ ) {
	    int zi = (int)(i*zoomFactor);
            //float zi = i*zoomFactor;
            for( int j=0; j < array[i].length; j++ ) {
		int zj = (int)(j*zoomFactor);
            	//float zj = j*zoomFactor;
		byte data = array[i][j];
                for( int k=0; k < zoomFactor; k++ ) {
		    int zindex = (int)((k + zi) * xySize + zj);
                    for( int m=0; m < zoomFactor; m++ ) pixBuffer[zindex++] = data;
		}
	    }
	}
    }

    private void superZoomExpand(int x, int y) {
	int szSize = superZoom.xySize/4;
	byte[][] szPix = new byte[szSize][szSize];
	for (int i = y; i < y+szSize; i++) {
	   for (int j = x; j < x+szSize; j++) {
	      szPix[i-y][j-x] = pixBuffer[i*xySize+j];
	   }
	}
	superZoom.zoomExpand(szPix);
    }

    private void doAnalysis() {
	if( statsMode ) calcStats();
	if( linecutMode ) calcLineCut();
	if( radialMode ) calcRadialProfile();
	if( apertureMode ) calcAperture();
    }

    public synchronized void applyLinearScale() { applyLinearScale( oMin, oMax ); }
    
    public synchronized void applyLinearScale(double minv, double maxv)
    {
	if( zoomData == null ) return;
	doAnalysis();
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
        double range = maxv - minv;

        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double f = ( zoomData[i][j] - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ), minv, maxv, scaleFactor );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public synchronized void applyLogScale(double threshold)
    {
	if( zoomData == null ) return;
	doAnalysis();
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
        double maxv = Math.log( (double)oMax );
        double range = maxv - minv;
        
        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double sd = zoomData[i][j];
		double f = 0.0;

		if( sd > threshold ) f = ( Math.log( sd ) - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ), threshold, oMax, scaleFactor );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public synchronized void applyPowerScale(double threshold, double power)
    {
	if( zoomData == null ) return;
	doAnalysis();
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
	if( threshold <= 0 ) threshold = 1;
	if( power <= 0 ) power = 0.5;
	if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
        double maxv = Math.pow( (double)oMax, power );
        double range = maxv - minv;
        
        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double sd = zoomData[i][j];
		double f = 0.0;

		if( sd > threshold ) f = ( Math.pow( sd, power ) - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ), threshold, oMax, scaleFactor );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    // Apply 3x3 boxcar smoothing to zoomBuffer.image recursively.
    // If smoothIters = 0, smooth function just returns conversion to double array.

    private void smoothImage()
    {
	zoomData = UFImageOps.smooth( zoomBuffer.image, 3, smoothIters );

	if( smoothIters > 0 ) {
	    oMin = zoomData[0][0];
	    oMax = oMin;
	    for( int i=0; i < zoomData.length; i++ ) {
		for( int j=0; j < zoomData[i].length; j++ ) {
		double zdata = zoomData[i][j];
                if( zdata < oMin ) oMin = zdata;
                if( zdata > oMax ) oMax = zdata;
		}
	    }
	    zoomBuffer.s_min = (float)oMin;
	    zoomBuffer.s_max = (float)oMax;
	}
	else {
	    oMin = zoomBuffer.min;
	    oMax = zoomBuffer.max;
	    zoomBuffer.s_min = zoomBuffer.min;
	    zoomBuffer.s_max = zoomBuffer.max;
	}
    }

    public void calcStats()
    {
        if (sxinit==0 || syinit==0 || sxfin==0 || syfin==0 ) return;
	int x1 = (int)(Math.min( sxinit, sxfin )/zoomFactor);
	int y1 = (int)(Math.min( syinit, syfin )/zoomFactor);
	int x2 = (int)(Math.max( sxinit, sxfin )/zoomFactor);
	int y2 = (int)(Math.max( syinit, syfin )/zoomFactor);

	if( adjustZoom.setNewMin || adjustZoom.setNewMax ) {
	    statsPanel.update( zoomData, x1, y1, x2, y2, adjustZoom.newMin, adjustZoom.newMax,
			       scaleFactor, zoomBuffer.Ndifs, zoomBuffer.getShortNameNum() );
	}
	else statsPanel.update( zoomData, x1, y1, x2, y2, scaleFactor, zoomBuffer.Ndifs, zoomBuffer.getShortNameNum() );
    }

    public void calcLineCut() {
        if (cxinit==0 || cyinit==0 || cxfin==0 || cyfin==0 ) return;
	if (lineCutStyle.equals("Nice Angles Only")) _niceAngleLineCut();

	int x1 = (int)(cxinit/zoomFactor);
	int y1 = (int)(cyinit/zoomFactor);
	int x2 = (int)(cxfin/zoomFactor);
	int y2 = (int)(cyfin/zoomFactor);
	linecutPanel.update( zoomData, x1, y1, x2, y2, scaleFactor, zoomBuffer.getShortNameNum() );
    }

    public void calcRadialProfile() {
        
        if (rxinit==0 || ryinit==0 || rxfin==0 || ryfin==0 ) return;
		
	int r = (int)(Math.sqrt( (rxfin-rxinit)*( rxfin-rxinit ) + ( ryfin-ryinit )*( ryfin-ryinit ) )/zoomFactor);	
	
	if (r*zoomFactor > rxinit) r=(int)(rxinit/zoomFactor);
	if (r*zoomFactor > ryinit) r=(int)(ryinit/zoomFactor);
	if (r*zoomFactor > xySize-rxinit) r=(int)((xySize-rxinit)/zoomFactor);
	if (r*zoomFactor > xySize-ryinit) r=(int)((xySize-ryinit)/zoomFactor);
	rxfin= rxinit+ (int)(r*zoomFactor);
	ryfin= ryinit+ (int)(r*zoomFactor);
	repaint();
	double[][] radialData = new double[2*r][2*r];
	double[][] finalRadialData;
	
	int xInitProf = (int)(rxinit/zoomFactor-r);
	int yInitProf = (int)(ryinit/zoomFactor-r);
	
	for (int i=0; i<2*r; i++) {
	   for (int j=0; j<2*r; j++) {
	      radialData[i][j]=zoomData[yInitProf+i][xInitProf+j];
	   };	
	};
		
	int[] zmax = UFArrayOps.whereMaxValue( radialData );
	if (zmax[0] < 4 || zmax[0] > 2*r-4) zmax[0] = r;
	if (zmax[1] < 4 || zmax[1] > 2*r-4) zmax[1] = r;
	centrd = UFImageOps.getCentroid( radialData, zmax[0], zmax[1], 5 );
	
        float xc = centrd[0] * zoomFactor;
        float yc = centrd[1] * zoomFactor;
        float[] cenPnt = {0, 0};
//System.out.println("X: "+xc+" "+xStart+" "+rxinit+" "+zoomFactor+" "+r);
//System.out.println("Y: "+yc+" "+yStart+" "+ryinit+" "+zoomFactor+" "+r);
	
	if (centrd[0] != -1 && centrd[1]!= -1 && centroidFollowing.equals("On")) {
	    fwhm = UFImageOps.fwhm2D( radialData, (int)Math.round(centrd[0]), (int)Math.round(centrd[1]) );
            float xCentr = xStart + (int)(rxinit/zoomFactor) - r + xc/zoomFactor;
            float yCentr = zoomBuffer.frameConfig.height - yStart - (int)(ryinit/zoomFactor) + r - yc/zoomFactor - 1;
            rxinit= (int)(rxinit-r*zoomFactor+xc);
            ryinit= (int)(ryinit-r*zoomFactor+yc);
            if (r*zoomFactor > rxinit) r=(int)(rxinit/zoomFactor);
            if (r*zoomFactor > ryinit) r=(int)(ryinit/zoomFactor);
            if (r*zoomFactor > xySize-rxinit) r=(int)((xySize-rxinit)/zoomFactor);
            if (r*zoomFactor > xySize-ryinit) r=(int)((xySize-ryinit)/zoomFactor);
            rxfin= (int)(rxinit+ r*zoomFactor);
            ryfin= (int)(ryinit+ r*zoomFactor);
            finalRadialData = new double[2*r][2*r];
            for (int i=0; i<2*r; i++) {
               for (int j=0; j<2*r; j++) {
            	  finalRadialData[i][j]=zoomData[(int)(ryinit/zoomFactor-r+i)][(int)(rxinit/zoomFactor-r+j)];
               };	 
            };
	    cenPnt[0] = Math.round(xCentr*100)/100.f;
	    cenPnt[1] = Math.round(yCentr*100)/100.f;
            System.out.println("Centroid (X, Y)= (" +xCentr +", "+ yCentr +")");
            repaint();
	} else {
            finalRadialData = new double[2*r][2*r];
            for (int i=0; i<2*r; i++) {
               for (int j=0; j<2*r; j++) {
            	  finalRadialData[i][j]=zoomData[yInitProf+i][xInitProf+j];
               };  
            };	
	}
	
	boolean isCentrd = false;;
	if (centrd[0]==-1) isCentrd = false;
	  else 	isCentrd = true;
	
	radialProfilePanel.update( finalRadialData, fwhm, isCentrd, zoomBuffer.getShortNameNum(), cenPnt);
    }
    public void calcAperture() {
        if (apxinit==0 || apyinit==0 || apxfin==0 || apyfin==0 ) return;
	float xcen = ((float)apxinit+(float)apxfin)/(2*zoomFactor);
	float ycen = ((float)apyinit+(float)apyfin)/(2*zoomFactor);
	float xrad = Math.abs(apxinit-apxfin)/(2.0f*zoomFactor);
	float yrad = Math.abs(apyinit-apyfin)/(2.0f*zoomFactor);
	float bsize = (float)(apbsize)/zoomFactor;
	float isize = 2.0f/zoomFactor;

        if( adjustZoom.setNewMin || adjustZoom.setNewMax ) {
            aperturePanel.update( zoomData, xcen, ycen, xrad, yrad, bsize, isize, adjustZoom.newMin, adjustZoom.newMax, scaleFactor, zoomBuffer.Ndifs, zoomBuffer.getShortNameNum() );
        }
        else aperturePanel.update( zoomData, xcen, ycen, xrad, yrad, bsize, isize, scaleFactor, zoomBuffer.Ndifs, zoomBuffer.getShortNameNum() );
    }

    public void addImageDisplayPanel(ImageDisplayPanel p) {
	this.imDispPanel = p;
    }

    public void setZoomBoxColor(int i) {
        if (i == 1) transLucent = new Color(128,255,128,24);
        else if (i == 2) transLucent = new Color(128,255,128,0);
        else transLucent = new Color(128,255,128,49);
        repaint();
    }

    public int getZoomBoxColor() {
        if (transLucent == new Color(128,255,128,24)) return 1;
        else if (transLucent == new Color(128,255,128,0)) return 2;
        else return 0;
    }

    public void keyPressed(KeyEvent kev) {
        moveMouse(kev);
    }
    public void keyTyped(KeyEvent kev) { }
    public void keyReleased(KeyEvent kev) {}

    private void moveMouse(KeyEvent kev) {
	int xMove = 0, yMove = 0;
        switch (kev.getKeyCode()) {
            case KeyEvent.VK_LEFT:  xMove = -1; break;
            case KeyEvent.VK_RIGHT: xMove = 1; break;
            case KeyEvent.VK_UP:    yMove = -1; break;
            case KeyEvent.VK_DOWN:  yMove = 1; break;
            case KeyEvent.VK_KP_LEFT:  xMove = -1; break;
            case KeyEvent.VK_KP_RIGHT: xMove = 1; break;
            case KeyEvent.VK_KP_UP:    yMove = -1; break;
            case KeyEvent.VK_KP_DOWN:  yMove = 1; break;
            default: return;
        }
	try {
	   Robot r = new Robot();
	   Point p = MouseInfo.getPointerInfo().getLocation();
	   r.mouseMove(p.x+xMove, p.y+yMove);
	} catch(Exception e) {}
    }

    private void _niceAngleLineCut() {
        int dx = cxfin - cxinit;
        int dy = cyfin - cyinit;
        //if line cut is already horizontal or vertical then no problem just return...
        if( dx == 0 || dy == 0 ) return;

        int xmid = (int)Math.round( (cxinit + cxfin)/2.0 );
        int ymid = (int)Math.round( (cyinit + cyfin)/2.0 );
        double slope = Math.abs( (double)dy/(double)dx );

        if( slope < 0.5 ) //default to horizontal line cut:
            {
                cyinit = ymid;
                cyfin = ymid;
            }
        else if( slope < 1.5 )  //default to 45 or 135 deg. angle:
            {
                int avdxy = (int)Math.ceil( ( ( Math.abs(dx) + Math.abs(dy))/4 ) );
                cxinit = xmid - avdxy;
                cxfin = xmid + avdxy;

                if( dx*dy < 0 ) {
                    cyinit = ymid + avdxy;
                    cyfin = ymid - avdxy;
                }
                else {
                    //default to 135 deg. angle:
                    cyinit = ymid - avdxy;
                    cyfin = ymid + avdxy;
                }
            }
        else //default to vertical line cut:
            {
                cxinit = xmid;
                cxfin = xmid;
            }
    }
}
