package ufjdd;
/**
 * Title:        Java Data Display (JDD): DataAccessPanel.java (to access data in frame acq. server buffers)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Frank Varosi, Ziad Saleh, Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  Extends class UFLibPanel to use _socket attrib. and connectToServer() method.
 */
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

class NIRDataAccessPanel extends DataAccessPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: NIRDataAccessPanel.java,v 1.1 2006/03/24 23:02:46 warner Exp $";

    public NIRDataAccessPanel(String host, int port, ImagePanel[] imagePanels)
    {
	super(host, port, imagePanels);
        if (host.equals("")) {
           this.mode = MODE_NOHOST;
        } else {
           this.mode = MODE_REGULAR;
        }
        this.imagePanels = imagePanels;
        this.serverName = "Data Acq. Server";
        this.connectToServer();
        this.initialize();
    }

    public void changeMode (int newMode) {
        if (newMode != 1 && newMode != 4) {
            System.err.println("DataAccessPanel.changeMode> Invalid mode: "+newMode);
        } else {
            mode = newMode;
	    pauseUpdates = true;
	    if (mode == MODE_REGULAR) {
                //InitDataDisplay.obsMonitor.setHostAndPort(_Host, _Port);
		InitDataDisplay.obsMonitor = new UFobsMonitor( _Host, _Port, this );
		//connectButton.doClick();
		if (_socket == null) {
		   InitDataDisplay.obsMonitor.connectToServer();
		   connectToServer();
		}
		pauseUpdates = false;
	    }
	}
    }

    public boolean connectToServer()
    {
	if( super.connectToServer() ) {
	    frameConfig = getFrameConfig();
	    return true;
	}
	else {
	   frameConfig= new UFFrameConfig(2048,2048,32);
	   return false;
	}
    }

    public UFFrameConfig getFrameConfig() {
        // Send the agent a timestamp with the command FC to get Frame Configuration
        UFTimeStamp uft = new UFTimeStamp("FC");
        UFFrameConfig fc = null;
        
        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.getFrameConfig> UFTimeStamp send ERROR");
	    return null;
        }

        if( (fc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
            System.err.println("DataAccess.getFrameConfig> UFFrameConfig recv ERROR");
	    return null;
        }

	System.out.println("DataAccess.getFrameConfig> (width=" + fc.width + ",  height=" + fc.height
			   + ") ---> " + fc.name());
	frameConfig = fc;
	return fc;
    }

    public synchronized boolean request( String buffName )
    {
	if( buffName == null || _socket == null) return false;
        // Send the agent a timestamp with the desired Buffer Name:
        UFTimeStamp uft = new UFTimeStamp(buffName);

        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.request(" + buffName + ")> UFTimeStamp send ERROR");
            return false;
        }
	else return true;
    }

    public synchronized ImageBuffer recvBuff()
    {
        UFFrameConfig bfc = null;
        try {
            if( (bfc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
                System.err.println("DataAccess.recvBuff> UFFrameConfig recv ERROR");
                return null;
            }
        } catch( ClassCastException cce ) {
            System.err.println("DataAccess.recvBuff> Class Cast Exception " + cce);
            return null;
        }
        
        if( bfc.name().indexOf("ERROR") >= 0 ) {
            System.out.println("DataAccess.recvBuff> " + bfc.name());
            return null;
        }
	else {
	    frameConfig = bfc;
	    UFInts ufi = null;
            
            if( (ufi = (UFInts)UFProtocol.createFrom(_socket)) == null )
                System.err.println("DataAccess.recvBuff> UFInts Read ERROR for buffer: " + bfc.name());

            ImageBuffer imgBuffer = new ImageBuffer(bfc, ufi);
            return imgBuffer;
        }
    }

    public void updateFrames(UFFrameConfig notifyFrameConfig) {

	if( pauseUpdates ) return;
	
	//erase previous display:
	/*if( imagePanels[0] != null ) {
	    imagePanels[0].imageDisplay.updateImage( (ImageBuffer)null );
	    imagePanels[0].imageDisplay.applyLinearScale();
	}*/

	ImageBuffer newImgBuff = fetch("new");
	updateScaling(newImgBuff);
    }

    public void updateFrames(ImageBuffer newImgBuff) {
	updateScaling(newImgBuff);
    }

    public void updateScaling(ImageBuffer newImgBuff) {
        if( newImgBuff != null ) {
           String zModeBoxSel = (String)(InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem());
           String scaleBoxSel = (String)(InitDataDisplay.jddFullFrame.scaleBox.getSelectedItem());
           imagePanels[0].imageDisplay.updateImage( newImgBuff );
           if (zModeBoxSel.indexOf("Auto") >= 0) {
              imagePanels[0].adjustPanel.updateMinMax();
              imagePanels[0].adjustPanel.scaleAndDraw(); //checks desired scaling and repaints image.
           } else if (zModeBoxSel.indexOf("Manual") >= 0) {
              if (scaleBoxSel.indexOf("Linear") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(
                                            Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText()),
                                            Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText()) );
                //InitDataDisplay.jddFullFrame.srcPreviewImage.applyLinearScale(min, max);
                //if (InitDataDisplay.jddFullFrame.bgdPreviewImage != null)
                //   InitDataDisplay.jddFullFrame.bgdPreviewImage.applyLinearScale(min, max) ;
              } else if (scaleBoxSel.indexOf("Log") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText()));
              } else if (scaleBoxSel.indexOf("Power") >= 0) {
                imagePanels[0].adjustPanel.reDrawImage(
                                            Double.parseDouble(InitDataDisplay.jddFullFrame.zMax.getText()),
                                            Double.parseDouble(InitDataDisplay.jddFullFrame.zMin.getText()));
              }
           } else {
              System.out.println(scaleBoxSel);
              if (zModeBoxSel.indexOf("Zscale") >= 0) {
                imagePanels[0].imageDisplay.applyZScale();
              } else if (zModeBoxSel.indexOf("Zmax") >= 0) {
                imagePanels[0].imageDisplay.applyZScale("Zmax");
              } else if (zModeBoxSel.indexOf("Zmin") >= 0) {
                imagePanels[0].imageDisplay.applyZScale("Zmin");
              }
              imagePanels[0].adjustPanel.reDrawImage(Float.parseFloat(InitDataDisplay.jddFullFrame.zMin.getText()), Float.parseFloat(InitDataDisplay.jddFullFrame.zMax.getText()) );
           }
           if (hasCsf) csf.updateStats(imagePanels[0].imageDisplay.imageBuffer.pixels, true);
        }
        imagePanels[0].imageDisplay.setZoomImage();
    }

    public void setCsf(ChannelStatFrame csf) {
        this.csf = csf;
        this.hasCsf = true;
    }

    public void removeCsf() {
        this.hasCsf = false;
    }
}
