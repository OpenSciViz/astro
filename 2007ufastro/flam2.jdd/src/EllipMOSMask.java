package ufjdd;
/**
 * Title:        EllipMOSMask.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner
 * Company:      University of Florida
 * Description:  Creates Masks to overlay on top of the data in jdd
 */

import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class EllipMOSMask extends MOSMask {

   float xrad, yrad;
   int imgxrad, imgyrad;

   public EllipMOSMask(float x, float y, float xr, float yr) {
      //Correct for ds9 starting with (1,1) instead of (0,0)
      x--;
      y--;
      this.shape = "Ellipse";
      this.xcen = x;
      this.ycen = y;
      this.xrad = xr;
      this.yrad = yr;
      this.text = "";
      setScale(1,1);
   }

   public EllipMOSMask(float x, float y, float r) {
      this(x,y,r,r);
      this.shape = "Circle";
   }

   public EllipMOSMask(float x, float y) {
      this(x,y,5,5);
      this.shape = "Point";
   }

   public EllipMOSMask(float x, float y, float xr, float yr, String text) {
      x--;
      y--;
      this.shape = "Ellipse";
      this.xcen = x;
      this.ycen = y;
      this.xrad = xr;
      this.yrad = yr;
      this.text = text;
      setScale(1,1);
   }

   public EllipMOSMask(float x, float y, float r, String text) {
      this(x,y,r,r,text);
      this.shape = "Circle";
   }

   public EllipMOSMask(float x, float y, String text) {
      this(x,y,3,3,text);
      this.shape = "Point";
   }

   public void drawMOSMask(Graphics g) {
      Color oldColor = g.getColor();
      if (color != null) g.setColor(color);
      g.drawOval(imgxcen, imgycen, imgxrad, imgyrad);
      if (displayText) {
	g.setFont(f);
	g.drawString(text, imgxcen, imgycen);
      }
      if (color != null) g.setColor(oldColor);
   }

   public void setScale(float xscale, float yscale) {
      this.imgxcen = Math.round(xcen*xscale);
      this.imgycen = Math.round(ycen*yscale);
      this.imgxrad = Math.round(xrad*xscale);
      this.imgyrad = Math.round(yrad*yscale);
   }

   public void setScale(float xscale, float yscale, float ysize) {
      this.imgxcen = Math.round(xcen*xscale);
      this.imgycen = Math.round((ysize-ycen)*yscale);
      this.imgxrad = Math.round(xrad*xscale);
      this.imgyrad = Math.round(yrad*yscale);
   }

   public void addText(String text) {
      this.text = text;
   }

   public void setTextMode(boolean b) {
      this.displayText = b;
   }

   public void setFont(Font f) {
      this.f = f;
   }

   public void setColor(String s) {
      if (s.toLowerCase().equals("green")) this.color =  Color.GREEN;
      else if (s.toLowerCase().equals("red")) this.color = Color.RED;
      else if (s.toLowerCase().equals("blue")) this.color = Color.BLUE;
      else if (s.toLowerCase().equals("black")) this.color = Color.BLACK;
      else if (s.toLowerCase().equals("white")) this.color = Color.WHITE;
      else if (s.toLowerCase().equals("cyan")) this.color = Color.CYAN;
      else if (s.toLowerCase().equals("magenta")) this.color = Color.MAGENTA;
      else if (s.toLowerCase().equals("yellow")) this.color = Color.YELLOW;
   }
}
