import javaUFProtocol.*;
import java.io.*;
import java.net.*;


public class UFQlook {

    protected static final int DEFAULT_TIMEOUT = 9000; //milliseconds.
    protected static final int CONNECT_TIMEOUT = 7000;
    protected static final int HANDSHAKE_TIMEOUT = 8000;
    //protected int socTimeout = DEFAULT_TIMEOUT;
    protected int socTimeout = 0;

    Socket _socket;

    public UFQlook() {
	connectToDaemon("irflam2a",52001,"replication");
	while (true) {
	    UFProtocol ufp = UFProtocol.createFrom(_socket);
	    if (ufp.typeId() == UFProtocol.MsgTyp._Floats_) {
		System.out.println("UFFloats!");
	    } else if (ufp.typeId() == UFProtocol.MsgTyp._Ints_) {
		System.out.println("UFInts!");
	    } else if (ufp.typeId() == UFProtocol.MsgTyp._TimeStamp_){
		System.out.println("UFTimeStamp!");
	    } else{
		System.out.println("Unknown! Type id: " + ufp.typeId());
	    }
	}
    }

    public boolean connectToDaemon(String host, int port, String handshake) {
	try {
	    InetSocketAddress agentIPsoca = new InetSocketAddress(host,port);

	    _socket = new Socket();
	    _socket.connect(agentIPsoca, CONNECT_TIMEOUT);
	    _socket.setSoTimeout( HANDSHAKE_TIMEOUT);
	    
	    UFTimeStamp uft = new UFTimeStamp(handshake);
	    if( uft.sendTo(_socket) <= 0 ) {
		System.err.println("UFQlook.connectToDaemon> Error sending"+
				   " ufprotocol object");
		return false;
	    }
	    
	    //get response from agent
	    UFProtocol ufp = null;
	    
	    if( (ufp = UFProtocol.createFrom(_socket)) == null ) {
		System.err.println("UFQlook.connectToDaemon> Error receiving"+
				   " ufprotocol object");
		return false;
	    }
	    
	    //set normal timeout for socket
	    _socket.setSoTimeout( socTimeout );
	    return true;
	}
	catch (Exception x) {
	    _socket = null;
	    System.err.println("UFQlook.connectToDaemon> "+x.toString());
	    return false;
	}
    }

    public static void main(String [] args) {
	new UFQlook();
    }

}
