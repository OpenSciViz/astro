import javaUFProtocol.*;
import java.io.*;
import java.net.*;


public class UFProtocolTestClient {

    protected static final int DEFAULT_TIMEOUT = 9000; //milliseconds.
    protected static final int CONNECT_TIMEOUT = 7000;
    protected static final int HANDSHAKE_TIMEOUT = 8000;
    protected int socTimeout = 0;

    Socket _socket;

    public UFProtocolTestClient() {
	//connectToServer("irflam2a",52001,"replicant");
	connectToServer("localhost",52100,"hello");
	while (true) {
	    UFProtocol ufp = UFProtocol.createFrom(_socket);
	    if (ufp instanceof UFInts) {
		System.out.println("Got UFInts!");
	    } else if (ufp instanceof UFFloats) {
		System.out.println("Got UFFloats!");
	    } else if (ufp instanceof UFStrings) {
		System.out.println("Got UFStrings!");
	    } else if (ufp instanceof UFTimeStamp) {
		System.out.println("Got UFTimeStamp!");
	    } else {
		System.out.println("Got Unknown!");
	    }
	    System.out.println("Free Mem: "+
			       Runtime.getRuntime().freeMemory());
	}
    }

    public boolean connectToServer(String host, int port, String handshake) {
	try {
	    InetSocketAddress agentIPsoca = new InetSocketAddress(host,port);

	    _socket = new Socket();
	    _socket.connect(agentIPsoca, CONNECT_TIMEOUT);
	    _socket.setSoTimeout( HANDSHAKE_TIMEOUT);
	    
	    UFTimeStamp uft = new UFTimeStamp(handshake);
	    if( uft.sendTo(_socket) <= 0 ) {
		System.err.println("UFQlook.connectToDaemon> Error sending"+
				   " ufprotocol object");
		return false;
	    }
	    
	    //get response from agent
	    UFProtocol ufp = null;
	    
	    if( (ufp = UFProtocol.createFrom(_socket)) == null ) {
		System.err.println("UFQlook.connectToDaemon> Error receiving"+
				   " ufprotocol object");
		return false;
	    }
	    
	    //set normal timeout for socket
	    _socket.setSoTimeout( socTimeout );
	    return true;
	}
	catch (Exception x) {
	    _socket = null;
	    System.err.println("UFQlook.connectToDaemon> "+x.toString());
	    return false;
	}
    }

    public static void main(String [] args) {
	new UFProtocolTestClient();
    }

}
