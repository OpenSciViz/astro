import javaUFProtocol.*;
import java.io.*;
import java.net.*;


public class UFProtocolTestServer {

    protected static final int DEFAULT_TIMEOUT = 9000; //milliseconds.
    protected static final int CONNECT_TIMEOUT = 7000;
    protected static final int HANDSHAKE_TIMEOUT = 8000;
    protected int socTimeout = DEFAULT_TIMEOUT;

    Socket _socket;

    public UFProtocolTestServer() {
	//connectToDaemon("irflam2a",52001,"replicant");
	if (getClientConnection(52100)) {
	    String [] types = {"UFTimeStamp","UFInts","UFFloats","UFStrings"};
	    int [] imageData = new int[2048*2048];
	    float [] flts = {0.0f};
	    for (int i=0; i<imageData.length; i++) imageData[i] = i;
	    while (true ) {
		
		(new UFFloats("Hello Yourself",flts)).sendTo(_socket);
		System.out.println("Sent UFFloats");
		try { Thread.sleep(1000); } catch (Exception e){}
		(new UFInts("Hello Yourself",imageData)).sendTo(_socket);
		System.out.println("Sent UFInts");
// 		int i = (int)(Math.random()*4);
// 		if (i == 0) {
// 		    (new UFTimeStamp("Hello yourself")).sendTo(_socket);
// 		} else if (i == 1) {
// 		    (new UFInts("Hello yourself",imageData)).sendTo(_socket);
// 		} else if (i == 2) {
// 		    float [] flts = { 0.0f };
// 		    (new UFFloats("Hello yourself",flts)).sendTo(_socket);
// 		} else if (i == 3) {
// 		    String [] strs = { "Hello yourself" };
// 		    (new UFStrings("hello",strs)).sendTo(_socket);
// 		}		
// 		System.out.println("Sent "+types[i]);
		
		
		System.out.println("Free Mem: "+
				   Runtime.getRuntime().freeMemory());
		try { Thread.sleep(1000); }
		catch (Exception e) {
		    System.err.println("Who woke me up? "+e.toString());
		}
	    }
	}
    }

    public boolean getClientConnection(int port) {
	try {
	    ServerSocket ss = new ServerSocket(port);
	    _socket = ss.accept();
	    UFProtocol.createFrom(_socket);
	    (new UFTimeStamp("Hello Yourself")).sendTo(_socket);
	    return true;
	} catch (Exception e) {
	    System.err.println("UFProtocolTestServer.getClientConnection> "+
			       e.toString());
	    return false;
	}
    }

    public static void main(String [] args) {
	new UFProtocolTestServer();
    }

}
