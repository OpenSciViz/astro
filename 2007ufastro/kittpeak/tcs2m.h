#ifndef __TCS2M_H__
#define __TCS2M_H__

#include	<string>

using std::string ;

extern "C" {
  #include "tcpclnLib.h"
}

static const size_t __ResultSize__ = 4096 ;

class tcs2m {

public:
	virtual ~tcs2m() ;

	static int main( int, char** ) ;
	static tcs2m* create( const string& ) ;
	static int commandTcs( const string& command, string& result );

	string getFocus() ; // microns
	string getOffset() ; // arc-sec. east, west
	string getInformation() ; // all

	string setFocus( int ) ; // relative +/- (microns)
	string setFocus( unsigned int ) ; // absolute (microns)

	string zeroOffset() ;
	string setOffsetRel( unsigned int east, unsigned int north,
		char polarity = '+' ) ;
	string setOffsetAbs( unsigned int, unsigned int ) ;

protected:
	tcs2m() {}
	tcs2m( const tcs2m& ) {}
	tcs2m( const string& host )
		: _host( host ) {}


	string	_host ;
	static tcs2m* _instance ;

} ;

#endif // __TCS2M_H__
