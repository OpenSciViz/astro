/* tcpclnLib.c  02/18/99        NOAO    */

/*
 *  Developed 1998 by the National Optical Astronomy Observatories(*)
 *
 * (*) Operated by the Association of Universities for Research in
 *     Astronomy, Inc. (AURA) under cooperative agreement with the
 *     National Science Foundation.
 */

/*
 * modification history
 * --------------------
 * 01a 20jun95,gsh - written from tcpLocalCmd.
 * 02a 01mar96,gsh - moved to 5.2
 * 01a 26mar99,rcr - Ported to Linux 2.0.36.
 */

/*----------------------------------------------------------------------
 * tcpclnLib.c
 *
 * This is the client side of the TCP-RPC interface. This module is
 * used to communicate with the server.
 *---------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>
#include "tcpclnLib.h"

#define HOW_QUEUE 1

/*----------------------------------------------------------------------
 *	Global Variables
 *---------------------------------------------------------------------*/

int interested = 0;

/*----------------------------------------------------------------------
 *	Local Variables
 *---------------------------------------------------------------------*/

static CLIENT *tcpClient = NULL;

/* default RPC timeout */
static struct timeval defTimeout = {40, 0};

static tcp_msg *tcpLastReply = NULL;
static int rpcClnStatus = -1 ;
static char *tcpServer = NULL;

/*----------------------------------------------------------------------
 *	Local Function Prototypes
 *---------------------------------------------------------------------*/

/*
int tcpClnOpen (char *name);
int tcpClnClose ();
int tcpClnCmd (char *msg, int how);
char *tcpClnReply ();
*/
static tcp_msg *send_tcp_msg_1 (tcp_msg * argp, CLIENT * clnt);

/*----------------------------------------------------------------------
 * tcpClnOpen - open a tcp command channel to a named server system
 *
 *---------------------------------------------------------------------*/
int tcpClnOpen (char *name)
{
  char host[4096] = { 0 } ;
  int uid = 0, gid = 0;

  /* open new server */
  tcpClient = clnt_create (name, TCP_MSG_PROG, TCP_MSG_VERS, "tcp");
  if (tcpClient == NULL) {
    clnt_pcreateerror (name);
    return (ERROR);
  }
  uid = 0;
  gid = 127;
  gethostname (host, sizeof (host));
  tcpClient->cl_auth = authunix_create (host, uid, gid, 0, 0);
  return OK;
}

/*----------------------------------------------------------------------
 * tcpClnClose - the client registration
 *
 *---------------------------------------------------------------------*/
int tcpClnClose ()
{
  if (tcpLastReply != NULL)
    xdr_free ((xdrproc_t) xdr_tcp_msg_cln, (char *) tcpLastReply);
  tcpLastReply = NULL;
  tcpClient = NULL;
  return OK;
}

/*----------------------------------------------------------------------
 * tcpClnCmd - send a command over the communications channel
 *
 * The return code is the success/failure of the communications
 * path, not the success/failure of the command contents.  Use
 * tcpLocalError() to get the specific message reply code for the
 * most recent message.
 *
 * The ASCII reply to the command message is obtained with the
 * tcpClnReply() command.  You should check the reply message code
 * first using the tcpLocalError() command.
 *
 * A return code of ERROR indicates the communications channel
 * has failed and has been closed.  A new 'tcpClnOpen' command
 * must be given before communications can be resumed, if at all.
 *
 * The 'how' code is a historic hangover.  Always use how = 1
 * for proper operation.  Other modes are only partially supported
 * and are not guaranteed in the future.
 *
 *---------------------------------------------------------------------*/
int tcpClnCmd (char *msg, int how)
{
  tcp_msg command;

  if (tcpClient == NULL)
    return (ERROR);
  if (tcpLastReply != NULL)
    {
    /* BUG: This could cause a seg fault later on if clients
     * of this library still have a pointer to the message
     * contained by tcpLastReply (see tcpClnReply() -- erroneous
     * method).
     */
    xdr_free ((xdrproc_t) xdr_tcp_msg_cln, (char *) tcpLastReply);
    }
  tcpLastReply = NULL;
  command.message = msg;
  command.how = how;

  tcpLastReply = send_tcp_msg_1 (&command, tcpClient);
  if (tcpLastReply == NULL) {
    /* fatal RPC communications error */
    clnt_perror (tcpClient, tcpServer);
    tcpClnClose ();
    return (ERROR);
  }

  return (OK);
}

/*----------------------------------------------------------------------
 * tcpClnReply - response message from last rpc message
 *
 * A NULL pointer is returned if the channel is not open.
 *
 * In tcp command error situations, you should examine the response
 * for one or more lines beginning with "ERROR".  These lines are
 * the error explanation.  At least one ERROR line is always guaranteed
 * in cases of message error.
 *
 *---------------------------------------------------------------------*/
char *tcpClnReply ()
{
  if (tcpLastReply != NULL)
    return (tcpLastReply->message);

  return (NULL);
}

/*----------------------------------------------------------------------
 * send_tcp_msg_1 - the actual RPC call.
 *
 * This is defined as a local, because there is a global send_tcp_msg_1
 * defined in the server code that is different.  This routine invokes
 * the RPC mechanism to contact the server.  The server's version sends a
 * message to the parser to actually perform parsing and execution.
 *
 * This version is the one defined in the client side code.
 *---------------------------------------------------------------------*/
static tcp_msg *send_tcp_msg_1 (tcp_msg * argp, CLIENT * clnt)
{
static tcp_msg res;

memset( (void*) &res, 0, sizeof( res ) ) ;
if (clnt_call (clnt, SEND_TCP_MSG, (xdrproc_t) xdr_tcp_msg_cln,
		 (caddr_t) argp, (xdrproc_t) xdr_tcp_msg_cln,
		 (caddr_t) & res, defTimeout) != RPC_SUCCESS)
	{
	return (NULL);
	}

return (&res);
}

/*----------------------------------------------------------------------
 * xdr_tcp_msg_cln - transport a tcp message
 *
 * This is the network transport routine for handling a tcp_msg
 * over the RPC protocol.  It exists in two different versions.
 * One for the client side and one for the server side. 
 *---------------------------------------------------------------------*/
bool_t xdr_tcp_msg_cln (XDR * xdrs, tcp_msg * objp)
{
  if (!xdr_int (xdrs, &objp->how))
    return (FALSE);
  if (!xdr_string (xdrs, &objp->message, 2048))
    return (FALSE);

  return (TRUE);
}

/*----------------------------------------------------------------------
 *	Get/Set Module Interface
 *---------------------------------------------------------------------*/
int tcpcln_get_status ()
{
  return rpcClnStatus ;
}
