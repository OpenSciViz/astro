const char rcsId[] = "$Id: tcs4m.cc 14 2008-06-11 01:49:45Z hon $";

#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>

#include <new>
#include <string>
#include <iostream>
#include <strstream>

#include <cstdio>
#include <ctime>
#include <cstdlib>

#include "tcs4m.h"

using std::string ;
using std::strstream ;
using std::cin ;
using std::endl ;
using std::cout ;
using std::ends ;
using std::clog ;
using std::nothrow ;

tcs4m* tcs4m::_instance = 0 ;

int main( int argc, char** argv ) {
  return tcs4m::main( argc, argv );
}

int tcs4m::commandTcs(const string& command, string& result) {
  //clog << "tcs4m::commandTcs> Sending: " << command << endl ;

  int status = ::tcpClnCmd(const_cast< char* >(command.c_str()), 1);
  result = ::tcpClnReply();
  return status;
}

int tcs4m::main( int argc, char** argv ) {
  tcs4m* tcs = tcs4m::create( "sienna.kpno.noao.edu" ) ;
  if( NULL == tcs ) {
    clog << "Unable to connect to cyan.kpno.noao.edu\n" ;
    return -1;
  }

  string result;
  string command;
  int status = 0 ;

  if( argc > 1 ) {
    //clog <<argv[0]<<" "<<argv[1]<<endl;
    if( argv[1][strlen(argv[1])-1] == '"' )
      argv[1][strlen(argv[1])-1] = '\0';

    if( argv[1][0] == '"' )
      command = &argv[1][1];
    else
      command = &argv[1][0];

    status = tcs->commandTcs( command, result );
    //printf( "%s (%d) -> %s\n", command.c_str(), status, result.c_str());
    cout<<result<<endl;
    delete tcs ;
    return 0;
  }

  while ( true ) {
    cout << "tcs> " ;
    string line ;
    getline( cin, line ) ;
    if( (line == "exit") || (line == "quit") ) {
      cout << "Exiting...\n" ;
      break ;
    }
    command = line;
    status = tcs->commandTcs(command, result);
    printf("%s (%d) -> %s\n",command.c_str(), status, result.c_str());
  } // while( true )
	
  delete tcs;
  return 0;
}

tcs4m::~tcs4m() {
  if( _instance != NULL ) {
    ::tcpClnClose();
    tcs4m* tmp = _instance;
    _instance = 0;
    delete tmp;
  }
}

tcs4m* tcs4m::create( const string& hostName ) {
  if( _instance != 0 ) {
    return _instance ;
  }

  if( ERROR == ::tcpClnOpen( const_cast< char* >( hostName.c_str() ) ) ) {
    clog << "tcs4m::create> Failed to connect to: "
	 << hostName << endl ;
    return 0 ;
  }

  _instance = new (nothrow) tcs4m( hostName ) ;
  return _instance ;
}

string tcs4m::getFocus() {
  string command("tele focus");
  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs4m::getInformation() {
  string command("tele info");
  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string() ;
  }

  return result ;
}

string tcs4m::getOffset() {
  string command("tele offset");
  string result( __ResultSize__, 0 ) ;

  int status = commandTcs( command, result ) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs4m::setFocus( int relative ) {
  strstream s ;
  s	<< "tele focus " ;
  s	<< ((relative >= 0) ? '+' : '-') << '=' ;
  s	<< ' ' << ::abs( relative ) << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status )	{
    return string("ERROR") ;
  }

  return result ;
}

string tcs4m::setFocus( unsigned int absolute ) {
  strstream s ;
  s	<< "tele focus = " << absolute << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs4m::zeroOffset() {
  strstream s ;
  s	<< "tele offset = 0" << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

// relative
string tcs4m::setOffsetRel( unsigned int east, unsigned int north,
			    char polarity ) {
  strstream s ;
  s	<< "tele offset " ;
  s	<< polarity << '=' ;
  s	<< east << ", " ;
  s	<< north << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

// absolute
string tcs4m::setOffsetAbs( unsigned int east, unsigned int north ) {
  strstream s ;
  s	<< "tele offset = " << east << ", " << north << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}
