; Detector Parameters 
; Comment lines may be added anywhere unless explicity stated otherwise.
; Placing a semicolon ANYWHERE in a line will completely comment out
; the entire line (they have been placed in the beginning of the lines
; here as a matter of convention).
;
; dt_clk
0.123
; tmin
0.128
; max_frmcoadd
10.0
; pre_valid_time
10.0
; post_valid_time
10.0
; max_frmtime
10.0
; min_frmtime
10.0
; max_chpfreq
10.0
; max_savefreq
0.1
; min_savefreq
0.1
;
;User Parameters
;
; frmtime
10.0
; savefreq
0.1
; objtime
1.0
; chpfreq
10.0
; chpdelay
10.0
; nodtime
30.0
; noddelay
4.0
