#
#  T-ReCS filter name to wheel position translation table
#


# Name		Filter 1	Filter 2	Window

Si-7.9um	Si-7.9um	Open		ZnSe  
PAH-8.6um	PAH-8.6um	Open		ZnSe
PAH2		PAH2		Open		ZnSe
Si-8.8um	Si-8.8um	Open		ZnSe
Si-9.7um	Si-9.7um	Open		ZnSe
Si-10.4um	Si-10.4um	Open		ZnSe
Si-11.7um	Si-11.7um	Open		ZnSe
Si-12.3um	Si-12.3um	Open		ZnSe
Ar-III		Ar-III		Open		ZnSe
K	 	Open		K		ZnSe               
L	 	Open		L		ZnSe     
M	 	Open		M		ZnSe      
N	 	Open		N		ZnSe 
NeII	 	Open		NeII		ZnSe 
NeII_ref2 	Open		NeII_ref2	ZnSe 
SIV	 	Open		SIV		ZnSe 
Qs-18.3um	Open		Qs-18.3um	ZnSe
Qone		Open		Qone		ZnSe
Ql-24.5um	Open		Ql-24.5um	ZnSe
Qw-20.8um	Qw-20.8um	Open		ZnSe
Align-Spot	Align-Spot	Align-Spot	ZnSe
