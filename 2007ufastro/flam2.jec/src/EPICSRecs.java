package uffjec;

import java.util.*;

public class EPICSRecs extends HashMap {
    public static final String initialvelocity = "initialveloctiy";

    //temperature panel
    public static final String camrange_a = "camrange_a";
    public static final String camonoff_b = "camonoff_b";
    public static final String camsetpoint_a = "camsetpoint_a";
    public static final String camsetpoint_b = "camsetpoint_b";
    public static final String camp_a = "camp_a";
    public static final String cami_a = "cami_a";
    public static final String camd_a = "camd_a";
    public static final String camp_b = "camp_b";
    public static final String cami_b = "cami_b";
    public static final String camd_b = "camd_b";
    public static final String valCamrange_a = "valCamrange_a";
    public static final String valCamonoff_b = "valCamonoff_b";
    public static final String valCamsetpoint_a = "valCamsetpoint_a";
    public static final String valCamsetpoint_b = "valCamsetpoint_b";
    public static final String valCamp_a = "valCamp_a";
    public static final String valCami_a = "valCami_a";
    public static final String valCamd_a = "valCamd_a";
    public static final String valCamp_b = "valCamp_b";
    public static final String valCami_b = "valCami_b";
    public static final String valCamd_b = "valCamd_b";

    public static final String lock = "lock";
    public static final String valLock = "valLock";

    public static final String sadCamABench = "sadCamABench";
    public static final String sadCamBDet = "sadCamBDet";
    public static final String sadCamASetPt = "sadCamASetPt";
    public static final String sadCamBSetPt = "sadCamBSetPt";
    public static final String sadCamAAPwr = "sadCamAAPwr";
    public static final String sadCamBAPwr = "sadCamBAPwr";
    public static final String sadAmanual = "sadAmanual";
    public static final String sadBmanual = "sadBmanual";
    public static final String valAmanual = "valAmanual";
    public static final String valBmanual = "valBmanual";
    public static final String amanual = "amanual";
    public static final String bmanual = "bmanual";

    public static final String sadAnalog = "sadAnalog";
    public static final String sadCamARange = "sadCamARange";
    public static final String sadLock = "sadLock";
    public static final String sadCamAPID = "sadCamAPID";
    public static final String sadCamBPID = "sadCamBPID";

    //detector high-level panel
    public static final String lvdtPosition = "lvdtPosition";
    public static final String lvdtVoltage = "lvdtVoltage"; 
    public static final String biasValue = "biasValue"; 
    public static final String focalPosition = "focalPosition"; 
    public static final String numReads = "numReads"; 
    public static final String exposureTime = "exposureTime";
    //detector low-level panel
    public static final String preamp_partial = "preamp_partial";
    public static final String valPreamp_partial = "valPreAmp_partial";
    public static final String sadPreamp_partial = "sadPramp";
    public static final String biasGate = "biasGate";
    public static final String biasPwr = "biasPwr";
    public static final String biasVCC = "biasVCC";
    public static final String biasVReset = "biasVReset";
    public static final String valBiasGate = "valBiasGate";
    public static final String valBiasPwr = "valBiasPwr";
    public static final String valBiasVCC = "valBiasVCC";
    public static final String valBiasVReset = "valBiasVReset";
    public static final String sadBiasGate = "sadBiasGate";
    public static final String sadBiasPwr = "sadBiasPwr";
    public static final String sadBiasVCC = "sadBiasVCC";
    public static final String sadBiasVReset = "sadBiasVReset";
    public static final String pixelBaseClock = "pixelBaseClock";
    public static final String valPixelBaseClock = "valPixelBaseClock";
    public static final String sadPixelBaseClock = "sadPixelBaseClock";
    public static final String expCnt = "expCnt";
    public static final String valExpCnt = "valExpCnt";
    public static final String preResets = "preResets";
    public static final String postResets = "postResets";
    public static final String valPreResets = "valPreResets";
    public static final String valPostResets = "valPostResets";
    public static final String sadNumPreResets = "sadNumPreResets";
    public static final String sadNumPostResets = "sadNumPostResets";
    public static final String cdsReads = "cdsReads";
    public static final String valCDSReads = "valCDSReads";
    public static final String sadCDSReads = "sadCDSReads";
    public static final String cycleType = "cycleType";
    public static final String valCycleType = "valCycleType";
    public static final String sadCycleType = "sadCycleType";

    public static final String mceAction = "mceAction";
    public static final String mceQuery = "mceQuery";
    public static final String setIdleMode = "setIdleMode";
    public static final String integrationTime = "integrationTime";
    public static final String labDef = "labDef";


    //observation panel
    public static final String pause = "pause";
    public static final String pauseC = "pauseC";
    public static final String continu = "continu";
    public static final String continueC = "continueC";
    public static final String stop = "stop";
    public static final String stopC = "stopC";
    public static final String abort = "abort";
    public static final String abortC = "abortC";
    public static final String apply = "apply";
    public static final String observe = "observe";
    public static final String observeC = "observeC";
    public static final String dataLabel = "dataLabel";
    public static final String valDataLabel = "valDataLabel";
    public static final String dataMode = "dataMode";
    public static final String valDataMode = "valDataMode";
    public static final String ditherIndex = "ditherIndex";
    public static final String valDitherIndex = "valDitherIndex";
    public static final String userInfo = "userInfo";
    public static final String valUserInfo = "valUserInfo";
    public static final String pixLutFile = "pixLutFile";
    public static final String valPixLutFile = "valPixLutFile";

    //obsSetup panel
    public static final String expTime = "expTime";
    public static final String valExpTime = "valExpTime";
    //public static final String numReads = "numReads";
    public static final String valNumReads ="valNumReads";
    public static final String biasMode = "biasMode";
    public static final String valBiasMode = "valBiasMode";
    public static final String overrideBias = "overrideBias";
    public static final String valOverrideBias = "valOverrideBias";
    public static final String readoutMode= "readoutMode";
    public static final String valReadoutMode = "valReadoutMode";
    //public static final String pixLutFile = "pixLutFile";
    //public static final String valPixLutFile = "valPixLutFile";
    public static final String instrumSetupC = "instrumSetupC";
    public static final String obsSetupC = "obsSetupC";


    //instrument setup panel
    public static final String reboot = "reboot";
    public static final String init = "init";
    public static final String datum = "datum";
    public static final String test = "test";
    public static final String rebootC = "rebootC";
    public static final String initC = "initC";
    public static final String datumC = "datumC";
    public static final String testC = "testC";
    public static final String beamMode = "beamMode";
    public static final String valBeamMode = "valBeamMode";
    public static final String wheelBiasMode = "wheelBiasMode";
    public static final String valWheelBiasMode = "valWheelBiasMode";
    public static final String windowCover = "windowCover";
    public static final String valWindowCover = "valWindowCover";
    public static final String mosSlit = "mosSlit";
    public static final String valMosSlit = "valMosSlit";
    public static final String decker = "decker";
    public static final String valDecker = "valDecker";;
    public static final String overrideDecker = "overrideDecker";
    public static final String valOverrideDecker = "valOverrideDecker";
    public static final String detPosFocus = "detPosFocus";
    public static final String valDetPosFocus = "valDetPosFocus";
    public static final String overrideDetPos = "overrideDetPos";
    public static final String valOverrideDetPos = "valOverrideDetPos";
    public static final String filter = "filter";
    public static final String valFilter = "valFilter";
    public static final String grism = "grism";
    public static final String valGrism = "valGrism";
    public static final String overrideGrism = "overrideGrism";
    public static final String valOverrideGrism = "valOverrideGrism";
    public static final String lyot = "lyot";
    public static final String valLyot = "valLyot";
    public static final String overrideLyot = "overrideLyot";
    public static final String valOverrideLyot = "valOverrideLyot";
    public static final String camSetPointA = "camSetPointA";
    public static final String valCamSetPointA = "valCamSetPointA";
    public static final String camSetPointB = "camSetPointB";
    public static final String valCamSetPointB = "valCamSetPointB";
    public static final String overrideCamSetPointA = "overrideCamSetPointA";
    public static final String valOverrideCamSetPointA = "valOverrideCamSetPointA";
    public static final String overrideCamSetPointB = "overrideCamSetPointB";
    public static final String valOverrideCamSetPointB = "valOverrideCamSetPointB";
    public static final String mosSetPoint = "mosSetPoint";
    public static final String valMosSetPoint = "valMosSetPoint";
    public static final String overrideMosSetPoint = "overrideMosSetPoint";
    public static final String valOverrideMosSetPoint = "valOverrideMosSetPoint";
    public static final String instrumSetupMark = "instrumSetupMark";
    public static final String obsSetupMark = "obsSetupMark";
    public static final String observeMark = "observeMark";

    public static final String ccSetup = "ccSetup";
    public static final String ccDatum = "ccDatum"; 
    public static final String abortAll = "abortAll";
    public static final String setupOrigin = "setupOrigin";
    public static final String ccSetupMark = "ccSetupMark";
    public static final String ccDatumMark = "ccDatumMark";
    public static final String ccParkMark = "ccParkMark";
    public static final String ccClearDatum = "ccClearDatum";
    public static final String ccDatumAll = "ccDatumAll";

    public static final String ecSetup = "ecSetup";
    public static final String ecDatum = "ecDatum";
    public static final String cSetupMark = "ecSetupMark";
    public static final String ecDatumMark = "ecDatumMark";
    public static final String ecParkMark = "ecParkMark";
    public static final String ecClearDatum = "ecClearDatum";
    public static final String ecDatumAll = "ecDatumAll";

    //LVDT panel
    public static final String lvdtPwrOnOff = "LVDTPwrOnOff";
    public static final String lvdtExcVoltsHiLow = "LVDTExcVoltsHiLow";
    public static final String lvdtExcFreqHiLow = "LVDTExcFreqHiLow";
    public static final String limitVel = "LimitVel";
    public static final String focusStep = "focusStep";
    public static final String stepVel = "stepVel";

    //Eng Observe panel
    public static final String dhsInit = "dhsInit";
    public static final String dhsLabel = "dhsLabel";
    public static final String ds9 = "ds9";
    public static final String edtInit = "edtInit";
    public static final String engObsExpTime = "engObsExpTime";
    public static final String frameMode = "frameMode";
    public static final String engObsIndex = "engObsIndex";
    public static final String fitsFile = "fitsFile";
    public static final String pngFile = "pngFile";
    public static final String engObsLut = "engObsLut";
    public static final String simMode = "simMode";
    public static final String sadEDTTotal = "sadEDTTotal";
    public static final String sadEDTFrame = "sadEDTFrame";
    public static final String sadEDTActn = "sadEDTActn";

    //MOS Barcode
    public static final String MOSBarCd = "MOSBarCd";
    public static final String MOSBarCdDir = "MOSBarCdDir";
    public static final String sadMosSteps = "sadMosSteps";
    public static final String MOSBarCdDirection = "MOSBarCdDirection"; 
    public static final String datumMOSBarCd = "datumMOSBarCd";
    public static final String datumMOSBarCdMark = "datumMOSBarCdMark";
    public static final String MOSBarCdSteps = "MOSBarCdSteps";
    public static final String MOSBarCdVAL = "MOSBarCdVAL";

    public static String[] carNames;

    public boolean isPortable;

    private String epicsPrefix; 

    static String [] portableCarNames = {
        "applyC",
        "rebootC",
        "abortC",
        "stopC",
        "initC",
        "pauseC",
        "continueC",
        "datumC",
        "cc:abortC",
        "dc:abortC",
        "ec:abortC",
        "cc:stopC",
        "dc:stopC",
        "ec:stopC",
        "cc:datumC",
        "dc:datumC",
        "ec:datumC",
        "cc:initC",
        "cc:setupC",
        "cc:setupBarCdC",
        "cc:testC",
        "dc:initC",
        "dc:setupC",
        "dc:testC",
        "dc:mcecmdC",
        "ec:initC",
        "ec:setupC"
    };

    static String [] vmeCarNames = {
        "applyC",
        "rebootC",
        "abortC",
        "stopC",
        "initC",
        "pauseC",
        "continueC",
        "datumC",
        "cc:applyC",
        "dc:applyC",
        "ec:applyC",
    };

    public EPICSRecs() {
	this(EPICS.PORTABLE, "flam:");
    }

    public EPICSRecs(boolean isPortable) {
	this(isPortable, "flam:");
    }

    public EPICSRecs(boolean isPortable, String prefix) {
	this.isPortable = isPortable;
	this.epicsPrefix = prefix;
	populateHashMap();
    }

    public void changeType(boolean isPortable) {
	this.isPortable = isPortable;
	populateHashMap();
    }

    public void changePrefix(String prefix) {
	this.epicsPrefix = prefix;
    }

    public void populateHashMap() {
	clear();
	getTemperatureNames();
	getDetectorNames();
	getObsSetupNames();
	getObserveNames();
	getInstrumentSetupNames();
        getLvdtNames();
	getEngObserveNames();
	String[] indexorNames = {"Window","MOS","Decker","Filter1","Lyot","Filter2","Grism","Focus"};
	for (int j = 0; j < indexorNames.length; j++) {
	    getIndexorNames(indexorNames[j]);
	}
	if (isPortable) getIndexorNames("MOSBarCd");
	getMosBarCdNames();
	setCarNames();
    }

    public String get(Object key) {
	String temp = (String)(super.get(key));
	return epicsPrefix+temp;
    }

    public String getName(Object key) {
	String temp = (String)(super.get(key));
	return temp;
    }

    public String[] getCarNames() {
	return carNames;
    }

    public void getTemperatureNames() {
	if (isPortable) {
	    put(camrange_a,"ec:setup.CamARange");
	    put(camsetpoint_a,"ec:setup.CamASetPnt");
	    put(camp_a,"ec:setup.CamAP");
	    put(cami_a,"ec:setup.CamAI");
	    put(camd_a,"ec:setup.CamAD");
            put(valCamrange_a,"ec:setup.VALCamARange");
            put(valCamsetpoint_a,"ec:setup.VALCamASetPnt");
            put(valCamp_a,"ec:setup.VALCamAP");
            put(valCami_a,"ec:setup.VALCamAI");
            put(valCamd_a,"ec:setup.VALCamAD");
	    put(camonoff_b,"ec:setup.CamBOnOff");
	    put(camsetpoint_b,"ec:setup.CamBSetPnt");
	    put(camp_b,"ec:setup.CamBP");
	    put(cami_b,"ec:setup.CamBI");
	    put(camd_b,"ec:setup.CamBD");
            put(valCamonoff_b,"ec:setup.VALCamBOnOff");
            put(valCamsetpoint_b,"ec:setup.VALCamBSetPnt");
            put(valCamp_b,"ec:setup.VALCamBP");
            put(valCami_b,"ec:setup.VALCamBI");
            put(valCamd_b,"ec:setup.VALCamBD");

	    put(sadCamABench,"sad:CAMABENC");
	    put(sadCamBDet,"sad:CAMBDETC");
	    put(sadCamASetPt,"sad:CAMASETP");
            put(sadCamBSetPt,"sad:CAMBSETP");

	    put(lock,"ec:setup.Lock");
	    put(amanual,"ec:setup.AManualPcnt");
            put(bmanual,"ec:setup.BManualPcnt");
            put(valAmanual,"ec:setup.VALAManualPcnt");
            put(valBmanual,"ec:setup.VALBManualPcnt");
            put(sadAmanual,"sad:CAMAHTPW");
            put(sadBmanual,"sad:CAMBHTPW");
	    put(valLock,"ec:setup.VALLock");
	    put(sadAnalog,"sad:ANALOG");
	    put(sadCamARange,"sad:CAMARANG");
	    put(sadCamAAPwr,"sad:CAMAAPWR");
	    put(sadCamBAPwr,"sad:CAMBAPWR");
	    put(sadLock,"sad:LOCK");
	    put(sadCamAPID,"sad:CAMAPID");
            put(sadCamBPID,"sad:CAMBPID");
	} else {
	    String tempPrefix = "ec:tempSetG.";
            put(camrange_a,tempPrefix+"C");
            put(camsetpoint_a,tempPrefix+"B");
            put(camp_a,tempPrefix+"D");
            put(cami_a,tempPrefix+"E");
            put(camd_a,tempPrefix+"F");
            put(valCamrange_a,tempPrefix+"VALD");
            put(valCamsetpoint_a,tempPrefix+"VALC");
            put(valCamp_a,tempPrefix+"VALE");
            put(valCami_a,tempPrefix+"VALF");
            put(valCamd_a,tempPrefix+"VALG");
            put(camonoff_b,tempPrefix+"J");
            put(camsetpoint_b,tempPrefix+"I");
            put(camp_b,tempPrefix+"K");
            put(cami_b,tempPrefix+"L");
            put(camd_b,tempPrefix+"M");
            put(valCamonoff_b,tempPrefix+"VALI");
            put(valCamsetpoint_b,tempPrefix+"VALH");
            put(valCamp_b,tempPrefix+"VALJ");
            put(valCami_b,tempPrefix+"VALK");
            put(valCamd_b,tempPrefix+"VALL");
	    //need new field for Lock 
	}
    }

    public static void constructorific() {
	//HashMap envMap = getTemperatureNames();
    }

    public void setCarNames() {
	String[] subsystems = {"cc","dc","ec"};
	if (isPortable) {
	    carNames = new String[portableCarNames.length];
	    for (int j = 0; j < carNames.length; j++) {
		carNames[j] = epicsPrefix+portableCarNames[j];
	    }
	    for (int j = 0; j < subsystems.length; j++) {
		put(subsystems[j]+"initC",subsystems[j]+":initC");
                put(subsystems[j]+"datumC",subsystems[j]+":datumC");
                put(subsystems[j]+"setupC",subsystems[j]+":setupC");
                put(subsystems[j]+"parkC",subsystems[j]+":parkC");
		put(subsystems[j]+"SetupMark",subsystems[j]+":setup.MARK");
                put(subsystems[j]+"DatumMark",subsystems[j]+":datum.MARK");
                put(subsystems[j]+"ParkMark",subsystems[j]+":park.MARK");
                put(subsystems[j]+"Setup",subsystems[j]+":setup.DIR");
                put(subsystems[j]+"Datum",subsystems[j]+":datum.DIR");
                put(subsystems[j]+"ClearDatum",subsystems[j]+":datum.DIR");
                put(subsystems[j]+"DatumAll",subsystems[j]+":datum.All");
	    }
	    //put(ccSetupMark,"cc:setup.MARK");
	} else {
	    carNames = new String[vmeCarNames.length];
            for (int j = 0; j < carNames.length; j++) {
                carNames[j] = epicsPrefix+vmeCarNames[j];
            }
            for (int j = 0; j < subsystems.length; j++) {
                put(subsystems[j]+"initC",subsystems[j]+":applyC");
                put(subsystems[j]+"datumC",subsystems[j]+":applyC");
                put(subsystems[j]+"setupC",subsystems[j]+":applyC");
                put(subsystems[j]+"parkC",subsystems[j]+":applyC");
                put(subsystems[j]+"SetupMark",subsystems[j]+":apply.MARK");
                put(subsystems[j]+"DatumMark",subsystems[j]+":datum.MARK");
                put(subsystems[j]+"ParkMark",subsystems[j]+":park.MARK");
                put(subsystems[j]+"Setup",subsystems[j]+":apply.DIR");
                put(subsystems[j]+"Datum",subsystems[j]+":datum.DIR");
                put(subsystems[j]+"ClearDatum","null");
                put(subsystems[j]+"DatumAll",subsystems[j]+":datum.DIR");
            }
	    //put(ccSetupMark,"cc:apply.MARK");
	} 
	//put(ccDatumMark,"cc:datum.MARK");
	//put(ccParkMark,"cc:park.MARK");
    }

    public void getDetectorNames() {
	if (isPortable) {
	    //high level
	    put(lvdtPosition,"sad:LVDTDISP");
	    put(lvdtVoltage,"sad:LVDTVLTS");
	    put(biasValue,"obsSetup.OverrideBias");
	    put(focalPosition,"sad:FOCUSPOS");
	    put(numReads,"obsSetup.NumReads");
	    put(exposureTime,"sad:EXPTIME");
	    //low level
	    String dcPrefix = "dc:setup.";
	    put(preamp_partial,dcPrefix+"PreAmp");
	    put(valPreamp_partial,dcPrefix+"VALPreAmp");
	    put(sadPreamp_partial,"sad:DACPRE");
	    //now programmatically put in actual values
	    //but, NB: we won't have constants defined for these (thus the 'partials' used above)
	    for (int i=0; i<32; i++) {
		put("preAmp"+i,"dc:setup.PreAmp" + (i<9?"0"+(i+1):""+(i+1)));
		put("valPreAmp"+i,"dc:setup.VALPreAmp" + (i<9?"0"+(i+1):""+(i+1)));
		put("sadPreAmp"+i,"sad:DACPRE" + (i<=9?"0"+(i):""+(i)));
	    }
	    
	    put(biasGate,dcPrefix+"BiasGATE");
	    put(biasPwr,dcPrefix+"BiasPWR");
	    put(biasVCC,dcPrefix+"BiasVCC");
	    put(biasVReset,dcPrefix+"BiasVRESET");
	    put(valBiasGate,dcPrefix+"VALBiasGATE");
	    put(valBiasPwr,dcPrefix+"VALBiasPWR");
	    put(valBiasVCC,dcPrefix+"VALBiasVCC");
	    put(valBiasVReset,dcPrefix+"VALBiasVRESET");
	    put(sadBiasGate,"sad:BIASGATE");
	    put(sadBiasPwr,"sad:BIASPWR");
	    put(sadBiasVCC,"sad:BIASVCC");
	    put(sadBiasVReset,"sad:BIASVRES");
	    
	    put(pixelBaseClock,dcPrefix+"PixelBaseClock");
            put(valPixelBaseClock,dcPrefix+"VALPixelBaseClock"); 
            put(sadPixelBaseClock,"sad:PIXBSCLK"); 

	    put(expCnt,dcPrefix+"ExpCnt");
            put(valExpCnt,dcPrefix+"VALExpCnt");
	    
            put(preResets,dcPrefix+"Presets");
            put(postResets,dcPrefix+"Postsets");
            put(valPreResets,dcPrefix+"VALPresets");
            put(valPostResets,dcPrefix+"VALPostsets");
	    put(sadNumPreResets,"sad:PRESETS");
	    put(sadNumPostResets,"sad:POSTSETS");

	    put(cdsReads,dcPrefix+"CDSReads");
	    put(valCDSReads,dcPrefix+"VALCDSReads");
            put(sadCDSReads,"sad:CDSREADS");

	    put(cycleType,dcPrefix+"CycleType");
            put(valCycleType,dcPrefix+"VALCycleType");
            put(sadCycleType,"sad:CYCLETYP");

	    put(mceAction,"dc:mcecmd.Action");
	    put(mceQuery,"dc:mcecmd.Query");
	    
	    put(setIdleMode,"");
	    //Not sure what to do here
	    put(integrationTime,"");
	    put(labDef, "dc:datum.LabDef");
	} else {
            //high level
            put(lvdtPosition,"sad:LVDTDISP");
            put(lvdtVoltage,"sad:LVDTVLTS");
            put(biasValue,"observationSetup.E");
            put(focalPosition,"sad:FOCUSPOS");
            put(numReads,"observationSetup.B");
            put(exposureTime,"sad:EXPTIME");

	    //NEED LDVARS
	    put(sadCDSReads,"sad:CDSREADS");
	    //NEED Cycle Type
            put(sadCycleType,"sad:CYCLETYP");

            //low level
            String dcPrefix = "dc:DCBiasG.";
            for (int i=0; i<32; i++) {
                put("sadPreAmp"+i,"sad:DACPRE" + (i<=9?"0"+(i):""+(i)));
            }
	    char c = 'K';
	    for (int i=0; i < 11; i++) {
		put("preAmp"+i,"dc:PreAmpG."+c);
		put("valPreAmp"+i,"dc:PreAmpG.VAL"+c);
		c++;
	    }
	    c = 'A';
            for (int i=11; i < 32; i++) {
                put("preAmp"+i,"dc:PreAmpG2."+c);
                put("valPreAmp"+i,"dc:PreAmpG2.VAL"+c);
                c++;
            }
            put(biasGate,dcPrefix+"J");
            put(biasPwr,dcPrefix+"I");
            put(biasVCC,dcPrefix+"K");
            put(biasVReset,dcPrefix+"L");
            put(valBiasGate,dcPrefix+"VALI");
            put(valBiasPwr,dcPrefix+"VALH");
            put(valBiasVCC,dcPrefix+"VALJ");
            put(valBiasVReset,dcPrefix+"VALK");
            put(sadBiasGate,"sad:BIASGATE");
            put(sadBiasPwr,"sad:BIASPWR");
            put(sadBiasVCC,"sad:BIASVCC");
            put(sadBiasVReset,"sad:BIASVRES");

            put(pixelBaseClock,"observationSetup.F");
            put(valPixelBaseClock,"observationSetup.VALF");
            put(sadPixelBaseClock,"sad:PIXBSCLK");
            put(expCnt,"observationSetup.B");
	    put(expCnt,"observationSetup.VALB");

            put(sadNumPreResets,"sad:PRESETS");
            put(sadNumPostResets,"sad:POSTSETS");
            put(mceAction,"dc:obsControl.G");
            put(mceQuery,"dc:obsControl.H");

            put(setIdleMode,"");
            put(integrationTime,"observationSetup.A");
            put(labDef, "dc:datum.O");
	}
    }

    public void getObsSetupNames() {
	if (isPortable) {
	    String obsPrefix = "obsSetup.";
	    put(expTime,obsPrefix+"ExpTime");
	    put(valExpTime,obsPrefix+"VALExpTime");
	    
	    //warning -- numReads already used in detector high-level (but same recname used)!!!
	    put(numReads,obsPrefix+"NumReads");
	    put(valNumReads,obsPrefix+"VALNumReads");
	    put(biasMode,obsPrefix+"BiasMode");
	    put(valBiasMode,obsPrefix+"VALBiasMode");
	    put(overrideBias,obsPrefix+"OverrideBias");
	    put(valOverrideBias,obsPrefix+"VALOverrideBias");
	    put(readoutMode,obsPrefix+"ReadoutMode");
	    put(valReadoutMode,obsPrefix+"VALReadoutMode");
	    
	    //warning -- pixlutfile and valpixlutfile already used in observe!!!
	    put(pixLutFile,obsPrefix+"PixLUTFile");
	    put(valPixLutFile,obsPrefix+"VALPixLUTFile");

	    put(instrumSetupC,"instrumSetupC");
	    put(obsSetupC,"obsSetupC");
            put(obsSetupMark, obsPrefix+"MARK");
	} else {
	    String obsPrefix = "observationSetup.";
	    put(expTime,obsPrefix+"A");
	    put(valExpTime,obsPrefix+"VALA");
	    put(numReads,obsPrefix+"B");
	    put(valNumReads,obsPrefix+"VALB");
	    put(biasMode,obsPrefix+"D");
	    put(valBiasMode,obsPrefix+"VALD");
	    put(overrideBias,obsPrefix+"E");
	    put(valOverrideBias,obsPrefix+"VALE");
	    put(readoutMode,obsPrefix+"C");
	    put(valReadoutMode,obsPrefix+"VALC");
	    put(instrumSetupC,"instrumentSetupC");
	    put(obsSetupC,"observationSetupC");
            put(obsSetupMark, obsPrefix+"MARK");
	}
    }

    public void getObserveNames() {
	String obsPrefix = "observe.";
	if (isPortable) {
	    put(dataLabel,obsPrefix+"DataLabel");
	    put(valDataLabel,obsPrefix+"VALDataLabel");
	    put(dataMode,obsPrefix+"DataMode");
	    put(valDataMode,obsPrefix+"VALDataMode");
	    put(ditherIndex,obsPrefix+"DitherIndex");
	    put(valDitherIndex,obsPrefix+"VALDitherIndex");
	    put(userInfo,obsPrefix+"UserInfo");
	    put(valUserInfo,obsPrefix+"VALUserInfo");
	    put(pixLutFile,obsPrefix+"PixLUTFile");
	    put(valPixLutFile,obsPrefix+"VALPixLUTFile");
            put(stopC,"stopC");
            put(abortC,"abortC");
	} else {
	    String acqPrefix = "dc:acqControl.";
	    put(dataLabel,obsPrefix+"A");
	    put(valDataLabel,obsPrefix+"VALA");	    
	    put(dataMode,acqPrefix+"C");
	    put(valDataMode,acqPrefix+"VALC");
            put(ditherIndex,acqPrefix+"E");
            put(valDitherIndex,acqPrefix+"VALE");
            put(userInfo,acqPrefix+"G");
            put(valUserInfo,acqPrefix+"VALG");
            put(pixLutFile,acqPrefix+"H");
            put(valPixLutFile,acqPrefix+"VALH");
	}
	put(pause,"pause.DIR");
	put(pauseC,"pauseC");
	put(continu,"continue.DIR");
	put(continueC,"continueC");
	put(stop,"stop.DIR");
	put(abort,"abort.DIR");
	put(observe,"observe.DIR");
	put(observeC,"observeC");
	put(apply,"apply.DIR");
        put(observeMark, obsPrefix+"MARK");
    }

    public void getInstrumentSetupNames() {
	put(reboot,"reboot.DIR");
	put(init,"init.DIR");
	put(datum,"datum.DIR");
	put(test,"test.DIR");
	put(rebootC,"rebootC");
	if (isPortable) {
            put(initC,"initC");
            put(datumC,"datumC");
            put(testC,"testC");
	    String isPrefix = "instrumSetup.";
	    put(beamMode,isPrefix+"BeamMode");
	    put(valBeamMode,isPrefix+"VALBeamMode");
	    put(wheelBiasMode,isPrefix+"WheelBiasMode");
	    put(valWheelBiasMode,isPrefix+"VALWheelBiasMode");
	    put(windowCover,isPrefix+"WindowCover");
	    put(valWindowCover,isPrefix+"VALWindowCover");
	    put(mosSlit,isPrefix+"MOSSlit");
	    put(valMosSlit,isPrefix+"VALMOSSlit");
	    put(decker,isPrefix+"Decker");
	    put(valDecker,isPrefix+"VALDecker");
	    put(overrideDecker,isPrefix+"OverrideDecker");
	    put(valOverrideDecker,isPrefix+"VALOverrideDecker");
	    put(detPosFocus,isPrefix+"DetPosFocus");
	    put(valDetPosFocus,isPrefix+"VALDetPosFocus");
	    put(overrideDetPos,isPrefix+"OverrideDetPos");
	    put(valOverrideDetPos,isPrefix+"VALOverrideDetPos");
	    put(filter,isPrefix+"Filter");
	    put(valFilter,isPrefix+"VALFilter");
	    put(grism,isPrefix+"Grism");
	    put(valGrism,isPrefix+"VALGrism");
	    put(overrideGrism,isPrefix+"OverrideGrism");
	    put(valOverrideGrism,isPrefix+"VALOverrideGrism");
	    put(lyot,isPrefix+"Lyot");
	    put(valLyot,isPrefix+"VALLyot");
	    put(overrideLyot,isPrefix+"OverrideLyot");
	    put(valOverrideLyot,isPrefix+"VALOverrideLyot");
            put(camSetPointA,isPrefix+"CamSetPointA");
            put(valCamSetPointA,isPrefix+"VALCamSetPointA");
            put(camSetPointB,isPrefix+"CamSetPointB");
            put(valCamSetPointB,isPrefix+"VALCamSetPointB");
	    put(overrideCamSetPointA,isPrefix+"OverrideCamSetPointA");
	    put(valOverrideCamSetPointA,isPrefix+"VALOverrideCamSetPointA");
	    put(overrideCamSetPointB,isPrefix+"OverrideCamSetPointB");
	    put(valOverrideCamSetPointB,isPrefix+"VALOverrideCamSetPointB");
            put(mosSetPoint,isPrefix+"MOSSetPoint");
            put(valMosSetPoint,isPrefix+"VALMOSSetPoint");
            put(overrideMosSetPoint,isPrefix+"OverrideMOSSetPoint");
            put(valOverrideMosSetPoint,isPrefix+"VALOverrideMOSSetPoint");
            put(instrumSetupMark, isPrefix+"MARK");
	} else {
	    String isPrefix = "instrumentSetup.";
	    put(beamMode,isPrefix+"A");
	    put(valBeamMode,isPrefix+"VALA");	   
	    put(wheelBiasMode,isPrefix+"B");
	    put(valWheelBiasMode,isPrefix+"VALB");
	    put(windowCover,isPrefix+"C");
	    put(valWindowCover,isPrefix+"VALC");
	    put(mosSlit,isPrefix+"E");
	    put(valMosSlit,isPrefix+"VALE");
	    put(decker,isPrefix+"D");
	    put(valDecker,isPrefix+"VALD");
	    put(overrideDecker,isPrefix+"J");
	    put(valOverrideDecker,isPrefix+"VALJ");
	    put(detPosFocus,isPrefix+"I");
	    put(valDetPosFocus,isPrefix+"VALI");
	    put(overrideDetPos,isPrefix+"M");
	    put(valOverrideDetPos,isPrefix+"VALM");
	    put(filter,isPrefix+"F");
	    put(valFilter,isPrefix+"VALF");
	    put(grism,isPrefix+"H");
	    put(valGrism,isPrefix+"VALH");
	    put(overrideGrism,isPrefix+"L");
	    put(valOverrideGrism,isPrefix+"VALL");
	    put(lyot,isPrefix+"G");
	    put(valLyot,isPrefix+"VALG");
	    put(overrideLyot,isPrefix+"K");
	    put(valOverrideLyot,isPrefix+"VALK");
            put(instrumSetupMark, isPrefix+"MARK");
	}
    }

    public void getLvdtNames() {
        if (isPortable) {
            String focusPrefix = "eng:focus.";
            put(lvdtPwrOnOff,focusPrefix+"LVDTPwrOnOff");
            put(lvdtExcVoltsHiLow,focusPrefix+"LVDTExcVoltsHiLow");
            put(lvdtExcFreqHiLow,focusPrefix+"LVDTExcFreqHiLow");
            put(limitVel,focusPrefix+"LimitVel");
            put(focusStep,focusPrefix+"Step");
            put(stepVel,focusPrefix+"StepVel");
	} else {
	    String focusPrefix = "lvdt:lvdtControl.";
            put(lvdtPwrOnOff,focusPrefix+"B");
            put(lvdtExcVoltsHiLow,focusPrefix+"C");
            put(lvdtExcFreqHiLow,focusPrefix+"D");
            put(limitVel,focusPrefix+"E");
            put(focusStep,focusPrefix+"G");
            put(stepVel,focusPrefix+"H");
	}
    }

    public void getEngObserveNames() {
        if (isPortable) {
            String engObsPrefix = "eng:observe.";
            put(dhsInit,engObsPrefix+"DHSInit");
	    put(dhsLabel,engObsPrefix+"DHSLabel");
	    put(ds9,engObsPrefix+"DS9");
	    put(edtInit,engObsPrefix+"EDTInit");
	    put(engObsExpTime,engObsPrefix+"ExpTime");
	    put(frameMode,engObsPrefix+"FrameMode");
	    put(engObsIndex,engObsPrefix+"Index");
	    put(fitsFile,engObsPrefix+"FITSFile");
	    put(pngFile,engObsPrefix+"PNGFile");
	    put(engObsLut,engObsPrefix+"LUT");
	    put(simMode,engObsPrefix+"SimMode");
	    put(sadEDTTotal,"sad:EDTTOTAL");
	    put(sadEDTFrame,"sad:EDTFRAME");
	    put(sadEDTActn,"sad:EDTACTN");
        } else {
            String engObsPrefix = "eng:engControl.";
            put(dhsInit,engObsPrefix+"D");
            put(dhsLabel,engObsPrefix+"E");
            put(ds9,engObsPrefix+"G");
            put(edtInit,engObsPrefix+"H");
            put(engObsExpTime,engObsPrefix+"B");
            put(frameMode,engObsPrefix+"C");
            put(engObsIndex,engObsPrefix+"I");
            put(fitsFile,engObsPrefix+"J");
            put(pngFile,engObsPrefix+"K");
            put(engObsLut,engObsPrefix+"L");
            put(simMode,engObsPrefix+"M");
            put(sadEDTTotal,"sad:EDTTOTAL");
            put(sadEDTFrame,"sad:EDTFRAME");
            put(sadEDTActn,"sad:EDTACTN");
        }
    }

    public void getMosBarCdNames() {
        if (isPortable) {
            put(MOSBarCd,"cc:setup.MOSBarCd");
	    put(MOSBarCdDir,"apply.DIR");
	    put(MOSBarCdDirection,"cc:setup.MOSBarCdDirection");
	    put(datumMOSBarCd,"cc:datum.MOS");
	    put(datumMOSBarCdMark,""+EPICS.PRESET);
	    put(MOSBarCdSteps,"cc:setup:MOSBarCdStep");
	    //??? ask David
	    put(MOSBarCdVAL,"cc:applyC");
	} else {
	    put(MOSBarCd,"ec:readBc.A");
	    put(MOSBarCdDir,"ec:readBc.DIR");
	    put(MOSBarCdDirection,"null");
            put(datumMOSBarCd,"cc:MOS:datum.DIR");
            put(datumMOSBarCdMark,""+EPICS.MARK);
	    put(MOSBarCdSteps,"cc:MOS:steps.A");
	    put(MOSBarCdVAL,"ec:readBcC.VAL");
	}
	put("sadMOSCIRC1","sad:MOSCIRC1");
        put("sadMOSCIRC2","sad:MOSCIRC2");
        put("sadMOSPLT01","sad:MOSPLT01");
        put("sadMOSPLT02","sad:MOSPLT02");
        put("sadMOSPLT03","sad:MOSPLT03");
        put("sadMOSPLT04","sad:MOSPLT04");
        put("sadMOSPLT05","sad:MOSPLT05");
        put("sadMOSPLT06","sad:MOSPLT06");
        put("sadMOSPLT07","sad:MOSPLT07");
        put("sadMOSPLT08","sad:MOSPLT08");
        put("sadMOSPLT09","sad:MOSPLT09");
        put("sadMOSPLT10","sad:MOSPLT10");
        put("sadMOSPLT01","sad:MOSPLT01");
        put("sadMOSPLT11","sad:MOSPLT11");
	put(sadMosSteps,"sad:MOSSteps");
    }

    public void getIndexorNames(String motName) {
	if (isPortable) {
	    put(abortAll, "cc:abort.All");
	    put(setupOrigin, "cc:setup.Origin");
	    String idxPrefix = "cc:setup."+motName;
	    put(motName+"Backlash",idxPrefix+"Backlash");
	    put(motName+"NamedPos",idxPrefix);
	    put(motName+"Step",idxPrefix+"Step");
	    put(motName+"InitVel",idxPrefix+"InitVel");
	    put(motName+"SlewVel",idxPrefix+"SlewVel");
	    put(motName+"Acc",idxPrefix+"Acc");
	    put(motName+"Dec",idxPrefix+"Dec");
	    put(motName+"RunCur",idxPrefix+"RunCur");
	    put(motName+"HoldCur",idxPrefix+"HoldCur");
	    put(motName+"DatumVel","cc:datum."+motName+"Vel");
	    put(motName+"DatumDir","cc:datum."+motName+"Direction");
	    idxPrefix = "cc:setup.VAL"+motName;
	    put(motName+"VALBacklash",idxPrefix+"Backlash");
	    put(motName+"VALNamedPos",idxPrefix);
	    put(motName+"VALStep",idxPrefix+"Step");
	    put(motName+"VALInitVel",idxPrefix+"InitVel");
	    put(motName+"VALSlewVel",idxPrefix+"SlewVel");
	    put(motName+"VALAcc",idxPrefix+"Acc");
	    put(motName+"VALDec",idxPrefix+"Dec");
	    put(motName+"VALRunCur",idxPrefix+"RunCur");
	    put(motName+"VALHoldCur",idxPrefix+"HoldCur");
	    put(motName+"VALDatumVel","cc:datum.VAL"+motName+"Vel");
	    put(motName+"VALDatumDir","cc:datum.VAL"+motName+"Direction");

	    put(motName+"Abort","cc:abort."+motName);
	    put(motName+"Status","cc:test."+motName);
	    put(motName+"Origin","cc:datum.Origin");
	    put(motName+"Datum","cc:datum."+motName);
            String idxDatumPrefix = "cc:datum."+motName;
	    put(motName+"InitVelDatum", idxDatumPrefix+"InitVel");
	    put(motName+"AccDatum", idxDatumPrefix+"Acc");
            put(motName+"DecDatum", idxDatumPrefix+"Dec");
            put(motName+"RunCurDatum", idxDatumPrefix+"RunCur");
	    put(motName+"HoldCurDatum", idxDatumPrefix+"HoldCur");
	    put(motName+"BacklashDatum", idxDatumPrefix+"Backlash");
            put(motName+"InitSlowDatum", idxDatumPrefix+"InitSlow");
            put(motName+"SlowDatum", idxDatumPrefix+"Slow");
            idxDatumPrefix = "cc:datum.VAL"+motName;
            put(motName+"VALInitVelDatum", idxDatumPrefix+"InitVel");
            put(motName+"VALAccDatum", idxDatumPrefix+"Acc");
            put(motName+"VALDecDatum", idxDatumPrefix+"Dec");
            put(motName+"VALRunCurDatum", idxDatumPrefix+"RunCur");
	    put(motName+"VALHoldCurDatum", idxDatumPrefix+"HoldCur");
	    put(motName+"VALBacklashDatum", idxDatumPrefix+"Backlash");
            put(motName+"VALInitSlowDatum", idxDatumPrefix+"InitSlow");
            put(motName+"VALSlowDatum", idxDatumPrefix+"Slow");

            put(motName+"MotorC","null");
            put(motName+"MotorG","null");
            put(motName+"DatumMark","null");
	    //put(ccSetup,"cc:setup.DIR");
	} else {
	    String idxPrefix = "cc:"+motName+":";
	    put(motName+"Backlash",idxPrefix+"motorG.L");
	    put(motName+"NamedPos",idxPrefix+"namedPos.A");
	    put(motName+"Step",idxPrefix+"steps.A");
	    put(motName+"InitVel",idxPrefix+"motorG.N");
	    put(motName+"SlewVel",idxPrefix+"motorG.O");
	    put(motName+"Acc",idxPrefix+"motorG.P");
	    put(motName+"Dec",idxPrefix+"motorG.Q");
	    put(motName+"RunCur",idxPrefix+"motorG.R");
	    put(motName+"DatumVel",idxPrefix+"motorG.S");
	    put(motName+"DatumDir",idxPrefix+"motorG.T");

	    put(motName+"VALBacklash",idxPrefix+"motorG.VALL");
	    put(motName+"VALNamedPos",idxPrefix+"namedPos.VALA");
	    put(motName+"VALStep",idxPrefix+"steps.VALA");
	    put(motName+"VALInitVel",idxPrefix+"motorG.VALD");
	    put(motName+"VALSlewVel",idxPrefix+"motorG.VALE");
//InitSlow and Slow need to be added
	    put(motName+"VALAcc",idxPrefix+"motorG.VALF");
	    put(motName+"VALDec",idxPrefix+"motorG.VALG");
	    put(motName+"VALRunCur",idxPrefix+"motorG.VALH");
	    put(motName+"VALDatumVel",idxPrefix+"motorG.VALI");
	    put(motName+"VALDatumDir",idxPrefix+"motorG.VALJ");

	    put(motName+"Abort",idxPrefix+"abort.A");
	    put(motName+"Status","cc:test."+motName);
	    put(motName+"Origin",idxPrefix+"origin.A");
	    put(motName+"Datum",idxPrefix+"datum.A");

	    put(motName+"MotorC",idxPrefix+"motorC");
	    put(motName+"MotorG",idxPrefix+"motorG");
	    put(motName+"DatumMark",idxPrefix+"datum.MARK");
	    //put(ccSetup,"cc:apply.DIR");
	}
	//put(ccDatum, "cc:datum.DIR");
	//its SAD, really...
	String sadMotorName;
	if (isPortable) sadMotorName = motName.toUpperCase(); else sadMotorName = motName;
	if (sadMotorName.length() > 6) { 
	    //mosbarcd?
	    if (sadMotorName.startsWith("MOS")) 
		sadMotorName = "MOS";
	    else if (isPortable)// filter wheel
		sadMotorName = sadMotorName.substring(0,4) + sadMotorName.charAt(sadMotorName.length()-1);
	}
	put(motName+"SADInitVel","sad:"+sadMotorName+"IV");
	put(motName+"SADSlewVel","sad:"+sadMotorName+"SV");
	put(motName+"SADAcc","sad:"+sadMotorName+"AC");
	put(motName+"SADDec","sad:"+sadMotorName+"DC");
	put(motName+"SADRunc","sad:"+sadMotorName+"RC");
    }
    
    /*
	    

	loDatumButton.setText("Datum");
	hiDatumButton.setText("Datum");
	loParkButton.setText("Park");
	hiParkButton.setText("Park");

	new EPICSCarCallback(EPICS.applyPrefix+"applyC",new EPICSCarListener(){
		public void carTransition(int transition) {
		    if (transition == EPICS.IDLE) {
			CAR_FLAG &= ~APPLYC;
			if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
			    motionStatus(false);
		    } else if(transition == EPICS.BUSY) {
			CAR_FLAG |= APPLYC;
			motionStatus(true);
		    } else if(transition == EPICS.ERROR){
			CAR_FLAG &= ~APPLYC;
			if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
			    motionStatus(false);
		    } else if (transition == Integer.MIN_VALUE) {
			
		    } else{
			System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
		    }
		}
	    });
	if (EPICS.PORTABLE) {
	    new EPICSCarCallback(EPICS.applyPrefix+"cc:initC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~INITC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= INITC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~INITC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			
			} else if (transition == Integer.MIN_VALUE) {
			    // do nothing
			} else {
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	    new EPICSCarCallback(EPICS.applyPrefix+"cc:setupC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~SETUPC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= SETUPC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~SETUPC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	    new EPICSCarCallback(EPICS.applyPrefix+"cc:datumC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~DATUMC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= DATUMC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~DATUMC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if (transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	} else {
	    new EPICSCarCallback(EPICS.applyPrefix+"cc:"+epicsName+":motorC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~MOTORC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= MOTORC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~MOTORC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});	    
	}
	//fix for inconsistant epics rec names
	sadRec = EPICS.prefix+"sad:"+epicsName+"Steps.VAL";
	if (sadRec.toLowerCase().indexOf("barcd") != -1)
	    sadRec = EPICS.prefix+"sad:"+"MOS"+"Steps.VAL";	
	new EPICSCallback(sadRec, new EPICSCallbackListener(){
		public void valueChanged(String val) {
		    try {
			int step;
			try {
			    step = Integer.parseInt(val.trim());
			} catch (NumberFormatException nfe) {
			    step = (int) Double.parseDouble(val.trim());
			}
			highAnimation.setCurrentStep(step);
			lowAnimation.setCurrentStep(step);
			statusAnimation.setCurrentStep(step);
			String s = getPositionString(step);
			hiStatusLabel.setText(s);
			loStatusLabel.setText(s);
			statusStatusLabel.setText(s);
			hiStatusLabel.setToolTipText(sadRec+" = "+val);
			loStatusLabel.setToolTipText(sadRec+" = "+val);
			statusStatusLabel.setToolTipText(sadRec+" = "+val);
		    } catch (Exception e) {
			System.err.println("FJECMotor.animationMonitorChanged> "+e.toString());
		    }
		}
	    });
	if (EPICS.PORTABLE) {
	    new EPICSCallback(EPICS.prefix+"cc:setup.MARK", new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("red",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.setupRecMonitorChanged> "+e.toString());
			}		    
		    }
		});
	    new EPICSCallback(EPICS.prefix+"cc:datum.MARK", new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("green",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.datumRecMonitorChanged> "+e.toString());
			}		    
		    }
		});
	    
	    new EPICSCallback(EPICS.prefix + "cc:park.MARK", new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("blue",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.parkRecMonitorChanged> "+e.toString());
			}		    
		    }
		    });
	} else {
	    new EPICSCallback(EPICS.prefix + "cc:"+epicsName+":motorG.MARK", new EPICSCallbackListener() {
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("blue",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor> "+e.toString());
			}
		    }
		});
	    new EPICSCallback(EPICS.prefix + "cc:"+epicsName+":datum.MARK", new EPICSCallbackListener() {
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("green",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor> "+e.toString());
			}
		    }
		});
	}
	*/
public static void main(String[] args) {
   EPICSRecs a = new EPICSRecs();
}
}
