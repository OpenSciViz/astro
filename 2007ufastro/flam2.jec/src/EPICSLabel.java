
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Company:      University of Florida
//Description:

package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.*;
import java.util.*;
import javaUFLib.*;

//===============================================================================
/**
 * Handles Labels related to EPICS
 */
public class EPICSLabel extends JLabel implements UFCAToolkit.MonitorListener {
    public static final String rcsID = "$Name:  $ $Id: EPICSLabel.java,v 0.31 2006/10/25 22:27:55 warner Exp $";
    static final long serialVersionUID = 0; 
    
    String hashKey = null;
    String monitorRec;
    String prefix;
    boolean showRec;
    JButton button = null;
    long timeStamp = 0;
    String outVal = "";
    Vector shadowVec;
    Vector shadowHash = new Vector();
    HashMap shadowMap,shadowValMap;
    int parse=0;
    float unitFactor=1;
    HashMap displays, colors;
    boolean isPreamp = false;

//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   */
  public EPICSLabel(String rec) {
    try  {
      if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
          hashKey = rec;
          rec = EPICS.recs.get(hashKey);
      }
      jbInit(rec,"");
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel
//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  public EPICSLabel(String rec, String prefix) {
    try  {
      if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
          hashKey = rec;
          rec = EPICS.recs.get(hashKey);
      }
      jbInit(rec,prefix);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   * Constructor used to associate this EPICSLabel with
   * a JButton
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  public EPICSLabel(String rec, String prefix, JButton button) {
    try  {
      if (rec.indexOf(".") == -1 && rec.indexOf(":") == -1) {
          hashKey = rec;
          rec = EPICS.recs.get(hashKey);
      }
      jbInit(rec,prefix,button);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  private void jbInit(String rec, String prefix, JButton button) throws Exception {
    this.monitorRec = rec;
    this.prefix = prefix;
    this.button = button;
    this.timeStamp = 0;
    shadowVec = new Vector();
    shadowMap = new HashMap();
    shadowValMap = new HashMap();
    displays = new HashMap();
    colors = new HashMap();
    if (monitorRec.trim().equals("")) return;
 
    if (!EPICS.addMonitor(monitorRec,this)) 
	setBackground(Color.red);
    else
	setForeground(new JTextField().getForeground());
    /*
   // setup monitor(s)
    int mask = 7;
    
    try {
	gov.aps.jca.Channel theChannel = EPICS.theContext.createChannel(rec);
	
	EPICS.theContext.pendIO(EPICS.TIMEOUT);
	outMon = theChannel.addMonitor(mask,this);
	//EPICS.theContext.pendIO(EPICS.TIMEOUT);
	EPICS.theContext.flushIO();
	//EPICS.monitorCheat(outMon,this).start();
	//theChannel.destroy();
	//outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
	this.setForeground(new JTextField().getForeground());
    }
    catch (Exception ex) {
	this.setBackground(Color.red);
	}
    */
    this.setHorizontalAlignment(JLabel.LEFT);
    setBorder(new BevelBorder(BevelBorder.LOWERED));
  } //end of jbInit

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
        if (hashKey != null) {
            monitorRec = EPICS.recs.get(hashKey);
        } else {
            monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
	    return;
        }
	EPICS.addMonitor(monitorRec,this);
	Object [] shadows = shadowVec.toArray();	
        Object [] shadowHashArray = shadowHash.toArray();
	shadowVec.clear();
        shadowHash.clear();
	shadowValMap.clear();
	for (int i=0; i<shadows.length; i++) {
            String shady;
            if (shadowHashArray[i] != null) {
                shady = EPICS.recs.get((String)shadowHashArray[i]);
                shadowHash.add((String)shadowHashArray[i]);
            } else {
                shady = (String)shadows[i];
                shady = dbname + shady.substring(shady.indexOf(":")+1);
            }
            EPICS.removeMonitor(shady,(UFCAToolkit.MonitorListener)shadowMap.get(shady));
	    shadowVec.add(shady);
	    shadowMap.put(shady,shadowMap.remove(shadows[i]));
	    if (shady != null) {
		EPICS.addMonitor(shady,(UFCAToolkit.MonitorListener)shadowMap.get(shady));
	    }
	}
	setToolTipText();
    }

    private void setToolTipText() {
	String tip = "<html>"+monitorRec + " = " + outVal;
	Object [] shadows = shadowVec.toArray();
	for (int i=0; i<shadows.length; i++) {
	    tip += "<br>"+(String)shadows[i]+" = "+(String)shadowValMap.get((String)shadows[i]);
	}
	if (isPreamp && !outVal.equals("null") && !outVal.equals("")) {
	    tip += " volts<br>"+(int)(Float.parseFloat(outVal)*255/9.4+0.0005)+" bits";
	}
	tip += "</html>";
	//this.setToolTipText( monitorRec + " = " + outVal );
	this.setToolTipText(tip);
    }


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  private void jbInit(String Rec, String prefix) throws Exception {
      jbInit(Rec, prefix, null);
  } //end of jbInit


    public long getTimeStamp() { return timeStamp; }

    public void addShadowRecord(String shadowRecName) {
        if (shadowRecName.indexOf(".") == -1 && shadowRecName.indexOf(":") == -1) {
            shadowHash.add(shadowRecName);
            shadowRecName = EPICS.recs.get(shadowRecName);
        } else {
            shadowHash.add(null);
        }
	shadowVec.add(shadowRecName);
	final String localName = shadowRecName;
	shadowMap.put(shadowRecName,new UFCAToolkit.MonitorListener(){
		public void monitorChanged(String val) {
		    shadowValMap.put(EPICS.prefix+localName.substring(localName.indexOf(":")+1),val);
		    setToolTipText();
		}
		public void reconnect(String newPrefix) {
		    
		}
	    });
	EPICS.addMonitor(shadowRecName,(UFCAToolkit.MonitorListener)shadowMap.get(shadowRecName));
    }

    public void removeShadowRecord(String shadowRecName) {
	shadowVec.remove(shadowRecName);
	EPICS.removeMonitor(shadowRecName,(UFCAToolkit.MonitorListener)shadowMap.get(shadowRecName));
	shadowMap.remove(shadowRecName);
    }


    public void setTokenNum(int tokenNum) {
	parse = tokenNum;
    }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(String value) {
    try {
      

      //if (event.pv().name().equals(this.monitorRec)) 
      {
	  outVal = value.trim();
	  if (parse != 0) {
	    StringTokenizer st = new StringTokenizer(outVal,",");
	    String stemp = null;
	    int tokens = 0;
	    while (tokens < parse && st.hasMoreElements()) {
		stemp = st.nextToken();
		tokens++;
	    }
	    if (stemp != null) outVal = stemp;
	  }

	  if (unitFactor != 1) {
	    try {
		double tempd = Double.parseDouble(outVal);
		outVal = ""+UFLabel.truncFormat((unitFactor*tempd),5,5);
	    } catch(Exception e) {}
	  }

          if (colors.containsKey(outVal)) this.setForeground((Color)colors.get(outVal));
	  if (displays.containsKey(outVal)) outVal = (String)displays.get(outVal);

	  this.setText(prefix + "  " + outVal + "   ");
	  if (button != null) {
	      String dbval = value.trim();
	      if( prefix.indexOf("CAR") >= 0 ) {
		  if( dbval.equals("BUSY") )
		      button.setEnabled(false);
		  else
		      button.setEnabled(true);
	      }
	      else if (checkforBadSocket(dbval)) {
		  button.setBackground(Color.red);
		  button.setForeground(Color.white);
		  button.setText("Reconnect");
	      }
	  }
          if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
          if( this.getText().indexOf("ERR") >= 0 ) {
	      Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	  }
      } 
      timeStamp = System.currentTimeMillis();
      setToolTipText();
    }
    catch (Exception ex) {
       fjecError.show("Error in monitor changed for EPICSLabel: " + monitorRec + " " + ex.toString());
    } 
       
  } //end of monitorChanged
    
//-------------------------------------------------------------------------------

  boolean checkforBadSocket( String EPICSmessage ) {	
    if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
      return true;
    else
      return false;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      try {
        EPICS.removeMonitor(monitorRec,this);
      }
      catch (Exception ee) {
        fjecError.show(e.toString());
      }
      ///dispose();
    }
    ///super.processWindowEvent(e);
  } //end of processWindowEvent

  public void updateFactor(float newFactor) {
    try {
	double tempd = Double.parseDouble(this.getText());
	setText(prefix+"  "+UFLabel.truncFormat((tempd*newFactor/unitFactor),5,5)+"   ");
    } catch(Exception e) {}
    this.unitFactor = newFactor;
  }

  public void addDisplayValue(String input, String display) {
    displays.put(input, display);
  }

  public void addDisplayValue(String input, String display, Color color) {
    displays.put(input, display);
    colors.put(input, color);
  }

  public void setPreamp(boolean b) {
    this.isPreamp = b;
  }

} //end of class EPICSTextField

