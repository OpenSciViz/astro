package uffjec;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import javax.swing.*;
import java.util.*;
import javaUFLib.*;

public class EPICSApplyButton extends UFColorButton implements EPICSCarListener{

    static final long serialVersionUID = 0;
    public static final String consistantLayout = "0.01,0.90;0.18,0.10";

    public static int rememberChoice = 0;
    public static boolean checkIS = true;

    Vector blueVec,fieldVec;
    boolean doApply;

    String recToMark, hashKey;

    UFClock clock = null;
    int defTime = -1;

    public EPICSApplyButton() {
	this("Apply","");
    }
    public EPICSApplyButton(String title, String markRec) {
	this(title,markRec,EPICS.MARK+"",false, COLOR_SCHEME_GREEN);
    }
    public EPICSApplyButton(String title, String markRec, String markVal) {
	this(title,markRec,markVal,false, COLOR_SCHEME_GREEN);
    }
    
    public EPICSApplyButton(String title, final String markRec, final String markVal, final boolean abortButton, int COLOR_SCHEME) {
	super(title, COLOR_SCHEME);
	//need to strip off epics prefix in case someone tried to connect to a different db
	hashKey = "";
	recToMark = markRec;
        if (recToMark.indexOf(":") == -1 && recToMark.indexOf(".") == -1) {
           hashKey = recToMark; 
           recToMark = EPICS.recs.get(recToMark);
        }
	if (recToMark.startsWith(EPICS.prefix))
	    recToMark = recToMark.substring(recToMark.indexOf(EPICS.prefix)+EPICS.prefix.length());
	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
                    if (EPICS.prefix.equals("null:")) {
                        //JOptionPane.showMessageDialog(null, "Warning: You are not connected to any database!", "Warning", JOptionPane.WARNING_MESSAGE);
			fjecFrame.connectToEPICS();
                        return;
                    }
		    if (!abortButton) {
			doApply = true;
			checkForBlueComponents();
			if (!doApply) return;
			checkForInstrumentSetup();
			if (!doApply) return;
			if (clock != null) {
			    int clockTimer = getClockTimer();
			    if (clockTimer > 0) {
				clock.setClock(clockTimer);
			    	clock.start();
			    }
			}
		    }
		    if (markRec != null && !markRec.trim().equals("")) {
			if (!hashKey.equals("")) {
			    EPICS.put(EPICS.recs.get(hashKey),markVal);
			} else {
			    EPICS.put(EPICS.prefix + recToMark,markVal);
			}
		    }
		    EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
		}
	    });   
	if (!abortButton)
	    new EPICSCarCallback(EPICS.prefix + "applyC",this);	
    }

    public void addClock(UFClock clock, int defTime) {
	this.clock = clock;
	this.defTime = defTime;
    }
    
    public void carTransition(int value) {
	if (value == EPICS.BUSY)
	    setEnabled(false);
	else if (value == EPICS.IDLE) {
            setEnabled(true);
	    if (clock != null) clock.resetClock();
	}
	else if (value == EPICS.ERROR){
            setEnabled(true);
	    if (clock != null) clock.resetClock();
	}
    }

    private void instrumSetupAction(int choice) {
	if (choice == 1) {
	    doApply = true;
	    return;
	} else if (choice == 2) {
	    JPanelIS.saveInstrumSetup();
	    doApply = true;
	    return;
	} else if (choice == 3) {
	    JPanelIS.saveInstrumSetup();
	    doApply = false;
	    return;
	} else {
	    System.err.println("EPICSApplyButton.instrumSetupAction> Invalid choice: "+choice);
	}
    }

    public int getClockTimer() {
	String val;
	int time;
        val = EPICS.get(EPICS.recs.get(EPICSRecs.observe));
        if (val != null && val.equals("1")) {
            String temp = EPICS.get(EPICS.recs.get(EPICSRecs.expTime));
            if (temp != null) {
                time = Integer.parseInt(temp);
                return time;
            } else return defTime;
        }
	val = EPICS.get(EPICS.recs.get(EPICSRecs.observeMark));
	if (val != null && val.equals("1")) {
	    return defTime;
	}
        val = EPICS.get(EPICS.recs.get(EPICSRecs.obsSetupMark));
        if (val != null && val.equals("1")) {
            return defTime;
        }
	val = EPICS.get(EPICS.recs.get(EPICSRecs.instrumSetupMark));
	if (val != null && val.equals("1")) {
	   return defTime;
	}
	return defTime;
    }

    public void checkForInstrumentSetup() {
	if (!checkIS) return;
	String val;
	val = EPICS.get(EPICS.recs.get(EPICSRecs.instrumSetupMark));
/*
	if (EPICS.PORTABLE) {
	    val = EPICS.get(EPICS.prefix+"instrumSetup.MARK");
	} else {
	    val = EPICS.get(EPICS.prefix+"instrumentSetup.MARK");
	}
*/
	if (val == null || val.trim().equals("")) return;
	val = val.trim().toLowerCase();
	if (val.equals("1")) {
	    if (rememberChoice != 0) {
		instrumSetupAction(rememberChoice);
		return;
	    }
	    final JDialog jd = new JDialog();
	    JPanel dialogPanel = new JPanel(new RatioLayout());
	    final JRadioButton justApply = new JRadioButton("Just Apply");
	    final JRadioButton addToHistory = new JRadioButton("Apply and add to history",true);
	    final JRadioButton justAddToHistory = new JRadioButton("Just add to history");
	    final JCheckBox noAnnoyBox = new JCheckBox("Remember response",false);
	    if (fjec.easterEggs) 
		noAnnoyBox.setText("<html>For the love of god, stop annoying me with your @#$* pop-up windows!<br>Automatically use my reply to this dialog in the future<br>(requires a restart of fjec to reset)</html>");


	    ButtonGroup bg = new ButtonGroup();
	    bg.add(justApply); bg.add(addToHistory); bg.add(justAddToHistory);
	    	    
	    JButton okButton = new JButton("Ok");
	    JButton cancelButton = new JButton("Cancel Apply");
	    
	    okButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			if (justApply.isSelected()) {
			    instrumSetupAction(1);
			    if (noAnnoyBox.isSelected())
				rememberChoice = 1;
			} else if (addToHistory.isSelected()) {
			    instrumSetupAction(2);
			    if (noAnnoyBox.isSelected())
				rememberChoice = 2;
			} else if (justAddToHistory.isSelected()) {
			    instrumSetupAction(3);
			    if (noAnnoyBox.isSelected())
				rememberChoice = 3;
			} else {
			    System.err.println("EPICSApplyButton.checkForInstrumentSetup> CONGRATULATIONS!!!!! You have just discovered a bug in the java VM.");
			}
			jd.setVisible(false);
		    }
		});
	    cancelButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			doApply = false;
			jd.setVisible(false);
		    }
		});
	    

	    dialogPanel.add("0.01,0.01;0.99,0.10",new JLabel("You're about to process the Instrument Setup Record."));
	    dialogPanel.add("0.01,0.11;0.99,0.10",new JLabel(" Would you like to:"));
	    dialogPanel.add("0.01,0.21;0.99,0.10",justApply);
	    dialogPanel.add("0.01,0.31;0.99,0.10",addToHistory);
	    dialogPanel.add("0.01,0.41;0.99,0.10",justAddToHistory);
	    dialogPanel.add("0.01,0.51;0.99,0.39",noAnnoyBox);
	    dialogPanel.add("0.13,0.90;0.30,0.10",okButton);
	    dialogPanel.add("0.63,0.90;0.30,0.10,",cancelButton);
	    jd.setModal(true);
	    jd.setAlwaysOnTop(true);
	    jd.setContentPane(dialogPanel);
	    jd.setSize(400,300);
	    jd.setVisible(true);
	}
    }


    public void checkForBlueComponents(){
	doApply = true;
	blueVec = new Vector();
	fieldVec = new Vector();
	Frame [] frms = Frame.getFrames();
	for (int i=0; i<frms.length; i++)
	    recurseEPICSComponents(frms[i]);
	if (blueVec.size() == 0) return;
	JPanel dialogPanel = new JPanel(new RatioLayout());
	final JList dialogList = new JList(blueVec);
	JScrollPane jsp = new JScrollPane(dialogList);
	final JDialog jd = new JDialog();
	
	JButton putAndApplyButton = new JButton("Put selected values and Apply");
	putAndApplyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    int [] indices = dialogList.getSelectedIndices();
		    for (int i=0; i<indices.length; i++)
			((EPICSTextField)fieldVec.elementAt(i)).forcePut();
		    doApply = true;
		    jd.setVisible(false);
		}
	    });
	JButton putButton = new JButton("Just put selected values");
	putButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    int [] indices = dialogList.getSelectedIndices();
		    for (int i=0; i<indices.length; i++)
			((EPICSTextField)fieldVec.elementAt(i)).forcePut();	
		    doApply = false;
		    jd.setVisible(false);
		}
	    });
	JButton applyButton = new JButton("Just apply");
	applyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    doApply = true;
		    jd.setVisible(false);
		}
	    });
	JButton cancelButton = new JButton("Cancel");
	cancelButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    doApply = false;
		    jd.setVisible(false);
		}
	    });

	dialogPanel.add("0.01,0.01;0.99,0.10",new JLabel("You have edited some epics fields without putting the values..."));
	dialogPanel.add("0.01,0.11;0.99,0.10",new JLabel("EPICS recname ---- value in db ---- value you entered"));
	dialogPanel.add("0.01,0.21;0.99,0.50",jsp);
	dialogPanel.add("0.01,0.71;0.99,0.10",new JLabel("(Hold down the control key to select multiple entries)"));
	dialogPanel.add("0.01,0.81;0.49,0.10",putAndApplyButton);
	dialogPanel.add("0.51,0.81;0.49,0.10",putButton);
	dialogPanel.add("0.01,0.91;0.49,0.10",applyButton);
	dialogPanel.add("0.51,0.91;0.49,0.10",cancelButton);
	
	jd.setContentPane(dialogPanel);
	jd.setModal(true);
	jd.setAlwaysOnTop(true);
	jd.setSize(500,400);
	jd.setVisible(true);
	jd.dispose();
	blueVec = null;
	fieldVec = null;
	
    }

    public void recurseEPICSComponents(Container c){
	Component [] cmps = c.getComponents();
	for (int i=0; i<cmps.length; i++) {
	    if ( cmps[i] instanceof EPICSTextField && cmps[i].getBackground().equals(EPICSTextField.CHANGED_COLOR)) {
		EPICSTextField etf = (EPICSTextField)cmps[i];
		blueVec.add(etf.getPutrecName()+" ---- "+etf.getMonval()+" ---- "+etf.getText());
		fieldVec.add(etf);
	    } else if (cmps[i] instanceof Container)
		recurseEPICSComponents((Container)cmps[i]);
	}
    }

    
}
