package uffjec;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.lang.*;
import java.net.*;
import java.io.*;
import com.sun.j3d.utils.applet.MainFrame;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.behaviors.keyboard.*;
import com.sun.j3d.utils.behaviors.mouse.*;
import com.sun.j3d.utils.image.*;
import com.sun.j3d.utils.picking.behaviors.*;
import com.sun.j3d.utils.picking.*;
import javax.media.j3d.*;
import javax.vecmath.*;
import javax.swing.*;
import java.util.*;

public class EPICSView3D extends JApplet {

    int RING_SIZE = 30;

    Hashtable recBranchHash = new Hashtable();

    public EPICSView3D() {
	readRecShell();
	GraphicsConfigTemplate3D gconfig = new GraphicsConfigTemplate3D();
	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
	GraphicsDevice[] devices = ge.getScreenDevices();
	
	Canvas3D canvas3D = new Canvas3D(devices[0].getBestConfiguration(gconfig));
	//canvas3D.getGraphicsContext3D().setBackground(back);
	//canvas3D.getGraphicsContext3D().flush(true);
        getContentPane().add("Center", canvas3D);
	//getContentPane().add(canvas3D);
	// SimpleUniverse is a Convenience Utility class
	SimpleUniverse simpleU = new SimpleUniverse(canvas3D);
	BranchGroup scene = createSceneGraph(simpleU);
	// This moves the ViewPlatform back a bit so the
	// objects in the scene can be viewed.
	simpleU.getViewingPlatform().setNominalViewingTransform();
	
	simpleU.addBranchGraph(scene);
	//set the front and back clip distances
	simpleU.getViewer().getView().setFrontClipDistance(0.0005);
	simpleU.getViewer().getView().setBackClipDistance(1000);
	
	TransformGroup vpTrans = simpleU.getViewingPlatform().getViewPlatformTransform();
	Transform3D trans = new Transform3D();
	trans.setTranslation(new Vector3d(0,-10,-30));
	trans.lookAt(new Point3d(0f,-10f,-30f), new Point3d(0f,0f,-50f), new Vector3d(0f,1f,0f));
	vpTrans.setTransform(trans);

	
    }

    public BranchGroup getRing(TransformGroup parent, File f, UserData parentData) {
	File [] files = f.listFiles();
	BranchGroup bg = new BranchGroup();
	bg.setCapability(bg.ALLOW_DETACH);
	for (int i=0; i<files.length; i++) {
	    TransformGroup tg = getNonLeafNode(3,files[i].getName(),parentData);
	    Transform3D trans = new Transform3D();
	    trans.setScale(0.25);
	    trans.setTranslation(new Vector3d(RING_SIZE*Math.sin(2*i*Math.PI/files.length),-10,-50+RING_SIZE*Math.cos(2*i*Math.PI/files.length)));
	    Transform3D trans2 = new Transform3D();
	    parent.getTransform(trans2);
	    //trans2.setScale(0.25);
	    //trans2.setTranslation(new Vector3d(RING_SIZE*Math.sin(2*j*Math.PI/files.length),20,-50+RING_SIZE*Math.cos(2*j*Math.PI/files.length)));
	    trans2.mul(trans);
	    trans2.normalize();
	    tg.setTransform(trans2);
	    bg.addChild(tg);
	}
	return bg;
    }

    public BranchGroup createSceneGraph(SimpleUniverse su) {
	File [] files = (new File("foo")).listFiles();
	BranchGroup objRoot = new BranchGroup();
	DirectionalLight lightA = new DirectionalLight();
	lightA.setInfluencingBounds(new BoundingSphere(new Point3d(),1000000.0));
	lightA.setDirection(0, 0.5f, -1);
	objRoot.addChild(lightA);
	BranchGroup bg = new BranchGroup();
	bg.setCapability(bg.ALLOW_CHILDREN_EXTEND);
	bg.setCapability(bg.ALLOW_CHILDREN_READ);
	bg.setCapability(bg.ALLOW_CHILDREN_WRITE);
	bg.setCapability(bg.ALLOW_DETACH);
	for (int i=0; i<files.length; i++) {
	    TransformGroup tg = getNonLeafNode(3, files[i].getAbsolutePath(),null);
	    Transform3D trans = new Transform3D();
	    trans.setScale(0.25);
	    trans.setTranslation(new Vector3d(RING_SIZE*Math.sin(2*i*Math.PI/files.length),0,-50+RING_SIZE*Math.cos(2*i*Math.PI/files.length)));
	    tg.setTransform(trans);
	    bg.addChild(tg);
	    //BranchGroup ring = getRing(tg,files[i]);
	    //bg.addChild(ring);
	    //recBranchHash.put(files[i].getName(),ring);
	}

	objRoot.addChild(bg);

	TransformGroup vpTrans = su.getViewingPlatform().getViewPlatformTransform();
	KeyNavigatorBehavior keyNavBeh = new KeyNavigatorBehavior(vpTrans);
	keyNavBeh.setSchedulingBounds(new BoundingSphere(new Point3d(),1000.0));
	objRoot.addChild(keyNavBeh);

	Mouser pmb = new Mouser(su.getCanvas(),bg);
	pmb.setSchedulingBounds(new BoundingSphere(new Point3d(),1000.0));
	objRoot.addChild(pmb);

	Background back = new Background(0f,0f,0f);
	back.setApplicationBounds(new BoundingSphere(new Point3d(),1));
	back.setCapability(back.ALLOW_COLOR_READ);
	back.setCapability(back.ALLOW_COLOR_WRITE);
	back.setCapability(back.ALLOW_IMAGE_READ);
	back.setCapability(back.ALLOW_IMAGE_WRITE);
	bg.addChild(back);
	objRoot.compile();
	return objRoot;
   }

    public TransformGroup getNonLeafNode(int sides, String name, UserData parent) {
	// Appearance attributes for cube
	ColoringAttributes ca = new ColoringAttributes(0.0f, 150.0f, 0.0f, ColoringAttributes.SHADE_GOURAUD);
	PolygonAttributes pa = new PolygonAttributes();
	pa.setCullFace(PolygonAttributes.CULL_BACK);
	//pa.setBackFaceNormalFlip(true);
	Material material = new Material();
	material.setDiffuseColor(1.0f,1.0f,1.0f,0.0f);
	material.setAmbientColor(0.2f,0.2f,0.2f);
	//TransparencyAttributes ta = new TransparencyAttributes(TransparencyAttributes.BLENDED,0.35f);
	Appearance appeer = new Appearance();
	//appeer.setTransparencyAttributes(ta);
	appeer.setMaterial(material);
	appeer.setColoringAttributes(ca);
	appeer.setPolygonAttributes(pa);
	appeer.setCapability(appeer.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
	appeer.setCapability(appeer.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
	TransparencyAttributes ta = new TransparencyAttributes();
	ta.setCapability(ta.ALLOW_VALUE_WRITE);
	ta.setCapability(ta.ALLOW_MODE_WRITE);
	ta.setTransparencyMode(ta.NONE);
	//ta.setSrcBlendFunction(ta.BLEND_ONE_MINUS_SRC_ALPHA);
	//ta.setDstBlendFunction(ta.BLEND_ONE);
	ta.setTransparency(0);
	appeer.setTransparencyAttributes(ta);

	//    Transform3D trans = new Transform3D();
	//    trans.setScale(4.0 / NUM_SIDES);
	//    trans.setTranslation(new Vector3d(x,y,z));

	//    TransformGroup tg = new TransformGroup(trans);
	TransformGroup tg = new TransformGroup();
	//tg.addChild(new Box(1.0f,1.0f, 1.0f, Box.GEOMETRY_NOT_SHARED | Box.GENERATE_NORMALS |Box.ENABLE_GEOMETRY_PICKING,appeer));
	Shape3D db = getBox(sides);
	db.setAppearance(appeer);
	db.setUserData(new UserData(name,tg,parent));
	tg.addChild(db);
	// set capabilities to allow picking on these groups
	
	tg.setCapability(TransformGroup.ENABLE_PICK_REPORTING);
	tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	tg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
	tg.setCapability(tg.ALLOW_CHILDREN_READ);
	Transform3D t3d = new Transform3D();
	tg.getTransform(t3d);

	//t3d.rotY((float)-Math.PI/4.0);
	t3d.rotY( -2.0* Math.PI / (sides)  );
	tg.setTransform(t3d);
	return tg;
	
    }

    public Shape3D getBox(int numSides) {
	Shape3D thisa = new Shape3D();
	int [] stripCounts = new int[1];
	stripCounts[0] = 4 + (numSides-1) * 2;
	TriangleStripArray tsa = new TriangleStripArray(4 + (numSides-1) * 2,
							TriangleStripArray.COORDINATES | TriangleStripArray.NORMALS,stripCounts);
	Point3d [] vertexArray = new Point3d[4 + (numSides-1) * 2];
	int N = 0;
	for (int i=0; i<vertexArray.length; i+=2) {
	    vertexArray[i] =   new Point3d(1.5*Math.sin(N*2*Math.PI/numSides),1 ,
					   1.5*Math.cos(N*2*Math.PI/numSides));
	    vertexArray[i+1] = new Point3d(1.5*Math.sin(N*2*Math.PI/numSides),-1,
					   1.5*Math.cos(N++*2*Math.PI/numSides));
	}
	tsa.setCoordinates(0,vertexArray);
	GeometryInfo gi = new GeometryInfo(tsa);
	//gi.setCoordinates(vertexArray);
	NormalGenerator ng = new NormalGenerator();
	ng.generateNormals(gi);
	Stripifier st = new Stripifier();
	st.stripify(gi);
	GeometryArray ga = gi.getGeometryArray();
	ga.setCapability(tsa.ALLOW_COUNT_READ);
	ga.setCapability(tsa.ALLOW_FORMAT_READ);
	ga.setCapability(tsa.ALLOW_COORDINATE_READ);
	ga.setCapability(tsa.ALLOW_NORMAL_READ);
	ga.setCapability(tsa.ALLOW_NORMAL_WRITE);
	thisa.setGeometry(ga);
	thisa.setCapability(thisa.ALLOW_GEOMETRY_READ);
	thisa.setCapability(thisa.ALLOW_APPEARANCE_READ);
	thisa.setCapability(thisa.ALLOW_APPEARANCE_WRITE);
	PickTool.setCapabilities((Node)thisa,PickTool.INTERSECT_FULL);
	Appearance app = new Appearance();
	app.setCapability(app.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
	app.setCapability(app.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
	thisa.setAppearance(app);
	return thisa;
    }








    public static void readRecs(BufferedReader br) {
        try {
            //listdata = new Vector();
            while (br.ready()) {
                String sss = br.readLine();
                //if (sss.length() < 17) continue;
                sss = sss.substring(17);
                StringTokenizer st = new StringTokenizer(sss," ,");
                String lastToke = "";
                while (st.hasMoreTokens()) {
                    String toke = st.nextToken().trim();
                    if (toke.equals(lastToke)) continue;
                    if (toke.equals("recname:")) {
                        toke = st.nextToken();
                        StringTokenizer st1 = new StringTokenizer(toke,":");
                        String path = "";
                        while (st1.hasMoreTokens()) {
                            path += st1.nextToken()+"/";
                        }
                        path = path.substring(0,path.length()-1);
                        System.out.println("trying to create: "+path);
                        (new File(path)).mkdirs();
                        lastToke = toke;
                    } else if (toke.equals("pvname:")) {
                        toke = st.nextToken();
                        StringTokenizer st1 = new StringTokenizer(toke,":.");
                        String path = "";
                        while (st1.hasMoreTokens()) {
                            path += st1.nextToken()+"/";
                        }
                        path = path.substring(0,path.length()-1);
                        System.out.println("trying to create: "+path);
                        (new File(path)).createNewFile();
                        lastToke = toke;
                    } else {


                    }
                    //if (toke.startsWith("flam:")) {
                    //  listdata.add(EPICS.prefix+toke.substring(5));
                    //} else if (toke.startsWith("foo:")) {
                    //  listdata.add(EPICS.prefix+toke.substring(4));
                    //}
                }
            }
            //recList.setListData(listdata);
        } catch (FileNotFoundException fnfe) {
            System.err.println("EPICSView3D::readRecs> "+fnfe.toString());
        } catch (IOException ioe) {
            System.err.println("EPICSView3D::readRecs> "+ioe.toString());
        }
    }






       public static void readRecShell() {
        try {

            //Process proc = Runtime.getRuntime().exec("uf"+EPICS.prefix.substring(0,EPICS.prefix.length()-1)+"isd -l -vv");
            Process proc = Runtime.getRuntime().exec("/share/local/ufdrashkin/sbin/uffooepicsd -l -vv");
            BufferedInputStream bis =
                new BufferedInputStream(proc.getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            boolean keepGoing = true;
            while (keepGoing) {
                byte [] b = new byte[bis.available()];
                bis.read(b,0,b.length);
                baos.write(b,0,b.length);

                //have to figure out if the process is still going
                //proc.exitValue will throw an exception if the
                //process is still running, will return an integer
                //value otherwise.
                try { proc.exitValue(); keepGoing = false; }
                catch (Exception e) {}
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray())));
            readRecs(br);
        } catch (IOException ioe) {
            System.err.println("EPICSView3D::readRecShell> "+
                               ioe.toString());
            return;
        }
    }



    public static void main(String [] args) {
	JFrame j = new JFrame();
	j.setSize(400,400);
	j.setDefaultCloseOperation(3);
	j.setContentPane(new EPICSView3D());
	j.setVisible(true);
    }



    private class UserData {
	String name;
	TransformGroup trans;
	UserData parent;
	
	public UserData(String name, TransformGroup trans, UserData parent) {
	    this.name = name;
	    this.parent = parent;
	    this.trans = trans;
	}
	public String getName() { return name;}
	public UserData getParent() { return parent;}
	public TransformGroup getTransformGroup () { return trans;}
	public String toString() { return name;}
    }




    private class Mouser extends PickMouseBehavior {

	WakeupCriterion [] mouseEvents;
	WakeupOr mouseCriterion;
	BranchGroup bg;

	public Mouser(Canvas3D can, BranchGroup root) {
	    super(can,root,new BoundingSphere());
	    bg = root;
	    setMode(PickTool.GEOMETRY);
	    pickCanvas = new PickCanvas(can, root);
	    pickCanvas.setMode(PickTool.GEOMETRY_INTERSECT_INFO);
	    pickCanvas.setTolerance(0.0f);
	}

	public void initialize() {
	    mouseEvents = new WakeupCriterion[6];
	    mouseEvents[0] = new WakeupOnAWTEvent(MouseEvent.MOUSE_MOVED);
	    mouseEvents[1] = new WakeupOnAWTEvent(MouseEvent.MOUSE_PRESSED);
	    mouseEvents[2] = new WakeupOnAWTEvent(MouseEvent.MOUSE_RELEASED);
	    mouseEvents[3] = new WakeupOnAWTEvent(MouseEvent.MOUSE_DRAGGED);
	    mouseEvents[4] = new WakeupOnAWTEvent(KeyEvent.KEY_PRESSED);
	    mouseEvents[5] = new WakeupOnAWTEvent(KeyEvent.KEY_TYPED);
	    mouseCriterion = new WakeupOr(mouseEvents);
	    wakeupOn (mouseCriterion);	    
	}

	public void processStimulus (Enumeration criteria) {
	    WakeupCriterion wakeup;
	    AWTEvent [] event;
	    int id;
	    while (criteria.hasMoreElements()) {
		wakeup = (WakeupCriterion) criteria.nextElement();
		if (wakeup instanceof WakeupOnAWTEvent) {
		    event = ((WakeupOnAWTEvent)wakeup).getAWTEvent();
		    for (int i=0; i<event.length; i++) {
			id = event[i].getID();
			if (id == MouseEvent.MOUSE_PRESSED) {
			    pickCanvas.setShapeLocation((MouseEvent)event[i]);
			    if (pickCanvas.pickClosest() == null) { System.out.println("No dice"); continue;}
			    Node node = pickCanvas.pickClosest().getObject();
			    UserData ud = (UserData)node.getUserData();
			    if (recBranchHash.get(ud.getName()) == null) {
				if ((new File(ud.getName())).isDirectory()) {
				    BranchGroup ring = getRing(ud.getTransformGroup(),new File(ud.getName()),ud.getParent());
				    bg.addChild(ring);
				    recBranchHash.put(ud.getName(),ring);
				} else {
				    System.out.println("Going to get what's coming to me");
				}
			    } else {
				BranchGroup bran = (BranchGroup)recBranchHash.remove(ud.getName());
				bran.detach();
			    }
			}
		    }
		}
	    }

	    wakeupOn(mouseCriterion);
	}

	public void updateScene(int xpos, int ypos) {}

    }


}



