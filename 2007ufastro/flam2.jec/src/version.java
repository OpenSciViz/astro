package uffjec;


/**
 *Handles the current version of the package
 *Date is hard coded in the version field
 *Date is six digits starting with the year then month then day
 *This string is used for the name in the title bar
 */
  public class version {
  public static final String rcsID = "$Name:  $ $Id: version.java,v 0.2 2006/01/26 19:24:01 drashkin Exp $";
  public static final String version = "Beta 1";
}
