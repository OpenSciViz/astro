package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class EPICSRadioButton extends JRadioButton implements UFCAToolkit.MonitorListener {

	static final long serialVersionUID = 0;
	
    String hashKey = null;
    String clearHashKey = null;
    String monitorRec;
    String recToClear;
    String theValToMark;
    String outVal;

    public EPICSRadioButton(String epicsName, String epicsNameToClear,final String valToMark,final String valToClear) {
	theValToMark = valToMark;
	monitorRec = epicsName;
	recToClear = epicsNameToClear;
        if (monitorRec.indexOf(".") == -1 && monitorRec.indexOf(":") == -1) {
            hashKey = monitorRec;
            monitorRec = EPICS.recs.get(hashKey);
        }
        if (recToClear != null && recToClear.indexOf(".") == -1 && recToClear.indexOf(":") == -1) {
            clearHashKey = recToClear;
            recToClear = EPICS.recs.get(clearHashKey);
        }
	if (monitorRec.trim().equals("")) return;
 
	if (!EPICS.addMonitor(monitorRec,this)) 
	    setBackground(Color.red);
	else
	    setForeground(new JCheckBox().getForeground());

	addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (isSelected()) {
			EPICS.put(monitorRec,theValToMark);
			if (recToClear != null)
			    EPICS.put(recToClear,valToClear);
		    } else {
			EPICS.put(monitorRec,valToClear);
		    }
		}
	    });	
    }

    public EPICSRadioButton(String epicsName) {
	this(epicsName,null,EPICS.MARK+"","null");
    }

    public EPICSRadioButton(String epicsName, String epicsNameToClear) {
	this(epicsName,epicsNameToClear,EPICS.MARK+"","null");
    }

    public void monitorChanged(String value) {
	try {
	    outVal = value;
	    setToolTipText();
	    if (value.trim().toLowerCase().equals("null") || value.equals("")) {
		setSelected(false);
		
	    }else {
		int val = Integer.parseInt(value);
		if (val == EPICS.MARK || val == EPICS.PRESET) {
		    setSelected(true);		    
		} else
		    setSelected(false);
	    }
	} catch (Exception e) {
	    System.err.println("EPICSCheckBox.monitorChanged> "+e.toString());
	}
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
        if (hashKey != null) {
            monitorRec = EPICS.recs.get(hashKey);
        } else {
            monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
        }
	EPICS.addMonitor(monitorRec,this);
    }

    public void updateValToMark(String valToMark) {
	theValToMark = valToMark;
    }

    private void setToolTipText() {
	this.setToolTipText(monitorRec + " = " + outVal);
    }

}
