package uffjec;

import java.io.*;

//===============================================================================
/**
 * Opens a file for IN, OUT or APPEND in constructor. Methods include:
 * ok, readLine, println, and close.
 */
public class TextFile {
  BufferedReader in_file ;
  PrintWriter out_file ;
  String file_name;
  int mode;
  int line_num;
  boolean status_is_ok;
  public static int IN = 1;
  public static int OUT = 2;
  public static int APPEND = 3;

//-------------------------------------------------------------------------------
  /**
   * Construcs the TextFile object
   * @param file_name The name of the file to be read from or written to
   * @param mode must be TextFile.IN, TextFile.OUT or TextFile.APPEND
   */
  public TextFile(String file_name, int mode) {
    this.file_name = file_name;
    this.mode = mode;
    line_num = 0;
    if (mode == IN) {
      try {
        in_file = new  BufferedReader (new FileReader(file_name));
        status_is_ok = true;
      }
      catch (IOException ioe) {
        fjecError.show("opening " + file_name + " - " + ioe.toString());
        status_is_ok = false;
      }
    }
    else if (mode == OUT) {
      try {
        out_file = new  PrintWriter (new FileWriter(file_name), true /*autoflush*/) ;
        status_is_ok = true;
      }
      catch (IOException ioe) {
        fjecError.show("opening " + file_name + " - " + ioe.toString());
        status_is_ok = false;
      }
    }
    else if (mode == APPEND) {
      try {
        out_file = new  PrintWriter (new FileWriter(file_name, true /*append*/),
            true /*autoflush*/) ;
        status_is_ok = true;
      }
      catch (IOException ioe) {
        fjecError.show("opening " + file_name + " - " + ioe.toString());
        status_is_ok = false;
      }
    }
    else {
      fjecError.show("bad mode, should be TextFile.IN, TextFile.OUT or TextFile.APPEND");
      status_is_ok = false;
    }
  } //end of TextFile

//-------------------------------------------------------------------------------
  /**
   *Returns the status boolean
   */
  public boolean ok() {
    return status_is_ok;
  }

//-------------------------------------------------------------------------------
  /**
   *Gets the next line from the TextFile in IN mode and returns it
   *It shows errors if not in IN mode and if the status is bad
   */
  String readLine() {
    String line = "";
    if (!(mode == IN)) {
      fjecError.show("this file is not open for reading");
      status_is_ok = false;
    }
    else if (status_is_ok) {
      line_num++;
      try {
        line = in_file.readLine();
      }
      catch (IOException ioe) {
        fjecError.show("reading " + file_name + " at line_num " + line_num +
            " - " + ioe.toString());
        status_is_ok = false;
      }
    }
    else {
      // not status_is_ok, but they know that.
    }
    return line;
  } //end of readLine


//-------------------------------------------------------------------------------
  /**
   * Returns the first string from the file that
   * doesn't have a semicolon at the front
   */
  public String nextUncommentedLine()
    {
       String l = ";";
       while (l.charAt(0) == ';')
       {
          l = readLine();
	  if (!status_is_ok) return "";
	  if (l == null) break;
          if (l.trim().equals("")) break;
       }
       return l;
    } //end of nextUncommentedLine


//-------------------------------------------------------------------------------
  /**
   *Checks to see if its in OUT or APPEND mode
   *if not, it shows an error and the status flag turns false
   *otherwise it trys to write
   *then shows error if status is bad
   */
  public boolean println(String line) {
    if (!(mode == OUT) && !(mode == APPEND)) {
      fjecError.show("this file is not open for output or append");
      status_is_ok = false;
    }
    else if (status_is_ok) {
      line_num++;
      out_file.println(line);
      if (out_file.checkError()) {
        fjecError.show("writing \"" + line + "\" to " + file_name +
            " at line_num " + line_num );
        status_is_ok = false;
      }
    }
    else {
      // status_is_ok == false, but they know that.
    }
    return status_is_ok;
  } //end of println

//-------------------------------------------------------------------------------
  /**
   *Prints the string passed at the cursor point in the file
   *shows errors if not in OUT or APPEND mode and if status is bad
   *returns the status
   *@param line String: line to be written to the file
   */
  public boolean print(String line) {
    if (!(mode == OUT) && !(mode == APPEND)) {
      fjecError.show("this file is not open for output or append");
      status_is_ok = false;
    }
    else if (status_is_ok) {
      out_file.print(line);
      if (out_file.checkError()) {
        fjecError.show("writing \"" + line + "\" to " + file_name +
            " at line_num " + (line_num + 1) );
        status_is_ok = false;
      }
    }
    else {
      // status_is_ok == false, but they know that.
    }
    return status_is_ok;
  } //end of print

//-------------------------------------------------------------------------------
  /**
   *Closes the file
   */
  public void close() {
    if (mode == IN) {
      try {
        in_file.close();
      }
      catch (IOException ioe) {
        fjecError.show("closing " + file_name + " - " + ioe.toString());
        status_is_ok = false;
      }
    }
    if ((mode == OUT) || (mode == APPEND)) {
      out_file.close();
      if (out_file.checkError()) {
        fjecError.show("closing " + file_name);
        status_is_ok = false;
      }
    }
  } //end of close

} //end of class TextFile
