package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class JPanelDetectorHighLevel extends JPanel {
    
	static final long serialVersionUID = 0;
	
	JButton lvdtOnOffButton;
    EPICSLabel lvdtPosition;
    EPICSLabel lvdtVoltage;
    EPICSTextField biasValue;
    EPICSTextField exposureTime;
    EPICSLabel focalPosition;
    EPICSTextField numReads;

    public JPanelDetectorHighLevel() {
	lvdtOnOffButton = new JButton("Turn On LVDT");
	lvdtOnOffButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    lvdtButtonActionPerformed();
		}
	    });
	lvdtPosition = new EPICSLabel(EPICS.prefix+"sad:LVDTDISP","");
	lvdtVoltage = new EPICSLabel(EPICS.prefix+"sad:LVDTVLTS","");
	biasValue = new EPICSTextField(EPICS.prefix+"obsSetup.OverrideBias","");
	focalPosition = new EPICSLabel(EPICS.prefix+"sad:FOCUSPOS","");
	numReads = new EPICSTextField(EPICS.prefix+"obsSetup.NumReads","");
	exposureTime = new EPICSTextField(EPICS.prefix+"sad:EXPTIME","");
	JPanel notSurePanel = new JPanel();
	notSurePanel.setLayout(new GridLayout(0,1));
	notSurePanel.setBorder(new EtchedBorder(0));
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	p1.add(new JLabel("Focal Position:"));
	p1.add(focalPosition);
	p2.add(new JLabel("Number of Reads:"));
	p2.add(numReads);
	p3.add(new JLabel("Exposure Time:"));
	p3.add(exposureTime);
	p4.add(new JLabel("Bias Value:"));
	p4.add(biasValue);
	notSurePanel.add(p3); notSurePanel.add(p2); notSurePanel.add(p4);
	notSurePanel.add(p1);
	

	
	//JPanelLVDT lvdtPanel = new JPanelLVDT();
	//lvdtPanel.setBorder(new EtchedBorder(0));
	
	/*
	//////////////////////////////////////
	////////////////////////////////////////
	////////////////////////////////////////
	JPanel lvdtPanel = new JPanel();
	lvdtPanel.setBorder(new EtchedBorder(0));
	lvdtPanel.add(new JLabel("LVDT Displacement:"));
	lvdtPanel.add(lvdtPosition);
	lvdtPanel.add(new JLabel("LVDT Voltage:"));
	lvdtPanel.add(lvdtVoltage);
	lvdtPanel.add(lvdtOnOffButton);
	*/


	//JPanel biasPanel = new JPanel();
	//biasPanel.setBorder(new EtchedBorder(0));
	//biasPanel.add(new JLabel("Bias Value: "));
	//biasPanel.add(biasValue);
	setLayout(new RatioLayout());
	//if (lvdtPanel != null)
	//  add("0.15,0.60;0.70,0.20",lvdtPanel);
	//add("0.79,0.01;0.20,0.20",biasPanel);
	add("0.25,0.01;0.50,0.50",notSurePanel);
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("dc:"));
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	
	numReads.setText     ("0            ");
	focalPosition.setText("0            ");
	biasValue.setText    ("0            ");
	exposureTime.setText ("0            ");

    }

    public void lvdtButtonActionPerformed() {
	if (lvdtOnOffButton.getText().indexOf("On") != -1)
	    lvdtOnOffButton.setText("Turn Off LVDT");
	else
	    lvdtOnOffButton.setText("Turn On LVDT");
    }
}
