package uffjec;

import javax.swing.*;
import javax.swing.border.*;

public class JPanelLVDT extends JPanel {

	static final long serialVersionUID = 0;
	
    FJECMotor helloMoto = null;

    public JPanelLVDT() {
	setupVisuals();
    }

    public JPanelLVDT(FJECMotor moto) {
	helloMoto = (FJECMotor) moto.clone();
	setupVisuals();
    }

    public JPanelLVDT(FJECMotor [] motors) {
	if (motors != null){
	    for (int i=0; i<motors.length; i++) {
		if (motors[i].getName().toLowerCase().indexOf("focus") != -1){
		    helloMoto = (FJECMotor) motors[i].clone();
		    break;
		}
	    }
	}
	if (helloMoto == null) {
	    System.err.println("JPanelLVDT.JPanelLVDT> Did not find a motor with 'focus' in the name");
	}
	setupVisuals();
    }

    public void setupVisuals() {

/*
	Old hard-coded values
	EPICSLabel displacementLabel = new EPICSLabel(EPICS.prefix+"sad:LVDTDISP","");
	EPICSLabel voltageLabel = new EPICSLabel(EPICS.prefix+"sad:LVDTVLTS","");
	EPICSLabel aLabel = new EPICSLabel(EPICS.prefix+"sad:LVDTDISP","");
	EPICSLabel bLabel = new EPICSLabel(EPICS.prefix+"sad:LVDTDISP","");
*/
        EPICSLabel displacementLabel = new EPICSLabel(EPICSRecs.lvdtPosition,"");
        EPICSLabel voltageLabel = new EPICSLabel(EPICSRecs.lvdtVoltage,"");
        EPICSLabel aLabel = new EPICSLabel(EPICSRecs.lvdtPosition,"");
        EPICSLabel bLabel = new EPICSLabel(EPICSRecs.lvdtPosition,"");

	String [] strs = {"On","Off"};
	//EPICSComboBox powerBox = new EPICSComboBox(EPICS.prefix+"eng:focus.LVDTPwrOnOff",strs,EPICSComboBox.ITEM);
        EPICSComboBox powerBox = new EPICSComboBox(EPICSRecs.lvdtPwrOnOff,strs,EPICSComboBox.ITEM);
	String [] strs1 = {"Low","Hi"};
	//EPICSComboBox voltageBox = new EPICSComboBox(EPICS.prefix+"eng:focus.LVDTExcVoltsHiLow",strs1,EPICSComboBox.ITEM);
        EPICSComboBox voltageBox = new EPICSComboBox(EPICSRecs.lvdtExcVoltsHiLow,strs1,EPICSComboBox.ITEM);
	//EPICSComboBox freqBox = new EPICSComboBox(EPICS.prefix+"eng:focus.LVDTExcFreqHiLow",strs1,EPICSComboBox.ITEM);
        EPICSComboBox freqBox = new EPICSComboBox(EPICSRecs.lvdtExcFreqHiLow,strs1,EPICSComboBox.ITEM);
	//EPICSComboBox limitDirBox = new EPICSComboBox(EPICS.prefix+"eng:focus.LimitDirection",strs3,EPICSComboBox.ITEM);
/*
	Old hard-coded values
	EPICSTextField limitVelField = new EPICSTextField(EPICS.prefix+"eng:focus.LimitVel","");	
	EPICSTextField stepField = new EPICSTextField(EPICS.prefix+"eng:focus.Step","");
	EPICSTextField stepVelField = new EPICSTextField(EPICS.prefix+"eng:focus.StepVel","");
*/
        EPICSTextField limitVelField = new EPICSTextField(EPICSRecs.limitVel,"");
        EPICSTextField stepField = new EPICSTextField(EPICSRecs.focusStep,"");
        EPICSTextField stepVelField = new EPICSTextField(EPICSRecs.stepVel,"");

	JPanel powerPanel = new JPanel();
	powerPanel.setLayout(new RatioLayout());
	powerPanel.add("0.01,0.01;0.40,0.99",new JLabel("Power:"));
	powerPanel.add("0.51,0.01;0.40,0.99",powerBox);
	JPanel voltagePanel = new JPanel();
	voltagePanel.setLayout(new RatioLayout());
	voltagePanel.add("0.01,0.01;0.40,0.99",new JLabel("Voltage:"));
	voltagePanel.add("0.51,0.01;0.40,0.99",voltageBox);
	//voltagePanel.add("0.91,0.01;0.09,0.99",new JLabel("V"));
	JPanel freqPanel = new JPanel();
	freqPanel.setLayout(new RatioLayout());
	freqPanel.add("0.01,0.01;0.40,0.99",new JLabel("Frequency:"));
	freqPanel.add("0.51,0.01;0.40,0.99",freqBox);
	//freqPanel.add("0.91,0.01;0.09,0.99",new JLabel("kHz"));

	JPanel statusPanel = new JPanel();
	statusPanel.setBorder(new EtchedBorder(0));
	statusPanel.setLayout(new RatioLayout());
	statusPanel.add("0.01,0.01;0.25,0.49",new JLabel("LVDT Displacement:"));
	statusPanel.add("0.26,0.01;0.15,0.49",displacementLabel);
	statusPanel.add("0.51,0.01;0.25,0.49",new JLabel("LVDT Voltage:"));
	statusPanel.add("0.76,0.01;0.15,0.49",voltageLabel);
	statusPanel.add("0.01,0.51;0.25,0.49",new JLabel("A Readout:"));
	statusPanel.add("0.26,0.51;0.15,0.49",aLabel);
	statusPanel.add("0.51,0.51;0.25,0.49",new JLabel("B Readout:"));
	statusPanel.add("0.76,0.51;0.15,0.49",bLabel);

	JPanel controlPanel = new JPanel();
	controlPanel.setLayout(new RatioLayout());
	controlPanel.add("0.01,0.01;0.28,0.49",powerPanel);
	controlPanel.add("0.33,0.01;0.28,0.49",voltagePanel);
	controlPanel.add("0.67,0.01;0.28,0.49",freqPanel);
	controlPanel.add("0.01,0.51;0.15,0.49",new JLabel("Limit Vel"));
	controlPanel.add("0.16,0.51;0.15,0.49",limitVelField);
	//controlPanel.add("0.51,0.51;0.15,0.49",new JLabel("Limit Direction"));
	//controlPanel.add("0.66,0.51;0.25,0.49",limitDirBox);
	controlPanel.add("0.33,0.51;0.15,0.49",new JLabel("Steps: "));
	controlPanel.add("0.48,0.51;0.15,0.49",stepField);
	controlPanel.add("0.66,0.51;0.15,0.49",new JLabel("Step Vel:"));
	controlPanel.add("0.81,0.51;0.15,0.49",stepVelField);
	controlPanel.setBorder(new EtchedBorder(0));

	JPanel motorPanel = new JPanel();
	motorPanel.setBorder(new EtchedBorder(0));
	motorPanel.setLayout(new RatioLayout());	
	if (helloMoto != null) {
	    motorPanel.add("0.01,0.01;0.99,0.33",new JLabel("Focus Indexor",JLabel.CENTER));
	    motorPanel.add("0.01,0.34;0.99,0.33",FJECMotor.getMotorLowLevelLabelPanel());
	    motorPanel.add("0.01,0.67;0.99,0.33",helloMoto.getMotorLowLevelPanel());	    
	}
	

	setLayout(new RatioLayout());
	add("0.15,0.01;0.70,0.16",controlPanel);
	add("0.15,0.20;0.70,0.20",motorPanel);
	add("0.25,0.50;0.50,0.20",statusPanel);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	
	//add("0.01,0.20;0.30,0.15",new JLabel("Focus Wheel Position:"));	
    }

    public static void main(String [] args) {
	// unit test

	JFrame jf = new JFrame();
	jf.setSize(400,400);
	jf.setDefaultCloseOperation(3);
	jf.setContentPane(new JPanelLVDT());
	jf.setVisible(true);
    }

}
