//non-zero cycle type
//dbl from vme


package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class EPICSErrorWindow {

    static JTextArea errorArea;
    static JFrame mainFrame;

    static boolean docked;
    static int oldWidth;

    static Vector vCars = new Vector();

    static String [] carNames;

    public static void createCarMonitors() {
	vCars.clear();
	carNames = EPICS.recs.getCarNames();
	for (int i=0; i<carNames.length; i++) {
	    final int j = i;
	    vCars.add(
		new EPICSCarCallback(carNames[i], new EPICSCarListener(){
		    public void carTransition(int value) {
			if (value == EPICS.ERROR) {
			    signalError(carNames[j]);
			}
		    }
		})
	    );
	}
    }

    public static void checkForDock(JFrame j) {
    }

    public static void setupVisualComponents() {
	docked = false;
	errorArea = new JTextArea();
	mainFrame = new JFrame("EPICS Errors");
	mainFrame.setSize(400,400);
	oldWidth = 400;
	JPanel mainPanel = new JPanel();
	mainPanel.setLayout(new RatioLayout());
	JButton dismissButton = new JButton("Dismiss");
	JButton clearButton = new JButton("Clear");
	dismissButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    mainFrame.setVisible(false);
		}
	    });
	clearButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    errorArea.setText("");
		}
	    });
	mainPanel.add("0.01,0.01;0.99,0.89",new JScrollPane(errorArea));
	mainPanel.add("0.08,0.91;0.33,0.09",dismissButton);
	mainPanel.add("0.58,0.91;0.33,0.09",clearButton);
	mainFrame.setContentPane(mainPanel);
	mainFrame.setVisible(true);
	mainFrame.addComponentListener(new ComponentListener(){
		public void componentHidden(ComponentEvent ce) {}
		public void componentMoved(ComponentEvent ce) {
		    JFrame j = mainFrame;
		    if (fjecFrame.me == null || j == null) return;
		    int x = fjecFrame.me.getX();
		    int y = fjecFrame.me.getY()+fjecFrame.me.getHeight();
		    int xx = j.getX();
		    int yy = j.getY();
		    if (Math.abs(x - xx) < 10 && Math.abs(y - yy) < 10 ) {
			System.out.println("Docked: "+docked+", width: "+j.getWidth());
			//if (!docked) {
			    oldWidth = j.getWidth();
			    j.setBounds(x,y,fjecFrame.me.getWidth(),
					j.getHeight());
			    j.setPreferredSize(new Dimension(fjecFrame.me.getWidth(),j.getHeight()));
			    j.setMinimumSize(new Dimension(fjecFrame.me.getWidth(),j.getHeight()));
			    j.setSize(new Dimension(fjecFrame.me.getWidth(),j.getHeight()));
			    docked = true;
			    j.repaint();
			    System.out.println("Now docked, oldWidth:"+oldWidth+", new: "+fjecFrame.me.getWidth()+", actual: "+j.getWidth());
			    //}
		    } else {//if (docked) {
			docked = false;
			j.setSize(oldWidth,j.getHeight());
			System.out.println("Undocked");
		    }
		}
		public void componentResized(ComponentEvent ce) {}
		public void componentShown(ComponentEvent ce) {}
	    });
    }

    public static void signalError(String carRecName) {
	String crn = carRecName;
	if (!crn.startsWith(EPICS.prefix)) crn = EPICS.prefix+crn;
	//String msg = EPICS.get(EPICS.prefix+carRecName+".OMSS");
	String msg = EPICS.get(crn+".OMSS");
	if (errorArea == null || mainFrame == null) setupVisualComponents();
	errorArea.append(crn+": "+msg+"\n");
	errorArea.invalidate();
	mainFrame.setVisible(true);
	mainFrame.toFront();
	FJECSounds.play("scream.wav");
	/*
	try {
	    new Thread(){
		public void run(){
		    
		    long ll = System.currentTimeMillis();
		    mainFrameG = fjecFrame.getDrawBuffer();
		    Color oldC = mainFrameG.getColor();
		    //Font oldF = mainFrameG.getFont();
		    //Rectangle2D r = oldF.getStringBounds("EPICS ERROR",((Graphics2D)mainFrameG).getFontRenderContext());
		    //double scaleX = (fjecFrame.me.getWidth())/r.getWidth();
		    //double scaleY = (fjecFrame.me.getHeight())/r.getHeight();
		    //int fontSize = oldF.getSize();
		    //fontSize *= Math.min(scaleX,scaleY);
		    //Font newF = new Font(oldF.getFontName(),oldF.getStyle(),fontSize);
		    //AffineTransform at = new AffineTransform();
		    //at.scale(scaleX,scaleY);
		    //int fontSize = oldF.deriveFont(at).getSize();
		    //System.out.println("Set up fonts: "+(System.currentTimeMillis()-ll));
		    ll = System.currentTimeMillis();
		    for (int i=0; i<5; i++) {
			long lll = System.currentTimeMillis();
			if (lll-ll > 3000) break; // note, those are lower case l's, not the number 1
			mainFrameG = fjecFrame.getDrawBuffer();
			fjecFrame.repaintMe(mainFrameG);
			//mainFrameG.setFont(newF);
			//mainFrameG.setFont(newF);
			mainFrameG.setColor(new Color(255,0,0,128-(i*25)));
			mainFrameG.fillRect(0,0,fjecFrame.getTheWidth(),fjecFrame.getTheHeight());
			//mainFrameG.drawString("EPICS ERROR",0,fjecFrame.me.getHeight()*3/4);
			fjecFrame.repaintMe();
			//try {Thread.sleep(Math.max(100 - (System.currentTimeMillis()-lll),1));}
			//catch(Exception e){System.err.println("EPICSErrorWindow.Thread.run> "+e.toString());}			
		    }
		    mainFrameG.setColor(oldC);
		    //mainFrameG.setFont(oldF);
		    fjecFrame.repaintMe(mainFrameG);
		    fjecFrame.repaintMe();
		    System.out.println("Done signaling error: "+(System.currentTimeMillis()-ll));
		}
	    }.start();
	} catch (Exception e) {
	    System.err.println("EPICSErrorWindow.signalError> "+e.toString());
	}

	*/
    }

    public static void reconnect(String dbPrefix) {
	//implemented 6/26/06 -- remove old monitors and start new ones when db changed
	//System.err.println("EPICSErrorWindow.reconnect> THIS METHOD NOT YET IMPLEMENTED!!!!! Get on it, slacker!");
	EPICSCarCallback tempCallback;
	for (int j = 0; j < vCars.size(); j++) {
	    tempCallback = (EPICSCarCallback)(vCars.get(j));
	    tempCallback.disconnect();
	}
	vCars.clear();
        carNames = EPICS.recs.getCarNames();
        for (int i=0; i<carNames.length; i++) {
            final int j = i;
            vCars.add(
                new EPICSCarCallback(carNames[i], new EPICSCarListener(){
                    public void carTransition(int value) {
                        if (value == EPICS.ERROR) {
                            signalError(carNames[j]);
                        }
                    }
                })
            );
        }
    }

}
