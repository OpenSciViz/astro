package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import java.net.*;
import java.util.*;

public class JPanelWebBrowser extends JPanel {

    JEditorPane htmlPane;
    JTextField urlField;
    JLabel linkLabel;
    JTextField thinkingField;
    Thread pageLoadThread;
    Stack backHistory;
    int backHistorySize;
    JButton backButton;

    private void setThePage(final URL theURL) {	
	try {
	    if (pageLoadThread != null && pageLoadThread.isAlive()){
		System.err.println("JPanelWebBrowser.setThePage> Already trying to load another page!");
		return;
	    }
	    urlField.setText(theURL.toString());
	    thinkingField.setBackground(Color.yellow);
	    pageLoadThread = new Thread() {
		    public void run() {
			try {
			    htmlPane.setPage(theURL);
			    thinkingField.setBackground(Color.green);
			    htmlPane.invalidate();
			} catch (Exception e) {
			    System.err.println("JPanelWebBrowser.setThePage.run> "+e.toString());
			    thinkingField.setBackground(Color.red);
			}
		    }
		};
	    pageLoadThread.start();
	} catch (Exception e) {
	    System.err.println("JPanelWebBrowser.setThePage> "+e.toString());
	}
    }

    private void setBackHistory() {
	if (htmlPane.getPage() == null) return;
	backHistory.push(htmlPane.getPage());
	//backHistory.setSize(backHistorySize);
	//backHistory.trimToSize();
	backButton.setEnabled(true);
    }

    public JPanelWebBrowser() {
	try {
	    backHistorySize = 20;
	    backHistory = new Stack();
	    thinkingField = new JTextField();
	    thinkingField.setEditable(false);
	    thinkingField.setBackground(Color.green);
	    linkLabel = new JLabel("");
	    urlField = new JTextField("http://www.astro.ufl.edu/~drashkin");
	    urlField.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			try {
			    Document doc = htmlPane.getDocument();
			    doc.putProperty(Document.StreamDescriptionProperty, null);
			    setBackHistory();
			    setThePage(new URL(urlField.getText()));
			}catch (Exception e) {
			    System.err.println("JPanelWebBrowser.actionPerformed> "+e.toString());
			    thinkingField.setBackground(Color.red);
			}
		    }
		});
	    setLayout(new RatioLayout());
	    htmlPane = new JEditorPane();
	    htmlPane.setEditable(false);
	    htmlPane.addHyperlinkListener(new HyperlinkListener() {
		    public void hyperlinkUpdate(HyperlinkEvent e) {
			if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			    JEditorPane pane = (JEditorPane) e.getSource();
			    if (e instanceof HTMLFrameHyperlinkEvent) {
				HTMLFrameHyperlinkEvent  evt = (HTMLFrameHyperlinkEvent)e;
				HTMLDocument doc = (HTMLDocument)pane.getDocument();
				doc.processHTMLFrameHyperlinkEvent(evt);
			    } else {
				try {
				    setBackHistory();
				    setThePage(e.getURL());				    
				} catch (Exception ex) {
				    System.err.println("JPanelWebBrowser.hyperlinkUpdate> "+ex.toString());
				    thinkingField.setBackground(Color.red);
				}
			    }
			} else if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
			    if (e.getURL() != null && e.getURL().toString() != null) 
				linkLabel.setText(e.getURL().toString());
			    else
				linkLabel.setText("");
			} else if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
			    linkLabel.setText("");
			}
		    }
		});
	    backButton = new JButton("<-");
	    backButton.setEnabled(false);
	    backButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			try {
			    setThePage((URL)backHistory.pop());
			    if (backHistory.size() == 0)
				backButton.setEnabled(false);
			} catch (EmptyStackException ese) {}
		    }
		});
	    add("0.01,0.01;0.08,0.08",backButton);
	    add("0.10,0.01;0.50,0.08",urlField);
	    add("0.92,0.01;0.08,0.08",thinkingField);
	    add("0.01,0.10;0.99,0.85",new JScrollPane(htmlPane));
	    //add("0.01,0.10;0.99,0.85",(htmlPane));
	    add("0.01,0.95;0.99,0.05",linkLabel);
	} catch (Exception e) {
	    System.err.println("JPanelWebBrowser> "+e.toString());
	}
    }

    public static void main(String [] args) {
	JFrame j = new JFrame();
	j.setContentPane(new JPanelWebBrowser());
	j.setSize(400,400);
	j.setDefaultCloseOperation(3);
	j.setVisible(true);
    }

}
