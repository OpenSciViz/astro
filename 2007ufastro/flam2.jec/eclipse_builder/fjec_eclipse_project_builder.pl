#!/usr/bin/perl




if (!$ARGV[0] or !$ARGV[1] or !$ARGV[2]) {
    print "Usage: project_builder <workspace_dir_name> <source_dir_to_symlink_source_files_from> <metadata_tar_file>\n";
} else {

    `mkdir -p $ARGV[0]/fjec/uffjec`;
 
    `echo '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' > $ARGV[0]/fjec/.project`;
    `echo '<projectDescription>' >> $ARGV[0]/fjec/.project`;
    `echo '<name>fjec</name>' >> $ARGV[0]/fjec/.project`;
    `echo '<comment></comment>'>> $ARGV[0]/fjec/.project`; 
    `echo '<projects>' >> $ARGV[0]/fjec/.project`;
    `echo '</projects>' >> $ARGV[0]/fjec/.project`;
    `echo '<buildSpec>'>> $ARGV[0]/fjec/.project`;
    `echo '<buildCommand>'>> $ARGV[0]/fjec/.project`;
    `echo '<name>org.eclipse.jdt.core.javabuilder</name>'>> $ARGV[0]/fjec/.project`;
    `echo '<arguments>'>> $ARGV[0]/fjec/.project`;
    `echo '</arguments>'>> $ARGV[0]/fjec/.project`;
    `echo '</buildCommand>'>> $ARGV[0]/fjec/.project`;
    `echo '</buildSpec>'>> $ARGV[0]/fjec/.project`;
    `echo '<natures>'>> $ARGV[0]/fjec/.project`;
    `echo '<nature>org.eclipse.jdt.core.javanature</nature>'>> $ARGV[0]/fjec/.project`;
    `echo '</natures>' >> $ARGV[0]/fjec/.project`;
    `echo '</projectDescription>'>> $ARGV[0]/fjec/.project`;

    `echo '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' > $ARGV[0]/fjec/.classpath`;
    `echo '<classpath>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"src\" path=\"\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"con\" path=\"org.eclipse.jdt.launching.JRE_CONTAINER\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"lib\" path=\"$ENV{'UFINSTALL'}/javalib/javaUFLib.jar\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"lib\" path=\"$ENV{'UFINSTALL'}/javalib/javaUFProtocol.jar\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"lib\" path=\"$ENV{'UFINSTALL'}/javalib/ufjca.jar\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '<classpathentry kind=\"output\" path=\"\"/>' >> $ARGV[0]/fjec/.classpath`;
    `echo '</classpath>' >> $ARGV[0]/fjec/.classpath`;


    `mkdir -p $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches`;

    `echo '<?xml version=\"1.0\" encoding=\"UTF-8\"?>' > $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '<launchConfiguration type=\"org.eclipse.jdt.launching.localJavaApplication\">' >> $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '<stringAttribute key=\"org.eclipse.jdt.launching.MAIN_TYPE\" value=\"uffjec.fjec\"/>' >> $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '<stringAttribute key=\"org.eclipse.jdt.launching.PROJECT_ATTR\" value=\"fjec\"/>' >> $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '<stringAttribute key=\"org.eclipse.jdt.launching.VM_ARGUMENTS\" value=\"-Dfjec.data_path=${env_var:UFINSTALL}/fjec/\"/>' >> $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '<booleanAttribute key=\"org.eclipse.debug.core.appendEnvironmentVariables\" value=\"true\"/>' >> $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;
    `echo '</launchConfiguration>' >>  $ARGV[0]/.metadata/.plugins/org.eclipse.debug.core/.launches/fjec.launch`;

    `cd $ARGV[0]/fjec/uffjec; ln -s $ARGV[1]/*.java .`;
    `tar  xzvf  $ARGV[2] -C $ARGV[0]` ; 
}


exit;


#labdef -- if marked (set to any positive integer, will disregard all other inputs -- will load all the lab (ldvar vars and settings) into mce4.
