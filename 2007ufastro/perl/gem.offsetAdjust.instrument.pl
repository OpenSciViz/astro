#!/usr/local/bin/perl -w

use strict;

use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 2 ) { # assume -help
   die "\n\tUsage:".
       "\n\tEnter relative offset, in units of arcseconds, ".
       "\n\tin instrument coordinate frame".
       "\n\toffsetAdjust.instrument.pl instr_x_offset instr_y_offset".
       "\n\n";

}else{
  my $x_offset = shift;
  my $y_offset = shift;

  if( $x_offset =~ m/[a-zA-Z]/ or $y_offset =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical x_offset or y_offset ".
        "by mistake?".
	"\n\nx_offset = $x_offset; y_offset = $y_offset\n\n";

  }else{
    my $soc = ufgem::viiconnect();
    #ufgem::offsetAdjust_instrument( $soc, $x_offset, $y_offset );
    offsetAdjust_instrument( $soc, $x_offset, $y_offset );
    close( $soc );
  }
}#End


