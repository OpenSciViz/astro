package AllbutMosWheel;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Wheel ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.

our %EXPORT_TAGS = 
  ( 'all' => [ qw(   

$DECKER_REV  
@DECKER_NAMES     @DECKER_BASE     @DECKER_OFFSET     @DECKER_MDPTS 
@DECKER_NEG_NAMES @DECKER_NEG_BASE                    @DECKER_NEG_MDPTS
@DECKER           @NEG_DECKER  

$FILTER_REV       		   
@FILTER_NAMES      @FILTER_BASE      @FILTER_OFFSET   @FILTER_MDPTS
@FILTER_NEG_NAMES  @FILTER_NEG_BASE                   @FILTER_NEG_MDPTS 
@FILTER            @NEG_FILTER

$LYOT_REV
@LYOT_NAMES        @LYOT_BASE       @LYOT_OFFSET      @LYOT_MDPTS 
@LYOT_NEG_NAMES    @LYOT_NEG_BASE                     @LYOT_NEG_MDPTS
@LYOT              @NEG_LYOT

$GRISM_REV
@GRISM_NAMES       @GRISM_BASE       @GRISM_OFFSET   @GRISM_MDPTS
@GRISM_NEG_NAMES   @GRISM_NEG_BASE                   @GRISM_NEG_MDPTS  
@GRISM             @NEG_GRISM

&get_wheel_info

) ] ,

    'test' => [ qw( add_test2 ) ]
  );


our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} }, @{ $EXPORT_TAGS{'test'} } );
our @EXPORT = qw( );
our $VERSION = '0.01';


# Preloaded methods go here.

#<<<<<WHEEL POSITIONS>>>>>#
our $str_quote = "\"";
our $space     = " ";
our $ampersand = " & ";

our $cmd_head = $ENV{UFMOTOR}.$space.
                $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
                $ENV{CLIENT_QUIET}.$space;

use constant PI => 4 * atan2 1, 1;

our $DECKER_REV       = 9000;
our @DECKER_NAMES     = qw( dark  imaging   slit    mos );
our @DECKER_BASE      =   (    0,    2250,  4500,  6750 );
our @DECKER_OFFSET    =   (    1,       0,    20,    20 );
our @DECKER_MDPTS     =   ( 1125,    3375,  4625,  7875 );

our @DECKER_NEG_NAMES = qw(  dark     mos   slit  imaging );
our @DECKER_NEG_BASE  =   (     0,  -2250, -4500, -6750   );
our @DECKER_NEG_MDPTS =   ( -1125,  -3375, -4625, -7875   );


our @DECKER;
for(my $i=0; $i < 4; $i++){ 
  $DECKER[$i] = $DECKER_BASE[$i] + $DECKER_OFFSET[$i];
}

our @NEG_DECKER;
for(my $i=0; $i < 4; $i++){ 
  $NEG_DECKER[$i] = $DECKER_NEG_BASE[$i] + $DECKER_OFFSET[$i];
}

#####FILTER
our $FILTER_REV = 1250;
our @FILTER_NAMES  = qw( J     H    K   HK   JH    KS );
our @FILTER_BASE   =   ( 0,  208, 417, 625, 833, 1042 );
our @FILTER_OFFSET =   ( 0,    0,   0,   0,   0,    0 );
our @FILTER_MDPTS  = ( 104,  313, 521, 729, 937, 1146 );

our @FILTER_NEG_NAMES = qw(    J    KS    JH    HK     K      H );
our @FILTER_NEG_BASE  =   (    0, -208, -417, -625, -833, -1042 );
our @FILTER_NEG_MDPTS =   ( -104, -313, -521, -729, -937, -1146 );

our @FILTER;
for(my $i=0; $i < 6; $i++){
  $FILTER[$i] = $FILTER_BASE[$i] + $FILTER_OFFSET[$i];
}

our @NEG_FILTER;
for(my $i=0; $i < 6; $i++){
  $NEG_FILTER[$i] = $FILTER_NEG_BASE[$i] + $FILTER_OFFSET[$i];
}

####LYOT
our $LYOT_REV    = 1250;
our @LYOT_NAMES  = ( "Hartmann1", "Hartmann2", "open", "gem", "mmt", "4m", "2.1m" );
#our @LYOT_NAMES  = ( "dark1", "dark2", "open", "gem", "mmt", "4m", "2.1m" );
our @LYOT_BASE   = (       0,     178,    357,   536,  714,   893,   1071 );
our @LYOT_OFFSET = (      11,      11,     12,    12,   12,    12,     12 );
our @LYOT_MDPTS  = (      89,     267,    446,   624,  804,   983,   1160 );

our @LYOT_NEG_NAMES = ( "dark1", "2.1m",  "4m", "mmt", "gem", "open", "dark2" );
our @LYOT_NEG_BASE  = (       0,   -179,  -357,  -536,  -714,   -893,  -1072  );
our @LYOT_NEG_MDPTS = (     -89,   -268,  -446,  -625,  -803,   -982,  -1161  );

our @LYOT;
for(my $i=0; $i < 7; $i++){
  $LYOT[$i] = $LYOT_BASE[$i] + $LYOT_OFFSET[$i];
}

our @NEG_LYOT;
for(my $i=0; $i < 7; $i++){
  $NEG_LYOT[$i] = $LYOT_NEG_BASE[$i] + $LYOT_OFFSET[$i];
}

######Grism
our $GRISM_REV    = 1250;
our @GRISM_NAMES  = ( "open1", "dark1", "dark2", "open2", "HK" );
our @GRISM_BASE   = (       0,     250,     500,     750, 1000 );
our @GRISM_OFFSET = (       0,       0,       0,       0,    0 );
our @GRISM_MDPTS  = (     125,     375,     625,     875, 1125 );

our @GRISM_NEG_NAMES = ( "open1", "HK", "open2", "dark2", "dark1" );
our @GRISM_NEG_BASE  = (       0, -250,    -500,    -750,   -1000 );
our @GRISM_NEG_MDPTS = (    -125, -375,    -625,    -875,   -1125 );

our @GRISM;
for(my $i=0; $i < 5; $i++){
  $GRISM[$i] = $GRISM_BASE[$i] + $GRISM_OFFSET[$i];
}

our @NEG_GRISM;
for(my $i=0; $i < 5; $i++){
  $NEG_GRISM[$i] = $GRISM_NEG_BASE[$i] + $GRISM_OFFSET[$i];
}
##########
####SUBS
sub get_wheel_status{
  my @motors    = qw( a c d e );
  my @wheels    = qw( decker filter lyot grism );
  #The following arrays will have 4 entries that correspond to @wheels
  my ( @actual_pos, @closest_pos, @closest_name, @difference, @motion );

  #PUT flamingos configuration calls made through system here
  #print "SHOULD BE:\n";
  #print "Asking the motor controllers for the present positions....\n\n";
  #print "USING TEST POSITIONS\n";
  #@actual_pos = ( 2250, 0, 541, 0 );
  print "Asking the motor controllers for the present positions....\n\n";
  @actual_pos = Get_Positions();
  print "\n\n";
  #print "SHOULD BE:\n";
  #print "Asking if the motors are moving...\n\n";
  #print "USING TEST MOTIONS\n";
  #@motion = qw( Stationary Stationary Stationary Stationary );
  print "Asking if the motors are moving...\n\n";
  @motion = Get_Motions( );
  print "\n\n";

  #Decker 
  ( $closest_pos[0], $closest_name[0], $difference[0] ) =
    det_nearest_posn( $actual_pos[0], 
		      \@DECKER,       \@NEG_DECKER,
		      \@DECKER_NAMES, \@DECKER_NEG_NAMES,
		      \@DECKER_MDPTS, \@DECKER_NEG_MDPTS, $DECKER_REV );

  #Filter
  ( $closest_pos[1], $closest_name[1], $difference[1] ) =
    det_nearest_posn( $actual_pos[1],
		      \@FILTER,       \@NEG_FILTER,
		      \@FILTER_NAMES, \@FILTER_NEG_NAMES,
		      \@FILTER_MDPTS, \@FILTER_NEG_MDPTS, $FILTER_REV );

  #Lyot
  ( $closest_pos[2], $closest_name[2], $difference[2] ) =
    det_nearest_posn( $actual_pos[2], 
		      \@LYOT,       \@NEG_LYOT,
		      \@LYOT_NAMES, \@LYOT_NEG_NAMES,
		      \@LYOT_MDPTS, \@LYOT_NEG_MDPTS, $LYOT_REV );

  #Grism 
  ( $closest_pos[3], $closest_name[3], $difference[3] ) =
    det_nearest_posn( $actual_pos[3], 
		      \@GRISM,       \@NEG_GRISM,
		      \@GRISM_NAMES, \@GRISM_NEG_NAMES,
		      \@GRISM_MDPTS, \@GRISM_NEG_MDPTS, $GRISM_REV );

  return( \@motors,       \@wheels, 
	  \@actual_pos,   \@closest_pos, 
	  \@closest_name, \@difference, \@motion );

}#Endsub get_wheel_info


sub Get_Positions{
  my @positions = ( 0, 0, 0, 0 );

  #GET MOTOR ALL POSITIONS AT ONCE
  my $cmd_str = $cmd_head . $str_quote."aZ & cZ & dZ & eZ".$str_quote;
  print "Executing\n$cmd_str\n";
  my $reply = `$cmd_str`;
  sleep 1.0;
  
  my $a_index = index $reply, 'aZ';
  my $c_index = index $reply, 'cZ';
  my $d_index = index $reply, 'dZ';
  my $e_index = index $reply, 'eZ';
  my $l_index = length $reply;
  
  my $a_substr = substr $reply, $a_index, $c_index - $a_index;
  my $c_substr = substr $reply, $c_index, $d_index - $c_index;
  my $d_substr = substr $reply, $d_index, $e_index - $d_index;
  my $e_substr = substr $reply, $e_index, $l_index - $e_index;
  
  my $a_dot = index $a_substr, "\.";
  my $a_val = substr $a_substr, 2, $a_dot - 2;
  
  my $c_dot = index $c_substr, "\.";
  my $c_val = substr $c_substr, 2, $c_dot -2;
  
  my $d_dot = index $d_substr, "\.";
  my $d_val = substr $d_substr, 2, $d_dot -2;
  
  my $e_dot = index $e_substr, "\.";
  my $e_val = substr $e_substr, 2, $e_dot -2;
  
  $positions[0] = $a_val;
  $positions[1] = $c_val;
  $positions[2] = $d_val;
  $positions[3] = $e_val;
  
  return @positions;
}#endsub Get_Positions


sub Get_Motions{
  my @motions = ( 0, 0, 0, 0 );

  #GET All MOTOR MOTIONS AT ONCE
  my $cmd_str = $cmd_head .$str_quote."a^ & c^ & d^ & e^".$str_quote;
  my $reply = `$cmd_str`;
  sleep 1.0;
  
  my $a_index = index $reply, 'a^';
  my $c_index = index $reply, 'c^';
  my $d_index = index $reply, 'd^';
  my $e_index = index $reply, 'e^';
  my $l_index = length $reply;
  
  my $a_substr = substr $reply, $a_index, $c_index - $a_index;
  my $c_substr = substr $reply, $c_index, $d_index - $c_index;
  my $d_substr = substr $reply, $d_index, $e_index - $d_index;
  my $e_substr = substr $reply, $e_index, $l_index - $e_index;
  
  my $a_dot = index $a_substr, "\n";
  my $a_val = substr $a_substr, 2, $a_dot - 2;
  
  my $c_dot = index $c_substr, "\n";
  my $c_val = substr $c_substr, 2, $c_dot -2;
  
  my $d_dot = index $d_substr, "\n";
  my $d_val = substr $d_substr, 2, $d_dot -2;
  
  my $e_dot = index $e_substr, "\n";
  my $e_val = substr $e_substr, 2, $e_dot -2;
  
  #my $interpretation = Interpret_Motion($a_val);
  $motions[0] = Interpret_Motion($a_val);
  $motions[1] = Interpret_Motion($c_val);
  $motions[2] = Interpret_Motion($d_val);
  $motions[3] = Interpret_Motion($e_val);

  return @motions;
}#Endsub Get_Motions


sub Interpret_Motion{
  my $input = $_[0];

  #print "input is $input\n";
  if( $input == 0  ){ return "Stationary" }
  if( $input == 1  ){ return "Moving"     }
  if( $input == 10 ){ return "Homing, Constant Vel" }
  if( $input == 42 ){ return "Homing, Constant Vel, Ramping" }

}#Endsub Interpret_Motion


sub det_nearest_posn{
  my ($pos,         $aref_wval, $aref_neg_wval,
      $aref_wnam,   $aref_neg_wnam,
      $aref_wmdpts, $aref_wnegmdpts, $wheel_rev ) = @_;
  my $guess_index     = 0;
  my @wheel_vals      = @$aref_wval;
  my @wheel_neg_vals  = @$aref_neg_wval;
  my @wheel_names     = @$aref_wnam;
  my @wheel_neg_names = @$aref_neg_wnam;
  my @wheel_mdpts     = @$aref_wmdpts;
  my @wheel_neg_mdpts = @$aref_wnegmdpts;

  my $num_posn        = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #print "pos=$pos \t num_pos=$num_posn\n";
  my $actual_pos = $pos;
  if( $pos >= 0 ){
    #print ">>>Checking against positives\n";
    if( $pos > $wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $wheel_rev;
	if( $pos <= $wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    if( $pos >= 0  and $pos < $wheel_mdpts[0] ){
      #print "case 0: between 0 and first midpt\n\n";
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	#Positive vals first
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking posn=$pos against hp".($i-1).
	#      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";

	if( ($pos >= $wheel_mdpts[$i - 1]) and 
	    ($pos < $wheel_mdpts[$i] ) ){
	  #print "in if $i ";
	  $guess_index = $i;
	}elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
		$pos <= $wheel_rev ){
	  #This checks between last position and home
	  #print "in last check\n";
	  #print "pos = $pos\n";
	  #print "lastpos = $wheel_neg_mdpts[$num_posn - 1]\n";
	  #print "wrev    = $wheel_rev\n";
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos < 0 ){
    #print ">>>Checking against negatives\n";
    my $neg_wheel_rev = -1 * $wheel_rev;

    if( $pos < $neg_wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $neg_wheel_rev;
	if( $pos >= $neg_wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    #print "pos now=$pos\n";
    if( $pos <= 0  and $pos > $wheel_neg_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	  #print "i=$i, guess_index=$guess_index:  ";
	  #print "checking posn=$pos against hp".($i-1).
	  #  "=$wheel_neg_mdpts[$i-1] and hp$i=$wheel_neg_mdpts[$i]\n";
	if( $pos <= $wheel_neg_mdpts[$i - 1] and 
	    $pos > $wheel_neg_mdpts[$i] ){
	  $guess_index = $i;
	}elsif( $pos < $wheel_neg_mdpts[$num_posn - 1] and 
		$pos >= $wheel_rev ){
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_neg_vals[$guess_index];
    $closest_name = $wheel_neg_names[$guess_index];
    $diff         = $actual_pos - $wheel_neg_vals[$guess_index];

  }#End ifelse pos and neg

  #print "\n\n$closest_pos=$closest_name, $diff\n\n";
  return( $closest_pos, $closest_name, $diff );
}#Endsub det_nearest_posn


sub PrintAllbutMosinfo{
  my ( $aref1, $aref2, $aref3, $aref4, $aref5, 
       $aref6, $aref7, $aref8) = @_;
  my @motors       = @$aref1;
  my @wheels       = @$aref2;
  my @actual_pos   = @$aref3;
  my @closest_pos  = @$aref4;
  my @closest_name = @$aref5;
  my @difference   = @$aref6;
  my @motion       = @$aref7;
  my @home_types   = @$aref8;

  #system("clear");
  print "\n\n";
  print 
">>>>>>>>>>>>>>>>>>   PRESENT STATUS OF WHEELS   <<<<<<<<<<<<<<<<\n";
  print 
"________________________________________________________________\n";

  print "\t\t\t ACTUAL\t\tCLOSEST LIKELY\n";
  print "MOTOR\tWHEEL\t\tPOSITION\tPOSITION\  NAME   DIFFERENCE  MOTION".
        "\tHOME-TYPE\n";
  print "_____\t_____\t\t________\t____________________________________".
        "_________________\n";

  #PRINT OUT DECKER, FILTER, LYOT, GRISM INFO
  for( my $i = 0; $i < 4; $i++ ){
    print " $motors[$i]\t$wheels[$i]\t\t$actual_pos[$i]\t\t";
    print "$closest_pos[$i]\t$closest_name[$i]\t     $difference[$i]\t";  
    print "     $motion[$i]\t";

    my $ht_interp;
    if( $home_types[$i] == 0 ){
      print "Near-home\n";
    }elsif( $home_types[$i] == 1 ){
      print "Limit-switch\n";
    }
  }
  print 
"_________________________________________________________________________".
 "___________________\n";

}#Endsub PrintAllbutMosinfo



#Export tests
#our $ADD_TEST = DECKER_BASE->{"mos"} + DECKER_OFFSET->{"mos"};
use constant BASE => 100;
use constant OFFSET => 11;
use constant add_test2 => BASE + OFFSET;

#Old, factor of 2 gear reduction numbers
#our @MOS_SLIT_BASE =    ( 0,        2134,  3096,  3986,
#			  4876,     5766,  6658,  7548,
#			  8438,     9328, 10220, 12000,
#			  13780,   15560, 17340, 19120,
#			  20900,   21834
#			  );

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 Wheel.pl

Wheel - a perl module containing Flamingos wheel info and subs

=head1 SYNOPSIS

  use Wheel;

=head1 DESCRIPTION

Use to control Flamingos filter wheels.

=head2 EXPORT

None by default.


=head2 EXPORT_OK

constant PI

=head1 AUTHOR

SNR

=head1 SEE ALSO

perl(1).

=cut
