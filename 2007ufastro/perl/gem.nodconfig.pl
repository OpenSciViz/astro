#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 2 ) {
  #print help message
  die "\n\n\tUsage:  gem.nodconfig.pl throw (arcsec) pa (deg)\n\n";

}else{
  my $throw = shift;
  my $pa    = shift;

  if( $throw =~ m/[a-zA-Z]/ or $pa =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical throw or pa by mistake?".
        "\n\nthrow = $throw; pa = $pa\n\n";

  }else{
    my $soc = ufgem::viiconnect();
    ufgem::nodConfig($soc, $throw, $pa);
    close( $soc );
  }

}#End


