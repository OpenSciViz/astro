#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage: gem.mountguide.pl on|off\n";

}else {
  my $state = shift;

  if( $state =~ m/^on$/i ){
    print "Turning mountGuide on\n";
    my $soc = ufgem::viiconnect();
    mountGuideOn( $soc );
    close( $soc );

  }elsif( $state =~ m/^off$/i ){
    print "Turning mountGuide off\n";
    my $soc = ufgem::viiconnect();    
    mountGuideOff( $soc );
    close( $soc );

  }else{
    die "\n\n\tPlease enter gem.mountguide.pl on or ".
        "gem.mountguide.pl off\n\n";
  }

}#End


