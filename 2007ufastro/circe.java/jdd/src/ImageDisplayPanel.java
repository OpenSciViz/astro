package ufjdd;

/**
 * Title:        Java Data Display (JDD): ImageDisplayPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For single image display of contents of a frame buffer from Data Acq. Server
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;

class ImageDisplayPanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ImageDisplayPanel.java,v 1.23 2004/10/04 19:49:25 varosi Exp $";
    int Ncolors = 256;
    int width = 320;
    int height = 240;
    int XLOC, YLOC;
    int startX , startY;
    int range;
    int zoomFactor =0;
    int min, max;
    int newMin, newMax;
    public int index;
    boolean reset = true;
    boolean zoom = false;
    public int pixels[] ;
    int ArrayPixels[][];
    public byte scaledPixels[];
    public String frameName = "";
    public Image img = null;
    ImageBuffer imageBuf;
    IndexColorModel colorModel;
    JComboBox zoomFacSelect;
    ZoomPanel zoomPanel;
    jddFrame frm;
    
    public ImageDisplayPanel( int index, jddFrame jddfrm, ImageBuffer img,
			      JComboBox zoomFacSelect, IndexColorModel colorModel, ZoomPanel zoomPanel )
    {
        this.index = index;
        this.colorModel = colorModel;
        this.imageBuf = img;
        this.zoomFacSelect = zoomFacSelect;
        this.zoomPanel = zoomPanel;
        this.frm = jddfrm;
        this.setMinimumSize(new Dimension(320, 240));
        
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(zoomFactor > 0) 
                    Zoom(evt);
            }
        });
        
        MyListener myListener = new MyListener();
        zoomFacSelect.addActionListener(myListener);
        
        // Create the Image pixel array
        pixels = new int[ width * height ];
        scaledPixels  = new byte[ width * height ];
        repaint();
    }
    
    public void scaleArray(int minv, int maxv) {
        float range;
        float f;
        range = maxv - minv;
        
        //System.out.println("Scaling to Min = " + minv + " Max = " + maxv);
        
        for(int i=0; i < pixels.length; i++) {
            f = (float)(pixels[i] - minv)/(float) range;
            if(f<0) {
                scaledPixels[i] = 0;
            }
            else if(f>1) {
                scaledPixels[i] = (byte)255;
            }
            else
                scaledPixels[i] = (byte)Math.round(f*255);
        }
    }
    
    public synchronized void createImage(ImageBuffer imgBuff) {
        imageBuf = imgBuff;
        
        if(imageBuf == null) {
            img = null;
        } else {
            max = imageBuf.max;
            min = imageBuf.min;
            pixels = imageBuf.pixels;
            scaleArray(min, max);
            img = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }
        // Redraw the image
        repaint();
    }
    
    public void updateImage( IndexColorModel icm ) {
        this.colorModel = icm;
        
        if(imageBuf == null) {
            img = null;
        } else {
            img = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }
        // Redraw the image
        repaint();
    }
    
    public void updateImage(int nmin, int nmax) {
        
        newMin = nmin;
        newMax = nmax;
        
        if(imageBuf == null) {
            img = null;
        } else {
            scaleArray(newMin, newMax);
            img = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }
        // Redraw the image
        repaint();
        
    }
    
    public void drawOriginalImage() {
        
        if(imageBuf == null) {
            img = null;
        } else {
            scaleArray(min, max);
            img = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }
        // Redraw the image
        repaint();
    }
    
    public void paintComponent( Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        
        if (img == null)
            g.drawString("Image not Available", 20, 20);
        else if( zoomFactor == 1) {
            g.drawImage(img, 0, 0, this);
        } else {
            
            if(reset) {
                g.drawImage(img, 0, 0, this);
            } else {
                if(frm.globalindex != index) {
                    g.drawImage(img, 0, 0, this);
                } else {
                    g.drawImage(img, 0, 0, this);
                    // Paint a rectangle with a translucent color
                    g.setColor(new Color(128, 255, 128, 56));
                    g.fillRect(XLOC -range/2, YLOC - range/2, range, range);
                    
                    // Paint a solid black rectangular outline
                    g.setColor(Color.BLACK);
                    g.drawRect(XLOC - range/2, YLOC - range/2, range, range);
                    zoom(startX, startY);
                }
            }
        }
    }
    
    private void Zoom(java.awt.event.MouseEvent evt) {
        
        ArrayPixels = new int[height][width];
        
        reset = false;
        frm.globalindex = index;
        
        if(img == null)
            return ;
        
        XLOC =evt.getX();
        YLOC = evt.getY();
        
        if (XLOC < range/2)
            XLOC = range/2;
        if (YLOC < range/2)
            YLOC = range/2;
        if ((width - XLOC) < range/2)
            XLOC = width - range/2 ;
        if ((height - YLOC) < range/2)
            YLOC = height - range/2 ;
        
        if ( zoomFactor > 1) {
            startX = XLOC - range/2 ;
            startY = YLOC - range/2 ;
            
            if((startX + range) > width)
                startX--;
            if((startY + range) > height)
                startY--;
            
            // Copy the original pixel into two dimensional array
            for ( int i=0, index1=0, index2=0; i < height; i++)
                for (int j=0; j < width; j++) {
                    ArrayPixels[i][j] = pixels[index2 ++];
                }

            zoomPanel.updateImage(ArrayPixels, colorModel, startX, startY, range, imageBuf.scaleFactor, zoomFactor);
            frm.colorBar.updateColorMap(frm.colorModel);
            ButtonModel bmodel = frm.histnormButton.getModel();
            bmodel.setSelected(false);
            frm.histnormButton.setText("Apply Histogram Eq");            
        }
        
        for(int i=0; i < frm.imgPanelArray.length; i++) {
            if(frm.imgPanelArray[i].imgDisplayPanel.index == frm.globalindex) { // Do nothing
            } else {
                frm.imgPanelArray[i].imgDisplayPanel.reset = true;
                frm.imgPanelArray[i].repaint();
            }
        }
        repaint();
    }
    
    private void zoom( int startX, int startY) {
        Image img;
        ArrayPixels = new int[height][width];
        
        if (XLOC < range/2)
            XLOC = range/2;
        if (YLOC < range/2)
            YLOC = range/2;
        if ((width - XLOC) < range/2)
            XLOC = width - range/2 ;
        if ((height - YLOC) < range/2)
            YLOC = height - range/2 ;
        
        startX = XLOC - range/2 ;
        startY = YLOC - range/2 ;
        
        if((startX + range) > width)
            startX--;
        if((startY + range) > height)
            startY--;
        
        // Copy the original pixel into two dimensional array
        for ( int i=0, index1=0, index2=0; i < height; i++)
            for (int j=0; j < width; j++) {       
                ArrayPixels[i][j] = pixels[index2 ++];
            }
 
        zoomPanel.updateImage(ArrayPixels, colorModel, startX, startY, range, imageBuf.scaleFactor, zoomFactor);
        frm.colorBar.updateColorMap(frm.colorModel);
        ButtonModel bmodel = frm.histnormButton.getModel();
        bmodel.setSelected(false);
        frm.histnormButton.setText("Apply Histogram Eq");
    }
    
    public void applyLogScale(float threshold) {
        
        double scaleFactor = Math.log(10);
        
        double temp_double[] = new double[height*width];
        
        int temp[] = new int[height*width];
        
        for ( int i=0; i < temp.length; i++) {
            if(pixels[i] < threshold)
                temp_double[i] = Math.log(threshold);
            else
                temp_double[i] = Math.log((double)pixels[i])/scaleFactor;
        }
        
        double mn = Math.log(threshold)/scaleFactor;
        double mx = Math.log((double)max)/scaleFactor;
        
        double range = mx - mn;
        
        System.out.println(" mn " + mn + " mx " + mx + " range " + range);
        
        for(int i=0; i < temp.length; i++) {
	    double f = (temp_double[i] - mn)/range;
            if(f<0) {
                temp[i] = 0;
            }
            else if(f>1) {
                temp[i] =255;
            }
            else
                temp[i] = Math.round((float)f*255);
        }
        
        img = createImage(new MemoryImageSource(width, height, colorModel, temp, 0, width));
        repaint();
    }
    
    
    class MyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            
            String comboitem = (String)zoomFacSelect.getSelectedItem();
            
            reset = true;
            
            if(comboitem.length() == 0 ) {
                zoomFactor = 1;
            } else {
                zoomFactor =  Integer.parseInt((String)zoomFacSelect.getSelectedItem());
                range = 420/zoomFactor;
            }
            repaint();
        }
    }
}
