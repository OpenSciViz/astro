package ufjdd;

/**
 * Title:        LineCutPanel.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh, Frank Varosi, Craig Warner
 * Company:      University of Florida
 * Description:  For ploting image data pixel values along a user selected line thru image in ZoomPanel.
 */

import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.geom.*;

public class LineCutPanel {

    public static final
	String rcsID = "$Name:  $ $Id: LineCutPanel.java,v 1.34 2004/10/05 15:56:00 warner Exp $";

    UFPlot ufp;
    
    public LineCutPanel() {
	ufp = new UFPlot();
    }
    
    public void update(int[][] data, int x1, int y1, int x2, int y2, int scalefactor) {
        int x_index, y_index;
	int temp;
	if (x2 < x1 && Math.abs(x2-x1) >= Math.abs(y2-y1)) {
	   temp = x1;
	   x1 = x2;
	   x2 = temp;
           temp = y1;
           y1 = y2;
           y2 = temp;
	} else if (y2 < y1 && Math.abs(y2-y1) > Math.abs(x2-x1)) {
           temp = x1;
           x1 = x2;
           x2 = temp;
	   temp = y1;
	   y1 = y2;
	   y2 = temp;
	}
        int x_range = x2 - x1;
        int y_range = y2 - y1; 
        int range;
        
        if (x_range < y_range) {
            range = y_range;
        } else {
            range = x_range;
        }
        
        float D;
        float slope = (float) (y2 -y1)/(x2-x1);
        float theta = (float)Math.atan(slope );
        double new_x, new_y;
        
	float[] x_values = new float[range];
	float[] y_values = new float[range];
        
        System.out.println("(x1,y1) = (" + x1 + "," + y1 + ") : (x2,y2) = ( " + x2 + "," + y2 + ")");
        System.out.println("slope = " + slope + " theta = " + theta);

	if (x_range >= y_range) { 

	   for(int i=0; i < range; i++) {
	      x_index = x1 + i;
              y_index =(int)Math.round(y1 + slope*(float)i);
              D = (float)Math.sqrt(Math.pow((double)(x_index-x1), 2.0) + Math.pow((double)(y_index-y1), 2.0));
	
	      x_values[i] = (float)D;
	      y_values[i] = (float)data[y_index][x_index]/scalefactor;
	   }
	} else {
           for(int i=0; i < range; i++) {
              y_index = y1 + i;
              if (x_range != 0) x_index =(int)Math.round(x1 + (float)i/slope);
	      else x_index = x1;
              D = (float)Math.sqrt(Math.pow((double)(x_index-x1), 2.0) + Math.pow((double)(y_index-y1), 2.0));

              x_values[i] = (float)D;
              y_values[i] = (float)data[y_index][x_index]/scalefactor;
           }
	}

	ufp.plot(x_values, y_values, "*xtitle=Pixel, *ytitle=Data, *title=Line Cut, *xminval=0, *psym=-4");
    }
    
    private void exitForm(java.awt.event.WindowEvent evt) {
        System.exit(0);
    }
}
