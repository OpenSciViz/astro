/*
 * CoordinatePanel.java
 *
 * Created on April 30, 2004, 1:24 AM
 */

package ufjdd;

/**
 *
 * @author  ziad
 */
import javax.swing.*;
import java.awt.*;

public class CoordinatePanel extends JPanel {
    
    JLabel X_Label;
    JLabel Y_Label;
    JLabel Data_Label;
    
    public void CoordinatePanel() {
        
        super.setLayout(new GridLayout(1,3));
        
        X_Label = new JLabel("X : ");
        Y_Label = new JLabel("Y : ");
        Data_Label = new JLabel("Data Value : ");
        add(X_Label);
        add(Y_Label);
        add(Data_Label);
    }
    
    public void setLabels( int x, int y, int data) {
        
        X_Label.setText("X : " + x);
        Y_Label.setText("Y : " + y);
        Data_Label.setText("Data Value : " + data);
        
    }
}
