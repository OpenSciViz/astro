package ufjdd;

/**
 * Title:        Java Data Display  (JDD) : ColorMapDialog.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For interactively adjusting the index color mapping.
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*; 
import java.util.*;

public class ColorMapDialog extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: ColorMapDialog.java,v 1.7 2004/10/05 23:08:55 varosi Exp $";

    // Contains the color map attributes
    byte[] r ;
    byte[] b ;
    byte[] g ;
    public IndexColorModel colorModel;
    final int Ncolors = 256;
    double[] minSlopeValues;
    double[] maxSlopeValues;
    double[] gammaValues;
    final int NSteps = 200;
    JSlider minColorsSlopeSliders[];
    JSlider maxColorsSlopeSliders[];
    JSlider gammaValuesSliders[];
    JPanel[] colorPanels;
    
    jddFrame jddframe;

    /** Creates a new instance of ColorMapDialog */

    public ColorMapDialog(jddFrame jddframe) {
        this.jddframe = jddframe;
        this.getContentPane().setLayout(null);
        this.setSize(600,  300);
        String colorNames[] = {"Red", "Green", "Blue"};
        String gammaColors[] = {"Red", "Green", "Blue"};
        minSlopeValues = new double[3];
        maxSlopeValues = new double[3];
        gammaValues = new double[3];
        colorPanels = new JPanel[3];
	minColorsSlopeSliders = new JSlider[3];

        // Create the JSlider for color map components

        for(int i=0; i<minColorsSlopeSliders.length; i++) {
            minColorsSlopeSliders[i] = new JSlider(JSlider.VERTICAL);
            minColorsSlopeSliders[i].setName(colorNames[i] + "" + i);
            minColorsSlopeSliders[i].setBorder(BorderFactory.createTitledBorder("min"));
            minColorsSlopeSliders[i].setMinimum(0);
            minColorsSlopeSliders[i].setMaximum(200);
            minColorsSlopeSliders[i].setValue(0);
            minColorsSlopeSliders[i].setPaintTicks(true);
            minSlopeValues[i] = 0.0;
            minColorsSlopeSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    int sliderValue = tempSlider.getValue();
                    // Scale the color slope to a value between 0 & 1
                    double slope = sliderValue/200.0;
                    minSlopeValues[index] = slope;
                    updatejddFrameColorMap();
                }
            });
        }
        
        maxColorsSlopeSliders = new JSlider[3];
        
        // Create the JSlider for color map components

        for(int i=0; i<maxColorsSlopeSliders.length; i++) {
            maxColorsSlopeSliders[i] = new JSlider(JSlider.VERTICAL);
            maxColorsSlopeSliders[i].setName(colorNames[i] + "" + i);
            maxColorsSlopeSliders[i].setBorder(BorderFactory.createTitledBorder("max"));
            maxColorsSlopeSliders[i].setMinimum(0);
            maxColorsSlopeSliders[i].setMaximum(200);
            maxColorsSlopeSliders[i].setValue(200);
            maxColorsSlopeSliders[i].setPaintTicks(true);
            maxSlopeValues[i] = 1.0;
            maxColorsSlopeSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    int sliderValue = tempSlider.getValue();
                    // Scale the color slope to a value between 0 & 1
                    double slope = sliderValue/200.0;
                    maxSlopeValues[index] = slope;
                    updatejddFrameColorMap();
                }
            });
        }
        
        gammaValuesSliders = new JSlider[3];
        
        // Create the JSlider for color map components

        for(int i=0; i<gammaValuesSliders.length; i++) {
            gammaValuesSliders[i] = new JSlider(JSlider.VERTICAL);
            gammaValuesSliders[i].setName(gammaColors[i] + "" + i);
            gammaValuesSliders[i].setBorder(BorderFactory.createTitledBorder("gamma"));
            gammaValuesSliders[i].setMinimum(-100);
            gammaValuesSliders[i].setMaximum(100);
            gammaValuesSliders[i].setValue(0);
            gammaValuesSliders[i].setPaintTicks(true);
            gammaValues[i] = 1.0;
            gammaValuesSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    int sliderValue = tempSlider.getValue();
                    // Scale the gamma value between -1 & 1
                    double gamma = sliderValue/100.0;
                    gammaValues[index] = Math.pow(10, gamma);
                    updatejddFrameColorMap();
                }
            });
        }
        
        for(int i=0; i < colorPanels.length; i++) {
            colorPanels[i] = new JPanel();
            colorPanels[i].setLayout(new FlowLayout());
            colorPanels[i].setSize(150, 200);
            colorPanels[i].setBorder(BorderFactory.createTitledBorder(colorNames[i]));
            colorPanels[i].add(minColorsSlopeSliders[i]);
            colorPanels[i].add(maxColorsSlopeSliders[i]);
            colorPanels[i].add(gammaValuesSliders[i]);
            colorPanels[i].setBounds( i*200, 0, 150, 250 );
            getContentPane().add(colorPanels[i]);
        }
        
        colorModel = generateColorModel(minSlopeValues, maxSlopeValues, gammaValues);
        
        this.setLocation(5, 5);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }
    
    private void updatejddFrameColorMap() {
        colorModel = generateColorModel(minSlopeValues, maxSlopeValues, gammaValues);
        jddframe.changeColorModel(colorModel);
    }
    
    private IndexColorModel generateColorModel(double[] minVals, double[] maxVals, double[] gamma) {
        // Generate 256-color model
        
        double r_slope = maxVals[0] - minVals[0];
        double g_slope = maxVals[1] - minVals[1];
        double b_slope = maxVals[2] - minVals[2];
        
        double r_min = minVals[0];
        double g_min = minVals[1];
        double b_min = minVals[2];
        
        double r_gamma = gamma[0];
        double g_gamma = gamma[1];
        double b_gamma = gamma[2];
        
        r = new byte[Ncolors];
        g = new byte[Ncolors];
        b = new byte[Ncolors];
        
        //System.out.println("r_min = " + r_min + " g_min = " + g_min + " b_min = " + b_min);
        //System.out.println("r_slope = " + r_slope + " g_slope = " + g_slope + " b_slope = " + b_slope);
        //System.out.println("r_gamma = " + r_gamma + " g_gamma = " + g_gamma + " b_gamma = " + b_gamma);
        
        // Set the default color model to grey
        for(int i=0; i < Ncolors; i++) {
            r[i] = (byte) Math.round((double)Ncolors*(r_min + r_slope*Math.pow((double)i/Ncolors,r_gamma)));
            g[i] = (byte) Math.round((double)Ncolors*(g_min + g_slope*Math.pow((double)i/Ncolors,g_gamma)));
            b[i] = (byte) Math.round((double)Ncolors*(b_min + b_slope*Math.pow((double)i/Ncolors,b_gamma)));
        }
        
        int bits = (int) Math.ceil(Math.log(Ncolors) / Math.log(2));
        return new IndexColorModel(bits, Ncolors, r, g, b);
        
    }

    public IndexColorModel normColorModel(int [] normHist) {
        
        byte[] r_norm = new byte[256] ;
        byte[] b_norm = new byte[256] ;
        byte[] g_norm = new byte[256] ;
        
        for(int i=0; i < normHist.length; i++) {
            r_norm[i] = r[normHist[i]];
            b_norm[i] = b[normHist[i]];
            g_norm[i] = g[normHist[i]];
        }
        
        int bits = (int) Math.ceil(Math.log(Ncolors) / Math.log(2));
        return new IndexColorModel(bits, Ncolors, r_norm, g_norm, b_norm);
    }
}
