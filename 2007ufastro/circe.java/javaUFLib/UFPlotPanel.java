package javaUFLib;

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;

public class UFPlotPanel extends javax.swing.JPanel implements ActionListener {
   final int xdim, ydim;
   int pxdim1, pxdim2, pydim1, pydim2;
   Graphics2D offscreenG;
   Image offscreenImg;
   float xmin, xmax, ymin, ymax, xscale, yscale, xoff, yoff, xstep, ystep;
   float xtickinterval, ytickinterval, binsize, minhist, maxhist;
   float[] xrange = {-1, -1};
   float[] yrange = {-1, -1};
   int xticks = -1, yticks = -1, charSize = 12, xcharSize, ycharSize;
   int xminor, yminor, psym, symsize, xticklen, yticklen, nbins;
   int xpos1 = 60, ypos1 = 25, xpos2, ypos2; 
   float[] xtickv, ytickv;
   int[] usymxs, usymys, multi={0, 1, 1};
   String[] xtickname, ytickname;
   String fontName = "Dialog", xfontName, yfontName, title, xtitle, ytitle;
   Font mainFont, xFont, yFont;
   Color plotColor, backColor, oplotColor;
   boolean noData = false, xlog = false, ylog = false;
   boolean doHist = false, fillHist = false;
   JPopupMenu menu;
   JMenuItem printItem, quitItem;
   final UFPlot ufp;

   public UFPlotPanel(int xdim, int ydim, final UFPlot ufp) {
      this.xdim = xdim;
      this.ydim = ydim;
      this.ufp = ufp;
      this.xpos2 = xdim - 20;
      this.ypos2 = ydim - 47;
      this.setBackground(Color.black);
      this.setForeground(Color.white);
      this.setPreferredSize(new Dimension(xdim, ydim));
      menu = new JPopupMenu();
      printItem = new JMenuItem("Print or Save");
      menu.add(printItem);
      printItem.addActionListener(this);
      quitItem = new JMenuItem("Quit");
      menu.add(quitItem);
      quitItem.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   ufp.dispose();
	}
      });

      addMouseListener(new java.awt.event.MouseListener() {
	public void mouseClicked(java.awt.event.MouseEvent evt) {
        }

        public void mousePressed(java.awt.event.MouseEvent ev) {
           if ((ev.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
              if(ev.isPopupTrigger()) {
		menu.show(ev.getComponent(), ev.getX(), ev.getY());
              }
           }
	}

        public void mouseReleased(java.awt.event.MouseEvent evt) {
	}

        public void mouseEntered(java.awt.event.MouseEvent evt) {
        }

        public void mouseExited(java.awt.event.MouseEvent evt) {
        }
      });
   }

   public void initPlot() {
      offscreenImg = createImage(xdim,ydim);
      offscreenG = (Graphics2D)offscreenImg.getGraphics();
      offscreenG.setColor(backColor);
      offscreenG.fillRect(0,0,xdim,ydim);
      offscreenG.setColor(plotColor);
      offscreenG.setFont(mainFont);
   }

   public void parseParams(String s) {
      int n;
      int m;

      n = s.indexOf("*xlog");
      if (n != -1) xlog = true;
      n = s.indexOf("*ylog");
      if (n != -1) ylog = true;

      n = s.indexOf("*xrange");
      if (n != -1) {
	n = s.indexOf("[",n);
	m = s.indexOf(",",n);
	xrange[0] = Float.parseFloat(s.substring(n+1, m));
	if (xrange[0] <= 0 && xlog) xrange[0] = 1.0e-10f;
	n = s.indexOf("]",m);
	xrange[1] = Float.parseFloat(s.substring(m+1, n));
      } else {
	if (xlog) {
	   xrange[0] = (float)Math.pow(10, Math.floor(Math.log(Math.max(xmin, 1.0e-10))/Math.log(10)));
	   xrange[1] = (float)Math.pow(10, 1+Math.floor(Math.log(xmax)/Math.log(10)));
	} else {
           xrange[0] = (float)(Math.floor(xmin/Math.pow(10, Math.floor(Math.log(xmax)/Math.log(10)))-1)*Math.pow(10, Math.floor(Math.log(xmax)/Math.log(10))));
           xrange[1] = (float)(Math.floor(1+xmax/Math.pow(10, Math.floor(Math.log(xmax)/Math.log(10))))*Math.pow(10, Math.floor(Math.log(xmax)/Math.log(10))));
	}
	if (doHist) xrange[0] = 0;
      }
      n = s.indexOf("*yrange");
      if (n != -1) {
        n = s.indexOf("[",n);
        m = s.indexOf(",",n);
        yrange[0] = Float.parseFloat(s.substring(n+1, m));
	if (yrange[0] <= 0 && ylog) yrange[0] = 1.0e-10f;
        n = s.indexOf("]",m);
        yrange[1] = Float.parseFloat(s.substring(m+1, n));
      } else {
        if (ylog) {
           yrange[0] = (float)Math.pow(10, Math.floor(Math.log(Math.max(ymin, 1.0e-10))/Math.log(10)));
           yrange[1] = (float)Math.pow(10, 1+Math.floor(Math.log(ymax)/Math.log(10)));
        } else {
           yrange[0] = (float)(Math.floor(ymin/Math.pow(10, Math.floor(Math.log(ymax)/Math.log(10)))-1)*Math.pow(10, Math.floor(Math.log(ymax)/Math.log(10))));
           yrange[1] = (float)(Math.floor(1+ymax/Math.pow(10, Math.floor(Math.log(ymax)/Math.log(10))))*Math.pow(10, Math.floor(Math.log(ymax)/Math.log(10))));
	}
      }

      n = s.indexOf("*xminval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xrange[0] = Float.parseFloat(s.substring(n+1,m));
      }

      n = s.indexOf("*xmaxval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xrange[1] = Float.parseFloat(s.substring(n+1,m));
      }

      n = s.indexOf("*yminval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yrange[0] = Float.parseFloat(s.substring(n+1,m));
      }

      n = s.indexOf("*ymaxval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yrange[1] = Float.parseFloat(s.substring(n+1,m));
      }

      xstep=(float)Math.pow(10, Math.floor(Math.log(xrange[1])/Math.log(10))); 
      ystep=(float)Math.pow(10, Math.floor(Math.log(yrange[1])/Math.log(10)));
      n = s.indexOf("*xticks");
      if (n != -1) {
	n = s.indexOf("=", n);
	m = s.indexOf(",", n);
	if (m < 0) m = s.length();
	xticks = (int)Float.parseFloat(s.substring(n+1,m));
	if (xticks < 2) xticks = 2;
      } else {
	if (xlog) xticks = (int)Math.ceil(Math.log(xrange[1])/Math.log(10.0)) - (int)Math.floor(Math.log(xrange[0])/Math.log(10.0)) + 1;
	else xticks = (int)Math.floor((xrange[1]-xrange[0])/xstep+1);
      }
      n = s.indexOf("*yticks");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yticks = (int)Float.parseFloat(s.substring(n+1,m));
	if (yticks < 2) yticks = 2;
      } else {
	if (ylog) yticks = (int)Math.ceil(Math.log(yrange[1])/Math.log(10.0)) - (int)Math.floor(Math.log(yrange[0])/Math.log(10.0)) + 1;
	else yticks = (int)Math.floor((yrange[1]-yrange[0])/ystep+1);
      }

      if (doHist) xstep = nbins/xticks;
      xtickv = new float[xticks];
      ytickv = new float[yticks];
      n = s.indexOf("*xtickv");
      if (n != -1) {
	n = s.indexOf("[", n);
	m = s.indexOf(",",n);
	int b = 0;
	while (b < xticks && m > -1 && m < s.indexOf("]", n)) {
	   xtickv[b] = Float.parseFloat(s.substring(n+1,m));
	   b++;
	   n = m; 
	   m = s.indexOf(",", n+1);
	}
	if (b < xticks) {
	   m = s.indexOf("]", n);
	   xtickv[b] = Float.parseFloat(s.substring(n+1, m));
	}
      } else {
	if (xlog) {
	   int ax = (int)Math.floor(Math.log(xrange[0])/Math.log(10.0));
	   for (int j = ax; j <= (int)Math.ceil(Math.log(xrange[1])/Math.log(10.0)); j++) xtickv[j-ax] = (float)Math.pow(10.0,j);
	} else {
	   if (xrange[0] % xstep == 0) xtickv[0] = xrange[0];
	   else xtickv[0] = (float)Math.floor(xrange[0]/xstep + 1)*xstep;
	   for (int j = 1; j < xticks; j++) xtickv[j] = xtickv[0] + j * xstep;
	}
      }
      n = s.indexOf("*ytickv");
      if (n != -1) {
        n = s.indexOf("[", n);
        m = s.indexOf(",",n);
        int b = 0;
        while (b < xticks && m > -1 && m < s.indexOf("]", n)) {
           ytickv[b] = Float.parseFloat(s.substring(n+1,m));
           b++;
           n = m;
           m = s.indexOf(",", n+1);
        }
        if (b < yticks) {
           m = s.indexOf("]", n);
           ytickv[b] = Float.parseFloat(s.substring(n+1, m));
        }
      } else {
	if (ylog) {
	   int ay = (int)Math.floor(Math.log(yrange[0])/Math.log(10.0));
	   for (int j = ay; j <= (int)Math.ceil(Math.log(yrange[1])/Math.log(10.0)); j++) ytickv[j-ay] = (float)Math.pow(10.0, j); 
        } else {
           if (yrange[0] % ystep == 0) ytickv[0] = yrange[0];
           else ytickv[0] = (float)Math.floor(yrange[0]/ystep + 1)*ystep;
           for (int j = 1; j < yticks; j++) ytickv[j] = ytickv[0] + j * ystep;
	}
      }

      n = s.indexOf("*xtickinterval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xtickinterval = Float.parseFloat(s.substring(n+1,m));
        float xtimin =xtickinterval*(float)Math.floor(xrange[0]/xtickinterval);
        float xtimax =xtickinterval*(float)Math.ceil(xrange[1]/xtickinterval);
        xticks = (int)((xtimax-xtimin)/xtickinterval)+1;
        xtickv = new float[xticks];
        for (int j = 0; j < xticks; j++)
           xtickv[j] = xtimin + j*xtickinterval;
      }

      n = s.indexOf("*ytickinterval");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        ytickinterval = Float.parseFloat(s.substring(n+1,m));
        float ytimin =ytickinterval*(float)Math.floor(yrange[0]/ytickinterval);
        float ytimax =ytickinterval*(float)Math.ceil(yrange[1]/ytickinterval);
        yticks = (int)((ytimax-ytimin)/ytickinterval)+1;
        ytickv = new float[yticks];
        for (int j = 0; j < yticks; j++)
           ytickv[j] = ytimin + j*ytickinterval;
      }

      xtickname = new String[xticks];
      ytickname = new String[yticks];
      n = s.indexOf("*xtickname");
      if (n != -1) {
        n = s.indexOf("[", n);
        m = s.indexOf(",",n);
        int b = 0;
        while (b < xticks && m > -1 && m < s.indexOf("]", n)) {
           xtickname[b] = s.substring(n+1,m);
           b++;
           n = m;
           m = s.indexOf(",", n+1);
        }
        if (b < xticks) {
           m = s.indexOf("]", n);
           xtickname[b] = s.substring(n+1, m);
        }
      } else {
        for (int j = 0; j < xticks; j++) {
	   if (doHist) {
	      xtickname[j] = "" + (j*binsize*xstep+minhist);
	      if (xtickname[j].indexOf('.') < xtickname[j].length()-3)
		xtickname[j] = xtickname[j].substring(0, xtickname[j].indexOf('.')+3);
	   }
	   else xtickname[j] = ""+xtickv[j];
	}
      }
      n = s.indexOf("*ytickname");
      if (n != -1) {
        n = s.indexOf("[", n);
        m = s.indexOf(",",n);
        int b = 0;
        while (b < yticks && m > -1 && m < s.indexOf("]", n)) {
           ytickname[b] = s.substring(n+1,m);
           b++;
           n = m;
           m = s.indexOf(",", n+1);
        }
        if (b < yticks) {
           m = s.indexOf("]", n);
           ytickname[b] = s.substring(n+1, m);
        }
      } else {
        for (int j = 0; j < yticks; j++) ytickname[j] = ""+ytickv[j];
      }

      n = s.indexOf("*xminor");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xminor = (int)Float.parseFloat(s.substring(n+1,m));
      } else {
	if (xlog) xminor = 1;
	else if (xstep % 6 == 0) xminor = 6;
	else if (xstep % 5 == 0) xminor = 5;
	else if (xstep % 4 == 0) xminor = 4;
	else if (xstep % 3 == 0) xminor = 3;
	else if (xstep % 2 == 0) xminor = 2;
	else xminor = 5;
      } 
      n = s.indexOf("*yminor");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yminor = (int)Float.parseFloat(s.substring(n+1,m));
      } else {
	if (ylog) yminor = 1;
        else if (ystep % 6 == 0) yminor = 6;
        else if (ystep % 5 == 0) yminor = 5;
        else if (ystep % 4 == 0) yminor = 4;
        else if (ystep % 3 == 0) yminor = 3;
        else if (ystep % 2 == 0) yminor = 2;
        else yminor = 5;
      } 

      n = s.indexOf("*xticklen");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
	xticklen = (int)Float.parseFloat(s.substring(n+1,m));
      } else xticklen = 10;

      n = s.indexOf("*yticklen");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yticklen = (int)Float.parseFloat(s.substring(n+1,m));
      } else yticklen = 10;

      n = s.indexOf("*charsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        charSize = (int)Float.parseFloat(s.substring(n+1,m));
      }
      n = s.indexOf("*font");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
	fontName = s.substring(n+1, m);
      }
      n = s.indexOf("*xcharsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xcharSize = (int)Float.parseFloat(s.substring(n+1,m));
      } else xcharSize = charSize;
      n = s.indexOf("*xfont");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xfontName = s.substring(n+1, m);
      } else xfontName = fontName;
      n = s.indexOf("*ycharsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        ycharSize = (int)Float.parseFloat(s.substring(n+1,m));
      } else ycharSize = charSize;
      n = s.indexOf("*yfont");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        yfontName = s.substring(n+1, m);
      } else yfontName = fontName;
      mainFont = new Font(fontName, 0, charSize);
      xFont = new Font(xfontName, 0, xcharSize);
      yFont = new Font(yfontName, 0, ycharSize);

      n = s.indexOf("*title");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        title = s.substring(n+1, m);
      } else title = "";
      n = s.indexOf("*xtitle");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xtitle = s.substring(n+1, m);
      } else xtitle = "";
      n = s.indexOf("*ytitle");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        ytitle = s.substring(n+1, m);
      } else ytitle = "";

      n = s.indexOf("*color");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        int red = (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf(",",m+1);
        int green = (int)Float.parseFloat(s.substring(m+1, n));
	m = s.indexOf(",", n+1);
	if (m < 0) m = s.length();
	int blue = (int)Float.parseFloat(s.substring(n+1, m));
	plotColor = new Color(red, green, blue);
      } else plotColor = new Color(0, 0, 0);

      n = s.indexOf("*background");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        int red = (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf(",",m+1);
        int green = (int)Float.parseFloat(s.substring(m+1, n));
        m = s.indexOf(",", n+1);
        if (m < 0) m = s.length();
        int blue = (int)Float.parseFloat(s.substring(n+1, m));
        backColor = new Color(red, green, blue);
      } else backColor = new Color(255, 255, 255);

      n = s.indexOf("*nodata");
      if (n != -1) noData = true;

      n = s.indexOf("*psym");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
	psym = (int)Float.parseFloat(s.substring(n+1, m));
      } else psym = 0;

      n = s.indexOf("*symsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
	symsize = (int)Float.parseFloat(s.substring(n+1, m));
      } else symsize = 5;

      n = s.indexOf("*xmargin");
      if (n != -1) {
        n = s.indexOf("[",n);
        m = s.indexOf(",",n);
        xpos1 = pxdim1 + (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf("]",m);
        xpos2 = pxdim2 - (int)Float.parseFloat(s.substring(m+1, n));
      }

      n = s.indexOf("*ymargin");
      if (n != -1) {
        n = s.indexOf("[",n);
        m = s.indexOf(",",n);
        ypos1 = pydim1 + (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf("]",m);
        ypos2 = pydim2 - (int)Float.parseFloat(s.substring(m+1, n));
      }

      n = s.indexOf("*position");
      if (n != -1) {
        n = s.indexOf("[",n);
        m = s.indexOf(",",n);
        xpos1 = (int)(pxdim1+(pxdim2-pxdim1)*Float.parseFloat(s.substring(n+1, m)));
        n = s.indexOf(",",m+1);
        ypos1 = (int)(pydim1+(pydim2-pydim1)*Float.parseFloat(s.substring(m+1, n)));
        m = s.indexOf(",", n+1);
	xpos2 = (int)(pxdim1+(pxdim2-pxdim1)*Float.parseFloat(s.substring(n+1, m)));
	n = s.indexOf("]", m+1);
	ypos2 = (int)(pydim1+(pydim2-pydim1)*Float.parseFloat(s.substring(m+1, n)));
      }
   }

   public void drawAxes(float[] x, float[] y, String s) {
      xmin = UFArrayOps.minValue(x);
      xmax = UFArrayOps.maxValue(x);
      ymin = UFArrayOps.minValue(y);
      ymax = UFArrayOps.maxValue(y);
      pxdim1 = (int)((xdim/multi[1]) * (multi[0] % multi[1]));
      pxdim2 = (int)((xdim/multi[1]) * (1 + multi[0] % multi[1]));
      pydim1 = (int)((ydim/multi[2]) * (multi[0] / multi[2]));
      pydim2 = (int)((ydim/multi[2]) * (1 + multi[0] / multi[2]));
      xpos1 = pxdim1 + 60;
      xpos2 = pxdim2 - 20;
      ypos1 = pydim1 + 25;
      ypos2 = pydim2 - 47;
      this.parseParams(s);
      if (multi[0] == 0) this.initPlot();
      if (!xlog) xscale = (xpos2-xpos1)/(xrange[1]-xrange[0]);
      if (!ylog) yscale = (ypos2-ypos1)/(yrange[1]-yrange[0]);
      if (!xlog) xoff = xpos1-(xscale*xrange[0]);
      if (!ylog) yoff = ypos2+(yscale*yrange[0]);
      if (xlog) x = this.doXlog(x);
      if (ylog) y = this.doYlog(y);
      int[] xaxis = {xpos1, xpos1, xpos2, xpos2, xpos1};
      int[] yaxis = {ypos1, ypos2, ypos2, ypos1, ypos1};
      offscreenG.drawPolyline(xaxis, yaxis, 5);
      this.drawTicks();
      offscreenG.setFont(xFont);
      offscreenG.drawString(xtitle, (int)((xpos2+xpos1)/2)-(int)(xtitle.length()-1)*4, ypos2+35);
      offscreenG.setFont(yFont);
      for (int j = 0; j < ytitle.length(); j++) {
	offscreenG.drawString(ytitle.substring(j, j+1), xpos1-45, (int)((ypos2+ypos1)/2)-(int)(ytitle.length()-1)*4+j*12);
      }
      offscreenG.setFont(mainFont);
      offscreenG.drawString(title, (int)((xpos2+xpos1)/2)-(int)(title.length()-1)*4, ypos1-7);
      repaint();
   }

   public float[] doXlog(float[] x) {
      xscale = (xpos2-xpos1)/(float)(Math.log(xrange[1])/Math.log(10.0)-Math.log(xrange[0])/Math.log(10.0));
      xoff = xpos1-(xscale*(float)(Math.log(xrange[0])/Math.log(10.0)));
      for (int j = 0; j < xtickv.length; j++) {
	xtickv[j] = (float)(Math.log(xtickv[j])/Math.log(10.0));
      }
      for (int j = 0; j < x.length; j++) {
	x[j] = (float)(Math.log(x[j])/Math.log(10.0));
      }
      return x;
   }

   public float[] doYlog(float[] y) {
      yscale = (ypos2-ypos1)/(float)(Math.log(yrange[1])/Math.log(10.0)-Math.log(yrange[0])/Math.log(10.0));
      yoff = ypos2+(yscale*(float)(Math.log(yrange[0])/Math.log(10.0)));
      for (int j = 0; j < ytickv.length; j++) {
        ytickv[j] = (float)(Math.log(ytickv[j])/Math.log(10.0));
      }
      for (int j = 0; j < y.length; j++) {
        y[j] = (float)(Math.log(y[j])/Math.log(10.0));
      }
      return y;
   }

   public void drawTicks() {
      int tickpos, minorpos, lastpos;
      offscreenG.setFont(xFont);
      lastpos = (int)Math.floor(xoff+xscale*(2*xtickv[0]-xtickv[1])+0.5);
      for (int j = 0; j <= xticks; j++) {
	if (j < xticks) tickpos = (int)Math.floor(xoff+xscale*xtickv[j]+0.5);
	else tickpos = (int)Math.floor(xoff+xscale*(2*xtickv[j-1]-xtickv[j-2])+0.5);
	if (tickpos <= xpos2 && tickpos >= xpos1 && j < xticks) {
	   offscreenG.drawLine(tickpos, ypos2, tickpos, ypos2-xticklen);
	   offscreenG.drawLine(tickpos, ypos1, tickpos, ypos1+xticklen);
	   offscreenG.drawString(xtickname[j], tickpos-(int)(xtickname[j].length()-1)*4, ypos2+15);
	}
	for (int l = 0; l < xminor; l++) {
	   minorpos = (int)Math.floor(lastpos+l*(tickpos-lastpos)/xminor+0.5);
	   if (minorpos <= xpos2 && minorpos >= xpos1) {
	      offscreenG.drawLine(minorpos, ypos2, minorpos, ypos2-xticklen/2);
	      offscreenG.drawLine(minorpos, ypos1, minorpos, ypos1+xticklen/2);
	   }	
	}
	lastpos = tickpos;
      }
      if (xlog && Math.log(xrange[0])/Math.log(10.0) != xtickv[0]) offscreenG.drawString(""+xrange[0], xpos1-(int)((""+xrange[0]).length()-1)*4, ypos2+15);
      if (!xlog && xrange[0] != xtickv[0]) offscreenG.drawString(""+xrange[0], xpos1-(int)((""+xrange[0]).length()-1)*4, ypos2+15);
      if (!doHist && xlog && (float)Math.log(xrange[1])/Math.log(10.0) != xtickv[xticks-1]) offscreenG.drawString(""+xrange[1], xpos2-(int)((""+xrange[1]).length()-1)*4, ypos2+15);
      if (!doHist && !xlog && xrange[1] != xtickv[xticks-1]) offscreenG.drawString(""+xrange[1], xpos2-(int)((""+xrange[1]).length()-1)*4, ypos2+15);
      if (doHist) offscreenG.drawString(""+maxhist, xpos2-(int)((""+xrange[1]).length()-1)*4, ypos2+15);

      offscreenG.setFont(yFont);
      lastpos = (int)Math.floor(yoff-(yscale*(2*ytickv[0]-ytickv[1])-0.5));
      for (int j = 0; j <= yticks; j++) {
        if (j < yticks) tickpos = (int)Math.floor(yoff-(yscale*ytickv[j]-0.5));
	else tickpos = (int)Math.floor(yoff-(yscale*(2*ytickv[j-1]-ytickv[j-2])-0.5));
        if (tickpos <= ypos2 && tickpos >= ypos1 && j < yticks) {
           offscreenG.drawLine(xpos1, tickpos, xpos1+yticklen, tickpos);
	   offscreenG.drawLine(xpos2, tickpos, xpos2-yticklen, tickpos);
	   offscreenG.drawString(ytickname[j], xpos1-(int)ytickname[j].length()*8, tickpos+4);
	}
        for (int l = 0; l < yminor; l++) {
	   minorpos = (int)Math.floor(lastpos+l*(tickpos-lastpos)/yminor+0.5);
           if (minorpos <= ypos2 && minorpos >= ypos1) {
	      offscreenG.drawLine(xpos2, minorpos, xpos2-yticklen/2, minorpos);
              offscreenG.drawLine(xpos1, minorpos, xpos1+yticklen/2, minorpos);
           }
        }
	lastpos = tickpos;
      }
      if (ylog && Math.log(yrange[0])/Math.log(10.0) != ytickv[0]) offscreenG.drawString(""+yrange[0], xpos1-(int)(""+yrange[0]).length()*8, ypos2);
      if (!ylog && yrange[0] != ytickv[0]) offscreenG.drawString(""+yrange[0], xpos1-(int)(""+yrange[0]).length()*8, ypos2);
      if (ylog && (float)Math.log(yrange[1])/Math.log(10.0) != ytickv[yticks-1]) offscreenG.drawString(""+yrange[1], xpos1-(int)(""+yrange[1]).length()*8, ypos1+4);
      if (!ylog && yrange[1] != ytickv[yticks-1]) offscreenG.drawString(""+yrange[1], xpos1-(int)(""+yrange[1]).length()*8, ypos1+4);
      offscreenG.setFont(mainFont);
   }

   public void plot(float[] x, float[] y, String s) {
      this.drawAxes(x, y, s);
      multi[0]++;
      if (multi[0] >= multi[1]*multi[2]) multi[0] = 0;
      if (!noData) this.makePlot(x, y);
   }

   public void makePlot(float[] x, float[] y) {
      int[] drawx = new int[x.length];
      int[] drawy = new int[x.length];
      offscreenG.setClip(xpos1, ypos1, xpos2-xpos1, ypos2-ypos1);
      for (int j = 0; j < x.length; j++) {
	drawx[j] = (int)Math.floor(xoff+xscale*x[j]+0.5);
	drawy[j] = (int)Math.floor(yoff-(yscale*y[j])+0.5);
	switch(Math.abs(psym) % 10) {
	   case 1:
	      offscreenG.drawLine(drawx[j], drawy[j]-symsize, drawx[j], drawy[j]+symsize);
	      offscreenG.drawLine(drawx[j]-symsize, drawy[j], drawx[j]+symsize, drawy[j]);
	      break;
	   case 2:
              offscreenG.drawLine(drawx[j], drawy[j]-symsize, drawx[j], drawy[j]+symsize);
              offscreenG.drawLine(drawx[j]-symsize, drawy[j], drawx[j]+symsize, drawy[j]);
              offscreenG.drawLine(drawx[j]-symsize, drawy[j]-symsize, drawx[j]+symsize, drawy[j]+symsize);
              offscreenG.drawLine(drawx[j]+symsize, drawy[j]-symsize, drawx[j]-symsize, drawy[j]+symsize);
	      break;
	   case 3:
	      offscreenG.drawLine(drawx[j], drawy[j], drawx[j], drawy[j]);
	      break;
	   case 4:
	      int[] symxs4 = {drawx[j], drawx[j]+symsize, drawx[j], drawx[j]-symsize, drawx[j]};
	      int[] symys4 = {drawy[j]-symsize, drawy[j], drawy[j]+symsize, drawy[j], drawy[j]-symsize};
	      if (Math.abs(psym)==4) offscreenG.drawPolygon(symxs4,symys4,5);
	      if (Math.abs(psym)==14) offscreenG.fillPolygon(symxs4,symys4,5);
	      break;
	   case 5:
	      int[] symxs5 = {drawx[j], drawx[j]+symsize, drawx[j]-symsize, drawx[j]};
	      int[] symys5 = {drawy[j]-symsize, drawy[j]+symsize, drawy[j]+symsize, drawy[j]-symsize};
	      if (Math.abs(psym)==5) offscreenG.drawPolygon(symxs5,symys5,4);
	      if (Math.abs(psym)==15) offscreenG.fillPolygon(symxs5,symys5,4);
              break;
	   case 6:
	      if (Math.abs(psym) == 6) offscreenG.drawRect(drawx[j]-symsize, drawy[j]-symsize, 2*symsize, 2*symsize);
	      if (Math.abs(psym) == 16) offscreenG.fillRect(drawx[j]-symsize, drawy[j]-symsize, 2*symsize, 2*symsize);
	      break;
           case 7:
              offscreenG.drawLine(drawx[j]-symsize, drawy[j]-symsize, drawx[j]+symsize, drawy[j]+symsize);
              offscreenG.drawLine(drawx[j]+symsize, drawy[j]-symsize, drawx[j]-symsize, drawy[j]+symsize);
              break;
	   case 8:
	      if (usymxs != null && usymys != null) {
	        int[] symxs8 = new int[usymxs.length]; 
                int[] symys8 = new int[usymys.length];
		for (int l = 0; l < usymxs.length; l++) {
		   symxs8[l] = drawx[j]+usymxs[l];
		   symys8[l] = drawy[j]+usymys[l];
		} 
                if (Math.abs(psym)==8) offscreenG.drawPolygon(symxs8,symys8,usymxs.length);
                if (Math.abs(psym)==18) offscreenG.fillPolygon(symxs8,symys8,usymys.length);
	      }
	      break;
	   case 9:
	      if (Math.abs(psym) == 9) offscreenG.drawOval(drawx[j]-symsize, drawy[j]-symsize, 2*symsize, 2*symsize);
	      if (Math.abs(psym) == 19) offscreenG.fillOval(drawx[j]-symsize, drawy[j]-symsize, 2*symsize, 2*symsize);
	      break;
	   default:
        }
      }
      if (psym <= 0) offscreenG.drawPolyline(drawx, drawy, drawx.length);
      offscreenG.setClip(0, 0, xdim, ydim); 
      repaint();
   }

   public void usersym(int[] usymxs, int[] usymys) {
      this.usymxs = usymxs;
      this.usymys = usymys;
   }

   public void overplot(float[] x, float[] y, String s) {
      this.parseOplotParams(s);
      offscreenG.setColor(oplotColor);
      this.makePlot(x, y);
      offscreenG.setColor(plotColor);
   }

   public void parseOplotParams(String s) {
      int n;
      int m;

      n = s.indexOf("*color");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        int red = (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf(",",m+1);
        int green = (int)Float.parseFloat(s.substring(m+1, n));
        m = s.indexOf(",", n+1);
        if (m < 0) m = s.length();
        int blue = (int)Float.parseFloat(s.substring(n+1, m));
        oplotColor = new Color(red, green, blue);
      } else oplotColor = new Color(0, 0, 0);

      n = s.indexOf("*psym");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        psym = (int)Float.parseFloat(s.substring(n+1, m));
      } else psym = 0;

      n = s.indexOf("*symsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        symsize = (int)Float.parseFloat(s.substring(n+1, m));
      } else symsize = 5;
   }

   public void xyouts(float xc, float yc, String text, String s) {
      int xcoord, ycoord, xyCharSize = 12;
      int n;
      int m;
      String style = "device", xyFontName = "Dialog";
      Color xyColor;
      Font xyFont;

      offscreenG.setClip(0, 0, xdim, ydim);

      n = s.indexOf("*normal");
      if (n != -1) style = "normal";

      n = s.indexOf("*data");
      if (n != -1) style = "data";

      n = s.indexOf("*color");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        int red = (int)Float.parseFloat(s.substring(n+1, m));
        n = s.indexOf(",",m+1);
        int green = (int)Float.parseFloat(s.substring(m+1, n));
        m = s.indexOf(",", n+1);
        if (m < 0) m = s.length();
        int blue = (int)Float.parseFloat(s.substring(n+1, m));
        xyColor = new Color(red, green, blue);
      } else xyColor = new Color(0, 0, 0);

      n = s.indexOf("*charsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xyCharSize = (int)Float.parseFloat(s.substring(n+1,m));
      }
      n = s.indexOf("*font");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        xyFontName = s.substring(n+1, m);
      }
      xyFont = new Font(xyFontName, 0, xyCharSize);

      if (style.equals("normal")) {
	xcoord = (int)Math.floor(xc * xdim + 0.5);
	ycoord = (int)Math.floor(yc * ydim + 0.5);
      } else if (style.equals("data")) {
        xcoord = (int)Math.floor(xoff+xscale*xc+0.5);
        ycoord = (int)Math.floor(yoff-(yscale*yc+0.5));
      } else {
	xcoord = (int)Math.floor(xc+0.5);
	ycoord = (int)Math.floor(yc+0.5);
      }
      offscreenG.setColor(xyColor);
      offscreenG.setFont(xyFont);
      offscreenG.drawString(text, xcoord, ycoord);
      offscreenG.setColor(plotColor);
      offscreenG.setFont(mainFont);
      repaint();
   }

   public void multi(int curr, int col, int row) {
      this.multi[0] = curr;
      this.multi[1] = col;
      this.multi[2] = row;
   }
 
   public void parseHistParams(String s) {
      int n;
      int m;

      n = s.indexOf("*fill");
      if (n != -1) fillHist = true;

      n = s.indexOf("*nbins");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        nbins = (int)Float.parseFloat(s.substring(n+1,m));
      } else nbins = 10;

      n = s.indexOf("*binsize");
      if (n != -1) {
        n = s.indexOf("=", n);
        m = s.indexOf(",", n);
        if (m < 0) m = s.length();
        binsize = Float.parseFloat(s.substring(n+1,m));
      } else binsize = (maxhist-minhist)/nbins;
   }

   public int[] hist(float[] x, String s) {
      doHist = true;
      xmin = UFArrayOps.minValue(x);
      xmax = UFArrayOps.maxValue(x);
      minhist = UFArrayOps.minValue(x);
      maxhist = UFArrayOps.maxValue(x);
      this.parseHistParams(s);
      int[] y = this.createHist(x);
      xmin = 1;
      xmax = nbins-1;
      ymin = 1; 
      ymax = (float)UFArrayOps.maxValue(y);
      this.parseParams(s); 
      this.drawHistAxes(y);
      multi[0]++;
      if (multi[0] >= multi[1]*multi[2]) multi[0] = 0;
      if (!noData) this.makeHist(y);
      return y;
   }

   public int[] createHist(float[] x) {
      int n;
      int[] y = new int[nbins];
      for (int j = 0; j < x.length; j++) {
	if (x[j] >= xmin && x[j] <= xmax) {
	   n = (int)((x[j]-xmin) / binsize);
	   if (n == nbins) n--;
	   y[n]++;
	}
      }
      return y;
   }

   public void drawHistAxes(int[] y) {
      pxdim1 = (int)((xdim/multi[1]) * (multi[0] % multi[1]));
      pxdim2 = (int)((xdim/multi[1]) * (1 + multi[0] % multi[1]));
      pydim1 = (int)((ydim/multi[2]) * (multi[0] / multi[2]));
      pydim2 = (int)((ydim/multi[2]) * (1 + multi[0] / multi[2]));
      xpos1 = pxdim1 + 60;
      xpos2 = pxdim2 - 20;
      ypos1 = pydim1 + 25;
      ypos2 = pydim2 - 47;
      if (multi[0] == 0) this.initPlot();
      if (!xlog) xscale = (xpos2-xpos1)/(xrange[1]-xrange[0]);
      if (!ylog) yscale = (ypos2-ypos1)/(yrange[1]-yrange[0]);
      if (!xlog) xoff = xpos1-(xscale*xrange[0]);
      if (!ylog) yoff = ypos2+(yscale*yrange[0]);
      xoff = xpos1;
      yoff = ypos2;
      int[] xaxis = {xpos1, xpos1, xpos2, xpos2, xpos1};
      int[] yaxis = {ypos1, ypos2, ypos2, ypos1, ypos1};
      offscreenG.drawPolyline(xaxis, yaxis, 5);
      this.drawTicks();
      offscreenG.setFont(xFont);
      offscreenG.drawString(xtitle, (int)((xpos2+xpos1)/2)-(int)(xtitle.length()-1)*4, ypos2+35);
      offscreenG.setFont(yFont);
      for (int j = 0; j < ytitle.length(); j++) {
        offscreenG.drawString(ytitle.substring(j, j+1), xpos1-55, (int)((ypos2+ypos1)/2)-(int)(ytitle.length()-1)*4+j*12);
      }
      offscreenG.setFont(mainFont);
      offscreenG.drawString(title, (int)((xpos2+xpos1)/2)-(int)(title.length()-1)*4, ypos1-7);
      repaint();
   }

   public void makeHist(int[] y) {
      int[] drawx = new int[2*y.length];
      int[] drawy = new int[2*y.length];
      offscreenG.setClip(xpos1, ypos1, xpos2-xpos1, ypos2-ypos1);
      for (int j = 0; j < y.length; j++) {
        drawx[2*j] = (int)Math.floor(xoff+xscale*j+0.5);
	drawx[2*j+1] = (int)Math.floor(xoff+xscale*(j+1)+0.5);
        drawy[2*j] = (int)Math.floor(yoff-(yscale*y[j])+0.5);
	drawy[2*j+1] = (int)Math.floor(yoff-(yscale*y[j])+0.5);
      }
      if (!fillHist) offscreenG.drawPolyline(drawx, drawy, drawx.length);
      else for (int j = 0; j < drawx.length; j+=2) {
	offscreenG.fillRect(drawx[j],drawy[j],(int)Math.round(xscale),(int)Math.round(yoff-drawy[j]));
      } 
      offscreenG.setClip(0, 0, xdim, ydim);
      repaint();
   }

   public float getYmax() {
      return ymax;
   }

   public void paintComponent(Graphics g) {
      super.paintComponent(g);
      if (offscreenImg != null) g.drawImage(offscreenImg, 0, 0, this);
   }

   public void actionPerformed(ActionEvent ev) {
      PageAttributes page = new PageAttributes();
      page.setPrinterResolution(84);
      page.setOrigin(PageAttributes.OriginType.PRINTABLE);
      PrintJob pjob = getToolkit().getPrintJob(new Frame(), "Java Plot", null, page);
      if (pjob != null) {
         Graphics pg = pjob.getGraphics();
         if (pg != null) {
           this.printAll(pg);
           pg.dispose();
         }
         pjob.end();
      }
   }

}
