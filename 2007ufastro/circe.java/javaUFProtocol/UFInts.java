package javaUFProtocol;

import java.io.*;

public class UFInts extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFInts.java,v 1.14 2004/09/14 22:18:37 varosi Exp $";

    protected int[] _values=null;

    public UFInts() {
	_type=MsgTyp._Ints_;
    }

    public UFInts(int length) {
	_length = length;
	_type=MsgTyp._Ints_;
    }

    public UFInts(String name) {
	_name = new String(name);
	_type = MsgTyp._Ints_;
	_length = _minLength();
    }

    public UFInts(String name, int[] vals) {
	_name = new String(name);
	_type = MsgTyp._Ints_;
	_elem = vals.length;
	_values = vals;
	_length = _minLength() + 4*vals.length;
    }

    // all methods declared abstract by UFProtocal can be defined here:

    public String description() { return new String("UFInts"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 4;
    }

    public int[] values() { return _values; }
    public int valData(int index) { return _values[index]; }

    public int maxVal() {
	int max=-2147483648;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public int minVal() {
	int min=2147483647;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    public void setNameAndVals(String s, int[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_length = _minLength();
	_values = new int[vals.length];
	for( int i=0; i<_elem; i++ ) _values[i] = vals[i];
	_length += (4*_elem);
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	try {
	    byte[] byteStream = new byte[4*_elem];
	    inp.readFully( byteStream );

	    _values = new int[_elem];
	    int shift1 = 256;
	    int shift2 = 256*256;
	    int shift3 = 256*256*256;
	    int bi = 0;
	    //shift and mask each 4 byte group into an integer:

	    for( int i=0; i<_elem; i++ ) {
		bi = i*4;
		_values[i] =
		    ( (byteStream[bi]  *shift3) & 0xff000000 ) |
		    ( (byteStream[bi+1]*shift2) & 0x00ff0000 ) |
		    ( (byteStream[bi+2]*shift1) & 0x0000ff00 ) |
		    ( (byteStream[bi+3])        & 0x000000ff );
	    }

	    return byteStream.length;
	}
	catch(EOFException eof) {
	    System.err.println("UFInts::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFInts::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFInts::recvData> "+e.toString());
	}
	return 0;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	int retval=0;
	try {
	    if (_values != null && _values.length > 0) {
		for( int elem=0; elem<_elem; elem++ ) {
		    out.writeInt(_values[elem]);
		    retval += 4;
		}
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFInts::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFInts::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFInts::sendData> "+e.toString());
	}
	return retval;
    }

    // additional methods (beyond base class's):
    // flip the image frame by reversing the order of rows:

    public void flipFrame( UFFrameConfig fc )
    {
	int nrow = fc.height;
	int ncol = fc.width;

	for( int irow=0; irow < (nrow/2); irow++ )
	    {
		int iflip = nrow - irow - 1;
		int kp = irow * ncol;
		int kf = iflip * ncol;

		for( int k=0; k<ncol; k++ ) {
		    int pixval = _values[kp];
		    _values[kp++] = _values[kf];
		    _values[kf++] = pixval;
		}
	    }
    }

    public void plusEquals(UFInts rhs) {
	if (rhs._elem == 1)
	    for (int i=0; i<this._elem; i++) 
		this._values[i] += rhs._values[0];
	else
	    for(int i=0; i<Math.min(this._elem,rhs._elem); i++) 
		this._values[i] += rhs._values[i];
    }

    public void minusEquals(UFInts rhs) {
	if (rhs._elem == 1)
	    for (int i=0; i<this._elem; i++) 
		this._values[i] -= rhs._values[0];
	else
	    for(int i=0; i<Math.min(this._elem,rhs._elem); i++) 
		this._values[i] -= rhs._values[i];
    }

    public void plusEquals(int rhs) {
	for (int i=0; i<this._elem; i++) this._values[i] += rhs;
    }

    public void minusEquals(int rhs) {
	for (int i=0; i<this._elem; i++) this._values[i] -= rhs;
    }

    public int sum(UFInts Left, UFInts Right) {
	if( Left._values == null || 
	    Right._values == null ) return 0;

	int [] viL = Left._values;
	int [] viR = Right._values;
	int elem = Math.min(Left._elem, Right._elem);

	if( this._values == null || _elem <= 0 ) this._values = new int[elem];

	for (int i=0; i<elem; i++) _values[i] = viL[i] + viR[i];

	return elem;
    }

    public int diff(UFInts Left, UFInts Right) {
	if( Left._values == null || 
	    Right._values == null ) return 0;

	int [] viL = Left._values;
	int [] viR = Right._values;
	int elem = Math.min(Left._elem, Right._elem);

	if( this._values == null || _elem <= 0 ) this._values = new int[elem];

	for (int i=0; i<elem; i++) _values[i] = viL[i] - viR[i];

	return elem;
    }
}
