package javaUFProtocol;

import java.io.*;

public class UFBytes extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFBytes.java,v 1.6 2004/09/14 22:18:07 varosi Exp $";

    protected byte[] _values=null;

    public UFBytes() {
	_length=_minLength();
	_type=MsgTyp._Bytes_;
    }

    public UFBytes(int length) {
	_length = length;
	_type=MsgTyp._Bytes_;
    }

    public UFBytes(String name) {
	_name = new String(name);
	_type=MsgTyp._Bytes_;
	_length = _minLength();
    }

    public UFBytes(String name, byte[] vals) {
	_name = new String(name);
	_type=MsgTyp._Bytes_;
	_elem=vals.length;
	_values = vals;
	_length = _minLength() + vals.length;
    }

    // all methods declared abstract by UFProtocol can be defined here:

    public String description() { return new String("UFBytes"); }
 
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 1;
    }

    public byte[] values() { return _values; }
    public byte valData(int index) { return _values[index]; }

    public byte maxVal() {
	byte max=-128;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i]>max ) max = _values[i];
	return max;
    }

    public byte minVal() {
	byte min=127;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    protected void _copyNameAndVals(String s, byte[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_values = new byte[vals.length];
	_length = _minLength() + vals.length;
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = vals[i];
	}
    }

    public void setNameAndVals(String s, byte[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_values = new byte[vals.length];
	_length = _minLength() + vals.length;
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = vals[i];
	}
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	try {
	    _values = new byte[_elem];
	    inp.readFully(_values);
	    return _values.length;
	}
	catch(EOFException eof) {
	    System.err.println("UFBytes::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFBytes::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFBytes::recvData> "+e.toString());
	}
	return 0;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	try {
	    if (_values != null && _values.length > 0) {
		out.write(_values);
		return _values.length;
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFBytes::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFBytes::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFBytes::sendData> "+e.toString());
	}
	return 0;
    }
}
