//Title:        JCIopticalPath for Java Control Interface (JCI)
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javaUFLib.*;

//===============================================================================
/**
 * Handles the Show Optical Path window
 */
public class JCIopticalPath extends JFrame
{
    public static final
	String rcsID = "$Name:  $ $Id: JCIopticalPath.java,v 1.2 2004/10/12 23:04:05 varosi Exp $";

    String win_name;
    final int x0 = 10;
    final int xw = 95;
    final int xs = 20;
    final int dx = 2;
    final int y0 = 10;
    final int yrow = 20; // y per row

    GraphicsPanel opticPathDiag = new GraphicsPanel();

    float opticsThruPut = 1;
    UFLabel netThruPut = new UFLabel(" Net Optical Throughput =");
    int Nmot;
    JLabel motor[][];
    UFLabel thruPuts[];
    int x1[], x2[], yp[],  hilighted[];

    Color on_path_color = Color.white;
    Color off_path_color = Color.lightGray;

    /**
     *Construct the frame
     */
    public JCIopticalPath( JCIMotor[] jciMotors )
    {
	enableEvents(AWTEvent.WINDOW_EVENT_MASK);
	try  {
	    opInit( jciMotors );
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void opInit( JCIMotor[] jciMotors ) throws Exception
    {
	win_name = "JCI: Optical Path of CanariCam";
	this.setTitle(win_name);
	this.setLocation( jci.get_screen_loc( win_name ) );

	addComponentListener(new java.awt.event.ComponentAdapter() {
		public void componentResized(ComponentEvent e) {
		    jci.set_screen_loc(win_name, getLocation(), getSize());
		}
	    });
	addComponentListener(new java.awt.event.ComponentAdapter() {
		public void componentMoved(ComponentEvent e) {
		    jci.set_screen_loc(win_name, getLocation(), getSize());
		}
	    });

	this.getContentPane().setLayout(new BorderLayout());
	this.getContentPane().add( opticPathDiag, BorderLayout.CENTER );
	this.getContentPane().add( netThruPut, BorderLayout.SOUTH );
	opticPathDiag.setLayout(null);

	Nmot = jciMotors.length;
	x1 = new int[Nmot];
	x2 = new int[Nmot];
	yp = new int[Nmot+2];
	yp[Nmot] = y0 + yrow / 2;
	hilighted = new int[Nmot];
	motor = new JLabel[Nmot][];
	thruPuts = new UFLabel[Nmot];
	int maxNpos = 0;
	int imot = 0;

	for( int i = 0; i < Nmot; i++)
	    {
		int Npos = jciMotors[i].positions.length;
		motor[i] = new JLabel[Npos];
		hilighted[i] = -1;

		if( jciMotors[i].name.toUpperCase().indexOf("ROTAT") <= 0 )
		    {
			if( Npos > maxNpos ) maxNpos = Npos;
			int xmot = x0 + imot * (xw + xs);
			++imot;
			x1[i] = xmot + xw + dx;
			x2[i] = xmot + xw + xs - dx;
			yp[i] = y0 + yrow / 2;
			JLabel motorName = new JLabel( jciMotors[i].name, JLabel.CENTER );
			motorName.setBounds( xmot-4, y0-4, xw+8, yrow+4 );
			motorName.setBorder( new EtchedBorder(0) );
			opticPathDiag.add( motorName );
			thruPuts[i] = new UFLabel("Transm.=");
			thruPuts[i].setBounds( xmot, y0 + yrow, xw, yrow );
			opticPathDiag.add( thruPuts[i] );

			for( int j = 0; j < Npos; j++ ) {
			    motor[i][j] = new JLabel( jciMotors[i].positionNames[j] );
			    motor[i][j].setOpaque(true);
			    motor[i][j].setBackground( off_path_color );
			    motor[i][j].setBounds( xmot, y0 + (j+2)*yrow, xw, yrow );
			    opticPathDiag.add( motor[i][j] );
			}
		    }
		else yp[i] = -1;
	    }

	updatePathLines( jciMotors );
	Nmot = imot;
	this.setSize( new Dimension( 2*x0 + Nmot*xw + (Nmot-1)*xs + xs/2, (maxNpos+5)*yrow + y0 ) );
    }
//-------------------------------------------------------------------------------
    /**
     * Creates and draw the lines of optical path.
     */
    public void updatePathLines( JCIMotor[] jciMotors )
    {
	opticsThruPut = 1;

	for( int i = 0; i < jciMotors.length; i++)
	    if( jciMotors[i].name.toUpperCase().indexOf("ROTAT") <= 0 )
		update_motor_status( jciMotors[i] );

	System.out.println( this.getClass().getName() + "> optics Thru-Put = " + opticsThruPut );
	netThruPut.setText( Float.toString( opticsThruPut ) );
    }
//-------------------------------------------------------------------------------
    /**
     * Sets yp[] for the motor that moved
     */
    public void update_motor_status( JCIMotor jciMotor )
    {
	int mot_num = jciMotor.motorNumber;

	if( hilighted[mot_num] != -1 )
	    motor[mot_num][hilighted[mot_num]].setBackground( off_path_color );

	int pindex = jciMotor.nearestPosIndex;
	hilighted[mot_num] = pindex;
	motor[mot_num][pindex].setBackground( on_path_color );

	int steps_beyond_pos = jciMotor.nearestPosDistance;
	int steps_per_pos=100;

	opticsThruPut *= jciMotor.throughPuts[pindex];

	if( steps_beyond_pos == 0 || jciMotor.positionNames[pindex].indexOf("HighRes") >= 0 )
	    thruPuts[mot_num].setText( Float.toString( jciMotor.throughPuts[pindex] ) );
	else
	    thruPuts[mot_num].setText("?");

	if( steps_beyond_pos < 0 )
	    {
		if( pindex > 0 )
		    steps_per_pos = jciMotor.positions[pindex] - jciMotor.positions[pindex-1];
		else
		    steps_beyond_pos = 0;
	    }
	else if( steps_beyond_pos > 0 )
	    {
		if( pindex < jciMotor.positions.length-1 )
		    steps_per_pos = jciMotor.positions[pindex+1] - jciMotor.positions[pindex];
		else
		    steps_beyond_pos = 0;
	    }

	if( steps_per_pos <= 0 ) {
	    steps_per_pos = 1;
	    steps_beyond_pos = 0;
	}

	yp[mot_num] = y0 + yrow/2 + yrow*(jciMotor.nearestPosIndex + 2) + yrow*steps_beyond_pos/steps_per_pos;
	repaint();
    }

//===============================================================================
    /**
     * Graphical Interpretation of the Optical path
     */
    class GraphicsPanel extends JPanel {
	/**
	 * paints components: the lines of optical path.
	 *@param g Graphics: TBD
	 */
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    for (int i = 0; i < Nmot; i++) {
		if( yp[i+1] > 0 )
		    g.drawLine( x1[i], yp[i], x2[i], yp[i+1] );
		else
		    g.drawLine( x1[i], yp[i], x2[i], yp[i+2] );
	    }
	}
    }
} //end of class JCIopticalPath
