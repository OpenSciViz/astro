//Title:        JPanelMotorHighLevel class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of motors in CanariCam infrared camera system.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;

//===============================================================================
/**
 * Handles the Motor High Level tabbed pane
 */
public class JPanelMotorHighLevel extends JPanel{
    public static final
      String rcsID = "$Name:  $ $Id: JPanelMotorHighLevel.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";
    /**
     * Default constructor
     *@param jciMotors JCIMotor[]: Array of JCIMotor objects, one for each physical motor.
     */
    public JPanelMotorHighLevel(final JCIMotor[] jciMotors)
    {
	setLayout(new RatioLayout());
	JPanel highPanel = new JPanel();
	highPanel.setLayout(new GridLayout(0,1));
	highPanel.add( JCIMotor.getMotorHighLevelLabelPanel() );
	for (int i=0; i<jciMotors.length; i++) highPanel.add( jciMotors[i].getMotorHighLevelPanel() );
	add("0.05,0.10;0.99,0.35",highPanel);

	JPanel scrollPanel = new JPanel();
	scrollPanel.setLayout(new GridLayout(0,1));
	for (int i=0; i<jciMotors.length; i++)
	    if (jciMotors[i] instanceof JCIMotorGrating) {
		JCIMotorGrating gratingWheel = (JCIMotorGrating)jciMotors[i];
		int ng = gratingWheel.highResGratings.length;
		for( int k=0; k<ng; k++ )
		    scrollPanel.add( gratingWheel.highResGratings[k].getHiResGratingPanel() );
	    }
	//add("0.01,0.80;0.66,0.20",new JScrollPane(scrollPanel));

	JButton checkLocation = new JButton("Check  Location  of  All  Motors");
	checkLocation.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { checkLocation(jciMotors); } });
	add("0.35,0.75;0.30,0.10", checkLocation);
    }
//--------------------------------------------------------------------------------------------------
    public void checkLocation(final JCIMotor[] jciMotors)
    {
	//clear all status labels first so that user sees blank status:
	for (int i=0; i<jciMotors.length; i++) jciMotors[i].hiStatusLabel.setText(" ");
	for (int i=0; i<jciMotors.length; i++) jciMotors[i].doLocateThread();
    }
}
