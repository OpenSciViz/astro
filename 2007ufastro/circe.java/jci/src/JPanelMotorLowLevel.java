//Title:        JPanelMotorLowLevel class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;

//===============================================================================
/**
 *Tabbed pane for Low Level motor control by incremental steps.
 */
public class JPanelMotorLowLevel extends JPanel{
    public static final
	String rcsID = "$Name:  $ $Id: JPanelMotorLowLevel.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";
    /**
     *Default Constructor
     *@param jciMotors JCIMotor[]: Array of JCIMotors 
     */
    public JPanelMotorLowLevel(JCIMotor[] jciMotors) {
	JPanel singleMotorPanel = new JPanel();
	singleMotorPanel.setLayout(new GridLayout(0,1));
	singleMotorPanel.add(JCIMotor.getMotorLowLevelLabelPanel());
	for (int i=0; i<jciMotors.length; i++)
	    singleMotorPanel.add( jciMotors[i].getMotorLowLevelPanel() );
	
	setLayout(new RatioLayout());
	this.add("0.05,0.10;0.99,0.35", singleMotorPanel);
	this.add("0.01,0.70;0.99,0.15", new JPanelMultiMotor(jciMotors));
    }

    //===============================================================================
    /**
     *MultiMotor panel of components for handling multiple motors
     */
    private class JPanelMultiMotor extends JPanel implements ActionListener {
	public UFTextField moveIncField = new UFTextField("Move Increment","0",7);
	public JButton jButtonMove = new JButton("Move");
	public JButton jButtonDatum = new JButton("Datum");
	public JButton jButtonOrigin = new JButton("Origin");
	public JButton jButtonAbort = new JButton("Abort");
	public JButton checkLocation = new JButton("Check  Location  of  Selected  Motors");
	public JButton jButtonSelectAll = new JButton("Enable  ALL");
	public JButton jButtonSelectNone = new JButton("Enable  None");
	public JCheckBox jCheckBox[];
	private JCIMotor[] jciMotors;
	//-------------------------------------------------------------------------------
	/**
	 *Default constructor
	 *@param jciMotors JCIMotor[]: Array of JCIMotors
	 */
	public JPanelMultiMotor(JCIMotor[] jciMotors) {
	    this.setLayout(new BorderLayout());
	    this.jciMotors = jciMotors;
	    jCheckBox = new JCheckBox[jciMotors.length];
	    JPanel topPanel = new JPanel();
	    JPanel botPanel = new JPanel();
	    for (int i=0; i<jciMotors.length; i++) {
		jCheckBox[i] = new JCheckBox( jciMotors[i].Indexor + " " );
		botPanel.add(jCheckBox[i]);
		jCheckBox[i].addActionListener(this);
		jCheckBox[i].setActionCommand("check " + i);
	    }
	    botPanel.add(jButtonSelectAll);
	    jButtonSelectAll.addActionListener(this);
	    jButtonSelectAll.setActionCommand("selectall");
	    botPanel.add(jButtonSelectNone);
	    jButtonSelectNone.addActionListener(this);
	    jButtonSelectNone.setActionCommand("selectnone");
	    topPanel.add(new JLabel("Multi -  Motor Control: "));
	    topPanel.add(jButtonDatum);
	    jButtonDatum.addActionListener(this);
	    jButtonDatum.setActionCommand("datum");
	    topPanel.add(jButtonAbort);
	    jButtonAbort.addActionListener(this);
	    jButtonAbort.setActionCommand("abort");
	    topPanel.add(jButtonMove);
	    topPanel.add(moveIncField);
	    jButtonMove.addActionListener(this);
	    jButtonMove.setActionCommand("move");
	    checkLocation.addActionListener(this);
	    checkLocation.setActionCommand("Locate");
	    topPanel.add(checkLocation);
	    this.add(topPanel,BorderLayout.NORTH);
	    this.add(botPanel,BorderLayout.SOUTH);
	} // end of JPanelMultiMotor constructor

	//-------------------------------------------------------------------------------
	/**
	 *@param ae ActionEvent: String containing desired action from buttons.
	 */
	public void actionPerformed(ActionEvent ae) {
	    String theCmd = ae.getActionCommand();
	    if (theCmd.equals("selectall")) {
		for (int i=0; i<jCheckBox.length; i++) {
		    jCheckBox[i].setSelected(true);
		    jciMotors[i].hiNameCheckBox.setSelected(true);
		    jciMotors[i].loNameCheckBox.setSelected(true);
		    jciMotors[i].paramsCheckBox.setSelected(true);
		    jciMotors[i].ableButtons();
		}
	    }
	    else if (theCmd.equals("selectnone")) {
		for (int i=0; i<jCheckBox.length; i++) {
		    jCheckBox[i].setSelected(false);
		    jciMotors[i].hiNameCheckBox.setSelected(false);
		    jciMotors[i].loNameCheckBox.setSelected(false);
		    jciMotors[i].paramsCheckBox.setSelected(false);
		    jciMotors[i].ableButtons();
		}
	    }
	    else if (theCmd.equals("move")) {
		String moveIncrem = moveIncField.getText().trim();
		try {
		    int stm = Integer.parseInt( moveIncrem );
		    moveIncField.setNewState();
		}
		catch(NumberFormatException nfe) {
		    moveIncField.setNewState("ERR: number?");
		    return;
		}
		for (int i=0; i<jCheckBox.length; i++)
		    if (jCheckBox[i].isSelected()) {
			jciMotors[i].stepsToMoveField.setText( moveIncrem );
			jciMotors[i].doLoMoveThread();
		    }
	    }
	    else if (theCmd.equals("Locate")) {
		//clear all status labels first so that user sees blank status:
		for (int i=0; i<jciMotors.length; i++) {
		    if( jciMotors[i].loNameCheckBox.isSelected() ) jciMotors[i].loStatusLabel.setText(" ");
		}
		for (int i=0; i<jciMotors.length; i++) {
		    if( jciMotors[i].loNameCheckBox.isSelected() ) jciMotors[i].doLocateThread();
		}
	    }
	    else if (theCmd.equals("status")) {
		for (int i=0; i<jciMotors.length; i++) jciMotors[i].doStatusThread();
	    }
	    else if (theCmd.equals("datum")) {
		for (int i=0; i<jCheckBox.length; i++)
		    if (jCheckBox[i].isSelected()) jciMotors[i].doDatumThread();
	    }
	    else if (theCmd.equals("abort")) {
		for (int i=0; i<jCheckBox.length; i++)
		    if (jCheckBox[i].isSelected()) jciMotors[i].doAbortThread();
	    }
	    else if (theCmd.equals("origin")) {
		for (int i=0; i<jCheckBox.length; i++)
		    if (jCheckBox[i].isSelected()) jciMotors[i].doOriginThread();
	    }
	    else if( theCmd.indexOf("check") == 0 ) {
		String[] words = theCmd.split(" ");
		int motnum = Integer.parseInt( words[words.length-1] );
		if( jCheckBox[motnum].isSelected() ) {
		    jciMotors[motnum].hiNameCheckBox.setSelected(true);
		    jciMotors[motnum].loNameCheckBox.setSelected(true);
		    jciMotors[motnum].paramsCheckBox.setSelected(true);
		} else {
		    jciMotors[motnum].hiNameCheckBox.setSelected(false);
		    jciMotors[motnum].loNameCheckBox.setSelected(false);
		    jciMotors[motnum].paramsCheckBox.setSelected(false);
		}
		jciMotors[motnum].ableButtons();
	    }
	} // end of actionPerformed
    } // end of class JPanelMultiMotor
}
