//Title:        JPanelMotorParameters class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;
import javaUFLib.*;

//===============================================================================
/**
 * Motor Indexor Control Parameters tabbed pane
 */
public class JPanelMotorParameters extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: JPanelMotorParameters.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";

    JCIMotor[] jciMotors;
    JButton reConnectButton;
    JButton reReadButton;
    JButton reSendButton;
    JButton verboseButton;
    UFHostPortPanel ufhpp;
    /**
     *Default constructor
     *@param jciMotors JCIMotor[]: Array of JCIMotors
     */
    public JPanelMotorParameters(JCIMotor[] jciMotors) {
      this.jciMotors = jciMotors;
      setLayout(new GridLayout(0,1));
      add(JCIMotor.getMotorParameterLabelPanel());
      //add(JCIMotor.getMotorParameterHelpPanel());
      for (int i=0; i<jciMotors.length; i++) add(jciMotors[i].getMotorParameterPanel() );

      reConnectButton = new JButton("Connect");
      reReadButton = new JButton("Read Parameter File");
      reSendButton = new JButton("Send  All  Parameters");
      verboseButton = new JButton("Turn ON Verbose Replies");
      ufhpp = new UFHostPortPanel( jciMotors[0].getHost(), jciMotors[0].getPort() );
      ufhpp.setVisible(true);

      reConnectButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ae) { 
		  if( JOptionPane.showConfirmDialog( null,"Re-Connect all sockets to motor agent ?")
		      == JOptionPane.YES_OPTION ) reConnect();
	      }
	  });

      reReadButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ae) { reReadMotorParameters(); } });

      reSendButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ae) { reSendMotorParameters(); } });

      verboseButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ae) { verboseAction(); } });

      JPanel buttonsPanel = new JPanel();
      buttonsPanel.setLayout(new RatioLayout());
      buttonsPanel.add("0.00,0.01;0.24,0.50",ufhpp);
      buttonsPanel.add("0.24,0.01;0.20,0.50",reConnectButton);
      buttonsPanel.add("0.45,0.01;0.19,0.50",reSendButton);
      buttonsPanel.add("0.65,0.01;0.18,0.50",reReadButton);
      buttonsPanel.add("0.84,0.01;0.16,0.50",verboseButton);

      add(new JPanel()); //just a blank filler panel above buttons.
      add(buttonsPanel);
      reConnect();
    }

    // method used by jciFrame to set hostname for motors:

    public void setHost(String host) {
	ufhpp.setHost(host);
    }

    // action method for verbose button:

    public void verboseAction() {
	boolean verbose = false;
	if( verboseButton.getText().indexOf("ON") > 0 ) {
	    verbose = true;
	    verboseButton.setText("Turn OFF Verbose Replies");
	}
	else verboseButton.setText("Turn ON Verbose Replies");
	for (int i=0; i<jciMotors.length; i++) jciMotors[i].verbose = verbose;
    }

    // action method for reConnect button:

    public void reConnect()
    {
	int retval_sum = 0;

	for (int i=0; i<jciMotors.length; i++) {
	    try {
		jciMotors[i].setHostAndPort(ufhpp.getHostField(),ufhpp.getPortField());
		retval_sum += jciMotors[i].doReconnect();
	    }
	    catch (Exception e) {
		System.err.println("JPanelMotorParameters> "+e.toString());
		retval_sum = -1;
		break;
	    }
	}

	if (retval_sum < 0)
	    indicateConnStatus( false );
	else
	    indicateConnStatus( true );
    }

    public void indicateConnStatus( boolean statusOK ) {
	if( statusOK ) {
	    reConnectButton.setBackground(Color.green);
	    reConnectButton.setForeground(Color.black);
	    reConnectButton.setText("Connected to Motor Agent");
	} else {
	    reConnectButton.setBackground(Color.red);
	    reConnectButton.setForeground(Color.white);
	    reConnectButton.setText("Re-Connect to Motor Agent");
	    Toolkit.getDefaultToolkit().beep();
	}
    }

    // action method for send params button:

    public void reSendMotorParameters()
    {
	if( JOptionPane.showConfirmDialog( null,"Send current parameters for all indexors ?")
	    == JOptionPane.YES_OPTION )
	    {
		for (int i=0; i<jciMotors.length; i++) jciMotors[i].doParamsThread();
	    }
    }

    // action method for reRead button:

    public void reReadMotorParameters()
    {	
	if( JOptionPane.showConfirmDialog( null,"Replace all parameters with values from file ?")
	    != JOptionPane.YES_OPTION ) return;
	    
	JCIMotor[] tmpMotors = JCIMotor.createMotors( jciFrame.motorFileName );

	for (int i=0; i<jciMotors.length; i++) {
	    jciMotors[i].setInitialSpeed(tmpMotors[i].getInitialSpeed());
	    jciMotors[i].setTerminalSpeed(tmpMotors[i].getTerminalSpeed());
	    jciMotors[i].setAcceleration(tmpMotors[i].getAcceleration());
	    jciMotors[i].setDeceleration(tmpMotors[i].getDeceleration());
	    jciMotors[i].setHoldCurrent(tmpMotors[i].getHoldCurrent());
	    jciMotors[i].setDriveCurrent(tmpMotors[i].getDriveCurrent());
	    jciMotors[i].setDatumSpeed(tmpMotors[i].getDatumSpeed());
	    jciMotors[i].setFinalDatumSpeed(tmpMotors[i].getFinalDatumSpeed());
	    jciMotors[i].setBackLash(tmpMotors[i].getBackLash());
	    jciMotors[i].setDatumDirection(tmpMotors[i].getDatumDirection());
	    jciMotors[i].setName(tmpMotors[i].getName());
	    jciMotors[i].setHwName(tmpMotors[i].getHwName());
	    jciMotors[i].setNamedLocations( tmpMotors[i].getPositionNames(),
					    tmpMotors[i].getMotorPositions() );
	    jciMotors[i].setHostAndPort( tmpMotors[i].getHost(), tmpMotors[i].getPort() );
	    if (tmpMotors[i] instanceof JCIMotorGrating) {
		JCIhighResGrating[] hrGratsNew = ((JCIMotorGrating)tmpMotors[i]).highResGratings;
		JCIhighResGrating[] hrGratings = ((JCIMotorGrating)jciMotors[i]).highResGratings;
		for( int k=0; k < hrGratings.length; k++ ) {
		    hrGratings[k].setFiducialAngle( hrGratsNew[k].getFiducialAngle() );
		    hrGratings[k].setFiducialSteps( hrGratsNew[k].getFiducialSteps() );
		}
	    }
	}
    }
}
