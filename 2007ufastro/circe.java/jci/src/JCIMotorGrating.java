//Title:        JCIMotorGrating class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;

//===============================================================================
/**
 * Objects for special case of Hi-Level control of motor for a Grating Wheel.
 * Special processing is need for central wavelength positioning of High Resolution gratings.
 */
public class JCIMotorGrating extends JCIMotor {

    public JCIhighResGrating[] highResGratings;

    public JCIMotorGrating(String name, char Indexor, String host, int port) {
	super(name,Indexor,host,port);
    }
    public JCIMotorGrating(String name, char Indexor, String host, int port, 
			   int is, int ts, int acc, int dec, int dc, int hc, int ds, int fds, 
			   int ddir, int bl, int[] positions, String[] positionNames,
			   String[] hrgNames, int[] fiducialSteps, double[] fiducialAngles )
    {
	super(name,Indexor, host,port, is,ts,acc,dec,dc,hc,ds,fds,ddir,bl, positions,positionNames);
	int nhrgs = hrgNames.length;
	highResGratings = new JCIhighResGrating[nhrgs];
	for( int k=0; k < nhrgs; k++ ) {
	    highResGratings[k] = new JCIhighResGrating( hrgNames[k], fiducialSteps[k], fiducialAngles[k] );
	    highResGratings[k].createVisualComponents();
	}
    }

    //must overload the high-level move function
    public void doHiMove() {
	synchParametersWithVisuals();
	int ipos = namedLocationBox.getSelectedIndex() - 1; //first name is always blank so skip it.
	if( ipos >= 0 ) {
	    String positionName = positionNames[ipos];
	    if( positionName.indexOf("HighRes") >= 0 ) {
		int gratPos = 0;
		for( int k=0; k < highResGratings.length; k++ ) {
		    if( highResGratings[k].gratingName.equals( positionName ) )
			gratPos = highResGratings[k].gratingOffsetCalc();
		}
		if( gratPos == 0 ) {
		    System.err.println(".doHiMove> invalid position name: " + positionName );
		}
		else if( gratPos < 0 ) {
		    System.err.println(".doHiMove> error in grating position offset calc.");
		}
		else if( gratPos != currentSteps ) {
		    sendHiMove(gratPos);
		    motionStatus(true);
		}
		else {
		    namedLocationBox.setBackground( Color.white );
		    if( masterMotorGUI != null ) masterMotorGUI.setActive( positionName );
		}
	    }
	    else if( positions[ipos] != currentSteps ) {
		sendHiMove(positions[ipos]);
		motionStatus(true);
	    }
	    else {
		namedLocationBox.setBackground( Color.white );
		if( masterMotorGUI != null ) masterMotorGUI.setActive( positionNames[ipos] );
	    }
	}
    }
}
