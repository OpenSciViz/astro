//Title:        JCIMotorParameters class for Java Control Interface (JCI).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;
import java.util.*;

public class JCIMotorParameters {
    public static final
	String rcsID="$Name:  $ $Id: JCIMotorParameters.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";

    protected int initialSpeed;
    protected int terminalSpeed;
    protected int acceleration;
    protected int deceleration;
    protected int driveCurrent;
    protected int holdCurrent;
    protected int datumSpeed;
    protected int finalDatumSpeed;
    protected int datumDirection;
    protected int backLash;
    String name; // human name
    char Indexor; // hardware name
    int [] positions; // number of steps for each named position (high level)
    String [] positionNames; // named position names (high level)

    //nearest position stuff set by method nearestPosition(), see below:
    int nearestPosIndex;
    int nearestPosDistance;

    ////////////////
    //Constructors//
    ////////////////

    //default constructor
    JCIMotorParameters() {
	//change these defaults?
	initialSpeed     = 0; terminalSpeed  = 0; acceleration = 0;
	deceleration     = 0; driveCurrent   = 0; holdCurrent  = 0;
	datumSpeed       = 0; backLash       = 0;
	finalDatumSpeed  = 0; datumDirection = 0; 
	name = "NoName";
	Indexor = 'A';
	positions = new int[0];
	positionNames = new String[0];
    }

    JCIMotorParameters(int is, int ts, int acc, int dec, int dc, int hc, int ds, int fds,
		       int ddir, int bl, String name, char Indexor,
		       int [] positions, String[] positionNames) {
	initialSpeed     = is; terminalSpeed  = ts; acceleration = acc;
	deceleration     = dec; driveCurrent   = dc; holdCurrent  = hc;
	datumSpeed       = ds; backLash       = bl;
	finalDatumSpeed = fds; datumDirection = ddir; 
	this.name = new String(name);
	this.Indexor = Indexor;
	setMotorPositions(positions,false); // deep copy
	setPositionNames(positionNames,false); // deep copy
    }

    JCIMotorParameters(String name, char Indexor) {
	this();
	this.name = new String(name);
	this.Indexor = Indexor;
    }

    ////////////////////////////////
    //Accessor and mutator methods//
    ////////////////////////////////
    public void setInitialSpeed(int speed)  { this.initialSpeed = speed; }
    public void setTerminalSpeed(int speed) { this.terminalSpeed = speed; }
    public void setAcceleration(int accel)  { this.acceleration = accel; }
    public void setDeceleration(int decel)  { this.deceleration = decel; }
    public void setDriveCurrent(int drive)  { this.driveCurrent = drive; }
    public void setHoldCurrent(int current) { this.holdCurrent = current; }
    public void setBackLash(int lash)       { this.backLash = lash; }
    public void setDatumSpeed(int speed)    { this.datumSpeed = speed; }
    public void setDatumDirection(int direction) { this.datumDirection = direction; }
    public void setFinalDatumSpeed(int speed)    { this.finalDatumSpeed = speed;}
    public void setName(String name)   { this.name = new String(name); }
    public void setHwName(char Indexor) { this.Indexor = Indexor; }

    public int getInitialSpeed()  { return this.initialSpeed; }
    public int getTerminalSpeed() { return this.terminalSpeed; }
    public int getAcceleration()  { return this.acceleration; }
    public int getDeceleration()  { return this.deceleration; }
    public int getDriveCurrent()  { return this.driveCurrent; }
    public int getHoldCurrent()   { return this.holdCurrent; }
    public int getBackLash()      { return this.backLash; }
    public int getDatumSpeed()    { return this.datumSpeed; }
    public int getDatumDirection()  { return this.datumDirection; }
    public int getFinalDatumSpeed() { return this.finalDatumSpeed; }
    public String getName() { return new String(this.name); }
    public char getHwName() { return this.Indexor; }

    public void setMotorPositions (int [] positions) { this.positions = positions; }

    public void setMotorPositions(int[] positions, boolean shallow) {
	if (shallow)
	    setMotorPositions(positions);
	else {
	    this.positions = new int[positions.length];
	    for (int i=0; i<positions.length; i++) this.positions[i] = positions[i];
	}
    }

    public int [] getMotorPositions () { return positions; }

    public int [] getMotorPositions (boolean shallow) {
	if (shallow)
	    return getMotorPositions();
	else {
	    int [] temp = new int[this.positions.length];
	    for (int i=0; i<this.positions.length; i++)
		temp[i] = this.positions[i];
	    return temp;
	}
    }

    public void setPositionNames (String [] names) { this.positionNames = names; }

    public void setPositionNames (String [] names, boolean shallow) {
	if (shallow)
	    setPositionNames(names);
	else {
	    this.positionNames = new String[names.length];
	    for (int i=0; i<positionNames.length; i++) 
		this.positionNames[i] = new String(names[i]);
	}
    }

    public String [] getPositionNames () { return positionNames; }

    public String [] getPositionNames (boolean shallow) {
	if (shallow)
	    return getPositionNames();
	else {
	    String [] temp = new String[this.positionNames.length];
	    for (int i=0; i<this.positionNames.length; i++)
		temp[i] = this.positionNames[i];
	    return temp;
	}
    }

    public String[] sortedPositionNames()
    {
	Vector names = new Vector();

	for( int i=0; i<this.positionNames.length; i++ )
	{
	    String posname = positionNames[i].toLowerCase();

	    if( posname.indexOf("open") < 0 &&
		posname.indexOf("block") < 0 &&
		posname.indexOf("pup") < 0 &&
		posname.indexOf("park") < 0 &&
		posname.indexOf("datum") < 0 &&
		posname.indexOf("spot") < 0 ) names.add( positionNames[i] );
	}

	String[] posNames = new String[names.size()];
	names.copyInto( posNames );

	java.util.Arrays.sort( posNames );
	return posNames;
    }

    private void sort(int[] pos, String[] nam) {
	for (int i=0; i<pos.length; i++) 
	    for (int j=i+1; j<pos.length-1; j++) {
		if (pos[i] > pos[j]) {
		    int temp = pos[j];
		    pos[j] = pos[i];
		    pos[i] = temp;
		    String temper = new String (nam[j]);
		    nam[j] = new String(nam[i]);
		    nam[i] = new String(temper);
		}
	    }
    }

    public String nearestPosition(int currentSteps)
    {
	nearestPosIndex= -1;
	nearestPosDistance=2000000000;

	for( int i=0; i<positions.length; i++ )
	    {
		int delta = currentSteps - positions[i];
		if( Math.abs( delta ) < Math.abs( nearestPosDistance ) ) {
		    nearestPosDistance = delta;
		    nearestPosIndex = i;
		}
	    }

	String PosName = positionNames[ nearestPosIndex ];

	if( nearestPosDistance > 0 )
	    return PosName + "  +  " + nearestPosDistance;
	else if( nearestPosDistance < 0 )
	    return PosName + "  -  " + Math.abs( nearestPosDistance );
	else
	    return PosName;
    }
}
