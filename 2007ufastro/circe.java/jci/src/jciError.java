package ufjci;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import javaUFLib.*;

//===============================================================================
/**
 * A class to report and log errors
 */
public class jciError {
  public static final String rcsID = "$Name:  $ $Id: jciError.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

  static JFrame errorMsgFrame = new JFrame("jciError Window");
  static JTextArea jTextArea = new JTextArea();
  static JScrollPane scrollPane = new JScrollPane(jTextArea);;
  static JPanel buttonPanel = new JPanel();
  static JButton dismissButton = new JButton("Dismiss");
  static JButton clearButton = new JButton("Clear");
  static private boolean firstTime = true;
  static TextFile errorFile;
  //-------------------------------------------------------------------------------
  /**
   * Error message frame initialization
   */
  private static void errorMsgFrameInit() {
    try {
      buttonPanel.setLayout(new FlowLayout());
      buttonPanel.add(dismissButton);
      buttonPanel.add(clearButton);

      dismissButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) { dismissButton_actionPerformed(); } });
      clearButton.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ae) { clearButton_actionPerformed(); } });

      errorMsgFrame.getContentPane().setLayout(new BorderLayout());
      errorMsgFrame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);
      errorMsgFrame.getContentPane().add(scrollPane, BorderLayout.CENTER);

      errorMsgFrame.addWindowListener(new WindowAdapter() {
	      public void windowClosing(WindowEvent e) { dismissButton_actionPerformed(); } });

      errorMsgFrame.getContentPane().validate();
      errorMsgFrame.setLocation(jci.get_screen_loc("jciError"));
      errorMsgFrame.setSize(jci.get_screen_size("jciError"));

      errorMsgFrame.addComponentListener(new java.awt.event.ComponentAdapter() {
        public void componentResized(ComponentEvent e) {
          jci.set_screen_loc("jciError", errorMsgFrame.getLocation(), errorMsgFrame.getSize());
        }
      });
      errorMsgFrame.addComponentListener(new java.awt.event.ComponentAdapter() {
        public void componentMoved(ComponentEvent e) {
          jci.set_screen_loc("jciError", errorMsgFrame.getLocation(), errorMsgFrame.getSize());
        }
      });
      firstTime = false;
      StringTokenizer st = new StringTokenizer(new Date().toString());
      st.nextToken();
      String filename = "jci_errors_";
      filename += st.nextToken();
      st.nextToken(); st.nextToken(); st.nextToken();
      filename += "-" + st.nextToken();
      filename += ".log";
      errorFile = new TextFile(jci.data_path + filename, TextFile.APPEND);
    }
    catch (Exception e) {
      System.out.println("Error here" + e.toString());
    }
  } // end of errorMsgFrameInit
//------------------------------------------------------------------------------
  /**
   * Destroys the window when the Dismiss button is pressed.
   * or the when window is closed
   */
  public static void dismissButton_actionPerformed() {
      errorFile.close();
      errorMsgFrame.dispose();
      firstTime = true;
  }
//------------------------------------------------------------------------------
  /**
   * Clear the text area of previous messages
   */
  public static void clearButton_actionPerformed() { jTextArea.setText(""); }

//------------------------------------------------------------------------------
  /**
   * Show a new error to the log window and write it to log file
   *@param msg String: message to be displayed in the error frame
   */
  public static void show(String msg) {
    msg = "Error: " + msg;
    System.out.println(msg);
    if (firstTime) errorMsgFrameInit();
    jTextArea.append(msg + "\n");
    //    String className = "";
    //    try { className = getContext(1).toString(); }
    //    catch (Exception e) {}
    //    jTextArea.append("     " + className + "\n");
    errorMsgFrame.show();
    errorMsgFrame.repaint();
    try {
      String s = new Date().toString() + " " + msg + "\n";
      errorFile.println(s);
    }
    catch (Exception e) {
	System.out.println(e.toString());
    }
  }
//------------------------------------------------------------------------------
  /**
   * Create/Unhide the error window
   */
   public static void show() {
     if (firstTime) errorMsgFrameInit();
     errorMsgFrame.show();
     errorMsgFrame.repaint();
   }
//-------------------------------------------------------------------------------
  /**
   * Determine where the error occured. Level will normally be 3.
   * This is a bit of a kluge. Do yo know a better way?
   *@param level level of the error
   */
  static Context getContext(int level)
  {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    PrintStream pout = new PrintStream(out);
    new Exception().printStackTrace(pout);

    BufferedReader din
        = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(out.toByteArray())));
    String targetLine = "  at CLASS.METHOD(FILE.java:0000)"; // ???
    try {
      for (int i=0; i<level; i++) {
        targetLine = din.readLine();
      }
      targetLine = din.readLine();
    }
    catch (IOException e) {
      System.out.println("Error reading printStackTrace output in jciError");
    }
//Normal targetLine =
//"   at jci.JPanelMaster.jButton1_actionPerformed(JPanelMaster.java:1027)"
//and produces
//Error: Test of jciError JPanelMaster, jButton1_actionPerformed, JPanelMaster.java, 1027
//Sometimes targetline =
//"   at jci.JPanelMaster.jButton1_actionPerformed(Compiled Code)    }"
    StringTokenizer tk = new StringTokenizer(targetLine, " \n\t.():");
    String[] tokens = new String[8];
    int i;
    for (i=0; i < 6; i++) tokens[i] = "?";
    tokens[6] = "0";
    for (i=0; tk.hasMoreTokens() && i < 8; i++) {
      tokens[i] = tk.nextToken();
    }

    // setup context
    Context context = new Context();
    context.className  = tokens[2];
    context.methodName = tokens[3];
    String file = tokens[4];
    String ext  = tokens[5];
    context.sourceFile = file + "." + ext;
    context.lineNumber = Integer.parseInt(tokens[6]);

    return context;
  } // end of getContext

} // end of class jciError

//==============================================================================
/**
 * A class to identify the location of an error
 */
class Context {
  public String className;
  public String methodName;
  public String sourceFile;
  public int lineNumber;
//------------------------------------------------------------------------------
  /**
   * returns className + ", " + methodName + ", " + sourceFile + ", " + lineNumber
   */
  public String toString () {
    return className + ", " + methodName + ", " + sourceFile + ", " + lineNumber;
  }
} // end of Context




