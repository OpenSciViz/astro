//Title:        JCIMotor class for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of motors in CanariCam infrared camera system.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import javaUFLib.*;
import javaUFProtocol.*;

/** 
 * Main motor class -- contains all visual components as
 * well as parameters associated with a motor
 */ 
public class JCIMotor extends JCIMotorComm {
    public static final
	String rcsID = "$Name:  $ $Id: JCIMotor.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";

    // Visual Components
    JCheckBox hiNameCheckBox; // for use in high & low level panels
    JCheckBox loNameCheckBox;
    JCheckBox paramsCheckBox; // for motor params panel
    JButton sendParmsButton; // for use in motor parameter panel
    UFLabel hiStatusLabel;
    UFLabel loStatusLabel; 
    UFComboBox namedLocationBox;
    JButton hiMoveButton;
    JButton loMoveButton;
    JButton hiDatumButton;
    JButton loDatumButton;
    JButton hiAbortButton;
    JButton loAbortButton;
    JButton originButton;
    UFTextField stepsToMoveField; // Steps to move for low level control panel
    UFTextField isField;   // Initial speed
    UFTextField tsField;   // terminal speed
    UFTextField accField;  // acceleration
    UFTextField decField;  // deceleration
    UFTextField dcField;   // drive current
    UFTextField hcField;   // hold current
    UFTextField blField;   // back lash
    UFTextField dsField;   // datum speed
    UFTextField fdsField;  // final datum speed
    JComboBox datumDirBox; // datum direction

    //hook to corresponding JCIMotorGUI object in JPanelMaster:
    protected JCIMotorGUI masterMotorGUI = null;
    protected boolean updateMasterGUI = true;

    //keep track of location and which motor this is:
    protected int currentSteps;
    protected int motorNumber=0;
    protected boolean isMoving=false;

    //if _allowDatum=false then Datuming of this motor depends on position of a different motor:
    protected boolean _allowDatum = true;

    //flags and info for allow/disallow Datum of a different motor depending on position of this motor:
    protected char indexorDatumControl; //other indexor to use with method setDatumState()
    protected String[] indexorDatumEnables;
    protected boolean _sentDisallowDatum = false;

    //array of transmission throughputs for each position:
    public float[] throughPuts;

    //strings used for hi/loStatusLabels text in StatusMonitor and in doLocate:
    final String fillchars = "_______";
    final String separator = "  ==  ";

    // Status monitor object for JCIMotor:
    StatusMonitor statusMon = null;
    int statusSleep = 10;
//--------------------------------------------------------------------------
// Inner class to monitor motor status (position, etc.) in separate thread
//--------------------------------------------------------------------------
    private class StatusMonitor extends Thread {
	private boolean keepRunning = false;
	private boolean didStart = false;
	private int sleepAmount;
	private int errCnt = 0;
	/**
	 *Public constructor. <p>
	 *{@link JCIMotorComm#statusSoc} should already be connected to host.
	 *<p>
	 *@param int sleepAmount : # of milliseconds to sleep after each status update
	 */
	public StatusMonitor(int sleepAmount) {
	    if( sleepAmount < 10 ) sleepAmount = 10;
	    this.sleepAmount = sleepAmount;
	    //create a status socket on the same host and port as the regular socket
	    if( createStatusSocket() ) {
		keepRunning = true;
		this.start();
		didStart = true;
	    }
	    else setStatusLabels("status monitor: Failed connecting to Motor Agent");
	}
	/**
	 * Monitoring method to be run in seperate thread to update motor status
	 * (ie, location, movement, etc)
	 */
	public synchronized void run() {
	    while( true ) {
		while( keepRunning ) {
		    try {
			String step = getMotorStatus( statusSoc ).trim(); //this blocks until status is recvd.

			if( step.toUpperCase().indexOf("ERR") >= 0 ) {
			    if( ++errCnt > 1 ) {
				setStatusLabels(step);
				keepRunning = false;
				errCnt = 0;
				if( jciFrame.motorStatus.getText().indexOf("ERR") < 0 )
				    jciFrame.motorStatus.setText("comm. ERROR ?  ");
			    }
			    else this.sleep( 100 );
			}
			else if( step.equals("OK") ) {
			    motionStatus(false);
			    String status = hiStatusLabel.getSubText(); //use last position.
			    if( status.indexOf("Moving:") >= 0 )
				status = status.substring(7);
			    setStatusLabels(status);
			    if( masterMotorGUI != null ) updateMasterGUI = true; //reset.
			}
			else if( step.equals("STUCK") ) {
			    motionStatus(false);
			    String status = hiStatusLabel.getSubText(); //use last position.
			    if( status.indexOf("Moving:") >= 0 )
				status = status.substring(7);
			    setStatusLabels( "Stuck ? at: " + status );
			    if( masterMotorGUI != null ) updateMasterGUI = true; //reset.
			}
			else {
			    motionStatus(true);
			    int decimal = step.indexOf(".");
			    if( decimal > 0 ) step = step.substring(0,decimal);
			    String posName = "?";

			    try {
				//currentSteps = (int)Math.round( Float.parseFloat( step ) );
				currentSteps = Integer.parseInt( step );
				posName = nearestPosition( currentSteps );
			    }
			    catch (NumberFormatException nfe) {}

			    int ipos = step.length();
			    if( ipos >= fillchars.length() ) ipos = fillchars.length()-1;
			    String spacer = fillchars.substring(ipos);
			    String status = "Moving:" + spacer + step + separator + posName;
			    setStatusLabels(status);
			    if( masterMotorGUI != null ) {
				if( updateMasterGUI ) masterMotorGUI.setActive( posName );
			    }
			    this.sleep(sleepAmount);
			}
		    }
		    catch (Exception e) {
			System.err.println("JCIMotor-"+Indexor+"::StatusMonitor.run> "+e.toString());
			try { this.sleep(1000); } catch( Exception x ) {}
		    }
		}
		//if not running the get status loop then sleep for a second...
		try { this.sleep(1000); } catch( Exception x ) {}
	    }
	}

	public void resumeRunning() {
	    keepRunning = true;
	    errCnt = 0;
	    if( !didStart ) {
		this.start();
		didStart = true;
	    }		
	}

	public void stopRunning() { keepRunning = false; }  
	public boolean isRunning() { return keepRunning;}
    }

//---------------------------------------------------------------------------
// Inner class to send a command to motor agent in separate thread
//---------------------------------------------------------------------------
    private class CommandThread extends Thread
    {
	private String motorCmd;
	private JCIMotor masterMotor=null;

	public CommandThread(String motorCmd) {
	    this.motorCmd = motorCmd;
	    this.start();
	}

	public CommandThread( String motorCmd, JCIMotor masterMotor ) {
	    this.motorCmd = motorCmd;
	    this.masterMotor = masterMotor;
	    this.start();
	}

	public synchronized void run()
	{
	    if( masterMotor != null ) {
		try{ sleep(1300); }catch( Exception x ){}
		System.out.println("JCIMotor-"+Indexor+"::CommandThread.run> perform " + motorCmd +
				   " when indexor " + masterMotor.Indexor + " motion is complete...");
		int maxTrys = 99, nTrys=0;
		while( masterMotor.isMoving && ++nTrys < maxTrys ) {
		    System.out.print( nTrys+" " );
		    try{ sleep(1000); }catch( Exception x ){}
		} 
		if( masterMotor.isMoving ) {
		    System.err.println("JCIMotor-"+Indexor+"::CommandThread.run> giving up on " + motorCmd +
				       " because indexor " + masterMotor.Indexor + " is still moving");
		    return;
		}
		System.out.println("JCIMotor-"+Indexor+"::CommandThread.run> perform " + motorCmd);
	    }
	    if( motorCmd.equals("Status") )
		doStatus();
	    else if( motorCmd.equals("Locate") )
		doLocate();
	    else if( motorCmd.equals("HiMove") )
		doHiMove();
	    else if( motorCmd.equals("LoMove") )
		doLoMove();
	    else if( motorCmd.equals("Abort") )
		doAbort();
	    else if( motorCmd.equals("Datum") )
		doDatum();
	    else if( motorCmd.equals("Origin") )
		doOrigin();
	    else if( motorCmd.equals("Params") )
		doParamSend();
	}
    }

    ////////////////
    //Constructors//
    ////////////////
    /**
     *Public constructor. <p>
     *Low-level motor parameters should be set manually. <p>
     *(do not rely on default values for motor parameters)
     *<p>
     *@param String name : Human readable motor name
     *@param char Indexor : Hardware motor name (single letter)
     *@param String host : hostname (or IP address) of motor agent
     *@param int port : port to connect to motor agent
     */
    public JCIMotor(String name, char Indexor, String host, int port) {
	//invoking the super constructor should take care of connecting the
	//socket to the motor agent and setting up default values for the
	//parameters
	super(name, Indexor, host, port);
	setupVisualComponents();
    }
    /** 
     *Public constructor
     *<p>
     *@param String name : Human readable motor name
     *@param char Indexor : Hardware motor name (single letter)
     *@param String host : hostname (or IP address) of motor agent
     *@param int port : port to connect to motor agent
     *@param int is : Initial Speed
     *@param int ts : Terminal Speed
     *@param int acc : Acceleration
     *@param int dec : Deceleration
     *@param int dc : Drive Current
     *@param int hc : Hold Current
     *@param int ds : Datum Speed
     *@param int fds : Final Datum Speed
     *@param int ddir : Datum Direction
     *@param int bl : Back Lash
     *@param int [] positions : array of high level position locations
     *@param String [] positionNames : array of high level position names (should be 1-1 mapping of
     *                               names to locations (see previous parameter definition))
     */
    public JCIMotor(String name, char Indexor, String host, int port, 
		    int is, int ts, int acc, int dec, int dc, int hc, int ds, int fds, 
		    int ddir, int bl, int[] positions, String[] positionNames) {
	super(name, Indexor, host,port, is,ts,acc,dec,dc,hc,ds,fds,ddir,bl, positions,positionNames);
	setupVisualComponents();
	setNamedLocations(positionNames,positions);
    }
    /**
     *Constructor helper function creates and initializes visual components
     *associated with this motor (ie, JButtons, JTextFields, etc) and sets up
     *action methods associated with button presses.
     */
    protected void setupVisualComponents() {
	hiStatusLabel = new UFLabel(Indexor+" >");
	loStatusLabel = new UFLabel(Indexor+" >");
	hiNameCheckBox = new JCheckBox(name,false);
	loNameCheckBox = new JCheckBox(name,false);
	paramsCheckBox = new JCheckBox(Indexor+" :",false);
	sendParmsButton = new JButton(name);
	namedLocationBox = new UFComboBox(null);
	hiMoveButton = new JButton("Move");
	loMoveButton = new JButton("Move");
	hiDatumButton = new JButton("Datum");
	loDatumButton = new JButton("Datum");
	hiAbortButton = new JButton("Abort");
	loAbortButton = new JButton("Abort");
	originButton = new JButton("Origin");
	stepsToMoveField = new UFTextField(Indexor+"-StepsToMove","0");
	isField = new UFTextField(Indexor+"-InitialSpeed",getInitialSpeed()+"");
	tsField = new UFTextField(Indexor+"-TerminalSpeed",getTerminalSpeed()+"");
	accField = new UFTextField(Indexor+"-Acceleration",getAcceleration()+"");
	decField = new UFTextField(Indexor+"-Deceleration",getDeceleration()+"");
	dcField = new UFTextField(Indexor+"-DriveCurrent",getDriveCurrent()+"");
	blField = new UFTextField(Indexor+"-BackLashSteps",getBackLash()+"");
	dsField = new UFTextField(Indexor+"-DatumSpeed",getDatumSpeed()+"");
	fdsField = new UFTextField(Indexor+"-FinalDatumSpeed",getFinalDatumSpeed()+"");
	hcField = new UFTextField(Indexor+"-HoldCurrent",getHoldCurrent()+"");
	datumDirBox = new JComboBox();
	//datumDirBox.addItem("");
	datumDirBox.addItem("0");
	datumDirBox.addItem("1");
	datumDirBox.setSelectedIndex(getDatumDirection());

	// setup actionListeners for buttons
	hiMoveButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doHiMove(); } });

	loMoveButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doLoMove(); } });

	hiDatumButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doDatum(); } });

	loDatumButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doDatum(); } });

	sendParmsButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doParamSend(); } });

	hiAbortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doAbort(); } });

	loAbortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doAbort(); } });

	originButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) { doOrigin(); } });

	hiNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    loNameCheckBox.setSelected(hiNameCheckBox.isSelected());
		    paramsCheckBox.setSelected(hiNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	loNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    hiNameCheckBox.setSelected(loNameCheckBox.isSelected());
		    paramsCheckBox.setSelected(loNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	paramsCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    hiNameCheckBox.setSelected(paramsCheckBox.isSelected());
		    loNameCheckBox.setSelected(paramsCheckBox.isSelected());
		    ableButtons();
		}
	    });
    }

    public void setMasterGUI( JCIMotorGUI motorGUI ) { masterMotorGUI = motorGUI; }

    public void updateMasterGUI( boolean update ) { updateMasterGUI = update; }

    //helper methods used by most action methods below:

    void setStatusLabels( String message ) {
	hiStatusLabel.setText(message);
	loStatusLabel.setText(message,false); //false = no beep on error since hiStatusLabel already beeped.
    }

    void indicateCommError() {
	String errmsg = "ERROR: Time-out  waiting  for  reply  from Agent";
	setStatusLabels(errmsg);
    }

    //////////////////
    //Action Methods//
    //////////////////
    public int doReconnect()
    {
	String errmsg = "Failed  connecting  to  Motor Agent";
	try { 
	    connectCmdSocket();
	    if( comSoc == null )
		System.out.println("JCIMotor-"+Indexor+"::doReconnect> Command socket Connection Failed!");

	    if (statusMon == null)
		statusMon = new StatusMonitor( statusSleep ); //this creates status socket connection.
	    else {
		statusMon.stopRunning();
		if( createStatusSocket() )
		    statusMon.resumeRunning();
		else {
		    System.out.println("JCIMotor-"+Indexor+"::doReconnect> Status socket Connection Failed!");
		    setStatusLabels(errmsg);
		    jciFrame.motorStatus.setText("ERROR  connecting  ");
		    return -1;
		}
	    }

	    if( comSoc == null || !statusMon.keepRunning ) {
		setStatusLabels(errmsg);
		jciFrame.motorStatus.setText("ERROR  connecting  ");
		return -1;
	    }

	    if( needParams() ) {
		doParamSend();
		setDatumState();
	    }
	    //false means no indexor query, just get last known location:
	    doLocate( false );
	    jciFrame.motorStatus.setText("IDLE       "); //status monitor will set to moving if it is...
	    return 0;
	}
	catch (Exception e) { 
	    System.out.println("JCIMotor-"+Indexor+"::doReconnect> " + e.toString());
	    setStatusLabels(errmsg);
	    jciFrame.motorStatus.setText("ERROR  connecting  ");
	    return -1;
	}
    }

    // method to check if Motor Control agent has recvd indexor/motor params at least once:

    public boolean needParams()
    {
	String[] s = new String[2];
	s[0] = "status";
	s[1] = Indexor + " params ?";
	UFProtocol reply = sendCommand(s);

	if( reply instanceof UFStrings ) {
	    String answer = ((UFStrings)reply).valData(0);
	    if( answer.indexOf("sim") > 0 ) {
		jciFrame.motorStatus.newPrefix("Motors (sim):");
		hiStatusLabel.newPrefix( Indexor+" (sim) >");
		loStatusLabel.newPrefix( Indexor+" (sim) >");
	    }
	    else {
		jciFrame.motorStatus.newPrefix("Motors:");
		hiStatusLabel.newPrefix( Indexor+" >");
		loStatusLabel.newPrefix( Indexor+" >");
	    }
	    if( answer.indexOf("YES") > 0 )
		return false;
	    else
		return true;
	}
	else return false; //if reply is bad do not bother sending params
    }

    // method to tell Motor Control agent the current allow/disallow Datum state:

    public boolean setDatumState() { return setDatumState( Indexor, _allowDatum ); }

    public boolean setDatumState( char indexor, boolean allowDatum )
    {
	String[] s = new String[2];
	s[0] = "config";
	s[1] = indexor + " Datum-Allowed=";
	if( allowDatum )
	    s[1] += "Yes";
	else
	    s[1] += "No";

	UFProtocol reply = sendCommand(s);

	if( reply instanceof UFStrings ) {
	    String answer = ((UFStrings)reply).valData(0);
	    System.out.println("JCIMotor-"+Indexor+".setDatumState> " + answer);
	    return true;
	}
	else {
	    System.err.println("JCIMotor-"+Indexor+".setDatumState> failed cmd: " + s[0] + s[1]);
	    return false; //if reply is bad do not bother sending params
	}
    }

    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then retrieves the 
     * currently selected index from the {@link JCIMotor#namedLocationBox} and uses the index to retrieve
     * the position associated with the high-level position name({@link JCIMotorParameters#positions}),
     * and finally calls the {@link JCIMotorComm#sendHiMove(int)} method from the JCIMotorComm class.
     */
    public void doHiMove() {
	int ipos = namedLocationBox.getSelectedIndex() - 1; //first name is always blank.
	if( ipos >= 0 ) {
	    if( positions[ipos] != currentSteps ) {
		UFProtocol reply;
		if( (reply = sendHiMove(positions[ipos])) == null )
		    indicateCommError();
		else
		    motionStatus(true);
	    }
	    else {
		namedLocationBox.setBackground( Color.white );
		if( masterMotorGUI != null ) {
		    if( updateMasterGUI ) masterMotorGUI.setActive( positionNames[ipos] );
		    updateMasterGUI = true; //reset.
		}
	    }
	}
    }
    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then retrieves the 
     *steps to move from the {@link #stepsToMoveField} TextField, and finally calls
     *the {@link JCIMotorComm#sendMove(int)} method from the JCIMotorComm class
     */
    public void doLoMove() {
	int stm;
	try {
	    stm = Integer.parseInt( stepsToMoveField.getText().trim() );
	} catch(NumberFormatException nfe) {
	    loStatusLabel.setText("ERROR: steps  input  is  not  a  number!");
	    return;
	}
	UFProtocol reply = sendMove(stm);
	if( reply == null )
	    indicateCommError();
	else {
	    motionStatus(true);
	    stepsToMoveField.setNewState();
	}
    }
    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then calls
     *the {@link JCIMotorComm#sendDatum()} method from the JCIMotorComm class
     */
    public void doDatum() {
	synchParametersWithVisuals();
	UFProtocol reply = sendDatum();
	if( reply == null )
	    indicateCommError();
	else {
	    if( reply instanceof UFStrings ) {
		String answer = ((UFStrings)reply).valData(0);
		if( answer.toUpperCase().indexOf("REJECT") >= 0 ) {
		    System.out.println("JCIMotor-"+Indexor+"::doDatum> " + answer);
		    setStatusLabels( answer );
		    return;
		}
	    }
	    motionStatus(true);
	}
    }
    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then calls
     *the {@link JCIMotorComm#sendParams()} method from the JCIMotorComm class
     */
    public void doParamSend() {
	synchParametersWithVisuals();
	UFProtocol reply = sendParams();
	if( reply == null )
	    indicateCommError();
	else {
	    isField.setNewState();
	    tsField.setNewState();
	    accField.setNewState();
	    decField.setNewState();
	    dcField.setNewState();
	    blField.setNewState();
	    dsField.setNewState();
	    fdsField.setNewState();
	}
    }
    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then calls
     *the {@link JCIMotorComm#sendAbort()} method from the JCIMotorComm class
     */
    public void doAbort() {
	UFProtocol reply;
	if( (reply = sendAbort()) == null ) indicateCommError();
    }
    /**
     *First calls {@link JCIMotor#synchParametersWithVisuals()}, then calls
     *the {@link JCIMotorComm#sendOrigin()} method from the JCIMotorComm class
     */
    public void doOrigin() {
	UFProtocol reply;
	if( (reply = sendOrigin()) == null ) indicateCommError();
    }
    /**
     * Cmd agent to query and report motor motion status and set boolean isMoving accordingly.
     */
    public boolean doStatus() {
	UFProtocol reply = queryStatus();
	if( reply instanceof UFStrings ) {
	    String answer = ((UFStrings)reply).valData(0);
	    int carrot = answer.indexOf("^");
	    if( carrot > 0 ) {
		int mstatus = Integer.parseInt( answer.substring(carrot) );
		if( mstatus > 0 )
		    isMoving = true;
		else
		    isMoving = false;
	    }
	    return true; //just to indicate that UFStrings reply was recvd.
	}
	else {
	    indicateCommError();
	    return false; //to indicate that bad or null reply was recvd.
	}
    }
    /**
     * Cmd MC agent to query indexors, report motor position and update status Labels.
     * If queryIndexor = false then MC agent just reports last known location (no indexor query).
     */
    public boolean doLocate() { return doLocate( true ); } //default is to actually query indexor.

    public boolean doLocate( boolean queryIndexor ) {
	String step = getCurrentLocation( queryIndexor );
	if( step == null ) {
	    indicateCommError();
	    return false;
	}
	String posName = "?";
	int decimal = step.indexOf(".");
	if( decimal > 0 ) step = step.substring(0,decimal);
	try {
	    //currentSteps = (int)Math.round( Float.parseFloat( step ) );
	    currentSteps = Integer.parseInt( step );
	    posName = nearestPosition( currentSteps );
	}
	catch (NumberFormatException nfe) {}
	int ipos = step.length();
	if( ipos >= fillchars.length() ) ipos = fillchars.length()-1;
	String spacer = fillchars.substring(ipos);
	String Loc = spacer + step + separator + posName;
	setStatusLabels(Loc);
	if( masterMotorGUI != null ) masterMotorGUI.checkStatus(); //checks hiStatusLabel for Master Panel.
	return true;
    }
    /**
     * special methods to perform move, datum, etc. tasks in seperate threads:
     */
    public void doHiMoveThread() {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::HiMove> motor is already moving!");
	else {
	    CommandThread cmdthread = new CommandThread("HiMove");
	}
    }
    public void doHiMoveWhenReady( JCIMotor masterMotor ) {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::HiMove> motor is already moving!");
	else {
	    CommandThread cmdthread = new CommandThread("HiMove", masterMotor);
	}
    }
    public void doLoMoveThread() {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::LoMove> motor is already moving!");
	else {
	    CommandThread cmdthread = new CommandThread("LoMove");
	}
    }
    public void doDatumThread() {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::Datum> motor is already moving!");
	else {
	    CommandThread cmdthread = new CommandThread("Datum");
	}
    }

    public void doDatumWhenReady( JCIMotor masterMotor ) {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::Datum> motor is already moving!");
	else {
	    CommandThread cmdthread = new CommandThread("Datum", masterMotor);
	}
    }
    public void doOriginThread() {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::Origin> not allowed while motor is moving!");
	else {
	    CommandThread cmdthread = new CommandThread("Origin");
	}
    }
    public void doParamsThread() {
	if( isMoving )
	    System.err.println("JCIMotor-"+Indexor+"::sendParams> not allowed while motor is moving!");
	else {
	    CommandThread cmdthread = new CommandThread("Params");
	}
    }
    public void doAbortThread() {
	if( isMoving ) {
	    CommandThread cmdthread = new CommandThread("Abort");
	}
    }
    public void doLocateThread() { CommandThread cmdthread = new CommandThread("Locate"); }
    public void doStatusThread() { CommandThread cmdthread = new CommandThread("Status"); }
    /**
     *Enable/Disable buttons based on the state of {@link #hiNameCheckBox} and
     *{@link #loNameCheckBox} (which are always equal).
     */
    public void ableButtons() {
	hiMoveButton.setEnabled(hiNameCheckBox.isSelected());
	loMoveButton.setEnabled(hiNameCheckBox.isSelected());
	hiDatumButton.setEnabled(hiNameCheckBox.isSelected());
	loDatumButton.setEnabled(hiNameCheckBox.isSelected());
	hiAbortButton.setEnabled(hiNameCheckBox.isSelected());
	loAbortButton.setEnabled(hiNameCheckBox.isSelected());
	originButton.setEnabled(hiNameCheckBox.isSelected());
	namedLocationBox.setEnabled(hiNameCheckBox.isSelected());
	sendParmsButton.setEnabled(hiNameCheckBox.isSelected());
	//make sure check boxes are always enabled for user:
	hiNameCheckBox.setEnabled(true);
	loNameCheckBox.setEnabled(true);
	paramsCheckBox.setEnabled(true);
    }

    /**
     * Enable/Disable buttons based on motion of motors,
     * and keep track of which motors are moving via jciFrame.motorsMoving,
     * then setting jciFrame.motorStatus to "MOVING" or "IDLE".
     * @param isMotion true will disable buttons, false will enable them
     */
    public void motionStatus(boolean isMotion)
    {
	if( hiNameCheckBox.isSelected() ) {    //do not disable/enable buttons if not selected.
	    hiMoveButton.setEnabled(!isMotion);
	    loMoveButton.setEnabled(!isMotion);
	    hiDatumButton.setEnabled(!isMotion);
	    loDatumButton.setEnabled(!isMotion);
	    originButton.setEnabled(!isMotion);
	    sendParmsButton.setEnabled(!isMotion);
	    namedLocationBox.setEnabled(!isMotion);
	}

	if( isMotion ) {
	    if( indexorDatumControl != ' ' && !_sentDisallowDatum ) {
		if( setDatumState( indexorDatumControl, false ) ) _sentDisallowDatum = true;
	    }
	    jciFrame.motorsMoving[motorNumber] = 1;
	}
	else {
	    if( indexorDatumEnables[nearestPosIndex] != null && Math.abs( nearestPosDistance ) < 10 ) {
		_sentDisallowDatum = false;
		setDatumState( indexorDatumControl, true );
	    }
	    jciFrame.motorsMoving[motorNumber] = 0;
	}

	isMoving = isMotion;
	namedLocationBox.setNewState( hiStatusLabel.getText(), separator );
	int numberMoving=0;
	for( int i=0; i < jciFrame.motorsMoving.length; i++ ) numberMoving += jciFrame.motorsMoving[i];

	if( numberMoving > 0 ) {
	    jciFrame.motorStatus.setText( numberMoving + "  MOVING  ");
	    if( jciFrame.camStatus.getText().indexOf("IDLE") >= 0 )
		jciFrame.camStatus.setText("WAIT        ");
	}
	else {
	    jciFrame.motorStatus.setText("IDLE       ");
	    jciFrame.camStatus.setText("IDLE        ");
	}
    }

    /**
     *Reads values from visual components and sets the corresponding motor parameters.
     *This method updates the following motor parameters: <p>
     *Initial Speed, Terminal Speed, Acceleration, Deceleration, DriveCurrent,
     *Datum Speed, Back Lash, and Datum Direction.
     */
    public void synchParametersWithVisuals() {
	try {
	    super.setInitialSpeed(Integer.parseInt(isField.getText()));
	    super.setTerminalSpeed(Integer.parseInt(tsField.getText()));
	    super.setAcceleration(Integer.parseInt(accField.getText()));
	    super.setDeceleration(Integer.parseInt(decField.getText()));
	    super.setDriveCurrent(Integer.parseInt(dcField.getText()));
	    super.setBackLash(Integer.parseInt(blField.getText()));
	    int i = datumDirBox.getSelectedIndex();
	    super.setDatumDirection(i);
	    super.setDatumSpeed(Integer.parseInt(dsField.getText()));
	    super.setFinalDatumSpeed(Integer.parseInt(fdsField.getText()));
	    //	    super.setHoldCurrent(Integer.parseInt(hcField.getText()));
	} catch (Exception e) {
	    System.err.println("JCIMotor-"+Indexor+".synchParametersWithVisuals> "+e.toString());
	}
    }

    ////////////////////////////////////////////////////////////////
    //Mutator methods for visual components//
    ////////////////////////////////////////////////////////////////
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setInitialSpeed(int)}, then
     *sets the text in {@link #isField}
     *<p>
     *@param int speed : Initial speed for this motor
     */
    public void setInitialSpeed(int speed) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setInitialSpeed(speed);
	isField.setText(speed+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setTerminalSpeed(int)}, then
     *sets the text in {@link #tsField}
     *<p>
     *@param int speed : Terminal speed for this motor
     */
    public void setTerminalSpeed(int speed) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setTerminalSpeed(speed);
	tsField.setText(speed+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setAcceleration(int)}, then
     *sets the text in {@link #accField}
     *<p>
     *@param int acc : Acceleration for this motor
     */
    public void setAcceleration(int acc) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setAcceleration(acc);
	accField.setText(acc+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setDeceleration(int)}, then
     *sets the text in {@link #decField}
     *<p>
     *@param int dec : Deceleration for this motor
     */
    public void setDeceleration(int dec) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setDeceleration(dec);
	decField.setText(dec+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setHoldCurrent(int)}, then
     *sets the text in {@link #hcField}
     *<p>
     *@param int current : Hold current for this motor
     */
    public void setHoldCurrent(int current) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setHoldCurrent(current);
	hcField.setText(current+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setDriveCurrent(int)}, then
     *sets the text in {@link #dcField}
     *<p>
     *@param int current : Drive current for this motor
     */
    public void setDriveCurrent(int current) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setDriveCurrent(current);
	dcField.setText(current+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setDatumSpeed(int)}, then
     *sets the text in {@link #dsField}
     *<p>
     *@param int speed : Datum speed for this motor
     */
    public void setDatumSpeed(int speed) {
	// must synchronize visual components with internal (inherited) parameters
	super.setDatumSpeed(speed);
	dsField.setText(speed+"");
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setFinalDatumSpeed(int)}, then
     *sets the text in {@link #fdsField}
     *<p>
     *@param int speed : Final datum speed for this motor
     */
    public void setFinalDatumSpeed(int speed) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setFinalDatumSpeed(speed);
	fdsField.setText(speed+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setBackLash(int)}, then
     *sets the text in {@link #blField}
     *<p>
     *@param int lash : Back lash for this motor
     */
    public void setBackLash(int lash) { 
	// must synchronize visual components with internal (inherited) parameters
	super.setBackLash(lash);
	blField.setText(lash+""); 
    }
    /**
     *Mutator method. First calls {@link JCIMotorParameters#setDatumDirection(int)}, then
     *sets the appropriate index for {@link #datumDirBox}
     *<p>
     *@param int dir: Datum direction.  Valid values are 0 and 1.
     */
    public void setDatumDirection(int dir) {
	// must synchronize visual components with internal (inherited) parameters
	if (dir != 0 && dir != 1) {
	    System.err.println("JCIMotor-"+Indexor+".setDatumDirection> Dir should be 0 or 1, not "+dir);
	    return;
	}
	super.setDatumDirection(dir);
	datumDirBox.setSelectedIndex(dir);
    }
    /**
     *Mutator method. Checks to make sure that names and positions are non-NULL and same length.
     *Then calls {@link JCIMotorParameters#setMotorPositions(int [],boolean)} and 
     *{@link JCIMotorParameters#setPositionNames(String [],boolean)} (deep copy versions. If shallow
     *copies are desired, this method should be overloaded and the shallow copy versions of these two 
     *methods should be called).  Finally, all entries in {@link #namedLocationBox} are deleted and 
     *replaced by names.
     *<p>
     *@param String [] names  : Array of strings representing high-level position names
     *@param int [] positions : Array of integers representing hihg-level position locations
     */
    public void setNamedLocations(String [] names, int [] positions) {
	if (names == null || positions == null 
	    || names.length != positions.length) {
	    System.err.println("JCIMotor-"+Indexor+".setNamedLocations> Bad parameters");
	    return;
	}
	namedLocationBox.removeAllItems();
	setMotorPositions(positions,false); // deep copy
	setPositionNames(names,false); // deep copy
	namedLocationBox.addItem(" ");

	for (int i=0; i<names.length; i++) {
	    namedLocationBox.addItem(new String(names[i]));
	}
    }

    /////////////////////////
    //Visual helper methods//
    /////////////////////////

    /** 
     *Helper method.  This method arranges all parameter-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all parameter-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorParameterPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(1,0));
	panel.setLayout(new RatioLayout());
	fieldPanel.add(isField);
	fieldPanel.add(tsField);
	fieldPanel.add(accField);
	fieldPanel.add(decField);
	fieldPanel.add(dcField);
	fieldPanel.add(blField);
	fieldPanel.add(dsField);
	fieldPanel.add(fdsField);
	fieldPanel.add(datumDirBox);
	panel.add("0.00,0.02;0.04,0.45", paramsCheckBox);
	panel.add("0.04,0.02;0.13,0.45", sendParmsButton);
	panel.add("0.18,0.02;0.82,0.45", fieldPanel);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor. Rather, it should be called only once by
     * the application class when creating a motor parameter panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to
     * the JPanel returned by {@link #getMotorParameterPanel()}
     */ 
    public static JPanel getMotorParameterLabelPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(1,0));
	panel.setLayout(new RatioLayout());
	fieldPanel.add(new JLabel("Initial Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Term Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Acceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Deceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Drive Current",JLabel.CENTER));
	fieldPanel.add(new JLabel("Back Lash",JLabel.CENTER));
	fieldPanel.add(new JLabel("Datum Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Final Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Datum Dir",JLabel.CENTER));
	panel.add("0.01,0.01;0.16,0.99", new JLabel("Indexor:    Motor",JLabel.LEFT));
	panel.add("0.18,0.01;0.82,0.99", fieldPanel);
	return panel;
    }

    /** 
     *Helper method.  This method arranges all low-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all low-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorLowLevelPanel() {
	JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	buttonPanel.setLayout(new GridLayout(1,0));
	buttonPanel.add(loMoveButton);
	buttonPanel.add(loDatumButton);
	buttonPanel.add(loAbortButton);
	buttonPanel.add(originButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.00,0.02;0.15,0.99", loNameCheckBox);
	panel.add("0.17,0.02;0.14,0.99", stepsToMoveField);
	panel.add("0.32,0.02;0.37,0.99", buttonPanel);
	panel.add("0.69,0.02;0.30,0.99", loStatusLabel);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by
     * the applicationclass when creating a motor low-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to
     * the JPanel returned by {@link #getMotorLowLevelPanel()}
     */ 
    public static JPanel getMotorLowLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.CENTER));
	panel.add("0.17,0.02;0.14,0.99", new JLabel("Steps to Move",JLabel.CENTER));
	panel.add("0.69,0.02;0.30,0.99", new JLabel("Status",JLabel.LEFT));
	return panel;
    }
    /** 
     *Helper method.  This method arranges all high-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all high-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorHighLevelPanel() {
	JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	buttonPanel.setLayout(new GridLayout(1,0));
	buttonPanel.add(hiMoveButton);
	buttonPanel.add(hiDatumButton);
	buttonPanel.add(hiAbortButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.00,0.02;0.15,0.99", hiNameCheckBox);
	panel.add("0.17,0.02;0.14,0.99", namedLocationBox);
	panel.add("0.32,0.02;0.34,0.99", buttonPanel);
	panel.add("0.66,0.02;0.33,0.99", hiStatusLabel);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor. Rather, it should be called only once by
     * the application class when creating a motor high-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to
     * the JPanel returned by {@link #getMotorHighLevelPanel()}
     */ 
    public static JPanel getMotorHighLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.CENTER));
	panel.add("0.17,0.02;0.14,0.99", new JLabel("Named Location",JLabel.CENTER));
	panel.add("0.66,0.02;0.33,0.99", new JLabel("Status",JLabel.LEFT));
	return panel;
    }

    /////////////////////////////////
    ///////// STATIC METHODS/////////
    /////////////////////////////////
    public static JCIMotor [] createMotors(String filename) {
	int Num_motors ;
	String in_line ;
	try {
	    // read data file and assign motor names and positions
	    TextFile f = new TextFile(jci.data_path + filename, TextFile.IN);
	    //String host = f.nextUncommentedLine();
	    //int port = Integer.parseInt(f.nextUncommentedLine());
	    String host = jciFrame.hostName;
	    int port = 52005;
	    Num_motors = Integer.parseInt( f.nextUncommentedLine().trim() );
	    System.out.println("JCIMotor.createMotors> " + Num_motors + " motors...");
	    JCIMotor[] motors = new JCIMotor[Num_motors];

	    for (int i=0; i<Num_motors; i++) {
		String beginRec = f.nextUncommentedLine();
		if (!beginRec.equals("BEGIN_REC")) {
		    jciError.show("Error in motor parameter file? Expected 'BEGIN_REC', found:" + beginRec);
		    break;
		}

		//next line is indexor/motor params: IS  TS  A  D  HC  DC  AN DS FDD DD BL Name allowDatum
		StringTokenizer st = new StringTokenizer(f.nextUncommentedLine()," ");
		int isp = Integer.parseInt(st.nextToken());
		int tsp = Integer.parseInt(st.nextToken());
		int acc = Integer.parseInt(st.nextToken());
		int dec = Integer.parseInt(st.nextToken());
		int hcur = Integer.parseInt(st.nextToken());
		int dcur = Integer.parseInt(st.nextToken());
		char Indexor = (st.nextToken()).charAt(0);
		int dsp = Integer.parseInt(st.nextToken());
		int fdsp = Integer.parseInt(st.nextToken());
		int ddir = Integer.parseInt(st.nextToken());
		int blash = Integer.parseInt(st.nextToken()); 
		String mot_name = new String(st.nextToken());
		boolean allowDatum = true;

		while( st.hasMoreTokens() ) { //check for disallow datum directive:
		    String token = st.nextToken();
		    if( token.toUpperCase().indexOf("DATUM") >= 0 ) {
			if( token.toUpperCase().indexOf("NO") >= 0 ) allowDatum = false;
		    }
		    else mot_name += " " + token;
		}

		System.out.println("JCIMotor.createMotors> " + Indexor + " = " + mot_name );
		String posStr = f.nextUncommentedLine();

		if (!posStr.equals("POSITIONS")) {
		    jciError.show("Error in motor parameter file? Expected 'POSITIONS', found:" + posStr);
		    break;
		}

		int num_positions = Integer.parseInt(f.nextUncommentedLine().trim());
		int[] positions = new int[num_positions];
		String[] positionNames = new String[num_positions];
		String[] datumEnables = new String[num_positions];
		float[] throughPut = new float[num_positions];
		char indexorToControl = ' ';

		for (int j=0; j<num_positions; j++) {
		    StringTokenizer tr = new StringTokenizer(f.nextUncommentedLine()," ");
		    tr.nextToken(); // skip position number
		    positions[j] = Integer.parseInt(tr.nextToken());
		    throughPut[j] = Float.parseFloat(tr.nextToken());
		    positionNames[j] = tr.nextToken();
		    if( tr.hasMoreTokens() ) {
			datumEnables[j] = tr.nextToken();
			int dash = datumEnables[j].indexOf("-");
			if( dash > 0 ) {
			    String indexor = datumEnables[j].substring(dash);
			    indexorToControl = indexor.charAt(1); //first char after dash must be indexor.
			}
			else jciError.show("Error in motor param. file? Expected '-' in:" + datumEnables[j]);
		    }
		    else datumEnables[j] = null;
		}

		String nextLine = f.nextUncommentedLine();

		if( nextLine.indexOf("GRATING_PARAMS") > 0 ) {
		    //This record defines parameters for each of the high-resolution gratings:
		    int Num_hrgs = Integer.parseInt( f.nextUncommentedLine().trim() );
		    System.out.println("JCIMotor.createMotors> config " + Num_hrgs + " high-res gratings:");
		    String[] hrgNames = new String[Num_hrgs];
		    int[] fiducialSteps = new int[Num_hrgs];
		    double[] fiducialAngles = new double[Num_hrgs];
		    for( int k=0; k < Num_hrgs; k++ ) {
			String hrgParams = f.nextUncommentedLine();
			System.out.println("JCIMotor.createMotors> " + hrgParams );
			StringTokenizer gt = new StringTokenizer(hrgParams," ");
			hrgNames[k] = gt.nextToken();
			fiducialSteps[k] = Integer.parseInt( gt.nextToken() );
			fiducialAngles[k] = Double.parseDouble( gt.nextToken() );
		    }
		    //now record is finished, create grating motor object:
		    motors[i] = new JCIMotorGrating( mot_name, Indexor, host,port, isp,tsp, acc,dec,
						     dcur,hcur,dsp,fdsp,ddir,blash, positions, positionNames,
						     hrgNames, fiducialSteps, fiducialAngles );
		    nextLine = f.nextUncommentedLine();
		}
		else { //record is finished, create new motor object:
		    motors[i] = new JCIMotor( mot_name, Indexor, host,port, isp,tsp, acc,dec,
					      dcur,hcur,dsp,fdsp,ddir,blash, positions, positionNames );
		}

		//define standard attributes:
		motors[i].motorNumber = i;
		motors[i]._allowDatum = allowDatum;
		motors[i].ableButtons();
		motors[i].throughPuts = throughPut;
		motors[i].indexorDatumEnables = datumEnables;
		motors[i].indexorDatumControl = indexorToControl;

		if( !nextLine.equals("END_REC") ) {
		    jciError.show("Error in motor parameter file? Expected 'END_REC', found:"+nextLine);
		    break;
		}
	    }

	    f.close();
	    return motors;
	}
	catch (Exception eee) {
	    jciError.show("Possible error in motor parameter file: "+ eee.toString());
	    eee.printStackTrace();
	}
	return null;
    }
}
