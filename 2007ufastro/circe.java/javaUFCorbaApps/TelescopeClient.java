import CCS.*;
import DAF.*;
import TelescopeServer.*;

import org.omg.CORBA.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;

public class TelescopeClient {

    public static void printBindings(NamingContextExt ncRef) {
	BindingListHolder bl = new BindingListHolder();
	BindingIteratorHolder blIt= new BindingIteratorHolder();
	ncRef.list(1000, bl, blIt);	
	Binding bindings[] = bl.value;
	for (int i=0; i < bindings.length; i++) {
	    if (bindings[i] == null) continue;
	    int lastIx = bindings[i].binding_name.length-1;
	    
	    // check to see if this is a naming context
	    if (bindings[i].binding_type == BindingType.ncontext) {
		System.out.println( "Context: " + 
				    bindings[i].binding_name[lastIx].id);
	    } else {
		System.out.println("Object: " + 
				   bindings[i].binding_name[lastIx].id);
	    }
	}
    }

    public static Telescope_ifce getTelescopeServerReference(String args[]) {
	try {
	    // create and initialize the ORB
	    ORB orb = ORB.init(args,null);

	    // get the root naming context
	    org.omg.CORBA.Object objRef = 
		orb.resolve_initial_references("NameService");
	    // Use NamingContextExt instead of NamingContext. This is
	    // part of the Interoperable naming Service.
	    NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);
       
	    // resolve the Object Refernce in Naming
	    String name = "Telescope.GCS";
	  
	    Telescope_ifce telescope_ifce //=null;
		= Telescope_ifceHelper.narrow(ncRef.resolve_str(name));
	    return telescope_ifce;
	} catch (NotFound nfe) {
	    System.out.println(nfe.toString());
	    //System.out.println(nfe.why.value());
	    //System.out.println(NotFoundReason.missing_node.value());
	    //for (int i=0; i<nfe.rest_of_name.length; i++) {
	    //	System.out.print(nfe.rest_of_name[i].id+" ");
	    //	System.out.println(nfe.rest_of_name[i].kind);
	    //}
	    return null;
	} catch (Exception e) {
	    System.out.println( e.toString());
	    e.printStackTrace(System.out);
	    return null;
	}
    }
	
    public static void main (String args[] ) {
	String host = "newton";
	int port = 12344;
	if (args.length == 0) {
	    args = new String[2];
	    args[0] = "-ORBInitRef";
	    args[1] = "NameService=corbaloc:iiop:"+host+":"+port+
		"/NameService";
	} else {
	    if (CMD.findArg(args,"-host")) {
		host = CMD.getArg(args,"-host");
		args = CMD.stripArg(args,"-host",true);
	    }
	    if (CMD.findArg(args,"-port")) {
		port = Integer.parseInt(CMD.getArg(args,"-port"));
		args = CMD.stripArg(args,"-port",true);
	    }
	    if (CMD.findArg(args,"-ORBInitRef","NameService")) {
		args = CMD.stripArg(args,"-ORBInitRef","NameService");
	    }
	    args = CMD.addArg(args,"-ORBInitRef","NameService=corbaloc:"+
			      "iiop:"+host+":"+port+"/NameService");
	}
	Telescope_ifce telescope_ifce = getTelescopeServerReference(args);
	try {
	    telescope_ifce.requestTelescopeOffset(0.0,0.0);
	} catch (DAF.GCSException gcse) {
	    System.out.println(gcse.toString());
	}
	
    }

}
