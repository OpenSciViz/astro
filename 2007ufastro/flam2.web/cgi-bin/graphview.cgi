#!/usr/bin/perl -w

#use strict;

use lib '/astro/homes/drashkin/local';

use GD;
use CGI;
use GD::Polyline;
use Text::ParseWords;

BEGIN {
    $ENV{PATH} = '/bin:/usr/bin:/usr/local/bin';
    delete @ENV{ qw( IFS CDPATH ENV BASH_ENV ) };
}

use constant ORIGIN_X_COORD => 30;
use constant GRAPH_SIZE => 400;
use constant IMAGE_SIZE => 512;
use constant ORIGIN_Y_COORD => IMAGE_SIZE - (IMAGE_SIZE - GRAPH_SIZE) / 2;
use constant TICK_LENGTH => 3;

use constant AXIS_COLOR => ( 0, 0, 0 );
use constant TEXT_COLOR => ( 0, 0, 0 );
use constant BG_COLOR => ( 255, 255, 255 );
use constant AREA_COLOR => ( 255, 0, 0 );

$MAX_TEMP = 400;
$MIN_TEMP = 0;
$MAX_TIME = 10;
$MIN_TIME = 0;

binmode STDOUT;

@nums = (); 

@polylines = () ;

@checkboxes = ();

$count = 0;

my $q = new CGI;

$MAX_TEMP = $q->param("MAXTEMP");
if ($MAX_TEMP == 0) { $MAX_TEMP = 400;}
$MIN_TEMP = $q->param("MINTEMP") ;
if ($MIN_TEMP == 0) { $MIN_TEMP = 0;}
$MAX_TIME = $q->param("MAXTIME");
if ($MAX_TIME == 0) { $MAX_TIME = 10;}
$MIN_TIME = $q->param("MINTIME");
if ($MIN_TIME == 0) { $MIN_TIME = 0;}

$autoscale = $q->param("AUTOSCALE");

$checkboxes[0] = $q->param("MOSVAC");
$checkboxes[1] = $q->param("CAMVAC");
$checkboxes[2]  = $q->param("MOSCOLD1");
$checkboxes[3]  = $q->param("MOSBENC2");
$checkboxes[4]  = $q->param("MOSBENC3");
$checkboxes[5]  = $q->param("MOSBENC4");
$checkboxes[6]  = $q->param("CAMCOLD5");
$checkboxes[7]  = $q->param("CAMBENC6");
$checkboxes[8]  = $q->param("CAMBENC7");
$checkboxes[9]  = $q->param("CAMBENC8");
$checkboxes[10]  = $q->param("CAMBENCA");
$checkboxes[11]  = $q->param("CAMDETCB");


for (my $i = 0; $i < 12; $i++) {
    $polylines[$i] = new GD::Polyline;
}

$TIME_INTERVAL = 5;

$divValue = int(($MAX_TIME - $MIN_TIME) / $TIME_INTERVAL);
if ($divValue == 0) {
    $divValue = 1;
}
$numLines = $divValue + 1;

my @ootput = `tail -n $numLines /astro/data/doradus0/data/environment/current`;

$maxDataValue = -1;
$minDataValue = 1000;

for ($count = 0; $count < $numLines; $count++) {
    @nums = get_nums($ootput[$count]);
    add_to_polylines( \@nums );
}


print $q->header( -type => "image/jpeg", -expires => "-1d" );


print area_graph();


exit;






sub get_nums {
    my @data = shift;
    my @nums = parse_line(",",0,@data);
    
    #print STDERR "@nums\n";

    return @nums;
}

sub add_to_polylines {
    my $data = shift;
    for (my $i = 2; $i < 13 + 1; $i++) {
	#$polylines[$i-1]->addPt( 0, GRAPH_SIZE);
	if (lc($checkboxes[$i-2]) eq "on"){
	    $polylines[$i-2]->addPt( $count * GRAPH_SIZE / $divValue, GRAPH_SIZE - ( GRAPH_SIZE / ($MAX_TEMP - $MIN_TEMP) * ($$data[$i] - $MIN_TEMP)));
	    if (lc($autoscale) eq "on") {
		if ($$data[$i] > $maxDataValue) { $maxDataValue = $$data[$i]; }
		if ($$data[$i] < $minDataValue) { $minDataValue = $$data[$i]; }
	    }
	}	
        #my $asdf = GRAPH_SIZE - ( GRAPH_SIZE / MAX_TEMP * $$data[$i]);
	#print STDERR "$$data[$i] : $asdf\n";
    }

}

sub area_graph {
#    my $data = shift;
    my $image = new GD::Image( IMAGE_SIZE, IMAGE_SIZE );
    my $subimage = new GD::Image( GRAPH_SIZE, GRAPH_SIZE );
    my $bgcolor = $image->colorAllocate( BG_COLOR );
    my $bgcolor2= $subimage->colorAllocate( BG_COLOR );
    my $textcolor = $image->colorAllocate( TEXT_COLOR );
    my $areacolor = $subimage->colorAllocate( AREA_COLOR );
    my $axiscolor = $image->colorAllocate( AXIS_COLOR );

    my @linecolor = ();
    for (my $i = 0; $i < 12; $i++) {
	$linecolor[$i] = $subimage->colorAllocate( 255 - ( $i / 12 * 255) , ( $i / 12 * 255) ,  255 - ( $i / 12 * 255) );
    }

    for (my $i = 0; $i < 12; $i++ ) {
	if (lc($checkboxes[$i]) eq "on"){
	    if (lc($autoscale) eq "on") {
		$polylines[$i]->offset(0 , (-($MAX_TEMP - $maxDataValue) - ($MIN_TEMP - $minDataValue)));
		#$polylines[$i]->scale(1, , 0, 0);
	    }
	    $subimage->polydraw( $polylines[$i], $linecolor[$i] );
	}
    }
   
#    my $polyline = new GD::Polyline;
#
#    for (my $i = 1; $i < @$data; $i++) {
#	$polyline->addPt( $i, $$data[$i] );
#    }
#
#
#    $polyline->scale(  GRAPH_SIZE / MAX_TIME , - GRAPH_SIZE / MAX_TEMP, 0, 0 );
#    $polyline->offset( 0, GRAPH_SIZE );




#add polygon

#    $subimage->polydraw( $polyline, $areacolor );
    $subimage->transparent($bgcolor2);

    $image->string(gdSmallFont,0,15,"Temperature (K)",$textcolor);

#add x axis
    $image->line(ORIGIN_X_COORD, ORIGIN_Y_COORD,
		 ORIGIN_X_COORD + GRAPH_SIZE, ORIGIN_Y_COORD,
		 $axiscolor);
    $image->string(gdSmallFont,ORIGIN_X_COORD,ORIGIN_Y_COORD + 10,"-$MAX_TIME",$textcolor);
    $image->string(gdSmallFont,ORIGIN_X_COORD + GRAPH_SIZE,ORIGIN_Y_COORD + 10,"-$MIN_TIME",$textcolor);
#add y axis
    $image->line(ORIGIN_X_COORD, ORIGIN_Y_COORD,
		 ORIGIN_X_COORD, ORIGIN_Y_COORD - GRAPH_SIZE,
		 $axiscolor);
    $image->string(gdSmallFont,ORIGIN_X_COORD - 20,ORIGIN_Y_COORD,"$MIN_TEMP",$textcolor);
    $image->string(gdSmallFont,ORIGIN_X_COORD - 20,ORIGIN_Y_COORD - GRAPH_SIZE,"$MAX_TEMP",$textcolor);
    $image->string(gdSmallFont,ORIGIN_X_COORD + GRAPH_SIZE / 4,ORIGIN_Y_COORD + 10,"Time ( <- Seconds in the past )",$textcolor);
    
    $image->copy($subimage, ORIGIN_X_COORD, ORIGIN_Y_COORD - GRAPH_SIZE, 0, 0, GRAPH_SIZE, GRAPH_SIZE);

    $image->transparent($bgcolor);
    
    return $image->jpeg;
}
