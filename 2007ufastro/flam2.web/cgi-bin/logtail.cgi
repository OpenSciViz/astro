#!/usr/bin/perl -wT

use CGI;

BEGIN {
    $ENV{PATH} = '/bin:/usr/bin:/usr/local/bin';
    delete @ENV{ qw( IFS CDPATH ENV BASH_ENV ) };
}


my $q = new CGI;

print $q->header(-Refresh=>'10; URL=http://www.astro.ufl.edu/~drashkin/cgi-bin/logtail.cgi', -type => "text/html");

my $headly = `head -n 1 /astro/data/doradus0/data/environment/current`;
my @begintime = split(/ /, $headly);
my $timestamp = $begintime[2];
my ($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime($timestamp);
$mo = $mo + 1;
$y = $y +1900;
if ($s < 10) {$s = "0"."$s";}
if ($m < 10) {$m = "0"."$m";}
if ($h < 10) {$h = "0"."$h";}

print "Logging Began at: $mo-$md-$y $h:$m:$s<br>";

my $ootput = `tail -n 100 /astro/data/doradus0/data/environment/current`;

print "<table border=\"5\" frame=\"vsides\" rules=\"cols\"><tr><td>";
#print "Date</td><td>Sec Offst</td><td>CamBencA</td><td>CamDetcB</td><td>MosVac</td><td>CamVac</td><td>MosCold1</td><td>MosBenc2</td><td>MosBenc3</td><td>MosBenc4</td><td>CamCold5</td><td>CamBenc6</td><td>CamBenc7</td><td>CamBenc8</td></tr><tr><td>";
print "Date</td><td>Sec Offst</td><td>MosVac</td><td>CamVac</td><td>Mos 218 1=Stg 1</td><td>Mos 218 2=Stg 2</td><td>Mos 218 3=Bench</td><td>Mos 218 4=Bench (LS)</td><td>Cam 218 5=Stg 1</td><td>Cam 218 6=Cold Finger</td><td>Cam 218 7=Bench</td><td>Cam 218 8=Bench</td><td>Cam 232 A=Bench</td><td>Cam 232 B=Det</td></tr><tr><td>";
$ootput =~ s/\n/<\/tr><tr><td>/g;
$ootput =~ s/,/<\/td><td>/g;

print "$ootput\n";
#print "Date</td><td>Sec Offst</td><td>CamBencA</td><td>CamDetcB</td><td>MosVac</td><td>CamVac</td><td>MosCold1</td><td>MosBenc2</td><td>MosBenc3</td><td>MosBenc4</td><td>CamCold5</td><td>CamBenc6</td><td>CamBenc7</td><td>CamBenc8</td></tr><tr><td>";
print "Date</td><td>Sec Offst</td><td>MosVac</td><td>CamVac</td><td>Mos 218 1=Stg 1</td><td>Mos 218 2=Stg 2</td><td>Mos 218 3=Bench</td><td>Mos 218 4=Bench (LS)</td><td>Cam 218 5=Stg 1</td><td>Cam 218 6=Cold Finger</td><td>Cam 218 7=Bench</td><td>Cam 218 8=Bench</td><td>Cam 232 A=Bench</td><td>Cam 232 B=Det</td></tr><tr><td>";
print "</td></tr></table>";
print "Logging Began at: $mo-$md-$y $h:$m:$s<br>";
exit;
