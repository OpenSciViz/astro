
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

public class XYFrame extends JFrame {

   public final UFPlotPanel thePlot;
   public JPanel xyPanel;
   public JTextField xField, yField, textField, charField, colorField;
   public JButton addButton, cancelButton;
   public JRadioButton dataButton, deviceButton;

   public XYFrame(final UFPlotPanel thePlot) {
      super("Add Text");
      setSize(200, 180);
      this.thePlot = thePlot;
      xyPanel = new JPanel();
      xyPanel.setPreferredSize(new Dimension(200,180));

      xyPanel.add(new JLabel("X: "));
      xField = new JTextField(5);
      xyPanel.add(xField);
      xyPanel.add(Box.createRigidArea(new Dimension(20, 5)));
      xyPanel.add(new JLabel("Y: "));
      yField = new JTextField(5);
      xyPanel.add(yField);
      xyPanel.add(new JLabel("Text: "));
      textField = new JTextField(12);
      xyPanel.add(textField);
      xyPanel.add(new JLabel("CharSize: "));
      charField = new JTextField(2);
      xyPanel.add(charField);
      xyPanel.add(Box.createRigidArea(new Dimension(80, 5)));
      xyPanel.add(new JLabel("Color: "));
      colorField = new JTextField(10);
      xyPanel.add(colorField);
      xyPanel.add(Box.createRigidArea(new Dimension(10, 5)));
      dataButton = new JRadioButton("Data", true);
      deviceButton = new JRadioButton("Device");
      ButtonGroup coord = new ButtonGroup();
      coord.add(dataButton);
      coord.add(deviceButton);
      xyPanel.add(new JLabel("Coords: "));
      xyPanel.add(dataButton);
      xyPanel.add(deviceButton);
      addButton = new JButton("Add Text");
      xyPanel.add(addButton);
      addButton.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   float xc=0, yc=0;
	   String temp;
           String opts = "";
	   temp = xField.getText();
	   if (!temp.trim().equals("")) xc = Float.parseFloat(temp);
           temp = yField.getText();
           if (!temp.trim().equals("")) yc = Float.parseFloat(temp);
	   temp = charField.getText();
	   if (!temp.trim().equals("")) opts+="*charsize="+temp+", ";
	   temp = colorField.getText();
	   if (!temp.trim().equals("")) {
	      if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ",","); 
	      opts+="*color="+temp+", ";
	   }
	   if (dataButton.isSelected()) opts+="*data, ";
	   else opts +="*device, ";
	   thePlot.xyouts(xc, yc, textField.getText(), opts);
	   thePlot.removexyFrame();
	   XYFrame.this.dispose();
	}
      });
      xyPanel.add(Box.createRigidArea(new Dimension(10, 5)));
      cancelButton = new JButton("Cancel");
      xyPanel.add(cancelButton);
      cancelButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
	   XYFrame.this.dispose();
        }
      });
      getContentPane().add(xyPanel);
      pack();
      setVisible(true);
   }

}
