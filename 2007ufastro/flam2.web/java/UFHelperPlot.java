package javaUFLib;
/**
 * Title:        UFHelperPlot.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Craig Warner
 * Company:      University of Florida
 * Description:  UFPlotPanel for flam2helper
 */

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class UFHelperPlot extends UFPlotPanel implements ActionListener {
   float[] newx, tempy;
   public Vector vx;
   public Vector[] vy;
   GraphThread graphThread;
   String logFileName;
   String[] sensorNames, oPlotOpts;
   int numSensors, startIndex;
   boolean showY[];
   long currTime;
   String plotOpts = "", units= "seconds";
   String title = "";
   int npts;
   JMenuItem resetRangeItem;
   NewFlam2Helper.UFHelperPlotPanel hpp = null;

   public UFHelperPlot(int numberOfSensors, int startIndex, String logFile) {
      this.logFileName = logFile;
      this.numSensors = numberOfSensors;
      this.startIndex = startIndex;
      npts = 0;
      vx = new Vector(100000,10000);
      vy = new Vector[numSensors];
      showY = new boolean[numSensors];
      sensorNames = new String[numSensors];
      oPlotOpts = new String[numSensors];
      for (int j = 0; j < numSensors; j++) {
	vy[j] = new Vector(100000,10000);
	showY[j] = true;
	sensorNames[j] = "Sensor "+j;
	oPlotOpts[j] = "";
      }
      setupPlot(640, 512);
   }

   public UFHelperPlot(String[] namesOfSensors, int startIndex, String logfile) {
      this(namesOfSensors.length, startIndex, logfile);
      for (int j = 0; j < sensorNames.length; j++) {
	sensorNames[j] = namesOfSensors[j];
      }
   }

   public void updateLogFile(String logFile) {
      if (! this.logFileName.equals(logFile)) {
	this.logFileName = logFile;
	npts = 0;
	vx = new Vector(100000,10000);
        for (int j = 0; j < numSensors; j++) {
	   vy[j] = new Vector(100000,10000);
	}
	if (plotOpts.indexOf(",*title") != -1) {
	   plotOpts = plotOpts.substring(0, plotOpts.indexOf(",*title"));
	}
	startThread();
      }
   }

   public void updatePlotOpts(String plotOpts, String[] oPlotOpts) {
      this.plotOpts = plotOpts;
      this.oPlotOpts = oPlotOpts;
   }

   public void setPlotPanel(NewFlam2Helper.UFHelperPlotPanel hpp) {
      this.hpp = hpp;
      if (!resetRangeItem.isVisible()) resetRangeItem.setVisible(true);
   }

   public void setupPlot(int xdim, int ydim) {
      this.xdim = xdim;
      this.ydim = ydim;
      this.xpos2 = xdim - 20;
      this.ypos2 = ydim - 47;
      this.setBackground(Color.black);
      this.setForeground(Color.white);
      this.setPreferredSize(new Dimension(xdim, ydim));

      menu = new JPopupMenu();

      exportItem = new JMenuItem("Export as PNG/JPEG");
      menu.add(exportItem);
      exportItem.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           JFileChooser jfc = new JFileChooser(saveDir);
           int returnVal = jfc.showSaveDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
              String filename = jfc.getSelectedFile().getAbsolutePath();
              saveDir = jfc.getCurrentDirectory();
              File f = new File(filename);
              if (f.exists()) {
                String[] saveOptions = {"Overwrite","Cancel"};
                int n = JOptionPane.showOptionDialog(UFHelperPlot.this, filename+" already exists.", "File exists!", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, saveOptions, saveOptions[1]);
                if (n == 1) {
                   return;
                }
              }
              String format = "png";
              if (filename.toLowerCase().endsWith(".jpg") || filename.toLowerCase().endsWith(".jpeg")) format = "jpeg";
	      UFHelperPlot upp = UFHelperPlot.this;
              try {
                BufferedImage image = new BufferedImage(upp.xdim, upp.ydim, BufferedImage.TYPE_INT_BGR);
                image.createGraphics().drawImage(offscreenImg,0,0,upp.xdim,upp.ydim,upp);
                ImageIO.write(image, format, f);
              } catch(IOException e) {
                System.err.println("UFPlotPanel error > could not create JPEG image!");
              }
           }
        }
      });

      printItem = new JMenuItem("Print or Save");
      menu.add(printItem);
      printItem.addActionListener(this);

      resetRangeItem = new JMenuItem("Reset Range");
      resetRangeItem.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   hpp.xMinField.setText("0");
           hpp.yMinField.setText("");
           hpp.xMaxField.setText("");
           hpp.yMaxField.setText("");
           hpp.plotButton.doClick();
        }
      });
      menu.add(resetRangeItem);
      if (hpp == null) resetRangeItem.setVisible(false);

      addMouseListener(new java.awt.event.MouseListener() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
        }

        public void mousePressed(java.awt.event.MouseEvent ev) {
           if ((ev.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
              if(ev.isPopupTrigger()) {
                menu.show(ev.getComponent(), ev.getX(), ev.getY());
              }
           }

           if ((ev.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
              if (xyFrame != null) {
                xyFrame.xField.setText(""+ev.getX());
                xyFrame.yField.setText(""+ev.getY());
                xyFrame.deviceButton.setSelected(true);
              }
           }
        }

        public void mouseReleased(java.awt.event.MouseEvent evt) {
        }

        public void mouseEntered(java.awt.event.MouseEvent evt) {
        }

        public void mouseExited(java.awt.event.MouseEvent evt) {
        }
      });
   }

   public void initPlot() {
      offscreenImg = createImage(xdim,ydim);
      while( offscreenImg == null ) {
          offscreenImg = createImage(xdim,ydim);
      }
      offscreenG = (Graphics2D)offscreenImg.getGraphics();
      offscreenG.setColor(backColor);
      offscreenG.fillRect(0,0,xdim,ydim);
      offscreenG.setColor(plotColor);
      offscreenG.setFont(mainFont);
   }

   public void parseParams(String s) {
      super.parseParams(s);
      if (xrange[1] - xrange[0] > 20) {
	for (int j = 0; j < xtickname.length; j++) {
	   xtickname[j] = ""+(int)(Float.parseFloat(xtickname[j]));
	}
      }
      if (yrange[1] - yrange[0] > 20) {
	for (int j = 0; j < ytickname.length; j++) {
	   ytickname[j] = ""+(int)(Float.parseFloat(ytickname[j]));
	}
      }
   }

   public void setLinLog(float[] x, float[] y) {
      linX = new float[x.length];
      logX = new float[x.length];
      linY = new float[y.length];
      logY = new float[y.length];
      for (int j = 0; j < x.length; j++) {
        linX[j] = x[j];
      }
      for (int j = 0; j < y.length; j++) {
        linY[j] = y[j];
      }
   }

   public void updateUnits(String units) {
      this.units = units;
   }

   public void updatePlot() {
long t = System.currentTimeMillis();
      long[] x = new long[vx.size()];
      float[][] ys = new float[vy.length][vy[0].size()];
      float[] tempy = new float[ys[0].length];
      //tempy[0] = 1000;
      //tempy[x.length-1] = 0;
      for (int j = 0; j < x.length; j++) {
        x[j] = ((Long)(vx.get(j))).longValue();
        for (int l = 0; l < ys.length; l++) {
           ys[l][j] = ((Double)(vy[l].get(j))).floatValue();
           //if (ys[l][j] < tempy[0]) tempy[0] = ys[l][j];
           //if (ys[l][j] > tempy[x.length-1]) tempy[x.length-1] = ys[l][j];
        }
      }
      tempy[0] = UFArrayOps.minValue(ys);
      tempy[x.length-1] = UFArrayOps.maxValue(ys);
//System.out.println(tempy[0]+" "+tempy[x.length-1]+" "+UFArrayOps.minValue(ys)+" "+UFArrayOps.maxValue(ys));
      long minX = UFArrayOps.minValue(x);
      float[] newx = UFArrayOps.castAsFloats(UFArrayOps.subArrays(x, minX));
      for (int j = 1; j < tempy.length-1; j++) {
	tempy[j] = tempy[0];
      }
      plotOpts+=",*title="+title;
      if (units.equalsIgnoreCase("minutes")) {
	newx = UFArrayOps.divArrays(newx, 60.0f);
	plotOpts+=",*xtitle=Minutes";
      } else if (units.equalsIgnoreCase("hours")) {
	newx = UFArrayOps.divArrays(newx, 3600.0f);
	plotOpts+=",*xtitle=Hours";
      } else if (units.equalsIgnoreCase("days")) {
	newx = UFArrayOps.divArrays(newx, 86400.0f);
	plotOpts+=",*xtitle=Days";
      } else {
	plotOpts+=",*xtitle=Seconds";
      }
//System.out.println("time: "+(System.currentTimeMillis()-t));
      super.plot(newx, tempy, "*nodata"+plotOpts);
      for (int j = 0; j < ys.length; j++) {
	if (showY[j]) super.overplot(newx, ys[j], oPlotOpts[j]);
      }
   }

    public void startThread() {
        if (isThreadAlive())
            stopThread();
        graphThread = new GraphThread();
        graphThread.start();
    }

    public boolean isThreadAlive() {
        if (graphThread == null) return false;
        return graphThread.isAlive();
    }

    public void stopThread() {

        graphThread.keepRunning = false;
        while (graphThread.stillRunning);//wait for graphThread to exit
    }

    //Class GraphThread
    public class GraphThread extends Thread {
        boolean keepRunning = true;
        boolean stillRunning = false;
        //-------------------------------------------------------------------------------
        /**
         *
         */

        public void run() {
	    BufferedReader br = null;
            long beginTimestamp = 0;
            keepRunning = true; stillRunning = true;
	    String s = " ";
	    StringTokenizer st;
	    long secOffset;
	    long _currTime;
	    boolean hasData = true;
            while (keepRunning) {
                try {
                    if (br == null) {
			br = new BufferedReader(new FileReader(logFileName));
			try {
			    st = new StringTokenizer(br.readLine());
                            st.nextToken();st.nextToken();
                            beginTimestamp = Long.parseLong(st.nextToken());
			    title = "Relative to "+st.nextToken()+" "+st.nextToken();
			} catch (NoSuchElementException ne) {
			    title = "Relative to ????";
			    beginTimestamp = 0;
			}
                        long timeToStart = (System.currentTimeMillis()/1000 - beginTimestamp);
                        br.readLine(); // skip over header line
                        secOffset = 0;
			int n = 0;
			while (s != null) {
			   s = br.readLine();
			   if (s == null) break;
                           st = new StringTokenizer(s,",");
			   st.nextToken();
			   secOffset = Integer.parseInt(st.nextToken().trim());
			   //_currTime = beginTimestamp + (secOffset*1000);
                           _currTime = beginTimestamp + secOffset;
			   currTime = _currTime;
			   //skip to the start index specified in the constructor
			   for (int i=0; i<startIndex; i++) st.nextToken();
			   vx.add(new Long(currTime));
			   for (int i=0; i<numSensors; i++) {
			      vy[i].add(new Double(Double.parseDouble(st.nextToken().trim())));
			   }
			}
			//updatePlot();
		    }
                    stillRunning = true;
                    s = br.readLine();
		    if (s == null && hasData) { updatePlot(); hasData = false; }
                    if (s == null || s.trim().equals("")) { Thread.sleep(1000); continue; }
		    hasData = true;
                    st = new StringTokenizer(s,",");
                    st.nextToken();
                    secOffset = Integer.parseInt(st.nextToken().trim());
                    //_currTime = beginTimestamp + (secOffset*1000);
                    _currTime = beginTimestamp + secOffset;
                    currTime = _currTime;
                    //skip to the start index specified in the constructor
                    for (int i=0; i<startIndex; i++) st.nextToken();
		    vx.add(new Long(currTime));
		    for (int i=0; i<numSensors; i++) {
			vy[i].add(new Double(Double.parseDouble(st.nextToken().trim())));
                    }
		    //updatePlot();
                } catch (Exception e) {
                    System.err.println("UFGraphPanel.GraphThread.run> "+e.toString());
                    keepRunning = stillRunning = false;
                }
            }
            stillRunning = false;
        }
    } //end of class GraphThread

    public String [] getSensorNames() { return sensorNames;}
    public Color [] getColors() { return null; }

   public void calcZoom() {
      if (sxinit==0 || syinit==0 || sxfin==0 || syfin==0 ) return;
      float x1 = (Math.min( sxinit, sxfin )-xoff)/xscale;
      float y1 = (Math.min( yoff-syinit, yoff-syfin ))/yscale;
      float x2 = (Math.max( sxinit, sxfin )-xoff)/xscale;
      float y2 = (Math.max( yoff-syinit, yoff-syfin ))/yscale;
      x1 = (float)(Math.floor(x1*1000)*.001);
      x2 = (float)(Math.floor(x2*1000)*.001);
      y1 = (float)(Math.floor(y1*1000)*.001);
      y2 = (float)(Math.floor(y2*1000)*.001);
      if ((""+x1).equals("NaN") || (""+y1).equals("NaN") || (""+x2).equals("NaN") || (""+y2).equals("NaN")) return;
      //String s = "*xrange=["+(int)x1+","+(int)x2+"], *yrange=["+(int)y1+","+(int)y2+"],"+plotOpts;
      if (hpp != null) {
        hpp.xMinField.setText(""+x1);
        hpp.xMaxField.setText(""+x2);
        hpp.yMinField.setText(""+y1);
        hpp.yMaxField.setText(""+y2);
        hpp.plotButton.doClick();
      }
    }

   public static void main(String[] args) {
      JFrame jf = new JFrame();
      UFHelperPlot hp = new UFHelperPlot(2, 5, "testlog");
      jf.getContentPane().add(hp);
      jf.pack();
      jf.setVisible(true);
      hp.startThread();
   }
}
