;Detector Parameter File
;
;dc agent host IP number
128.227.184.153
;dc agent port number
52008
Name(no spaces)  Register  LatchOrder  Bias(volts)  DacScaleFactor(#/volts)  DacOffset(#)

V_CasUC            0         1         -3.50             65.469                255
V_RstUC            1         1         -2.00            113.839                255
V_DetGrdRng        2         1         -5.30             24.06                 255
V_DetGrv           3         1         -6.50             24.06                 255
V_ClCol            4         1         -5.5            41.872                  255
V_ShPchg           5         1         -3.00             76.946                255
V_Sh               6         1         -0.00           2318.182                255
V_Offset           7         1         -2.9              51.000                255
V_ClOut            8         1         -4.8              47.309                255
V_Pw               9         1         -6.00             38.404                255
V_Vss1            10         1         -5.50             41.803                255
V_Vss2            11         1         -5.50             41.872                255
V_VssD            12         2         -6.00             38.231                255
V_Vgg1            13         1         -0.00            153.800                255
V_Vgg2            14         1         -2.15            108.143                255
V_Vgg3            15         1         -1.9             134.211                255
V_Vgg4            16         1         -1.58            153.986                255
V_Imux            17         1         -1.75            142.061                255
V_Cap             18         1         -0.00             51.000                255
V_GateHi          19         1         -4.00             38.346                255
V_GateLo          20         1         -6.00             38.346                255
V_Gate            21         1         -6.00             38.346                255
V_Neg             22         1         -9.60             24.06                 255
V_Sub             23         1         -0.00           2318.182                255

