## Example vxWorks startup file

nfsMount("irflam2a","/share","/share")
cd "/share/home/rojas/FLAM/flam/iocBoot/iocflam"
pwd

## The following is needed if your board support package doesn't at boot time
## automatically cd to the directory containing its startup script
#cd "/home/gemvx/rrojas/FLAM/uf2005/flam/iocBoot/iocflam"

< cdCommands
#< ../nfsCommands

cd topbin
## You may have to change flam to something else
## everywhere it appears in this file

ld < flam.munch

## This drvTS initializer is needed if the IOC has a hardware event system
#TSinit

## Register all support components
cd top
dbLoadDatabase("dbd/flam.dbd",0,0)
flam_registerRecordDeviceDriver(pdbbase)

## Load record instances
dbLoadRecords("db/flamGeminiSim.db","user=flam")
dbLoadRecords("db/flamSadTop.db","user=flam,no=1,scan=1 second")
dbLoadRecords("db/flamTop.db","user=flam,no=2,scan=2 second")

## Set this to see messages from mySub
#mySubDebug = 1

cd startup
iocInit()

## Start any sequence programs
#seq &sncExample,"user=flam"
putenv "UFEPICSDBNAME=flam"
#pvload "./pv/flamInitialize.pv"
ufLogStdOut 1
taskDelay 1800
ufInetStartSim "192.168.111.222"

#seq &timeSeq, "sys=flam:"

dbpf "flam:confwcs.A","/gemini/trecs/wcs/wcs_trecs.cfg"
dbpf "flam:confwcs.DIR","0"
dbpf "flam:confwcs.DIR","2"

taskSpawn ("initWcsCtrl",60,0x8,10000,initWcsCtrl,0,0,0,0,0,0)
taskSpawn ("computeWcsCtrl",60,0x8,10000,computeWcsCtrl,0,0,0,0,0,0)

dbpf "flam:confwcs.DIR","3"


ufDatumCnt("sys")

