#!/bin/bash

# This script sets up EDM for use at Gemini North.

if [ -z "$EPICS" ]; then
  echo "EPICS environment variables have not been defined."
  echo "Execute your local EPICS startup script and try again"
  exit 1
fi

# Set up the environment variables before starting edm

export EDMFILES=`pwd`
export EDMCOLORFILE=colorsMK.list
export EDMFONTFILE=fontsMK.list
export EDMPRINTDEF=$EDMFILES/edmPrintMK.def
export EDMCALC=$EDMFILES/calcMK.list

# These environment variables should default to the location
# of the GEM9.0 installation tree

GEM9=/usr/software/dev/packages/epics/epics3.14.6GEM9.0/extensions

export EDMOBJECTS=$GEM9/src/edm/setup
export EDMPVOBJECTS=$GEM9/src/edm/setup
export EDMHELPFILES=$GEM9/src/edm/helpFiles

$GEM9/bin/${EPICS_HOST_ARCH}/edm $1 &
