#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"
#ifndef INCloadH
#define INCloadH
typedef struct loadRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	epicsInt32		val;	/*Status value*/
	char		msg[40]; /*Message Value*/
	DBLINK		out;	/*Hardware Specificatn*/
	DBLINK		dnl;	/*Download Specificatn*/
	char		dnv[40]; /*Download Value*/
	DBLINK		upl;	/*Upload Specificatn*/
	char		upv[40]; /*Upload Value*/
	epicsInt32		pval;	/*Prev Status Value*/
	char		pmsg[40]; /*Prev Message Value*/
	char		pdnv[40]; /*Prev Download Value*/
	char		pupv[40]; /*Prev Upload Value*/
	DBLINK		sdnl;	/*Sim Downld Specifctn*/
	char		sdnv[40]; /*Sim Downld Value*/
	DBLINK		supl;	/*Sim Upload Specifctn*/
	char		supv[40]; /*Sim Upload Value*/
	DBLINK		siol;	/*Sim Value Location*/
	epicsInt32		sval;	/*Simulation Value*/
	DBLINK		siml;	/*Sim Mode Location*/
	epicsEnum16	simm;	/*Simulation Mode*/
	epicsEnum16	sims;	/*Sim mode Alarm Svrty*/
} loadRecord;
#define loadRecordNAME	0
#define loadRecordDESC	1
#define loadRecordASG	2
#define loadRecordSCAN	3
#define loadRecordPINI	4
#define loadRecordPHAS	5
#define loadRecordEVNT	6
#define loadRecordTSE	7
#define loadRecordTSEL	8
#define loadRecordDTYP	9
#define loadRecordDISV	10
#define loadRecordDISA	11
#define loadRecordSDIS	12
#define loadRecordMLOK	13
#define loadRecordMLIS	14
#define loadRecordDISP	15
#define loadRecordPROC	16
#define loadRecordSTAT	17
#define loadRecordSEVR	18
#define loadRecordNSTA	19
#define loadRecordNSEV	20
#define loadRecordACKS	21
#define loadRecordACKT	22
#define loadRecordDISS	23
#define loadRecordLCNT	24
#define loadRecordPACT	25
#define loadRecordPUTF	26
#define loadRecordRPRO	27
#define loadRecordASP	28
#define loadRecordPPN	29
#define loadRecordPPNR	30
#define loadRecordSPVT	31
#define loadRecordRSET	32
#define loadRecordDSET	33
#define loadRecordDPVT	34
#define loadRecordRDES	35
#define loadRecordLSET	36
#define loadRecordPRIO	37
#define loadRecordTPRO	38
#define loadRecordBKPT	39
#define loadRecordUDF	40
#define loadRecordTIME	41
#define loadRecordFLNK	42
#define loadRecordVAL	43
#define loadRecordMSG	44
#define loadRecordOUT	45
#define loadRecordDNL	46
#define loadRecordDNV	47
#define loadRecordUPL	48
#define loadRecordUPV	49
#define loadRecordPVAL	50
#define loadRecordPMSG	51
#define loadRecordPDNV	52
#define loadRecordPUPV	53
#define loadRecordSDNL	54
#define loadRecordSDNV	55
#define loadRecordSUPL	56
#define loadRecordSUPV	57
#define loadRecordSIOL	58
#define loadRecordSVAL	59
#define loadRecordSIML	60
#define loadRecordSIMM	61
#define loadRecordSIMS	62
#endif /*INCloadH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int loadRecordSizeOffset(dbRecordType *pdbRecordType)
{
    loadRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->msg);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->msg - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->out);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->out - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->dnl);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->dnl - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->dnv);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->dnv - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->upl);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->upl - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->upv);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->upv - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->pval);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->pval - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->pmsg);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->pmsg - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->pdnv);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->pdnv - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->pupv);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->pupv - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->sdnl);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->sdnl - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->sdnv);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->sdnv - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->supl);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->supl - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->supv);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->supv - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->siol);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->siol - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->sval);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->sval - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->siml);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->siml - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->simm);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->simm - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->sims);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->sims - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(loadRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/
