#ifndef INCmenuDirectiveH
#define INCmenuDirectiveH
typedef enum {
	menuDirectiveMARK,
	menuDirectiveCLEAR,
	menuDirectivePRESET,
	menuDirectiveSTART,
	menuDirectiveSTOP
}menuDirective;
#endif /*INCmenuDirectiveH*/
