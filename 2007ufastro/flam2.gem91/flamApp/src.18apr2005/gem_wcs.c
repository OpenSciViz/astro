/* VxWorks include files */
#include "vxWorks.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "dbAccess.h"
#include "cadRecord.h"

/* menus */
#include "carRecord.h"

/* wcs toolkit */
#include "timeLib.h"
#include "slalib.h"
#include "astLib.h"

#define GEM_WCS_INTERNAL
#include "gem_wcs.h"
#undef GEM_WCS_INTERNAL

/* 
 * VX_FP_TASK = ???
 * 
 * taskSpawn ("initWcsCtrl",60,VX_FP_TASK,10000,initWcsCtrl,0,0,0,0,0,0)
 *
 */

static wcsInfoStruct wcsInfo;

/*-----------------------------------------------------------------------------
 * loadWcsFile
 */
int loadWcsFile(char *file) {
   int n,
   count;
   char buffer[81];
   char *p;
   FILE *fp;
   int ibin, jbin, iskip, jskip, i;

   wcsInfo.numPoints = 0;
   if ((strncmp(file, "NONE", 4) == 0) || (*file == '\0')) {
      printf("NONE/empty wcs data\n");
      return OK;
      }
   fp = fopen(file, "r");
   if (fp == NULL) {
      printf("Cannot open wcs file '%s'\n", file);
      return ERROR;
      }
   for (n = 0; n < NPOINTS; n++) {
      p = fgets(buffer, 80, fp);
      if (p == NULL) {
         if (feof(fp))
            break;
         else {
            printf("Error reading wcs file '%s'\n", file);
            fclose(fp);
            return ERROR;
            }
         }
      count = sscanf(buffer, "%lf %lf %lf %lf", &(wcsInfo.pixij[n][0]), &(wcsInfo.pixij[n][1]), &(wcsInfo.fpxy[n][0]), &(wcsInfo.fpxy[n][1]));
      if (count != 4) {
         n--;
         continue;
         }
      }
   fclose(fp);
   wcsInfo.numPoints = n;

    /* calibrate - michelle has no ROI or binning, hence we can do it here */
    ibin=1;
    jbin=1;
    iskip=0;
    jskip=0;

    if (wcsInfo.numPoints>0) {
       for (i = 0; i < wcsInfo.numPoints; i++) {
           wcsInfo.detij[i][0] = ((wcsInfo.pixij[i][0] - 0.5 - (double) iskip) / (double) ibin) + 0.5;
           wcsInfo.detij[i][1] = ((wcsInfo.pixij[i][1] - 0.5 - (double) jskip) / (double) jbin) + 0.5;
       }
    }
   printf ("Read %d entries from wcs config file %s\n",wcsInfo.numPoints,file);
   return OK;
   }

/*-----------------------------------------------------------------------------
 * initWcsCtrl
 */
int initWcsCtrl(void) {
    char *dbRec="flam:confwcs.OUTA";
    struct dbAddr addr;
    struct cadRecord* pCad;
    
    /* Create a semaphore for the task */
    semInitWcs = semBCreate(SEM_Q_FIFO,SEM_EMPTY);
      
    /* Get the address of the data structure containing command arguments */
    if (dbNameToAddr (dbRec,&addr)) {
      printf ("Init wcs failed!!\n");
      return -1;
      }
    
    pCad = (struct cadRecord *) addr.precord;

    /* inifinite loop waiting for the wcsInit directive */
    while(1) {
      /* get the semaphore   */
	   semTake(semInitWcs, WAIT_FOREVER);
      /* send file names to controller */
      if ( loadWcsFile((char*)pCad->vala) == OK) {
         /* set car back to idle*/
/*
 *          cadstatus=menuCarstatesIDLE;
 *          if( dbPut(&addr,DBR_LONG,&cadstatus, 1) ) {
 *             printf ("WCS - Failed to set car IDLE!!\n");
 *             }
 */
         printf ("WCS loaded\n");
         }
      else {
	      /* set car 	to error if InitWcs returned an error*/
/*
 * 	      cadstatus=menuCarstatesERROR;
 *          if( dbPut(&addr,DBR_LONG,&cadstatus, 1) ) {
 *             printf ("WCS - Failed to set car ERROR!!\n");
 *             }
 */
         printf ("WCS failed to load\n");
         }
      }
   return 0;
   }


/*-----------------------------------------------------------------------------
 * my_dbPutString
 */
static int my_dbPutString(char *pvname, char *val) {

    struct dbAddr addr;

    /* Get the address of the data structure containing command arguments */
    if (dbNameToAddr (pvname,&addr)) {
      return -1;
      }
    if( dbPut(&addr,DBR_STRING,val, 1) ) {
      return -1;
      }
		
return 0;
}
/*-----------------------------------------------------------------------------
 * my_dbPutDouble
 */
static int my_dbPutDouble(char *pvname, double val) {

    struct dbAddr addr;

    /* Get the address of the data structure containing command arguments */
    if (dbNameToAddr (pvname,&addr)) {
      return -1;
      }
    if( dbPut(&addr,DBR_DOUBLE,&val, 1) ) {
      return -1;
      }

return 0;
}



/*-----------------------------------------------------------------------------
 * computeWcsCtrl
 */
int computeWcsCtrl(void) {
    wcsHeader mywcshdr;

    /* Create a semaphore for the task */
    semComputeWcs = semBCreate(SEM_Q_FIFO,SEM_EMPTY);
      
    /* inifinite loop waiting for the wcsInit directive */
    while(1) {
      /* get the semaphore   */
      semTake(semComputeWcs, WAIT_FOREVER);
      /* do the compute */
      if ( getWcsCurrent(&mywcshdr) == OK) {
      	/* write the values */
      	my_dbPutDouble("flam:wcs:crpix1",mywcshdr.crpix1);
      	my_dbPutDouble("flam:wcs:crval1",mywcshdr.crval1);
      	my_dbPutDouble("flam:wcs:crval2",mywcshdr.crval2);
      	my_dbPutDouble("flam:wcs:crpix2",mywcshdr.crpix2);
      	my_dbPutDouble("flam:wcs:mjdobs",mywcshdr.mjdobs);
      	my_dbPutDouble("flam:wcs:cd21",mywcshdr.cd2_1);
      	my_dbPutDouble("flam:wcs:cd11",mywcshdr.cd1_1);
      	my_dbPutDouble("flam:wcs:cd12",mywcshdr.cd1_2);
      	my_dbPutDouble("flam:wcs:cd22",mywcshdr.cd2_2);
      	my_dbPutDouble("flam:wcs:equinox",mywcshdr.equinox);
      	my_dbPutString("flam:wcs:ctype1",mywcshdr.ctype1);
      	my_dbPutString("flam:wcs:ctype2",mywcshdr.ctype2);
      	my_dbPutString("flam:wcs:radecsys",mywcshdr.radecsys);
      	}
      else {
        printf ("failed to compute wcs\n");
      	/* write the values */
      	my_dbPutDouble("flam:wcs:crpix1",-0.0);
      	my_dbPutDouble("flam:wcs:crval1",-0.0);
      	my_dbPutDouble("flam:wcs:crval2",-0.0);
      	my_dbPutDouble("flam:wcs:crpix2",-0.0);
      	my_dbPutDouble("flam:wcs:mjdobs",-0.0);
      	my_dbPutDouble("flam:wcs:cd21",-0.0);
      	my_dbPutDouble("flam:wcs:cd11",-0.0);
      	my_dbPutDouble("flam:wcs:cd12",-0.0);
      	my_dbPutDouble("flam:wcs:cd22",-0.0);
      	my_dbPutDouble("flam:wcs:equinox",-0.0);
      	my_dbPutString("flam:wcs:ctype1","INVALID");
      	my_dbPutString("flam:wcs:ctype2","INVALID");
      	my_dbPutString("flam:wcs:radecsys","INVALID");
      	}
      }
   return OK;
   }
/*-----------------------------------------------------------------------------
 * computeWcs
 *
 * from "World Coordinates, Part1: Astrometry" P.T. Wallace
 * and ICDX.X from Steven Beard for a predigested version
 *
 */
 
/*
 *  taskSpawn("computeWcs", 200, 0x8 , 60000,computeWcs,"FK5",5000,"J2000")
 */

int computeWcs(char *trackFrameString,double trackWavelength, char *trackEquinoxString,wcsHeader *wcshdr) {
    double timeTAI;
    struct WCS wcs;
    struct WCS wcsij;
    struct WCS_CTX ctx;
    struct EPOCH trackEquinox;
    FRAMETYPE trackFrame;
    double rawTime, timeStamp;
    double pixis;               /* x to i scale factor.     */
    double pixjs;               /* y to j scale factor.     */
    double perp;                /* Non-perpendicularity of i and j axes in
                                 * radians.
                                 */
    double orient;              /* Orientation of (i,j) axes with respect to
                                 *  (x,y) in radians.
                                 */

/* the output of the wcs calculations consists of the following 13 items */
    char ctype1[81];              /* World Coordinate System projection  */
                                  /* type for axis 1.                    */
    double crpix1;                /* Pixel coordinate reference for      */
                                  /* axis 1.                             */
    double crval1;                /* World coordinate reference for      */
                                  /* axis 1.                             */
    char ctype2[81];              /* World Coordinate System projection  */
                                  /* type for axis 2.                    */
    double crpix2;                /* Pixel coordinate reference for      */
                                  /* axis 2.                             */
    double crval2;                /* World coordinate reference for      */
                                  /* axis 2.                             */
    double cd1_1;                 /* xi rotation/skew matrix element.    */
    double cd1_2;                 /* xj rotation/skew matrix element.    */
    double cd2_1;                 /* yi rotation/skew matrix element.    */
    double cd2_2;                 /* yj rotation/skew matrix element.    */
    char radecsys[81];            /* Type of RA/Dec (for celestial       */
                                  /* coordinates).                       */
    double equinox;               /* Epoch of mean equator & equinox     */
                                  /* (celestial  coords).                */
    double mjdobs;                /* Modified Julian Date.               */



   
   if(!gem_TCSconnection) {
      printf("ERROR computeWcs -> TCS disconnected");
      goto WCSERROR;
      }
      
   /* get time from tcs and convert it to TAI */
   if(timeNow(&timeStamp) != OK) {
      printf("ERROR computeWcs -> Failed to get time stamp");
      goto WCSERROR;
      }
    
   if(timeThenD(timeStamp,TT,&timeTAI) != OK) {
      printf("ERROR computeWcs -> Failed to convert time stamp to TAI\n");
      goto WCSERROR;
      } 

   if(strcmp(trackFrameString,"FK4") == 0)
      trackFrame = FK4;
   else if(strcmp(trackFrameString,"FK5") == 0)
      trackFrame = FK5;
   else if(strcmp(trackFrameString,"Apparent") == 0)
      trackFrame = APPT;
   else if(strcmp(trackFrameString,"Observer Altaz") == 0)
      trackFrame = AZEL_TOPO;
   else if(strcmp(trackFrameString,"Mount Altaz") == 0)
      trackFrame = AZEL_MNT;
   else {
      printf("ERROR computeWcs -> invalid trackFrame %s",trackFrameString);
      goto WCSERROR;
      }
      
   /* convert wavelength from angstroms to microns*/
   trackWavelength = trackWavelength * 10000;

   trackEquinox.year = atof(&(trackEquinoxString[1]));
   trackEquinox.type = trackEquinoxString[0];


   /* get ctx context from tcs */
   if(astGetctx(&ctx) != OK) {
      printf("ERROR computeWcs -> Failed to get wcs context\n");
      goto WCSERROR;
      }
      
   /* extract the current focal plane to sky WCS transformation from the tcs context */
   /* here, the zero, means chopA */
   if ( astCtx2tr(ctx,trackFrame, trackEquinox, trackWavelength, 0, &wcs, &rawTime) != OK ) {
      printf("ERROR computeWcs -> bad status for astCtx2tr\n");
      goto WCSERROR;
      }

   /* if no wcs data, return */
   if (wcsInfo.numPoints == 0) {
      printf("ERROR computeWcs -> no entries\n");
      goto WCSERROR;
   }
    
   /* calibrate - michelle has no ROI or binning, hence this is done when we load the wcs file */

   if (astFitij(wcsInfo.numPoints, wcsInfo.fpxy, wcsInfo.detij, wcsInfo.cij, &pixis, &pixjs, &perp, &orient) != OK) {
      printf("ERROR computeWcs -> bad status for astFitij\n");
      goto WCSERROR;
   }

   
   if ( astXtndtr(wcsInfo.cij, wcs, &wcsij) != OK) { 
      printf("ERROR computeWcs -> bad status for astXtndtr\n");
      goto WCSERROR;
   }
    
    /* Calculate the FITS header values */
   if ( astFITSv(wcsij, trackFrame, trackEquinox, timeTAI, ctype1, &crpix1, &crval1, ctype2, &crpix2, &crval2, &cd1_1, &cd1_2, &cd2_1, &cd2_2, radecsys, &equinox, &mjdobs)  != OK) {
      printf("ERROR computeWcs -> bad status for astFITSv\n");
      goto WCSERROR;
      }
    
    /* success */              
    printf("World Coordinate\n");
    printf("----------------\n");
    printf("ctype1   = %s\n", ctype1);
    printf("crpix1   = %f pixels\n", crpix1);
    printf("crval1   = %f degrees = %f hours\n", crval1, (crval1 / (double) 15.0));
    printf("ctype2   = %s\n", ctype2);
    printf("crpix2   = %f pixels\n", crpix2);
    printf("crval2   = %f degrees\n", crval2);
    printf("cd1_1    = %f\n", cd1_1);
    printf("cd1_2    = %f\n", cd1_2);
    printf("cd2_1    = %f\n", cd2_1);
    printf("cd2_2    = %f\n", cd2_2);
    printf("radecsys = %s\n", radecsys);
    printf("equinox  = %f\n", equinox);
    printf("mjd-obs  = %f\n", mjdobs);

   if (wcshdr !=NULL) {
      strcpy(wcshdr->ctype1,ctype1);
      wcshdr->crpix1=crpix1;
      wcshdr->crval1=crval1;
      strcpy(wcshdr->ctype2,ctype2);
      wcshdr->crpix2=crpix2;
      wcshdr->crval2=crval2;
      strcpy(wcshdr->radecsys,radecsys);
      wcshdr->cd1_1=cd1_1;
      wcshdr->cd1_2=cd1_2;
      wcshdr->cd2_1=cd2_1;
      wcshdr->cd2_2=cd2_2;
      wcshdr->mjdobs=mjdobs;
      wcshdr->equinox=equinox;
      }
   return OK;

WCSERROR:
   if (wcshdr !=NULL) {
      strcpy(wcshdr->ctype1,"INVALID");
      wcshdr->crpix1=-0.0;
      wcshdr->crval1=-0.0;
      strcpy(wcshdr->ctype2,"INVALID");
      wcshdr->crpix2=-0.0;
      wcshdr->crval2=-0.0;
      strcpy(wcshdr->radecsys,"INVALID");
      wcshdr->cd1_1=-0.0;
      wcshdr->cd1_2=-0.0;
      wcshdr->cd2_1=-0.0;
      wcshdr->cd2_2=-0.0;
      wcshdr->equinox=-0.0;
      wcshdr->mjdobs=-0.0;
      }

   return ERROR;
   }



/*-----------------------------------------------------------------------------
 * getWcs
 */
int getWcs(char *trackFrameString,double *trackWavelength, char *trackEquinoxString,wcsHeader *wcshdr) {
   return computeWcs(trackFrameString,*trackWavelength,trackEquinoxString,wcshdr);
   }

/*-----------------------------------------------------------------------------
 * getWcsCurrent
 */
int getWcsCurrent(wcsHeader *wcshdr) {
   return computeWcs(trackFrameCurrent,trackWavelengthCurrent,trackEquinoxCurrent,wcshdr);
   }
