#if !defined(__UFGEMGENSUBCOMMBT_C__)
#define __UFGEMGENSUBCOMMBT_C__ "RCS: $Name:  $Id: "
static const char rcsIdufGEMGENSUBCOMMCBT[] = __UFGEMGENSUBCOMMBT_C__;

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>

/* use  select on socfd -- hon */
#include <sockLib.h>

#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
/* #include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <dbStaticLib.h>
#include <dbBase.h>

#include "flam.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"
#include "ufdbl.h"

static char bt_DEBUG_MODE[6];

static char bt_host_ip[20];
static long bt_port_no;

/********************** BT CODING ************************/

/******************** SNAM for adjHrdwrG ******************/
long
ufadjHrdwrGprocxxx (genSubRecord * pgs)
{
  char yy[40];
  char *original;
  char *zz;

  if (strcmp (pgs->a, "") != 0)
    {
      original = pgs->a;
      zz = pgs->a;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vala, yy);	/* 1 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valb, yy);	/* 2 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valc, yy);	/* 3 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vald, yy);	/* 4 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vale, yy);	/* 5 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valf, yy);	/* 6 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valg, yy);	/* 7 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valh, yy);	/* 8 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vali, yy);	/* 9 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valj, yy);	/* 10 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valk, yy);	/* 11 */
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vall, yy);	/* 12 */

      strcpy (pgs->valm, "1");
      strcpy (pgs->valn, "START");
      strcpy (pgs->a, "");
      pgs->noa = 12;
    }
  else
    {
      strcpy (pgs->valn, "CLEAR");
    }
  return OK;
}

long
read_bt_file (char *rec_name, genSubCommPrivate * pPriv)
{
  FILE *btFile = 0;
  char in_str[80];
  double numnum;
  int i;

  sprintf( _UFerrmsg, __HERE__ "> Attempting to read file: %s",
	   bt_param_filename ) ;
  ufLog( _UFerrmsg ) ;

  btFile = fopen( bt_param_filename, "r" ) ;
  if( 0 == btFile )
    {
      sprintf( _UFerrmsg, __HERE__ "> Unable to open file: %s",
	       bt_param_filename ) ;
      ufLog( _UFerrmsg ) ;
      return -1 ;
    }

  /* Read the host IP number */
  NumFiles++;

  fgets (in_str, 40, btFile);	/* comment line */
  fgets (in_str, 40, btFile);	/* comment line */
  fgets (in_str, 40, btFile);	/* comment line */
  fgets (in_str, 40, btFile);	/* IP number */

  strcpy (bt_host_ip, in_str);
  i = 0;
  while (((isdigit (bt_host_ip[i])) || (bt_host_ip[i] == '.'))
	 && (i < 15))
    i++;
  bt_host_ip[i] = '\0';

  /* Read the port number */
  fgets (in_str, 40, btFile);	/* comment line */

  fscanf (btFile, "%lf", &numnum);

  bt_port_no = (long) numnum;

  fclose (btFile);
  NumFiles--;
  
  return OK;
}

/******************** INAM for ObsControlG *******************/
long
ufrebootGinit (genSubRecord * pgs)
{
  genSubBTPrivate *pPriv;
  char buffer[MAX_STRING_SIZE];
  long status = OK;

  /* 
   *  Create a private control structure for this gensub record
   */
  pPriv = (genSubBTPrivate *) malloc (sizeof (genSubBTPrivate));
  if (pPriv == NULL)
    {
      sprintf( _UFerrmsg, __HERE__ "> Memory allocation failure" ) ;
      ufLog( _UFerrmsg ) ;

      return -1;
    }

  /* 
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
   */
  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status)
    {
      sprintf( _UFerrmsg, __HERE__ "> dbNameToAddr() failed for: %s",
	       buffer ) ;
      ufLog( _UFerrmsg ) ;

      return -1;
    }

  strcpy (buffer, pgs->name);
  buffer[strlen (buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status)
    {
      sprintf( _UFerrmsg, __HERE__ "> dbNameToAddr() failed for: %s",
	       buffer ) ;
      ufLog( _UFerrmsg ) ;

      return -1;
    }

  /* Assign private information needed */
  pPriv->port_no = bt_port_no;
  pPriv->agent = 5;		/* BT agent */

  pPriv->CurrParam.command_mode = SIMM_NONE;	/* Simulation 
						   Mode
						   mode
						   (start
						   in real
						   mode) */
  strcpy (pPriv->CurrParam.all_devices, "");
  strcpy (pPriv->CurrParam.annex, "");
  strcpy (pPriv->CurrParam.ls_218, "");
  strcpy (pPriv->CurrParam.ls_340, "");
  strcpy (pPriv->CurrParam.vacuum, "");
  strcpy (pPriv->CurrParam.indexors, "");
  strcpy (pPriv->CurrParam.cryocooler, "");
  strcpy (pPriv->CurrParam.ppcVME, "");
  strcpy (pPriv->CurrParam.mce4, "");
  strcpy (pPriv->CurrParam.all_agents, "");
  strcpy (pPriv->CurrParam.executive, "");
  strcpy (pPriv->CurrParam.ufacqframed, "");
  strcpy (pPriv->CurrParam.ufgdhsd, "");
  strcpy (pPriv->CurrParam.ufgls218, "");
  strcpy (pPriv->CurrParam.ufgls340, "");
  strcpy (pPriv->CurrParam.ufg354vacd, "");
  strcpy (pPriv->CurrParam.ufgmotord, "");
  strcpy (pPriv->CurrParam.ufgmce4d, "");

  /* Clear the Gensub inputs */
  *(long *) pgs->a = -1;	/* */
  strcpy ((char *) pgs->b, "");	/* */
  strcpy ((char *) pgs->c, "");	/* */
  strcpy ((char *) pgs->d, "");	/* */
  strcpy ((char *) pgs->e, "");	/* */
  strcpy ((char *) pgs->f, "");	/* */
  strcpy ((char *) pgs->g, "");	/* */
  strcpy ((char *) pgs->h, "");	/* */
  strcpy ((char *) pgs->i, "");	/* */
  strcpy ((char *) pgs->j, "");	/* */
  strcpy ((char *) pgs->k, "");	/* */
  strcpy ((char *) pgs->l, "");	/* */
  strcpy ((char *) pgs->m, "");	/* */
  strcpy ((char *) pgs->n, "");	/* */
  strcpy ((char *) pgs->o, "");	/* */
  strcpy ((char *) pgs->p, "");	/* */
  strcpy ((char *) pgs->q, "");	/* */
  strcpy ((char *) pgs->r, "");	/* */
  strcpy ((char *) pgs->s, "");	/* */
  strcpy ((char *) pgs->t, "");	/* */

  /* Create a callback for this gensub record */

  pPriv->pCallback = initCallback (flamGensubCallback);

  if (pPriv->pCallback == NULL)
    {
      sprintf( _UFerrmsg, __HERE__ "> initCallBack() failed" ) ;
      ufLog( _UFerrmsg ) ;

      return -1;
    }

  pPriv->pCallback->pRecord = (struct dbCommon *) pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback (pPriv->pCallback, TRX_INIT_DC_AGENT_WAIT);

  pPriv->boot_inp.command_mode = -1;

  strcpy (pPriv->boot_inp.all_devices, "");
  strcpy (pPriv->boot_inp.annex, "");
  strcpy (pPriv->boot_inp.ls_218, "");
  strcpy (pPriv->boot_inp.ls_340, "");
  strcpy (pPriv->boot_inp.vacuum, "");
  strcpy (pPriv->boot_inp.indexors, "");
  strcpy (pPriv->boot_inp.cryocooler, "");
  strcpy (pPriv->boot_inp.ppcVME, "");
  strcpy (pPriv->boot_inp.mce4, "");
  strcpy (pPriv->boot_inp.all_agents, "");
  strcpy (pPriv->boot_inp.executive, "");
  strcpy (pPriv->boot_inp.ufacqframed, "");
  strcpy (pPriv->boot_inp.ufgdhsd, "");
  strcpy (pPriv->boot_inp.ufgls218, "");
  strcpy (pPriv->boot_inp.ufgls340, "");
  strcpy (pPriv->boot_inp.ufg354vacd, "");
  strcpy (pPriv->boot_inp.ufgmotord, "");
  strcpy (pPriv->boot_inp.ufgmce4d, "");

  pgs->dpvt = (void *) pPriv;

  return OK;
}

/******************** SNAM for ObsControlG *******************/
long
ufrebootGproc (genSubRecord * pgs)
{
  genSubBTPrivate *pPriv;
  long status = OK;
  char response[40];
  long some_num;
  long ticksNow;

  char *endptr;
  int num_str = 0;
  static char **com;
  int i;
  char rec_name[31];

  double numnum;

  if (!debugPGS (__HERE__, pgs))
    {
      return -1;
    }

  /* strcpy(bt_DEBUG_MODE,pgs->m) ; */

  pPriv = (genSubBTPrivate *) pgs->dpvt;
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState)
    {
    case TRX_GS_INIT:
      if (flam_initialized != 1)
	init_flam_config ();

      /* if (!bt_initialized) init_bt_config() ; */
      read_bt_file (pgs->name, (genSubCommPrivate *) pPriv);

      /* pPriv->port_no = bt_port_no ; *//* ZZZ */

      /* Clear outpus A-J for temp Gensub */

      *(long *) pgs->vala = SIMM_NONE;	/* command mode
					   (start in real
					   mode) */
      *(long *) pgs->valb = -1;	/* socket Number */
      strcpy ((char *) pgs->valc, "");
      strcpy ((char *) pgs->vald, "");
      strcpy ((char *) pgs->vale, "");
      strcpy ((char *) pgs->valf, "");
      strcpy ((char *) pgs->valg, "");
      strcpy ((char *) pgs->valh, "");
      strcpy ((char *) pgs->vali, "");
      strcpy ((char *) pgs->valj, "");
      strcpy ((char *) pgs->valk, "");
      strcpy ((char *) pgs->vall, "");
      strcpy ((char *) pgs->valm, "");
      strcpy ((char *) pgs->valn, "");
      strcpy ((char *) pgs->valo, "");
      strcpy ((char *) pgs->valp, "");
      strcpy ((char *) pgs->valq, "");
      strcpy ((char *) pgs->valr, "");
      strcpy ((char *) pgs->vals, "");
      strcpy ((char *) pgs->valt, "");
      strcpy ((char *) pgs->valu, "");

      pPriv->socfd = -1;	/* ZZZ */
      *(long *) pgs->valb = (long) pPriv->socfd;	/* current 
							   socket 
							   number 
							 */

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

      /* 
       *  In idle state we wait for one of the inputs to 
       *  change indicating that a new command has arrived. Status
       *  and updating from the Agent comes during BUSY state.
       */

    case TRX_GS_DONE:
      /* Check to see if the input has changed. */
      trx_debug ("Processing from DONE state", pgs->name, "FULL",
		 bt_DEBUG_MODE);
      trx_debug ("Getting inputs A-F", pgs->name, "FULL", bt_DEBUG_MODE);

      pPriv->boot_inp.command_mode = *(long *) pgs->a;
      strcpy (pPriv->boot_inp.all_devices, pgs->b);
      strcpy (pPriv->boot_inp.annex, pgs->c);
      strcpy (pPriv->boot_inp.ls_218, pgs->d);
      strcpy (pPriv->boot_inp.ls_340, pgs->e);
      strcpy (pPriv->boot_inp.vacuum, pgs->f);
      strcpy (pPriv->boot_inp.indexors, pgs->g);
      strcpy (pPriv->boot_inp.cryocooler, pgs->h);
      strcpy (pPriv->boot_inp.ppcVME, pgs->i);
      strcpy (pPriv->boot_inp.mce4, pgs->k);
      strcpy (pPriv->boot_inp.all_agents, pgs->l);
      strcpy (pPriv->boot_inp.executive, pgs->m);
      strcpy (pPriv->boot_inp.ufacqframed, pgs->n);
      strcpy (pPriv->boot_inp.ufgdhsd, pgs->o);
      strcpy (pPriv->boot_inp.ufgls218, pgs->p);
      strcpy (pPriv->boot_inp.ufgls340, pgs->q);
      strcpy (pPriv->boot_inp.ufg354vacd, pgs->r);
      strcpy (pPriv->boot_inp.ufgmotord, pgs->s);
      strcpy (pPriv->boot_inp.ufgmce4d, pgs->t);

      /* 
         printf("My inputs are ......\n") ; printf("Link A 
         %ld\n",*(long *)pgs->a ) ; printf("Link B
         %s\n",(char *)pgs->b ) ; printf("Link C
         %s\n",(char *)pgs->c ) ; printf("Link D
         %s\n",(char *)pgs->d ) ; printf("Link E
         %s\n",(char *)pgs->e ) ; printf("Link F
         %s\n",(char *)pgs->f ) ; printf("Link G
         %s\n",(char *)pgs->g ) ; printf("Link H
         %s\n",(char *)pgs->h ) ; printf("Link I
         %s\n",(char *)pgs->i ) ; printf("Link K
         %s\n",(char *)pgs->k ) ; printf("Link L
         %s\n",(char *)pgs->l ) ; printf("Link M
         %s\n",(char *)pgs->m ) ; printf("Link N
         %s\n",(char *)pgs->n ) ; printf("Link O
         %s\n",(char *)pgs->o ) ; printf("Link P
         %s\n",(char *)pgs->p ) ; printf("Link Q
         %s\n",(char *)pgs->q ) ; printf("Link R
         %s\n",(char *)pgs->r ) ; printf("Link S
         %s\n",(char *)pgs->s ) ; printf("Link T
         %s\n",(char *)pgs->t ) ; printf("Link U
         %s\n",(char *)pgs->u ) ; */


      if ((pPriv->boot_inp.command_mode != -1) ||
	  (strcmp (pPriv->boot_inp.all_devices, "") != 0) ||
	  (strcmp (pPriv->boot_inp.annex, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ls_218, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ls_340, "") != 0) ||
	  (strcmp (pPriv->boot_inp.vacuum, "") != 0) ||
	  (strcmp (pPriv->boot_inp.indexors, "") != 0) ||
	  (strcmp (pPriv->boot_inp.cryocooler, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ppcVME, "") != 0) ||
	  (strcmp (pPriv->boot_inp.mce4, "") != 0) ||
	  (strcmp (pPriv->boot_inp.all_agents, "") != 0) ||
	  (strcmp (pPriv->boot_inp.executive, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufacqframed, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufgdhsd, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufgls218, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufgls340, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufg354vacd, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufgmotord, "") != 0) ||
	  (strcmp (pPriv->boot_inp.ufgmce4d, "") != 0))
	{

	  /* A new command has arrived so set the CAR to
	     busy */
	  strcpy (pPriv->errorMessage, "");
	  status = dbPutField (&pPriv->carinfo.carMessage,
			       DBR_STRING, pPriv->errorMessage, 1);
	  if (status)
	    {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to BUSY",
		       pgs->name ) ;
	      ufLog( _UFerrmsg ) ;

	      return OK;
	    }

	  some_num = CAR_BUSY;
	  status = dbPutField (&pPriv->carinfo.carState,
			       DBR_LONG, &some_num, 1);
	  if (status)
	    {
	      sprintf( _UFerrmsg, __HERE__ "> (%s) Failed to set CAR to BUSY",
		       pgs->name ) ;
	      ufLog( _UFerrmsg ) ;

	      return OK;
	    }

	  /* set up a CALLBACK after 0.5 seconds in order
	     to send it.  set the Command state to SENDING */
	  pPriv->commandState = TRX_GS_SENDING;
	  requestCallback (pPriv->pCallback, TRX_ERROR_DELAY);

	  /* Clear the input links */
	  trx_debug ("Clearing inputs A-T", pgs->name, "FULL", bt_DEBUG_MODE);

	  *(long *) pgs->a = -1;	/* */
	  strcpy ((char *) pgs->b, "");	/* */
	  strcpy ((char *) pgs->c, "");	/* */
	  strcpy ((char *) pgs->d, "");	/* */
	  strcpy ((char *) pgs->e, "");	/* */
	  strcpy ((char *) pgs->f, "");	/* */
	  strcpy ((char *) pgs->g, "");	/* */
	  strcpy ((char *) pgs->h, "");	/* */
	  strcpy ((char *) pgs->i, "");	/* */
	  strcpy ((char *) pgs->k, "");	/* */
	  strcpy ((char *) pgs->l, "");	/* */
	  strcpy ((char *) pgs->m, "");	/* */
	  strcpy ((char *) pgs->n, "");	/* */
	  strcpy ((char *) pgs->o, "");	/* */
	  strcpy ((char *) pgs->p, "");	/* */
	  strcpy ((char *) pgs->q, "");	/* */
	  strcpy ((char *) pgs->r, "");	/* */
	  strcpy ((char *) pgs->s, "");	/* */
	  strcpy ((char *) pgs->t, "");	/* */

	}
      else
	{
	  /* 
	   *  Check to see if the agent response input has changed.
	   */
	  strcpy (response, pgs->j);

	  if (strcmp (response, "") != 0)
	    {
	      /* in this state we should not expect
	         anything in the J field from the Agent
	         unless the agent is late with something */
	      /* Log a message that the agent responded
	         unexpectedly */
	      sprintf( _UFerrmsg, __HERE__ "> (%s): Unexpected response in "
		       "DONE state", pgs->name ) ;
	      ufLog( _UFerrmsg ) ;

	    }
	  else
	    {
	      /* OK. So somone processed the GENSUB without 
	         inputs or the inputs are the same as the
	         clear directives Why would anyone do that? 
	         I know why but I won't tel. */
	      trx_debug ("Unexpected processing:DONE", pgs->name, "NONE",
			 bt_DEBUG_MODE);
	      /* This happens because of the CLEAR
	         directive for the CAD's . it put zero or
	         empty strings on its out[put link but will 
	         not push them out.  Because we have
	         records that PULL those values when the
	         CAD receives a START directive, those
	         valuses are PUSHED to the GENSUB. Freaky
	         stuff. So to fix that, the CAD's are
	         programmed to write clear values (-1, 0 or 
	         "") on their output links when they
	         receive an empty string as input and the
	         START directive is executed. It's not my
	         fault this happens. is it?!! */
	    }
	  /* Clear the J Field */
	  strcpy (pgs->j, "");
	  pgs->noj = 22;
	  /* Is it possible to get a CALLBACK processing
	     here? */
	}
      break;

    case TRX_GS_SENDING:
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an
         INIT? */
      if (pPriv->boot_inp.command_mode != -1)
	{
	  /* We have an init command */
	  /* Check first to see if we have a valid INIT
	     directive */
	  if ((pPriv->boot_inp.command_mode == SIMM_NONE) ||
	      (pPriv->boot_inp.command_mode == SIMM_FAST) ||
	      (pPriv->boot_inp.command_mode == SIMM_FULL))
	    {
	      /* need to reconnect to the agent ? -- hon */
	      if (pPriv->socfd >= 0)
		{
		  if (checkSoc (pPriv->socfd, 0.0) > 0)
		    {
		      ufLog( __HERE__ "> socket is writable, no need to reconnect");
		    }
		  else
		    {
		      trx_debug ("Closing curr connection", pgs->name, "FULL",
				 bt_DEBUG_MODE);
		      ufClose (pPriv->socfd);
		      pPriv->socfd = -1;
		      *(long *) pgs->valb = (long) pPriv->socfd;	/* current 
									   socket 
									   number 
									 */
		    }
		}
	      pPriv->CurrParam.command_mode = pPriv->boot_inp.command_mode;	/* simulation 
										   level 
										 */
	      *(long *) pgs->vala = (long) pPriv->CurrParam.command_mode;
	      /* Re-Connect if needed */
	      /* Read some initial parameters from a file */
	      /* 
	         pPriv->CurrParam.init_velocity = -1.0 ;
	         pPriv->CurrParam.slew_velocity = -1.0 ;
	         pPriv->CurrParam.acceleration = -1.0 ;
	         pPriv->CurrParam.deceleration = -1.0 ;
	         pPriv->CurrParam.drive_current= -1.0 ;
	         pPriv->CurrParam.datum_speed = -1 ;
	         pPriv->CurrParam.datum_direction = -1 ; */
	      /* if (read_bt_file(pgs->name, pPriv) == OK)
	         pPriv->send_init_param = 1 ; */
	      /* read_bt_file(pgs->name, (genSubCommPrivate 
	       *)pPriv) ; */
	      pPriv->port_no = bt_port_no;

	      if (pPriv->boot_inp.command_mode != SIMM_FAST)
		{
		  if (pPriv->socfd < 0)
		    {
		      trx_debug ("Attempting to reconnect", pgs->name, "FULL",
				 bt_DEBUG_MODE);
		      pPriv->socfd =
			UFGetConnection (pgs->name, pPriv->port_no,
					 bt_host_ip);
		      trx_debug ("came back from connect", pgs->name, "FULL",
				 bt_DEBUG_MODE);
		      *(long *) pgs->valb = (long) pPriv->socfd;	/* current 
									   socket 
									   number 
									 */
		    }
		  if (pPriv->socfd < 0)
		    {		/* we have a bad socket
				   connection */
		      pPriv->commandState = TRX_GS_DONE;
		      strcpy (pPriv->errorMessage,
			      "Error Connecting to Agent");
		      /* set the CAR to ERR */
		      status = dbPutField (&pPriv->carinfo.carMessage,
					   DBR_STRING, pPriv->errorMessage,
					   1);
		      if (status)
			{
			  trx_debug ("can't set CAR message", pgs->name,
				     "NONE", bt_DEBUG_MODE);
			  return OK;
			}
		      some_num = CAR_ERROR;
		      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to ERROR", pgs->name,
				     "NONE", bt_DEBUG_MODE);
			  return OK;
			}
		    }
		}


	      /* update output links if connection is good
	         or we are in SIMM_FAST */
	      if ((pPriv->boot_inp.command_mode == SIMM_FAST)
		  || (pPriv->socfd > 0))
		{
		  /* update the output links */

		  strcpy (pgs->valc, pPriv->CurrParam.annex);
		  strcpy (pgs->vald, pPriv->CurrParam.ls_218);
		  strcpy (pgs->vale, pPriv->CurrParam.ls_340);
		  strcpy (pgs->valf, pPriv->CurrParam.vacuum);
		  strcpy (pgs->valg, pPriv->CurrParam.indexors);
		  strcpy (pgs->valh, pPriv->CurrParam.cryocooler);
		  strcpy (pgs->vali, pPriv->CurrParam.ppcVME);
		  strcpy (pgs->valj, pPriv->CurrParam.mce4);
		  strcpy (pgs->valk, pPriv->CurrParam.executive);
		  strcpy (pgs->vall, pPriv->CurrParam.ufacqframed);
		  strcpy (pgs->valm, pPriv->CurrParam.ufgdhsd);
		  strcpy (pgs->valn, pPriv->CurrParam.ufgls218);
		  strcpy (pgs->valo, pPriv->CurrParam.ufgls340);
		  strcpy (pgs->valp, pPriv->CurrParam.ufg354vacd);
		  strcpy (pgs->valq, pPriv->CurrParam.ufgmotord);
		  strcpy (pgs->valr, pPriv->CurrParam.ufgmce4d);

		  /* Reset Command state to DONE */
		  pPriv->commandState = TRX_GS_DONE;
		  some_num = CAR_IDLE;
		  status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				       &some_num, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR to IDLE", pgs->name, "NONE",
				 bt_DEBUG_MODE);
		      return OK;
		    }
		}
	    }
	  else
	    {			/* we have an error in
				   input of the init
				   command */
	      /* Reset Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      strcpy (pPriv->errorMessage, "Bad INIT Directive BT");
	      /* set the CAR to ERR */
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     bt_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status =
		dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     bt_DEBUG_MODE);
		  return OK;
		}
	    }
	}
      else
	{			/* We have a configuration
				   command */
	  trx_debug ("We have a ObsControl Command", pgs->name, "FULL",
		     bt_DEBUG_MODE);
	  /* Save the current parameters */

	  strcpy (pPriv->OldParam.all_devices, pPriv->CurrParam.all_devices);
	  strcpy (pPriv->OldParam.annex, pPriv->CurrParam.annex);
	  strcpy (pPriv->OldParam.ls_218, pPriv->CurrParam.ls_218);
	  strcpy (pPriv->OldParam.ls_340, pPriv->CurrParam.ls_340);
	  strcpy (pPriv->OldParam.vacuum, pPriv->CurrParam.vacuum);
	  strcpy (pPriv->OldParam.indexors, pPriv->CurrParam.indexors);
	  strcpy (pPriv->OldParam.cryocooler, pPriv->CurrParam.cryocooler);
	  strcpy (pPriv->OldParam.ppcVME, pPriv->CurrParam.ppcVME);
	  strcpy (pPriv->OldParam.mce4, pPriv->CurrParam.mce4);
	  strcpy (pPriv->OldParam.all_agents, pPriv->CurrParam.all_agents);
	  strcpy (pPriv->OldParam.executive, pPriv->CurrParam.executive);
	  strcpy (pPriv->OldParam.ufacqframed, pPriv->CurrParam.ufacqframed);
	  strcpy (pPriv->OldParam.ufgdhsd, pPriv->CurrParam.ufgdhsd);
	  strcpy (pPriv->OldParam.ufgls218, pPriv->CurrParam.ufgls218);
	  strcpy (pPriv->OldParam.ufgls340, pPriv->CurrParam.ufgls340);
	  strcpy (pPriv->OldParam.ufg354vacd, pPriv->CurrParam.ufg354vacd);
	  strcpy (pPriv->OldParam.ufgmotord, pPriv->CurrParam.ufgmotord);
	  strcpy (pPriv->OldParam.ufgmce4d, pPriv->CurrParam.ufgmce4d);

	  /* get the input into the current parameters */
	  if (strcmp (pPriv->boot_inp.all_devices, "") != 0)
	    strcpy (pPriv->CurrParam.all_devices,
		    pPriv->boot_inp.all_devices);
	  if (strcmp (pPriv->boot_inp.annex, "") != 0)
	    strcpy (pPriv->CurrParam.annex, pPriv->boot_inp.annex);
	  if (strcmp (pPriv->boot_inp.ls_218, "") != 0)
	    strcpy (pPriv->CurrParam.ls_218, pPriv->boot_inp.ls_218);
	  if (strcmp (pPriv->boot_inp.ls_340, "") != 0)
	    strcpy (pPriv->CurrParam.ls_340, pPriv->boot_inp.ls_340);
	  if (strcmp (pPriv->boot_inp.vacuum, "") != 0)
	    strcpy (pPriv->CurrParam.vacuum, pPriv->boot_inp.vacuum);
	  if (strcmp (pPriv->boot_inp.indexors, "") != 0)
	    strcpy (pPriv->CurrParam.indexors, pPriv->boot_inp.indexors);
	  if (strcmp (pPriv->boot_inp.cryocooler, "") != 0)
	    strcpy (pPriv->CurrParam.cryocooler, pPriv->boot_inp.cryocooler);
	  if (strcmp (pPriv->boot_inp.ppcVME, "") != 0)
	    strcpy (pPriv->CurrParam.ppcVME, pPriv->boot_inp.ppcVME);
	  if (strcmp (pPriv->boot_inp.mce4, "") != 0)
	    strcpy (pPriv->CurrParam.mce4, pPriv->boot_inp.mce4);
	  if (strcmp (pPriv->boot_inp.all_agents, "") != 0)
	    strcpy (pPriv->CurrParam.all_agents, pPriv->boot_inp.all_agents);
	  if (strcmp (pPriv->boot_inp.executive, "") != 0)
	    strcpy (pPriv->CurrParam.executive, pPriv->boot_inp.executive);
	  if (strcmp (pPriv->boot_inp.ufacqframed, "") != 0)
	    strcpy (pPriv->CurrParam.ufacqframed,
		    pPriv->boot_inp.ufacqframed);
	  if (strcmp (pPriv->boot_inp.ufgdhsd, "") != 0)
	    strcpy (pPriv->CurrParam.ufgdhsd, pPriv->boot_inp.ufgdhsd);
	  if (strcmp (pPriv->boot_inp.ufgls218, "") != 0)
	    strcpy (pPriv->CurrParam.ufgls218, pPriv->boot_inp.ufgls218);
	  if (strcmp (pPriv->boot_inp.ufgls340, "") != 0)
	    strcpy (pPriv->CurrParam.ufgls340, pPriv->boot_inp.ufgls340);
	  if (strcmp (pPriv->boot_inp.ufg354vacd, "") != 0)
	    strcpy (pPriv->CurrParam.ufg354vacd, pPriv->boot_inp.ufg354vacd);
	  if (strcmp (pPriv->boot_inp.ufgmotord, "") != 0)
	    strcpy (pPriv->CurrParam.ufgmotord, pPriv->boot_inp.ufgmotord);
	  if (strcmp (pPriv->boot_inp.ufgmce4d, "") != 0)
	    strcpy (pPriv->CurrParam.ufgmce4d, pPriv->boot_inp.ufgmce4d);

	  /* see if this a command right after an init */
	  /* 
	     if (pPriv->send_init_param) {
	     pPriv->mot_inp.init_velocity =
	     pPriv->CurrParam.init_velocity ;
	     pPriv->mot_inp.slew_velocity =
	     pPriv->CurrParam.slew_velocity ;
	     pPriv->mot_inp.acceleration =
	     pPriv->CurrParam.acceleration;
	     pPriv->mot_inp.deceleration =
	     pPriv->CurrParam.deceleration ;
	     pPriv->mot_inp.drive_current =
	     pPriv->CurrParam.drive_current;
	     pPriv->mot_inp.datum_speed =
	     pPriv->CurrParam.datum_speed ;
	     pPriv->mot_inp.datum_direction =
	     pPriv->CurrParam.datum_direction ;
	     pPriv->send_init_param = 0 ; } */
	  /* formulate the command strings */
	  com = malloc (50 * sizeof (char *));
	  for (i = 0; i < 50; i++)
	    com[i] = malloc (70 * sizeof (char));
	  strcpy (rec_name, pgs->name);
	  rec_name[strlen (rec_name)] = '\0';
	  strcat (rec_name, ".J");

	  num_str =
	    UFcheck_bt_inputs (pPriv->boot_inp, com, rec_name,
			       &pPriv->CurrParam,
			       pPriv->CurrParam.command_mode);

	  /* Clear the input structure */
	  pPriv->boot_inp.command_mode = -1;

	  strcpy (pPriv->boot_inp.all_devices, "");
	  strcpy (pPriv->boot_inp.annex, "");
	  strcpy (pPriv->boot_inp.ls_218, "");
	  strcpy (pPriv->boot_inp.ls_340, "");
	  strcpy (pPriv->boot_inp.vacuum, "");
	  strcpy (pPriv->boot_inp.indexors, "");
	  strcpy (pPriv->boot_inp.cryocooler, "");
	  strcpy (pPriv->boot_inp.ppcVME, "");
	  strcpy (pPriv->boot_inp.mce4, "");
	  strcpy (pPriv->boot_inp.all_agents, "");
	  strcpy (pPriv->boot_inp.executive, "");
	  strcpy (pPriv->boot_inp.ufacqframed, "");
	  strcpy (pPriv->boot_inp.ufgdhsd, "");
	  strcpy (pPriv->boot_inp.ufgls218, "");
	  strcpy (pPriv->boot_inp.ufgls340, "");
	  strcpy (pPriv->boot_inp.ufg354vacd, "");
	  strcpy (pPriv->boot_inp.ufgmotord, "");
	  strcpy (pPriv->boot_inp.ufgmce4d, "");

	  /* do we have commands to send? */
	  if (num_str > 0)
	    {
	      /* if no Agent needed then go back to DONE */
	      /* we are talking about SIMM_FAST or commands 
	         that do not need the agent */
	      printf ("********** Thn number of strings is : %d\n", num_str);
	      for (i = 0; i < num_str; i++)
		printf ("%s\n", com[i]);

	      if ((pPriv->CurrParam.command_mode == SIMM_FAST))
		{
		  for (i = 0; i < 50; i++)
		    free (com[i]);
		  free (com);
		  /* set Command state to DONE */
		  pPriv->commandState = TRX_GS_DONE;
		  /* update the output links */

		  strcpy (pgs->valc, pPriv->CurrParam.annex);
		  strcpy (pgs->vald, pPriv->CurrParam.ls_218);
		  strcpy (pgs->vale, pPriv->CurrParam.ls_340);
		  strcpy (pgs->valf, pPriv->CurrParam.vacuum);
		  strcpy (pgs->valg, pPriv->CurrParam.indexors);
		  strcpy (pgs->valh, pPriv->CurrParam.cryocooler);
		  strcpy (pgs->vali, pPriv->CurrParam.ppcVME);
		  strcpy (pgs->valj, pPriv->CurrParam.mce4);
		  strcpy (pgs->valk, pPriv->CurrParam.executive);
		  strcpy (pgs->vall, pPriv->CurrParam.ufacqframed);
		  strcpy (pgs->valm, pPriv->CurrParam.ufgdhsd);
		  strcpy (pgs->valn, pPriv->CurrParam.ufgls218);
		  strcpy (pgs->valo, pPriv->CurrParam.ufgls340);
		  strcpy (pgs->valp, pPriv->CurrParam.ufg354vacd);
		  strcpy (pgs->valq, pPriv->CurrParam.ufgmotord);
		  strcpy (pgs->valr, pPriv->CurrParam.ufgmce4d);

		  some_num = CAR_IDLE;
		  status =
		    dbPutField (&pPriv->carinfo.carState, DBR_LONG, &some_num,
				1);
		  if (status)
		    {
		      logMsg ("can't set CAR to IDLE\n", 0, 0, 0, 0, 0, 0);
		      return OK;
		    }
		}
	      else
		{		/* we are in SIMM_NONE or
				   SIMM_FULL */
		  /* See if you can send the command and go 
		     to BUSY state */
		  if (pPriv->socfd > 0)
		    status =
		      ufStringsSend (pPriv->socfd, com, num_str, pgs->name);
		  else
		    status = 0;
		  for (i = 0; i < 50; i++)
		    free (com[i]);
		  free (com);
		  if (status == 0)
		    {		/* there was an error
				   sending the commands to
				   the agent */
		      /* set Command state to DONE */
		      pPriv->commandState = TRX_GS_DONE;
		      /* set the CAR to ERR with
		         appropriate message like "Bad
		         socket" and go to DONE state */
		      strcpy (pPriv->errorMessage, "Bad socket connection BT1");
		      /* set the CAR to ERR */
		      status = dbPutField (&pPriv->carinfo.carMessage,
					   DBR_STRING, pPriv->errorMessage,
					   1);
		      if (status)
			{
			  trx_debug ("can't set CAR message", pgs->name,
				     "NONE", bt_DEBUG_MODE);
			  return OK;
			}
		      some_num = CAR_ERROR;
		      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
					   &some_num, 1);
		      if (status)
			{
			  trx_debug ("can't set CAR to ERROR", pgs->name,
				     "NONE", bt_DEBUG_MODE);
			  return OK;
			}
		    }
		  else
		    {		/* send successful */
		      /* establish a CALLBACK */
		      requestCallback (pPriv->pCallback, TRX_BT_TIMEOUT);
		      pPriv->startingTicks = tickGet ();
		      /* set Command state to BUSY */
		      pPriv->commandState = TRX_GS_BUSY;
		    }
		}
	    }
	  else
	    {
	      for (i = 0; i < 50; i++)
		free (com[i]);
	      free (com);
	      if (num_str < 0)
		{
		  /* set Command state to DONE */
		  pPriv->commandState = TRX_GS_DONE;
		  /* we have an error in the Input */
		  strcpy (pPriv->errorMessage, "Error in input BT");
		  /* set the CAR to ERR */
		  status = dbPutField (&pPriv->carinfo.carMessage,
				       DBR_STRING, pPriv->errorMessage, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR message", pgs->name, "NONE",
				 bt_DEBUG_MODE);
		      return OK;
		    }
		  some_num = CAR_ERROR;
		  status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				       &some_num, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
				 bt_DEBUG_MODE);
		      return OK;
		    }
		}
	      else
		{		/* num_str == 0 */
		  /* is it possible to get here? */
		  /* here we have a change in input.  The
		     inputs though got sent to be checked
		     but no string got formulated.  */
		  /* this can only happen if somehow the
		     memory got corrupted */
		  /* or the gensub is being processed by an 
		     alien. */
		  /* Weird. */
		  trx_debug ("Unexpected processing:SENDING", pgs->name,
			     "NONE", bt_DEBUG_MODE);
		}
	    }
	}

      /* Is this a J field processing? */
      strcpy (response, pgs->j);
      if (strcmp (response, "") != 0)
	{
	}

      if (strcmp (response, "") != 0)
	{
	  /* in this state we should not expect anything in 
	     the J field from the Agent unless the agent
	     is late with something */
	  /* Log a message that the agent responded
	     unexpectedly */
	  trx_debug ("Unexpected response from Agent:SENDING", pgs->name,
		     "NONE", bt_DEBUG_MODE);
	}
      /* Clear the J Field */
      strcpy (pgs->j, "");

      break;

    case TRX_GS_BUSY:
      trx_debug ("Processing from BUSY state", pgs->name, "FULL",
		 bt_DEBUG_MODE);
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need 
         for 0.5 delay */
      /* establish a CALLBACK */
      /* 
         if ( (strcmp(pgs->i,"") != 0) ||
         (strcmp(pgs->k,"") != 0)) { cancelCallback
         (pPriv->pCallback);
         strcpy(pPriv->mot_inp.stop_mess,pgs->i) ;
         strcpy(pPriv->mot_inp.abort_mess,pgs->k) ;
         strcpy(pgs->i,"") ; strcpy(pgs->k,"") ;
         requestCallback(pPriv->pCallback,0 ) ;
         pPriv->commandState = TRX_GS_SENDING; return OK ; 
         } */

      strcpy (response, pgs->j);
      if (strcmp (response, "") != 0)
	{
	  trx_debug (response, pgs->name, "NONE", bt_DEBUG_MODE);
	}

      if (strcmp (response, "") == 0)
	{
	  /* is this a CALLBACK timeout? or someone pushed
	     the apply? */
	  printf ("******** Is this a callback from timeout?\n");
	  printf ("%s\n", pPriv->errorMessage);
	  /* if CALLBACK timeout, then set CAR to ERR and
	     go to DONE state */
	  ticksNow = tickGet ();
	  if ((ticksNow >= (pPriv->startingTicks + TRX_BT_TIMEOUT)) &&
	      (strcmp (pPriv->errorMessage, "CALLBACK") == 0))
	    {
	      /* set Command state to DONE */
	      pPriv->commandState = TRX_GS_DONE;
	      /* The command timed out */
	      strcpy (pPriv->errorMessage, "Boot Command Timed out");
	      /* set the CAR to ERR */
	      status = dbPutField (&pPriv->carinfo.carMessage,
				   DBR_STRING, pPriv->errorMessage, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR message", pgs->name, "NONE",
			     bt_DEBUG_MODE);
		  return OK;
		}
	      some_num = CAR_ERROR;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
			     bt_DEBUG_MODE);
		  return OK;
		}
	    }
	  else
	    {
	      /* Quit playing with the buttons. Can't you
	         see I am BUSY? */
	      trx_debug ("Unexpected processing:BUSY", pgs->name, "NONE",
			 bt_DEBUG_MODE);
	    }

	}
      else
	{
	  /* What do we have from the Agent? */
	  /* if Agent is done then cancel the call back go 
	     to DONE state and set CAR to IDLE */
	  if (strcmp (response, "OK") == 0)
	    {
	      cancelCallback (pPriv->pCallback);
	      pPriv->commandState = TRX_GS_DONE;

	      /* update the output links */

	      strcpy (pgs->valc, pPriv->CurrParam.annex);
	      strcpy (pgs->vald, pPriv->CurrParam.ls_218);
	      strcpy (pgs->vale, pPriv->CurrParam.ls_340);
	      strcpy (pgs->valf, pPriv->CurrParam.vacuum);
	      strcpy (pgs->valg, pPriv->CurrParam.indexors);
	      strcpy (pgs->valh, pPriv->CurrParam.cryocooler);
	      strcpy (pgs->vali, pPriv->CurrParam.ppcVME);
	      strcpy (pgs->valj, pPriv->CurrParam.mce4);
	      strcpy (pgs->valk, pPriv->CurrParam.executive);
	      strcpy (pgs->vall, pPriv->CurrParam.ufacqframed);
	      strcpy (pgs->valm, pPriv->CurrParam.ufgdhsd);
	      strcpy (pgs->valn, pPriv->CurrParam.ufgls218);
	      strcpy (pgs->valo, pPriv->CurrParam.ufgls340);
	      strcpy (pgs->valp, pPriv->CurrParam.ufg354vacd);
	      strcpy (pgs->valq, pPriv->CurrParam.ufgmotord);
	      strcpy (pgs->valr, pPriv->CurrParam.ufgmce4d);


	      some_num = CAR_IDLE;
	      status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				   &some_num, 1);
	      if (status)
		{
		  trx_debug ("can't set CAR to IDLE", pgs->name, "NONE",
			     bt_DEBUG_MODE);
		  return OK;
		}
	    }
	  else
	    {
	      /* else do an update if you can and exit */
	      numnum = strtod (response, &endptr);
	      if (*endptr != '\0')
		{		/* we do not have a number */
		  cancelCallback (pPriv->pCallback);
		  pPriv->commandState = TRX_GS_DONE;
		  /* restore the old parameters */

		  strcpy (pPriv->CurrParam.all_devices,
			  pPriv->OldParam.all_devices);
		  strcpy (pPriv->CurrParam.annex, pPriv->OldParam.annex);
		  strcpy (pPriv->CurrParam.ls_218, pPriv->OldParam.ls_218);
		  strcpy (pPriv->CurrParam.ls_340, pPriv->OldParam.ls_340);
		  strcpy (pPriv->CurrParam.vacuum, pPriv->OldParam.vacuum);
		  strcpy (pPriv->CurrParam.indexors,
			  pPriv->OldParam.indexors);
		  strcpy (pPriv->CurrParam.cryocooler,
			  pPriv->OldParam.cryocooler);
		  strcpy (pPriv->CurrParam.ppcVME, pPriv->OldParam.ppcVME);
		  strcpy (pPriv->CurrParam.mce4, pPriv->OldParam.mce4);
		  strcpy (pPriv->CurrParam.all_agents,
			  pPriv->OldParam.all_agents);
		  strcpy (pPriv->CurrParam.executive,
			  pPriv->OldParam.executive);
		  strcpy (pPriv->CurrParam.ufacqframed,
			  pPriv->OldParam.ufacqframed);
		  strcpy (pPriv->CurrParam.ufgdhsd, pPriv->OldParam.ufgdhsd);
		  strcpy (pPriv->CurrParam.ufgls218,
			  pPriv->OldParam.ufgls218);
		  strcpy (pPriv->CurrParam.ufgls340,
			  pPriv->OldParam.ufgls340);
		  strcpy (pPriv->CurrParam.ufg354vacd,
			  pPriv->OldParam.ufg354vacd);
		  strcpy (pPriv->CurrParam.ufgmotord,
			  pPriv->OldParam.ufgmotord);
		  strcpy (pPriv->CurrParam.ufgmce4d,
			  pPriv->OldParam.ufgmce4d);

		  /* set the CAR to ERR with whatever the
		     Agent sent you */
		  strcpy (pPriv->errorMessage, response);
		  /* set the CAR to ERR */
		  status = dbPutField (&pPriv->carinfo.carMessage,
				       DBR_STRING, pPriv->errorMessage, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR message", pgs->name, "NONE",
				 bt_DEBUG_MODE);
		      return OK;
		    }
		  some_num = CAR_ERROR;
		  status = dbPutField (&pPriv->carinfo.carState, DBR_LONG,
				       &some_num, 1);
		  if (status)
		    {
		      trx_debug ("can't set CAR to ERROR", pgs->name, "NONE",
				 bt_DEBUG_MODE);
		      return OK;
		    }
		}
	    }
	}
      /* Clear the J Field */
      strcpy (pgs->j, "");
      break;
    }				/* switch
				   (pPriv->commandState) */

  trx_debug ("Exiting", pgs->name, "FULL", bt_DEBUG_MODE);
  return OK;
}







#endif /* __UFGEMGENSUBCOMMBT_C__ */
