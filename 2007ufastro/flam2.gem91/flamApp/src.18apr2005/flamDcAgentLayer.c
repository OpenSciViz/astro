

/*
 *
 *  Header for dc agent simulation CAD record code.
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <msgQLib.h>

#include <recSup.h>

#include <cad.h>
#include <cadRecord.h>

#include <flamDcAgentLayer.h>
#include <flamDcAgentSim.h>


/*
 * Local defines
 */

#define MAX_MESSAGES                5	/* max messages per 
					   queue */
#define CAD_Q_WAIT_TIME             5	/* max time to wait 
					   for Q */


/*
 *  Private data
 */

static MSG_Q_ID dcCommandQ = NULL;	/* message queue
					   for agent */



/*
 *************************************
 *
 *  Header for dcCadInit
 *
 *
 *************************************
 */


long
dcCadInit (cadRecord * pcr	/* cad record structure */
  )
{
  long status;			/* function return status */



  /* 
   *  If a message queue has already been created then there is nothing
   *  to do since initialization is only done the first time this function
   *  is called
   */

  if (dcCommandQ)
    {
      return OK;
    }


  /* 
   *  Otherwise this is the first call, create a message queue 
   *  to communicate with the simulated DC agent then initialize
   *  the agent itself.
   */

  dcCommandQ = msgQCreate (MAX_MESSAGES, sizeof (dcAgentCommand), MSG_Q_FIFO);
  if (dcCommandQ == NULL)
    {
      status = -1;
      recGblRecordError (status, pcr, __FILE__ ":can't create message Queue");
      return status;
    }

  status = initDcAgentSim (dcCommandQ);

  return status;
}


 /* 
  *************************************
  *
  *  Header for dcCadProcess 
  *
  *
  *************************************
  */

long
dcCadProcess (cadRecord * pcr	/* cad record structure */
  )
{
  char *pCommand;		/* command name pointer */
  dcAgentCommand command;	/* agent command structure */
  long status;			/* function return status */


  /* 
   *  Process adcording to the directive given
   */

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be adcepted immediately
       */

    case menuDirectiveMARK:
      break;

    case menuDirectiveCLEAR:
      break;

    case menuDirectiveSTOP:
      break;


      /* 
       *  Preset and Start both check the input attributes before doing
       *  anything.
       */

    case menuDirectivePRESET:
    case menuDirectiveSTART:

      /* 
       *  Preset just checks the attributes, so bail out here
       */

      if (pcr->dir == menuDirectivePRESET)
	{
	  break;
	}


      /* 
       *  Start executes the command.
       */


      /* 
       *  Copy record names into the command structure then
       *  place it on the message queue.
       */

      strcpy (command.carName, "flam:dc:applyC");
      strcpy (command.attributeA, pcr->a);
      strcpy (command.attributeB, pcr->b);
      strcpy (command.attributeC, pcr->c);
      strcpy (command.attributeD, pcr->d);
      strcpy (command.attributeE, pcr->e);
      strcpy (command.attributeF, pcr->f);
      strcpy (command.attributeG, pcr->g);
      strcpy (command.attributeH, pcr->h);
      strcpy (command.attributeI, pcr->i);
      strcpy (command.attributeJ, pcr->j);
      strcpy (command.attributeK, pcr->k);
      strcpy (command.attributeL, pcr->l);
      strcpy (command.attributeM, pcr->m);
      strcpy (command.attributeN, pcr->n);
      strcpy (command.attributeO, pcr->o);
      strcpy (command.attributeP, pcr->p);
      strcpy (command.attributeQ, pcr->q);
      strcpy (command.attributeR, pcr->r);
      strcpy (command.attributeS, pcr->s);
      strcpy (command.attributeT, pcr->t);


      /* 
       *  Set the execution time based on the command received
       */

      for (pCommand = pcr->name; *pCommand != ':'; pCommand++);
      for (pCommand++; *pCommand != ':'; pCommand++);
      pCommand++;

      strcpy (command.commandName, pCommand);

      command.executionTime = 20;

      status = msgQSend (dcCommandQ,
			 (char *) &command,
			 sizeof (command), CAD_Q_WAIT_TIME, MSG_PRI_NORMAL);
      if (status == ERROR)
	{
	  strncpy (pcr->mess, "Can not send agent message", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      /* 
       *  Message sent successfully so copy inputs to outputs
       */

      strcpy (pcr->vala, pcr->a);
      strcpy (pcr->valb, pcr->b);
      strcpy (pcr->valc, pcr->c);
      strcpy (pcr->vald, pcr->d);
      strcpy (pcr->vale, pcr->e);
      strcpy (pcr->valf, pcr->f);
      strcpy (pcr->valg, pcr->g);
      strcpy (pcr->valh, pcr->h);
      strcpy (pcr->vali, pcr->i);
      strcpy (pcr->valj, pcr->j);
      strcpy (pcr->valk, pcr->k);
      strcpy (pcr->vall, pcr->l);
      strcpy (pcr->valm, pcr->m);
      strcpy (pcr->valn, pcr->n);
      strcpy (pcr->valo, pcr->o);
      strcpy (pcr->valp, pcr->p);
      strcpy (pcr->valq, pcr->q);
      strcpy (pcr->valr, pcr->r);
      strcpy (pcr->vals, pcr->s);
      strcpy (pcr->valt, pcr->t);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}
