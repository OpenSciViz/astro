

/*
 *
 *  Header stuffs for dcAgentSim.c
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <semLib.h>
#include <taskLib.h>
#include <sysLib.h>
#include <tickLib.h>
#include <logLib.h>
#include <msgQLib.h>

#include <recSup.h>
#include <db_access.h>
#include <dbAddr.h>
#include <car.h>

#include <flamDcAgentSim.h>


/*
 *   Local defines
 */

#define DC_AGENT_PRIORITY          80	/* timer task
					   priority */
#define DC_AGENT_STACK             0x1000	/* task
						   stack
						   size */
#define DC_AGENT_SCAN_RATE         10	/* task repeat per
					   sec */
#define DC_AGENT_OPTIONS           0	/* no extra task
					   options */

#define DC_AGENT_IDLE              0	/* waiting for a
					   command */
#define DC_AGENT_STARTING          1	/* starting a new
					   command */
#define DC_AGENT_EXECUTING         2	/* executing a
					   command */

#define DC_NOD_FRAMES              10	/* frames per nod
					   cycle */

#define DC_FRAME_TIME_HI           5	/* high background
					   exposure */
#define DC_TEMP_SET_HI             80.3	/* high background
					   det temp */
#define DC_POWER_SET_HI            4	/* high background
					   heater pwr */

#define DC_FRAME_TIME_LO           10	/* low background
					   exposure */
#define DC_TEMP_SET_LO             60.75	/* low
						   background 
						   det temp 
						 */
#define DC_POWER_SET_LO            2	/* low background
					   heater pwr */

#define DC_CAMERA_IDLE             0
#define DC_CAMERA_PAUSED           1
#define DC_CAMERA_EXPOSING         2


/*
 *
 *  Data structures
 *
 */

static long dcAgentState;	/* command progress flag */
static long dcActionState;	/* command state flag */
static dcAgentCommand command;	/* agent command to process 
				 */

static long scanDelayTicks;	/* ticks between task scans 
				 */
static long elapsedCommandTime;	/* elpased command time */
static long elapsedHeartbeatTime;	/* elapsed
					   heartbeat time */

static long chopping;		/* chopping requested flag */
static long nodding;		/* nodding requested flag */
static long onSource;		/* exposing on source flag */

static long onSourceTime;	/* looking at source flag */
static long frameTime;		/* exposure frame time */

static long elapsedFrameTime;	/* elapsed exposure time */
static long elapsedNodFrames;	/* frames since last nod */
static long elapsedOnSourceTime;	/* elapsed time on
					   source */

static long heartbeat;		/* heartbeat counter */
long flamDcDebug = FALSE;	/* current debug level */

static double tempSet;		/* heater setpoint */
static long powerSet;		/* heater power */

static long acq;		/* aquisition flag */
static long prep;		/* prepare-to-readout flag */
static long rdout;		/* reading out flag */
static long cameraState;	/* idle/exposing/readout */
static long lastCameraState;	/* last recognized state */
static long nodComplete;	/* telescope is in position 
				 */

static struct dbAddr carIval;	/* place to write CAR value 
				 */
static struct dbAddr carImss;	/* place to write CAR
				   message */
static struct dbAddr observeCIval;	/* observation in
					   progress..  */
static struct dbAddr observeCImss;	/* observation
					   error location */

static struct dbAddr heartbeatVal;	/* where to write
					   heartbeat */
static struct dbAddr debugVal;	/* current debugging level */
static struct dbAddr logVal;	/* where to write log
				   messages */
static struct dbAddr stateVal;	/* state status record */

static struct dbAddr acqVal;	/* acq status record */
static struct dbAddr prepVal;	/* prep status record */
static struct dbAddr rdoutVal;	/* rdout status record */
static struct dbAddr nodCompleteVal;	/* not cycle
					   complete flag */

static struct dbAddr tempSetVal;	/* detector
					   temperature */
static struct dbAddr powerSetVal;	/* detetctor heater 
					   power */

static MSG_Q_ID dcCommandQ = NULL;	/* commands arrive
					   here */


/*
 *
 *  Private function prototypes
 *
 */

static int dcAgentSimTask (int, int, int, int, int, int, int, int, int, int);



/*
 *
 *  Public functions
 *
 */


/*
 *  Add header for initDcAgentSim
 */

long
initDcAgentSim (MSG_Q_ID messageQ	/* queue to wait
					   for commands on */
  )
{
  static char buffer[MAX_STRING_SIZE];	/* scratchpad
					   buffer */
  int taskId;			/* timer task ID */
  long status;			/* function return status */



  /* 
   *  Make sure that the agent setup code is only executed once.
   */

  if (dcCommandQ)
    {
      return OK;
    }


  /* 
   *  Define where commands will be coming from
   */

  dcCommandQ = messageQ;


  /* 
   *  Initialize the agent
   */

  scanDelayTicks = sysClkRateGet () / DC_AGENT_SCAN_RATE;

  elapsedCommandTime = 0;
  elapsedFrameTime = 0;
  elapsedHeartbeatTime = 0;
  elapsedOnSourceTime = 0;
  elapsedNodFrames = 0;

  onSourceTime = 0;
  frameTime = 0;

  chopping = FALSE;
  nodding = FALSE;
  acq = FALSE;
  prep = FALSE;
  rdout = FALSE;

  cameraState = DC_CAMERA_IDLE;
  lastCameraState = DC_CAMERA_IDLE;
  nodComplete = FALSE;

  tempSet = DC_TEMP_SET_HI;
  powerSet = DC_POWER_SET_HI;

  heartbeat = 0;
  dcAgentState = DC_AGENT_IDLE;
  dcActionState = CAR_IDLE;



  /* 
   *  Connect to the epics database
   */

  strcpy (buffer, "flam:observeC.IVAL");
  status = dbNameToAddr (buffer, &observeCIval);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:observeC.IMSS");
  status = dbNameToAddr (buffer, &observeCImss);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:heartbeat.VAL");
  status = dbNameToAddr (buffer, &heartbeatVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:acq.VAL");
  status = dbNameToAddr (buffer, &acqVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:prep.VAL");
  status = dbNameToAddr (buffer, &prepVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:rdout.VAL");
  status = dbNameToAddr (buffer, &rdoutVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:TCSISnodCompl.J");
  status = dbNameToAddr (buffer, &nodCompleteVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:state.VAL");
  status = dbNameToAddr (buffer, &stateVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:debug.VAL");
  status = dbNameToAddr (buffer, &debugVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:log.VAL");
  status = dbNameToAddr (buffer, &logVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:obsControlG.S");
  status = dbNameToAddr (buffer, &tempSetVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }

  strcpy (buffer, "flam:dc:obsControlG.T");
  status = dbNameToAddr (buffer, &powerSetVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }


  /* 
   *  Create the timekeeping task
   */

  taskId = taskSpawn ("tDcAgentSim",	/* task name */
		      DC_AGENT_PRIORITY,	/* set
						   priority 
						 */
		      DC_AGENT_OPTIONS,	/* set options */
		      DC_AGENT_STACK,	/* set stack size */
		      dcAgentSimTask,	/* function to
					   spawn */
		      0, 0, 0, 0, 0, 0, 0, 0, 0, 0);	/* invocation 
							   args 
							 */
  if (taskId == NULL)
    {
      status = -1;
      logMsg ("can't create DC agent simulator", 0, 0, 0, 0, 0, 0);
      return status;
    }

  return OK;
}


/*
 *  Header for dc agent simulator task
 */


int dcAgentSimTask
  (int a1, int a2, int a3, int a4, int a5,
   int a6, int a7, int a8, int a9, int a10)
{
  static char buffer[MAX_STRING_SIZE];	/* scratchpad
					   buffer */
  long startingTicks;		/* system ticks at start of 
				   task */
  long elapsedTicks;		/* system ticks elapsed
				   during task */
  long action;			/* curent action state */
  long options = 0;		/* dbGetField options */
  long nRequest = 1;		/* dbGetField number of
				   elements */
  long status;			/* function return status */


  while (TRUE)
    {
      /* 
       *  Get the time at task start
       */

      startingTicks = tickGet ();


      /* 
       *  fake exposure status stuffs get handled first.....
       */

      switch (cameraState)
	{
	  /* 
	   *  In idle state we wait for the next exposure to start
	   */

	case DC_CAMERA_IDLE:
	  if (lastCameraState != DC_CAMERA_IDLE)
	    {
	      lastCameraState = DC_CAMERA_IDLE;

	      if (flamDcDebug)
		logMsg ("<%ld> stopping readout\n",
			(int) tickGet (), 0, 0, 0, 0, 0);

	      if (flamDcDebug)
		logMsg ("<%ld> exposure finished\n",
			(int) tickGet (), 0, 0, 0, 0, 0);

	      acq = FALSE;
	      rdout = FALSE;

	      status = dbPutField (&acqVal, DBR_LONG, &acq, 1);
	      if (status)
		{
		  logMsg ("can't write to acq.VAL", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&rdoutVal, DBR_LONG, &rdout, 1);
	      if (status)
		{
		  logMsg ("can't write to rdout.VAL", 0, 0, 0, 0, 0, 0);
		}

	      strcpy (buffer, "IDLE");
	      status = dbPutField (&stateVal, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to state.VAL", 0, 0, 0, 0, 0, 0);
		}

	      break;
	    }

	  break;


	  /* 
	   *  In paused state we wait for the telescope to get in
	   *  position after a nod request.
	   */

	case DC_CAMERA_PAUSED:
	  if (lastCameraState != DC_CAMERA_PAUSED)
	    {
	      lastCameraState = DC_CAMERA_PAUSED;
	      rdout = FALSE;

	      status = dbPutField (&rdoutVal, DBR_LONG, &rdout, 1);
	      if (status)
		{
		  logMsg ("can't write to rdout.VAL", 0, 0, 0, 0, 0, 0);
		}

	      strcpy (buffer, "PAUSED");
	      status = dbPutField (&stateVal, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to state.VAL", 0, 0, 0, 0, 0, 0);
		}

	      break;
	    }

	  status = dbGetField (&nodCompleteVal,
			       DBR_LONG,
			       &nodComplete, &options, &nRequest, NULL);
	  if (status)
	    {
	      logMsg ("can't read from nodComplete.VAL", 0, 0, 0, 0, 0, 0);
	    }

	  if (nodComplete)
	    {
	      if (flamDcDebug)
		logMsg ("<%ld> nod complete\n",
			(int) tickGet (), 0, 0, 0, 0, 0);

	      cameraState = DC_CAMERA_EXPOSING;
	    }

	  break;


	  /* 
	   *  In exposing state we count the time on source and trigger
	   *  nod cycles if required.
	   */

	case DC_CAMERA_EXPOSING:
	  if (lastCameraState == DC_CAMERA_PAUSED)
	    {
	      lastCameraState = DC_CAMERA_EXPOSING;

	      if (flamDcDebug)
		logMsg ("<%ld> starting readout\n",
			(int) tickGet (), 0, 0, 0, 0, 0);

	      rdout = TRUE;
	      status = dbPutField (&rdoutVal, DBR_LONG, &rdout, 1);
	      if (status)
		{
		  logMsg ("can't write to rdout.VAL", 0, 0, 0, 0, 0, 0);
		}

	      strcpy (buffer, "EXPOSING");
	      status = dbPutField (&stateVal, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to state.VAL", 0, 0, 0, 0, 0, 0);
		}

	      break;
	    }

	  else if (lastCameraState == DC_CAMERA_IDLE)
	    {
	      lastCameraState = DC_CAMERA_EXPOSING;

	      if (flamDcDebug)
		logMsg ("<%ld> starting exposure\n",
			(int) tickGet (), 0, 0, 0, 0, 0);

	      if (flamDcDebug)
		logMsg ("<%ld> starting readout\n",
			(int) tickGet (), 0, 0, 0, 0, 0);
	      acq = TRUE;
	      rdout = TRUE;

	      status = dbPutField (&acqVal, DBR_LONG, &acq, 1);
	      if (status)
		{
		  logMsg ("can't write to acq.VAL", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&rdoutVal, DBR_LONG, &rdout, 1);
	      if (status)
		{
		  logMsg ("can't write to rdout.VAL", 0, 0, 0, 0, 0, 0);
		}

	      strcpy (buffer, "EXPOSING");
	      status = dbPutField (&stateVal, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to state.VAL", 0, 0, 0, 0, 0, 0);
		}


	      strcpy (buffer, "");
	      status = dbPutField (&observeCImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to observeC.IMSS", 0, 0, 0, 0, 0, 0);
		}

	      action = CAR_BUSY;
	      status = dbPutField (&observeCIval, DBR_LONG, &action, 1);
	      if (status)
		{
		  logMsg ("can't write to observeC.IVAL", 0, 0, 0, 0, 0, 0);
		}
	      break;
	    }


	  /* 
	   *  Throw in some fake detector hardware errors occasionally...
	   */

	  if ((tickGet () % 10000) == 0)
	    {
	      logMsg ("Nasty camera controler fault\n", 0, 0, 0, 0, 0, 0);

	      action = CAR_ERROR;
	      strcpy (buffer, "Nasty camera controller fault....");

	      status = dbPutField (&observeCImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't write to observeC.IMSS", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&observeCIval, DBR_LONG, &action, 1);
	      if (status)
		{
		  logMsg ("can't write to observeC.IVAL", 0, 0, 0, 0, 0, 0);
		}

	      cameraState = DC_CAMERA_IDLE;
	      break;
	    }


	  /* 
	   *  While we are on source accumulate photons...
	   */

	  if (onSource)
	    {
	      elapsedOnSourceTime++;
	    }

	  if (elapsedOnSourceTime >= onSourceTime)
	    {
	      elapsedOnSourceTime = 0;

	      action = CAR_IDLE;
	      status = dbPutField (&observeCIval, DBR_LONG, &action, 1);
	      if (status)
		{
		  logMsg ("can't write to observeC.IVAL", 0, 0, 0, 0, 0, 0);
		}

	      cameraState = DC_CAMERA_IDLE;
	      break;
	    }


	  /* 
	   *  Fake frame readout and chop/nod stuffs here....
	   */

	  if (elapsedFrameTime++ >= frameTime)
	    {
	      elapsedFrameTime = 0;

	      if (flamDcDebug)
		{
		  if (onSource)
		    {
		      logMsg ("<%ld> finished on-source frame\n",
			      (int) tickGet (), 0, 0, 0, 0, 0);
		    }
		  else
		    {
		      logMsg ("<%ld> finished off-source frame\n",
			      (int) tickGet (), 0, 0, 0, 0, 0);
		    }
		}

	      if (chopping)
		{
		  if (flamDcDebug)
		    logMsg ("<%ld> sending chop signal\n",
			    (int) tickGet (), 0, 0, 0, 0, 0);

		  onSource = !onSource;

		}

	      if (nodding)
		{
		  if (elapsedNodFrames++ >= DC_NOD_FRAMES)
		    {
		      elapsedNodFrames = 0;

		      if (flamDcDebug)
			logMsg ("<%ld> stopping readout\n",
				(int) tickGet (), 0, 0, 0, 0, 0);

		      if (flamDcDebug)
			logMsg ("<%ld> starting nod\n",
				(int) tickGet (), 0, 0, 0, 0, 0);

		      cameraState = DC_CAMERA_PAUSED;
		      break;
		    }
		}
	    }

	  break;

	default:
	  break;
	}



      /* 
       *  Process adcording to the current state of the command
       *  simulation state machine.
       */

      switch (dcAgentState)
	{
	  /* 
	   *  In IDLE state wait for a new command to arrive
	   */

	case DC_AGENT_IDLE:
	  status = msgQReceive ((MSG_Q_ID) dcCommandQ,
				(char *) &command, sizeof (command), NO_WAIT);

	  if (status == ERROR && errno == S_objLib_OBJ_UNAVAILABLE)
	    {
	      break;
	    }

	  else if (status == sizeof (command))
	    {
	      dcAgentState = DC_AGENT_STARTING;
	    }

	  else
	    {
	      logMsg ("dcAgentSim:msgQReceive failed\n", 0, 0, 0, 0, 0, 0);
	    }

	  break;


	  /* 
	   *  In STARTING state we locate the car record and set it to BUSY
	   */

	case DC_AGENT_STARTING:

	  /* 
	   *  Locate the CAR record associated with this command
	   */

	  strcpy (buffer, command.carName);
	  strcat (buffer, ".IVAL");

	  status = dbNameToAddr (buffer, &carIval);
	  if (status)
	    {
	      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
	      dcAgentState = DC_AGENT_IDLE;
	      break;
	    }


	  strcpy (buffer, command.carName);
	  strcat (buffer, ".IMSS");

	  status = dbNameToAddr (buffer, &carImss);
	  if (status)
	    {
	      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
	      dcAgentState = DC_AGENT_IDLE;
	      break;
	    }


	  /* 
	   *  Make sure that the CAR record is BUSY while executing
	   *  commands
	   */

	  if (dcActionState != CAR_BUSY)
	    {

	      dcActionState = CAR_BUSY;
	      strcpy (buffer, "");

	      status = dbPutField (&carImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IMSS", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&carIval, DBR_LONG, &dcActionState, 1);
	      if (status)
		{
		  logMsg ("can't write to CAR.IVAL", 0, 0, 0, 0, 0, 0);
		  dcAgentState = DC_AGENT_IDLE;
		  break;
		}
	    }


	  /* 
	   *  Start executing the received command
	   */

	  dcAgentState = DC_AGENT_EXECUTING;

	  /* 
	   *  Log the command .....
	   */

	  if (strcmp (command.commandName, "acqControl") == 0)
	    {
	      if (flamDcDebug)
		logMsg ("<%ld> %s starting execution\n",
			(int) tickGet (),
			(int) &command.commandName, 0, 0, 0, 0);

	      if (flamDcDebug)
		logMsg ("CM:%s ID:%s DHS:%s QL:%s SIM:%s\n",
			(int) &command.attributeA,
			(int) &command.attributeB,
			(int) &command.attributeC,
			(int) &command.attributeD,
			(int) &command.attributeE, 0);

	      if (strcmp (command.attributeA, "INIT") == 0)
		{
		  strcpy (buffer, "IDLE");
		  status = dbPutField (&stateVal, DBR_STRING, &buffer, 1);

		  if (status)
		    {
		      logMsg ("can't write to state.VAL", 0, 0, 0, 0, 0, 0);
		    }

		  cameraState = DC_CAMERA_IDLE;
		}

	      else if (strcmp (command.attributeA, "START") == 0)
		{
		  onSource = TRUE;
		  cameraState = DC_CAMERA_EXPOSING;
		}

	      else if (strcmp (command.attributeA, "ABORT") == 0)
		{
		  action = CAR_IDLE;
		  status = dbPutField (&observeCIval, DBR_LONG, &action, 1);
		  if (status)
		    {
		      logMsg ("can't write observeC.IVAL", 0, 0, 0, 0, 0, 0);
		    }

		  cameraState = DC_CAMERA_IDLE;
		}

	      else if (strcmp (command.attributeA, "STOP") == 0)
		{
		  action = CAR_IDLE;
		  status = dbPutField (&observeCIval, DBR_LONG, &action, 1);
		  if (status)
		    {
		      logMsg ("can't write observeC.IVAL", 0, 0, 0, 0, 0, 0);
		    }

		  cameraState = DC_CAMERA_IDLE;
		}

	      else if (strcmp (command.attributeA, "CONFIGURE") == 0)
		{
		  sprintf (buffer, "%.2f", tempSet);
		  status = dbPutField (&tempSetVal, DBR_STRING, buffer, 1);
		  if (status)
		    {
		      logMsg ("can't write to tempSet", 0, 0, 0, 0, 0, 0);
		      return status;
		    }

		  sprintf (buffer, "%ld", powerSet);
		  status = dbPutField (&powerSetVal, DBR_STRING, buffer, 1);
		  if (status)
		    {
		      logMsg ("can't write to powerSet", 0, 0, 0, 0, 0, 0);
		      return status;
		    }
		}

	    }

	  else if (strcmp (command.commandName, "obsControl") == 0)
	    {
	      if (flamDcDebug)
		logMsg ("<%ld> %s starting execution\n",
			(int) tickGet (),
			(int) &command.commandName, 0, 0, 0, 0);

	      if (flamDcDebug)
		logMsg ("CMD:%s CAM:%s OBS:%s OST:%s\n",
			(int) &command.attributeA,
			(int) &command.attributeD,
			(int) &command.attributeE,
			(int) &command.attributeF, 0, 0);

	      if (flamDcDebug)
		logMsg ("FLT:%s GRT:%s LYT:%s SEC:%s SLT:%s WIN:%s\n",
			(int) &command.attributeH,
			(int) &command.attributeI,
			(int) &command.attributeM,
			(int) &command.attributeN,
			(int) &command.attributeO, (int) &command.attributeP);

	      if (flamDcDebug)
		logMsg ("CHP:%s EFF:%s CWL:%s LLO:%s LHI:%s\n",
			(int) &command.attributeR,
			(int) &command.attributeQ,
			(int) &command.attributeJ,
			(int) &command.attributeB,
			(int) &command.attributeC, 0);

	      chopping = ((strcmp (command.attributeE, "chop-nod") == 0) ||
			  (strcmp (command.attributeE, "chop") == 0));

	      nodding = ((strcmp (command.attributeE, "chop-nod") == 0) ||
			 (strcmp (command.attributeE, "nod") == 0));

	      sscanf (command.attributeF, "%ld", &onSourceTime);
	      if (flamDcDebug)
		logMsg ("onSourceTime = %d\n",
			(int) onSourceTime, 0, 0, 0, 0, 0);

	      if (tickGet () % 2)
		{
		  frameTime = DC_FRAME_TIME_HI;
		  tempSet = DC_TEMP_SET_HI;
		  powerSet = DC_POWER_SET_HI;
		}
	      else
		{
		  frameTime = DC_FRAME_TIME_LO;
		  tempSet = DC_TEMP_SET_LO;
		  powerSet = DC_POWER_SET_LO;
		}
	    }

	  break;


	  /* 
	   *  In EXECUTING state we fake execution by waiting for the
	   *  given execution time then setting the CAR to IDLE
	   */

	case DC_AGENT_EXECUTING:

	  /* 
	   *  Wait for the command execution time to expire
	   */

	  if (elapsedCommandTime++ >= command.executionTime)
	    {
	      elapsedCommandTime = 0;

	      if (flamDcDebug)
		logMsg ("<%ld> %s finished execution\n",
			(int) tickGet (),
			(int) &command.commandName, 0, 0, 0, 0);

	      /* 
	       *  If another command is ready stay busy and
	       *  go back to execute it immediately...
	       */

	      if (msgQNumMsgs (dcCommandQ) != 0)
		{
		  dcAgentState = DC_AGENT_IDLE;
		  break;
		}


	      /* 
	       *  Throw in the occasional detector controller fault
	       */

	      if ((tickGet () % 10000) == 0)
		{
		  logMsg ("Nasty camera controler fault\n", 0, 0, 0, 0, 0, 0);

		  dcActionState = CAR_ERROR;
		  strcpy (buffer, "Nasty detector controller fault....");
		}
	      else
		{
		  dcActionState = CAR_IDLE;
		  strcpy (buffer, "");
		}

	      status = dbPutField (&carImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IMSS", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&carIval, DBR_LONG, &dcActionState, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IVAL", 0, 0, 0, 0, 0, 0);
		}

	      dcAgentState = DC_AGENT_IDLE;
	    }

	  break;

	}			/* end switch */


      /* 
       *  Update the heartbeat counter once a second and write the
       *  new value to the agent heatbeat record....
       */

      if (elapsedHeartbeatTime++ >= DC_AGENT_SCAN_RATE - 1)
	{
	  elapsedHeartbeatTime = 0;
	  heartbeat++;
	  status = dbPutField (&heartbeatVal, DBR_LONG, &heartbeat, 1);
	  if (status)
	    {
	      logMsg ("can't adcess heartbeatVal", 0, 0, 0, 0, 0, 0);
	    }
	}

      /* 
       *  Task finished .... wait for a bit then run it again
       */

      elapsedTicks = tickGet () - startingTicks;
      if (elapsedTicks >= scanDelayTicks)
	{
	  logMsg ("dcAgentSim took too long\n", 0, 0, 0, 0, 0, 0);
	}
      else
	{
	  taskDelay (scanDelayTicks - elapsedTicks);
	}

    }				/* end while TRUE */

  return OK;			/* for completeness.... */
}
