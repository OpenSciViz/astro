

/*
 *
 *  Header file for dcAgentSim.c module.
 *
 */

#define MAX_STRING_SIZE 40

/*
 *
 *  Dc agent command structure
 *
 */

typedef struct
{
  char commandName[MAX_STRING_SIZE];
  char carName[MAX_STRING_SIZE];
  char attributeA[MAX_STRING_SIZE];
  char attributeB[MAX_STRING_SIZE];
  char attributeC[MAX_STRING_SIZE];
  char attributeD[MAX_STRING_SIZE];
  char attributeE[MAX_STRING_SIZE];
  char attributeF[MAX_STRING_SIZE];
  char attributeG[MAX_STRING_SIZE];
  char attributeH[MAX_STRING_SIZE];
  char attributeI[MAX_STRING_SIZE];
  char attributeJ[MAX_STRING_SIZE];
  char attributeK[MAX_STRING_SIZE];
  char attributeL[MAX_STRING_SIZE];
  char attributeM[MAX_STRING_SIZE];
  char attributeN[MAX_STRING_SIZE];
  char attributeO[MAX_STRING_SIZE];
  char attributeP[MAX_STRING_SIZE];
  char attributeQ[MAX_STRING_SIZE];
  char attributeR[MAX_STRING_SIZE];
  char attributeS[MAX_STRING_SIZE];
  char attributeT[MAX_STRING_SIZE];
  char attributeU[MAX_STRING_SIZE];
  long executionTime;
}
dcAgentCommand;

/*
 *
 *  Public function prototypes
 *
 */

long initDcAgentSim (MSG_Q_ID dcCommandQueue);
