
#include "vxWorks.h"
#include "stdio.h"
#include "string.h"


/* Gemini Records required */
#include "alarm.h"
#include "cadRecord.h"
#include "carRecord.h"
#include "genSubRecord.h"

#include "cad.h"
#include "menuCarstates.h"
#include "slalib.h"
#include "astLib.h"
#include "gem_wcs.h"

wcsHeader wcshdrTest;
char *trackFrameStringTest="FK5";
double trackWavelengthTest=5000.0;
char *trackEquinoxStringTest="J2000";

void wcsTestMeNow(void) {
   getWcs(trackFrameStringTest,&trackWavelengthTest,trackEquinoxStringTest,&wcshdrTest);
   }
   
