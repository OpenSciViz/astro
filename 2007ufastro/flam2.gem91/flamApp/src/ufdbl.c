#if !defined(__UFDBL_C__)
#define __UFDBL_C__ "$Name:  $ $Id: ufdbl.c,v 0.0 2005/09/01 20:23:14 drashkin Exp $"
static const char rcsId[] = __UFDBL_C__;

/* uf modification of "dbl" func. in epics 3.12Gem */

#include "vxWorks.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "timexLib.h"
#include "tickLib.h"
#include "memLib.h"
#include "ellLib.h"
/* #include "fast_lock.h" */
#include "dbDefs.h"
#include "dbAccess.h"
#include "dbBase.h"
/* #include "dbRecType.h"
#include "dbRecords.h" */
#include "dbCommon.h"
#include "recSup.h"
#include "devSup.h"
#include "drvSup.h"
#include "special.h"
/* #include "choice.h"
#include "dbRecDes.h" */
#include "dbStaticLib.h"
#include "dbEvent.h"
#include "ellLib.h"
#include "callback.h"
#include "cad.h"
#include "car.h"
/* #include "genSub.h" */
#include "genSubRecord.h"
#include "cadRecord.h"
#include "sirRecord.h"
#include "dbFldTypes.h"
#include "math.h"

#include "ufdbl.h"
#include "ufLog.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

typedef struct
{
  DBRstatus DBRtime float value[10];
}
MYBUFFER;

/* -extern struct dbBase *pdbBase; */
struct dbBase *pdbBase;

static int _verbose = 1;

static char *_agenthostIP = "000.000.000.000";
static char *_dbname = "flam or miri";
static struct dbAddr _iAddr;
static double theCenWaveLength = 10.5 ;

/* add datum counters -- hon */
static int _sysdatum = 0;
static int _ccdatum = 0;
static int _dcdatum = 0;
static int _ecdatum = 0;
static sirRecord* _datumCntSir= 0;
static sirRecord* _ccdatumCntSir= 0;
static sirRecord* _dcdatumCntSir= 0;
static sirRecord* _ecdatumCntSir= 0;

int ufGetDatumCnt(char* id) {
  /* default should be system datum counter */
  if( id == 0 )
    id = "sys";

  if( strstr(id, "cc") || strstr(id, "CC") )
    return _ccdatum;

  if( strstr(id, "dc") || strstr(id, "DC") )
    return _dcdatum;

  if( strstr(id, "ec") || strstr(id, "EC") )
    return _ecdatum;

  return _sysdatum;
}

int ufClearDatumCnt(char* id) {
  if( id == 0 )
    id = "sys";

  if( strstr(id, "cc") || strstr(id, "CC") ) {
    _ccdatum = 0;
    if( _ccdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_ccdatumCntSir->val)) = _ccdatum;
     return _ccdatum;
  }

  if( strstr(id, "dc") || strstr(id, "DC") ) {
    _dcdatum = 0;
    if( _dcdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_dcdatumCntSir->val)) = _dcdatum;
    return _dcdatum;
  }

  if( strstr(id, "ec") || strstr(id, "EC") )  {
    _ecdatum = 0;
    if( _ecdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_ecdatumCntSir->val)) = _ecdatum;
    return _ecdatum;
  }

  _sysdatum = 0; _ccdatum = 0; _dcdatum = 0; _ecdatum = 0;

  if( _ccdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_ccdatumCntSir->val)) = _ccdatum;
  if( _dcdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_dcdatumCntSir->val)) = _dcdatum;
  if( _ecdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_ecdatumCntSir->val)) = _ecdatum;
  if( _datumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_datumCntSir->val)) = _sysdatum;

  return 0;
}


int ufDatumCnt(char* id) {
  /* default should be system datum counter */
  if( id == 0 )
    id = "sys";

  if( strstr(id, "cc") || strstr(id, "CC") ) {
    ++_ccdatum;
    if( _ccdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_ccdatumCntSir->val)) = _ccdatum;
    return _ccdatum;
  }

  if( strstr(id, "dc") || strstr(id, "DC") ) {
    ++_dcdatum;
    if( _dcdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_dcdatumCntSir->val)) = _dcdatum;
    return _dcdatum;
  }

  if( strstr(id, "ec") || strstr(id, "EC") ) {
    ++_ecdatum;
   if( _ecdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
      *((int*)(_ecdatumCntSir->val)) = _ecdatum;
    return _ecdatum;
  }

  /* sys. datum assumes all subsystems succesfully datumed */
  ++_ccdatum; ++_dcdatum; ++_ecdatum; ++_sysdatum;

  if( _ccdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_ccdatumCntSir->val)) = _ccdatum;
  if( _dcdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_dcdatumCntSir->val)) = _dcdatum;
  if( _ecdatumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_ecdatumCntSir->val)) = _ecdatum;
  if( _datumCntSir ) /* force update of sir -- this func. is callable from oustide sir */ 
    *((int*)(_datumCntSir->val)) = _sysdatum;

  return _sysdatum;
}

void ufSetDatumCnt( const sirRecord* sPtr ) {
  char* id = "sys";
  int input = 1;
  if( 0 == sPtr ) {
    sprintf( _UFerrmsg, __HERE__ "> NULL sPtr" );
    ufLog( _UFerrmsg );
    return;
  }
  else if( 0 == sPtr->val ) {
    sprintf( _UFerrmsg, __HERE__ "> NULL sPtr->val" );
    ufLog( _UFerrmsg );
    return;
  }

  /* get subsys. id */
  id = (char*)sPtr->name;

  /* save sir record pointers */
  if( strstr(id, "cc") || strstr(id, "CC") ) {
    if( _ccdatumCntSir == 0 )
      _ccdatumCntSir = (sirRecord*) sPtr; /* should be unique */
  }
  else if( strstr(id, "dc") || strstr(id, "DC") ) {
    if( _dcdatumCntSir == 0 )
      _dcdatumCntSir = (sirRecord*) sPtr; /* should be unique */
  }
  else if( strstr(id, "dc") || strstr(id, "DC") ) {
    if( _dcdatumCntSir == 0 )
      _dcdatumCntSir = (sirRecord*) sPtr; /* should be unique */
  }
  else if( _datumCntSir == 0 ) {
      _datumCntSir = (sirRecord*) sPtr; /* should be unique */
  }

  input = *((int*)(sPtr->rval)); /* raw value input */
  if( input == 0 ) /* clear -- presumably by pvload at boot/startup */
    *((int*)(sPtr->val)) = ufClearDatumCnt(id);
  else /* any other input value is a 'process i.e. increment' action */
    *((int*)(sPtr->val)) = ufDatumCnt(id);

  sprintf( _UFerrmsg, __HERE__ "> datum counts (sys, cc, ec, dc): %d, %d, %d, %d",
	   _sysdatum, _ccdatum, _ecdatum, _dcdatum) ;
  ufLog( _UFerrmsg ) ;
  return;
}

double ufGetWaveLength()
{
  sprintf( _UFerrmsg, __HERE__ "> Returning: %f",
           theCenWaveLength ) ;
  ufLog( _UFerrmsg ) ;

  return theCenWaveLength ;
}

void
ufSetCentralWavelength( const char* w )
{
  if( w == 0 )
    theCenWaveLength = 0;
  else
    theCenWaveLength = atof(w);

  sprintf( _UFerrmsg, __HERE__ "> theCenWaveLength: %f", theCenWaveLength ) ;
  ufLog( _UFerrmsg ) ;
  return;
}

void
ufSetWaveLength( const sirRecord* sPtr )
{
  if( 0 == sPtr )
    {
      sprintf( _UFerrmsg, __HERE__ "> NULL sPtr" ) ;
      ufLog( _UFerrmsg ) ;
      return ;
    }

  if( 0 == sPtr->val )
    {
      sprintf( _UFerrmsg, __HERE__ "> NULL sPtr->val" ) ;
      ufLog( _UFerrmsg ) ;
      return ;
    }

  ufSetCentralWavelength( (const char*) sPtr->val ) ;
  return;
}

double ufGetFiducialAngle()
{
  char val[ 40 ] ;
  char recordName[ 40 ] ;

  const char* dbName = 0 ;

  sprintf( _UFerrmsg, __HERE__ "> BEGIN") ;
  ufLog( _UFerrmsg ) ;
  memset( val, 0, sizeof( val ) ) ;
  sprintf( _UFerrmsg, __HERE__ "> memset( val,") ;
  ufLog( _UFerrmsg ) ;
  memset( recordName, 0, sizeof( recordName ) ) ;
  sprintf( _UFerrmsg, __HERE__ "> after memset( recordName,") ;
  ufLog( _UFerrmsg ) ;

/*   dbName = ufdbName() ; */
  dbName = getenv ("UFEPICSDBNAME");
  sprintf( _UFerrmsg, __HERE__ "> dbName= %s ",dbName ) ;
  ufLog( _UFerrmsg ) ;
  if( 0 == dbName )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbName() returned NULL" ) ;
      ufLog( _UFerrmsg ) ;

      return 0.0 ;
    }

  strcpy( recordName, dbName ) ;
  strcat( recordName, ":sad:fiducialAngle" ) ;

  sprintf( _UFerrmsg, __HERE__ "> recordName= %s ",recordName ) ;
  ufLog( _UFerrmsg ) ;

  if( ufdbStrGet( recordName, val ) )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbStrGet() failed" ) ;
      ufLog( _UFerrmsg ) ;

      return 0.0 ;
    }

  return atof( val ) ;
}

double ufGetFiducialSteps()
{
  char val[ 40 ] ;
  char recordName[ 40 ] ;

  const char* dbName = 0 ;

  memset( val, 0, sizeof( val ) ) ;
  memset( recordName, 0, sizeof( recordName ) ) ;

  /* dbName = ufdbName() ; */
  dbName = getenv ("UFEPICSDBNAME");
  if( 0 == dbName )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbName() returned NULL" ) ;
      ufLog( _UFerrmsg ) ;

      return 0.0 ;
    }

  strcpy( recordName, dbName ) ;
  strcat( recordName, ":sad:fiducialSteps" ) ;

  if( ufdbStrGet( recordName, val ) )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbStrGet() failed" ) ;
      ufLog( _UFerrmsg ) ;

      return 0.0 ;
    }

  return atof( val ) ;
}

double uf_rint( double roundMe )
{
if( (roundMe - (int) roundMe) >= 0.5 )
	{
	return (((int) roundMe) + 1.0) ;
	}

return (int) roundMe ;
}

/**
 * This function will calculate and return the number of steps to offset
 * the high-res grating wheel, as a function of desired grating central wavelength.
 * This function also stores the adjusted (actual) wavelength into the appropriate SIR record.
 */
double ufGratingOffsetCalc()
{
  double gratingWavelen = 0.0 ;
  double fiducialAngle = 0.0 ;
  double fiducialSteps = 0.0 ;
  double predictedStep = 0.0 ;
  double roundedStep = 0.0 ;
  double adjWaveLength = 0.0 ;
  double roundedAngle = 0.0 ;
  double predictedAngle = 0.0 ;
  double degToRad = 2 * M_PI / 360.0;
  double K2blazeAng = 16.8,  mOrder=1.0,  Dgroove=11.11,  stepsRev=9000.0;

  /* Polynomial coefficients for power series approximation of data */
  double pCoeff[] = { 7.5111263, 2.9258179, -0.03931878, 0.00261846 } ;

  sprintf( _UFerrmsg, __HERE__ "> BEGIN");
  ufLog( _UFerrmsg ) ;
  gratingWavelen = ufGetWaveLength() ;
  sprintf( _UFerrmsg, __HERE__ "> wavelen=%f", gratingWavelen);
  ufLog( _UFerrmsg ) ;

  fiducialAngle = ufGetFiducialAngle() ;
  sprintf( _UFerrmsg, __HERE__ "> fiducialAngle=%f", fiducialAngle);
  ufLog( _UFerrmsg ) ;

  fiducialSteps = ufGetFiducialSteps() ;
  sprintf( _UFerrmsg, __HERE__ "> fiducialSteps=%f", fiducialSteps);
  ufLog( _UFerrmsg ) ;

  predictedAngle = pCoeff[ 0 ]
    + (pCoeff[ 1 ] * gratingWavelen)
    + (pCoeff[ 2 ] * pow( gratingWavelen, 2 ))
    + (pCoeff[ 3 ] * pow( gratingWavelen, 3 )) ;

  predictedStep = (stepsRev/360.0) * (predictedAngle - fiducialAngle) + fiducialSteps ;

  sprintf( _UFerrmsg, __HERE__ "> wavelen=%f, predSteps=%f", gratingWavelen, predictedStep );
  ufLog( _UFerrmsg ) ;

  roundedStep = uf_rint( predictedStep ) ;

  roundedAngle = (360.0/stepsRev) * (roundedStep - fiducialSteps) + fiducialAngle ;

  adjWaveLength = (Dgroove/mOrder) * ( sin( roundedAngle*degToRad ) +
				       sin( (roundedAngle - K2blazeAng)*degToRad ) ) ;

  sprintf( _UFerrmsg, __HERE__ "> actual Angle=%f, actual wavelen=%f", roundedAngle, adjWaveLength);
  ufLog( _UFerrmsg ) ;

  ufStoreAdjWaveLength( adjWaveLength ) ;

  return roundedStep ;
}

void ufStoreAdjWaveLength( double storeMe )
{
  const char* dbName = 0 ;
  char recordName[ 40 ] ;
  char storeMeString[ 40 ] ;

  memset( recordName, 0, sizeof( recordName ) ) ;
  memset( storeMeString, 0, sizeof( storeMeString ) ) ;

  /* dbName = ufdbName() ; */
  dbName = getenv ("UFEPICSDBNAME");
  if( 0 == dbName )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbName() returned NULL" ) ;
      ufLog( _UFerrmsg ) ;

      return ;
    }

  strcpy( recordName, dbName ) ;
  strcat( recordName, ":sad:adjWaveLength" ) ;

  sprintf( storeMeString, "%f", storeMe ) ;

  if( !ufdbStrPut( recordName, storeMeString ) )
    {
      sprintf( _UFerrmsg, __HERE__ "> ufdbStrPut() failed" ) ;
      ufLog( _UFerrmsg ) ;
    }
}

/**
 * recName must be of the form "miri:sad:adjWaveLength"
 */
bool ufdbStrPut( char* recName, char* val )
{
  struct dbAddr recordAddress ;

  if( (0 == recName) || (0 == val) || (0 == *recName) || (0 == *val) )
    {
      sprintf( _UFerrmsg, __HERE__ "> Invalid argument" ) ;
      ufLog( _UFerrmsg ) ;

      return false ;
    }

  if( dbNameToAddr( recName, &recordAddress ) )
    {
      sprintf( _UFerrmsg, __HERE__ "> dbNameToAddr() failed" ) ;
      ufLog( _UFerrmsg ) ;

      return false ;
    }

  if( dbPut( &recordAddress, DBR_STRING, val, 1 ) )
    {
      sprintf( _UFerrmsg, __HERE__ "> dbPut() failed" ) ;
      ufLog( _UFerrmsg ) ;

      return false ;
    }

  /* Successful */
  return true ;
}

char *
ufdbName ()
{
  DBENTRY dbentry;
  DBENTRY *pdbentry = &dbentry;
  long status = 0;
  char *cln = ":", *recnam = 0;
  sprintf( _UFerrmsg, __HERE__ ">BEGIN") ;
  ufLog( _UFerrmsg ) ;

  sprintf( _UFerrmsg, __HERE__ "> _dbname=%s ",_dbname) ;
  ufLog( _UFerrmsg ) ;

  if (strcmp (_dbname, "flam or miri") != 0)
    return _dbname;

  /* init it */
  dbInitEntry (pdbBase, pdbentry);
  sprintf( _UFerrmsg, __HERE__ ">after dbInitEntry ") ;
  ufLog( _UFerrmsg ) ;
  status = dbFirstRecordType (pdbentry);
  sprintf( _UFerrmsg, __HERE__ ">after dbFirstRecdes ") ;
  ufLog( _UFerrmsg ) ;
  status = dbFirstRecord (pdbentry);
  sprintf( _UFerrmsg, __HERE__ ">after dbFirstRecord ") ;
  ufLog( _UFerrmsg ) ;
  recnam = dbGetRecordName (pdbentry);
  sprintf( _UFerrmsg, __HERE__ ">after dbGetRecordName ") ;
  ufLog( _UFerrmsg ) ;
  dbFinishEntry (pdbentry);
  sprintf( _UFerrmsg, __HERE__ ">after dbFinishEntry ") ;
  ufLog( _UFerrmsg ) ;
  if (recnam == 0)
    return 0;
  sprintf( _UFerrmsg, __HERE__ ">after if (recnam == 0) ") ;
  ufLog( _UFerrmsg ) ;

  cln = index (recnam, ':');
  sprintf( _UFerrmsg, __HERE__ ">after index (recnam cln=%s ",cln) ;
  ufLog( _UFerrmsg ) ;
  if (cln == 0)
    return 0;

  memset (_dbname, 0, strlen (_dbname));
  sprintf( _UFerrmsg, __HERE__ ">after memset (_dbname, ") ;
  ufLog( _UFerrmsg ) ;
  strncpy (_dbname, recnam, (cln - recnam));
  sprintf( _UFerrmsg, __HERE__ ">after strncpy (_dbname=%s ",_dbname) ;
  ufLog( _UFerrmsg ) ;
  return _dbname;
}
  
long
ufdbl (char *precdesname)
{
  DBENTRY dbentry;
  DBENTRY *pdbentry = &dbentry;
  long status;
  long dbcnt = 0;

  dbInitEntry (pdbBase, pdbentry);
  if (!precdesname)
    status = dbFirstRecordType (pdbentry);
  else
    status = dbFindRecordType (pdbentry, precdesname);
  if (status)
    printf ("No record description\n"); 
  while (!status)
    {
      status = dbFirstRecord (pdbentry);
      while (!status)
	{
	  if (precdesname != 0)
	    printf ("%s\n", dbGetRecordName (pdbentry));
	  else
	    ++dbcnt;
	  status = dbNextRecord (pdbentry);
	}
      if (precdesname)
	break;
      status = dbNextRecordType (pdbentry);
    }
  dbFinishEntry (pdbentry);
  return (dbcnt);
}

long
ufHeartbeat ()
{
  DBENTRY dbentry;
  DBENTRY *pdbentry = &dbentry;
  long status;
  char buffer[MAX_STRING_SIZE];
  struct dbAddr heartbeat_address;
  long heartbeat_val;
  long myoptions = 0;
  long num_req = 1;


  dbInitEntry (pdbBase, pdbentry);
  status = dbFindRecordType (pdbentry, "longout"); 
  while (!status)
    {
      status = dbFirstRecord (pdbentry);
      while (!status)
	{
	  if (strstr (dbGetRecordName (pdbentry), "heartbeat") != 0)
	    {
	      /* printf("%s\n", dbGetRecordName(pdbentry)); 
	       */
	      strcpy (buffer, dbGetRecordName (pdbentry));
	      /* strcat (buffer, ".VAL"); */
	      status = dbNameToAddr (buffer, &heartbeat_address);
	      if (status)
		{
		  return -1;
		}
	      status =
		dbGetField (&heartbeat_address, DBR_LONG, &heartbeat_val,
			    &myoptions, &num_req, NULL);
	      printf ("Record %s value is: %ld\n", dbGetRecordName (pdbentry),
		      heartbeat_val);
	    }
	  status = dbNextRecord (pdbentry);
	}
      status = dbNextRecordType (pdbentry);
    }
  dbFinishEntry (pdbentry);
  return OK;
}

long
ufuptime ()
{
  long tick_val;
  long days, hours, minutes, seconds;
  char shours[3], sminutes[3], sseconds[3];
  tick_val = tickGet () / 60;
  days = tick_val / (24 * 60 * 60);
  tick_val = tick_val - days * 24 * 60 * 60;
  hours = tick_val / (60 * 60);
  tick_val = tick_val - hours * 60 * 60;
  minutes = tick_val / (60);
  tick_val = tick_val - minutes * 60;
  seconds = tick_val;
  if (seconds < 10)
    sprintf (sseconds, "0%ld", seconds);
  else
    sprintf (sseconds, "%ld", seconds);
  if (minutes < 10)
    sprintf (sminutes, "0%ld", minutes);
  else
    sprintf (sminutes, "%ld", minutes);
  if (hours < 10)
    sprintf (shours, "0%ld", hours);
  else
    sprintf (shours, "%ld", hours);
  printf ("\nIOC Uptime is %ld day(s), and %s:%s:%s\n", days, shours,
	  sminutes, sseconds);
  return OK;
}

/*
long ufmemShow () {
  int status ;
  PART_ID ufPart_Id ;
  MEM_PART_STATS ufppartStats ;

  memShowInit() ;
  printf("memShow(0)\n") ;
  memShow(0) ;

  ufPart_Id = memPartCreate (RF_MEM_BASE, RF_MEM_SIZE)); 
  status = memPartInfoGet (ufPart_Id, &ufppartStats);
  printf("The status is %d\n",status) ;
  printf("Number of Free bytes is %ld\n",ufppartStats.numBytesFree) ;
  
    typedef struct
    {
    unsigned long numBytesFree,     Number of Free Bytes in Partition      
    numBlocksFree,    Number of Free Blocks in Partition      
    maxBlockSizeFree,  Maximum block size that is free.       
    numBytesAlloc,    Number of Allocated Bytes in Partition 
                numBlocksAlloc;  Number of Allocated Blocks in Partition 

        }  MEM_PART_STATS; 

  return status;

}
*/
long
ufAlive ()
{
  printf ("%ld Records loaded.\n", ufdbl (NULL));
  ufHeartbeat ();
  ufuptime ();
  return OK;
}

long
ufdbStrGet (char *name, char *str)
{
  long nel = 1, stat = 0, options=0, nmlen = strlen(name);
  char* field = rindex(name, '.');
  char tmpnam[40];

  sprintf( _UFerrmsg, __HERE__ "> BEGIN");
  ufLog( _UFerrmsg ) ;
  strncpy(tmpnam, name, sizeof(tmpnam)-1);

  sprintf( _UFerrmsg, __HERE__ "> after strncpy. field = %s",field);
  ufLog( _UFerrmsg ) ;

  if( field == 0 ) {
    memset(tmpnam, 0, sizeof(tmpnam));
    sprintf( _UFerrmsg, __HERE__ "> after memset field == 0");
    ufLog( _UFerrmsg ) ;

    strcat(tmpnam, name); strcat(tmpnam, ".VAL");
    sprintf( _UFerrmsg, __HERE__ "> after strcat field == 0");
    ufLog( _UFerrmsg ) ;
  }
  else if( (int)(field - name) + 1 >= nmlen ) { /* nothing follows the "."? */
    memset(tmpnam, 0, sizeof(tmpnam));
    sprintf( _UFerrmsg, __HERE__ "> after memset field != 0");
    ufLog( _UFerrmsg ) ;

    strcat(tmpnam, name); strcat(tmpnam, ".VAL");
    sprintf( _UFerrmsg, __HERE__ "> after strcat field != 0");
    ufLog( _UFerrmsg ) ;
  }
  sprintf( _UFerrmsg, __HERE__ "> tmpnam = %s",tmpnam);
  ufLog( _UFerrmsg ) ;

  stat = dbNameToAddr (tmpnam, &_iAddr);

  sprintf( _UFerrmsg, __HERE__ "> after dbNameToAddr");
  ufLog( _UFerrmsg ) ;
  
  stat = dbGetField (&_iAddr, DBR_STRING, str, &options, &nel, NULL);

  sprintf( _UFerrmsg, __HERE__ "> stat=%ld, got val: %s from %s", stat, str, tmpnam);
  ufLog( _UFerrmsg ) ;

  return stat;
}

const char *
ufAgentHostIP ()
{
  char sirdbnam[40], sirdbval[40];
/*  char *dbnam = ufdbName (); */
  char *dbnam = getenv ("UFEPICSDBNAME");
  memset (sirdbnam, 0, sizeof (sirdbnam));
  strcat (sirdbnam, dbnam);
  strcat (sirdbnam, ":sad:agenthostIP");
  memset (sirdbval, 0, sizeof (sirdbval));
  ufdbStrGet (sirdbnam, sirdbval);
  strncpy (_agenthostIP, sirdbval, strlen (sirdbval));
  return _agenthostIP;
}

long
ufdbStrTransfer (char *input, char *output)
{
  long nel = 1, stat = 0;
  char str[40];
  struct dbAddr iAddr, oAddr;

  dbNameToAddr (input, &iAddr);
  dbNameToAddr (output, &oAddr);

  stat = dbGetField (&iAddr, DBR_STRING, str, 0, &nel, 0);
  if (_verbose > 0)
    fprintf (stderr, "ufdbStrTransfer> got val: %s from %s\n", str, input);

  /* this will attempt to lock the record, but it is
     already locked...(presum. by write_so) stat =
     dbPutField(oAddr, DBR_STRING, str, &nel ); */

  /* so just do a dbPut: */
  stat = dbPut (&oAddr, DBR_STRING, str, (int) nel);
  if (_verbose > 0)
    fprintf (stderr, "ufdbStrTransfer> put val: %s to %s\n", str, output);
  return stat;
}

bool
debugPGS (const char *funcName, const genSubRecord * pgs)
{
  if (NULL == pgs)
    {
      return false;
    }

  sprintf (_UFerrmsg, "%s> pgs->name: %s", funcName, pgs->name);
  ufLog (_UFerrmsg);
  
  return true;
}

bool
debugCAD( const char* funcName, const cadRecord* pcr )
{
if( 0 == pcr )
	{
	return false ;
	}

sprintf( _UFerrmsg, "%s> pcr->name: %s\n", funcName, pcr->name ) ;
ufLog( _UFerrmsg ) ;

return true ;
}

bool
status_changed (const char *current, int last)
{
  bool changed = false ;
  switch (last)
    {
    case CAR_IDLE:
      if (strcmp (current, "IDLE") != 0)
        changed = true;
      break;
    case CAR_BUSY:
      if (strcmp (current, "BUSY") != 0)
        changed = true;
      break;
    case CAR_ERROR:
      if (strcmp (current, "ERR") != 0)
        changed = true;
      break;
    }
  return changed;
}

static void dumpSingle( const char* funcName,
			const void* fieldPtr,
			const char whichField,
			const unsigned short int type,
			const char* prefix )
{
  switch( type )
    {
    case DBF_STRING:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: string, value: %s",
	       funcName, prefix, whichField, (const char*) fieldPtr ) ;
      break ;
    case DBF_CHAR:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: char, value: %c",
	       funcName, prefix, whichField, *((const char*) fieldPtr) ) ;
      break ;
    case DBF_UCHAR:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: uchar, value: %c",
	       funcName, prefix, whichField, *((const unsigned char*) fieldPtr) ) ;
      break ;
    case DBF_SHORT:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: short, value: %d",
	       funcName, prefix, whichField, *((const short*) fieldPtr) ) ;
      break ;
    case DBF_USHORT:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: ushort, value: %d",
	       funcName, prefix, whichField, *((const unsigned short int*) fieldPtr) ) ;
      break ;
    case DBF_LONG:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: long, value: %ld",
	       funcName, prefix, whichField, *((const long*) fieldPtr) ) ;
      break ;
    case DBF_ULONG:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: ulong, value: %ld",
	       funcName, prefix, whichField, *((const unsigned long int*) fieldPtr) ) ;
      break ;
    case DBF_FLOAT:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: float, value: %f",
	       funcName, prefix, whichField, *((const float*) fieldPtr) ) ;
      break ;
    case DBF_DOUBLE:
      sprintf( _UFerrmsg, "%s> Field %s%c, type: double, value: %f",
	       funcName, prefix, whichField, *((const double*) fieldPtr) ) ;
      break ;
    default:
      sprintf( _UFerrmsg, "%s> Unhandled type: %d",
	       funcName, type ) ;
    } /* switch() */

  ufLog( _UFerrmsg ) ;
}

void dumpPGS( const char* funcName, const genSubRecord* pgs, bool useVal )
{
  /* Yes, this is ugly, I know */
  /* If useVal is true, then print pgs->val[a-z], otherwise pgs->[a-z] */
  if( useVal )
    {
      dumpSingle( funcName, pgs->vala, 'a', pgs->ftva, "val" ) ;
      dumpSingle( funcName, pgs->valb, 'b', pgs->ftvb, "val" ) ;
      dumpSingle( funcName, pgs->valc, 'c', pgs->ftvc, "val" ) ;
      dumpSingle( funcName, pgs->vald, 'd', pgs->ftvd, "val" ) ;
      dumpSingle( funcName, pgs->vale, 'e', pgs->ftve, "val" ) ;
      dumpSingle( funcName, pgs->valf, 'f', pgs->ftvf, "val" ) ;
      dumpSingle( funcName, pgs->valg, 'g', pgs->ftvg, "val" ) ;
      dumpSingle( funcName, pgs->valh, 'h', pgs->ftvh, "val" ) ;
      dumpSingle( funcName, pgs->vali, 'i', pgs->ftvi, "val" ) ;
      dumpSingle( funcName, pgs->valj, 'j', pgs->ftvj, "val" ) ;
      dumpSingle( funcName, pgs->valk, 'k', pgs->ftvk, "val" ) ;
      dumpSingle( funcName, pgs->vall, 'l', pgs->ftvl, "val" ) ;
      dumpSingle( funcName, pgs->valm, 'm', pgs->ftvm, "val" ) ;
      dumpSingle( funcName, pgs->valn, 'n', pgs->ftvn, "val" ) ;
      dumpSingle( funcName, pgs->valo, 'o', pgs->ftvo, "val" ) ;
      dumpSingle( funcName, pgs->valp, 'p', pgs->ftvp, "val" ) ;
      dumpSingle( funcName, pgs->valq, 'q', pgs->ftvq, "val" ) ;
      dumpSingle( funcName, pgs->valr, 'r', pgs->ftvr, "val" ) ;
      dumpSingle( funcName, pgs->vals, 's', pgs->ftvs, "val" ) ;
      dumpSingle( funcName, pgs->valt, 't', pgs->ftvt, "val" ) ;
      dumpSingle( funcName, pgs->valu, 'u', pgs->ftvu, "val" ) ;
    }
  else
    {
      dumpSingle( funcName, pgs->a, 'a', pgs->fta, "" ) ;
      dumpSingle( funcName, pgs->b, 'b', pgs->ftb, "" ) ;
      dumpSingle( funcName, pgs->c, 'c', pgs->ftc, "" ) ;
      dumpSingle( funcName, pgs->d, 'd', pgs->ftd, "" ) ;
      dumpSingle( funcName, pgs->e, 'e', pgs->fte, "" ) ;
      dumpSingle( funcName, pgs->f, 'f', pgs->ftf, "" ) ;
      dumpSingle( funcName, pgs->g, 'g', pgs->ftg, "" ) ;
      dumpSingle( funcName, pgs->h, 'h', pgs->fth, "" ) ;
      dumpSingle( funcName, pgs->i, 'i', pgs->fti, "" ) ;
      dumpSingle( funcName, pgs->j, 'j', pgs->ftj, "" ) ;
      dumpSingle( funcName, pgs->k, 'k', pgs->ftk, "" ) ;
      dumpSingle( funcName, pgs->l, 'l', pgs->ftl, "" ) ;
      dumpSingle( funcName, pgs->m, 'm', pgs->ftm, "" ) ;
      dumpSingle( funcName, pgs->n, 'n', pgs->ftn, "" ) ;
      dumpSingle( funcName, pgs->o, 'o', pgs->fto, "" ) ;
      dumpSingle( funcName, pgs->p, 'p', pgs->ftp, "" ) ;
      dumpSingle( funcName, pgs->q, 'q', pgs->ftq, "" ) ;
      dumpSingle( funcName, pgs->r, 'r', pgs->ftr, "" ) ;
      dumpSingle( funcName, pgs->s, 's', pgs->fts, "" ) ;
      dumpSingle( funcName, pgs->t, 't', pgs->ftt, "" ) ;
      dumpSingle( funcName, pgs->u, 'u', pgs->ftu, "" ) ;
    }
}

void dumpPCR( const char* funcName, const cadRecord* pcr, bool useVal )
{
  /* For cad records, the boolean value is not used, since all inputs
   * are char[] */
  /* Copy paste! */
  dumpSingle( funcName, pcr->vala, 'a', pcr->ftva, "val" ) ;
  dumpSingle( funcName, pcr->valb, 'b', pcr->ftvb, "val" ) ;
  dumpSingle( funcName, pcr->valc, 'c', pcr->ftvc, "val" ) ;
  dumpSingle( funcName, pcr->vald, 'd', pcr->ftvd, "val" ) ;
  dumpSingle( funcName, pcr->vale, 'e', pcr->ftve, "val" ) ;
  dumpSingle( funcName, pcr->valf, 'f', pcr->ftvf, "val" ) ;
  dumpSingle( funcName, pcr->valg, 'g', pcr->ftvg, "val" ) ;
  dumpSingle( funcName, pcr->valh, 'h', pcr->ftvh, "val" ) ;
  dumpSingle( funcName, pcr->vali, 'i', pcr->ftvi, "val" ) ;
  dumpSingle( funcName, pcr->valj, 'j', pcr->ftvj, "val" ) ;
  dumpSingle( funcName, pcr->valk, 'k', pcr->ftvk, "val" ) ;
  dumpSingle( funcName, pcr->vall, 'l', pcr->ftvl, "val" ) ;
  dumpSingle( funcName, pcr->valm, 'm', pcr->ftvm, "val" ) ;
  dumpSingle( funcName, pcr->valn, 'n', pcr->ftvn, "val" ) ;
  dumpSingle( funcName, pcr->valo, 'o', pcr->ftvo, "val" ) ;
  dumpSingle( funcName, pcr->valp, 'p', pcr->ftvp, "val" ) ;
  dumpSingle( funcName, pcr->valq, 'q', pcr->ftvq, "val" ) ;
  dumpSingle( funcName, pcr->valr, 'r', pcr->ftvr, "val" ) ;
  dumpSingle( funcName, pcr->vals, 's', pcr->ftvs, "val" ) ;
  dumpSingle( funcName, pcr->valt, 't', pcr->ftvt, "val" ) ;
}

#endif /* __UFDBL_C__ */
