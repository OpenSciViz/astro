[schematic2]
uniq 971
[tools]
[detail]
w -252 4411 100 0 n#969 ebis.ebis#968.VAL 64 3840 96 3840 96 3936 -256 3936 -256 4896 224 4896 egenSubB.egenSubB#847.INPB
w -476 3899 100 0 n#965 elongouts.elongouts#963.OUT -576 3520 -480 3520 -480 4288 224 4288 egenSubB.egenSubB#847.U
w -430 3691 100 0 n#949 elongouts.elongouts#963.FLNK -576 3584 -512 3584 -512 3680 -288 3680 junction
w -292 4523 100 0 n#949 elongouts.elongouts#914.FLNK -576 5376 -288 5376 -288 3680 -224 3680 flamLinkEnable.flamLinkEnable#946.ENABLE
w -462 5155 100 0 n#949 elongouts.elongouts#915.FLNK -576 5152 -288 5152 junction
w -462 4931 100 0 n#949 elongouts.elongouts#926.FLNK -576 4928 -288 4928 junction
w -462 4451 100 0 n#949 elongouts.elongouts#916.FLNK -576 4448 -288 4448 junction
w -462 4211 100 0 n#949 estringouts.estringouts#918.FLNK -576 4208 -288 4208 junction
w -462 4019 100 0 n#949 estringouts.estringouts#919.FLNK -576 4016 -288 4016 junction
w -462 3811 100 0 n#949 eaos.eaos#917.FLNK -576 3808 -288 3808 junction
w -1486 3363 100 0 n#967 ecad2.ecad2#957.STLK -2880 2784 -2816 2784 -2816 3424 -1952 3424 -1952 3360 -960 3360 -960 3552 -832 3552 elongouts.elongouts#963.SLNK
w -2414 3459 100 0 n#966 ecad2.ecad2#957.VALA -2880 3072 -2848 3072 -2848 3456 -1920 3456 -1920 3392 -992 3392 -992 3584 -832 3584 elongouts.elongouts#963.DOL
w -3332 3195 100 0 n#961 ecad2.ecad2#957.VAL -2880 3264 -2752 3264 -2752 2624 -3328 2624 -3328 3776 -3200 3776 eapply.$(top)$(dev)motorApply.INPH
w -3300 3195 100 0 n#960 ecad2.ecad2#957.MESS -2880 3232 -2784 3232 -2784 2656 -3296 2656 -3296 3744 -3200 3744 eapply.$(top)$(dev)motorApply.INMH
w -3022 3587 100 0 n#959 eapply.$(top)$(dev)motorApply.OUTH -2816 3776 -2752 3776 -2752 3584 -3232 3584 -3232 3264 -3200 3264 ecad2.ecad2#957.DIR
w -3054 3619 100 0 n#958 eapply.$(top)$(dev)motorApply.OCLH -2816 3744 -2784 3744 -2784 3616 -3264 3616 -3264 3232 -3200 3232 ecad2.ecad2#957.ICID
w 250 4019 100 0 n#950 estringouts.$(top)$(dev)debug.OUT 448 3824 528 3824 528 4016 32 4016 32 4224 -32 4224 -32 4544 224 4544 egenSubB.egenSubB#847.M
w 124 3931 100 0 n#948 flamLinkEnable.flamLinkEnable#946.FLINK 96 3648 128 3648 128 4224 224 4224 egenSubB.egenSubB#847.SLNK
w -2270 2403 100 0 n#947 eapply.$(top)$(dev)motorApply.FLNK -2816 4352 -2784 4352 -2784 4416 -3552 4416 -3552 2400 -928 2400 -928 3264 -320 3264 -320 3616 -224 3616 flamLinkEnable.flamLinkEnable#946.SLINK
w -142 4739 100 0 n#956 eaos.eaos#927.OUT -576 4608 -448 4608 -448 4736 224 4736 egenSubB.egenSubB#847.G
w -324 4235 100 0 n#956 eaos.eaos#917.OUT -576 3744 -320 3744 -320 4736 junction
w -356 4291 100 0 n#953 estringouts.estringouts#919.OUT -576 3984 -352 3984 -352 4608 224 4608 egenSubB.egenSubB#847.K
w -110 4675 100 0 n#952 estringouts.estringouts#918.OUT -576 4176 -384 4176 -384 4672 224 4672 egenSubB.egenSubB#847.I
w -366 4867 100 0 n#951 elongouts.elongouts#926.OUT -576 4864 -96 4864 -96 4800 224 4800 egenSubB.egenSubB#847.E
w -420 4619 100 0 n#951 elongouts.elongouts#916.OUT -576 4384 -416 4384 -416 4864 junction
w -350 5091 100 0 n#941 elongouts.elongouts#915.OUT -576 5088 -64 5088 -64 4864 224 4864 egenSubB.egenSubB#847.C
w -334 5315 100 0 n#940 elongouts.elongouts#914.OUT -576 5312 -32 5312 -32 4928 224 4928 egenSubB.egenSubB#847.A
w -1028 3259 100 0 n#939 ecad2.$(top)$(dev)steps.STLK -1120 2752 -1024 2752 -1024 3776 -832 3776 eaos.eaos#917.SLNK
w -1060 3419 100 0 n#938 ecad2.$(top)$(dev)steps.VALA -1120 3040 -1056 3040 -1056 3808 -832 3808 eaos.eaos#917.DOL
w -1604 3083 100 0 n#937 ecad2.$(top)$(dev)abort.STLK -1696 2752 -1600 2752 -1600 3424 -1088 3424 -1088 4000 -832 4000 estringouts.estringouts#919.SLNK
w -1124 3739 100 0 n#936 ecad2.$(top)$(dev)abort.VALA -1696 3040 -1632 3040 -1632 3456 -1120 3456 -1120 4032 -832 4032 estringouts.estringouts#919.DOL
w -1710 3491 100 0 n#935 ecad2.$(top)$(dev)stop.STLK -2304 2752 -2208 2752 -2208 3488 -1152 3488 -1152 4192 -832 4192 estringouts.estringouts#918.SLNK
w -1742 3523 100 0 n#934 ecad2.$(top)$(dev)stop.VALA -2304 3040 -2240 3040 -2240 3520 -1184 3520 -1184 4224 -832 4224 estringouts.estringouts#918.DOL
w -1956 4091 100 0 n#933 ecad4.ecad4#842.STLK -2080 3776 -1952 3776 -1952 4416 -1760 4416 -1760 4480 -1280 4480 -1280 4640 -832 4640 eaos.eaos#927.SLNK
w -766 4739 100 0 n#932 eaos.eaos#927.FLNK -576 4672 -544 4672 -544 4736 -928 4736 -928 4896 -832 4896 elongouts.elongouts#926.SLNK
w -1678 4515 100 0 n#931 ecad4.ecad4#842.VALB -2080 4128 -1984 4128 -1984 4512 -1312 4512 -1312 4672 -832 4672 eaos.eaos#927.DOL
w -1710 4547 100 0 n#930 ecad4.ecad4#842.VALA -2080 4192 -2016 4192 -2016 4544 -1344 4544 -1344 4928 -832 4928 elongouts.elongouts#926.DOL
w -1220 4091 100 0 n#928 ecad2.$(top)$(dev)datum.STLK -1440 3776 -1216 3776 -1216 4416 -832 4416 elongouts.elongouts#916.SLNK
w -1070 4451 100 0 n#929 ecad2.$(top)$(dev)datum.VALA -1440 4064 -1248 4064 -1248 4448 -832 4448 elongouts.elongouts#916.DOL
w -1134 5123 100 0 n#923 ecad2.ecad2#837.STLK -1664 4672 -1376 4672 -1376 5120 -832 5120 elongouts.elongouts#915.SLNK
w -1150 5155 100 0 n#922 ecad2.ecad2#837.VALA -1664 4960 -1408 4960 -1408 5152 -832 5152 elongouts.elongouts#915.DOL
w -1630 5475 100 0 n#921 ecad2.$(top)$(dev)test.STLK -2272 4672 -2144 4672 -2144 5472 -1056 5472 -1056 5344 -832 5344 elongouts.elongouts#914.SLNK
w -1630 5507 100 0 n#920 ecad2.$(top)$(dev)test.VALA -2272 4960 -2176 4960 -2176 5504 -1024 5504 -1024 5376 -832 5376 elongouts.elongouts#914.DOL
w -2270 2435 100 0 n#913 ecad2.$(top)$(dev)steps.VAL -1120 3232 -960 3232 -960 2432 -3520 2432 -3520 3968 -3200 3968 eapply.$(top)$(dev)motorApply.INPE
w -2270 2467 100 0 n#912 ecad2.$(top)$(dev)steps.MESS -1120 3200 -992 3200 -992 2464 -3488 2464 -3488 3936 -3200 3936 eapply.$(top)$(dev)motorApply.INME
w -2526 2499 100 0 n#911 ecad2.$(top)$(dev)abort.VAL -1696 3232 -1536 3232 -1536 2496 -3456 2496 -3456 3904 -3200 3904 eapply.$(top)$(dev)motorApply.INPF
w -2526 2531 100 0 n#910 ecad2.$(top)$(dev)abort.MESS -1696 3200 -1568 3200 -1568 2528 -3424 2528 -3424 3872 -3200 3872 eapply.$(top)$(dev)motorApply.INMF
w -2782 2563 100 0 n#909 ecad2.$(top)$(dev)stop.VAL -2304 3232 -2112 3232 -2112 2560 -3392 2560 -3392 3840 -3200 3840 eapply.$(top)$(dev)motorApply.INPG
w -2782 2595 100 0 n#908 ecad2.$(top)$(dev)stop.MESS -2304 3200 -2144 3200 -2144 2592 -3360 2592 -3360 3808 -3200 3808 eapply.$(top)$(dev)motorApply.INMG
w -2556 3675 100 0 n#907 eapply.$(top)$(dev)motorApply.OUTE -2816 3968 -2560 3968 -2560 3392 -1984 3392 -1984 3328 -1472 3328 -1472 3232 -1440 3232 ecad2.$(top)$(dev)steps.DIR
w -2588 3643 100 0 n#906 eapply.$(top)$(dev)motorApply.OCLE -2816 3936 -2592 3936 -2592 3360 -2016 3360 -2016 3296 -1504 3296 -1504 3200 -1440 3200 ecad2.$(top)$(dev)steps.ICID
w -2620 3611 100 0 n#905 eapply.$(top)$(dev)motorApply.OUTF -2816 3904 -2624 3904 -2624 3328 -2048 3328 -2048 3232 -2016 3232 ecad2.$(top)$(dev)abort.DIR
w -2652 3579 100 0 n#904 eapply.$(top)$(dev)motorApply.OCLF -2816 3872 -2656 3872 -2656 3296 -2080 3296 -2080 3200 -2016 3200 ecad2.$(top)$(dev)abort.ICID
w -2684 3531 100 0 n#903 eapply.$(top)$(dev)motorApply.OUTG -2816 3840 -2688 3840 -2688 3232 -2624 3232 ecad2.$(top)$(dev)stop.DIR
w -2716 3499 100 0 n#902 eapply.$(top)$(dev)motorApply.OCLG -2816 3808 -2720 3808 -2720 3200 -2624 3200 ecad2.$(top)$(dev)stop.ICID
w -2510 5443 100 0 n#868 ecad4.ecad4#842.MESS -2080 4352 -1920 4352 -1920 3616 -1312 3616 -1312 4416 -1440 4416 -1440 5440 -3520 5440 -3520 4000 -3200 4000 eapply.$(top)$(dev)motorApply.INMD
w -2510 5411 100 0 n#867 ecad4.ecad4#842.VAL -2080 4384 -1888 4384 -1888 3648 -1344 3648 -1344 4384 -1472 4384 -1472 5408 -3488 5408 -3488 4032 -3200 4032 eapply.$(top)$(dev)motorApply.INPD
w -2510 5379 100 0 n#866 ecad2.$(top)$(dev)datum.MESS -1440 4224 -1376 4224 -1376 4352 -1504 4352 -1504 5376 -3456 5376 -3456 4064 -3200 4064 eapply.$(top)$(dev)motorApply.INMC
w -2510 5347 100 0 n#865 ecad2.$(top)$(dev)datum.VAL -1440 4256 -1408 4256 -1408 4320 -1536 4320 -1536 5344 -3424 5344 -3424 4096 -3200 4096 eapply.$(top)$(dev)motorApply.INPC
w -2500 4171 100 0 n#862 eapply.$(top)$(dev)motorApply.OCLD -2816 4000 -2496 4000 -2496 4352 -2400 4352 ecad4.ecad4#842.ICID
w -2532 4203 100 0 n#861 eapply.$(top)$(dev)motorApply.OUTD -2816 4032 -2528 4032 -2528 4384 -2400 4384 ecad4.ecad4#842.DIR
w -2238 4451 100 0 n#864 eapply.$(top)$(dev)motorApply.OCLC -2816 4064 -2560 4064 -2560 4448 -1856 4448 -1856 4224 -1760 4224 ecad2.$(top)$(dev)datum.ICID
w -2238 4483 100 0 n#863 eapply.$(top)$(dev)motorApply.OUTC -2816 4096 -2592 4096 -2592 4480 -1824 4480 -1824 4256 -1760 4256 ecad2.$(top)$(dev)datum.DIR
w -2510 5315 100 0 n#858 ecad2.ecad2#837.MESS -1664 5120 -1568 5120 -1568 5312 -3392 5312 -3392 4128 -3200 4128 eapply.$(top)$(dev)motorApply.INMB
w -2510 5283 100 0 n#857 ecad2.ecad2#837.VAL -1664 5152 -1600 5152 -1600 5280 -3360 5280 -3360 4160 -3200 4160 eapply.$(top)$(dev)motorApply.INPB
w -2084 4811 100 0 n#856 eapply.$(top)$(dev)motorApply.OCLB -2816 4128 -2624 4128 -2624 4512 -2080 4512 -2080 5120 -1984 5120 ecad2.ecad2#837.ICID
w -2116 4843 100 0 n#855 eapply.$(top)$(dev)motorApply.OUTB -2816 4160 -2656 4160 -2656 4544 -2112 4544 -2112 5152 -1984 5152 ecad2.ecad2#837.DIR
w -2798 5259 100 0 n#854 ecad2.$(top)$(dev)test.MESS -2272 5120 -2208 5120 -2208 5248 -3328 5248 -3328 4192 -3200 4192 eapply.$(top)$(dev)motorApply.INMA
w -2798 5227 100 0 n#853 ecad2.$(top)$(dev)test.VAL -2272 5152 -2240 5152 -2240 5216 -3296 5216 -3296 4224 -3200 4224 eapply.$(top)$(dev)motorApply.INPA
w -2692 4651 100 0 n#852 eapply.$(top)$(dev)motorApply.OCLA -2816 4192 -2688 4192 -2688 5120 -2592 5120 ecad2.$(top)$(dev)test.ICID
w -2724 4683 100 0 n#851 eapply.$(top)$(dev)motorApply.OUTA -2816 4224 -2720 4224 -2720 5152 -2592 5152 ecad2.$(top)$(dev)test.DIR
w -1988 3787 100 0 n#849 ecad4.ecad4#842.OUTC -2080 4032 -1984 4032 -1984 3552 -1824 3552 hwout.hwout#843.outp
w 626 4867 100 0 n#846 egenSubB.egenSubB#847.OUTC 512 4864 800 4864 hwout.hwout#845.outp
w 658 5059 100 0 n#824 ecars.$(top)$(dev)motorC.FLNK 608 5056 768 5056 hwout.hwout#822.outp
s 7 4555 100 0 debug mode
s 578 4874 100 0 current position
s 578 4844 100 0 init_velocity
s 578 4813 100 0 slew_velocity
s 578 4779 100 0 acceleration
s 578 4746 100 0 deceleration
s 578 4714 100 0 drive_current
s 578 4681 100 0 datum_speed
s 578 4653 100 0 datum_direction
s 448 2736 250 0 motor22.sch 2/13/2002 1:22 PM EST
s 7 4940 100 0 perform_test
s 7 4506 100 0 init_velocity
s 7 4475 100 0 slew_velocity
s 7 4441 100 0 acceleration
s 7 4408 100 0 deceleration
s 7 4376 100 0 drive_current
s 7 4343 100 0 datum_speed
s 7 4315 100 0 datum_direction
s 7 4806 100 0 datum_motor
s 7 4749 100 0 num_steps
s 7 4685 100 0 stop_mess
s 7 4619 100 0 abort_mess
s 578 4908 100 0 socket number
s 7 4873 100 0 command mode
s 580 4938 100 0 command mode
s -1968 3568 120 0 Position number
s 580 4619 100 0 home switch status
[cell use]
use estringins 320 3591 100 0 estringins#970
xform 0 448 3664
p 432 3584 100 1024 -1 name:$(cc)$(dev)$(delim)log
use ebis -192 3783 100 0 ebis#968
xform 0 -64 3856
p -80 3776 100 1024 -1 name:$(cc)$(dev)$(delim)HmDisable
use elongouts -832 3463 100 0 elongouts#963
xform 0 -704 3552
p -896 3424 100 0 1 OMSL:closed_loop
p -896 3456 100 768 1 name:$(cc)$(dev)$(delim)orgnW
use elongouts -832 5255 100 0 elongouts#914
xform 0 -704 5344
p -899 5219 100 0 1 OMSL:closed_loop
p -899 5245 100 768 1 name:$(cc)$(dev)$(delim)testW
use elongouts -832 5031 100 0 elongouts#915
xform 0 -704 5120
p -899 4994 100 0 1 OMSL:closed_loop
p -899 5020 100 768 1 name:$(cc)$(dev)$(delim)initW
use elongouts -832 4327 100 0 elongouts#916
xform 0 -704 4416
p -899 4294 100 0 1 OMSL:closed_loop
p -899 4323 100 768 1 name:$(cc)$(dev)$(delim)datumW
use elongouts -832 4807 100 0 elongouts#926
xform 0 -704 4896
p -899 4772 100 0 1 OMSL:closed_loop
p -899 4801 100 768 1 name:$(cc)$(dev)$(delim)datumWPos
use ecad2 -3200 2695 100 0 ecad2#957
xform 0 -3040 3008
p -3104 3008 100 0 0 FTVA:LONG
p -3104 2720 100 0 0 SNAM:motorOriginCommand
p -3088 2688 100 1024 -1 name:$(cc)$(dev)$(delim)origin
use ecad2 -1984 4583 100 0 ecad2#837
xform 0 -1824 4896
p -1888 5056 100 0 0 DESC:Motor Init CAD
p -1888 4896 100 0 0 FTVA:LONG
p -1888 4608 100 0 0 SNAM:motorInitCommand
p -1872 4576 100 1024 -1 name:$(cc)$(dev)$(delim)init
use ecad2 -1440 2663 -100 0 $(top)$(dev)steps
xform 0 -1280 2976
p -1344 3136 100 0 0 DESC:Raw Steps CAD
p -1344 2976 100 0 0 FTVA:DOUBLE
p -1344 2688 100 0 0 SNAM:motorStepsCommand
p -1328 2656 100 1024 -1 name:$(cc)$(dev)$(delim)steps
use ecad2 -2624 2663 -100 0 $(top)$(dev)stop
xform 0 -2464 2976
p -2528 3136 100 0 0 DESC:Motor Stop CAD
p -2528 2688 100 0 0 SNAM:motorStopCommand
p -2512 2656 100 1024 -1 name:$(cc)$(dev)$(delim)stop
use ecad2 -2016 2663 -100 0 $(top)$(dev)abort
xform 0 -1856 2976
p -1920 3136 100 0 0 DESC:Motor Abort CAD
p -1920 2688 100 0 0 SNAM:motorAbortCommand
p -1904 2656 100 1024 -1 name:$(cc)$(dev)$(delim)abort
use ecad2 -1760 3687 -100 0 $(top)$(dev)datum
xform 0 -1600 4000
p -1664 4160 100 0 0 DESC:Motor Datum CAD
p -1664 4000 100 0 0 FTVA:LONG
p -1664 3712 100 0 0 SNAM:motorDatumCommand
p -1648 3680 100 1024 -1 name:$(cc)$(dev)$(delim)datum
use ecad2 -2592 4583 -100 0 $(top)$(dev)test
xform 0 -2432 4896
p -2496 5056 100 0 0 DESC:Motor Test CAD
p -2496 4896 100 0 0 FTVA:LONG
p -2496 4608 100 0 0 SNAM:motorTestCommand
p -2480 4576 100 1024 -1 name:$(cc)$(dev)$(delim)test
use flamLinkEnable -224 3527 100 0 flamLinkEnable#946
xform 0 -64 3648
p -224 3520 100 0 1 setSystem:system $(cc)$(dev)$(delim)
use eaos -832 3687 100 0 eaos#917
xform 0 -704 3776
p -899 3652 100 0 1 OMSL:closed_loop
p -899 3680 100 768 1 name:$(cc)$(dev)$(delim)stepsW
use eaos -832 4551 100 0 eaos#927
xform 0 -704 4640
p -899 4515 100 0 1 OMSL:closed_loop
p -899 4543 100 768 1 name:$(cc)$(dev)$(delim)stepsWPos
use estringouts 192 3767 -100 0 $(top)$(dev)debug
xform 0 320 3840
p 219 3880 100 0 0 DESC:Debug Mode (NONE,MIN,FULL)
p 304 3760 100 1024 -1 name:$(cc)$(dev)$(delim)debug
use estringouts -832 4119 100 0 estringouts#918
xform 0 -704 4192
p -899 4091 100 0 1 OMSL:closed_loop
p -899 4113 100 768 1 name:$(cc)$(dev)$(delim)stopW
use estringouts -832 3927 100 0 estringouts#919
xform 0 -704 4000
p -899 3899 100 0 1 OMSL:closed_loop
p -899 3920 100 768 1 name:$(cc)$(dev)$(delim)abortW
use egenSubB 224 4135 100 0 egenSubB#847
xform 0 368 4560
p 67 4875 100 0 0 DESC:Motor $(dev) Gensub
p 1 3909 100 0 0 FTA:LONG
p 1 3909 100 0 0 FTB:STRING
p 1 3877 100 0 0 FTC:LONG
p 1 3813 100 0 0 FTE:LONG
p 1 3685 100 0 0 FTI:STRING
p 1 3653 100 0 0 FTJ:STRING
p 1 3653 100 0 0 FTK:STRING
p 1 3653 100 0 0 FTM:STRING
p 1 3653 100 0 0 FTT:LONG
p 1 3653 100 0 0 FTU:LONG
p 1 3909 100 0 0 FTVA:LONG
p 1 3909 100 0 0 FTVB:LONG
p 1 3877 100 0 0 FTVC:STRING
p 1 3653 100 0 0 FTVJ:LONG
p 1 3653 100 0 0 FTVK:STRING
p -64 4542 100 0 0 INAM:ufmotorGinit
p -64 4510 100 0 0 SNAM:ufmotorGproc
p 336 4128 100 1024 -1 name:$(cc)$(dev)$(delim)motorG
p 512 4874 75 0 -1 pproc(OUTC):PP
use hwout 768 5015 100 0 hwout#822
xform 0 864 5056
p 971 5045 100 0 -1 val(outp):$(cc)MrgCarG.J PP NMS
use hwout -1824 3511 100 0 hwout#843
xform 0 -1728 3552
p -1623 3546 100 0 -1 val(outp):$(sad)$(dev)Pos.VAL PP NMS
use hwout 800 4823 100 0 hwout#845
xform 0 896 4864
p 1000 4855 100 0 -1 val(outp):$(sad)$(dev)RawPos.VAL PP NMS
use ecad4 -2400 3687 100 0 ecad4#842
xform 0 -2240 4064
p -2464 3648 100 0 0 DESC:Named Position CAD
p -2464 3488 100 0 0 FTVA:LONG
p -2464 3456 100 0 0 FTVB:DOUBLE
p -2464 3424 100 0 0 FTVC:STRING
p -2464 3136 100 0 0 SNAM:motorNamedPosCommand
p -2288 3680 100 1024 -1 name:$(cc)$(dev)$(delim)namedPos
p -2080 4032 75 768 -1 pproc(OUTC):PP
use ecars 288 4999 -100 0 $(top)$(dev)motorC
xform 0 448 5168
p 384 5216 100 0 0 DESC:Motor CAR Record
p 400 4992 100 1024 -1 name:$(cc)$(dev)$(delim)motorC
use bd200tr -3680 2248 -100 0 frame
xform 0 -1040 3952
use eapply -3200 3687 -100 0 $(top)$(dev)motorApply
xform 0 -3008 4048
p -3104 4032 100 0 0 DESC:Motor Low Level Apply
p -3088 3680 100 1024 -1 name:$(cc)$(dev)$(delim)motorApply
[comments]
RCS: "$Name:  $ $Id: flamMotor.sch,v 0.0 2005/09/01 20:21:37 drashkin Exp $"
