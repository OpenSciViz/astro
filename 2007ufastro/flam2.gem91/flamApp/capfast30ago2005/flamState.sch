[schematic2]
uniq 298
[tools]
[detail]
w 514 11 100 0 n#297 egenSubD.egenSubD#295.INPT 896 640 736 640 736 0 352 0 hwin.hwin#296.in
w 1394 483 100 0 n#287 egenSubD.egenSubD#295.VALJ 1184 672 1312 672 1312 480 1536 480 estringouts.estringouts#267.SDIS
w 1340 603 100 0 n#286 egenSubD.egenSubD#295.OUTI 1184 704 1344 704 1344 512 1536 512 estringouts.estringouts#267.SLNK
w 1250 803 100 0 n#285 egenSubD.egenSubD#295.VALH 1184 800 1376 800 1376 704 1536 704 estringouts.estringouts#263.SDIS
w 1266 835 100 0 n#284 egenSubD.egenSubD#295.OUTG 1184 832 1408 832 1408 736 1536 736 estringouts.estringouts#263.SLNK
w 1330 931 100 0 n#283 egenSubD.egenSubD#295.VALF 1184 928 1536 928 estringouts.estringouts#262.SDIS
w 1330 963 100 0 n#282 egenSubD.egenSubD#295.OUTE 1184 960 1536 960 estringouts.estringouts#262.SLNK
w 1266 1059 100 0 n#281 egenSubD.egenSubD#295.VALD 1184 1056 1408 1056 1408 1152 1536 1152 estringouts.estringouts#124.SDIS
w 1250 1091 100 0 n#280 egenSubD.egenSubD#295.OUTC 1184 1088 1376 1088 1376 1184 1536 1184 estringouts.estringouts#124.SLNK
w 1340 1275 100 0 n#279 egenSubD.egenSubD#295.VALB 1184 1184 1344 1184 1344 1376 1536 1376 estringouts.estringouts#238.SDIS
w 1394 1411 100 0 n#278 egenSubD.egenSubD#295.OUTA 1184 1216 1312 1216 1312 1408 1536 1408 estringouts.estringouts#238.SLNK
w 1810 499 100 0 n#266 estringouts.estringouts#267.OUT 1792 496 1888 496 hwout.hwout#268.outp
w 1810 723 100 0 n#261 estringouts.estringouts#263.OUT 1792 720 1888 720 hwout.hwout#264.outp
w 1810 947 100 0 n#260 estringouts.estringouts#262.OUT 1792 944 1888 944 hwout.hwout#265.outp
w 604 571 100 0 n#259 hwin.hwin#215.in 352 384 608 384 608 768 896 768 egenSubD.egenSubD#295.INPP
w 700 379 100 0 n#257 hwin.hwin#217.in 352 96 704 96 704 672 896 672 egenSubD.egenSubD#295.INPS
w 1810 1171 100 0 n#254 estringouts.estringouts#124.OUT 1792 1168 1888 1168 hwout.hwout#253.outp
w 1810 1395 100 0 n#251 estringouts.estringouts#238.OUT 1792 1392 1888 1392 hwout.hwout#250.outp
w 668 443 100 0 n#235 egenSubD.egenSubD#295.INPR 896 704 672 704 672 192 352 192 hwin.hwin#258.in
w 636 507 100 0 n#234 egenSubD.egenSubD#295.INPQ 896 736 640 736 640 288 352 288 hwin.hwin#216.in
w 706 803 100 0 n#233 egenSubD.egenSubD#295.INPO 896 800 576 800 576 480 352 480 hwin.hwin#256.in
w 690 835 100 0 n#232 egenSubD.egenSubD#295.INPN 896 832 544 832 544 576 352 576 hwin.hwin#84.in
w 674 867 100 0 n#231 egenSubD.egenSubD#295.INPM 896 864 512 864 512 672 352 672 hwin.hwin#81.in
w 658 899 100 0 n#230 egenSubD.egenSubD#295.INPL 896 896 480 896 480 768 352 768 hwin.hwin#218.in
w 642 931 100 0 n#229 egenSubD.egenSubD#295.INPK 896 928 448 928 448 864 352 864 hwin.hwin#79.in
w 594 963 100 0 n#228 egenSubD.egenSubD#295.INPJ 896 960 352 960 hwin.hwin#56.in
w 700 1531 100 0 n#227 egenSubD.egenSubD#295.INPA 896 1248 704 1248 704 1824 352 1824 hwin.hwin#38.in
w 668 1467 100 0 n#226 egenSubD.egenSubD#295.INPB 896 1216 672 1216 672 1728 352 1728 hwin.hwin#40.in
w 636 1403 100 0 n#225 egenSubD.egenSubD#295.INPC 896 1184 640 1184 640 1632 352 1632 hwin.hwin#43.in
w 604 1339 100 0 n#224 egenSubD.egenSubD#295.INPD 896 1152 608 1152 608 1536 352 1536 hwin.hwin#44.in
w 706 1123 100 0 n#223 egenSubD.egenSubD#295.INPE 896 1120 576 1120 576 1440 352 1440 hwin.hwin#47.in
w 690 1091 100 0 n#222 egenSubD.egenSubD#295.INPF 896 1088 544 1088 544 1344 352 1344 hwin.hwin#48.in
w 674 1059 100 0 n#221 egenSubD.egenSubD#295.INPG 896 1056 512 1056 512 1248 352 1248 hwin.hwin#51.in
w 658 1027 100 0 n#220 egenSubD.egenSubD#295.INPH 896 1024 480 1024 480 1152 352 1152 hwin.hwin#52.in
w 642 995 100 0 n#219 egenSubD.egenSubD#295.INPI 896 992 448 992 448 1056 352 1056 hwin.hwin#55.in
s 2624 2032 100 1792 2001/09/12
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Add initialized,datummed,parked
s 2016 2032 100 1792 B
s 2624 2064 100 1792 2000/12/30
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamState.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/09/12
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 FLAMINGOS State Generator
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use hwin 160 151 100 0 hwin#258
xform 0 256 192
p -96 192 100 0 -1 val(in):$(top)ec:applyC.VAL
use hwin 160 535 100 0 hwin#84
xform 0 256 576
p -64 576 100 0 -1 val(in):$(top)abortC.VAL
use hwin 160 631 100 0 hwin#81
xform 0 256 672
p -48 672 100 0 -1 val(in):$(top)stopC.VAL
use hwin 160 823 100 0 hwin#79
xform 0 256 864
p -192 864 100 0 -1 val(in):$(top)observationSetupC.VAL
use hwin 160 919 100 0 hwin#56
xform 0 256 960
p -112 960 100 0 -1 val(in):$(top)setDhsInfoC.VAL
use hwin 160 1015 100 0 hwin#55
xform 0 256 1056
p -64 1056 100 0 -1 val(in):$(top)setWcsC.VAL
use hwin 160 1111 100 0 hwin#52
xform 0 256 1152
p -96 1152 100 0 -1 val(in):$(top)dataModeC.VAL
use hwin 160 1207 100 0 hwin#51
xform 0 256 1248
p -176 1248 100 0 -1 val(in):$(top)instrumentSetupC.VAL
use hwin 160 1303 100 0 hwin#48
xform 0 256 1344
p -64 1344 100 0 -1 val(in):$(top)rebootC.VAL
use hwin 160 1399 100 0 hwin#47
xform 0 256 1440
p -64 1440 100 0 -1 val(in):$(top)debugC.VAL
use hwin 160 1495 100 0 hwin#44
xform 0 256 1536
p -48 1536 100 0 -1 val(in):$(top)parkC.VAL
use hwin 160 1591 100 0 hwin#43
xform 0 256 1632
p -64 1632 100 0 -1 val(in):$(top)datumC.VAL
use hwin 160 1687 100 0 hwin#40
xform 0 256 1728
p -48 1728 100 0 -1 val(in):$(top)testC.VAL
use hwin 160 1783 100 0 hwin#38
xform 0 256 1824
p -48 1824 100 0 -1 val(in):$(top)initC.VAL
use hwin 160 343 100 0 hwin#215
xform 0 256 384
p -96 384 100 0 -1 val(in):$(top)cc:applyC.VAL
use hwin 160 247 100 0 hwin#216
xform 0 256 288
p -96 288 100 0 -1 val(in):$(top)dc:applyC.VAL
use hwin 160 55 100 0 hwin#217
xform 0 256 96
p -96 96 100 0 -1 val(in):$(top)heartbeat.VAL
use hwin 160 727 100 0 hwin#218
xform 0 256 768
p -112 768 100 0 -1 val(in):$(top)observeCmdC.VAL
use hwin 160 439 100 0 hwin#256
xform 0 256 480
p -80 480 100 0 -1 val(in):$(top)observeC.VAL
use hwin 160 -41 100 0 hwin#296
xform 0 256 0
p 163 -8 100 0 -1 val(in):$(top)SimulationMode.VAL
use egenSubD 896 455 100 0 egenSubD#295
xform 0 1040 880
p 673 229 100 0 0 FTA:LONG
p 673 229 100 0 0 FTB:LONG
p 673 197 100 0 0 FTC:LONG
p 673 165 100 0 0 FTD:LONG
p 673 133 100 0 0 FTE:LONG
p 673 69 100 0 0 FTF:LONG
p 673 69 100 0 0 FTG:LONG
p 673 37 100 0 0 FTH:LONG
p 673 5 100 0 0 FTI:LONG
p 673 -27 100 0 0 FTJ:LONG
p 673 229 100 0 0 FTK:LONG
p 673 229 100 0 0 FTL:LONG
p 673 197 100 0 0 FTM:LONG
p 673 165 100 0 0 FTN:LONG
p 673 133 100 0 0 FTO:LONG
p 673 69 100 0 0 FTP:LONG
p 673 69 100 0 0 FTQ:LONG
p 673 37 100 0 0 FTR:LONG
p 673 5 100 0 0 FTS:LONG
p 673 -27 100 0 0 FTT:STRING
p 673 -27 100 0 0 FTU:LONG
p 673 229 100 0 0 FTVA:STRING
p 673 229 100 0 0 FTVB:LONG
p 673 197 100 0 0 FTVC:STRING
p 673 165 100 0 0 FTVD:LONG
p 673 133 100 0 0 FTVE:STRING
p 673 69 100 0 0 FTVF:LONG
p 673 69 100 0 0 FTVG:STRING
p 673 37 100 0 0 FTVH:LONG
p 673 5 100 0 0 FTVI:STRING
p 673 -27 100 0 0 FTVJ:LONG
p 960 416 100 0 1 INAM:flamStateCombineGInit
p 960 352 100 0 1 SCAN:.1 second
p 960 384 100 0 1 SNAM:flamStateCombineGProcess
p 612 650 100 0 0 def(INPT):
p 960 448 100 0 1 name:$(top)stateCombineG
p 1184 1226 75 0 -1 pproc(OUTA):PP
p 1184 1098 75 0 -1 pproc(OUTC):PP
p 1184 970 75 0 -1 pproc(OUTE):PP
p 1184 842 75 0 -1 pproc(OUTG):PP
p 1184 714 75 0 -1 pproc(OUTI):PP
use changeBar 1984 1991 100 0 changeBar#294
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#293
xform 0 2336 2064
use hwout 1888 455 100 0 hwout#268
xform 0 1984 496
p 2112 496 100 0 -1 val(outp):$(sad)parked.VAL PP NMS
use hwout 1888 903 100 0 hwout#265
xform 0 1984 944
p 2112 944 100 0 -1 val(outp):$(sad)initialized.VAL PP NMS
use hwout 1888 679 100 0 hwout#264
xform 0 1984 720
p 2112 720 100 0 -1 val(outp):$(sad)datummed.VAL PP NMS
use hwout 1888 1351 100 0 hwout#250
xform 0 1984 1392
p 2112 1392 100 0 -1 val(outp):$(sad)health.VAL PP NMS
use hwout 1888 1127 100 0 hwout#253
xform 0 1984 1168
p 2112 1168 100 0 -1 val(outp):$(sad)state.VAL PP NMS
use estringouts 1536 439 100 0 estringouts#267
xform 0 1664 512
p 1600 400 100 0 0 OMSL:supervisory
p 1600 432 100 768 1 name:$(top)parked
p 1792 496 75 768 -1 pproc(OUT):PP
use estringouts 1536 663 100 0 estringouts#263
xform 0 1664 736
p 1600 624 100 0 0 OMSL:supervisory
p 1600 656 100 768 1 name:$(top)datummed
p 1792 720 75 768 -1 pproc(OUT):PP
use estringouts 1536 887 100 0 estringouts#262
xform 0 1664 960
p 1600 848 100 0 0 OMSL:supervisory
p 1600 880 100 768 1 name:$(top)initialized
p 1792 944 75 768 -1 pproc(OUT):PP
use estringouts 1536 1111 100 0 estringouts#124
xform 0 1664 1184
p 1600 1072 100 0 0 OMSL:supervisory
p 1696 1104 100 1024 1 name:$(top)state
p 1792 1168 75 768 -1 pproc(OUT):PP
use estringouts 1536 1335 100 0 estringouts#238
xform 0 1664 1408
p 1600 1296 100 0 0 OMSL:supervisory
p 1600 1328 100 768 1 name:$(top)health
p 1792 1392 75 768 -1 pproc(OUT):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamState.sch,v 0.0 2005/09/01 20:21:37 drashkin Exp $
