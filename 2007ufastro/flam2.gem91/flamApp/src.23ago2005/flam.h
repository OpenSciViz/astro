#if !defined(__FLAM_H__)
#define __FLAM_H__ "RCS: $Name:  $ $Id: flam.h,v 0.0 2005/09/01 20:25:30 drashkin Exp $"
static const char rcsIdufFLAMH[] = __FLAM_H__;

/* min ticks for busy */
#define MIN_CAR_BUSY_TIME 30

#define SIMM_NONE             0	/* Real Mode "raw" */
#define SIMM_VSM              1	/* Virtual Simulation Mode */
#define SIMM_FAST             2	/* Simulation w/o the Agent "sim" */
#define SIMM_FULL             3	/* Simulation with the Agent "sim" */


#define TRX_DEBUG_NONE        0	/* Just error statements */
#define TRX_DEBUG_MIN         1	/* Error + descriptions */
#define TRX_DEBUG_FULL        2	/* Everything */

#if defined(__FLAM_C__)
int flam_initialized = 0;
int NumFiles = 0;
#else
extern int flam_initialized;
extern int NumFiles;
#endif

static char ccconfig_filename[] = "./pv/ccconfig.txt";
static char mot_filename[] = "./pv/mot_param.txt";

static char dcconfig_filename[] = "./pv/dcconfig.txt";
static char dc_param_filename[] = "./pv/dc_param.txt";
static char dc_bias_filename[] = "./pv/dc_bias_param.txt";
static char dc_preamp_filename[] = "./pv/dc_preamp_param.txt";
static char ecconfig_filename[] = "./pv/ecconfig.txt";
static char env_gs_filename[] = "./pv/env_gs.txt";
static char env_hb_filename[] = "./pv/env_hb.txt";
static char env_array_filename[] = "./pv/env_array.txt";
static char env_tempMon_filename[] = "./pv/env_tempMon.txt";
static char env_press_filename[] = "./pv/env_press.txt";
static char temp_cont_filename[] = "./pv/temp_cont.txt";
static char ch_param_filename[] = "./pv/ch_param.txt";
static char vac_param_filename[] = "./pv/vac_param.txt";
static char bt_param_filename[] = "./pv/bt_param.txt";

int init_flam_config ();
void trx_debug (const char *message,
		const char *rec_name,
		const char *mess_level,
		const char *debug_level);

#endif
