

/*
 *
 *  Header for dummy lookup table code.
 *
 */

#include <stdioLib.h>
#include <stdlib.h>
#include <ellLib.h>
#include <string.h>
#include "flam.h" 
#include <flamBogusLookup.h>


/*
 * Local defines
 */

/*
 *  Private data
 */

static ELLLIST *pFilterList, *pNameList;


typedef struct
{
  ELLNODE node;
  char name[NAME_SIZE];
  char filter1[NAME_SIZE];
  char filter2[NAME_SIZE];
  char window[NAME_SIZE];
}
FILTER_NODE;


typedef struct
{
  ELLNODE node;
  char name[NAME_SIZE];
  char position[NAME_SIZE];
  float efficiency;
}
NAME_NODE;



/*
 *************************************
 *
 *  Header for flamInitBogusLookup
 *
 *************************************
 */


long
flamInitBogusLookup (void)
{
  long status = OK;		/* function return status */

  /* 
   *  Create the two lookup table lists
   */

  pFilterList = (ELLLIST *) malloc (sizeof (ELLLIST));
  ellInit (pFilterList);

  pNameList = (ELLLIST *) malloc (sizeof (ELLLIST));
  ellInit (pNameList);


  return status;
}


/*
 *************************************
 *
 *  Header for flamReloadBogusLookup
 *
 *************************************
 */

long
flamReloadBogusLookup (void)
{
  FILE *fp = NULL;		/* generic file pointer */
  char dataLine[256];
  FILTER_NODE *pFilterNode = NULL;
  NAME_NODE *pNameNode = NULL;
  long status = OK;		/* function return status */



  /* 
   *  reload filter configuration lookup table
   */

  fp = fopen ("./pv/bogusFilterLookup.txt", "r");
  if (fp == NULL)
    {
      printf ("can't open filterConfiguration file\n");
      return ERROR;
    }
  NumFiles++;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */
  ellFree (pFilterList);

  pFilterNode = (FILTER_NODE *) ellFirst (pFilterList);

  while (fgets (dataLine, sizeof (dataLine), fp) != NULL)
    {
      if (*dataLine == '#' || *dataLine == '\n')
	{
	  continue;
	}

      if (!pFilterNode)
	{
	  pFilterNode = (FILTER_NODE *) malloc (sizeof (FILTER_NODE));
	  ellAdd (pFilterList, &pFilterNode->node);
	}

      if (sscanf (dataLine,
		  "%s %s %s %s",
		  pFilterNode->name,
		  pFilterNode->filter1,
		  pFilterNode->filter2, pFilterNode->window) != 4)
	{
	  ellFree (pFilterList);
	  printf ("error reading filter translation file\n");
	  fclose (fp);
	  NumFiles--;
	  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of
	     files is %d\n \n",NumFiles) ; */
	  return ERROR;
	}

      pFilterNode = (FILTER_NODE *) ellNext (&pFilterNode->node);
    }

  fclose (fp);
  NumFiles--;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */


  /* 
   *  reload position lookup table
   */

  fp = fopen ("./data/bogusPositionLookup.txt", "r");
  if (fp == NULL)
    {
      printf ("can't open position file\n");
      return ERROR;
    }
  NumFiles++;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */
  ellFree (pNameList);

  pNameNode = (NAME_NODE *) ellFirst (pNameList);

  while (fgets (dataLine, sizeof (dataLine), fp) != NULL)
    {
      if (*dataLine == '#' || *dataLine == '\n')
	{
	  continue;
	}

      if (!pNameNode)
	{
	  pNameNode = (NAME_NODE *) malloc (sizeof (NAME_NODE));
	  ellAdd (pNameList, &pNameNode->node);
	}

      if (sscanf (dataLine,
		  "%s %s %f",
		  pNameNode->position,
		  pNameNode->name, &pNameNode->efficiency) != 3)
	{
	  ellFree (pNameList);
	  printf ("error reading position translation file\n");
	  fclose (fp);
	  NumFiles--;
	  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of
	     files is %d\n \n",NumFiles) ; */
	  return ERROR;
	}

      pNameNode = (NAME_NODE *) ellNext (&pNameNode->node);
    }

  fclose (fp);
  NumFiles--;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */
  return status;
}


/*
 *************************************
 *
 *  Header for flamBogusFilterLookup
 *
 *************************************
 */

long flamBogusFilterLookup
  (char *filter, char *filter1, char *filter2, char *window)
{
  FILTER_NODE *pFilterNode;
  long status = OK;		/* function return status */

  pFilterNode = (FILTER_NODE *) ellFirst (pFilterList);

  while (pFilterNode != NULL)
    {
      if (strcmp (pFilterNode->name, filter) == 0)
	{
	  strcpy (filter1, pFilterNode->filter1);
	  strcpy (filter2, pFilterNode->filter2);
	  strcpy (window, pFilterNode->window);

	  break;
	}

      pFilterNode = (FILTER_NODE *) ellNext (&pFilterNode->node);
    }

  if (pFilterNode == NULL)
    {
      status = ERROR;
    }

  return status;
}


/*
 *************************************
 *
 *  Header for flamBogusNameLookup
 *
 *************************************
 */

long
flamBogusNameLookup (char *name, char *position, float *efficiency)
{
  NAME_NODE *pNameNode;
  long status = OK;		/* function return status */

  pNameNode = (NAME_NODE *) ellFirst (pNameList);

  while (pNameNode != NULL)
    {
      if (strcmp (pNameNode->name, name) == 0)
	{
	  strcpy (position, pNameNode->position);
	  *efficiency = pNameNode->efficiency;

	  break;
	}

      pNameNode = (NAME_NODE *) ellNext (&pNameNode->node);
    }

  if (pNameNode == NULL)
    {
      strcpy (position, "");
      *efficiency = 0.0;
      status = ERROR;
    }

  return status;
}
