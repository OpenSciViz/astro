#if !defined(__UFGEMGENSUBCOMM_C__)
#define __UFGEMGENSUBCOMM_C__ "RCS: $Name:  $ $Id: ufGemGensubComm.c,v 0.0 2005/09/01 20:25:30 drashkin Exp $"
static const char rcsIdufGEMGENSUBCOMMC[] = __UFGEMGENSUBCOMM_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>

#include <string.h>

#include <sysLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
/* #include <genSub.h> */
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>


#include <dbStaticLib.h>
#include <dbBase.h>

#include "ufGemComm.h"
#include "ufGemGensubComm.h"

static FILE *logFile = NULL;

/******************* UTILITY  CODING **********************/
void
flushLog ()
{
  fflush (logFile);
}

/***************** Unpacks array of strings ***************/
long
unpack_array (genSubRecord * pgs)
{
  char *zz;
  int elem_cnt = pgs->noj;
  char yy[40];
  /* if (bingo) printf("I got %d elements \n",elem_cnt) ; */
  zz = (char *) pgs->j;

  strncpy (yy, zz, 39);
  yy[39] = 0;
  strcpy (pgs->vala, yy);

  if (elem_cnt > 1)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valb, yy);
    }
  if (elem_cnt > 2)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valc, yy);
    }
  if (elem_cnt > 3)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vald, yy);
    }
  if (elem_cnt > 4)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vale, yy);
    }
  if (elem_cnt > 5)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valf, yy);
    }
  if (elem_cnt > 6)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valg, yy);
    }
  if (elem_cnt > 7)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valh, yy);
    }
  if (elem_cnt > 8)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vali, yy);
    }
  if (elem_cnt > 9)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valj, yy);
    }
  if (elem_cnt > 10)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valk, yy);
    }
  if (elem_cnt > 11)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vall, yy);
    }
  if (elem_cnt > 12)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valm, yy);
    }
  if (elem_cnt > 13)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valn, yy);
    }
  if (elem_cnt > 14)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valo, yy);
    }
  if (elem_cnt > 15)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valp, yy);
    }
  if (elem_cnt > 16)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valq, yy);
    }
  if (elem_cnt > 17)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valr, yy);
    }
  if (elem_cnt > 18)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vals, yy);
    }
  if (elem_cnt > 19)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valt, yy);
    }
  if (elem_cnt > 20)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valu, yy);
    }

  return OK;
}


/***************** Unpacks array of strings ***************/
long
unpack_any_array (genSubRecord * pgs, char *zz, int elem_cnt, char *remainder)
{

  char yy[40];
  /* if (bingo) printf("I got %d elements \n",elem_cnt) ; */
  char *original;

  original = zz;
  strncpy (yy, zz, 39);
  yy[39] = 0;
  strcpy (pgs->vala, yy);

  if (elem_cnt > 1)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valb, yy);
    }
  if (elem_cnt > 2)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valc, yy);
    }
  if (elem_cnt > 3)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vald, yy);
    }
  if (elem_cnt > 4)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vale, yy);
    }
  if (elem_cnt > 5)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valf, yy);
    }
  if (elem_cnt > 6)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valg, yy);
    }
  if (elem_cnt > 7)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valh, yy);
    }
  if (elem_cnt > 8)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vali, yy);
    }
  if (elem_cnt > 9)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valj, yy);
    }
  if (elem_cnt > 10)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valk, yy);
    }
  if (elem_cnt > 11)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vall, yy);
    }
  if (elem_cnt > 12)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valm, yy);
    }
  if (elem_cnt > 13)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valn, yy);
    }
  if (elem_cnt > 14)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valo, yy);
    }
  if (elem_cnt > 15)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valp, yy);
    }
  if (elem_cnt > 16)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valq, yy);
    }
  if (elem_cnt > 17)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valr, yy);
    }
  if (elem_cnt > 18)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->vals, yy);
    }
  if (elem_cnt > 19)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valt, yy);
    }
  if (elem_cnt > 20)
    {
      zz = zz + 40;
      strncpy (yy, zz, 39);
      yy[39] = 0;
      strcpy (pgs->valu, yy);
    }
  remainder = zz;
  zz = original;

  return OK;
}

/******************* CALL BACK CODING *********************/
/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * initCallback
 *
 * INVOCATION:
 * pCallback = initCallback ( callbackFunction );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * void(*)(GENSUB_CALLBACK *) callbackFunction
 *
 * FUNCTION VALUE:
 * (GENSUB_CALLBACK *) callback structure
 *
 * PURPOSE:
 * Create and initialize a callback timer
 *
 * DESCRIPTION:
 *
 *      Create a record callback structure.
 *      Register the callback with the EPICS callback system.
 *      Create an EPICS watchdog timer for the command timeout.
 *      
 *      
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * triggerCallback, abortCallback
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

GENSUB_CALLBACK *
initCallback (void (*callbackFunction) (GENSUB_CALLBACK *))
{
  GENSUB_CALLBACK *pCallback = NULL;	/* Callback
					   structure ptr.  */


  /* 
   *  Create a command timeout timer callback structure and save it in
   *  the internal control structure for this record.
   */

  pCallback = (GENSUB_CALLBACK *) malloc (sizeof (GENSUB_CALLBACK));
  if (pCallback == NULL)
    {
      printf ("can't create callback structure\n");
      return NULL;
    }


  /* 
   *  Register the command timeout callback with the EPICS callback system
   */

  callbackSetCallback (callbackFunction, &pCallback->callback);


  /* 
   *  Create a watchdog timer to invoke it via the EPICS watchdog task
   */

  pCallback->watchdogId = wdCreate ();
  if (pCallback->watchdogId == NULL)
    {
      printf ("can't create watchdog timer\n");
      return NULL;
    }


  /* 
   *  Set this as a very low priority callback
   */
  /* --hon epics/base/include/callback.h defines:
     priorityLow 0 define priorityMedium 1 define
     priorityHigh 2 callbackSetPriority( priorityLow,
     &pCallback->callback ); */
  callbackSetPriority (priorityMedium, &pCallback->callback);

  return pCallback;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * requestCallback
 *
 * INVOCATION:
 * status = requestCallback ( pCallback, ticks );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pCallback  (GENSUB_CALLBACK *)   callback to trigger.
 * (>) ticks      (long)                time before callback.
 *
 * FUNCTION VALUE:
 * (long) return status code.
 *
 * PURPOSE:
 * Trigger a callback after the given number of system ticks.
 *
 * DESCRIPTION:
 *
 *      Ask the EPICS watchdog task to invoke the associated callback 
 *          function when the given time interval expires
 *  
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * initCallback
 * abortCallback
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */
long
requestCallback (GENSUB_CALLBACK * pCallback,	/* (in) ptr 
						   to Ass
						   record */
		 long ticks	/* (in) time to wait */
  )
{
  long status = OK;		/* function return status */


  /* 
   *  Start the EPICS watchdog timer identified in the timeout struct.
   */

  status = wdStart (pCallback->watchdogId,
		    ticks, (FUNCPTR) callbackRequest, (int) pCallback);

  if (status == ERROR)
    {
      printf ("can't start watchdog timer\n");
      return status;
    }

  return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * cancelCallback
 *
 * INVOCATION:
 * status = cancelCallback (pCallback);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pCallback  (GENSUB_CALLBACK *) callback to cancel.
 *
 * FUNCTION VALUE:
 * (long) return status code.
 *
 * PURPOSE:
 * Abort a callback in progress.
 *
 * DESCRIPTION:
 * Calls the EPICS watchdog cancel function to abort a callback request
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *      initCallback
 *      requestCallback
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long
cancelCallback (GENSUB_CALLBACK * pCallback	/* (in)
						   callback 
						   to
						   cancel */
  )
{
  long status;			/* function return status */

  /* 
   * Cancel the EPICS watchdog timer associated with the command timeout
   * function.
   */

  status = wdCancel (pCallback->watchdogId);
  if (status == ERROR)
    {
      printf ("can't cancel the callback\n");
    }

  return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * flamGensubCallback
 *
 * INVOCATION:
 * flamGensubCallback (pCallback);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) GENSUB_CALLBACK *pcallback    Ptr to callback struct.
 *
 * FUNCTION VALUE:
 * none.
 *
 * PURPOSE:
 * Handle command failures without blocking the record processing.
 *
 * DESCRIPTION:
 * Set the CAR record associated with the gensub record to the ERROR
 * state.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */
void
flamGensubCallback (GENSUB_CALLBACK * pCallback	/* (in) 
							   Ptr 
							   to 
							   callback 
							   struct. 
							 */
  )
{
  genSubRecord *pgs;		/* gensub record structure */
  genSubCommPrivate *pPriv;	/* private control
				   structure */
  /* long status; *//* function return status */
  struct rset *pRset;

  /* 
   * Recover the private control structure from the callback struct.
   */

  pgs = (genSubRecord *) pCallback->pRecord;
  pPriv = (genSubCommPrivate *) pgs->dpvt;

  printf ("flamGensubCallback> start... %s\n", pgs->name);

  /* 
   *  If the command state was busy when the callback happened
   *  then the agent took too long to respond and may have
   *  gone away.....  Set the command to error.
   */

  if ((pPriv->commandState == TRX_GS_SENDING)
      || (pPriv->commandState == TRX_GS_BUSY)
      || (pPriv->commandState == TRX_GS_INIT))
    {
      pgs = (genSubRecord *) pCallback->pRecord;
      pRset = pgs->rset;

      strcpy (pPriv->errorMessage, "CALLBACK");
      /* pCallback->callbackFlag = TRUE; */
      /* pPriv->commandState = pCallback->exec_state ; */
      dbScanLock (pCallback->pRecord);
      (*pRset->process) (pgs);
      dbScanUnlock (pCallback->pRecord);

      return;
    }
  /* 
     if (pPriv->commandState == TRX_GS_BUSY) { strcpy
     (pPriv->errorMessage, "Command timeout...");
     pPriv->commandState = TRX_GS_DONE; }

     status = dbPutField (&pPriv->carinfo.carMessage,
     DBR_STRING, pPriv->errorMessage, 1); if (status) {
     printf ("can't set CAR message\n"); }

     status = dbPutField (&pPriv->carinfo.carState,
     DBR_LONG, &pPriv->commandState, 1); if (status) {
     printf ("can't set CAR to ERROR\n"); } */
  /* pPriv->commandState = TRX_GS_DONE; */

  return;
}


/* ===================================================================== */

/* INDENT OFF */

/*
 * Function name:
 *  ufflamNullGInit
 *
 * Purpose:
 *  Dummy initialization function that simply creates a private
 *  long word.
 * 
 * Invocation:
 *  status = flamNullGInit (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


long
ufflamNullGInit (genSubRecord * pgs	/* cad record
					   structure */
  )
{
  long *pLong = NULL;
  long status = OK;		/* function return status */

  pLong = (long *) malloc (sizeof (long));
  if (pLong == NULL)
    {
      logMsg ("can't create long pointer for gensub\n", 0, 0, 0, 0, 0, 0);
      status = ERROR;
    }

  *pLong = 0;

  pgs->dpvt = (void *) pLong;

  return status;
}


/* ===================================================================== */

/* INDENT OFF */

/*
 * Function name:
 *  ufflamCmdCombineGProcess
 *
 * Purpose:
 *  Combine the CAR record states and error messages from up to 10 
 *  instrument sequencer commands into a single IDLE/BUSY/ERR state.
 * 
 * Invocation:
 *  status = flamCmdCombineGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     long    command 1 CAR state
 *      > pgs->b     string  command 1 error message
 *      > pgs->c     long    command 2 CAR state
 *      > pgs->d     string  command 2 error message
 *      > pgs->e     long    command 3 CAR state
 *      > pgs->f     string  command 3 error message
 *      > pgs->g     long    command 4 CAR state
 *      > pgs->h     string  command 4 error message
 *      > pgs->i     long    command 5 CAR state
 *      > pgs->k     string  command 5 error message
 *      > pgs->l     long    command 6 CAR state
 *      > pgs->m     string  command 6 error message
 *      > pgs->n     long    command 7 CAR state
 *      > pgs->o     string  command 7 error message
 *      > pgs->p     long    command 8 CAR state
 *      > pgs->q     string  command 8 error message
 *      > pgs->r     long    command 9 CAR state
 *      > pgs->s     string  command 9 error message
 *      > pgs->t     long    command 10 CAR state
 *      > pgs->u     string  command 10 error message
 * 
 * Parameters out:
 *      < pgs->vala  string  error message
 *      < pgs->valb  long    combined CAR state
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */

long
ufflamCmdCombineGProcess (genSubRecord * pgs	/* gensub
						   record
						   structure 
						 */
  )
{
  long initialActive;
  long *active = (long *) pgs->dpvt;

  /* 
   *  Remember which commands have gone busy
   */
  initialActive = *active;
  if (*(long *) pgs->a == CAR_BUSY)
    *active |= TRX_CMD1;
  if (*(long *) pgs->c == CAR_BUSY)
    *active |= TRX_CMD2;
  if (*(long *) pgs->e == CAR_BUSY)
    *active |= TRX_CMD3;
  if (*(long *) pgs->g == CAR_BUSY)
    *active |= TRX_CMD4;
  if (*(long *) pgs->i == CAR_BUSY)
    *active |= TRX_CMD5;
  if (*(long *) pgs->l == CAR_BUSY)
    *active |= TRX_CMD6;
  if (*(long *) pgs->n == CAR_BUSY)
    *active |= TRX_CMD7;
  if (*(long *) pgs->p == CAR_BUSY)
    *active |= TRX_CMD8;
  if (*(long *) pgs->r == CAR_BUSY)
    *active |= TRX_CMD9;
  if (*(long *) pgs->t == CAR_BUSY)
    *active |= TRX_CMD10;


  /* 
   *  If any input is BUSY then the resulting value is BUSY
   */

  if (*(long *) pgs->a == CAR_BUSY ||
      *(long *) pgs->c == CAR_BUSY ||
      *(long *) pgs->e == CAR_BUSY ||
      *(long *) pgs->g == CAR_BUSY ||
      *(long *) pgs->i == CAR_BUSY ||
      *(long *) pgs->l == CAR_BUSY ||
      *(long *) pgs->n == CAR_BUSY ||
      *(long *) pgs->p == CAR_BUSY ||
      *(long *) pgs->r == CAR_BUSY || *(long *) pgs->t == CAR_BUSY)
    {
      *(char *) (pgs->vala) = '\0';
      *(long *) pgs->valb = CAR_BUSY;

      /*
      if (_verbose)
	printf ("ufflamCmdCombineGProcess> set CAR_BUSY ... %s\n",
		pgs->name);
      */
    }


  /* 
   *  Otherwise, check to see if any of the busy commands are now
   *  in an error state.  The first error will set the resulting 
   *  value to ERR and copy the associated message to the message output.
   */

  else if ((*active & TRX_CMD1) && (*(long *) pgs->a == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->b);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field A ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD2) && (*(long *) pgs->c == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->d);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field C ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD3) && (*(long *) pgs->e == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->f);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field E ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD4) && (*(long *) pgs->g == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->h);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field G ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD5) && (*(long *) pgs->i == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->k);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field I ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD6) && (*(long *) pgs->l == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->m);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field L ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD7) && (*(long *) pgs->n == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->o);
      *(long *) pgs->valb = CAR_ERROR;
    }

  else if ((*active & TRX_CMD8) && (*(long *) pgs->p == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->q);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field P ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD9) && (*(long *) pgs->r == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->s);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field R ... %s\n",
	   pgs->name);
      */
    }

  else if ((*active & TRX_CMD10) && (*(long *) pgs->t == CAR_ERROR))
    {
      *active = 0;
      strcpy (pgs->vala, pgs->u);
      *(long *) pgs->valb = CAR_ERROR;

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set CAR_ERROR from field T ... %s\n",
	   pgs->name);
      */
    }


  /* 
   *  None of the active commands had an error .... must be IDLE
   */

  else if (*(long *) pgs->vala != CAR_ERROR)
    {
      *active = 0;
      *(long *) pgs->valb = CAR_IDLE;

      /*
      if (_verbose)
	printf ("ufflamCmdCombineGProcess> set CAR_IDLE, VAL A ... %s\n",
		pgs->name);
      */
    }

  /* 
   *  If the change in active state indicates that
   *  a command has started or completed then update
   *  the CAR record.
   */

  if ((initialActive == 0 && *active != 0) ||
      (initialActive != 0 && *active == 0))
    {
      *(long *) pgs->valc = 0;
    }
  else
    {
      *(long *) pgs->valc = 1;
    }



  /* 
   *  If the CAR state has not changed then set VALC to inhibit
   *  the attached CAR record updating records.  This prevents the
   *  CAR record from being processed unless there is a change.
   */
/* 
  if (*(long *) pgs->vala == *(long *) pgs->ovla)
    {
      *(long *) pgs->valc = 1;
    }
  else
    {
      *(long *) pgs->valc = 0;
    }
*/
  /* Let's figure out the state of the system and its
     health */

  if (*(long *) pgs->a == CAR_ERROR ||
      *(long *) pgs->c == CAR_ERROR ||
      *(long *) pgs->e == CAR_ERROR ||
      *(long *) pgs->g == CAR_ERROR ||
      *(long *) pgs->i == CAR_ERROR ||
      *(long *) pgs->l == CAR_ERROR ||
      *(long *) pgs->n == CAR_ERROR ||
      *(long *) pgs->p == CAR_ERROR ||
      *(long *) pgs->r == CAR_ERROR || *(long *) pgs->t == CAR_ERROR)
    {
      strcpy (pgs->vale, "ERROR");
      strcpy (pgs->vald, "BAD");

      /*
      if (_verbose)
	printf
	  ("ufflamCmdCombineGProcess> set field E ERROR, field D BAD ... %s\n",
	   pgs->name);
      */
    }
  else
    {
      if (*(long *) pgs->a == CAR_BUSY ||
	  *(long *) pgs->c == CAR_BUSY ||
	  *(long *) pgs->e == CAR_BUSY ||
	  *(long *) pgs->g == CAR_BUSY ||
	  *(long *) pgs->i == CAR_BUSY ||
	  *(long *) pgs->l == CAR_BUSY ||
	  *(long *) pgs->n == CAR_BUSY ||
	  *(long *) pgs->p == CAR_BUSY ||
	  *(long *) pgs->r == CAR_BUSY || *(long *) pgs->t == CAR_BUSY)
	{
	  strcpy (pgs->vale, "BUSY");
	  strcpy (pgs->vald, "GOOD");

	  /*
	  if (_verbose)
	    printf
	      ("ufflamCmdCombineGProcess> set field E BUSY, field D GOOD ... %s\n",
	       pgs->name);
	  */
	}
      else
	{
	  strcpy (pgs->vale, "IDLE");
	  strcpy (pgs->vald, "GOOD");

	  /*
	  if (_verbose)
	    printf
	      ("ufflamCmdCombineGProcess> set field E IDLE, field D GOOD ... %s\n",
	       pgs->name);
	  */
	}
    }

  return OK;
}


#endif
