[schematic2]
uniq 1404
[tools]
[detail]
w 1924 827 100 0 n#1403 estringouts.estringouts#1401.OUT 1120 320 1248 320 1248 256 1920 256 1920 1408 2016 1408 egenSubFLAM.egenSubFLAM#730.T
w 516 691 100 0 n#1402 ecad20.$(top)$(dev)obsControl.VALS 384 1024 512 1024 512 368 864 368 estringouts.estringouts#1401.DOL
w 1170 403 100 0 n#1400 estringouts.estringouts#1401.FLNK 1120 352 1152 352 1152 400 1248 400 estringouts.estringouts#1198.SLNK
w 2780 619 100 0 n#1362 elongouts.elongouts#1358.OUT 2752 608 2880 608 hwout.hwout#1361.outp
w 2396 683 100 0 n#1360 elongins.elongins#1357.VAL 2336 608 2368 608 2368 672 2496 672 elongouts.elongouts#1358.DOL
w 2380 651 100 0 n#1359 elongins.elongins#1357.FLNK 2336 640 2496 640 elongouts.elongouts#1358.SLNK
w 2236 2291 100 0 n#1336 estringouts.estringouts#889.FLNK 2144 2304 2208 2304 2208 2288 2336 2288 estringouts.estringouts#900.SLNK
w 1932 2147 100 0 n#1335 estringouts.estringouts#889.OUT 2144 2272 2176 2272 2176 2144 1760 2144 1760 1952 2016 1952 egenSubFLAM.egenSubFLAM#730.C
w 1196 163 100 0 n#1399 ecad20.$(top)$(dev)obsControl.STLK 384 736 480 736 480 160 1984 160 1984 976 2064 976 efanouts.efanouts#691.SLNK
w 956 483 100 0 n#1202 estringouts.estringouts#1198.FLNK 1504 416 1568 416 1568 512 1216 512 1216 480 768 480 768 624 864 624 estringouts.estringouts#904.SLNK
w 860 435 100 0 n#1374 ecad20.$(top)$(dev)obsControl.VALR 384 1088 544 1088 544 432 1248 432 estringouts.estringouts#1198.DOL
w 1884 907 100 0 n#1200 estringouts.estringouts#1198.OUT 1504 384 1888 384 1888 1440 2016 1440 egenSubFLAM.egenSubFLAM#730.S
w 1164 2139 100 0 n#1132 estringouts.estringouts#893.FLNK 1120 2080 1152 2080 1152 2128 1248 2128 estringouts.estringouts#892.SLNK
w 1324 195 100 0 n#1305 efanouts.efanouts#691.LNK1 2304 1056 2400 1056 2400 1152 1952 1152 1952 192 768 192 768 336 864 336 estringouts.estringouts#1401.SLNK
w 2162 1187 100 0 n#837 flamLinkEnable.flamLinkEnable#664.FLINK 2816 1056 2848 1056 2848 896 2432 896 2432 1184 1952 1184 1952 1312 2016 1312 egenSubFLAM.egenSubFLAM#730.SLNK
w 2556 1611 100 0 n#836 elongouts.elongouts#703.FLNK 2208 2592 2880 2592 2880 2048 2560 2048 2560 1184 2464 1184 2464 1088 2496 1088 flamLinkEnable.flamLinkEnable#664.ENABLE
w 1852 1003 100 0 n#835 estringouts.estringouts#904.OUT 1120 608 1248 608 1248 544 1856 544 1856 1472 2016 1472 egenSubFLAM.egenSubFLAM#730.R
w 1820 1083 100 0 n#834 estringouts.estringouts#903.OUT 1504 672 1824 672 1824 1504 2016 1504 egenSubFLAM.egenSubFLAM#730.Q
w 1788 1179 100 0 n#833 estringouts.estringouts#902.OUT 1120 896 1248 896 1248 832 1792 832 1792 1536 2016 1536 egenSubFLAM.egenSubFLAM#730.P
w 1756 1259 100 0 n#832 estringouts.estringouts#901.OUT 1504 960 1760 960 1760 1568 2016 1568 egenSubFLAM.egenSubFLAM#730.O
w 1724 1347 100 0 n#831 estringouts.estringouts#899.OUT 1120 1184 1248 1184 1248 1104 1728 1104 1728 1600 2016 1600 egenSubFLAM.egenSubFLAM#730.N
w 1692 1435 100 0 n#830 estringouts.estringouts#897.OUT 1504 1248 1696 1248 1696 1632 2016 1632 egenSubFLAM.egenSubFLAM#730.M
w 1426 1411 100 0 n#829 estringouts.estringouts#898.OUT 1120 1472 1248 1472 1248 1408 1664 1408 1664 1664 2016 1664 egenSubFLAM.egenSubFLAM#730.L
w 1794 1699 100 0 n#828 estringouts.estringouts#895.OUT 1504 1536 1632 1536 1632 1696 2016 1696 egenSubFLAM.egenSubFLAM#730.K
w 1778 1763 100 0 n#827 estringouts.estringouts#896.OUT 1120 1760 1248 1760 1248 1696 1600 1696 1600 1760 2016 1760 egenSubFLAM.egenSubFLAM#730.I
w 1778 1795 100 0 n#826 estringouts.estringouts#894.OUT 1504 1824 1600 1824 1600 1792 2016 1792 egenSubFLAM.egenSubFLAM#730.H
w 1410 1987 100 0 n#672 estringouts.estringouts#893.OUT 1120 2048 1248 2048 1248 1984 1632 1984 1632 1824 2016 1824 egenSubFLAM.egenSubFLAM#730.G
w 1810 1859 100 0 n#824 estringouts.estringouts#892.OUT 1504 2112 1664 2112 1664 1856 2016 1856 egenSubFLAM.egenSubFLAM#730.F
w 1442 2275 100 0 n#823 estringouts.estringouts#891.OUT 1120 2336 1248 2336 1248 2272 1696 2272 1696 1888 2016 1888 egenSubFLAM.egenSubFLAM#730.E
w 1724 2155 100 0 n#822 estringouts.estringouts#888.OUT 1504 2400 1728 2400 1728 1920 2016 1920 egenSubFLAM.egenSubFLAM#730.D
w 1602 2435 100 0 n#814 estringouts.estringouts#888.FLNK 1504 2432 1760 2432 1760 2288 1888 2288 estringouts.estringouts#889.SLNK
w 2226 2083 100 0 n#816 elongouts.elongouts#703.OUT 2208 2528 2688 2528 2688 2080 1824 2080 1824 2016 2016 2016 egenSubFLAM.egenSubFLAM#730.A
w 2194 2115 100 0 n#820 estringouts.estringouts#900.OUT 2592 2272 2656 2272 2656 2112 1792 2112 1792 1984 2016 1984 egenSubFLAM.egenSubFLAM#730.B
w 572 899 100 0 n#721 ecad20.$(top)$(dev)obsControl.VALQ 384 1152 576 1152 576 656 864 656 estringouts.estringouts#904.DOL
w 898 723 100 0 n#720 ecad20.$(top)$(dev)obsControl.VALP 384 1216 608 1216 608 720 1248 720 estringouts.estringouts#903.DOL
w 636 1107 100 0 n#719 ecad20.$(top)$(dev)obsControl.VALO 384 1280 640 1280 640 944 864 944 estringouts.estringouts#902.DOL
w 930 1011 100 0 n#718 ecad20.$(top)$(dev)obsControl.VALN 384 1344 672 1344 672 1008 1248 1008 estringouts.estringouts#901.DOL
w 514 1411 100 0 n#717 ecad20.$(top)$(dev)obsControl.VALM 384 1408 704 1408 704 1232 864 1232 estringouts.estringouts#899.DOL
w 962 1299 100 0 n#716 ecad20.$(top)$(dev)obsControl.VALL 384 1472 736 1472 736 1296 1248 1296 estringouts.estringouts#897.DOL
w 658 1523 100 0 n#715 ecad20.$(top)$(dev)obsControl.VALK 384 1536 512 1536 512 1520 864 1520 estringouts.estringouts#898.DOL
w 850 1587 100 0 n#714 ecad20.$(top)$(dev)obsControl.VALJ 384 1600 512 1600 512 1584 1248 1584 estringouts.estringouts#895.DOL
w 530 1667 100 0 n#713 ecad20.$(top)$(dev)obsControl.VALI 384 1664 736 1664 736 1808 864 1808 estringouts.estringouts#896.DOL
w 1180 2595 100 0 n#1391 ecad20.$(top)$(dev)obsControl.VALA 384 2176 480 2176 480 2592 1952 2592 elongouts.elongouts#703.DOL
w 1132 2531 100 0 n#1396 ecad20.$(top)$(dev)obsControl.VALB 384 2112 512 2112 512 2528 1824 2528 1824 2368 2256 2368 2256 2320 2336 2320 estringouts.estringouts#900.DOL
w 1132 2499 100 0 n#1393 ecad20.$(top)$(dev)obsControl.VALC 384 2048 544 2048 544 2496 1792 2496 1792 2320 1888 2320 estringouts.estringouts#889.DOL
w 882 2451 100 0 n#709 ecad20.$(top)$(dev)obsControl.VALD 384 1984 576 1984 576 2448 1248 2448 estringouts.estringouts#888.DOL
w 604 2147 100 0 n#708 ecad20.$(top)$(dev)obsControl.VALE 384 1920 608 1920 608 2384 864 2384 estringouts.estringouts#891.DOL
w 914 2163 100 0 n#707 ecad20.$(top)$(dev)obsControl.VALF 384 1856 640 1856 640 2160 1248 2160 estringouts.estringouts#892.DOL
w 668 1939 100 0 n#706 ecad20.$(top)$(dev)obsControl.VALG 384 1792 672 1792 672 2096 864 2096 estringouts.estringouts#893.DOL
w 946 1875 100 0 n#705 estringouts.estringouts#894.DOL 1248 1872 704 1872 704 1728 384 1728 ecad20.$(top)$(dev)obsControl.VALH
w 2210 2403 100 0 n#890 estringouts.estringouts#900.FLNK 2592 2304 2624 2304 2624 2400 1856 2400 1856 2560 1952 2560 elongouts.elongouts#703.SLNK
w 1170 979 100 0 n#694 estringouts.estringouts#902.FLNK 1120 928 1152 928 1152 976 1248 976 estringouts.estringouts#901.SLNK
w 1138 771 100 0 n#693 estringouts.estringouts#903.FLNK 1504 704 1568 704 1568 768 768 768 768 912 864 912 estringouts.estringouts#902.SLNK
w 1170 691 100 0 n#692 estringouts.estringouts#904.FLNK 1120 640 1152 640 1152 688 1248 688 estringouts.estringouts#903.SLNK
w 2370 1027 100 0 n#676 efanouts.efanouts#691.LNK2 2304 1024 2496 1024 flamLinkEnable.flamLinkEnable#664.SLINK
w 1138 1059 100 0 n#675 estringouts.estringouts#901.FLNK 1504 992 1568 992 1568 1056 768 1056 768 1200 864 1200 estringouts.estringouts#899.SLNK
w 1170 2419 100 0 n#673 estringouts.estringouts#891.FLNK 1120 2368 1152 2368 1152 2416 1248 2416 estringouts.estringouts#888.SLNK
w 1138 2211 100 0 n#671 estringouts.estringouts#892.FLNK 1504 2144 1568 2144 1568 2208 768 2208 768 2352 864 2352 estringouts.estringouts#891.SLNK
w 1138 1923 100 0 n#670 estringouts.estringouts#894.FLNK 1504 1856 1568 1856 1568 1920 768 1920 768 2064 864 2064 estringouts.estringouts#893.SLNK
w 1170 1843 100 0 n#669 estringouts.estringouts#896.FLNK 1120 1792 1152 1792 1152 1840 1248 1840 estringouts.estringouts#894.SLNK
w 1138 1635 100 0 n#668 estringouts.estringouts#895.FLNK 1504 1568 1568 1568 1568 1632 768 1632 768 1776 864 1776 estringouts.estringouts#896.SLNK
w 1170 1555 100 0 n#667 estringouts.estringouts#898.FLNK 1120 1504 1152 1504 1152 1552 1248 1552 estringouts.estringouts#895.SLNK
w 1138 1347 100 0 n#666 estringouts.estringouts#897.FLNK 1504 1280 1568 1280 1568 1344 768 1344 768 1488 864 1488 estringouts.estringouts#898.SLNK
w 1170 1267 100 0 n#665 estringouts.estringouts#899.FLNK 1120 1216 1152 1216 1152 1264 1248 1264 estringouts.estringouts#897.SLNK
s -144 1472 100 0 executive
s 2368 1696 100 0 executive
s 3584 160 210 0 flam_boot_02.sch 12/20/2001 11:56 AM EST
s -144 2048 120 0 Annex
s -144 1088 120 0 ufgmotord
s -144 1856 120 0 Vacuum
s -144 2176 120 0 command mode
s -144 1280 120 0 ufgls218
s -144 1216 120 0 ufgls340d
s -144 1344 120 0 ufgdhsd
s -144 1792 120 0 Indexors
s -144 1536 120 0 All Agents
s -144 1600 120 0 MCE 4
s -144 1408 120 0 ufacqframed
s -144 1152 120 0 ufg354vacd
s -144 1664 120 0 ppcVME
s -144 1728 120 0 Cryocooler
s -144 1984 120 0 Lakeshore 218
s -144 2112 120 0 All Devices
s -144 1920 120 0 Lakeshore 340
s 2368 2016 100 0 sim mode
s 2368 1984 100 0 socket number
s -144 1024 120 0 ufgmce4d
s 2368 1952 100 0 Annex
s 2368 1504 100 0 ufgmotord
s 2368 1856 100 0 Vacuum
s 2368 1600 100 0 ufgls218
s 2368 1568 100 0 ufgls340d
s 2368 1632 100 0 ufgdhsd
s 2368 1824 100 0 Indexors
s 2368 1728 100 0 MCE 4
s 2368 1664 100 0 ufacqframed
s 2368 1536 100 0 ufg354vacd
s 2368 1760 100 0 ppcVME
s 2368 1792 100 0 Cryocooler
s 2368 1920 100 0 Lakeshore 218
s 2368 1888 100 0 Lakeshore 340
s 2368 1472 100 0 ufgmce4d
[cell use]
use estringouts 1248 327 100 0 estringouts#1198
xform 0 1376 400
p 1264 288 100 0 1 OMSL:closed_loop
p 1264 320 100 768 -1 name:$(exec)dev17W
use estringouts 864 551 100 0 estringouts#904
xform 0 992 624
p 880 512 100 0 1 OMSL:closed_loop
p 880 544 100 768 -1 name:$(exec)dev16W
use estringouts 1248 615 100 0 estringouts#903
xform 0 1376 688
p 1264 576 100 0 1 OMSL:closed_loop
p 1264 608 100 768 -1 name:$(exec)dev15W
use estringouts 864 839 100 0 estringouts#902
xform 0 992 912
p 880 800 100 0 1 OMSL:closed_loop
p 880 832 100 768 -1 name:$(exec)dev14W
use estringouts 1248 903 100 0 estringouts#901
xform 0 1376 976
p 1264 864 100 0 1 OMSL:closed_loop
p 1264 896 100 768 -1 name:$(exec)dev13W
use estringouts 2336 2215 100 0 estringouts#900
xform 0 2464 2288
p 2352 2160 100 0 1 OMSL:closed_loop
p 2352 2192 100 768 -1 name:$(exec)dev1W
use estringouts 864 1127 100 0 estringouts#899
xform 0 992 1200
p 880 1088 100 0 1 OMSL:closed_loop
p 880 1120 100 768 -1 name:$(exec)dev12W
use estringouts 864 1415 100 0 estringouts#898
xform 0 992 1488
p 880 1376 100 0 1 OMSL:closed_loop
p 880 1408 100 768 -1 name:$(exec)dev10W
use estringouts 1248 1191 100 0 estringouts#897
xform 0 1376 1264
p 1264 1152 100 0 1 OMSL:closed_loop
p 1264 1184 100 768 -1 name:$(exec)dev11W
use estringouts 864 1703 100 0 estringouts#896
xform 0 992 1776
p 880 1664 100 0 1 OMSL:closed_loop
p 880 1696 100 768 -1 name:$(exec)dev8W
use estringouts 1248 1479 100 0 estringouts#895
xform 0 1376 1552
p 1264 1440 100 0 1 OMSL:closed_loop
p 1264 1472 100 768 -1 name:$(exec)dev9W
use estringouts 1248 1767 100 0 estringouts#894
xform 0 1376 1840
p 1264 1728 100 0 1 OMSL:closed_loop
p 1264 1760 100 768 -1 name:$(exec)dev7W
use estringouts 864 1991 100 0 estringouts#893
xform 0 992 2064
p 880 1952 100 0 1 OMSL:closed_loop
p 880 1984 100 768 -1 name:$(exec)dev6W
use estringouts 1248 2055 100 0 estringouts#892
xform 0 1376 2128
p 1264 2016 100 0 1 OMSL:closed_loop
p 1264 2048 100 768 -1 name:$(exec)dev5W
use estringouts 864 2279 100 0 estringouts#891
xform 0 992 2352
p 880 2240 100 0 1 OMSL:closed_loop
p 880 2272 100 768 -1 name:$(exec)dev4W
use estringouts 1888 2215 100 0 estringouts#889
xform 0 2016 2288
p 1904 2160 100 0 1 OMSL:closed_loop
p 1904 2192 100 768 -1 name:$(exec)dev2W
use estringouts 1248 2343 100 0 estringouts#888
xform 0 1376 2416
p 1264 2304 100 0 1 OMSL:closed_loop
p 1264 2336 100 768 -1 name:$(exec)dev3W
use estringouts 864 263 100 0 estringouts#1401
xform 0 992 336
p 880 224 100 0 1 OMSL:closed_loop
p 880 256 100 768 -1 name:$(exec)dev18W
use hwout 2880 567 100 0 hwout#1361
xform 0 2976 608
p 2976 599 100 0 -1 val(outp):
use elongouts 1952 2471 100 0 elongouts#703
xform 0 2080 2560
p 1984 2432 100 0 1 OMSL:closed_loop
p 1984 2464 100 768 -1 name:$(exec)CmodeW
use elongouts 2496 551 100 0 elongouts#1358
xform 0 2624 640
p 2544 512 100 0 1 OMSL:closed_loop
p 2544 544 100 768 -1 name:$(exec)heartbeatOut
p 2752 608 75 768 -1 pproc(OUT):PP
use elongins 2080 551 100 0 elongins#1357
xform 0 2208 624
p 2192 544 100 1024 -1 name:$(exec)heartbeat
use bd200tr -544 -344 -100 0 frame
xform 0 2096 1360
use ecars 2656 1671 100 0 ecars#1356
xform 0 2816 1840
p 2768 1664 100 1024 -1 name:$(exec)rebootC
use egenSubFLAM 2016 1223 100 0 egenSubFLAM#730
xform 0 2160 1648
p 1793 997 100 0 0 FTA:LONG
p 1793 997 100 0 0 FTB:STRING
p 1793 965 100 0 0 FTC:STRING
p 1793 933 100 0 0 FTD:STRING
p 1793 901 100 0 0 FTE:STRING
p 1793 837 100 0 0 FTF:STRING
p 1793 837 100 0 0 FTG:STRING
p 1793 805 100 0 0 FTH:STRING
p 1793 773 100 0 0 FTI:STRING
p 1793 741 100 0 0 FTJ:STRING
p 1793 741 100 0 0 FTK:STRING
p 1793 741 100 0 0 FTL:STRING
p 1793 741 100 0 0 FTM:STRING
p 1793 741 100 0 0 FTN:STRING
p 1793 741 100 0 0 FTO:STRING
p 1793 741 100 0 0 FTP:STRING
p 1793 741 100 0 0 FTQ:STRING
p 1793 741 100 0 0 FTR:STRING
p 1793 741 100 0 0 FTS:STRING
p 1793 741 100 0 0 FTT:STRING
p 1793 741 100 0 0 FTU:STRING
p 1793 997 100 0 0 FTVA:LONG
p 1793 997 100 0 0 FTVB:LONG
p 1793 965 100 0 0 FTVC:STRING
p 1793 933 100 0 0 FTVD:STRING
p 1793 901 100 0 0 FTVE:STRING
p 1793 837 100 0 0 FTVF:STRING
p 1793 837 100 0 0 FTVG:STRING
p 1793 805 100 0 0 FTVH:STRING
p 1793 773 100 0 0 FTVI:STRING
p 1793 741 100 0 0 FTVJ:STRING
p 1793 741 100 0 0 FTVK:STRING
p 1793 741 100 0 0 FTVL:STRING
p 1793 741 100 0 0 FTVM:STRING
p 1793 741 100 0 0 FTVN:STRING
p 1793 741 100 0 0 FTVO:STRING
p 1793 741 100 0 0 FTVP:STRING
p 1793 741 100 0 0 FTVQ:STRING
p 1793 741 100 0 0 FTVR:STRING
p 1793 741 100 0 0 FTVS:STRING
p 1793 741 100 0 0 FTVT:STRING
p 1793 741 100 0 0 FTVU:STRING
p 1728 1630 100 0 0 INAM:ufrebootGinit
p 1728 1598 100 0 0 SNAM:ufrebootGproc
p 2128 1216 100 1024 -1 name:$(exec)rebootG
p 2304 1962 75 0 -1 pproc(OUTC):PP
p 2304 1930 75 0 -1 pproc(OUTD):PP
p 2304 1898 75 0 -1 pproc(OUTE):PP
p 2304 1866 75 0 -1 pproc(OUTF):PP
p 2304 1834 75 0 -1 pproc(OUTG):PP
p 2304 1802 75 0 -1 pproc(OUTH):PP
p 2304 1770 75 0 -1 pproc(OUTI):PP
p 2304 1738 75 0 -1 pproc(OUTJ):PP
p 2304 1706 75 0 -1 pproc(OUTK):PP
p 2304 1674 75 0 -1 pproc(OUTL):PP
p 2304 1642 75 0 -1 pproc(OUTM):PP
p 2304 1610 75 0 -1 pproc(OUTN):PP
p 2304 1578 75 0 -1 pproc(OUTO):PP
p 2304 1546 75 0 -1 pproc(OUTP):PP
p 2304 1514 75 0 -1 pproc(OUTQ):PP
p 2304 1482 75 0 -1 pproc(OUTR):PP
p 2304 1450 75 0 -1 pproc(OUTS):PP
p 2304 1418 75 0 -1 pproc(OUTT):PP
use efanouts 2064 839 100 0 efanouts#691
xform 0 2184 992
p 2176 832 100 1024 -1 name:$(exec)Fanout
use flamLinkEnable 2496 935 100 0 flamLinkEnable#664
xform 0 2656 1056
p 2496 928 100 0 1 setSystem:system $(exec)eg
use ecad20 64 647 -100 0 $(top)$(dev)obsControl
xform 0 224 1536
p 160 2272 100 0 0 DESC:Observation Control CAD
p 160 2112 100 0 0 FTVA:LONG
p 160 1248 100 0 0 SNAM:btrebootCommand
p 176 640 100 1024 -1 name:$(exec)reboot
p 384 746 75 0 -1 pproc(STLK):PP
[comments]
RCS: "$Name:  $ $Id: flamBoot.sch,v 0.0 2005/09/01 20:20:10 drashkin Exp $"
