[schematic2]
uniq 97
[tools]
[detail]
w 24 1131 100 0 n#96 hwin.hwin#95.in 0 1120 96 1120 ecad2.ecad2#48.INPB
w 112 1547 -100 0 c#89 ecad2.ecad2#48.DIR 96 1408 32 1408 32 1536 240 1536 240 1856 128 1856 inhier.DIR.P
w 80 1579 -100 0 c#90 ecad2.ecad2#48.ICID 96 1376 0 1376 0 1568 208 1568 208 1728 128 1728 inhier.ICID.P
w 352 1547 -100 0 c#91 ecad2.ecad2#48.VAL 416 1408 480 1408 480 1536 272 1536 272 1856 416 1856 outhier.VAL.p
w 384 1579 -100 0 c#92 ecad2.ecad2#48.MESS 416 1376 512 1376 512 1568 304 1568 304 1728 416 1728 outhier.MESS.p
w 1564 523 100 0 n#83 flamSubSysCommand.flamSubSysCommand#57.CAR_IVAL 1472 416 1568 416 1568 640 1728 640 flamSubSysCombine.flamSubSysCombine#77.DC_VAL
w 1628 459 100 0 n#82 flamSubSysCommand.flamSubSysCommand#57.CAR_IMSS 1472 352 1632 352 1632 576 1728 576 flamSubSysCombine.flamSubSysCombine#77.DC_MESS
w 1624 707 100 0 n#81 flamSubSysCommand.flamSubSysCommand#56.CAR_IMSS 1472 800 1568 800 1568 704 1728 704 flamSubSysCombine.flamSubSysCombine#77.CC_MESS
w 1528 867 100 0 n#80 flamSubSysCommand.flamSubSysCommand#56.CAR_IVAL 1472 864 1632 864 1632 768 1728 768 flamSubSysCombine.flamSubSysCombine#77.CC_VAL
w 2024 651 100 0 n#93 flamSubSysCombine.flamSubSysCombine#77.CAR_IMSS 1984 640 2112 640 ecars.ecars#53.IMSS
w 2024 715 100 0 n#78 flamSubSysCombine.flamSubSysCombine#77.CAR_IVAL 1984 704 2112 704 ecars.ecars#53.IVAL
w 904 923 100 0 n#76 efanouts.efanouts#13.LNK4 832 912 1024 912 1024 320 1184 320 flamSubSysCommand.flamSubSysCommand#57.START
w 936 987 100 0 n#71 efanouts.efanouts#13.LNK2 832 976 1088 976 1088 1328 1344 1328 estringouts.estringouts#68.SLNK
w 1592 1323 100 0 n#59 estringouts.estringouts#68.OUT 1600 1312 1632 1312 hwout.hwout#60.outp
w 936 955 100 0 n#52 efanouts.efanouts#13.LNK3 832 944 1088 944 1088 768 1184 768 flamSubSysCommand.flamSubSysCommand#56.START
w 1160 1547 100 0 n#94 efanouts.efanouts#13.LNK1 832 1008 1024 1008 1024 1536 1344 1536 elongouts.elongouts#14.SLNK
w 480 939 100 0 n#49 ecad2.ecad2#48.STLK 416 928 592 928 efanouts.efanouts#13.SLNK
w 1592 1515 100 0 n#19 elongouts.elongouts#14.OUT 1600 1504 1632 1504 hwout.hwout#18.outp
w 1272 1579 100 0 n#17 hwin.hwin#16.in 1248 1632 1248 1568 1344 1568 elongouts.elongouts#14.DOL
s 2624 2032 100 1792 2000/12/05
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 flamTest.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/05
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos test Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use hwin 1056 1591 100 0 hwin#16
xform 0 1152 1632
p 896 1632 100 0 -1 val(in):$(CAD_MARK)
use hwin -192 1079 100 0 hwin#95
xform 0 -96 1120
p -352 1120 100 0 -1 val(in):$(top)state
use outhier 432 1856 100 0 VAL
xform 0 400 1856
use outhier 432 1728 100 0 MESS
xform 0 400 1728
use inhier 64 1856 100 0 DIR
xform 0 128 1856
use inhier 48 1728 100 0 ICID
xform 0 128 1728
use flamSubSysCombine 1728 487 100 0 flamSubSysCombine#77
xform 0 1856 672
p 1728 352 100 0 1 setCmd:cmd test
use estringouts 1344 1255 100 0 estringouts#68
xform 0 1472 1328
p 1424 1216 100 0 0 OMSL:supervisory
p 1408 1216 100 0 1 VAL:TEST
p 1536 1248 100 1024 1 name:$(top)testDcCmd
use hwout 1632 1463 100 0 hwout#18
xform 0 1728 1504
p 1856 1504 100 0 -1 val(outp):$(top)cc:test.DIR PP NMS
use hwout 1632 1271 100 0 hwout#60
xform 0 1728 1312
p 1856 1312 100 0 -1 val(outp):$(top)dc:acqControl.A
use elongouts 1344 1447 100 0 elongouts#14
xform 0 1472 1536
p 1408 1408 100 0 0 OMSL:supervisory
p 1536 1440 100 1024 1 name:$(top)testCcMark
p 1600 1504 75 768 -1 pproc(OUT):PP
use flamSubSysCommand 1184 583 100 0 flamSubSysCommand#56
xform 0 1328 768
p 1184 544 100 0 1 setCommand:cmd test
p 1184 576 100 0 1 setSystem:sys cc
use flamSubSysCommand 1184 135 100 0 flamSubSysCommand#57
xform 0 1328 320
p 1184 96 100 0 1 setCommand:cmd test
p 1184 128 100 0 1 setSystem:sys dc
use ecars 2112 423 100 0 ecars#53
xform 0 2272 592
p 2272 416 100 1024 1 name:$(top)testC
use ecad2 96 839 100 0 ecad2#48
xform 0 256 1152
p 160 800 100 0 1 INAM:flamIsNullInit
p 160 752 100 0 1 SNAM:flamIsTestProcess
p 256 832 100 1024 1 name:$(top)test
p 432 928 75 1024 -1 pproc(STLK):PP
use efanouts 592 791 100 0 efanouts#13
xform 0 712 944
p 736 784 100 1024 1 name:$(top)testFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamTest.sch,v 0.0 2005/09/01 20:20:10 drashkin Exp $
