#if !defined(__uffits_cc__)
#define __uffits_cc__ "$Name:  $ $Id: uffits.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFFITSClient.h"

int main(int argc, char** argv, char** envp) {
  string name = "uffits";
  return UFFITSClient::main(name, argc, argv, envp);
}

#endif // __uffits_cc__
