#if !defined(__UFDHSwrap_h__)
#define __UFDHSwrap_h__ "$Name:  $ $Id: UFDHSwrap.h,v 0.23 2003/09/12 19:45:12 hon Exp $"
#define __UFDHSwrap_H__(arg) const char arg##DHSwrap_h__rcsId[] = __UFDHSwrap_h__;

// sys:
#include "unistd.h"
#include "stdio.h"
#include "stdlib.h"
#include "signal.h"
#include "dirent.h"

// DHS:
#include "sds.h"
#include "gen_types.h"
#include "dhs.h"


// c++ stdlib:
#include "string"
#include "map"
#include "vector"

using namespace std;

// this should be used for all DHS runtime interactions
// to support both quicklook and permanent archiving.
// in principle one instance of this (ala singleton)
// class could support all DHS streams in an application.
// since the DHS runtime only allows one application
// at a time to use it...
// also, since the DHS runtime is threaded, this class
// should support thread safe activities. however, there
// are some issues with even the SGI STL, which must be
// compiled with -D_PTHREADS, to insure some level of STL
// reentrancy. to support pthreads, some (the pthread)
// functions in this class must be static. any attributes used
// by these functions must be static, or must have
// a static accessor, or be provided with the class instance ...
// nevertheless there are reentrancy issues with the STL that
// i have not had the time to fully sort out. --hon
class UFDHSwrap {
public:
  enum { All= 0, Initial= 1, Final= 2, Extension= 3 }; // FITS header/attribute types

  struct Stream {
    string _name, _dataLabel;
    DHS_BD_DATASET _dataSet;
    DHS_BD_FRAME _dataFrame;
    int* _pDhsData;
    inline Stream() : _dataSet(DHS_BD_DATASET_NULL), _dataFrame(DHS_BD_FRAME_NULL), _pDhsData(0) {};
    inline Stream(const string& label) : _name(label), _dataLabel(label), _dataSet(DHS_BD_DATASET_NULL),
	                                 _dataFrame(DHS_BD_FRAME_NULL), _pDhsData(0) {};
    inline Stream(const string& label, DHS_BD_DATASET dataset) : _name(label), _dataLabel(label), _dataSet(dataset),
                                                                 _dataFrame(DHS_BD_FRAME_NULL), _pDhsData(0) {};
  };

  UFDHSwrap(const string& instrum= "TReCS", int w= 320, int h= 240);
  inline virtual ~UFDHSwrap() {}

  inline int width() { return _width; }
  inline int height() { return _height; }

  // open connection to DHS, allow sub-classe to override this virtual
  virtual DHS_STATUS open(const string& serverHost, const string& service= "dataServerSS");

  // close connection to DHS
  DHS_STATUS close();  
  DHS_STATUS reconnect(int tryCnt);

  // scan an 'internal' fitsHdr string array, assume that (most) keys that end in '2' are
  // final frame values (note ctor), all others are initial values -- kinda kludgy OSCIR/TReCS --
  // or reset _finalKeyExt attribute.
  // specific logic here. each string should be a FITS 'card', i.e. char[80]
  static DHS_STATUS setAttributes(const vector< const string* >& fitsHdr, DHS_AV_LIST& av,
			          int hdrtyp= UFDHSwrap::All);
  static DHS_STATUS resetAttributes(DHS_AV_LIST& av, map< string, string >& svals,
			            map< string, int >& ivals, map< string, float >& fvals);

  // dhs data "objects"
  inline void freeDataset(DHS_BD_DATASET& dataSet) { ::dhsBdDsFree(dataSet, &_status); dataSet = DHS_BD_DATASET_NULL; }
  inline void freeStream(UFDHSwrap::Stream* s) {
    if(s->_dataSet != DHS_BD_DATASET_NULL) ::dhsBdDsFree(s->_dataSet, &_status);
    delete s;
  }

  static string newlabel();

  // create a new dataset, setting minimal initial attributes 
  static DHS_STATUS newDataset(string& dataLabel, DHS_BD_DATASET& dataSet, const string* quicklook= 0);

  // Note vector< string* > rather that value or reference. this is due
  // to the likelyhood that the application will use dynamic FITS headers,
  // e.g. UF headers are created off of socket connections, etc...
  // create a new dataset, setting all initial value attributes , this does not really seem to work?
  DHS_STATUS newDataset(const vector< const string* >& fitsHdr, string& dataLabel,
			       DHS_BD_DATASET& dataSet, const string* quicklook= 0);

  // create final frame/dataset, setting all final value attributes, this also
  // seems dubious 
  DHS_STATUS finalDataset(const vector< const string* >& fitsHdr, string& dataLabel,
			         DHS_BD_DATASET& dataSet, const string* quicklook= 0);

  // create new frame for new dataset with minimal (mandatory?) attributes
  // return ptr to DHS lib. data/frame allocation buffer
  static int* newFrame(DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		       const map< string, int >& extradims,
		       int index= 0, int total= 1, const string* quicklook= 0);

  // create new frame for existing dataset datalabel
  // return ptr to DHS lib. data/frame allocation buffer
  static int* newFrame(string& dataLabel, DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		       int index= 0, int total= 1, const string* quicklook= 0);
  static int* newFrame(string& dataLabel, DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		       const map< string, int >& extradims,
		       int index= 0, int total= 1, const string* quicklook= 0);

  // create new frame for existing dataset, setting all primary header initial 
  // and/or final value attributes, along with any extension header attributes.
  // index & total are compared to decide which attributes to set; this seems
  // to be the only way it works (the server seems to like setting the instrument
  // attributes only after at least one frame is allocated into the dataset?). 
  // return ptr to DHS lib. data/frame allocation buffer
  static int* newFrame(const vector< const string* >& fitsPrmHdr,
                       const vector< const string* >& fitsExtHdr,
                       string& dataLabel, 
		       DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		       const map< string, int >& extradims,
		       int index= 0, int total= 1, const string* quicklook= 0);

  // create new frame for existing dataset, setting extennsion header attributes,
  // return ptr to DHS lib. data/frame allocation buffer
  static int* newFrame(const vector< const string* >& fitsExtHdr, string& dataLabel, 
		       DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		       const map< string, int >& extradims,
		       int index= 0, int total= 1, const string* quicklook= 0);

  static string obsInfoFrom(const vector< const string* >& fitsHdr, const string& observer, const string& target);

  // send dataset to dhs, using copy from acq. buff into dhs library's pDhsData buff...
  // could make pDhsData a class attribute (well 1 for archv and 14 for qlook) rather than arg.,
  // if the dsh newFrame func. supported user-supplied data buffer this copy could be avoided.
  DHS_STATUS putArchiveFrame(const int* data, int elements, const string& dataLabel,
			     DHS_BD_DATASET& dataSet, int* pDhsData,
		             int index= 0, bool finalframe= false);
  // when data (source) is collected in non-contiguous images 
  // (although destination, pDhsData, is unavoidably contiguous)... 
  DHS_STATUS putArchiveFrame(const vector< int* > data, int elements, const string& dataLabel,
			     DHS_BD_DATASET& dataSet, int* pDhsData,
		             int index= 0, bool finalframe= false);
  // since frame may have NAXIS > 2 there may be more than one image to put.
  // the offsets vec. indicates the number of images in the contiguous data frame
  // buffer and each image's offset position in the data buffer.
  DHS_STATUS putQlookFrame(const int* data, int elements, const vector< int >& offsets,
			   const string& dataLabel, DHS_BD_DATASET& dataset, int* pDhsData,
		           int index= 0, bool finalframe= false);
  // one image
  DHS_STATUS putQlookFrame(const int* data, int elements,
			   const string& dataLabel, DHS_BD_DATASET& dataset, int* pDhsData,
		           int index= 0, bool finalframe= false);

  // get image data from DHS Archive (quicklook streams are not available via the network)
  // unsigned char* should point to contiguous fitshdr+data buffer  
  unsigned char* getData(const string& name, int& size, int tryCnt= 1, bool hdrOnly= false);  

  // convenience funcs.
  static int setQlNames(const vector< string >& qlnames);
  static int setQlNames(const string& instrum, const vector< string >& qlnames);

  // decode header element provided as const string& 
  static int decodeFITS(const string& hdr,
		        map< string, string >& svals, 
		        map< string, int >& ivals,
		        map< string, float >& fvals,
		        map< string, string >& comments, bool append= true);

  // decode (all) header elements provided as vector< const string* >
  static int decodeFITS(const vector< const string* >& hdr,
		        map< string, string >& svals, 
		        map< string, int >& ivals,
		        map< string, float >& fvals,
		        map< string, string >& comments);

  // decode (only elements that lack_finalKeyExt) header
  static int decodeInitialFITS(const vector< const string* >& hdr,
		        map< string, string >& svals, 
		        map< string, int >& ivals,
		        map< string, float >& fvals,
		        map< string, string >& comments);

  // final value key-names are assumed to end in _finalKeyExt;
  // decode only elements that end with _finalKeyExt
  static int decodeFinalFITS(const vector< const string* >& hdr,
		             map< string, string >& svals, 
		             map< string, int >& ivals,
		             map< string, float >& fvals,
		             map< string, string >& comments);

  // archive dataLabel should be provided (operationally) by epics caget
  // of db rec. set by OCS/Sequencer
  inline string getArchvLabel() { return _archvDataLabel; }
  inline void setArchvLabel(const string& label) { _archvDataLabel = label; }
  // set the label by accessing the epics DB channel
  void setArchvLabel(const string& label, const string& epicsChan);

  void setArchvFrmCnt(int frmcnt, int total, const string& epicsChan= "");
  int getArchvFrmCnt(const string& epicsChan= "");

  void setQlookFrmCnt(int frmcnt, const string& qname= "");
  int getQlookFrmCnt(const string& qname= "");

  // make these public -- i'm getting tired of writing accessor & mutators...
  static bool _verbose, _wait;
  // write "libdd.config" (sort of) formatted attribute list to stdout
  static bool _writeDD;
  // skip keywords not found in libdd.config
  static bool _skip;

  static DHS_BD_FRAME _dataFrame0; // try making all frames after the 1st subframes of it?

  inline static DHS_STATUS abort(const string& datalabel) { 
    ::dhsBdCtl(_connection, DHS_BD_CTL_ABORT, datalabel.c_str(), &_status);
    return _status;
  }

protected:
  // all statics for now, since all class functions are static...
  static int _width, _height;
  static string _instrum, _obsinfo, _serverIp, _service;

  // potentially used by other dhs callbacks?
  static DHS_STATUS _status;
  static DHS_CONNECT _connection;
  static int _archvFrmCnt, _totalArchvFrms, _qlookFrmCnt;
  static string _archvDataLabel, _finalKeyExt;
  static vector< string > _QlookNames;
  static map< string, int > _QlookFrmCnt;
  static map< string, UFDHSwrap::Stream* > _QlookStrms; // for all q-look streams

  // helpers
  static map< string, string > _DataTypes; // for FITS datatypes
  static DHS_DATA_TYPE UFDHSwrap::_dataType(const string& val, const string& instrum);
  static int UFDHSwrap::_initDataTypes(const string& instrum);
  
  static DHS_STATUS _putArchiveFrame(const string& dataLabel,
                	             DHS_BD_DATASET& dataSet, int* pDhsData,
			             int index, bool finalframe); 

  // error callback:
  static void _dhsError(DHS_CONNECT connect, DHS_STATUS errorNum, DHS_ERR_LEVEL errorLev,
			char *msg, DHS_TAG tag, void *userData);

  // exclusively for the _get callback:
  static void* _DataSetRaw;
  static unsigned char* _DataSetFITS;
  static int _DataSize;
  static void _getDataSetFITS(DHS_CONNECT connect, DHS_TAG tag, char *label,
					 DHS_BD_GET_TYPE type, DHS_CMD_STATUS cmdStatus,
					 char *string, DHS_AV_LIST avList, void *data,
					 unsigned long length, void *userDataFunc);
};

#endif // __UFDHSwrap_h__
