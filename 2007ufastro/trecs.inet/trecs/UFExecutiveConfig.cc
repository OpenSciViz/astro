#if !defined(__UFExecutiveConfig_cc__)
#define __UFExecutiveConfig_cc__ "$Name:  $ $Id: UFExecutiveConfig.cc 14 2008-06-11 01:49:45Z hon $"

const char rcsId[] = __UFExecutiveConfig_cc__;

#include "UFExecutiveConfig.h"
#include "UFTReCSExecutiveAgent.h"
#include "UFFITSheader.h"

#include "stdio.h"
#include "ctype.h"

UFExecutiveConfig::UFExecutiveConfig(const string& name) : UFDeviceConfig(name) {}

UFExecutiveConfig::UFExecutiveConfig(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// Executive terminator new-line:
//string UFExecutiveConfig::terminator() { return "\n"; }

// Executive prefix is none:
//string UFExecutiveConfig::prefix() { return ""; }

string UFExecutiveConfig::simAgents(const string& agntargs) {
  string nil = "";
  char* uf = getenv("UFINSTALL");
  if( uf ==  0 )
    return nil;

  string script = uf;
  script += "/bin/ufsimagents "; // default script  behavior is to start all agents except executive
  script += agntargs;
  /*
  if( agnt != "" && 
      agnt.find("all") == string::npos && agnt.find("All") == string::npos && agnt.find("ALL") == string::npos )
    script += agnt; // start only specified agent? in simulation mode
    else
    script += "-ex"; // start all but trecs executive in simulation mode
  */
  FILE* fp = ::popen(script.c_str(), "r");
  if( fp == 0 )
    return nil;

  string res= "";
  char buf[BUFSIZ]; memset(buf, 0, BUFSIZ);
  while( ::fgets(buf, BUFSIZ, fp) != 0 ) {
    res += buf;
    memset(buf, 0, BUFSIZ);
  }
  ::pclose(fp);

  if( _verbose ) 
    clog<<"UFExecutiveConfig::simAgents> "<<res<<endl;

  return res;
}

string UFExecutiveConfig::startAgents(const string& agntargs) {
  string nil = "";
  char* uf = getenv("UFINSTALL");
  if( uf ==  0 )
    return nil;

  string script = uf;
  script += "/bin/ufstartagents "; // default script  behavior is to start all agents except executive
  script += agntargs;
  /*
  if( agnt != "" && 
      agnt.find("all") == string::npos && agnt.find("All") == string::npos && agnt.find("ALL") == string::npos )
    script += agnt; // start only specified agent
  else
  script += "-ex"; // start all but trecs executive
  */
  FILE* fp = ::popen(script.c_str(), "r");
  if( fp == 0 )
    return nil;

  string res= "";
  char buf[BUFSIZ]; memset(buf, 0, BUFSIZ);
  while( ::fgets(buf, BUFSIZ, fp) != 0 ) {
    res += buf;
    memset(buf, 0, BUFSIZ);
  }
  ::pclose(fp);

  if( _verbose ) 
    clog<<"UFExecutiveConfig::startAgents> "<<res<<endl;

  return res;
}

string UFExecutiveConfig::stopAgents(const string& agnt) {
  string nil = "";
  char* uf = getenv("UFINSTALL");
  if( uf ==  0 )
    return nil;

  string script = uf;
  script += "/bin/ufstopagents "; // default script  behavior is to stop all agents except executive
  if( agnt != "" && 
      agnt.find("all") == string::npos && agnt.find("All") == string::npos && agnt.find("ALL") == string::npos )
    script += agnt; // stop only specified agent
  //  else
  //script += "-ex"; // stop all but trecs executive
 
  clog<<"UFExecutiveConfig::stopAgents> script: "<<script<<endl;
  FILE* fp = ::popen(script.c_str(), "r");
  if( fp == 0 )
    return nil;

  string res= "";
  char buf[BUFSIZ]; memset(buf, 0, BUFSIZ);
  while( ::fgets(buf, BUFSIZ, fp) != 0 ) {
    res += buf;
    memset(buf, 0, BUFSIZ);
  }
  ::pclose(fp);

  clog<<"UFExecutiveConfig::stopAgents> res: "<<res<<endl;
  return res;
}

UFTermServ* UFExecutiveConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    _devIO = static_cast<UFTermServ*> (UFBaytech::create(host, port));
  }
  return _devIO;
}

vector< string >& UFExecutiveConfig::queryCmds() {
  _queries.push_back("history"); _queries.push_back("History"); _queries.push_back("HISTORY");
  _queries.push_back("powerstatus"); _queries.push_back("PowerStatus"); _queries.push_back("POWERSTATUS");
  return _queries;
}

vector< string >& UFExecutiveConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("shutdown"); _actions.push_back("ShutDown"); _actions.push_back("SHUTDOWN");
  _actions.push_back("poweron"); _actions.push_back("PowerOn"); _actions.push_back("POWERON");
  _actions.push_back("poweroff"); _actions.push_back("PowerOff"); _actions.push_back("POWEROFF");
  _actions.push_back("simagent"); _actions.push_back("SimAgent"); _actions.push_back("SIMAGENT");
  _actions.push_back("startagent"); _actions.push_back("StartAgent"); _actions.push_back("STARTAGENT");
  _actions.push_back("stopagent"); _actions.push_back("StopAgent"); _actions.push_back("STOPAGENT");

  return _actions;
}

int UFExecutiveConfig::validCmd(const string& c) {
  // allow psuedo querry in the form of a numeric (history cnt)
  const char* cc =  c.c_str();
  if( isdigit(*cc) != 0 )
    return 0;    

  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos ) {
      return 1;
    }
  }
  
  vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( c.find(qv[i++]) == 0 )
      return 1;
  }

  clog<<"UFExecutiveConfig::validCmd> ?Bad cmd: "<<c<<endl; 
  return -1;
}

int UFExecutiveConfig::validCmd(const string& name, const string& impl) {
  // if it's a valid command, check its parameter option-mode / implementation
  int n = validCmd(name);
  if( n < 0 ) 
    return n;

  string cmdname = name;
  if( cmdname.find("shut") != string::npos || cmdname.find("Shut") != string::npos || cmdname.find("SHUT") != string::npos )
    cmdname = "ShutDown";
  if( cmdname.find("on") != string::npos || cmdname.find("On") != string::npos || cmdname.find("ON") != string::npos )
    cmdname = "PowerOn";
  if( cmdname.find("off") != string::npos || cmdname.find("Off") != string::npos || cmdname.find("OFF") != string::npos)
    cmdname = "PowerOff";
  if( cmdname.find("stat") != string::npos || cmdname.find("Stat") != string::npos || 
      cmdname.find("STAT") != string::npos )
    cmdname = "PowerStat";
  if( cmdname.find("simta") != string::npos || cmdname.find("Sima") != string::npos || 
      cmdname.find("SIMA") != string::npos )
    cmdname = "SimAgent";
  if( cmdname.find("starta") != string::npos || cmdname.find("Starta") != string::npos || 
      cmdname.find("STARTA") != string::npos )
    cmdname = "StartAgent";

  map< string, vector<string>* > cmdoptions;

  vector< string > power;
  power.push_back("all"); power.push_back("All"); power.push_back("ALL");
  power.push_back("1"); power.push_back("ufperle"); 
  power.push_back("2"); power.push_back("lakeshore218");
  power.push_back("3"); power.push_back("lakeshore340");
  power.push_back("4"); power.push_back("vacuum");
  power.push_back("5"); power.push_back("indexors");
  power.push_back("6"); power.push_back("cryocooler"); power.push_back("coldhead");
  power.push_back("7"); power.push_back("ppcvme");
  power.push_back("8"); power.push_back("mce4");

  vector< string > powerStatus;
  powerStatus.push_back("full"); powerStatus.push_back("Full"); powerStatus.push_back("FULL"); 
  powerStatus.push_back("rmsamps"); powerStatus.push_back("RmsAmps"); powerStatus.push_back("RMSAMPS"); 
  powerStatus.push_back("maxamps"); powerStatus.push_back("MaxAmps"); powerStatus.push_back("MAXAMPS"); 
  powerStatus.push_back("celsius"); powerStatus.push_back("Celsius"); powerStatus.push_back("CELSIUS"); 
  powerStatus.push_back("1"); powerStatus.push_back("ufperle"); 
  powerStatus.push_back("2"); powerStatus.push_back("lakeshore218");
  powerStatus.push_back("3"); powerStatus.push_back("lakeshore340");
  powerStatus.push_back("4"); powerStatus.push_back("vacuum");
  powerStatus.push_back("5"); powerStatus.push_back("indexors");
  powerStatus.push_back("6"); powerStatus.push_back("cryocooler"); powerStatus.push_back("coldhead"); 
  powerStatus.push_back("7"); powerStatus.push_back("ppcvme");
  powerStatus.push_back("8"); powerStatus.push_back("mce4");

  vector< string > agents;
  agents.push_back("all"); agents.push_back("All"); agents.push_back("ALL"); 
  agents.push_back("ufacqframed"); 
  agents.push_back("ufgdhsd"); 
  agents.push_back("ufgls218d"); 
  agents.push_back("ufgls340d");
  agents.push_back("ufg354vacd"); 
  agents.push_back("ufgmotord"); 
  agents.push_back("ufgmce4d"); 

  cmdoptions["PowerOn"] = &power;
  cmdoptions["PowerOff"] = &power;
  cmdoptions["PowerStat"] = &power;

  cmdoptions["SimAgent"] = &agents;
  cmdoptions["StartAgent"] = &agents;
  cmdoptions["StopAgent"] = &agents;

  // now check the cmdimpl against the options
  vector< string >* opt = cmdoptions[cmdname];
  size_t cnt = opt->size();
  for( size_t i = 0; i < cnt; ++i ) {
    string s = (*opt)[i];
    if( impl.find(s) != string::npos ) // found it
      return n;
  }
  return -1; // not found, not allowed
}

UFStrings* UFExecutiveConfig::status(UFDeviceAgent* da) {
  UFTReCSExecutiveAgent* trecs = dynamic_cast< UFTReCSExecutiveAgent* > (da);
  if( trecs->_verbose )
    clog<<"UFExecutiveConfig::status> "<<trecs->name()<<endl;
  string btstat = trecs->status(); 
  // parse this and make a UFStrings...
  vector< string > vals;
  string s;

  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  return new UFStrings(trecs->name(), vals);
}

UFStrings* UFExecutiveConfig::statusFITS(UFDeviceAgent* da) {
  UFTReCSExecutiveAgent* trecs = dynamic_cast< UFTReCSExecutiveAgent* > (da);
  if( trecs->_verbose )
    clog<<"UFExecutiveConfig::statusFITS> "<<trecs->name()<<endl;
  string btstat = trecs->status(); 
  // parse this and make a UFStrings...
  map< string, string > valhash, comments;

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(trecs->name());

  return ufs;
}

int UFExecutiveConfig::chanOf(const string& name) {
  if( name.find("ufperle") != string::npos ||
      name.find("Annex") != string::npos || 
      name.find("ANNEX") != string::npos )
    return 1; 
  if( name.find("lakeshore218") != string::npos ||
      name.find("Lakeshore218") != string::npos || 
      name.find("LakeShore218") != string::npos || 
      name.find("LAKESHORE218") != string::npos )
    return 2; 
  if( name.find("lakeshore340") != string::npos ||
      name.find("Lakeshore340") != string::npos || 
      name.find("LakeShore340") != string::npos || 
      name.find("LAKESHORE340") != string::npos )
    return 3; 
  if( name.find("vacuum") != string::npos ||
      name.find("Vacuum") != string::npos || 
      name.find("VACUUM") != string::npos )
    return 4; 
  if( name.find("indexor") != string::npos ||
      name.find("Indexor") != string::npos || 
      name.find("INDEXOR") != string::npos )
    return 5; 
  if( name.find("cryocool") != string::npos ||
      name.find("Cryocool") != string::npos || 
      name.find("CRYOCOOL") != string::npos ||
      name.find("coldhead") != string::npos ||
      name.find("Coldhead") != string::npos || 
      name.find("COLDHEAD") != string::npos )
    return 6; 
  if( name.find("ppcvme") != string::npos ||
      name.find("PPCVME") != string::npos ) 
    return 7; 
  if( name.find("mce4") != string::npos ||
      name.find("MCE4") != string::npos ) 
    return 8; 

  return -1;
}

#endif // __UFExecutiveConfig_cc__
