#if !defined(__UFGemDHSgent_h__)
#define __UFGemDHSgent_h__ "$Name:  $ $Id: UFGemDHSAgent.h,v 0.2 2002/10/14 20:47:26 hon Exp $"
#define __UFGemDHSgent_H__(arg) const char arg##UFGemDHSgent_h__rcsId[] = __UFGemDHSgent_h__;

#include "iostream.h"
#include "deque"
#include "map"
#include "string"
#include "vector"

#include "UFDeviceAgent.h"
#include "UFTermServ.h"
#include "UFDHSTReCS.h"

class UFGemDHSgent: public UFDeviceAgent, public UFDHSTReCS {
public:
  // instrument name should indicate nature of frame buffs.
  // trecs has 14, flamingos just 2.
  UFGemDHSgent(const string& name, int argc, char** argv, char** envp);
  inline virtual ~UFGemDHSgent() { delete _edtcfg; }

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual void startup();
  virtual UFTermServ* init(); // this is dubious since there is no serial device here...
  virtual int options(string& servlist);
  virtual int query(const string& q, vector<string>& reply);

  // dhs put event loop(s):
  static void dhsThreads(UFGemDHSAgent& dha);
  // start dma event loop(s) in new pthread
  static void* _dhsThreads(void* p= 0);

  //agent::exec event loop:
  static void agentThread(UFGemDHSAgent& dha);
  // start exec event loop in new pthread
  static void* _agentThread(void* p= 0);

  // create & start new dmaThread for each observation
  // abort/stop should kill current dmeTread 
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& replies);
  // check client name for replication service
  virtual string newClient(UFSocket* clsoc);

  // main should start signal handler thread then dhs and agent::exec threads
  static int main(const string& name, int argc, char** argv, char** envp);

protected:
  // handlers
  static void _clearAll();
  static void _sighandler(int sig);
  static void _cancelhandler(void* p= 0);
  static void _postacq(unsigned char* acqbuf, char* fitsbuf, int fits_sz, const char* fitsfilenm);

  static pthread_t _dhsThrdId;
  static pthread_mutex_t _theDhsMutex; // protect dhs connection?

  static string _Instrum; 
  static UFDHSTReCS& _dhs; // reference to self as DHSwrap for use in agent action func?
};

#endif // __UFGemDHSgent_h__
      
