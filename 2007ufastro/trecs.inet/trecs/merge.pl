#!/bin/perl -w
$rscId = '$Name:  $ $Id: merge.pl 14 2008-06-11 01:49:45Z hon $';
$trecsfile = "trecs.fits";
$dhsfile = "dhstrecs.fits";
open D, "<$dhsfile" or die "unable to open $dhsfile\n";
open T, "<$trecsfile" or die "unable to open $trecsfile\n";
$cnt = 0;
while ( <D> ) {
  $cnt++;
  $dhsline = $_;
  chomp $dhsline;
  $trecsline = <T>;
  if( !defined($trecsline) ) { $trecsline = " ";  }
  chomp $trecsline;
  if( $cnt < 10 ) { print "00"; }
  elsif( $cnt < 100 ) { print "0"; }
  if( length($dhsline) > 3 ) { print "$cnt $dhsline \t $trecsline\n"; }
  else { print "$cnt $dhsline    \t $trecsline\n"; }
}
close D;
close T;
exit;
