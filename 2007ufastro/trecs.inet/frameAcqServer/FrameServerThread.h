#if !defined(__FrameServerThread_h__)
#define __FrameServerThread_h__ "$Name:  $ $Id: FrameServerThread.h,v 1.2 2003/07/10 23:54:11 varosi beta $"

/*
 * The clientListenThread accepts TCP/IP socket connections on default port = 52000,
 *  creating a FrameServerThread for each new client connection.
 *
 * Each FrameServerThread handles requests from its client to define/fetch UFLIB objects such as:
 *    UFObsConfig, UFFrameConfig, FITS header (UFStrings), and data frames (UFInts), etc.
 *  When a frame buffer is requested, server returns the buffer's FrameConfig and UFInts frame.
 *  Any FrameServerThread can open/close the FITS file and writes header when received from client.
 *  If a new FITS header is recvd while the FITS file is open, it is written to file before closing.
 *  Any FrameServerThread can start/abort the FrameGrabThread if so commanded by its client.
 *  Any client can request the "NOTIFY" or "REPICATE", thus getting a stream of FrameConfigs or raw
 *   frames, respectively, and the stream can be discontinued by requesting "NONOTIFY" or "NOREPLICA".
 *
 * See below for more info on client<->server transaction directives.
 *
 * Frank Varosi, July 9, 2003.
 */

extern void* FrameServerThread( void* ) ;
extern void* clientListenThread( void* ) ;
extern bool createListenThread( unsigned short int& ServerPort, UFTimeStamp* ServerVersion ) ;
extern void setServerLogs( bool Logging = false ) ;
extern void LogServerMessage( ofstream& serverLogFile, string message, int Loop, int count=0 ) ;
extern vector< string > reportIOstats() ;

// method specialRequest is to enable notification stream or replicated data stream:
extern bool specialRequest( UFProtocol* request, UFSocket* sock, string& name, vector< UFProtocol* >& reply );
extern void stopAllNotifications( string& threadName );
extern void closeOtherServerSockets( UFSocket* currentClientSocket, string& threadName );

// Methods for handling requests based on type of UFProtocol object received from a client:
extern vector< UFProtocol* > handleRequest( string& name, UFProtocol* );    // hand off to one of following:
extern vector< UFProtocol* > handleRequest( string& name, UFTimeStamp* );   // requests objects or actions.
extern vector< UFProtocol* > handleRequest( string& name, UFObsConfig* );   // set the ObsConfig.
extern vector< UFProtocol* > handleRequest( string& name, UFFrameConfig* ); // set the FrameConfig.
extern vector< UFProtocol* > handleRequest( string& name, UFStrings* );     // for FITS file related stuff.
extern vector< UFProtocol* > handleRequest( string& name, UFInts* );        // for PixelMaps or sim-mode.

//--------------------------------------------------------------------------------------------

// Cases of Client<->Server transactions (all using UFLib objects):

// If any UFLib object is recvd with Name field = "NOTIFY"
// then the server replies with a UFStrings message saying "client will be notified"
// and from then on the server sends the client a stream of FrameConfig objects
// (see below for definition), one FrameConfig for each new frame grabbed.
// The name field gives the names of which buffers have been updated (delimeted with "^").
// To turn off notification client must send a UFLib object with Name field = "NONOTIFY".

// If a UFObsConfig object is received from the client,
// the server stores and utilizes the ObsConfig to configure next observation.
// Server replies with UFStrings status message.
// Note: the ObsConfig must be defined before any START of observation can occur.

// If a UFInts is received from the client with Name field = "PIXELMAP"
// then it is stored to define the Pixel Map (global UFInts* thePixelMap).
// Server replies with UFStrings status message.

// If a UFInts is received from the client with Name field = "NBUF_EDTPDV"
// then the single integer value redefines the size of EDT DMA ring buffers (default = 9).
// Server replies with UFStrings status message.

// A stream of UFInts can be sent by the client to simulate acquired frames,
//  if server is in simulation mode (if EDT device can NOT be opened).
// Server does NOT reply.

// If server is in simulation mode (if EDT device can NOT be opened),
//  a UFFrameConfig can be sent by the client to tell the server the size
//  (width & height in pixels) of the simulation frames that it will be sending.
//  The new frame size will be stored in the FrameConfig.
// Server replies with UFStrings status message.

// The most important object is the FrameConfig, requested by a UFTimeStamp with name="FC".
// The name field of the FrameConfig gives current status of FrameAcqServer, and the
// other attributes give status of EDT interface, FrameGrab and FrameProcess Threads.
// A frameObsSeqNo value greater than or equal to zero and less than frameObsSeqTot
// indicates that an observation is in progress and frames are still being processed.
// Value frameObsSeqNo = -1 indicates that a new ObsConfig was recvd and awaiting START.
// A zero value for frameObsSeqTot indicates that an abort of observation is pending.
// A negative value for frameObsSeqTot indicates that observation was aborted or failed,
// but the absolute value still gives total frames that were originally expected.

  // If a UFTimeStamp object is received from the client, it may request a UFLib object
  // representing the current state, server info or data frames.
  // and the object requested is indicated by the name field of the UFTimeStamp:
  //
  //  Name = "FC", server sends current FrameConfig, with imagecnt, DMAcnt, frame counts,
  //                     in which the name field gives current status
  //                     and timestamp gives the time current frame was grabbed.
  //
  //  Name = "CT", server sends UFTimeStamp with current Camera Type in the name field,
  //                     and timestamp gives the start time of current/last observation.
  //
  //  Name = "VERSION", sends UFTimeStamp with name field giving server code version.
  //                     and timestamp gives the start time of FrameAcqServer execution.
  //
  //  Name = "HOST", server sends UFTimeStamp with name field giving name of host computer.
  //                     and timestamp gives the current time on the server host.
  //
  //  Name = "FH", server sends current FITS Header (UFStrings) to client,
  //                     and the name field also contains name of FITS file (if open),
  //                     and the timestamp gives the time that the file was opened.
  //
  //  Name = "FILENAME", server sends UFStrings with the name of the last FITS file written,
  //                     and the name field also contains status, such as errors if any.
  //                     and the timestamp gives the time that the file was closed.
  //
  //  Name = "OC", server sends current ObsConfig,
  //  Name = "PM", server sends current PixelMap (UFInts),
  //  Name = "BN", server sends Buffer Names (UFStrings) to client,
  //  Name = "buffer name", server sends corresponding FrameConfig and UFInts frame from named buffer.

// Buffer names are defined in FrameProcessThread(),
// and they are taken directly from the OSCIR/IDL cam.pro (by Robert Pina).

  // If a UFTimeStamp is received from the client, it may request an action,
  //  as indicated by the command in name field:
  //  Name = "START", start the FrameGrabThread for new observation.
  //                  (a numeric char after START will set the EDT-pdv timeout in seconds).
  //  Name = "ABORT", abort the observation and send SIGALRM to FrameGrabThread if needed.
  //  Name = "MPWAIT", map pixels in FrameProcessThread instead of FrameGrabThread (auto reset).
  //  Name = "FPWAIT", FrameProcessThread waits for FrameGrabThread to finish first (auto reset).
  // The server performs action and replies with a UFStrings status message,
  // of which the name field indicates either "ERROR", "NOOP", or the command name if success.

  // If a UFStrings object is received from the client, it may define the FITS header
  //   or to open/close a FITS file for saving frames, depending on the name field:
  //
  //  Name = "HEADER", then the strings are assumed to define the full FITS header.
  //  Name = "OPEN", then the first string is assumed to be the file name
  //                 and the FITS file is opened and FITS header is written to it.
  //  Name = "CLOSE", then the FITS file is closed (if all frames have been processed).
  //
  //  Name = "OPEN_QUERY_FITS" or "CLOSE_QUERY_FITS" are just like "OPEN" and "CLOSE" respectively,
  //                         but first all device agents are queried to assemble the FITS header.
  //
  //  Name = "APPEND_HEADER", then the strings are to be appended to the basic (internal) FITS header.
  //  Name = "APPEND_CLOSE", then the strings are to be appended to the basic (internal) FITS header
  //                         and the FITS file is closed (if all frames have been processed).
  //
  // If the UFStrings object has either HEADER or CLOSE in the name field
  //  and it contains at least the same number of strings as the original FITS header,
  //  but not exceeding the standard multiple of 36 records (so data is not overwritten),
  //  then it is assumed to be the final FITS header, and it is written to the file before closing
  // After the server performs any of above actions it replies with UFStrings status message,
  //  of which the name field indicates either "ERROR", "NOOP", or the action name if succeeded.

#endif /* __FrameServerThread_h__ */
