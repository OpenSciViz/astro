#if !defined(__FrameProcessThread_cc__)
#define __FrameProcessThread_cc__ "$Name:  $ $Id: FrameProcessThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameProcessThread_cc__;

#include "FrameAcqServer.h"
#include "FrameProcessThread.h"
#include "FrameGrabThread.h"
#include "FrameNotifyThread.h"
#include "FrameStreamThread.h"
#include "FrameWriteThread.h"

/*******************************************************************************************
 * If a thread needs to lock both _ObsConfigMutex & _FrameConfigMutex
 * the order of locking should be: _ObsConfigMutex then _FrameConfigMutex to avoid deadlock.
 * The order of unlocking should be reverse: _FrameConfigMutex then _ObsConfigMutex.
 * These objects are accessed & locked/unlocked by other threads using methods
 * access/define/copyFrameConfig() and access/define/copyObsConfig(), all in this class.
 *
 * Attributes of the FrameConfig count the # of frames recvd, processed, and # coadds.
 * Values of the ObsConfig are received from clients by FrameServerThreads.
 */

UFFrameConfig* _FrameConfig = 0;
pthread_mutex_t _FrameConfigMutex = PTHREAD_MUTEX_INITIALIZER ;

UFObsConfig* _ObsConfig = 0;
pthread_mutex_t _ObsConfigMutex = PTHREAD_MUTEX_INITIALIZER ;

/*
 * The thread ID and blocking mutex for starting/pausing the FrameProcess thread.
 */
pthread_t FrameProcessThreadID = 0 ;
pthread_mutex_t _FrameProcessMutex = PTHREAD_MUTEX_INITIALIZER ;
static bool _keepRunning = true ;
vector< string > _updatedBufferList;  //List of buffers updated/computed after each frame processed.

/*
 * The structures used to store the grabbed UFInts frames into Lists and mapped buffers,
 * and the mutexes used to synchronize access to the frameBuffers and the frameConfs.
 * The frameBuffers are initialized by createBuffers(), called by FrameProcessThread..
 */
static FrameBuffersType frameBuffers ;
static FrameBuffersType fcopyBuffers ;   //for copies of fixed accum buffs, used in getFramefromBuffer.

static pthread_mutex_t FrameBuffersMutex = PTHREAD_MUTEX_INITIALIZER ;
static pthread_mutex_t FcopyBuffersMutex = PTHREAD_MUTEX_INITIALIZER ;

/*
 * The structures used to store the FrameConfig of each frame buffer,
 * thus keeping track of things like the number of coadds in each frame buffer.
 * The frameConfs are initialized by createBuffers(), called by FrameProcessThread().
 */
static FrameConfsType frameConfs;    //FrameConfigs of frameBuffers (lock with FrameBuffersMutex)
static FrameConfsType fcopyConfs;    //FrameConfigs of fcopyBuffers (lock with FcopyBuffersMutex)

// The List used to store pointers to frames that are reserved (in use by sending threads):
static frameListType reservedFrames ;
static pthread_mutex_t reservedFramesMutex = PTHREAD_MUTEX_INITIALIZER ;

int NframesSent = 0;
int NframesCopied = 0;

#ifdef LittleEndian
/*
 * This List is used to lock pointers to frames that are being sent to clients
 * since the pixels are byte order reversed (host to network order), and they must
 * not be sent to any other clients until byte order is back to Little Endian:
 */
static frameListType LockedFrames ;
static pthread_mutex_t LockedFramesMutex = PTHREAD_MUTEX_INITIALIZER ;
int LockWaits = 0;         //counter
int maxLockWait = 0;       //counter
int maxWaitForLock = 7000; // default max milliseconds to wait for a Lock on a frame pointer.
int sleepTime = 100 ;      // time (ms) to sleep if a client thread must wait to Lock a frame for sending.
#endif

//---------------------- Counters, i.e. # of frames, savesets and nodsets processed:
static int _frameCount;
static int _chopBeam;
static int _nodBeam;
static int _saveSetCnt = 0;
static int _nodSetCnt = 1;
static int _ncoadds = 0;
static bool _nodComplete = false;
static int _ADUavgOn = 0,  _bgADUmax = 0, _bgADUmin = 99999;             //bkgnd (source + readout) ADUs.
static int _ADUavgOff = 0, _rdADUmax = 0, _rdADUmin = 99999;             //readout (dark) ADUs.
static float _percentWellOn = 0,  _bgWellMax = 0, _bgWellMin = 100;      //bkgnd % well.
static float _percentWellOff = 0, _rdWellMax = 0, _rdWellMin = 100;      //readout % well.
static float _sigmaNoiseOn = 0,  _bgnSigmaMax = 0, _bgnSigmaMin = 99999; //total noise (bkgnd + read)
static float _sigmaNoiseOff = 0, _rdnSigmaMax = 0, _rdnSigmaMin = 99999; //readout noise.
static string _readoutMode = "undefined";

/*
 * Region of interest for computing frame statistics (see method computeStats() below).
 * Initialize to 4 integers giving distance in pixels away from edges of frame: (xmin,ymin,xoff,yoff).
 */
UFInts* _statsRegion;
pthread_mutex_t _statsRegionMutex = PTHREAD_MUTEX_INITIALIZER ;

//*************************************************************************************************

bool createFrameProcessThread()
{
  if( ( FrameProcessThreadID = UFPosixRuntime::newThread( FrameProcessThread ) ) > 0 )
    return true;
  else
    return false;
}
//--------------------------------------------------------------------------------------------------

void* FrameProcessThread( void* )
{
  // This method runs as a thread,  processing and storing the frames into the named buffers,
  //  and passing frames to the FrameWriteThread (writeFrame()) and FrameSendThread (sendFrame()).

#ifdef LittleEndian
  clog << "\nFrameProcessor>   frame Lock wait sleep time = " << sleepTime << " millisecs\n";
  clog << "FrameProcessor> maximum Lock wait time = " << maxWaitForLock << " millisecs\n";
#endif

  //Define default _FrameConfig, the actual config is recvd from DC agent.
  _FrameConfig = new (nothrow) UFFrameConfig( 320, 240, 32 ) ;

  //Default # pixels away from edges of frame (xmin,ymin,xoff,yoff) for computeStats().
  int region[] = { 10, 20, 10, 20 };
  _statsRegion = new (nothrow) UFInts( string("STATS_REGION"), region, 4 );

  if( _FrameConfig == 0 || _statsRegion == 0 ) {
    clog << "FrameProcessThread> failed basic mem. alloc., terminating..."<<endl;
    return 0;
  }

  _FrameConfig->setFrameObsSeqNo(-1);  //value (-1) indicates no obs in progress.
  vector< UFInts* > prevReadoutFrames; //to save previous readout frames for noise computations

  // Create the frame-buffer mapped Lists:
  // The first four basic buffer names must correspond to buff_index 0 to 3
  //  and must always be in following order (as transferred by MCE to EDT):
  vector< string > bufferNames;
  bufferNames.push_back("src1");  //0
  bufferNames.push_back("ref1");  //1
  bufferNames.push_back("ref2");  //2
  bufferNames.push_back("src2");  //3
  bufferNames.push_back("accum(src1)");
  bufferNames.push_back("accum(ref1)");
  bufferNames.push_back("accum(src2)");
  bufferNames.push_back("accum(ref2)");
  bufferNames.push_back("dif1");
  bufferNames.push_back("dif2");
  bufferNames.push_back("accum(dif1)");
  bufferNames.push_back("accum(dif2)");
  bufferNames.push_back("sig");
  bufferNames.push_back("accum(sig)");
  clog << "\n FrameProcessThread> creating buffers for frames and processing:\n";

  if( !createBuffers( bufferNames, frameBuffers, frameConfs ) )
    {
      clog << "Failed to Create the frameBuffers" << endl ;
      _keepRunning = false;
      return 0 ;
    }

// These accum buffers are fixed (not pushed) during Frame Processing so
//   getFramefromBuffer() maintains copies for sending to clients:
  vector< string > copyBuffNames;
  copyBuffNames.push_back("accum(src1)");
  copyBuffNames.push_back("accum(ref1)");
  copyBuffNames.push_back("accum(src2)");
  copyBuffNames.push_back("accum(ref2)");
  copyBuffNames.push_back("accum(sig)");
#ifdef LittleEndian
  // These buffers need to be protected from byte swap during send on Intel arch
  //  because they are used later to compute the difference and signal buffers,
  //  so getFramefromBuffer() maintains copies for sending to clients:
  copyBuffNames.push_back("src1");
  copyBuffNames.push_back("ref2");
  copyBuffNames.push_back("accum(dif1)");
#endif
  clog << "\n FrameProcessThread> creating buffers for copies of frames:\n";

  if( !createBuffers( copyBuffNames, fcopyBuffers, fcopyConfs ) )
    {
      clog << "Failed to Create the fcopyBuffers for frame copies." << endl ;
      _keepRunning = false;
      return 0 ;
    }

  //While the grabbed frames List is not empty, keep processing frames:
  // compute pixel values from multi-sample readouts (unless single sampling),
  // move them into named frame buffers depending on current sequence number,
  // accumulate into respective accum. buffers,
  // compute differences src and ref buffers if chopping,
  // and compute sky & read noise and detector well filling.

  while( _keepRunning )
    {
      //mutex must be locked before checking if grabbed frames List is empty!
      ::pthread_mutex_lock( &_FrameProcessMutex );

      if( grabListEmpty() )
	{
	  ::pthread_mutex_lock( &_FrameProcessMutex );
	  // Since a lock was already obtained above, this second try to lock will block and wait.
	  // First mutex will be unlocked by FrameGrabThread when a new frame is grabbed.
	}

      ::pthread_mutex_unlock( &_FrameProcessMutex ); //Unlock mutex to reset for next time.
      if( !_keepRunning ) break;

      // Fetch another vector of detector readout frames, also performing multi-sample pixel maps:
      vector< UFInts* > readoutFrames = popGrabbedFrames();

      //no need to lock _frameCount, only this thread changes value, incremented in processFrame().
      _frameCount = _FrameConfig->frameObsSeqNo();

      if( _frameCount == 0 ) { //start of new obs., but _readoutMode is still from previous obs.
	  if( _readoutMode.find("RAW") == string::npos && prevReadoutFrames.size() > 1 ) {
	    // delete readout frames since newFrame for multi-sample readout was computed (below)
	    //  so in this case previous readout frames were never in frameBuffers:
	    clog << "FrameProcessThread> deleting old readout frames..." << endl ;
	    for( int i=0; i < (int)prevReadoutFrames.size(); i++ ) delete prevReadoutFrames[i];
	  }
	  else if( prevReadoutFrames.size() > 0 ) {
	    clog << "FrameProcessThread> un-reserving old readout frames..." << endl ;
	    for( int i=0; i < (int)prevReadoutFrames.size(); i++ ) unReserveFrame( prevReadoutFrames[i] );
	  }
	  prevReadoutFrames.clear();
      }

      string procBufname;
      _updatedBufferList.clear();
      _readoutMode = _ObsConfig->readoutMode(); //current detector readout mode.
      UFStrings::upperCase( _readoutMode );

      //**********************************************************
      // Process the single or multiple frames in vector of UFInts
      //  according to the detector readout mode:

      if( _readoutMode.find("RAW") != string::npos ) //multi-sample RAW mode: pretend chop or chop-nod.
	{
	  for( int i=0; i < (int)readoutFrames.size(); i++ )
	    procBufname = processFrame( readoutFrames[i], bufferNames );
	}
      else if( readoutFrames.size() > 1 ) //multi-sample readout mode: compute (S1-R1)-(R2-R3)
	{
	  UFInts* newFrame = *(readoutFrames[0]) - *(readoutFrames[1]); // detector ON-OFF (S1-R1).
	  newFrame->stampTime( readoutFrames[0]->timeStamp() );         //keep time frame was grabbed.
	  if( readoutFrames.size() > 3 ) {
	    *newFrame -= *(readoutFrames[2]); //subtract detector OFF column clamp readout.
	    *newFrame += *(readoutFrames[3]); //add the detector OFF output clamp readout -(R2-R3).
	  }
	  procBufname = processFrame( newFrame, bufferNames );
	}
      else procBufname = processFrame( readoutFrames[0], bufferNames ); //single sample readout mode.

      //**********************************************************
      //Compute average signal and standard deviation of noise in readout frames
      // if in "ref" chop beam, or always if not chopping, or always if readout mode is RAW:

      if( procBufname.find("ref") != string::npos || _ObsConfig->chopBeams() <= 1 ||
	  _readoutMode.find("RAW") != string::npos )
	{
	  UFInts* refold = 0;
	  if( prevReadoutFrames.size() > 0 ) //check if we can do background noise computaion.
	    refold = prevReadoutFrames[0];
	  computeStats( readoutFrames[0], refold, _ObsConfig->coaddsPerFrm(),
			_ADUavgOn, _percentWellOn, _sigmaNoiseOn );
	  if( readoutFrames.size() > 1 ) { //see if detector clamped OFF readout is available.
	    refold = 0;
	    if( prevReadoutFrames.size() > 1 ) //check if we can do read noise computaion.
	      refold = prevReadoutFrames[1];
	    computeStats( readoutFrames[1], refold, _ObsConfig->coaddsPerFrm(),
			  _ADUavgOff, _percentWellOff, _sigmaNoiseOff );
	  }
	  minMaxFrameStats();
	  if( _readoutMode.find("RAW") == string::npos && readoutFrames.size() > 1 ) {
	    // delete previous frames since newFrame for multi-sample readout was computed above
	    //  so in this case previous readout frames were never in frameBuffers:
	    for( int i=0; i < (int)prevReadoutFrames.size(); i++ ) delete prevReadoutFrames[i];
	  }
	  else { //we are in RAW disp of multi-sample readout, or just single-sample mode:
	    // remove previous readout frames from the reservedFrames list so they can be deleted:
	    for( int i=0; i < (int)prevReadoutFrames.size(); i++ ) unReserveFrame( prevReadoutFrames[i] );
	    // add new readout frames to the reservedFrames list so they will not get deleted:
	    ::pthread_mutex_lock( &reservedFramesMutex );
	    for( int i=0; i < (int)readoutFrames.size(); i++ ) reservedFrames.push_front( readoutFrames[i] );
	    ::pthread_mutex_unlock( &reservedFramesMutex );
	  }
	  prevReadoutFrames.clear();
	  prevReadoutFrames = readoutFrames; //save for noise computation with next frame(s).
      }
      else if( _readoutMode.find("RAW") == string::npos && readoutFrames.size() > 1 ) {
	// delete readout frames since newFrame for multi-sample readout was computed above
	//  so in this case readout frames are never in frameBuffers and not used for computeStats():
	for( int i=0; i < (int)readoutFrames.size(); i++ ) {
	  delete readoutFrames[i];
	  readoutFrames[i] = 0;
	}
      }

      //update image statistics info in the frame buffer's FrameConfig:
      ::pthread_mutex_lock( &FrameBuffersMutex ) ;
      UFFrameConfig* bfc = frameConfs[ procBufname ] ;
      if( bfc ) {
	bfc->bgADUs( _ADUavgOn );
	bfc->bgWellpc( _percentWellOn );
	bfc->sigmaFrmNoise( _sigmaNoiseOn );
	bfc->offADUs( _ADUavgOff );
	bfc->offWellpc( _percentWellOff );
	bfc->sigmaReadNoise( _sigmaNoiseOff );
      }
      ::pthread_mutex_unlock( &FrameBuffersMutex ) ;

      ::pthread_mutex_lock( &_FrameConfigMutex );
      _FrameConfig->setFrameObsSeqNo( _frameCount ); //store frame count, incremented by processFrame()
      _FrameConfig->setCoAdds( _ncoadds );
      _FrameConfig->setChopBeam( _chopBeam );
      _FrameConfig->setNodBeam( _nodBeam );
      _FrameConfig->setNodSet( _nodSetCnt );
      _FrameConfig->setSaveSet( _saveSetCnt );
      _FrameConfig->bgADUs( _ADUavgOn );
      _FrameConfig->bgADUmin( _bgADUmin );
      _FrameConfig->bgADUmax( _bgADUmax );
      _FrameConfig->bgWellpc( _percentWellOn );
      _FrameConfig->sigmaFrmNoise( _sigmaNoiseOn );
      _FrameConfig->sigmaFrmMin( _bgnSigmaMin );
      _FrameConfig->sigmaFrmMax( _bgnSigmaMax );
      _FrameConfig->offWellpc( _percentWellOff );
      _FrameConfig->offADUs( _ADUavgOff );
      _FrameConfig->rdADUmin( _rdADUmin );
      _FrameConfig->rdADUmax( _rdADUmax );
      _FrameConfig->sigmaReadNoise( _sigmaNoiseOff );
      _FrameConfig->sigmaReadMin( _rdnSigmaMin );
      _FrameConfig->sigmaReadMax( _rdnSigmaMax );
      int FrameTotal = _FrameConfig->frameObsSeqTot();
      ::pthread_mutex_unlock( &_FrameConfigMutex );     //unlock this as soon as possible.

      if( _nodComplete )
	++_nodSetCnt;    //increment nod set counter AFTER setting _FrameConfig.

      notifyClientsNewFC( _updatedBufferList );

      // periodically remove old frames from the buffers that are not being sent to a client:
      if( (_frameCount % 4) == 0 ) removeOldFrames();

      if( _frameCount >= FrameTotal ) { //either done with obs or abort occurred.
	if( grabListEmpty() ) {
	  if( FrameTotal == 0 )
	    clog << "FrameProcessThread> abort pending: _frameCount="<<_frameCount<<" . "<<endl ;
	  else if( FrameTotal < 0 )
	    clog << "FrameProcessThread> abort detected: _frameCount="<<_frameCount<<" . "<<endl ;
	  //write final processed (coadded etc.) image to file with .fits.accsig extension:
	  if( _ObsConfig->nodBeams() > 1 )
	    writeAccumSigImage( frameBuffers["accum(sig)"]->front() ); //nod or chop-nod mode
	  else if( _ObsConfig->chopBeams() > 1 )
	    writeAccumSigImage( frameBuffers["accum(dif1)"]->front() ); //chop only mode
	  else
	    writeAccumSigImage( frameBuffers["accum(src1)"]->front() ); //stare mode
	  clog << "FrameProcessThread> removing old frames..." << endl ;
	  removeOldFrames();
	}
      }
    } //end of while(_keepRunning) loop.

  clog << "FrameProcessThread> terminated."<<endl;
  return 0;
}
//-----------------------------------------------------------Process Chop and/or Nod modes-------

string processFrame( UFInts* newFrame, vector< string >& bufferNames )
{
  // Processing one more frame: move into a named frame buffer depending on current
  // sequence number, accumulate into respective accum. buffer, computed differences
  // of src and ref buffers if chopping, and compute current sky noise and detector well filling.
  // _frameCount is incremented here.
  // Method returns the current buffer name into which frame was moved.

  int buff_index = _ObsConfig->buffId( _frameCount++ ) - 1; //increment FrameCount after using it.
  _chopBeam = buff_index % 2;
  if( _chopBeam == 0 ) ++_saveSetCnt;  //global: count # of savesets (src & ref pairs).
  _nodBeam = buff_index/2;
  string BufferName = bufferNames[buff_index];
  string AccBufName = "accum(" + BufferName + ")";
  _updatedBufferList.push_back(BufferName);
  _updatedBufferList.push_back(AccBufName);
  newFrame->rename( BufferName );
  newFrame->setSeq( _frameCount, _ObsConfig->totFrameCnt() );

  if( _ObsConfig->chopBeams() <= 1 ) {  //for the case of nodding with NO chopping.
    _nodBeam = _chopBeam;
    _chopBeam = 0;
  }

  if( nowSavingData() )
    {
      if( !writeFrame( newFrame, _saveSetCnt ) )
	  clog << "ERROR: FrameProcessThread> Failed to queue for write: frame # "
	       << _frameCount << " (" <<  newFrame->name() << ")" <<endl;
    }

  // add data label to name and send frame to client data replication stream (if active):
  newFrame->relabel( _ObsConfig->dataLabel() );
  streamFrame( newFrame );

  ::pthread_mutex_lock( &FrameBuffersMutex ) ;
  // Insert the frame at the front of the list:
  frameBuffers[ BufferName ]->push_front( newFrame ) ;

  //update info in the buffer's FrameConfig:
  UFFrameConfig* bfc = frameConfs[ BufferName ] ;
  bfc->setCoAdds( 1 );
  bfc->setChopBeam( _chopBeam );
  bfc->setNodBeam( _nodBeam );
  bfc->setNodSet( _nodSetCnt );
  bfc->setSaveSet( _saveSetCnt );
  bfc->stampTime( newFrame->timeStamp() );
  bfc->setFrameObsSeqNo( _frameCount );

  // now coadd the frame into corresponding accum buffer
  UFInts* accFrame = 0;
  UFFrameConfig* bafc = frameConfs[ AccBufName ] ;
  _ncoadds = bafc->coAdds();
  frameListType* accBufList = frameBuffers[ AccBufName ];

  if( accBufList->empty() || _ObsConfig->clearBuff( _frameCount-1 ) )
    {
      _ncoadds = 0;
      accFrame = new (nothrow) UFInts( AccBufName, newFrame->elements() );

      if( UFProtocol::validElemCnt( accFrame ) )
	accBufList->push_front( accFrame );  //new zero buffer.
      else {
	string status = "ERROR: FrameProcessThread> mem. alloc. FAILED for buffer: " + AccBufName;
	clog << status <<endl;
	::pthread_mutex_lock( &_FrameConfigMutex );
	_FrameConfig->rename(status);
	::pthread_mutex_unlock( &_FrameConfigMutex );
	::pthread_mutex_unlock( &FrameBuffersMutex );
	return BufferName;
      }
    }
  else accFrame = accBufList->front(); //use current buffer, getFramefromBuffer makes copy if sent.

  *accFrame += *newFrame;     //accumulate frame using UFInts operator overloading.
  accFrame->stampTime( newFrame->timeStamp() );
  accFrame->setSeq( newFrame->seqCnt(), newFrame->seqTot() );

  bafc->setCoAdds( ++_ncoadds );  //increment coadds and store.
  bafc->setChopBeam( _chopBeam );
  bafc->setNodBeam( _nodBeam );
  bafc->setNodSet( _nodSetCnt );
  bafc->setSaveSet( _saveSetCnt );
  bafc->stampTime( newFrame->timeStamp() );
  bafc->setFrameObsSeqNo( _frameCount );

  if( _ObsConfig->doDiff1( _frameCount-1 ) || _ObsConfig->doDiff2( _frameCount-1 ) )
    {
      computeChopDiff();
      computeChopDiff("accum(");
    }

  if( _ObsConfig->doSig( _frameCount-1 ) )
    {
      computeNodSum();
      _nodComplete = true;
    }
  else _nodComplete = false;

  ::pthread_mutex_unlock( &FrameBuffersMutex );

  return BufferName; //end of processFrame( UFInts* newFrame )
}
//-----------------------------------------------------------Process Chop and/or Nod modes-------

void computeChopDiff( string accum )
{
  string nodbuf = "1";
  if( _ObsConfig->chopBeams() >= 2 ) nodbuf = UFRuntime::numToStr( _nodBeam + 1 );
  if( accum.length() > 1 ) nodbuf += ")";
  string difname = accum + "dif" + nodbuf;

#ifdef DEBUG_FP
  clog << "FrameProcessThread::computeChopDiff> computing Chop/Nod diff:" << difname <<endl ;
#endif

  UFInts* srcFrame = frameBuffers[ accum + "src" + nodbuf ]->front();
  UFInts* refFrame = frameBuffers[ accum + "ref" + nodbuf ]->front();

  if( srcFrame <= 0 ) {
    clog<<"ERROR: FrameProcessThread::computeChopDiff> "<<accum<<"src"<<nodbuf<<" frame is NULL"<<endl ;
    return;
  }
  else if( refFrame <= 0 ) {
    clog<<"ERROR: FrameProcessThread::computeChopDiff> "<<accum<<"ref"<<nodbuf<<" frame is NULL"<<endl ;
    return;
  }

  UFInts* diffFrame = *srcFrame - *refFrame;   // compute (src - ref) using overloaded "-" oper.

  if( ! UFProtocol::validElemCnt( diffFrame ) ) {
    string status = "ERROR: FrameProcessThread::computeChopDiff> mem. alloc. FAILED for: " + difname;
    clog << status << ". " <<endl;
    ::pthread_mutex_lock( &_FrameConfigMutex );
    _FrameConfig->rename(status);
    ::pthread_mutex_unlock( &_FrameConfigMutex );
    return;
  }

  diffFrame->setSeq( srcFrame->seqCnt(), srcFrame->seqTot() );
  diffFrame->rename( difname );
  frameBuffers[ diffFrame->name() ]->push_front( diffFrame ) ;   //push pointer into frameBuffers.

  //update info in the buffer's FrameConfig:
  UFFrameConfig* bfc = frameConfs[ diffFrame->name() ];
  if( accum.length() > 1 )
    bfc->setCoAdds( _ncoadds );
  else
    bfc->setCoAdds( 1 );
  bfc->setNodBeam( _nodBeam );
  bfc->setNodSet( _nodSetCnt );   //from global
  bfc->setSaveSet( _saveSetCnt ); //from global
  bfc->setFrameObsSeqNo( _frameCount );
  bfc->stampTime( diffFrame->timeStamp() );
  _updatedBufferList.push_back( diffFrame->name() );
}
//----------------------------------------------------------------------Process Nods----

void computeNodSum()
{
#ifdef DEBUG_FP
  clog << "FrameProcessThread::computeNodSum> computing Nod Sum of Chop Diffs."<<endl ;
#endif

  UFInts* dif1Frame = frameBuffers["accum(dif1)"]->front();
  UFInts* dif2Frame = frameBuffers["accum(dif2)"]->front();
  UFInts* sigFrame = 0;

  if( dif1Frame <= 0 ) {
    clog << "ERROR: FrameProcessThread::computeNodSum> accum(dif1) frame is NULL"<<endl ;
    return;
  }

  if( _ObsConfig->chopBeams() > 1 && _ObsConfig->nodBeams() > 1 )
    {
      if( dif2Frame <= 0 ) {
	clog << "ERROR: FrameProcessThread::computeNodSum> accum(dif2) frame is NULL"<<endl ;
	return;
      }
      sigFrame = *dif1Frame + *dif2Frame;   // compute dif1 + dif2 for sum of nod beams.
    }
  else sigFrame = new (nothrow) UFInts( string("sig"), dif1Frame->valInts(), dif1Frame->numVals() );

  if( ! UFProtocol::validElemCnt( sigFrame ) ) {
    string status = "ERROR: FrameProcessThread::computeNodSum> memory allocation for sig buffer FAILED!";
    clog << status <<endl;
    ::pthread_mutex_lock( &_FrameConfigMutex );
    _FrameConfig->rename(status);
    ::pthread_mutex_unlock( &_FrameConfigMutex );
    return;
  }

  sigFrame->setSeq( dif1Frame->seqCnt(), dif1Frame->seqTot() );
  sigFrame->rename("sig");
  frameBuffers[ sigFrame->name() ]->push_front( sigFrame ) ; //push pointer into frameBuffers.

  //update info in the buffer's FrameConfig:
  UFFrameConfig* bfc = frameConfs[ sigFrame->name() ] ;
  int sigCoadds = 2*_ncoadds;
  bfc->setCoAdds( sigCoadds );
  bfc->setNodSet( _nodSetCnt );
  bfc->setSaveSet( _saveSetCnt );
  bfc->setFrameObsSeqNo( _frameCount );
  bfc->stampTime( sigFrame->timeStamp() );
  _updatedBufferList.push_back( sigFrame->name() );

  // now coadd the frame into accum(sig) buffer:
  string sigAccBufName = "accum(sig)";
  UFInts* sigAccFrame = frameBuffers[ sigAccBufName ]->front();
  bfc = frameConfs[ sigAccBufName ] ;

  if( bfc->coAdds() <= 0 || sigAccFrame == 0 )
    {
      sigAccFrame = new (nothrow) UFInts( sigAccBufName, sigFrame->numVals() );

      if( UFProtocol::validElemCnt( sigAccFrame ) )
	frameBuffers[ sigAccBufName ]->push_front( sigAccFrame );  //push new zero buffer.
      else {
	string status =
	  "ERROR: FrameProcessThread::computeNodSum> mem. alloc. for accum(sig) buffer FAILED!";
	clog << status <<endl;
	::pthread_mutex_lock( &_FrameConfigMutex );
	_FrameConfig->rename(status);
	::pthread_mutex_unlock( &_FrameConfigMutex );
	return;
      }
    }

  *sigAccFrame += *sigFrame;   //final signal accumulation
  sigAccFrame->setSeq( sigFrame->seqCnt(), sigFrame->seqTot() );

  bfc->setCoAdds( bfc->coAdds() + sigCoadds );
  bfc->setNodSet( _nodSetCnt );   //from global
  bfc->setSaveSet( _saveSetCnt ); //from global
  bfc->setFrameObsSeqNo( _frameCount );
  bfc->stampTime( sigFrame->timeStamp() );
  _updatedBufferList.push_back( sigAccFrame->name() );
}
//---------------------------------------------------------------------------------

void computeStats( UFInts* refnew, UFInts* refold, int CoaddsPerFrame,
		   int& ADUavg, float& percentWell, float& sigma )
{
  int xsize = _FrameConfig->width();
  int ysize = _FrameConfig->height();
  ::pthread_mutex_lock( &_statsRegionMutex );
  int xmin = _statsRegion->valInt(0);
  int ymin = _statsRegion->valInt(1);
  int xmax = xsize - _statsRegion->valInt(2);
  int ymax = ysize - _statsRegion->valInt(3);
  ::pthread_mutex_unlock( &_statsRegionMutex );

  int* fdata = refnew->valInts();
  double totalCnts = 0;
  int Npix = 0;

  for( int j=ymin; j<ymax; j++ ) {
    int* fd = fdata + j*xsize + xmin;
    for( int i=xmin; i<xmax; i++ ) {
      ++Npix;
      totalCnts += (double)(*fd++);
    }
  }

  double refavg = totalCnts/Npix;
  ADUavg = (int)rint( refavg/CoaddsPerFrame );
  int ADCmax = (1<<16)-1;
  int ADCdark = 0, ADCsat = ADCmax;
  percentWell = 100*((float)(ADUavg-ADCdark))/(ADCsat-ADCdark);
  sigma = 0;

  if( refold ) {
    fdata = refold->valInts();
    totalCnts = 0;
    for( int j=ymin; j<ymax; j++ ) {
      int* fd = fdata + j*xsize + xmin;
      for( int i=xmin; i<xmax; i++ ) totalCnts += (double)(*fd++);
    }
    double refDiffAvg = refavg - totalCnts/Npix;
    UFInts* refdiff = *refnew - *refold;
    fdata = refdiff->valInts();
    double totDiffsq = 0;

    for( int j=ymin; j<ymax; j++ ) {
      int* fd = fdata + j*xsize + xmin;
      for( int i=xmin; i<xmax; i++ ) {
	double fpix = (double)(*fd++) - refDiffAvg;
	totDiffsq += (fpix*fpix);
      }
    }
    delete refdiff;
    if( totDiffsq > 0 ) sigma = sqrt( totDiffsq/CoaddsPerFrame/Npix/2 );
  }
}
//-----------------------------------------------------------------------------------------

void minMaxFrameStats()
{
  if( _ADUavgOn > _bgADUmax ) _bgADUmax = _ADUavgOn;
  if( _ADUavgOn < _bgADUmin ) _bgADUmin = _ADUavgOn;
  if( _ADUavgOff > _rdADUmax ) _rdADUmax = _ADUavgOff;
  if( _ADUavgOff < _rdADUmin ) _rdADUmin = _ADUavgOff;
  if( _percentWellOn > _bgWellMax ) _bgWellMax = _percentWellOn;
  if( _percentWellOn < _bgWellMin ) _bgWellMin = _percentWellOn;
  if( _percentWellOff > _rdWellMax ) _rdWellMax = _percentWellOff;
  if( _percentWellOff < _rdWellMin ) _rdWellMin = _percentWellOff;
  if( _sigmaNoiseOn > _bgnSigmaMax )  _bgnSigmaMax = _sigmaNoiseOn;
  if( _sigmaNoiseOn < _bgnSigmaMin && _sigmaNoiseOn > 0 ) _bgnSigmaMin = _sigmaNoiseOn;
  if( _sigmaNoiseOff > _rdnSigmaMax ) _rdnSigmaMax = _sigmaNoiseOff;
  if( _sigmaNoiseOff < _rdnSigmaMin && _sigmaNoiseOff > 0 ) _rdnSigmaMin = _sigmaNoiseOff;
}
//-----------------------------------------------------------------------------------------

void setStatsRegion( UFInts* sregion )
{
  ::pthread_mutex_lock( &_statsRegionMutex );
  delete _statsRegion;
  _statsRegion = sregion;
  ::pthread_mutex_unlock( &_statsRegionMutex );
  clog << "setStatsRegion> Mins: x:" << _statsRegion->valInt(0) << " y:" << _statsRegion->valInt(1)
       << "   Offsets from Maxs: x:" << _statsRegion->valInt(2) << " y:" << _statsRegion->valInt(3) <<endl;
}
//-----------------------------------------------------------------------------------------

UFStrings* getStatsRegion()
{
  strstream ss;
  ::pthread_mutex_lock( &_statsRegionMutex );
  ss << "Statistics Region> Mins: x:" << _statsRegion->valInt(0) << " y:" << _statsRegion->valInt(1);
  ss << "      Offsets from Maxs: x:" << _statsRegion->valInt(2) << " y:" << _statsRegion->valInt(3);
  ::pthread_mutex_unlock( &_statsRegionMutex );
  ss << ends;
  string message = ss.str();
  delete ss.str();
  clog << message <<endl;
  UFStrings* sregion = new (nothrow) UFStrings( _statsRegion->name(), &message );

  if( sregion == 0 ) {
    message = "ERROR: getStatsRegion> memory allocation failure!";
    clog << message <<endl;
    sregion = new (nothrow) UFStrings( message, 0 ) ;
  }
  else sregion->stampTime( _statsRegion->timeStamp() );   //original timestamp.

  return sregion;
}
//-----------------------------------------------------------------------------------------
// Give other threads access to status and counters:

void FP_Start() { ::pthread_mutex_unlock( &_FrameProcessMutex ); }

void FP_Shutdown()
{
  _keepRunning = false ;
  FP_Start();
}

int FP_NframesSent()   { return NframesSent; }
int FP_NframesCopied() { return NframesCopied; }

#ifdef LittleEndian
int FP_LockWaits()     { return LockWaits; }
int FP_maxLockWait()   { return maxLockWait; }

int FP_setLockWaitTimes( int waitTime, int maxWait )
{
  if( waitTime > 10 ) sleepTime = waitTime ;
  if( maxWait > 1000 ) maxWaitForLock = maxWait ;
  return maxWaitForLock/sleepTime;
}
#endif
//--------------------------------------------------------------------------

UFFrameConfig* accessFrameConfig( bool Lock )
{
  if( Lock ) {
    ::pthread_mutex_lock( &_FrameConfigMutex );
    return _FrameConfig;
  } else {
    ::pthread_mutex_unlock( &_FrameConfigMutex );
    return 0;
  }
}
//--------------------------------------------------------------------------

UFObsConfig* accessObsConfig( bool Lock )
{
  if( Lock && _ObsConfig != 0 ) {
    ::pthread_mutex_lock( &_ObsConfigMutex );
    return _ObsConfig;
  } else {
    ::pthread_mutex_unlock( &_ObsConfigMutex );
    return 0;
  }
}
//--------------------------------------------------------------------------

string defineFrameConfig( UFFrameConfig* fc )
{
  ::pthread_mutex_lock( &_FrameConfigMutex );
  _FrameConfig->setWidth( fc->width() ) ;
  _FrameConfig->setHeight( fc->height() ) ;
  _FrameConfig->frameCoadds( fc->frameCoadds() );
  _FrameConfig->chopSettleFrms( fc->chopSettleFrms() );
  _FrameConfig->chopCoadds( fc->chopCoadds() );
  _FrameConfig->frameTime( fc->frameTime() );
  _FrameConfig->savePeriod( fc->savePeriod() );
  _FrameConfig->stampTime( UFRuntime::currentTime() );
  ::pthread_mutex_unlock( &_FrameConfigMutex );

  return("FrameAcqServer> accepted new FrameConfig width, height, and MCE-CT counters.");
}
//--------------------------------------------------------------------------

string defineObsConfig( UFObsConfig* oc )
{
  ::pthread_mutex_lock( &_ObsConfigMutex );
  if( _ObsConfig != 0 ) delete _ObsConfig;
  _ObsConfig = oc;

  _readoutMode = oc->readoutMode();
  UFStrings::upperCase( _readoutMode );
  //if RAW is requested for multi-sample readout mode, pretend it is chopping and nodding,
  // so frame acq. server will process the readout frames seperately (instead of subtracting).

  if( _readoutMode.find("RAW") != string::npos ) {
    if( _readoutMode.find("S1R1") != string::npos || _readoutMode.find("1S1R") != string::npos ) {
      if( oc->chopBeams() != 2 || oc->nodBeams() != 1 ) {
	_ObsConfig = 0;
	_ObsConfig = new (nothrow) UFObsConfig( oc->dataLabel(), 1, 2,
						oc->saveSets() * oc->nodSets(), 1, oc->coaddsPerFrm() );
	if( _ObsConfig ) {
	  _ObsConfig->setReadoutMode( _readoutMode );
	  delete oc;
	} else {
	  clog<<"ERROR:FrameAcqServer::defineObsConfig> memory alloc. error during redefine!"<<endl;
	  _ObsConfig = oc;
	}
      }
    }
    else if( _readoutMode.find("S1R3") != string::npos || _readoutMode.find("1S3R") != string::npos ) {
      if( oc->chopBeams() != 2 || oc->nodBeams() != 2 ) {
	_ObsConfig = 0;
	_ObsConfig = new (nothrow) UFObsConfig( oc->dataLabel(), 2, 2,
						1, oc->saveSets() * oc->nodSets(), oc->coaddsPerFrm() );
	if( _ObsConfig ) {
	  _ObsConfig->setReadoutMode( _readoutMode );
	  delete oc;
	} else {
	  clog<<"ERROR:FrameAcqServer::defineObsConfig> memory alloc. error during redefine!"<<endl;
	  _ObsConfig = oc;
	}
      }
    }
  }

  _ObsConfig->stampTime( UFRuntime::currentTime() );
  clog<<"FrameAcqServer::defineObsConfig> approx. transfer time across EDT fiber = "
      <<_ObsConfig->estXferTime()<<" secs"<<endl;
  string message = "FrameAcqServer> new ObsConfig: " + _ObsConfig->name();
  ::pthread_mutex_unlock( &_ObsConfigMutex );

  return message;
}

//--------------------------------------------------------------------------

bool createBuffers( vector< string >& names, FrameBuffersType& frmBuffs, FrameConfsType& frmConfs )
{
  for ( vector< string >::size_type i=0; i<names.size(); i++ )
    {
      clog<<"createBuffers> " << names[i] <<endl;

      frameListType* fList = new frameListType;
      frmBuffs[names[i]] = fList;

      frmConfs[names[i]] = new (nothrow) UFFrameConfig( names[i], 128, 128, 32 );

      if( frmConfs[names[i]] == 0 ) {
	clog << "createBuffers> FrameConfig memory allocation failed! " << endl ;
	return false;
      }
    }

  clog<<"createBuffers> Created " << frmBuffs.size() << " frame-buffers with "
      << frmConfs.size() << " FrameConfigs"<<endl;

  return true ;
}
//--------------------------------------------------------------------------

UFStrings* getBufferNames()
{
  vector< string > names ;
  clog << "mapped frame buffer names:\n";

  for( FrameBuffersType::iterator ptr = frameBuffers.begin(); ptr != frameBuffers.end(); ++ptr )
    {
      names.push_back( ptr->first ) ;
      clog << ptr->second->size() << " frames in Buffer: " << ptr->first << endl;
    }

  return new (nothrow) UFStrings( "Frame Buffer Names", names );
}
//--------------------------------------------------------------------------

UFFrameConfig* copyFrameConfig()
{
  // Lock the mutex and copy _FrameConfig for sending to a client:
  ::pthread_mutex_lock( &_FrameConfigMutex );

  UFFrameConfig* uffc = new (nothrow) UFFrameConfig( _FrameConfig->name(),
						     _FrameConfig->valData(),
						     _FrameConfig->timeStamp() );
  ::pthread_mutex_unlock( &_FrameConfigMutex );
  return uffc;
}
//--------------------------------------------------------------------------

UFObsConfig* copyObsConfig( string& message )
{
  if( _ObsConfig != 0 )
    {
      ::pthread_mutex_lock( &_ObsConfigMutex );

      UFObsConfig* ufoc = new (nothrow) UFObsConfig( _ObsConfig->name(),
						     _ObsConfig->valData(),
						     _ObsConfig->numVals() );
      ::pthread_mutex_unlock( &_ObsConfigMutex );

      if( ufoc == 0 )
	message = "ERROR: FrameAcqServer> Mem.alloc. failure for ObsConfig copy!";
      else {
	message = "FrameAcqServer> sending copy of _ObsConfig...";;
	ufoc->stampTime( _ObsConfig->timeStamp() );
      }
      return ufoc;
    }
  else {
    message = "ERROR: FrameServerThread> ObsConfig is NOT defined!";
    return 0;
  }
}
//--------------------------------------------------------------------------

void ResetCounters()
{
  clog << "ResetCounters> resetting FrameConfig counters and stats..." <<endl;

  ::pthread_mutex_lock( &_FrameConfigMutex );
  if( _ObsConfig ) _FrameConfig->setFrameObsSeqTot( _ObsConfig->totFrameCnt() ) ;
  _FrameConfig->setDMACnt( 0 );
  _FrameConfig->frameGrabCnt( 0 );
  _FrameConfig->setCoAdds( 0 );
  _FrameConfig->setChopBeam( -1 );
  _FrameConfig->setSaveSet( 0 );
  _FrameConfig->setNodBeam( -1 );
  _FrameConfig->setNodSet( 0 );
  _FrameConfig->setFrameObsSeqNo( -1 );  //(-1) indicates obs NOT in progress.
  _FrameConfig->setFrameWriteCnt( 0 );
  _FrameConfig->setFrameSendCnt( 0 );
  _FrameConfig->bgADUs( 0 );
  _FrameConfig->bgWellpc( 0.0 );
  _FrameConfig->sigmaFrmNoise( 0.0 );
  _FrameConfig->offADUs( 0 );
  _FrameConfig->offWellpc( 0.0 );
  _FrameConfig->sigmaReadNoise( 0.0 );
  _FrameConfig->rename("Reset FrameConfig counters");
  _FrameConfig->stampTime( UFRuntime::currentTime() );  //remember when reset.
  ::pthread_mutex_unlock( &_FrameConfigMutex );

  _saveSetCnt = 0; //reset these globals which are used by FrameProcessThread.
  _nodSetCnt = 1;
  _ADUavgOn = 0;  _bgADUmax = 0; _bgADUmin = 99999;           //bkgnd (source + readout) ADUs.
  _ADUavgOff = 0; _rdADUmax = 0; _rdADUmin = 99999;           //readout (dark) ADUs.
  _percentWellOn = 0;  _bgWellMax = 0; _bgWellMin = 100;      //bkgnd % well.
  _percentWellOff = 0; _rdWellMax = 0; _rdWellMin = 100;      //readout % well.
  _sigmaNoiseOn = 0;  _bgnSigmaMax = 0; _bgnSigmaMin = 99999; //total noise (bkgnd + read)
  _sigmaNoiseOff = 0; _rdnSigmaMax = 0; _rdnSigmaMin = 99999; //readout noise.

  ::pthread_mutex_lock( &FrameBuffersMutex );

  for( FrameConfsType::iterator fCPtr = frameConfs.begin(); fCPtr != frameConfs.end(); ++fCPtr )
    {
	  UFFrameConfig* bfc = fCPtr->second;
	  bfc->setWidth( _FrameConfig->width() );
	  bfc->setHeight( _FrameConfig->height() );
	  bfc->setDepth( _FrameConfig->depth() );
	  bfc->setCoAdds( 0 );
	  bfc->setFrameObsSeqNo( 0 );
          bfc->setFrameObsSeqTot( _FrameConfig->frameObsSeqTot() );
	  bfc->setDMACnt( 0 );
	  bfc->frameGrabCnt( 0 );
	  bfc->setChopBeam( -1 );
	  bfc->setSaveSet( 0 );
	  bfc->setNodBeam( -1 );
	  bfc->setNodSet( 0 );
	  bfc->bgADUs( 0 );
	  bfc->bgWellpc( 0.0 );
	  bfc->sigmaFrmNoise( 0.0 );
	  bfc->frameCoadds( _FrameConfig->frameCoadds() );
	  bfc->chopSettleFrms( _FrameConfig->chopSettleFrms() );
	  bfc->chopCoadds( _FrameConfig->chopCoadds() );
	  bfc->frameTime( _FrameConfig->frameTime() );
	  bfc->savePeriod( _FrameConfig->savePeriod() );
 	  bfc->stampTime( UFRuntime::currentTime() );
    }
  ::pthread_mutex_unlock( &FrameBuffersMutex );

  ::pthread_mutex_lock( &FcopyBuffersMutex );

  for( FrameConfsType::iterator fCPtr = fcopyConfs.begin(); fCPtr != fcopyConfs.end(); ++fCPtr )
    {
	  UFFrameConfig* bfc = fCPtr->second;
	  bfc->setWidth( _FrameConfig->width() );
	  bfc->setHeight( _FrameConfig->height() );
	  bfc->setDepth( _FrameConfig->depth() );
	  bfc->setCoAdds( 0 );
	  bfc->setFrameObsSeqNo( 0 );
          bfc->setFrameObsSeqTot( _FrameConfig->frameObsSeqTot() );
	  bfc->setDMACnt( 0 );
	  bfc->frameGrabCnt( 0 );
	  bfc->setChopBeam( -1 );
	  bfc->setSaveSet( 0 );
	  bfc->setNodBeam( -1 );
	  bfc->setNodSet( 0 );
	  bfc->bgADUs( 0 );
	  bfc->bgWellpc( 0.0 );
	  bfc->sigmaFrmNoise( 0.0 );
	  bfc->frameCoadds( _FrameConfig->frameCoadds() );
	  bfc->chopSettleFrms( _FrameConfig->chopSettleFrms() );
	  bfc->chopCoadds( _FrameConfig->chopCoadds() );
	  bfc->frameTime( _FrameConfig->frameTime() );
	  bfc->savePeriod( _FrameConfig->savePeriod() );
 	  bfc->stampTime( UFRuntime::currentTime() );
    }
  ::pthread_mutex_unlock( &FcopyBuffersMutex );
}

#ifdef LittleEndian
/**---------------------------------------------------------------------------------------
 * On Little Endian arch (e.g. Intel) must lock frame during send
 * because byte order is reversed when sending from host to network,
 * so the following method is called by getFramefromBuffer().
 * After frame is sent it is erased from the LockedFrames List by unReserveFrame().
 */

bool LockFrameToSend( UFInts* frameToSend )
{
  int WaitForLock = 0;

  while( WaitForLock < maxWaitForLock )
    {
      ::pthread_mutex_lock( &LockedFramesMutex );
      frameListType::iterator fPtr = std::find( LockedFrames.begin(),
						LockedFrames.end(),
						frameToSend );
      if( fPtr == LockedFrames.end() )
	{
	  LockedFrames.push_back( frameToSend );
	  ::pthread_mutex_unlock( &LockedFramesMutex );
	  return true;
	}
      ::pthread_mutex_unlock( &LockedFramesMutex );
      LockWaits++;
      WaitForLock += sleepTime;
      maxLockWait = max( maxLockWait, WaitForLock );
      UFPosixRuntime::sleep( ((float)sleepTime)/1000.0 );    //try again after short sleep...
    }

  return false;
}
#endif

/**------------------------------------------------------------------------------------------------
 * To access/send a frame, must lock FrameBuffersMutex and reservedFramesMutex (IN THAT ORDER!).
 * Once both mutexes are locked, check for the existence of a particular frame desired.
 * If it is found, the frame pointer is added to the reservedFrames List (method getFramefromBuffer).
 * It will not be deleted until it is removed from the reservedFrames List by unReserveFrame().
 * This deletion is done in removeOldFrames() by first locking BOTH mutexes.
 */

vector< UFProtocol* > getFramefromBuffer( string BuffName )
{
  vector< UFProtocol* > Reply ;
  string message, instrument = "";

  // This method returns UFFrameConfig and UFInts pointers from the requested frame Buffer
  // as a 2 element vector of UFProtocol objects.
  // First check if instrument name is prepended to buffer name:

  if( BuffName.find(":") != string::npos ) {
    int ipos = BuffName.find(":");
    instrument = BuffName.substr(0,ipos+1);
    BuffName = BuffName.substr(ipos+1);
  }

  if( frameConfs.find( BuffName ) == frameConfs.end() ) return Reply; //non-existent buffer, empty reply.

  ::pthread_mutex_lock( &FrameBuffersMutex );
  // First part of reply is copy the FrameConfig of buffer::
  UFFrameConfig* bufFrameConf = frameConfs[ BuffName ];

  UFFrameConfig* bfc = new (nothrow) UFFrameConfig( instrument + bufFrameConf->name(),
						    bufFrameConf->valData(),
						    bufFrameConf->timeStamp() );
  if( bfc == 0 ) {
    ::pthread_mutex_unlock( &FrameBuffersMutex );
    message = "ERROR: getFramefromBuffer> FrameConfig copy memory alloc failure: " + bufFrameConf->name();
    clog << message << " ! " << endl;
    ::pthread_mutex_lock( &_FrameConfigMutex );
    _FrameConfig->rename( message );
    ::pthread_mutex_unlock( &_FrameConfigMutex );
    Reply.push_back( new (nothrow) UFTimeStamp( message ) );
    return Reply;
  }
  else Reply.push_back( bfc );  

  // make sure that frame buffer is not empty:
  frameListType* fBufList = frameBuffers[ BuffName ] ;

  if( fBufList->empty() )	  // frame not found
    {
      ::pthread_mutex_unlock( &FrameBuffersMutex );
      message = "ERROR: FrameAcqServer> Buffer Empty: " + BuffName;
      clog << message << " ! " << endl;
      bfc->rename( message );          //note that object bfc is already pushed into Reply vector.
      return Reply;
    }

  // Found the buffer we want and there is a frame there.
  // New frames are always in the front of the list:

  UFInts* frameToSend = fBufList->front();
  UFStrings::lowerCase( BuffName );
  ::pthread_mutex_lock( &FcopyBuffersMutex );

  //Check if buffer needs to be protected by sending a copy of it:
  // (only worry about it if obs mode is chopping or if an accumulation buffer)

  if( BuffName.find("accum") != string::npos || _ObsConfig->chopBeams() > 1 ) {
    if( fcopyBuffers.find( BuffName ) != fcopyBuffers.end() )
      {
	// get pointer to the copied frame buffer:
	frameListType* fcopyBufList = fcopyBuffers[ BuffName ] ;
	UFFrameConfig* copyBuffFC = fcopyConfs[ BuffName ];

	if( copyBuffFC->frameObsSeqNo() != bufFrameConf->frameObsSeqNo() )
	  {
	    UFInts* frameCopy = new (nothrow) UFInts( frameToSend->name(),
						      (int*)frameToSend->valData(),
						      frameToSend->numVals() );
	    if( frameCopy == 0 ) {
	      message = "WARNING: getFramefromBuffer> mem. alloc. FAILED: using old copy of: " + BuffName;
	      clog << message << " ! " << endl;
	      bfc->rename( message );          //note that object bfc is already pushed into Reply vector.
	    }
	    else {
	      fcopyBufList->push_front( frameCopy );              //update frame buffer with new copy.
	      frameCopy->stampTime( frameToSend->timeStamp() );   //remember the frame's timestamp.
	      NframesCopied++;
	      delete copyBuffFC;
	      copyBuffFC = new (nothrow) UFFrameConfig( bufFrameConf->name(),
							bufFrameConf->valData(),
							bufFrameConf->timeStamp() );
	      fcopyConfs[ BuffName ] = copyBuffFC;
	    }
	  }

	if( ! fcopyBufList->empty() )  frameToSend = fcopyBufList->front();
#ifdef LittleEndian
	else {        // even LockFrameToSend cannot protect this original frame.
	  ::pthread_mutex_unlock( &FcopyBuffersMutex );
	  ::pthread_mutex_unlock( &FrameBuffersMutex );
	  message="ERROR: FrameAcqServer> EMPTY frame copy buffer: " + BuffName;
	  clog << message << " ! " << endl;
	  bfc->rename( message );          //note that object bfc is already pushed into Reply vector.
	  return Reply;
	}
#endif
      }
  }
  // add this frame to the reservedFrames list so it will not get deleted while sending:
  ::pthread_mutex_lock( &reservedFramesMutex );
  reservedFrames.push_front( frameToSend );
  ::pthread_mutex_unlock( &reservedFramesMutex );

  // unlock and let other threads access:
  ::pthread_mutex_unlock( &FcopyBuffersMutex );
  ::pthread_mutex_unlock( &FrameBuffersMutex );

#ifdef LittleEndian
  if( !LockFrameToSend( frameToSend ) )
    {
      message="ERROR: FrameAcqServer> could not Lock frame for byte-swap and send: "+frameToSend->name();
      clog << message << " ! " << endl;
      bfc->rename( message );          //note that object bfc is already pushed into Reply vector.
      unReserveFrame( frameToSend, false );  //unreserve because not going to reply with frame.
      return Reply;
    }
#endif

  Reply.push_back( frameToSend );
  NframesSent++;
  clog << "FrameAcqServer> " << frameToSend->name() <<". \r";

  return Reply;
}
//------------------------------------------------------------------------------------------

bool unReserveFrame( UFProtocol* frame, bool unLock )
{
  const UFProtocolAttributes* fpa = frame->attributesPtr();
  if( fpa->_type != UFProtocol::_Ints_ ) return false;

#ifdef LittleEndian
  if( unLock )      // unlock the frame if it was locked by LockFrameToSend():
    {
      ::pthread_mutex_lock( &LockedFramesMutex );
      if( ! LockedFrames.empty() )
	{
	  frameListType::iterator LfPtr = std::find( LockedFrames.begin(),
						     LockedFrames.end(), frame );
	  if( LfPtr != LockedFrames.end() )
	    LockedFrames.erase( LfPtr );
	}
      ::pthread_mutex_unlock( &LockedFramesMutex );
    }
#endif

  // remove frame from the reservedFrames list if it is in there.
  ::pthread_mutex_lock( &reservedFramesMutex );
  bool found = false;
  if( ! reservedFrames.empty() )
    {
      frameListType::iterator rfPtr = std::find( reservedFrames.begin(),
						 reservedFrames.end(), frame );
      if( rfPtr != reservedFrames.end() ) {
	found = true;
	reservedFrames.erase( rfPtr );
      }
    }
  ::pthread_mutex_unlock( &reservedFramesMutex );
  return found;
}

/**-------------------------------------------------------------------------------------------------
 * This method deletes old frames from the frame-buffer Lists that are NOT being written to a client.
 * This method is called in main thread loop and in FrameGrabThread before start of EDT frame grabs.
 * NOTE: reservedFrames is the global List of frames reserved for sending by FrameServerThreads,
 *       and it is controlled by methods getFramefromBuffer() and unReserveFrame().
 * Inputs:
 *   FrameBuffersType* frmBuffs = mapped Lists of frames in buffers (frameBuffers or fcopyBuffers).
 *
 *   pthread_mutex_t& fBmutex = the mutex for locking frmBuffs.
 *
 *   int MaxNumFrames (with default value of one) specifies the number of frames
 *                    that will always be left in a buffer list (not deleted).
 */

void deleteOldFrames( FrameBuffersType& frmBuffs, pthread_mutex_t& fBmutex, int MaxNumFrames=1 )
{
  frameListType oldFrames ;

  // Order important for these locks
  ::pthread_mutex_lock( &fBmutex );
  ::pthread_mutex_lock( &reservedFramesMutex );

  for( FrameBuffersType::iterator fBPtr = frmBuffs.begin(); fBPtr != frmBuffs.end(); ++fBPtr )
    {
      // Usually require more than one frame in a buffer to consider removing any of them.
      // This specified by input MaxNumFrames, which has default value of one.

      if( fBPtr->second->size() <= (unsigned int )MaxNumFrames ) continue ;

      // For each buffer list, delete old frames if not in reservedFrames list.
      // Usually skip over the first frame in each buffer, this is the most current frame.

      frameListType::iterator fPtr = fBPtr->second->begin();
      for( int i=0; i < MaxNumFrames; i++ ) ++fPtr;          //skip first frames if specified.

      while( fPtr != fBPtr->second->end() )
	{
	  if( std::find( reservedFrames.begin(), reservedFrames.end(), *fPtr ) != reservedFrames.end() )
	    {
	      ++fPtr ; // this frame is reserved (currently in use), so check next frame in buffer...
	      continue ;
	    }

	  // Add frame to the oldFrames list for deletion and erase from frameBuffs:
	  oldFrames.push_back( *fPtr );
	  fPtr = fBPtr->second->erase( fPtr );

	} // frame List iterator
    } // frame Buffer iterator

  ::pthread_mutex_unlock( &reservedFramesMutex );
  ::pthread_mutex_unlock( &fBmutex );

  // Deallocate the frames which are no longer in use:
  for( frameListType::iterator ptr = oldFrames.begin(); ptr != oldFrames.end(); ++ptr )  delete *ptr;
}

//-------------------this method gives public access to deleteOldFrames():--------------

void removeOldFrames( int MaxNumFrames )
{
  if( MaxNumFrames < 0 ) // option to force deletion of ALL frames.
    {
      ::pthread_mutex_lock( &reservedFramesMutex );
      reservedFrames.clear();
      ::pthread_mutex_unlock( &reservedFramesMutex );
    }

  deleteOldFrames( frameBuffers, FrameBuffersMutex, MaxNumFrames );
  deleteOldFrames( fcopyBuffers, FcopyBuffersMutex, MaxNumFrames );

  if( MaxNumFrames < 0 ) // option to deallocate the mapped frame buffers:
    {
      frameBuffers.clear();
      fcopyBuffers.clear();
    }
}
//--------------------------------------------------------------------

void unLockMutexes()
{
      ::pthread_mutex_unlock( &_FrameConfigMutex );
      ::pthread_mutex_unlock( &_ObsConfigMutex );
      ::pthread_mutex_unlock( &FrameBuffersMutex );
      ::pthread_mutex_unlock( &FcopyBuffersMutex );
      ::pthread_mutex_unlock( &reservedFramesMutex );
#ifdef LittleEndian
      ::pthread_mutex_unlock( &LockedFramesMutex );
#endif
}

#endif /* __FrameProcessThread_cc__ */
