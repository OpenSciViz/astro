#if !defined(__FrameSimThread_cc__)
#define __FrameSimThread_cc__ "$Name:  $ $Id: FrameSimThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameSimThread_cc__;

#include "FrameAcqServer.h"
#include "FrameSimThread.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"

pthread_t FrameSimThreadID = 0 ;
static float nodPause = 7.0;

//---------------------------------------------------------------------------------
//Thread to create sequence of simulated Gaussian random noise frames
//and place each one into list that is checked by FrameGrabThread.

void* FrameSimThread( void* )
{
  UFObsConfig* theObsConfig = accessObsConfig( true );
  int Ncoadds = theObsConfig->coaddsPerFrm();
  int NframePerNod = theObsConfig->saveSets() * theObsConfig->chopBeams();
  int nodBeams = theObsConfig->nodBeams();
  int chopBeams = theObsConfig->chopBeams();
  string readOutMode = theObsConfig->readoutMode();
  UFStrings::upperCase( readOutMode );
  theObsConfig = accessObsConfig( false );

  int EDTwidthFactor = 1;
  if( readOutMode.find("S1R3") != string::npos ||
      readOutMode.find("1S3R") != string::npos ) EDTwidthFactor = 4;
  if( readOutMode.find("S1R1") != string::npos ||
      readOutMode.find("1S1R") != string::npos ) EDTwidthFactor = 2;

  bool rawReadOut = false;
  if( readOutMode.find("RAW") != string::npos ) rawReadOut = true;

  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int nx = theFrameConfig->width() * EDTwidthFactor;
  int ny = theFrameConfig->height();
  int NelemFrame = nx * ny;
  float savePeriod = theFrameConfig->savePeriod()/chopBeams;
  theFrameConfig = accessFrameConfig( false );

  float computeTime = 0.3 * EDTwidthFactor; //approx secs to compute a frame on UltraSparc-II
  if( savePeriod <= computeTime ) savePeriod = computeTime + 0.1;

  string message;
  UFInts* thePixelMap = copyPixelMap( message );
  int* pixelMap = 0;                          //default is no pixel mapping.

  if( thePixelMap ) {
    if( thePixelMap->numVals() == NelemFrame ) {
      clog << "FrameSimThread> using the " << thePixelMap->name() <<endl;
      pixelMap = thePixelMap->valInts();
    }
  }

  //Nunirand is the number of Uniformly distributed random variables to sum
  // for approximating a Gaussian (normal) random distribution.
  int Nunirand = 5;
  double meanUnir = Nunirand/2.0;
  double meanFval = Ncoadds*(1<<15); //mean val of frame = 1/2 ADC range.

  clog << "FrameSimThread> simulating " << totalFramesToGrab()
       << " frames, each with " << NelemFrame << " random integers of Gaussian noise."<<endl;
  int inod = 0;
  int nframe=0;

  while( nframe < totalFramesToGrab() || totalFramesToGrab() == 0 )
    {
      for( int ichop=0; ichop < chopBeams; ichop++ ) 
	{
	  if( (nframe % NframePerNod) == 0 && nodBeams > 1 && nframe > 0 && !rawReadOut ) {
	    clog<<"\nFrameSimThread> simulating pause for beam-switch (nodPause="<<nodPause<<"s)"<<endl;
	    UFPosixRuntime::sleep( nodPause );
	    inod = (++inod) % 2;
	    clog<<"\nFrameSimThread> resuming generation of simulation frames..."<<endl;
	  }

	  UFPosixRuntime::sleep( savePeriod - computeTime );
	  UFInts* simFrame = new (nothrow) UFInts( string("simFrame"), NelemFrame );

	  if( ! UFProtocol::validElemCnt( simFrame ) ) {
	    string error = "ERROR: FrameSimThread> failed mem. alloc. for sim frame.";
	    clog << error << endl;
	    FrameGrabAbort(error);
	    break;
	  }

	  if( (nframe % chopBeams) == 0 ) //fluctuate mean value for every src-ref pair:
	    meanFval = Ncoadds*( (1<<15) + 900*(::drand48()-0.5) );
	  double stdevfactor = sqrt( 12*meanFval/Nunirand );
	  int* pixels = simFrame->valInts();

	  for( int i=0; i<NelemFrame; i++ ) { //generate noise frames:
	    double sum=0;
	    //sum uniform random #s to simulate Gaussian noise:
	    for( int j=0; j<Nunirand; j++ ) sum += ::drand48();
	    pixels[i] = (int)rint( meanFval + stdevfactor * (sum - meanUnir) );
	  }

	  if( (nframe % chopBeams) == inod ) { //add fake source values:
	    int fakeSrc = Ncoadds*(int)rint( 10*(::drand48()+7) );
	    int ymax = ny/2 + (int)rint( 40*::drand48() );
	    int ymin = ymax - 40;
	    int xmax, xmin;
	    if( EDTwidthFactor > 1 ) {
	      fakeSrc = -fakeSrc; //for multi-point sampling looks better if neg.
	      xmax = nx/2 + 80;
	      xmin = xmax - 160;
	    } else {
	      xmax = nx/2 + (int)rint( 40*::drand48() );
	      xmin = xmax - 40;
	    }
	    for( int j=ymin; j<ymax; j++ ) {
	      int* fd = pixels + j*nx + xmin;
	      for( int i=xmin; i<xmax; i++ ) *fd++ += fakeSrc;
	    }
	    if( pixelMap && EDTwidthFactor == 1 ) {
	      UFInts* mappedFrame = new (nothrow) UFInts( string("simFrame"), pixels, NelemFrame,
							  false, pixelMap, true ); //bool invertMap=true.
	      if( UFProtocol::validElemCnt( mappedFrame ) ) {
		delete simFrame;
		simFrame = mappedFrame;
	      }
	    }
	  }

#ifdef BigEndian   // to simulate EDT PCI card in SPARC must reverse byte order:
	  simFrame->reverseByteOrder();
#endif
	  FrameGrabSimulate( simFrame ); //push into recvdFramesList of FrameGrabThread().
	  ++nframe;
	} // end of loop of chop beams.
    }     // end of loop over frames.

  clog << "FrameSimThread> terminated."<<endl;
  return 0;
}
//---------------------------------------------------------------------------------

bool createFrameSimThread()
{
  if( ( FrameSimThreadID = UFPosixRuntime::newThread( FrameSimThread ) ) > 0 )
    return true;
  else
    return false;
}
//---------------------------------------------------------------------------------

void setNodPause( float pauseTime )
{
  if( pauseTime > 0 ) {
    nodPause = pauseTime;
    clog << "FrameSimThread> pause for simulating beam-switch = " << nodPause << " seconds." <<endl;
  }
}

#endif /* __FrameSimThread_cc__ */
