#if !defined(__FrameStreamThread_cc__)
#define __FrameStreamThread_cc__ "$Name:  $ $Id: FrameStreamThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameStreamThread_cc__;

#include "FrameAcqServer.h"
#include "FrameStreamThread.h"
#include "FrameWriteThread.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"

// The thread ID and blocking mutex for starting/pausing the FrameStreamThread.

pthread_t FrameStreamThreadID = 0 ;
pthread_mutex_t FrameStreamMutex = PTHREAD_MUTEX_INITIALIZER ;
static bool _keepRunning = true ;

// The List used to store pointers to frames that are to be sent as a stream to DHS.

frameListType _sendFramesList ;
pthread_mutex_t _sendFramesListMutex = PTHREAD_MUTEX_INITIALIZER ;
int _maxFrmWaitSend = 300;     // default max # of frames allowed in waiting to send List.
int _troubleFrmWaitSend = 150; // threshold # of frames waiting to send indicating trouble.
int _totalFramesStreamed = 0 ; // counts total # of frames sent to stream client.
int _saveTotFrmStreamed = 0 ;  // save the count for checking for send problem.
bool _firstSend = false; //set true when first enabled to force sending ObsConfig.

/*
 * The client socket that should be sent stream of raw frames:
 * See method: bool enableFrameStream( UFSocket* )
 */
UFSocket* _clientStreamSocket = 0;
pthread_mutex_t _clientStreamMutex = PTHREAD_MUTEX_INITIALIZER ;

//****************************************************************************************

bool enableFrameStream( UFSocket* clientSocket )
{
  ::pthread_mutex_lock( &_clientStreamMutex );
  bool enabled = false;

  if( _clientStreamSocket == 0 )
    {
      _clientStreamSocket = clientSocket;
      clog<<"FrameAcqServer> client socket is enabled for frame stream."<<endl;
      enabled = true;
      _firstSend = true;
      clearSendList(); //start with empty list of frames (nothing to send).
      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
      theFrameConfig->rename("enableFrameStream> DHS client has re-connected.");
      clog << theFrameConfig->name() <<endl;
      theFrameConfig = accessFrameConfig( false );
    }
  else if( _clientStreamSocket == clientSocket )
    {
      clog<<"FrameAcqServer> client socket already enabled for frame stream."<<endl;
      enabled = true;
    }
  else clog<<"FrameAcqServer> frame stream already in use."<<endl;

  ::pthread_mutex_unlock( &_clientStreamMutex );
  return enabled;
}
//-----------------------------------------------------------------------------------------

bool disableFrameStream( UFSocket* clientSocket )
{
  int notLocked = ::pthread_mutex_trylock( &_clientStreamMutex );

  if( _clientStreamSocket == clientSocket ) {
    if( notLocked )
      ::pthread_mutex_lock( &_clientStreamMutex );
    _clientStreamSocket = 0;
    ::pthread_mutex_unlock( &_clientStreamMutex );
    clog<<"FrameAcqServer> DHS client has disabled frame stream."<<endl;
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename("ERROR: FrameAcqServer> DHS client has disconnected.");
    theFrameConfig = accessFrameConfig( false );
    return true;
  }
  else if( _clientStreamSocket > 0 )
    clog<<"FrameAcqServer> this client is not allowed to disable frame stream!"<<endl;

  if( notLocked == 0 )
    ::pthread_mutex_unlock( &_clientStreamMutex );
  else
      clog<<"FrameAcqServer> _clientStreamMutex is locked by different client."<<endl;

  return false;
}
//-----------------------------------------------------------------------------------------

bool createFrameStreamThread( int MaxFramesWaitSend )
{
  if( MaxFramesWaitSend > 1 ) _maxFrmWaitSend = MaxFramesWaitSend;
  _keepRunning = true;

  if( ( FrameStreamThreadID = UFPosixRuntime::newThread( FrameStreamThread ) ) <= 0 )
    return false;
  else
    return true;
}
//----------------------------------------------------------------------------------------------

void* FrameStreamThread( void* )
{
  // This method runs as a thread, popping frames from the sendFramesList,
  //  and sending them to the data replication stream client socket.
  clog << "FrameStreamThread> max # frames allowed waiting in send List = "
       << _maxFrmWaitSend << endl;

  while( _keepRunning )
    {
      //mutex must be locked before checking if sendFramesList is empty!
      ::pthread_mutex_lock( &FrameStreamMutex );
      
      if( sendListEmpty() ) {
	if( grabListEmpty() ) { //check if observation was aborted:
	  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	  int FrameTotal = theFrameConfig->frameObsSeqTot();
	  theFrameConfig = accessFrameConfig( false );
	  if( FrameTotal < 0 ) {
	    UFPosixRuntime::sleep( 3.0 ); //make sure FrameProcessThread is done with frames...
	    if( sendListEmpty() )
	      sendStopToClient(); //if obs was aborted and all frames sent, send STOP message.
	  }
	}
	// second try to lock mutex will block and wait if no frame is waiting to send.
	::pthread_mutex_lock( &FrameStreamMutex );
	//mutex is unlocked when new frame is copied to send (in method streamFrame() ).
      }

      ::pthread_mutex_unlock( &FrameStreamMutex ); //Unlock second mutex to reset for next time.
      if( !_keepRunning ) break;

      if( sendListSize() > 0 ) //make sure there is a frame (an abort signals dummy frame).
	{
	  ::pthread_mutex_lock( &_sendFramesListMutex );
	  UFInts* frameToSend = _sendFramesList.back();
	  ::pthread_mutex_unlock( &_sendFramesListMutex );

	  if( sendFrameToClient( frameToSend ) )
	    {
	      delete frameToSend;
	      ::pthread_mutex_lock( &_sendFramesListMutex );
	      _sendFramesList.pop_back();
	      ::pthread_mutex_unlock( &_sendFramesListMutex );
	    }
	  else UFPosixRuntime::sleep( 7.0 );
	}
    }

  sendShutdown();
  clearSendList();
  clog << "FrameStreamThread> terminated."<<endl;
  return 0;
}
//-----------------------------------------------------------------------------------------

bool sendFrameToClient( UFInts* frame )
{
  if( ! UFProtocol::validElemCnt( frame ) ) {
    clog << "sendFrameToClient> arg UFInts* frame is NOT valid object!" <<endl;
    return false;
  }

  ::pthread_mutex_lock( &_clientStreamMutex );
  bool wrote = true;

  if( _clientStreamSocket > 0 )
    {
      if( _clientStreamSocket->writable() > 0 )
	{
	  if( frame->seqCnt() == 1 || _firstSend ) { //start with ObsConfig and FrameConfig:
	    string message;
	    UFObsConfig* theObsConfig = copyObsConfig( message );
	    if( _clientStreamSocket->send( *theObsConfig ) < theObsConfig->length() ) {
	      message = "ERROR: FrameStreamThread> failed to send ObsConfig to DHS client!";
	      clog << message <<endl;
	      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	      theFrameConfig->rename( message );
	      theFrameConfig = accessFrameConfig( false );
	    }
	    delete theObsConfig;
	    UFFrameConfig* theFrameConfig = copyFrameConfig();
	    _clientStreamSocket->send( *theFrameConfig );
	    delete theFrameConfig;
	    UFStrings* FITSheader = getFITSheader();
	    _clientStreamSocket->send( *FITSheader );
	    delete FITSheader;
	    _firstSend = false;
	  }
	  else if( frame->seqCnt() == frame->seqTot() ) sendFinalFITSheader(); //before last frame!

	  if( _clientStreamSocket->send( *frame ) < frame->length() ) {
	    wrote = false;
	    string message = "ERROR: FrameStreamThread> failed to send frame to DHS client!";
	    clog << message <<endl;
	    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	    theFrameConfig->rename( message );
	    theFrameConfig = accessFrameConfig( false );
	  } else {
	    ++_totalFramesStreamed;
	    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	    theFrameConfig->frameSendCnt( theFrameConfig->frameSendCnt() + 1 );
	    string message = theFrameConfig->name();
	    if( message.find("ERROR: FrameStream") != string::npos ||
		message.find("WARNING: FrameStream") != string::npos ) {
	      theFrameConfig->rename("FrameStreamThread> resumed sending frames to DHS client.");
	      clog << theFrameConfig->name() <<endl;
	    }
	    theFrameConfig = accessFrameConfig( false );
	  }
	}
      else {
	wrote = false;
	string message = "ERROR: FrameStreamThread> DHS client socket NOT writable!";
	clog << message <<endl;
	UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	theFrameConfig->rename( message );
	theFrameConfig = accessFrameConfig( false );
      }
    }

  ::pthread_mutex_unlock( &_clientStreamMutex );
  return wrote;
}
//-----------------------------------------------------------------------------------------
int totalFramesStreamed() { return _totalFramesStreamed; }
//-----------------------------------------------------------------------------------------

void sendFinalFITSheader()
{
  int maxTry = 7, nTry=0;
  clog<<"FrameStreamThread> waiting for Final FITS header..."<<endl;
  UFPosixRuntime::sleep( 2.0 );
  UFStrings* FITSheader = getFITSheader();
  while( (FITSheader->name()).find("FINAL") == string::npos && ++nTry < maxTry ) {
    UFPosixRuntime::sleep( 1.0 );
    delete FITSheader;
    FITSheader = getFITSheader();
  }
  _clientStreamSocket->send( *FITSheader );
  clog<<"FrameStreamThread> sent "<<FITSheader->name()<<" after "<<nTry+2<<" seconds."<<endl;
  delete FITSheader;
}
//-----------------------------------------------------------------------------------------

void sendStopToClient()
{
  ::pthread_mutex_lock( &_clientStreamMutex );

  if( _clientStreamSocket > 0 )
    {
      if( _clientStreamSocket->writable() > 0 )
	{
	  sendFinalFITSheader(); //before last send!
	  UFTimeStamp stop("STOP");
	  if( abortDHS() ) stop.rename("ABORT");
	  clog<<"FrameStreamThread> sending "<<stop.name()<<" message to stream client (DHS)"<<endl;
	  UFObsConfig* theObsConfig = accessObsConfig( true );
	  stop.relabel( theObsConfig->dataLabel() );
	  theObsConfig = accessObsConfig( false );
	  _clientStreamSocket->send( stop );
	}
    }

  ::pthread_mutex_unlock( &_clientStreamMutex );
}
//-----------------------------------------------------------------------------------------

void sendShutdown()
{
  ::pthread_mutex_lock( &_clientStreamMutex );

  if( _clientStreamSocket > 0 )
    {
      if( _clientStreamSocket->writable() > 0 )
	{
	  UFTimeStamp shutdown("SHUTDOWN");
	  clog<<"FrameStreamThread> sending "<<shutdown.name()<<" message to stream client (DHS)"<<endl;
	  _clientStreamSocket->send( shutdown );
	}
    }

  ::pthread_mutex_unlock( &_clientStreamMutex );
}
//----------------------------------------------------------

bool sendListEmpty()
{
  ::pthread_mutex_lock( &_sendFramesListMutex );
  bool empty_status = _sendFramesList.empty();
  ::pthread_mutex_unlock( &_sendFramesListMutex );

  return empty_status;
}
//----------------------------------------------------------

int sendListSize()
{
  ::pthread_mutex_lock( &_sendFramesListMutex );
  int nelem = (int)_sendFramesList.size();
  ::pthread_mutex_unlock( &_sendFramesListMutex );

  return nelem;
}
//----------------------------------------------------------

void clearSendList()
{
  ::pthread_mutex_lock( &_sendFramesListMutex );
  if( !_sendFramesList.empty() ) {
    clog<<"FrameAcqServer> "<<_sendFramesList.size()
	<<" frames still in stream _sendFramesList, deleting them..."<<endl;
    for( frameListType::iterator pf = _sendFramesList.begin(); pf != _sendFramesList.end(); ++pf )
      delete *pf;
  }
  _sendFramesList.clear();
  ::pthread_mutex_unlock( &_sendFramesListMutex );
}
//---------------------------------------------------------------------------------
//Method streamFrame() is called by FrameProcessThread() to load data frames into send List.
//This should not try locking _clientStreamMutex because it may be stuck in sendFrameToClient()
// if socket is not working, and then the process thread would get stuck.

bool streamFrame( UFInts* sendMe )
{
  if( _clientStreamSocket == 0 ) return true; //no client so do not send.

  if( ! UFProtocol::validElemCnt( sendMe ) ) {
    clog << "streamFrame> arg UFInts* sendMe is NOT valid object!" <<endl;
    return false;
  }

  //if data label is discard-all or no-dhs then do not send to DHS:
  string dlab = sendMe->datalabel();
  UFStrings::lowerCase(dlab);
  if( (dlab.find("discard") != string::npos && dlab.find("all") != string::npos) ||
      (dlab.find("no")      != string::npos && dlab.find("dhs") != string::npos) )
    return true;

  if( sendListSize() > _troubleFrmWaitSend ) {
    string status = "WARNING: FrameStream> DHS client not keeping up? frame:";
    if( _saveTotFrmStreamed == 0 )
      _saveTotFrmStreamed = _totalFramesStreamed; //init checker.
    else if( _saveTotFrmStreamed == _totalFramesStreamed ) //if not changing then error:
      status = "ERROR: FrameStream> DHS client not receiving frames:";
    bool exceeded = ( sendListSize() > _maxFrmWaitSend );
    if( exceeded )
      status = "ERROR: FrameStream> DHS client way behind, dropping frame:";
    status += ( UFRuntime::numToStr(sendMe->seqCnt()) + ":" + sendMe->name() );
    clog << status <<endl;
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(status);
    theFrameConfig = accessFrameConfig( false );
    if( exceeded )
      return false;
  }
  else _saveTotFrmStreamed = 0; //reset checker;

  // Copy frame and push it to the front of sendFramesList for the FrameSendThread:
  UFInts* frameToSend = new (nothrow) UFInts( sendMe->name(), sendMe->valInts(), sendMe->numVals() );

  if( UFProtocol::validElemCnt( frameToSend ) )
    {
      frameToSend->setSeq( sendMe->seqCnt(), sendMe->seqTot() );
      ::pthread_mutex_lock( &_sendFramesListMutex );
      _sendFramesList.push_front( frameToSend );
      ::pthread_mutex_unlock( &_sendFramesListMutex );
      ::pthread_mutex_unlock( &FrameStreamMutex );
      return true;
    }
  else {
    string status = "ERROR: streamFrame> FAILED mem. alloc. for copy of " + sendMe->name();
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(status);
    theFrameConfig = accessFrameConfig( false );
    clog << status << "! " <<endl;
    return false;
  }
}

void FrameStreamStart() { ::pthread_mutex_unlock( &FrameStreamMutex ); }

void FrameStreamShutdown()
{
  _keepRunning = false;
  ::pthread_mutex_unlock( &_clientStreamMutex );
  ::pthread_mutex_unlock( &_sendFramesListMutex );
  ::pthread_mutex_unlock( &FrameStreamMutex );
}

#endif /* __FrameStreamThread_cc__ */
