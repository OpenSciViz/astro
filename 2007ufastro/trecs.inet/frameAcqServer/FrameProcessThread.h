#if !defined(__FrameProcessThread_h__)
#define __FrameProcessThread_h__ "$Name:  $ $Id: FrameProcessThread.h,v 1.3 2003/09/24 22:28:32 varosi beta $"

/*
 * The FrameProcessThread pops UFInts frame pointers from the grabbedFramesList, maps pixels,
 *  computes net samples if in multi-sample readout mode, and pushes a copy of each data frame
 *   to the FrameWriteThread & FrameStreamThread (pixel byte orders reversed if needed).
 *  Frame pointers are moved into the frameBuffers (named frame Lists) for processing and quick-look:
 *   frames are accumulated into the "accum()" buffers, difference buffers,
 *   and then the final signal buffer is computed (depending on the ObsConfig).
 *  The contents of any buffer will be sent to any client requesting the buffer (see below).
 *  Old frames are periodically deleted from the frameBuffers by calling removeOldFrames().
 *  The current FrameConfig with list of updated buffers is copied to a notify stream
 *   after each frame is processed, to be sent to all clients that have requested the "NOTIFY"
 *   option (see FrameNotifyThread).
 *
 * Buffer names are defined in method FrameProcessThread(),
 * and they are taken directly from the OSCIR/IDL cam.pro (by Robert Pina).
 *
 * Frank Varosi, July 9, 2003.
 */

/*
 * This structure is used as a functor to provide case insensitive std::string comparisons
 * for the mapped frameBuffer names, into which grabbed frames are placed.
 */
struct noCaseCompare
{
  inline bool operator()( const string& lhs, const string& rhs ) const
  {
    return (::strcasecmp( lhs.c_str(), rhs.c_str() ) < 0) ;
  }
};

/*
 * The type of structure used to store the grabbed UFInts frames into mapped buffers,
 *  and to store the FrameConfig of each frame buffer,
 *  for keeping track of things like the number of coadds in each frame buffer.
 */

typedef map< string, frameListType*, noCaseCompare > FrameBuffersType ;
typedef map< string, UFFrameConfig*, noCaseCompare > FrameConfsType ;

//------------------------------------------------------------------------------------------------

extern void* FrameProcessThread( void* ) ;
extern bool createFrameProcessThread() ;
extern string processFrame( UFInts* newFrame, vector< string >& bufferNames ) ;
extern bool createBuffers( vector< string >& names, FrameBuffersType& frmBuffs, FrameConfsType& frmCons );
extern void deleteOldFrames( FrameBuffersType* frmBuffs, pthread_mutex_t& fBmutex, int MaxNumFrames=1 );
extern void removeOldFrames( int MaxNumFrames=1 ) ;
extern void computeChopDiff( string accum = "" ) ;
extern void computeNodSum() ;
extern void setStatsRegion( UFInts* sregion ) ;
extern UFStrings* getStatsRegion() ;
extern void computeStats( UFInts* refnew, UFInts* refold,
			  int CoaddsPerFrame, int& ADUavg, float& percentWell, float& sigma ) ;
extern void minMaxFrameStats() ;
extern int FP_NframesSent() ;
extern int FP_NframesCopied() ;
extern int FP_setLockWaitTimes( int waitTime=100, int maxWait=7000 ) ;
extern int FP_LockWaits() ;
extern int FP_maxLockWait() ;
extern void FP_Start() ;
extern void FP_Shutdown() ;
extern void ResetCounters() ;
extern void unLockMutexes() ;
extern bool unReserveFrame( UFProtocol* frame, bool unLock=true );
extern UFFrameConfig* accessFrameConfig( bool Lock );
extern UFObsConfig*   accessObsConfig( bool Lock );
extern UFFrameConfig* copyFrameConfig();
extern UFObsConfig*   copyObsConfig( string& message );
extern UFStrings*     getBufferNames();
extern string defineFrameConfig( UFFrameConfig* fc );
extern string defineObsConfig( UFObsConfig* oc );
extern vector< UFProtocol* > getFramefromBuffer( string BuffName );

#endif /* __FrameProcessThread_h__ */
