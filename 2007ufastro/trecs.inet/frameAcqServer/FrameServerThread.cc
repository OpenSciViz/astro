#if !defined(__FrameServerThread_cc__)
#define __FrameServerThread_cc__ "$Name:  $ $Id: FrameServerThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameServerThread_cc__;

#include "FrameAcqServer.h"
#include "UFServerSocket.h"
#include "FrameServerThread.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"
#include "FrameNotifyThread.h"
#include "FrameSimThread.h"
#include "FrameStreamThread.h"
#include "FrameWriteThread.h"

// Type & object used to store info about the currently running client FrameServerThreads:

typedef map< UFSocket*, pthread_t > threadListType ;
threadListType _serverThreadList ;
pthread_mutex_t _serverThreadListMutex = PTHREAD_MUTEX_INITIALIZER ;

typedef set< UFSocket* > socketListType ;
socketListType _clientSocketList ;

pthread_t _clientListenThreadID = 0 ;
bool _serverLogging = false ;    //default is no Log files in FrameServerThreads.

static int _Nreplies = 0 ;      //-- count total # of replies by all FrameServerThreads:
static int _Total_Clients = 0 ; //count total number of clients ever connected.
UFTimeStamp* _serverVersion = 0;

//***********************************************************************************************

bool createListenThread( unsigned short int& ServerPort, UFTimeStamp* ServerVersion )
{
  if( ServerVersion )
    {
      if( _serverVersion ) delete _serverVersion;
      _serverVersion = ServerVersion;
    }

  if( (_clientListenThreadID = UFPosixRuntime::newThread( clientListenThread,
							 static_cast<void*>(&ServerPort) )) > 0 )
    return true;
  else
    return false;
}
//-----------------------------------------------------------------------------------------------

void* clientListenThread( void* data )
{
  // The input data must be pointer to the server's port number.
  // Listens for socket connections on port and creates a FrameServerThread for each new client.

  unsigned short int* serverPort = static_cast< unsigned short int* >( data ) ;
  UFServerSocket* serverSocket = new (nothrow) UFServerSocket( *serverPort ) ;

  if( serverSocket == 0 )
    {
      clog << "clientListenThread> UFServerSocket memory allocation failure!" <<endl;
      return 0 ;
    }

  if( serverSocket->listen().listenFd < 0 )
    {
      clog << "clientListenThread> Error in listen(): " << strerror(errno) <<endl;
      return 0 ;
    }

  clog << "clientListenThread> Listening for connections on port # " << *serverPort <<endl;

  while( serverSocket )
    {
      UFSocket clientSock ;

      if( serverSocket->accept( clientSock ) < 0 )
	{
	  clog << "\n clientListenThread> accept() failed: " << strerror( errno ) << endl ;
	  continue ;
	}

      // Make a copy of new socket connection for adding to the _serverThreadList,
      // and then create a FrameServerThread for the new client.

      clog << "\n clientListenThread> Accepted new connection.\n" ;
      UFSocket* newClientSock = new (nothrow) UFSocket( clientSock ) ;

      if( newClientSock == 0 ) {
	clog << "ERROR: clientListenThread> new UFSocket memory allocation failure!"<<endl;
	continue ;
      }

      pthread_t FSTid = 0 ;

      if( (FSTid = UFPosixRuntime::newThread( FrameServerThread,
					      static_cast<void*>(newClientSock) )) > 0 )
	{
	  // Add info of this FrameServerThread to the _serverThreadList:
	  ::pthread_mutex_lock( &_serverThreadListMutex ) ;
	  _serverThreadList.insert( threadListType::value_type( newClientSock, FSTid ) );
	  _clientSocketList.insert( newClientSock );
	  clog << "clientListenThread> created new FrameServerThread for client: "
	       << _serverThreadList.size() << " clients are now connected."<<endl;
	  ::pthread_mutex_unlock( &_serverThreadListMutex ) ;
	}
      else
	{
	  clog << "ERROR: clientListenThread> Failed to create new FrameServerThread: "
	       << strerror( errno ) << endl ;
	  newClientSock->close();
	  delete newClientSock;
	}
    }
  // The server socket will be officially closed and deallocated by the main() thread.
  clog << "clientListenThread> Exiting normally." <<endl;
  return 0 ;
}
//-----------------------------------------------------------------------------------------------

void setServerLogs( bool Logging ) { _serverLogging = Logging; }

//-----------------------------------------------------------------------------------------------

void LogServerMessage( ofstream& serverLogFile, string message, int Loop, int count )
{
  strstream LogStream;
  LogStream << message << ": Loop = " << Loop;
  if( count > 0 ) LogStream << ", count = " << count;
  LogStream << "\n" <<ends;
  string LogMess = LogStream.str();
  serverLogFile.write( LogMess.c_str(), LogMess.length() );
  serverLogFile.flush();
  delete [] LogStream.str();
}
//-----------------------------------------------------------------------------------------------

void* FrameServerThread( void* data )
{
  // A separate thread will run this method for each client that is connected to the server.
  UFPosixRuntime::sleep(0.1) ;
  string clientNumber = UFRuntime::numToStr( ++_Total_Clients );
  string FSthreadName = "FrameServerThread." + clientNumber;

  // Obtain a pointer to the client's socket
  UFSocket* clientSock = static_cast< UFSocket* >( data ) ;

  if( NULL == clientSock )
    {
      clog << FSthreadName << "> static_cast< UFSocket* > failed!" <<endl;
      return 0 ;
    }

  // Optional stream file for Logging messages when _serverLogging = true:
  ofstream serverLogFile ;

  if( _serverLogging ) {
    strstream filename;
    filename << "FSthread" << _Total_Clients << ".log" <<ends;
    string LogFileName = filename.str();
    delete filename.str();
    clog << FSthreadName << "> opening Log file: " << LogFileName <<endl;
    serverLogFile.open( LogFileName.c_str(), ios::out );
  }

  // This variable indicates that the client is currently connected
  bool isConnected = true ;
  int NemptyReads = 0;
  int Loop = 0;
  clog << FSthreadName << "> Processing requests from new client..." <<endl;

  while( isConnected )
    {
      if( _serverLogging ) {
	serverLogFile.seekp(0);
	LogServerMessage( serverLogFile, FSthreadName + "> checking readable at beginning", ++Loop );
      }

      // Check if any data is available for reading
      int readable = clientSock->readable( -1.0 ) ;

      if( readable < 0 )  // client is no longer connected
	{
	  clog << FSthreadName << "> Error in readable(): " << strerror( errno ) << endl ;
	  isConnected = false ;
	  continue ;
	}
      else if( readable == 0 )
	{
	  if( _serverLogging )
	    LogServerMessage( serverLogFile, FSthreadName + "> nothing to read", Loop, ++NemptyReads );
	  continue; // No data, skip the read and try again.
	}

      UFProtocol* clientRequest = UFProtocol::createFrom( *clientSock ) ;

      if( clientRequest == 0 )
	{
	  clog << FSthreadName << "> Read error: Lost connection? " << strerror( errno ) << endl ;
	  isConnected = false ;
	  continue ;
	}

      if( _serverLogging )
	LogServerMessage( serverLogFile, "client request> " + clientRequest->name(), Loop, _Nreplies );

      // Process the request recvd from client.
      // First check if it is a special request for notification stream or data stream,
      // otherwise, handleRequest() returns pointer (reply) to objects to be sent to the client.
      vector< UFProtocol* > reply;

      if( ! specialRequest( clientRequest, clientSock, FSthreadName, reply ) )
	  reply = handleRequest( clientNumber, clientRequest ); //normal client/server transaction.
      
      if( reply.empty() ) continue;
      // send back vector of replies (usually just one) to the client:

      for( vector< UFProtocol* >::iterator ptr = reply.begin() ; ptr != reply.end() ; ++ptr )
        {
	  int bytesWritten = clientSock->send( *ptr ) ;

	  if( bytesWritten < 0 )
	    {
	      clog << "ERROR: " << FSthreadName << "> error on send to client: "
		   << strerror( errno ) << endl ;
	      isConnected = false ;  // client connection no longer valid
	      if( _serverLogging )
		LogServerMessage( serverLogFile, FSthreadName + "> error on send", Loop, _Nreplies );
	      break ;
	    }

	  _Nreplies++ ;
	  if( _serverLogging )
	    LogServerMessage( serverLogFile, FSthreadName + "> sent reply", Loop, _Nreplies );
	}

      // Check vector of replies and remove each frame from the reserved frames list if it is in there,
      // or else delete the reply object if it is NOT a frame in the reserved frames list.
      // (The reserved frames list keeps frames in memory for sending to clients.
      //  Deleting one of them here would cause error later when removeOldFrames trys to delete it.)

      for( vector< UFProtocol* >::size_type i = 0 ; i < reply.size() ; ++i )
	{
	  if( _serverLogging )
	    LogServerMessage( serverLogFile, FSthreadName + "> calling unReserveFrame/delete", Loop );

	  if( ! unReserveFrame( reply[i] ) ) delete reply[i];
	}

      if( _serverLogging )
	LogServerMessage( serverLogFile, FSthreadName + "> at end of recv/send Loop", Loop );
 
    } // end while( isConnected )

  if( _serverLogging )
    LogServerMessage( serverLogFile, FSthreadName + "> client is disconnecting", Loop );

  clog << FSthreadName << "> client is disconnecting: closing socket..." <<endl;
  clientSock->close();
  // Remove this client from from notify and stream client Lists:
  removeNotifyList( clientSock );
  disableFrameStream( clientSock );

  // Remove this client from the server<->client List:
  ::pthread_mutex_lock( &_serverThreadListMutex ) ;
  _clientSocketList.erase( clientSock ) ;
  threadListType::iterator tPtr = _serverThreadList.find( clientSock ) ;
  delete clientSock ;

  if( tPtr == _serverThreadList.end() )
    clog << FSthreadName << "> Unable to find client socket in _serverThreadList!\n";
  else
    _serverThreadList.erase( tPtr ) ; // erase the client's UFSocket pointer from the List:

  clog<<FSthreadName<<"> exiting... "<<_serverThreadList.size()<<" clients are still connected."<<endl;
  ::pthread_mutex_unlock( &_serverThreadListMutex ) ;

  if( _serverLogging ) {
    LogServerMessage( serverLogFile, FSthreadName + "> client socket is closed: exiting", Loop );
    serverLogFile.close();
  }
  return 0;
}
//----------------------------------------------------------------------------------------

void stopAllNotifications( string& threadName )
{
  ::pthread_mutex_lock( &_serverThreadListMutex ) ;
  for( socketListType::iterator pClientSock = _clientSocketList.begin();
       pClientSock != _clientSocketList.end(); pClientSock++ )
    {
      UFSocket* clientSock = *pClientSock;
      if( inNotifyList( clientSock ) ) {
	clog << threadName << "> closing notify socket of server thread id = "
	     <<_serverThreadList[ clientSock ]<<endl;
	clientSock->close();
	UFPosixRuntime::sleep(0.1) ;
      }
    }
  ::pthread_mutex_unlock( &_serverThreadListMutex ) ;
}
//----------------------------------------------------------------------------------------

void closeOtherServerSockets( UFSocket* currentClientSocket, string& threadName )
{
  //After each socket is closed, the corresponding FrameServerThread automatically detects
  // the closure and terminates itself, after removing the UFSocket objects from all lists.
  ::pthread_mutex_lock( &_serverThreadListMutex ) ;

  for( socketListType::iterator pClientSock = _clientSocketList.begin();
       pClientSock != _clientSocketList.end();  pClientSock++ )
    {
      UFSocket* clientSock = *pClientSock;
      if( clientSock != currentClientSocket ) {
	clog << threadName << "> closing socket of server thread id = "
	     <<_serverThreadList[ clientSock ]<<endl;
	clientSock->close();
	UFPosixRuntime::sleep(0.1) ;
      }
    }
  ::pthread_mutex_unlock( &_serverThreadListMutex ) ;
}
//----------------------------------------------------------------------------------------

bool specialRequest( UFProtocol* clientRequest, UFSocket* clientSocket, string& threadName,
		     vector< UFProtocol* >& reply )
{
  string request = clientRequest->name();
  UFStrings::upperCase( request );

  // If the client has requested to be notified of new frames acquired,
  //  or requested to be sent a stream of replicated data frames, then
  //  add client to notification List or enable stream (reply only if operation failed).

  if( request.find("NOTIFY") != string::npos ) {
    string message;
    if( request.find("NO-NOTIFY") != string::npos ) {
      removeNotifyList( clientSocket );
      message = "Client will NOT be notified of new frames via FrameConfig object stream.";
    }
    else {
      if( addtoNotifyList( clientSocket ) )
	message = "Client will be notified of new frames via FrameConfig object stream.";
      else {
	message = "ERROR: Failed to add client to notify List (to many clients in List).";
	reply.push_back( new (nothrow) UFTimeStamp( message ) );	
      }
    }
    clog << threadName << "> " << message <<endl;
    return true;
  }
  else if( request.find("REPLICA") != string::npos ||
	   request.find("DATASTREAM") != string::npos ) {
    string message;
    if( request.find("NO-REPLICA") != string::npos ||
	request.find("NO-DATASTR") != string::npos ) {
      if( disableFrameStream( clientSocket ) )
	message = "Client will NOT be sent stream of raw data frames.";
      else {
	message = "ERROR: can not disable frame data stream!";
	reply.push_back( new (nothrow) UFTimeStamp( message ) );	
      }
    } else {
      if( enableFrameStream( clientSocket ) )
	message = "Client will be sent stream of raw data frames.";
      else {
	message = "ERROR: frame data stream already used by another client!";
	reply.push_back( new (nothrow) UFTimeStamp( message ) );	
      }
    }
    clog << threadName << "> " << message <<endl;
    return true;
  }
  else if( request == "CLOSE-ALL-OTHER-SERVER-SOCKETS" ) {
    closeOtherServerSockets( clientSocket, threadName );
    UFPosixRuntime::sleep(1.0) ;
    string message = threadName + "> closed all other server sockets.";
    clog << message <<endl;
    reply.push_back( new (nothrow) UFTimeStamp( message ) );	
    return true;
  }
  else if( request == "STOP-ALL-NOTIFICATIONS" ) {
    stopAllNotifications( threadName );
    string message = threadName + "> stopped all client notification streams.";
    clog << message <<endl;
    reply.push_back( new (nothrow) UFTimeStamp( message ) );	
    return true;
  }
  return false;
}
//----------------------------------------------------------------------------------------

vector< string > reportIOstats()
{
  vector< string > iostats;
  ::pthread_mutex_lock( &_serverThreadListMutex ) ;
  int Nclients = _serverThreadList.size();
  ::pthread_mutex_unlock( &_serverThreadListMutex ) ;

  strstream ss1;
  ss1 << "#connects=" << _Total_Clients
      << ", #clients=" << Nclients
      << ", #replies=" << _Nreplies+1 << "." <<ends; //include this reply (incremented later)
  string message = ss1.str();
  delete[] ss1.str();
  clog << "\n" << message << " ";
  iostats.push_back( message );

  strstream ss2;
  ss2 << "notifies:" << " #sent=" << totalNotifiesSent()
      << ", #err=" << totalNotifiesError()
      << ", #drop=" << totalNotifiesDropped()
      << ", #pend=" << totalNotifiesPending() << "." <<ends;
  message = ss2.str();
  delete[] ss2.str();
  clog << message << endl;
  iostats.push_back( message );

  strstream ss3;
  ss3 << "frames: #grabbed=" << FrameGrabNframes()
      << ", #copied=" << FP_NframesCopied()
      << ", #sent=" << FP_NframesSent()
      << ", #streamed=" << totalFramesStreamed();
#ifdef LittleEndian
  ss3 << ", #LockWaits=" << FP_LockWaits() << ", maxLockWait=" << FP_maxLockWait() << "ms.";
#else
  ss3 << ".";
#endif
  ss3 << ends;
  message = ss3.str();
  delete[] ss3.str();
  clog << message << endl;
  iostats.push_back( message );

  strstream ss4;
  ss4 << "obs:" << " #ok=" << FrameGrabNobsComp()
      << ", #abort=" << FrameGrabNobsAbort()
      << ", #err=" << FrameGrabNobsError()
      << " (Touts=" << FrameGrabTimeOuts()
      << "), #files=" << NfilesCreated() << "." <<ends;
  message = ss4.str();
  delete[] ss4.str();
  clog << message << endl;
  iostats.push_back( message );
  return iostats;
}
//-------------------------------------------------------------------------------------------
//The following methods handle requests from a FrameServerThread and create a reply for each:
 
vector< UFProtocol* > handleRequest( string& clientNum, UFProtocol* clientRequest )
{
  vector< UFProtocol* > Reply ;

  switch( clientRequest->typeId() )
    {
    case UFProtocol::_TimeStamp_:
      Reply = handleRequest( clientNum, dynamic_cast< UFTimeStamp* >( clientRequest ) ) ;
      break ;
    case UFProtocol::_Strings_:
      Reply = handleRequest( clientNum, dynamic_cast< UFStrings* >( clientRequest ) ) ;
      break ;
    case UFProtocol::_ObsConfig_:
      Reply = handleRequest( clientNum, dynamic_cast< UFObsConfig* >( clientRequest ) ) ;
      break ;
    case UFProtocol::_FrameConfig_:
      Reply = handleRequest( clientNum, dynamic_cast< UFFrameConfig* >( clientRequest ) ) ;
      break ;
    case UFProtocol::_Ints_:
      Reply = handleRequest( clientNum, dynamic_cast< UFInts* >( clientRequest ) ) ;
      break ;
    default:
      strstream ss;
      ss << "ERROR: FrameServerThread." + clientNum << "> Rejected UFProtocol object: "
	 << clientRequest->typeId() << ", name: " << clientRequest->name() <<ends;
      string message = ss.str();      delete ss.str();
      clog << "\n" << message << endl;
      Reply.push_back( new UFTimeStamp( message ) );
      delete clientRequest ;
    }

  return Reply ;
}
//------------------------------------------------------------UFTimeStamp------------

// Handle a UFTimeStamp request by name field:

// A UFTimeStamp object received from the client may request a UFLib object
// representing the current state and server info or data frames,
// and the object requested is indicated by the name field:
//
//  Name = "FC", server sends current FrameConfig, with imagecnt, DMAcnt, frame counts,
//                     in which the name field gives current status
//                     and timestamp gives the time current frame was grabbed.
//
//  Name = "CT", server sends UFTimeStamp with current Camera Type in the name field,
//                     and timestamp gives the start time of observation.
//
//  Name = "VERSION", sends UFTimeStamp with name field giving server code version.
//                     and timestamp gives the start time of FrameAcqServer execution.
//
//  Name = "HOST", server sends UFTimeStamp with name field giving name of host computer.
//                     and timestamp gives the current time on the server host.
//
//  Name = "FH", server sends current FITS Header (UFStrings) to client,
//                     and the name field also contains name of FITS file (if open),
//                     and the timestamp gives the time that the file was opened.
//
//  Name = "FILENAME", server sends UFStrings with the name of the last FITS file written,
//                     and the name field also contains status, such as errors if any.
//                     and the timestamp gives the time that the file was closed.
//
//  Name = "OC", server sends current ObsConfig,
//  Name = "PM", server sends current PixelMap (UFInts),
//  Name = "IOSTAT", server sends UFStrings with info on #obs, #clients, #replies, etc.
//  Name = "NBUFEDT", server sends UFStrings with info on size of EDT DMA ring buffers.
//  Name = "BN", server sends Buffer Names (UFStrings) to client,
//  Name = "buffer name", server sends FrameConfig and UFInts frame from named buffer.
//                       (see FrameProcessThread() for the vector of buffer names),

// A UFTimeStamp received from the client may request an action,
//  as indicated by the command in name field:
//  Name = "START", start the FrameGrabThread (any number after START will be the timeout secs).
//  Name = "ABORT", abort the observation and send SIGALRM to FrameGrabThread if needed.
//  Name = "MPWAIT", map pixels in FrameProcessThread instead of FrameGrabThread (auto reset).
//  Name = "FPWAIT", FrameProcessThread waits for FrameGrabThread to finish first (auto reset).
// The server performs action and replies with UFStrings/UFTimeStamp status message,
// of which the name field indicates either "ERROR", "NOOP", or the command name if success.

vector< UFProtocol* > handleRequest( string& clientNum, UFTimeStamp* request )
{
  vector< UFProtocol* > Reply ;
  string message;
  string FSthreadName = "FrameServerThread." + clientNum;
  string theRequest = request->name();
  clog << "\r client#"<<clientNum<<" request> " << theRequest << " : ";
  UFStrings::upperCase( theRequest );

  if( theRequest == "FC" )
    {
      UFFrameConfig* fc = copyFrameConfig();

      if( fc == 0 ) {
	message = "ERROR: " + FSthreadName + "> FrameConfig copy: Memory allocation failure!";
	clog << message <<endl;
	Reply.push_back( new (nothrow) UFTimeStamp(message) );
      }
      else Reply.push_back( static_cast< UFProtocol* >( fc ) ) ;
    }
  else if( theRequest == "OC" )
    {
      UFObsConfig* oc = copyObsConfig( message );
      clog << message <<endl;

      if( oc )
	Reply.push_back( static_cast< UFProtocol* >( oc ) ) ;
      else
	Reply.push_back( new (nothrow) UFTimeStamp(message) );
    }
  else if( theRequest.find("PM") != string::npos )
    {
      UFInts* pmc = copyPixelMap( message, theRequest );
      clog << message <<endl;

      if( pmc )
	Reply.push_back( static_cast< UFProtocol* >( pmc ) ) ;
      else
	Reply.push_back( new (nothrow) UFTimeStamp(message) );
    }
  else if( theRequest == "FH" )
    {
      Reply.push_back( getFITSheader() ) ;
    }
  else if( theRequest == "FILENAME" || theRequest == "LASTFILE" )
    {
      Reply.push_back( getLastFileName() ) ;
    }
  else if( theRequest == "BN" )
    {
      Reply.push_back( getBufferNames() ) ;
    }
  else if( theRequest == "CT" )
    {
      Reply.push_back( getCameraType() );
    }
  else if( theRequest == "VERSION" )
    {
      UFTimeStamp* SV = new (nothrow) UFTimeStamp( _serverVersion->name() );
      if( SV != 0 ) SV->stampTime( _serverVersion->timeStamp() );
      Reply.push_back( SV );
    }
  else if( theRequest == "HOST" )
    {
      Reply.push_back( new (nothrow) UFTimeStamp( UFRuntime::hostname() ) );
    }
  else if( theRequest == "IOSTAT" )
    {
      Reply.push_back( new (nothrow) UFStrings( UFRuntime::hostname(), reportIOstats() ) );
    }
  else if( theRequest == "NBUFEDT" )
    {
      message = "Nbuf_EDTpdv = " + UFRuntime::numToStr( FrameGrabNbuffers() );
      clog << message << endl;
      Reply.push_back( new (nothrow) UFStrings( UFRuntime::hostname(), &message ) );
    }
  else if( theRequest == "MAX_TIMEOUTS" )
    {
      message = "max # of EDT timeouts waiting for DMA until ABORT = "
	+ UFRuntime::numToStr( FrameGrabMaxTimeOuts() );
      clog << message << ". " << endl;
      Reply.push_back( new (nothrow) UFStrings( UFRuntime::hostname(), &message ) );
    }
  else if( theRequest == "MAXDMALAG" )
    {
      message = "max DMA Lag = " + UFRuntime::numToStr( FrameGrabMaxDMALag() );
      clog << message << ". " << endl;
      Reply.push_back( new (nothrow) UFStrings( UFRuntime::hostname(), &message ) );
    }
  else if( (theRequest).find("REGION") != string::npos )
    {
      Reply.push_back( getStatsRegion() ) ;
    }
  else if( theRequest == "UNLOCK" )
    {
      unLockMutexes();
      message = FSthreadName + "> All Mutexes are UnLocked.";
      clog << message << endl;
      Reply.push_back( new (nothrow) UFStrings( theRequest, &message ) );
    }
  else if( theRequest == "START" || theRequest == "RUN" )
    {
      Reply.push_back( FrameGrabStart( theRequest ) );
    }
  else if( theRequest == "ABORT" || theRequest == "STOP" )
    {
      UFStrings* reply_Abort = FrameGrabAbort( theRequest );

      if( reply_Abort->name() == "NOOP" )
	{
	  Reply.push_back( static_cast< UFProtocol* >( reply_Abort ) ) ;
	}
      else  //check if KILL is needed and combine the two replies into one:
	{
	  UFStrings* reply_Kill = FrameGrabKill( "KILL" );
	  UFStrings* replyAbortKill = new (nothrow) UFStrings( reply_Abort, reply_Kill );
	  delete reply_Abort;

	  if( replyAbortKill ) {
	    Reply.push_back( static_cast< UFProtocol* >( replyAbortKill ) ) ;
	    if( reply_Kill->name() == "ERROR" ) replyAbortKill->rename("ERROR");
	    delete reply_Kill;
	  } else {
	    clog<<"ERROR: "<<FSthreadName<<"> UFStrings* replyAbortKill mem. alloc. failed!"<<endl;
	    Reply.push_back( static_cast< UFProtocol* >( reply_Kill ) ) ;
	  }
	}
      clog << FSthreadName << ">  invoking  removeOldFrames()..." << endl ;
      removeOldFrames();
    }
  else if( theRequest == "MPWAIT" || theRequest == "MPP" )
    {
      message = "FrameAcqServer> Pixel Mapping is always performed in FrameProcessThread.";
      clog << message << endl;
      Reply.push_back( new (nothrow) UFStrings( "NOOP", &message ) );
    }
  else if( theRequest == "FPWAIT" || theRequest == "PPWAIT" )
    {
      Reply.push_back( delayProcessing() );
    }
  else if( (theRequest).find("START") != string::npos )
    {
      string cmd = theRequest;
      string seconds = cmd.substr( cmd.find("START") + 5 );
      int TimeOut = atoi( seconds.c_str() );
      Reply.push_back( FrameGrabStart( cmd, TimeOut ) );
    }
  else    //default is to request a frame from named buffer:
    {
      Reply = getFramefromBuffer( request->name() );

      if( Reply.size() == 0 )
	{
	  message = "ERROR: " + FSthreadName + "> Unknown Command: " + request->name();
	  clog << message << endl;
	  Reply.push_back( new (nothrow) UFTimeStamp( message ) );
	}
    }

  delete request ;
  return Reply ;
}
//------------------------------------------------------------------UFStrings----------

// Handle a UFStrings request:

// A UFStrings object received from the client can define the FITS header
//   or can open/close a FITS file for saving frames, depending on the name field:
//
//  Name = "HEADER", then the strings are assumed to define the full FITS header.
//  Name = "OPEN", then the first string is assumed to be the file name
//                 and the FITS file is opened and FITS header is written to it.
//  Name = "CLOSE", then the FITS file is closed (if all frames have been processed).
//
//  Name = "QUERY_FITS_OPEN" or "QUERY_FITS_CLOSE" are just like "OPEN" and "CLOSE" respectively,
//                         but first all device agents are queried to assemble the FITS header.
//
//  Name = "APPEND_HEADER", then the strings are to be appended to the basic (internal) FITS header.
//  Name = "APPEND_CLOSE", then the strings are to be appended to the basic (internal) FITS header
//                         and the FITS file is closed (if all frames have been processed).
//
// If the UFStrings object has either HEADER or CLOSE in the name field
//  and it contains at least the same number of strings as the original FITS header,
//  but not exceeding the standard multiple of 36 records (so data is not overwritten),
//  then it is assumed to be the final FITS header, and it is written to the file before closing
// After the server performs any of above actions it replies with UFStrings status message,
//  of which the name field indicates either "ERROR", "NOOP", or the action name if succeeded.

vector< UFProtocol* > handleRequest( string& clientNum, UFStrings* FileDirective )
{
  vector< UFProtocol* > Reply ;
  string message;
  string FSthreadName = "FrameServerThread." + clientNum;
  string directive = FileDirective->name();
  clog << "\n client#"<<clientNum<<" request> " << directive << " : ";
  UFStrings::upperCase( directive );

  if( ObsInProgress() )
    {
      delete FileDirective;
      message = FSthreadName + "> Observation in progress: can NOT change file status! ";
      clog << message << endl;
      Reply.push_back( new UFStrings( "ERROR", &message ) );
    }
  else if( directive == "HEADER" )
    {
      Reply.push_back( defineFITSheader( FileDirective ) );
    }
  else if( directive == "APPEND_HEADER" )
    {
      Reply.push_back( defineFITSheader( appendFITSheader( FileDirective ) ) );
    }
  else if( directive == "APPEND_CLOSE" )
    {
      UFStrings* rep_define = defineFITSheader( appendFITSheader( FileDirective ) );
      UFStrings* rep_close = closeFITSfile( new UFStrings( "CLOSE", "CLOSE" ) );
      Reply.push_back( new UFStrings( rep_define, rep_close ) );
    }
  else if( directive == "AUTO_CREATE" )
    {
      Reply.push_back( autoCreateFITSfile( FileDirective ) );
    }
  else if( directive == "OPEN" )
    {
      Reply.push_back( openFITSfile( FileDirective ) );
    }
  else if( directive == "OPEN_CLOSE" )
    {
      Reply.push_back( openFITSfile( FileDirective, true ) ); //true means to close FITS file when obs is done.
    }
  else if( directive == "QUERY_FITS_OPEN" )
    {
      UFStrings* rep_define = defineFITSheader( fetchFITSheader( FSthreadName ) );
      UFStrings* rep_open = openFITSfile( FileDirective );
      Reply.push_back( new UFStrings( rep_define, rep_open ) );
    }
  else if( directive == "CLOSE" )
    {
      Reply.push_back( closeFITSfile( FileDirective ) );
    }
  else if( directive == "QUERY_FITS_CLOSE" )
    {
      UFStrings* rep_define = defineFITSheader( fetchFITSheader( FSthreadName ) );
      UFStrings* rep_close = closeFITSfile( new UFStrings( "CLOSE", "CLOSE" ) );
      Reply.push_back( new UFStrings( rep_define, rep_close ) );
    }
  else {
    message = FSthreadName + "> invalid file directive: " + directive;
    clog << message << endl;
    delete FileDirective;
    Reply.push_back( new UFStrings( "ERROR", &message ) );
  }

  return Reply;
}
//---------------------------------------------------------------------------UFObsConfig------

vector< UFProtocol* > handleRequest( string& clientNum, UFObsConfig* ufoc )
{
  // Replace the working ObsConfig with new received ObsConfig
  // and reset all frame buffer counters for start of new observation.
  // Replies with UFStrings status message.

  vector< UFProtocol* > Reply ;
  string message="?", status="?";
  string FSthreadName = "FrameServerThread." + clientNum;
  clog << "\n client#"<<clientNum<<" request> UFObsConfig" <<endl;

  if( ObsInProgress() )
    {
      delete ufoc;
      message = FSthreadName + "> cannot change ObsConfig during observation.";
      status = "ERROR";
    }
  else if( !grabListEmpty() )
    {
      delete ufoc;
      message = FSthreadName + "> cannot change ObsConfig: Frame Processing NOT done.";
      status = "ERROR";
    }
  else if( nowSavingData() )
    {
      delete ufoc;
      message = FSthreadName + "> cannot change ObsConfig: FITS file is still open.";
      status = "ERROR";
    }
  else if( ufoc->elements() <= 0 )
    {
      delete ufoc;
      message = FSthreadName + "> cannot accept Zero Length ObsConfig: " + ufoc->name();
      status = "ERROR";
    }
  else
    {
      message = defineObsConfig( ufoc );
      status = "OK";
      clog << message << endl;

      ResetCounters();
      UFStrings* basicFITSheader = createFITSheader();

      if( basicFITSheader->name() != "ERROR" ) 
	{
	  message += " : created basic FITS header." ;
	  defineFITSheader( basicFITSheader ) ;
	}
    }

  Reply.push_back( new UFStrings( status, &message ) );
  clog << status << ": " << message << endl;
  return Reply ;
}
//---------------------------------------------------------------------------UFFrameConfig------

vector< UFProtocol* > handleRequest( string& clientNum, UFFrameConfig* uffc )
{
  vector< UFProtocol* > Reply ;
  string message="?", status="?";
  string FSthreadName = "FrameServerThread." + clientNum;
  clog << "\n client#"<<clientNum<<" request> UFFrameConfig" <<endl;

  if( ObsInProgress() )
    {
      message = FSthreadName + "> cannot change FrameConfig during observation.";
      status = "ERROR";
    }
  else if( !grabListEmpty() )
    {
      message = FSthreadName + "> cannot change FrameConfig: Frame Processing NOT done.";
      status = "ERROR";
    }
  else if( nowSavingData() )
    {
      message = FSthreadName + "> cannot change FrameConfig: FITS file is still open.";
      status = "ERROR";
    }
  else
    {
      message = defineFrameConfig( uffc );
      status = "OK";
      clog << message << endl;

      UFStrings* basicFITSheader = createFITSheader();

      if( basicFITSheader->name() != "ERROR" ) 
	{
	  message += " Created basic FITS header." ;
	  defineFITSheader( basicFITSheader ) ;
	}
    }

  delete uffc;
  Reply.push_back( new UFStrings( status, &message ) );
  clog << status << ": " << message << endl;
  return Reply ;
}
//------------------------------------------------------------------------------UFInts------

vector< UFProtocol* > handleRequest( string& clientNum, UFInts* receivedFrame )
{
  // Main purpose is to receive the Pixel Map array and related readout mapping arrays
  //   (if name contains "PIXELMAP"), which is the stored in FrameGrab class.
  // Secondary purpose is for simulation mode (if name starts with "sim" or "SIM"),
  //   to receive a sequence of test frames from client (no status reply).
  // Third purpose is to redefine size of EDT ring buffer (if name="NBUF_EDTPDV" ).
  // Fourth purpose is to redefine the region of frames for computation of statistics.
  // Fifth purpose is to set the time to pause for nods when in simulation mode.
  // Otherwise an error status message is returned.
  // Method replies with status message in UFStrings object for all cases except "SIM".

  string FSthreadName = "FrameServerThread." + clientNum;
  vector< UFProtocol* > Reply ;
  string message;

  if( (receivedFrame->name()).find("PIXELMAP") != string::npos )
    {
      clog << "\r client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";

      if( !ObsInProgress() && grabListEmpty() && !nowSavingData() )
	{
	  message = definePixelMap( receivedFrame );
	  if( message.find("ERROR") != string::npos )
	    Reply.push_back( new UFStrings( "ERROR", &message ) );
	  else {
	    Reply.push_back( new UFStrings( "OK", &message ) );
	    clog << "OK: ";
	  }
	}
      else {
	delete receivedFrame;
	message = FSthreadName + "> cannot change Pixel Map while obs in progress.";
	Reply.push_back( new UFStrings( "ERROR", &message ) );
	clog << "ERROR: ";
      }
    }
  else if( (receivedFrame->name()).find("sim") == 0 || (receivedFrame->name()).find("SIM") == 0 )
    {
      clog << "\r client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";
      FrameGrabSimulate( receivedFrame );
      //do not push anything into Reply so that empty Reply is returned and nothing is sent.
    }
  else if( receivedFrame->name() == "NBUF_EDTPDV" ) //to change value of Nbuf_EDTpdv in FrameGrabThread.
    {
      clog << "\n client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";

      if( ! ObsInProgress() )
	{
	  FrameGrabSetNbuffers( receivedFrame->valInt(0) );
	  message = "FrameAcqServer> accepted new value for # of frames in EDT DMA ring buffer.";
	  Reply.push_back( new UFStrings( "OK", &message ) );
	  clog << "OK: ";
	}
      else {
	message = FSthreadName + "> cannot change # of frames in EDT DMA ring buffer while obs in progress.";
	Reply.push_back( new UFStrings( "NOOP", &message ) );
	clog << "NOOP: ";
      }
      delete receivedFrame;
    }
  else if( receivedFrame->name() == "MAX_TIMEOUTS" ) //to change value of _maxTimeOuts in FrameGrabThread
    {
      clog << "\n client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";
      FrameGrabSetMaxTimeOuts( receivedFrame->valInt(0) );
      message = "FrameAcqServer> accepted new value for max # of EDT time outs until ABORT.";
      Reply.push_back( new UFStrings( "OK", &message ) );
      clog << "OK: ";
      delete receivedFrame;
    }
  else if( (receivedFrame->name()).find("REGION") != string::npos &&
	   receivedFrame->elements() == 4 )    //to change stats region.
    {
      clog << "\n client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";
      setStatsRegion( receivedFrame );
      message = "FrameAcqServer> accepted new region for computation of frame statistics.";
      Reply.push_back( new UFStrings( "OK", &message ) );
      clog << "OK: ";
    }
  else if( receivedFrame->name() == "NODPAUSE" )   //to change value of nodPause in FrameSimThread.
    {
      clog << "\n client#"<<clientNum<<" request> " << receivedFrame->name() << " : ";
      setNodPause( (float )receivedFrame->valInt(0) );
      message = "FrameAcqServer> accepted new value for nodPause.";
      Reply.push_back( new UFStrings( "OK", &message ) );
      delete receivedFrame;
      clog << "OK: ";
    }
  else
    {
      clog << "\nERROR: ";
      message = FSthreadName + "> unknown UFInts* client request> " + receivedFrame->name();
      Reply.push_back( new UFStrings( "ERROR", &message ) );
      delete receivedFrame;
    }

  if( message.length() > 0 ) clog << message << endl;
  return Reply;
}

#endif // __FrameServerThread_cc__ 
