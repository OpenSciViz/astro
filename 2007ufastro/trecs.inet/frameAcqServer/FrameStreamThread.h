#if !defined(__FrameStreamThread_h__)
#define __FrameStreamThread_h__ "$Name:  $ $Id: FrameStreamThread.h,v 1.3 2003/08/24 03:59:31 trecs beta $"

/*
 * The FrameStreamThread sends raw data frames to a client that requested "REPLICATE" or "DATASTREAM".
 *
 * Frank Varosi, July 9, 2003.
 */

extern void* FrameStreamThread( void* ) ;
extern bool createFrameStreamThread( int MaxFrmWaitSend ) ;
extern void FrameStreamStart() ;
extern void FrameStreamShutdown() ;
extern bool enableFrameStream( UFSocket* clientSocket ) ;
extern bool disableFrameStream( UFSocket* clientSocket ) ;
extern bool streamFrame( UFInts* sendMe ) ;
extern bool sendFrameToClient( UFInts* frame ) ;
extern bool sendListEmpty() ;
extern int totalFramesStreamed() ;
extern int sendListSize() ;
extern void clearSendList() ;
extern void sendShutdown() ;
extern void sendStopToClient() ;
extern void sendFinalFITSheader() ;

#endif /* __FrameStreamThread_h__ */
