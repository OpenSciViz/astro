#if !defined(__FrameNotifyThread_cc__)
#define __FrameNotifyThread_cc__ "$Name:  $ $Id: FrameNotifyThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameNotifyThread_cc__;

#include "FrameAcqServer.h"
#include "FrameNotifyThread.h"
#include "FrameProcessThread.h"

/*************************************************************************
 * The thread ID and blocking mutex for starting/pausing the FrameNotifyThread.
 */
pthread_t _frameNotifyThreadID = 0 ;
pthread_mutex_t _frameNotifyMutex = PTHREAD_MUTEX_INITIALIZER ;
pthread_cond_t _frameNotifyCond = PTHREAD_COND_INITIALIZER ;
static bool _keepRunning = true ;

// List to store pointers to FrameConfigs to be sent to clients requesting notification:

typedef list< UFFrameConfig* > frameConfigListType ;
frameConfigListType _frameConfigList ;
pthread_mutex_t _frameConfigListMutex = PTHREAD_MUTEX_INITIALIZER ;

unsigned int _maxListSize = 100; //max # of notifications allowed pending (gets rid of old ones).
int _totalNotifiesDropped = 0;   //counts total # of notifications not writable.
int _totalNotifiesError = 0;     //counts total # of notifications with write error.
int _totalNotifiesSent = 0;      //counts total # of notifications sent to clients.

/*
 * List used to store client sockets that should be notified after each new frame is processed:
 * See method: bool addtoNotifyList( UFSocket* )
 */
typedef set< UFSocket* > socketListType ;
socketListType _clientNotifyList ;
pthread_mutex_t _notifyListMutex = PTHREAD_MUTEX_INITIALIZER ;
unsigned int _maxNotifyClients = 99;
bool _notifyClients = false;

//*********************************************************************************************

bool addtoNotifyList( UFSocket* clientSocket )
{
  int canNotLock = ::pthread_mutex_trylock( &_notifyListMutex );
  if( canNotLock ) { 
    clog<<"FrameAcqServer::addtoNotifyList> cannot lock notify list."<<endl;
    return false;
  }

  if( _clientNotifyList.size() < _maxNotifyClients )
    {
      _clientNotifyList.insert( clientSocket );
      clog<<"FrameAcqServer> "<<_clientNotifyList.size()<<" clients in notify list."<<endl;
      _notifyClients = true;
      ::pthread_mutex_unlock( &_notifyListMutex );
      return true;
    }

  clog<<"FrameAcqServer> "<<_clientNotifyList.size()<<" clients in notify list."<<endl;
  clog<<"FrameAcqServer> allready reached maximum # clients in notification list." <<endl;
  ::pthread_mutex_unlock( &_notifyListMutex );
  return false;
}
//-----------------------------------------------------------------------------------------

void removeNotifyList( UFSocket* clientSocket )
{
  int canNotLock = ::pthread_mutex_trylock( &_notifyListMutex );
  if( canNotLock ) { 
    clog<<"FrameAcqServer::removeNotifyList> cannot lock notify list."<<endl;
    return;
  }

  _clientNotifyList.erase( clientSocket );
  clog<<"FrameAcqServer> "<<_clientNotifyList.size()<<" clients in notify list."<<endl;
  if( _clientNotifyList.size() <= 0 )
      _notifyClients = false;
  ::pthread_mutex_unlock( &_notifyListMutex );
}
//-----------------------------------------------------------------------------------------

bool inNotifyList( UFSocket* clientSocket )
{
  int canNotLock = ::pthread_mutex_trylock( &_notifyListMutex );

  bool isIn = (_clientNotifyList.count( clientSocket ) > 0);

  if( canNotLock )
    clog<<"FrameAcqServer::isInNotifyList> cannot lock notify list..."<<endl;
  else
    ::pthread_mutex_unlock( &_notifyListMutex );

  return isIn;
}
//-----------------------------------------------------------------------------------------
// notifyClientsNewFC() is invoked after each grabbed frame is processed by FrameProcessThread,
//  so therefore it should not use any mutex that could accidently hang it,
//  that is, it must be completely independent of method sendNotifications().

void notifyClientsNewFC( const vector< string >& updatedBufferList )
{
  if( !_notifyClients ) return;

  //if more than 6 names in buffer List, split up List into 2 notifications,
  // because they may not all fit into single name field of FrameConfig object.

  if( updatedBufferList.size() > 6 ) {
    vector< string > buffList;
    for( int i=0; i<6; i++ ) buffList.push_back( updatedBufferList[i] );
    notifyClientsNewFC( buffList );
    buffList.clear();
    for( int i=6; i < (int)updatedBufferList.size(); i++ ) buffList.push_back( updatedBufferList[i] );
    notifyClientsNewFC( buffList );
    return;
  }

  UFFrameConfig* newFC = copyFrameConfig();

  if( UFProtocol::validElemCnt( newFC ) ) {
    ::pthread_mutex_lock( &_frameConfigListMutex );
    newFC->setUpdatedBuffNames( updatedBufferList );
    _frameConfigList.push_front( newFC );
    if( _frameConfigList.size() > _maxListSize ) {
      UFFrameConfig* oldFC = _frameConfigList.back();
      _frameConfigList.pop_back();
      delete oldFC; //no reason to keep old notifications, make room for new ones.
    }
    ::pthread_mutex_unlock( &_frameConfigListMutex );
    ::pthread_mutex_unlock( &_frameNotifyMutex ); //this signals the thread to send to clients.
  } else {
    string status = "ERROR: notifyClientsNewFC> FAILED mem. alloc. for copy of FrameConfig.";
    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
    theFrameConfig->rename(status);
    theFrameConfig = accessFrameConfig( false );
    clog << status <<endl;
  }
}
//-----------------------------------------------------------------------------------------

void sendNotifications( UFFrameConfig* currFC )
{
  socketListType unWritableSockets;
  ::pthread_mutex_lock( &_notifyListMutex );

  if( _clientNotifyList.size() > 0 )
    {
      for( socketListType::iterator clientSocit = _clientNotifyList.begin();
	   clientSocit != _clientNotifyList.end(); clientSocit++ )
	{
	  UFSocket* clientSocket = *clientSocit;
	  if( clientSocket > 0 ) {
	    if( clientSocket->writable() > 0 ) {
	      if( clientSocket->send( *currFC ) >= currFC->length() )
		++_totalNotifiesSent;
	      else {
		++_totalNotifiesError;
		unWritableSockets.insert( clientSocket );
	      }
	    }
	    else {
	      ++_totalNotifiesDropped;
	      unWritableSockets.insert( clientSocket );
	    }
	  }
	}
    }

  if( unWritableSockets.size() > 0 )
    {
      for( socketListType::iterator clientSocit = unWritableSockets.begin();
	   clientSocit != unWritableSockets.end(); clientSocit++ )
	{
	  clog<<"\nFrameNotifyThread> closing un-writable client socket..."<<endl;
	  (*clientSocit)->close();
	  _clientNotifyList.erase( *clientSocit );
	  clog<<"FrameNotifyThread> "<<_clientNotifyList.size()<<" clients in notify list."<<endl;
	  if( _clientNotifyList.size() <= 0 )
	    _notifyClients = false;
	}
    }

  ::pthread_mutex_unlock( &_notifyListMutex );
}
//-----------------------------------------------------------------------------------------

bool notifyStreamEmpty()
{
  ::pthread_mutex_lock( &_frameConfigListMutex );
  bool empty_status = _frameConfigList.empty();
  ::pthread_mutex_unlock( &_frameConfigListMutex );

  return empty_status;
}
//----------------------------------------------------------------------------------------------

void* FrameNotifyThread( void* )
{
  // This method runs as a thread, popping frames from the sendFramesList,
  //  and sending them to the data replication stream client socket.
  clog << "FrameNotifyThread> starting..."<<endl;

  while( _keepRunning )
    {
      //mutex must be locked before checking if _frameConfigList is empty!
      ::pthread_mutex_lock( &_frameNotifyMutex );

      if( notifyStreamEmpty() )
	::pthread_mutex_lock( &_frameNotifyMutex );

      //Since lock was already obtained, this second try to lock mutex will block and wait.
      //First mutex is unlocked when new frame is copied to send (see sendFrame()).

      ::pthread_mutex_unlock( &_frameNotifyMutex ); //Unlock mutex to reset for next time.
      if( !_keepRunning ) break;

      ::pthread_mutex_lock( &_frameConfigListMutex );
      UFFrameConfig* notifyFC = _frameConfigList.back();
      _frameConfigList.pop_back();
      ::pthread_mutex_unlock( &_frameConfigListMutex );

      sendNotifications( notifyFC );
      delete notifyFC;
    }

  clog << "FrameNotifyThread> terminated."<<endl;
  return 0;
}
//-----------------------------------------------------------------------------------------

bool createFrameNotifyThread()
{
  if( ( _frameNotifyThreadID = UFPosixRuntime::newThread( FrameNotifyThread ) ) <= 0 )
    return false;
  else
    return true;
}
//----------------------------------------------------------

void clearNotifyStream()
{
  ::pthread_mutex_lock( &_frameConfigListMutex );
  if( !_frameConfigList.empty() ) {
    for( frameConfigListType::iterator ptr = _frameConfigList.begin();
	 ptr != _frameConfigList.end(); ++ptr )
      delete *ptr;
  }
  _frameConfigList.clear();
  ::pthread_mutex_unlock( &_frameConfigListMutex );
}
//---------------------------------------------------------------------------------

void FrameNotifyStart() { ::pthread_mutex_unlock( &_frameNotifyMutex ); }

void FrameNotifyShutdown()
{
  _keepRunning = false;
  FrameNotifyStart();
}
//-----------------------------------------------------------------------------------------

int totalNotifiesSent() { return _totalNotifiesSent; }
int totalNotifiesError() { return _totalNotifiesError; }
int totalNotifiesDropped() { return _totalNotifiesDropped; }

int totalNotifiesPending()
{
  ::pthread_mutex_lock( &_frameConfigListMutex );
  int NFCtoSend =_frameConfigList.size();
  ::pthread_mutex_unlock( &_frameConfigListMutex );
  return NFCtoSend;
}

#endif /* __FrameNotifyThread_cc__ */
