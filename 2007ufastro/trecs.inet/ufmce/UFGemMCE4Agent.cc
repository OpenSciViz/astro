#if !defined(__UFGemMCE4Agent_cc__)
#define __UFGemMCE4Agent_cc__ "$Name:  $ $Id: UFGemMCE4Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemMCE4Agent_cc__;

#include "UFGemMCE4Agent.h"
__UFGemMCE4Agent_H__(GemMCE4Agent_cc);

#include "UFMCE4Config.h"
__UFMCE4Config_H__(GemMCE4Agent_cc);

#include "UFEdtDMA.h"
__UFEdtDMA_H__(GemMCE4Agent_cc);

#include "UFObsConfig.h"
__UFObsConfig_H__(GemMCE4Agent_cc);

#include "sys/types.h"
#include "sys/wait.h"

//size of Raytheon det.arr. with CRC-774 MUX:
int UFGemMCE4Agent::_arrayColumns = 320;
int UFGemMCE4Agent::_arrayRows = 240;
int UFGemMCE4Agent::_arrayChannels = 16;

float UFGemMCE4Agent::_maxDTemp = 9.0;   //min-max range of Det.Temp in Kelvin for allowed power on.
float UFGemMCE4Agent::_minDTemp = 3.0;
int UFGemMCE4Agent::_queryDTperiod = 60; //seconds between periodic Det.Temp. & MCE queries.

// these statics might be of use to friend classes:
string UFGemMCE4Agent::_archvRootDir = "/data/trecs";
UFFITSClient* UFGemMCE4Agent::_agentFITSClient = 0;
UFClientSocket* UFGemMCE4Agent::_frmsoc = 0;
string UFGemMCE4Agent::_frmhost = "localhost";
int UFGemMCE4Agent::_frmport = 52000;
bool UFGemMCE4Agent::_frmAcqNow = false;
bool UFGemMCE4Agent::_fastmode = false; //true will delay frame processing/writing.
bool UFGemMCE4Agent::_sentLUTs = false; //indicates if pixel maps were sent to frm.acq.server.

// Note that "nod" of telescope is synonymous with "beam switch".
// _nodHandShake = true means wait for EPICS to indicate beam switch is complete,
//                                      or just count down NodSettleTime seconds.
bool UFGemMCE4Agent::_nodHandShake = false;
int UFGemMCE4Agent::_prevNodBeam = -1;  // nod beam = 0 or 1 during an obs.
int UFGemMCE4Agent::_nodCycle = 0;
int UFGemMCE4Agent::_prevFrmCnt = 0;
int UFGemMCE4Agent::_bgADUStart = 0; //0 indicates to set with first value from _FrmConf
int UFGemMCE4Agent::_rdADUStart = 0;
float UFGemMCE4Agent::_bgWellStart = 0.0;
float UFGemMCE4Agent::_bgWellMax = 0.0;
float UFGemMCE4Agent::_bgWellMin = 100.0;

// _BeamSwitching = true means invoke _ancilBeamSwitch() to check for completion,
//                       and when complete (or countdown done) tell MCE4 to RESUME coadding frames.
bool UFGemMCE4Agent::_BeamSwitching = false;
bool UFGemMCE4Agent::_BeamSwitchError = false;
bool UFGemMCE4Agent::_MCE4waitError = false;
float UFGemMCE4Agent::_maxBeamSwitchTime = 44.0; //seconds.

// these static objects keep the status/config of FrameAcqServer:
UFFrameConfig* UFGemMCE4Agent::_FrmConf = new UFFrameConfig( 320, 240, 32 );
UFObsConfig*   UFGemMCE4Agent::_ObsConf = new UFObsConfig(1,1,1,1,1);
UFObsConfig*   UFGemMCE4Agent::_abortedObsConf = 0;
string UFGemMCE4Agent::_FrmAcqVersion = "FrameAcqServer";
string UFGemMCE4Agent::_ObsCompStat = "unknown";

UFClientSocket* UFGemMCE4Agent::_ls340soc = 0;
string UFGemMCE4Agent::_ls340host = "localhost";
string UFGemMCE4Agent::_ls340handshake = "";
string UFGemMCE4Agent::_warmmux = "false";
int UFGemMCE4Agent::_ls340port = 52003;
int UFGemMCE4Agent::_nDTqueryFail = -1;  //-1 to force immeadiate query of Det.Temp. in ancillary().
int UFGemMCE4Agent::_maxDTqueryFail = 3;
//_devAgsHost is host on which other Device Agents are running (but default is the same host).
string UFGemMCE4Agent::_devAgsHost = "";

// (internal) simulation mode:
//map<string, double> UFGemMCE4Agent::_Sim;

//public:
// ctors:
UFGemMCE4Agent::UFGemMCE4Agent(int argc, char** argv,
			       char** envp) : UFGemDeviceAgent(argc, argv, envp) {
  //_Update = 2.0;
  _flush = 0.05; //  0.1; //0.35;
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":dc:configG";
  _heart = _epics + ":dc:heartbeat";
  _statRec = ""; // CAR should be set at start of each acquisition request packet
  _warmmux = "false";
  _dhsLabel= "Test";
  _comment= "";
  _dhsWrite = false;
  _archive = false;
  clog<<"UFGemMCE4Agent> archive Root Directory: "<<_archvRootDir<<endl;
  _queryLastTime = ::time(0);
}

UFGemMCE4Agent::UFGemMCE4Agent(const string& name, int argc,
			       char** argv, char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  //_Update = 2.0;
  _flush = 0.05; //  0.1; //0.35;
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":dc:configG";
  _heart = _epics + ":dc:heartbeat";
  _statRec = ""; // CAR should be set at start of each acquisition request packet
  _warmmux = "false";
  _dhsLabel= "Test";
  _comment= "";
  _dhsWrite = false;
  _archive = false;
  clog<<"UFGemMCE4Agent> archive Root Directory: "<<_archvRootDir<<endl;
  _queryLastTime = ::time(0);
}

// static funcs:
int UFGemMCE4Agent::main(int argc, char** argv, char** envp) {
  UFGemMCE4Agent ufs("UFGemMCE4Agent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemMCE4Agent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  if( clname.find("trecs:") != string::npos )
    _epics = "trecs";
  else if( clname.find("miri:") != string::npos )  
    _epics = "miri";
  else if( clname.find("flam:") != string::npos )  
    _epics = "flam";

  if( !( _epics == "trecs" || _epics == "miri" || _epics == "flam") )
    return clname;

  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string linecnt =  _epics + genSub + ".VALA";
  string positions =  _epics + genSub  + ".VALB";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemMCE4Agent::newClient> Epics client." <<clname<<endl;

  /* don't care right now
  int nr = getEpicsOnBoot(_epics, linecnt, _StatList);
  if( nr > 0 && _StatList.size() > 0 ) {
    linecnt = atof(_StatList[0].c_str());
  }

  nr = getEpicsOnBoot(_epics, positions, _StatList);
  */

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemMCE4Agent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may have been reset:
  _confGenSub = _epics + ":dc:configG";
  _heart = _epics + ":dc:heartbeat";

  _config = new UFMCE4Config(); // needs to be proper device subclass
  // defaults for trecs:
  _config->_tsport = 7008;
  _config->_tshost = "192.168.111.101";

  string arg;
  arg = findArg("-help"); 
  if( arg != "false" ) {
    clog<<"UFGemMCE4Agent::options> [-v] [-sim] [-[no]epics name] [-warm [kelvin]] [-conf db] [-heart db][-flush sec.] [-tsport annexport] [-tshost annexIP] [-frmport 52000] [-frmhost localhost] [-devagshost localhost]"<<endl;
    exit(0);
  } 

  arg = findArg("-warm");
  if( arg != "false" && arg != "true" )
    _warmmux = arg;
  else if( arg == "true" )
    _warmmux = "13";

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());
  
  arg = findArg("-frmport"); 
  if( arg != "false" && arg != "true" )
    _frmport = atoi(arg.c_str());

  arg = findArg("-frmhost");
  if( arg != "false" && arg != "true" )
    _frmhost = arg;
  
  arg = findArg("-devagshost");
  if( arg != "false" && arg != "true" )
    _devAgsHost = arg;
  else
    _devAgsHost = UFRuntime::hostname();

  _ls340host = _devAgsHost;
  
  arg = findArg("-sim"); // epics host/db name
  if( arg != "false" )
    _sim = true;

  arg = findArg("-epics"); // epics host/db name
  if( arg != "true" ) {
    _epics = arg; // could be false
    if( _epics != "false" ) {
      _confGenSub = _epics + ":dc:configG";
      _heart = _epics + ":dc:heartbeat";
    }
  }

  arg = findArg("-noepics"); // no channel access updates
  if( arg != "false" ) {
    _epics = "false";
  }

  arg = findArg("-heart");
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  //UFTermServ::_verbose = true;
  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
  }
  if( _vverbose )
    _verbose = true;
  UFMCE4Config::_verbose = _verbose;
  UFTermServ::_verbose = _verbose;

  arg = findArg("-scs");
  if( arg != "false" ) {
    UFMCE4Config::_SCSDelta = atof(arg.c_str());
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport > 7000 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52008;
  }
  if( _verbose ) {
    clog<<"UFGemMCE4Agent::options> set GemMCE4 port "<<port<<endl;
    clog<<"UFGemMCE4Agent::options> _heart: "<<_heart<<endl;
  }
  return port;
}

void UFGemMCE4Agent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemMCE4Agent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config->_devIO == 0 ) {
    clog<<"UFGemMCE4Agent::startup> no Device connection, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }

  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemMCE4Agent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;
}

UFTermServ* UFGemMCE4Agent::init(const string& tshost, int tsport) {
  if( _capipe == 0 && _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics); // this popens ufcaputarr
    if( _capipe )
      clog<<"UFGemMCE4Agent::init> Epics CA PUT started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemMCE4Agent::init> Ecpics CA PUT child proc. failed to start"<<endl;

    if( _capipeWr < 0 && _capipeRd < 0 ) {
      int stat = pipeToFromEpicsCAchild(_epics);
      if( stat < 0 )
        clog<<"UFGemMCE4Agent::init> Ecpics CA GET child proc. failed to start"<<endl;
      else
        clog<<"UFGemMCE4Agent::init> Epics CA GET started"<<endl;
    }
  }

  UFTermServ* ts= 0;
  if( ! _sim ) {
    // connect to the device:
    ts =_config->connect();
    if( _config->_devIO == 0 ) {
      clog<<"UFGemMCE4Agent> failed to connect to device port..."<<endl;
      return 0;
    }
    //ts->resetEOTS("\r"); // config object does this, and should do:
    ts->resetEOTR(">>"); // mce4 returns prompt
  }

  _powerUpDone = false; //this will cause code in ancillary method to cmdDAC power up.

  //arg=false means skip the actual query MCE for basic frame times,
  // use default timing params (TEST directive will query=true).
  if( !initMCEtiming( false ) )
    return 0;

  // Connectivity to MCE4 is checked again by ancillary when sending Det.Temp.
  // and method checkMCEstatus(), and if failed then error message is sent to epics,
  // so at this point just assume connectivity is ok and return.
  return ts;
}

//query MCE for basic frame times (at PBC=1).
// (this of course tests connectivity to MCE4).

bool UFGemMCE4Agent::initMCEtiming( bool queryMCE4 ) {
  if( queryMCE4 )
    clog<<"UFGemMCE4Agent::initMCEtiming> querying MCE for basic frame times..."<<endl;
  else
    clog<<"UFGemMCE4Agent::initMCEtiming> skipping MCE query, using default frame times."<<endl;

  if( _capipe != 0 && _epics != "false" ) {
    string message = "querying MCE timing params...";
    if( !queryMCE4 )
      message = "skipping querying MCE timing params...";
    sendEpics( _epics + ":sad:observationStatus", message );
    sendEpics( _epics + ":sad:dcState", message );
  }


  if( UFMCE4Config::queryMCEtiming( queryMCE4 ) ) {
    string message = "done querying MCE timing params.";
    if( !queryMCE4 )
      message = "using default MCE timing params.";
    clog<<"UFGemMCE4Agent::initMCEtiming> "<<message<<endl;
    if( _capipe != 0 && _epics != "false" ) {
      sendEpics( _epics + ":sad:observationStatus", message );
      sendEpics( _epics + ":sad:dcState", message );
    }
    return true;
  }
  else {
    clog<<"UFGemMCE4Agent::initMCEtiming> failed to query MCE for basic frame times!"<<endl;
    if( _capipe != 0 && _epics != "false" ) {
      string message = "ERR: failed to query MCE timing!";
      sendEpics( _epics + ":sad:observationStatus", message );
      sendEpics( _epics + ":sad:dcState", message );
    }
    return false;
  }
}

// F.Varosi code:
//  check current FrameConfig from FrameAcqServer for status of observation,
//  notify EPICS, and initiate Beam Switch when obsmode = "chop-nod":
//  If quickCheck = true then skip all actions and return after posting any error messages.

void UFGemMCE4Agent::_ancilFrmAcq( bool quickCheck ) {
    delete _FrmConf;
    _FrmConf = pollFrmAcq();

    if( UFProtocol::validElemCnt( _FrmConf ) ) {
      string facqErrMess, facqWarning; //string for error/warning message in _FrmConf from FrameAcqServer.

      if( (_FrmConf->name()).find("WARNING") != string::npos ) {
	facqWarning = _FrmConf->name();
	clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<facqWarning<<endl;
	facqWarning = facqWarning.substr( facqWarning.find(">")+1 );
      }

      if( (_FrmConf->name()).find("ERROR") != string::npos ) {
	facqErrMess = _FrmConf->name();
	clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<facqErrMess<<endl;
	facqErrMess = facqErrMess.substr( facqErrMess.find(">")+1 );
      }

      if( _epics != "false" ) {
	if( facqErrMess.length() > 0 )
	  sendEpics( _epics + ":sad:dcState", "ERR:" + facqErrMess );
	else if( facqWarning.length() > 0 )
	  sendEpics( _epics + ":sad:dcState", "WARN:" + facqWarning );
      }

      if( _dhsWrite ) //check for DHS client running or dead ?
	_checkDHSfrmCnt(_FrmConf);

      if( quickCheck ) return;

      if( _FrmConf->frameObsSeqNo() < _FrmConf->frameObsSeqTot() )  //acquisition of data still in progress.
	{
	  if( facqErrMess.length() > 1 ||  facqWarning.length() > 1) {
	    if( facqErrMess.length() > 1 )
	      _ObsCompStat = "ERR:" + facqErrMess;
	    else
	      _ObsCompStat = "WARN:" + facqWarning;
	    if( _epics != "false" ) {
	      sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
	      sendEpics( _epics + ":sad:dcState", _ObsCompStat );
	    }
	  }

	  if( !_BeamSwitching ) {
	    clog<<"UFGemMCE4Agent::_ancilFrmAcq> # grabbed="<<_FrmConf->frameGrabCnt()
		<<", processed="<<_FrmConf->frameObsSeqNo()
		<<", written="<<_FrmConf->frameWriteCnt()
		<<", sent="<<_FrmConf->frameSendCnt()
		<<", out of "<<_FrmConf->frameObsSeqTot() << " tot. \r";

	    if( _statRec != "" )  //send frame grab count only to epics acqControl CAR:
	      sendEpics(_statRec, UFRuntime::numToStr(_FrmConf->frameGrabCnt()) );

	    if( _bgADUStart <= 0 && _FrmConf->frameObsSeqNo() > 0 ) {
	      _bgADUStart = _FrmConf->bgADUs();
	      _rdADUStart = _FrmConf->rdADUs();
	      _bgWellStart = _FrmConf->bgWellpc();
	      _bgWellMax = _bgWellStart;
	      _bgWellMin = _bgWellStart;
	      if( _epics != "false" ) {
		sendEpics(_epics+":sad:bgAdcStart", UFRuntime::numToStr(_FrmConf->bgADUs()) );
		sendEpics(_epics+":sad:bgWellStart", UFRuntime::numToStr(_FrmConf->bgWellpc()) );
	      }
	    }

	    if( _FrmConf->frameObsSeqNo() > _prevFrmCnt ) { //if new frame processed send stats:
	      _prevFrmCnt = _FrmConf->frameObsSeqNo();
	      if( _epics != "false" ) {
		sendEpics(_epics+":sad:sigmaPerFrameRead", UFRuntime::numToStr(_FrmConf->sigmaFrmNoise()) );
		sendEpics(_epics+":sad:bgAdcCurrent", UFRuntime::numToStr(_FrmConf->bgADUs()) );
		sendEpics(_epics+":sad:bgWellCurrent", UFRuntime::numToStr(_FrmConf->bgWellpc()) );
	      }
	      if( _FrmConf->bgWellpc() > _bgWellMax ) _bgWellMax = _FrmConf->bgWellpc();
	      if( _FrmConf->bgWellpc() < _bgWellMin ) _bgWellMin = _FrmConf->bgWellpc();
	    }
	  }

	  // Determine if it is time to signal beam-switch.
	  // But do not beam-switch if readout mode is "RAW" since that uses pretend nodding.
	  string readOutMode = _ObsConf->readoutMode();
	  UFStrings::upperCase( readOutMode );

	  if( _ObsConf->nodBeams() > 1 && readOutMode.find("RAW") == string::npos )
	    {
	      int NframesPerNod = _ObsConf->saveSets() * _ObsConf->chopBeams();
	      int Nfgrab = _FrmConf->frameGrabCnt();
	      bool signalBeamSwitch = ((Nfgrab % NframesPerNod) == 0)
		                    && (Nfgrab > 0) && (Nfgrab < _FrmConf->frameObsSeqTot());

	      if( !_BeamSwitching && signalBeamSwitch && _FrmConf->NodBeam() != _prevNodBeam )
		{
		  _prevNodBeam = _FrmConf->NodBeam();
		  if( _FrmConf->NodBeam() > 0 )
		    clog<<"\nUFGemMCE4Agent::_ancilFrmAcq> finished Nod Cycle "<<_FrmConf->NodSet()<<endl;
		  else
		    clog<<"\nUFGemMCE4Agent::_ancilFrmAcq> middle of Nod Cycle "<<_FrmConf->NodSet()<<endl;
		  _nodStartTime = ::time(0);
		  _BeamSwitching = true;
		  if( _nodHandShake && _epics != "false" )
		    sendEpics(_epics + ":dc:TCSISnodComplG.J", "0" );
		  clog<<"UFGemMCE4Agent::_ancilFrmAcq> requesting telescope Beam Switch..."<<endl;
		  setEpicsAcqFlags( true, false );
		  string BS = "A->B";
		  if( _FrmConf->NodBeam() > 0 ) BS = "B->A";
		  string nodCycle = UFRuntime::numToStr(_FrmConf->NodSet());
		  _ObsCompStat = "Beam Switch " + BS + " " + nodCycle;
		  _nodCycle = _FrmConf->NodSet();
		  if( _epics != "false" ) {
		    sendEpics( _epics + ":sad:currentBeam", BS );
		    sendEpics( _epics + ":sad:currentNodCycle", nodCycle );
		    sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
		    sendEpics( _epics + ":sad:dcState", _ObsCompStat );
		  }
		}
	      else if( _BeamSwitching && _FrmConf->NodBeam() != _prevNodBeam )
		{
		  clog<<"UFGemMCE4Agent::_ancilFrmAcq> # grabbed="<<_FrmConf->frameGrabCnt()
		      <<", processed="<<_FrmConf->frameObsSeqNo()
		      <<", written="<<_FrmConf->frameWriteCnt()
		      <<", sent="<<_FrmConf->frameSendCnt()
		      <<", out of "<<_FrmConf->frameObsSeqTot() << " tot. "<<endl;
		  if( _nodHandShake ) {
		    _ObsCompStat = "ERR: MCE4 failed wait Nod completion!";
		    clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<_ObsCompStat<<endl;
		    _BeamSwitching = false;
		    notifyFrmAcq("STOP");
		    if( _config->_devIO ) {
		      string reply;
		      int stat = UFMCE4Config::acquisition("ABORT", reply, _flush);
		      clog<<"UFGemMCE4Agent::_ancilFrmAcq> MCE-4 abort status: "<<stat
			  <<", reply: "<<reply<<endl;
		    }
		  }
		  else {
		    _ObsCompStat = "ERR: MCE4 failed pause for Nod ?";
		    clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<_ObsCompStat<<endl;
		  }
		  _MCE4waitError = true;
		  if( _epics != "false" ) {
		    sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
		    sendEpics( _epics + ":sad:dcState", _ObsCompStat );
		  }
		}
	    }
	}
      else // frame acquistion has stopped so check completion status:
	{
	  if( _FrmConf->frameObsSeqTot() > 0 ) {
	    if( _archive && _FrmConf->frameWriteCnt() < _FrmConf->frameObsSeqNo() )
	      {
		//check file write error (even though all frames were grabbed):
		if( facqErrMess.find("write") != string::npos ||
		    facqErrMess.find("Write") != string::npos ||
		    facqErrMess.find("WRITE") != string::npos ) {
		  _ObsCompStat = "ERR:" + facqErrMess;
		  _FrmConf->setFrameObsSeqTot( -(_FrmConf->frameObsSeqTot()) ); //mark error status.
		}
		else _ObsCompStat = "COMPLETE";
	      }
	    else _ObsCompStat = "COMPLETE";
	  }
	  else {                          //non-positive frameObsSeqTot means ERROR or ABORT occurred:
	    if( facqErrMess.length() > 1 )
	      _ObsCompStat = "ERR:" + facqErrMess;  //frame acq server failure.
	    else if( _BeamSwitchError )
	      _ObsCompStat = "Beam-Switch Time-Out error";  //special case of an ABORT.
	    else if( _MCE4waitError )
	      _ObsCompStat = "MCE4 failed wait Nod completion";  //special case of an ABORT.
	    else
	      _ObsCompStat = "STOPPED @ " + UFRuntime::numToStr(_FrmConf->frameGrabCnt()) + " frames";
	  }

	  _frmAcqNow = false;
	  _BeamSwitching = false;

	  if( _epics != "false" ) {
	    if( facqErrMess.length() > 0 )
	      sendEpics( _epics + ":sad:dcState", "ERR:" + facqErrMess );
	    else if( facqWarning.length() > 0 )
	      sendEpics( _epics + ":sad:dcState", "WARN:" + facqWarning );
	    else
	      sendEpics( _epics + ":sad:dcState", _ObsCompStat );
	    sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
	    sendEpics( _epics + ":sad:bgAdcEnd", UFRuntime::numToStr(_FrmConf->bgADUs()) );
	    sendEpics( _epics + ":sad:bgWellEnd", UFRuntime::numToStr(_FrmConf->bgWellpc()) );
	  }

	  //if # processed < # grabbed then wait 1 to 3 secs for frm.acq.server processing to finish:
	  UFFrameConfig* finFrmConf = 0;

	  if( _FrmConf->frameObsSeqNo() < _FrmConf->frameGrabCnt() ) {
	    float stime = 1.0 + 0.1 * (_FrmConf->frameGrabCnt() - _FrmConf->frameObsSeqNo());
	    if( stime > 3.0 ) stime = 3.0;
	    clog<<"UFGemMCE4Agent::_ancilFrmAcq> waiting "<<stime<<" secs for frm. processing to finish"<<endl;
	    UFPosixRuntime::sleep(stime);
	    finFrmConf = pollFrmAcq();                    //check frm.acq.server config counters again
	    if( UFProtocol::validElemCnt( finFrmConf ) ) { //make sure it is valid UFProtocol object
	      delete _FrmConf;
	      _FrmConf = finFrmConf;
	    }
	  }

	  clog<<"UFGemMCE4Agent::_ancilFrmAcq> # frames grabbed="<<_FrmConf->frameGrabCnt()
	      <<", # processed="<<_FrmConf->frameObsSeqNo()
	      <<", # written="<<_FrmConf->frameWriteCnt()
	      <<", # sent="<<_FrmConf->frameSendCnt()
	      <<", out of "<<_FrmConf->frameObsSeqTot() << " total. " <<endl;
	  clog<<"UFGemMCE4Agent::_ancilFrmAcq> final acq status: "<<_FrmConf->name()<<endl;
	  clog<<"UFGemMCE4Agent::_ancilFrmAcq> Observation Status = "<<_ObsCompStat<<endl;

	  if( _FrmConf->bgWellpc() > _bgWellMax ) _bgWellMax = _FrmConf->bgWellpc();
	  if( _FrmConf->bgWellpc() < _bgWellMin ) _bgWellMin = _FrmConf->bgWellpc();

	  //if obs was aborted or incomplete, adjust the # of SaveSets/NodSets for FITS header:
	  if( _FrmConf->frameObsSeqTot() < 0 ) {
	    _abortedObsConf = new (nothrow) UFObsConfig( _ObsConf->name(),
							 _ObsConf->valData(),
							 _ObsConf->numVals() );
	    if( _abortedObsConf ) {	
	      clog<<"UFGemMCE4Agent::_ancilFrmAcq> adjusting temp.Obs.Conf. for final FITS header..."<<endl;
	      int saveCnt;
	      if( _archive )
		saveCnt = _FrmConf->frameWriteCnt()/_ObsConf->chopBeams();
	      else
		saveCnt = _FrmConf->frameObsSeqNo()/_ObsConf->chopBeams();
	      clog<<"UFGemMCE4Agent::_ancilFrmAcq> actual SaveSet count = "<<saveCnt<<endl;
	      if( _ObsConf->nodBeams() == 1 )
		_abortedObsConf->setSaveSets( saveCnt );
	      else {
		int nodbeams = saveCnt/_ObsConf->saveSets();
		int nodsets = nodbeams/_ObsConf->nodBeams();
		if( nodsets > 0 )
		  _abortedObsConf->setNodSets( nodsets );
		else {
		  _abortedObsConf->setNodSets( 1 );
		  _abortedObsConf->setNodBeams( 1 );
		  if( nodbeams <= 0 )
		    _abortedObsConf->setSaveSets( saveCnt );
		}
	      }
	      clog<<"UFGemMCE4Agent::_ancilFrmAcq> created final FITS header with adjusted Obs.Conf.:"
		  <<_abortedObsConf->name()<<endl;
	    }
	  }

	  //create basic header (uses _ObsConf unless _abortedObsConf exists).
	  UFFITSheader* DCagentFITSheader = basicFITSheader();
	  UFMCE4Config::makeFITSheader( DCagentFITSheader );   //add MCE4 params.
	  UFStrings* agentsFITShdrs = fetchFITSheaders();      //get params from other agents.
	  UFStrings* finFITSheader;                            //concatenate to form final FITS header:

	  if( (agentsFITShdrs->name()).find("ERROR") == string::npos )
	    finFITSheader = new UFStrings( DCagentFITSheader->Strings("CLOSE"), agentsFITShdrs );
	  else {
	    DCagentFITSheader->end();
	    finFITSheader = DCagentFITSheader->Strings("CLOSE");
	    if( _epics != "false" )
	      sendEpics( _epics + ":sad:dcState", "ERR: Failed get FITS info from agents!");
	  }

	  clog<<"UFGemMCE4Agent::_ancilFrmAcq> sending final FITS header and closing file..."<<endl;
	  finFITSheader->rename("CLOSE"); //make sure to CLOSE
	  _frmsoc->send( finFITSheader );
	  UFStrings* frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	  checkFrmAcqReply( frmreply, finFITSheader->name() );

	  // Send frame count and status to EPICS on ERROR or COMPLETE.
	  // Epics interprets anything other than COMPLETE as an error.
	  // For case of ABORT directive, the frame count was already sent to EPICS,
	  //  and only send a status if the ABORT is due to beam switch or MCE error
	  //  because EPICS gensub code interprets anything other than COMPLETE as and ERR.

	  if( _statRec != "" ) {
	    string status;
	    if( _ObsCompStat.find("ERROR") != string::npos )
	      {
		sendEpics(_statRec, UFRuntime::numToStr(_FrmConf->frameGrabCnt()) );
		status = _ObsCompStat.substr(_ObsCompStat.find(":")+1);
	      }
	    else if( _ObsCompStat == "COMPLETE" )
	      {
		sendEpics(_statRec, UFRuntime::numToStr(_FrmConf->frameGrabCnt()) );
		status = "COMPLETE";
	      }
	    else if( _BeamSwitchError || _MCE4waitError ) status = _ObsCompStat; //aborted by error.

	    if( status.length() > 1 ) {
	      UFPosixRuntime::sleep(0.5);   //wait for previous sendEpics to get processed.
	      sendEpics( _statRec, status );
	    }
	  }

	  setEpicsAcqFlags( false, false );  //indicate end of obs. to epics.
	  _statRec = "";     //do not allow any more status puts to acqControl CAR

	  if( _dhsWrite ) //final check for DHS client running or dead ?
	    _checkDHSfrmCnt(_FrmConf);
	}
    }
    else {
      string message = "ERR: Failed polling Frm.Acq.Server!";
      clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<message<<endl;
      if( _epics != "false" )
	sendEpics( _epics + ":sad:dcState", message );
    }
}

//helper function used by _ancilFrmAqc():

void UFGemMCE4Agent::_checkDHSfrmCnt( UFFrameConfig* frmCfg ) {
  if( UFProtocol::validElemCnt( frmCfg ) ) {
    string message;
    if( frmCfg->frameSendCnt() == 0 && frmCfg->frameObsSeqNo() > 1 ) {
      message = "ERR: DHS client not receiving frames ?";
    }
    if( frmCfg->frameSendCnt() < (frmCfg->frameObsSeqNo() - 50) ) {
      message = "ERR: DHS server not keeping up ?";
    }
    else if( frmCfg->frameSendCnt() < (frmCfg->frameObsSeqNo() - 100) ) {
      message = "ERR: DHS server not receiving frames ?";
    }
    if( message.length() > 0 ) {
      clog<<"UFGemMCE4Agent::_ancilFrmAcq> "<<message<<endl;
      string status =  frmCfg->name();
      clog<<"UFGemMCE4Agent::_ancilFrmAcq> status: "<<status<<endl;
      // if DHS client has re-connected then do not bother with ERR/WARN message.
      if( status.find("re-connected") == string::npos ) {
	if( _epics != "false" ) sendEpics( _epics + ":sad:dcState", message );
      }
    }
  }
  else clog<<"UFGemMCE4Agent::_checkDHSfrmCnt> invalid # elem count for frmCfg object!"<<endl;
}

// F.Varosi code:
// helper func. waits for Beam Switch completion from EPICS,
//  or just counts nodSettleTime secs if _nodHandShake = false,
//  and then tells MCE4 to resume coadding frames:

void UFGemMCE4Agent::_ancilBeamSwitch() {
  float bsTime = (float)( ::time(0) - _nodStartTime );
  if( bsTime < 2.1 ) return;
  string newNodBeam = "B";
  int newNodCycle = _nodCycle;

  if( _prevNodBeam > 0 ) {  //a nod cycle is beam A and B, then back to A.
    newNodBeam = "A";
    ++newNodCycle;
  }

  if( _nodHandShake ) {
      clog<<"UFGemMCE4Agent::_ancilBeamSwitch> checking for nod completion after "<<bsTime<<" sec. \r";

      if( getEpics(_epics, ":dc:TCSISnodComplG.VALA") == "1" ) {
	_BeamSwitching = false;
	clog<<"\nUFGemMCE4Agent::_ancilBeamSwitch> switched to Beam "<<newNodBeam
	    <<", in Nod Cycle "<<newNodCycle<<" out of "<<_ObsConf->nodSets()<<endl;
      }

      if( bsTime > _maxBeamSwitchTime && _BeamSwitching ) {
	clog<<"\nUFGemMCE4Agent::_ancilBeamSwitch> ERROR: wait for Beam Switch timed out!"<<endl;
	_BeamSwitching = false;
	_BeamSwitchError = true;
	notifyFrmAcq("STOP");
	if( _config->_devIO ) {
	  string reply;
	  int stat = UFMCE4Config::acquisition("ABORT", reply, _flush);
	  clog<<"UFGemMCE4Agent::_ancilBeamSwitch> MCE-4 abort status: "<<stat<<", reply: "<<reply<<endl;
	}
      }
    }
  else {
    clog<<"UFGemMCE4Agent::_ancilBeamSwitch> Beam Switch elapsed time = "<<bsTime<<" sec. \r";
    double nodSettleTime = UFMCE4Config::getNodSettleTime();
    if( nodSettleTime < 1.0 ) nodSettleTime = _maxBeamSwitchTime;

    if( bsTime > (nodSettleTime - 0.5) ) { //use 0.5 sec close enough check because MCE may start 
      _BeamSwitching = false;              // sending frames by the time dc agent polls acq.server.
      clog<<"\nUFGemMCE4Agent::_ancilBeamSwitch> assuming switched to Beam "<<newNodBeam
	  <<", in Nod Cycle "<<newNodCycle<<" out of "<<_ObsConf->nodSets()<<endl;
    }
  }

  if( !_BeamSwitching && !_BeamSwitchError ) {
    if( _nodHandShake ) {
      clog<<"UFGemMCE4Agent::_ancilBeamSwitch> sending RESUME directive to MCE4...";
      string reply;
      if( _config->_devIO )
	_config->_devIO->submit("RESUME", reply, _flush); // expect single line reply
      else
	reply = "simulation mode";
      clog<<"reply = "<<reply<<endl;
    }
    setEpicsAcqFlags( true, true );  //indicate obs. in progress to epics.
    string nodCycle = UFRuntime::numToStr( newNodCycle );
    _ObsCompStat = "Observing " + nodCycle + " " + newNodBeam;
    if( _epics != "false" ) {
      sendEpics( _epics + ":sad:currentBeam", newNodBeam );
      sendEpics( _epics + ":sad:currentNodCycle", nodCycle );
      sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
      sendEpics( _epics + ":sad:dcState", _ObsCompStat );
    }
  }
}

// Open a socket and query the Lakeshore 340 for detector array temperature,
// return value > 0 indicates success, otherwise query failed.
// If no reply is recvd the socket is closed (will reconnect next call).
// String detKelvin is changed only if reply is recvd,
//  but if reply is not a valid temp it is set to NULL and 0 is returned.

int UFGemMCE4Agent::_queryLS340DT(string& detKelvin) {
  if( _ls340soc == 0 ) {
    clog<<"\n";
    _ls340soc = new UFClientSocket();
    int trycnt= 3, socfd= 0;
    do {
      clog<<"UFGemMCE4Agent::_queryLS340DT> trying connect to LS340 agent, via port: "
	  <<_ls340port<<", on host: "<<_ls340host<<endl;
      socfd = _ls340soc->connect(_ls340host, _ls340port, false); //do not block on connect.
      if( trycnt > 0  && socfd <= 0 ) UFPosixRuntime::sleep(0.3);
    } while( socfd <= 0 && --trycnt > 0 ); 

    //must always first perform a handshake with agent by sending client name:
    if( _ls340soc->validConnection() ) {
      UFTimeStamp handshake("UFGemMCE4Agent");
      if( handshake.sendTo(*_ls340soc) > 0 ) {
	UFTimeStamp* LS340reply = dynamic_cast<UFTimeStamp*>(UFProtocol::createFrom(*_ls340soc));
	if( LS340reply ) {
	  if( LS340reply->length() >= handshake.length() ) {
	    _ls340handshake = LS340reply->name();
	    clog<<"UFGemMCE4Agent::_queryLS340DT> recvd handshake from LS340 agent: "<<endl;
	    clog<<_ls340handshake<<endl;
	  } else
	    clog<<"UFGemMCE4Agent::_queryLS340DT> failed to recv handshake from LS340 agent!"<<endl;
          delete LS340reply;
	}
	else clog<<"UFGemMCE4Agent::_queryLS340DT> null reply from LS340 agent!"<<endl;
      }
      else clog<<"UFGemMCE4Agent::_queryLS340DT> failed sending handshake to LS340 agent!"<<endl;
    }
    else clog<<"UFGemMCE4Agent::_queryLS340DT> failed to connect to LS340 agent!"<<endl;
  }
  //assume socket is dysfunctional until query and reply with LS340 agent succeeds:
  bool socok= false;
  int nrecvd= 0;
  if( _ls340handshake.length() > 1 ) {
    vector< string > LS340command;
    LS340command.push_back("raw");
    LS340command.push_back("krdg?a");
    UFStrings LS340query(_ls340handshake, LS340command);
    if( LS340query.sendTo(*_ls340soc) > 0 ) {
      UFStrings* LS340reply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_ls340soc));
      if( LS340reply ) {
	const string* kval = LS340reply->stringAt(0);
	if( kval != 0 ) { //detector temperature query succeeded:
          nrecvd = LS340reply->elements();
	  socok = true;
	  detKelvin = *kval;
	  if( _verbose )
	    clog<<"UFGemMCE4Agent::_queryLS340DT> Detector temperature reply: "<<detKelvin<<endl;
	  size_t dotpos = detKelvin.find(".");
	  size_t pluspos = detKelvin.find("+");
	  if( pluspos >= 0 && pluspos != string::npos && dotpos != string::npos ) {
	    detKelvin = detKelvin.substr(++pluspos, --dotpos);
	    if( _verbose )
	      clog<<"UFGemMCE4Agent::_queryLS340DT> Parsed temperature reply: "<<detKelvin<<endl;
	  }
	  else {
	    clog<<"UFGemMCE4Agent::_queryLS340DT> Detector temperature is invalid: "<<detKelvin<<endl;
	    nrecvd = 0;
	    detKelvin = "";
	  }
	}
	else {
         clog<<"UFGemMCE4Agent::_queryLS340DT> reply did not contain temperature string!"<<endl;
	}
        delete LS340reply;
      }
    }
  }
  if( !socok ) {
    clog<<"UFGemMCE4Agent::_queryLS340DT> failed to recv Det.Temp. from LS340 agent, closing socket."<<endl;
    _ls340soc->close();
    delete _ls340soc;
    _ls340soc = 0;
    _ls340handshake.erase();
  }
  return nrecvd;
}

// Send heartbeat to EPICS,
// monitor status of MCE4, the FrameAcqServer,
// the detector array temperature by querying the Lakeshore340,
// and check for the telescope beam switch (nodding) completion:

void* UFGemMCE4Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  //beam-switch and frame status are top priority.

  if( _BeamSwitching ) // _ancilFrmAcq() has requested beam switch so monitor for completion:
    _ancilBeamSwitch();

  if( _frmAcqNow ) // frameAcqServer (ufacqframed) is grabbing frames so check current FrameConfig:
    _ancilFrmAcq();

  if( _capipe == 0 && _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemMCE4Agent::ancillary> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemMCE4Agent::ancillary> Ecpics CA child proc. failed to start"<<endl;
  }

  // update the heartbeat:
  time_t clck = ::time(0);
  if( _capipe && _heart != "" )
    updateEpics(_heart, UFRuntime::numToStr(clck));

  //check the temperature of detector from LakeShore 340 and MCE status once a minute
  // (or always on startup when init _nDTqueryFail = -1):

  if( ((int)(clck - _queryLastTime)) >= _queryDTperiod || _nDTqueryFail < 0) {
    _queryLastTime = clck;
    string detKelvin;

    if( _queryLS340DT( detKelvin ) > 0 ) {
      if( detKelvin.length() > 0 )
	_nDTqueryFail = 0;
      else
	++_nDTqueryFail;
    }
    else ++_nDTqueryFail;

    if( _nDTqueryFail >= _maxDTqueryFail ) {
      detKelvin = "299";
      clog<<"UFGemMCE4Agent::ancillary> Detector temperature is unknown, assuming worst case: "
	  <<detKelvin<<"K"<<endl;
      if( _epics != "false" ) {
	string message = "WARN: Det. Temperature is unknown!";
	sendEpics( _epics + ":sad:observationStatus", message );
	sendEpics( _epics + ":sad:dcState", message );
      }
    }

    if( _frmAcqNow ) clog<<"\n";
    clog<<"UFGemMCE4Agent> time="<<currentTime()<<" \n";
    int mceStat = 0; //this should become > 0 if det.temp got sent to MCE.

    if( _config->_devIO && detKelvin.length() > 0 )
      {
	string mceKelvin = "ARRAY_TEMP " + detKelvin;
	clog<<"UFGemMCE4Agent::ancillary> Sending MCE4 the Det. Temp.: "<<mceKelvin<<" (K)"<<endl;
	string mceReply;
	mceStat = _config->_devIO->submit(mceKelvin, mceReply, _flush);

	if( mceStat <= 0 || mceReply.length() == 0 ) {
	  string message = "ERR: failed to send Det.Temp. to MCE !";
	  clog<<"UFGemMCE4Agent::ancillary> "<<message<<endl;
	  if( _epics != "false" ) {
	    sendEpics( _epics + ":sad:observationStatus", message );
	    sendEpics( _epics + ":sad:dcState", "failed MCE-I/O: check Terminal Server ?" );
	  }
	}
      }
    else {
      clog<<"UFGemMCE4Agent::ancillary> Detector temperature is: "<<detKelvin<<"K ";
      if( _sim ) {
	clog <<"(sim mode)";
	mceStat = 1;
      }
      clog<<endl;
    }

    // get MCE status and parse it for info about state of bias voltages:
    checkMCEstatus();
    float detTemp = ::atof( detKelvin.c_str() );

    if( (detTemp < _maxDTemp) && (detTemp > _minDTemp) && (mceStat>0) && !_powerUpDone ) {
      clog<<"UFGemMCE4Agent::ancillary> commanding MCE to power up detector..."<<endl;
      vector<string> biasCmds;
      biasCmds.push_back("BiasPower");
      biasCmds.push_back("ARRAY_POWER_UP_BIAS"); //power on bias voltages to detector.
      biasCmds.push_back("BiasVgate");
      biasCmds.push_back("ARRAY_VGATE 1"); //turn Vgate on.
      UFStrings BiasCmds( "BiasCmds", biasCmds );
      UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
      bool readflag, initflag;
      int statDAC = UFMCE4Config::cmdDAC( BiasCmdinfo, readflag, initflag );
      clog<<"UFGemMCE4Agent::ancillary> cmd DAC status = "<<statDAC<<endl;
      checkMCEstatus();
      if( (_biasPower == "1" && _Vgate == "1") || _sim )
	_powerUpDone = true;
    }

    //send status of detector power to epics sad db:
    if( _epics != "false" ) {
      string message;
      if( _biasPower == "1" && _Vgate == "1" )
	message = "Detector is powered ON";
      else if( _sim && _powerUpDone )
	message = "Detector is powered ON (sim mode)";
      else
	message = "WARN: Detector is NOT powered on.";
      sendEpics( _epics + ":sad:dcState", message );
    }

    //If not acquired frames then just perform a quick check of last frm.acq.status and log it.
    if( !_frmAcqNow )
      _ancilFrmAcq( true );
  }

  return p;
}

// check status of MCE, in particular the bias board voltage states,
// setting the protected attribs: string _biasPower, _Vgate, _wellDepth, _biasLevel
//  and then send status to Epics if connected to a db:

void UFGemMCE4Agent::checkMCEstatus()
{
  _biasPower="?", _Vgate="?", _wellDepth="?", _biasLevel="?";

  if( _config->_devIO ) {
    string mceReply;
    if( _config->_devIO->submit("status", mceReply, _flush) > 0 ) {
      string Line;
      if( mceReply.find("Bias Board") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Bias Board") );
	_biasPower = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Vgate") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Vgate") );
	_Vgate = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Well") != string::npos ) {
	Line = mceReply.substr( mceReply.find("Well") );
	_wellDepth = Line.substr( Line.find(">")+2, 1 );
      }
      if( mceReply.find("Detector Bias") != string::npos ) { //this is Bias Level setting
	Line = mceReply.substr( mceReply.find("Detector Bias") );
	_biasLevel = Line.substr( Line.find(">")+2, 1 );
      }
    }
    else {
      string message = "TermServ I/O failed to check MCE status!";
      clog<<"UFGemMCE4Agent::checkMCEstatus> "<<message<<endl;
      if( _epics != "false" ) {
	sendEpics( _epics + ":sad:observationStatus", message );
	sendEpics( _epics + ":sad:dcState", message );
      }
    }
  }
  else if( _sim ) {
    _wellDepth = UFRuntime::numToStr( UFMCE4Config::getWellDepth() );
    _biasLevel = UFRuntime::numToStr( UFMCE4Config::getBiasLevel() );
  }

  if( _epics != "false" ) {
    if( _biasPower == "0" || _Vgate == "0" )
      sendEpics( _epics + ":dc:DCBiasG.VALC", "off" );
    else
      sendEpics( _epics + ":dc:DCBiasG.VALC", "on" );
    sendEpics( _epics + ":dc:DCBiasG.VALI", _wellDepth );
    sendEpics( _epics + ":dc:DCBiasG.VALJ", _biasLevel );
  }

  clog<<"UFGemMCE4Agent::checkMCEstatus> biasPower="<<_biasPower<<", Vgate="<<_Vgate
      <<", wellDepth="<<_wellDepth<<", biasLevel="<<_biasLevel<<endl;
}

void* UFGemMCE4Agent::_simMCE4(void* p) {
  return p;
}

void UFGemMCE4Agent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0 )
    sleep(_Update); // no connections, no queued reqs., go to sleep
  else
    UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
}

// These are the key virtual functions to override.
// Send command string to TermServ, and return response.
// Presumably any allocated memory is freed by the calling layer....
// This version must always return null to (epics) clients, and
// instead send the command completion status to designated CARs (actually J input of Gensub ? ).
// If no CAR is desginated, an error/alarm condition should be indicated.
// new signature for action:

int UFGemMCE4Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFGemMCE4Agent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_client= true;
  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    if( _verbose )
      clog<<"UFGemMCE4Agent::action> No UFProtocol replies will be sent to Gemini/Epics client: "
	  <<act->clientinfo<<endl;
  }
  int elemcnt = (int)act->cmd_name.size();
  if( _verbose )
    clog<<"UFGemMCE4Agent::action> client: "<<act->clientinfo<<", req. elem.: "<<elemcnt<<endl;

  string car= "", errmsg= "";
  bool sim, raw, acq, status, readdac= false;
  int dac= -1, parm= -1, parmcnt = 0;

  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    errmsg = UFMCE4Config::stripErrmsg( cmdname );

    if( _verbose ) {
      clog<<"UFGemMCE4Agent::action> cmdname: "<<cmdname<<", cmdimpl: "<<cmdimpl<<endl;
      clog<<"UFGemMCE4Agent::action> errmsg: "<<errmsg<<endl;
    }

    //convert to uppercase for convenience:
    UFStrings::upperCase( cmdname );

    if( cmdname.find("CAR") != string::npos ) {
      reply_client= false;
      car = cmdimpl;
      clog<<"UFGemMCE4Agent::action> CAR: "<<car<<endl;
      continue;
    }

    //now convert to uppercase for convenience (after CAR has been saved):
    UFStrings::upperCase( cmdimpl );

    // now check for raw/real or sim commands
    // command-line option can force all actions to be simulated
    sim = _sim || cmdname.find("SIM") != string::npos;
 
    // if not simulation, expect either a raw/direct mce cmd or status/info req. or
    // a parameter configuration or an observation start/stop/abort:
    // status directive can be "FITS", "full/nonFITS" or "partial/nonFITS", the latter
    // is simply anything other than full. full status directive
    // returns not only mce4 state, but also the full list _activeH * _activeP
    // (and meta values?)
    raw    = cmdname.find("RAW") != string::npos;
    status = cmdname.find("STA") != string::npos;
    acq    = cmdname.find("ACQ") != string::npos;

    // two types of dac params: bias or preamp
    if( cmdname.find("BIAS") != string::npos ||
	cmdimpl.find("BIAS") != string::npos )
      dac = UFMCE4Config::DACBias;

    if( cmdname.find("PREAMP") != string::npos ||
	cmdimpl.find("PREAMP") != string::npos )
      dac = UFMCE4Config::DACPreamp;

    // three types of parms can be submitted (hardware, physical, meta):
    if( cmdname.find("PARM") != string::npos ) {
      if( cmdimpl.find("CLE") != string::npos ) {
	parm = 0;
	parmcnt = 0; 
      }
      if( cmdimpl.find("HARD") != string::npos ) {
	parm = UFMCE4Config::HdwrParm;
	parmcnt = UFMCE4Config::HdwrParmCnt; 
      }
      if( cmdimpl.find("PHYS") != string::npos ) {
	parm = UFMCE4Config::PhysParm;
	parmcnt = UFMCE4Config::PhysParmCnt;
      }
      if( cmdimpl.find("META") != string::npos ) {
	parm = UFMCE4Config::MetaParm;
	parmcnt = UFMCE4Config::MetaParmCnt;
      }
    }

    if( !(sim || status || raw || acq) && (dac < 0) && (parm < 0) ) { // directive not found/supported
      // quit
      errmsg += "?Bad directive " + cmdname + " " + cmdimpl;
      clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
      if( car != "" ) sendEpics(car, errmsg);
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    int nr = 1;
    if( !(status || raw) ) // any form of status or raw OK
      nr =_config->validCmd(cmdimpl); // check non-status directives

    if( nr < 0 ) {   // invalid cmd ?
      errmsg += "?Invalid MCE4 req: " + cmdname + " " + cmdimpl;
      clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
      act->cmd_reply.push_back(errmsg);
      //Do NOT reject commands thought to be invalid, MCE4 does not care anyway!
      //if( car != "" ) sendEpics(car, errmsg);
      //if( !reply_client )
      //  return (UFStrings*) 0;
      // this should be freed by the calling func...
      //return new UFStrings(act->clientinfo, act->cmd_reply); 
    }

    act->time_submitted = currentTime();
    _active = act;

    int stat = -1;
    string reply= "";
    string mcestatus= "";

    if( cmdimpl.find("STATUS") != string::npos)
      mcestatus = "status";

    if( sim ) {
      // simulate mce4 queries:
      strstream s;
      if( UFMCE4Config::_idle ) {
	if( UFMCE4Config::isConfigured() ) 
	  s << "(Sim. mode:, " << cmdname << "=>" << cmdimpl << ") MCE4 configured & idle" <<ends;
	else
	  s << "(Sim. mode: " << cmdname << "=>" << cmdimpl << ") MCE4 not configured & idle" <<ends;
      }
      else {
        s << "(Sim. mode: " << cmdname << "=>" << cmdimpl << ") MCE4 is cycling" <<ends;
      }
      reply = s.str(); delete s.str();
      if( _verbose )
	clog<<"UFGemMCE4Agent::action> "<<reply<<endl;
    }

    if( raw || status || mcestatus != "" ) {
      // if raw, use the annex/iocomm port to submit the action command and get reply:
      // (really should be calling DeviceConfig functions here)
      if( _verbose )
        clog<<"UFGemMCE4Agent::action> submit raw: "<<cmdimpl<<endl;
      // a raw stop directive should only be applied to mce4, not to any lingering frame acq.
      if( status && mcestatus != "" && _config->_devIO ) // submit mce status command directly
        stat = _config->_devIO->submit(mcestatus, reply, _flush); // expect single line reply
      else if( _config->_devIO ) // submit raw non-acq. command directly
        stat = _config->_devIO->submit(cmdimpl, reply, _flush); // expect single line reply?
      if( _verbose )
        clog<<"UFGemMCE4Agent::action> reply: "<<reply<<endl;
      if( _DevHistory.size() >= _DevHistSz )
        _DevHistory.pop_front();
      _DevHistory.push_back(reply);
      if( _config->_devIO && stat < 0 ) {
        errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        if( car != "" ) sendEpics(car, errmsg);
        if( _verbose )
          clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by the calling func...
      } // end failed dev. comm.

      act->cmd_reply.push_back(reply);
      // don't break here, allow multiple raw/status directives in a bundle...
    }
    else if( acq ) { 
      _statRec = car; //this allows acquisition status to be sent to EPICS acqControl gensub/CAR
      // parse for acq. attributes, if any, and store in class attributes:
      _acqAttributes( act );
      // perform directives involving the FrmAcq server and MCE4 (configure/start/abort/datum):
      if( cmdimpl.find("CONF") != string::npos ) {
        // first, apply the configuration to MCE4, then configure frm acq. daemon: 
        stat = UFMCE4Config::acquisition(cmdimpl, reply, _flush);
	if( stat >= 0 ) {
          UFStrings* frmreply = notifyFrmAcq(cmdimpl, act); // non "ERROR" reply means its ready
	  if( frmreply == 0 ) {
	    stat = -1;
	    errmsg = "FAS"; //indicate that Frm.Acq.Serv. did not accept command.
	  }
	  else {
	    if( (frmreply->name()).find("ERROR") != string::npos ) {
	      stat = -2;
	      errmsg = *(frmreply->stringAt(0)); //the error in Frm.Acq.Server
	    }
	    delete frmreply; //always clean up memory.
          }
        } // notifyFrmAcq
      } // CONF
      else {
	// start/run/abort/stop/datum/park/test: must first notify Frm. Acq. server, then MCE4:
        UFStrings* frmreply = notifyFrmAcq( cmdimpl );
        if( frmreply ) {
          if( (frmreply->name()).find("ERROR") == string::npos ) {
            // now can we start/abort/test MCE4:
	    stat = UFMCE4Config::acquisition(cmdimpl, reply, _flush);
	  }
          else {  //command invalid or caused error in Frm.Acq.Server
            stat = -2;
            errmsg = *(frmreply->stringAt(0));
          }
          delete frmreply;
        }
        else { //indicate that Frm.Acq.Server did not accept command, but start MCE anyway...
	  stat = -1;
	  errmsg = "FAS";
	  UFMCE4Config::acquisition(cmdimpl, reply, _flush);
	}
	//special cases:
	// for DATUM: re-read the meta-Config parameter file,
	// for PARK: power down the array and close connections to other agents.
	// for TEST: re-query MCE for basic frame times.
	if( cmdimpl.find("DATUM") != string::npos ) {
	  if( ! UFMCE4Config::readMetaConfigFile() ) {
	    errmsg = "ERROR reading MetaConfig parameter file";
	    stat = -1;
	  }
	}
	else if( cmdimpl.find("PARK") != string::npos ) {
	  clog << "UFGemMCE4Agent::action> closing connections to other agents..."<<endl;
	  UFSocket::closeAndClear(_agentConnections);
	  vector<string> biasCmds;
	  biasCmds.push_back("BiasPark");
	  biasCmds.push_back("ARRAY_POWER_DOWN_BIAS"); //power off bias voltages to detector.
	  UFStrings BiasCmds( "BiasCmds", biasCmds );
	  UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
	  bool readflag, initflag;
	  int statDAC = UFMCE4Config::cmdDAC( BiasCmdinfo, readflag, initflag );
	  clog<<"UFGemMCE4Agent::action> PARK cmd DAC status = "<<statDAC<<endl;
	}
	else if( cmdimpl.find("TEST") != string::npos ) {
	  if( ! initMCEtiming() ) {
	    errmsg = "ERR: failed query MCE frame times";
	    stat = -1;
	  }
	}
      } // eng of acq: start/run/stop/abort/datum/park/test
      // digest acq error messages:
      if( stat < 0 ) {
	//If error message is from frm.acq.server (stat == -2) and sending to Epics,
	// then extract essential trailing content of message because of Epics 40 char limit.
        if( stat < -1 && car != "" && errmsg.find(">") != string::npos )
	  errmsg = errmsg.substr( errmsg.find(">")+1 );
	errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
	if( car != "" ) sendEpics(car, errmsg);
        if( _epics != "false" ) sendEpics( _epics + ":sad:observationStatus", errmsg );
	if( _verbose )
	  clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
	act->cmd_reply.push_back(errmsg);
	if( !reply_client )
	  return (UFStrings*) 0;
	return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by calling func.
      }
      act->cmd_reply.push_back(reply);
      break; // this should  have processed the entire bundle of acquisition directives.
    } // acq
    else if( parm >= 0 ) {
      string obsmode, readoutmode;
      // get parms from remqaining elements in cmdname[], cmdimpl[] vecs.
      if( parm == 0 )
        stat = UFMCE4Config::clearAll();
      else if( parm == UFMCE4Config::HdwrParm ) {
        UFMCE4Config::HdwrParms hdwr;
        // set desired hparms from the given hardware values
        // don't submit to mce4 until a acq-config/commit is received
        stat = UFMCE4Config::parse(act, hdwr, obsmode, readoutmode);
        if( stat <= 0 )
          clog<<"UFGemMCE4Agent::action> ? no Hardware parms ?"<<endl;
        else
          UFMCE4Config::setDes(hdwr, obsmode, readoutmode);
      }
      else if( parm == UFMCE4Config::PhysParm ) {
        UFMCE4Config::PhysParms phys;
        // set desired p & hparms from the given physical values
        // don't submit to mce4 until a acq-config/commit is received
        stat = UFMCE4Config::parse(act, phys, obsmode, readoutmode);
        if( stat <= 0 )
          clog<<"UFGemMCE4Agent::action> ? no Physical parms ?"<<endl;
        else
          UFMCE4Config::setDes(phys, obsmode, readoutmode);
      }
      else if( parm == UFMCE4Config::MetaParm ) {
        // compute desired physical & hardware parms from the given meta-parameters:
        UFMCE4Config::TextParms metaParms;
        stat = UFMCE4Config::parse( act, metaParms );
        if( stat <= 0 ) {
	  errmsg = "no Meta-parameters?";
          clog<<"UFGemMCE4Agent::action> ? "<<errmsg<<endl;
	}
        else if( (stat = UFMCE4Config::setDes( metaParms, errmsg )) >= 0 ) {
	  checkMCEstatus();
	  // after computing desired params, configure MCE4 CT counters and Frm. Acq. server:
	  stat = UFMCE4Config::acquisition("CONF", reply, _flush);
	  if( stat >= 0 ) {
	    UFStrings* frmreply = notifyFrmAcq("CONF", act);
	    if( frmreply == 0 ) {
	      stat = -1;
	      errmsg = "FAS"; //indicate that Frm.Acq.Serv. did not accept command.
	    }
	    else {
	      if( (frmreply->name()).find("ERROR") != string::npos ) { //no ERROR in reply means ready
		stat = -2;
		errmsg = *(frmreply->stringAt(0));
		//if sending to Epics, extract essential trailing content of error message
		//                     so more of it fits into into Epics 40 char limit:
		if( car != "" && errmsg.find(">") != string::npos )
		  errmsg = errmsg.substr( errmsg.find(">")+1 );
	      }
	      delete frmreply;
	    }
	  } // notifyFrmAcq
	} // config mce4
      } // meta-parm
      // handle error messages:
      if( stat < 0 ) {
	errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        if( car != "" ) sendEpics(car, errmsg);
        if( _epics != "false" ) sendEpics( _epics + ":sad:observationStatus", errmsg );
        if( _verbose )
          clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by calling func...
      } // failed directive

      act->cmd_reply.push_back(reply);
      // assume for simplicity that if the cmd-bundle is a ParmConfig,
      break; // we have processed the entire ParmConfig bundle of directives.
    } // parms
    else if( dac > 0 ) {
      // submit DAC cmd(s) to MCE4
      // cmdDAC() gets dac (bias/preamp) parms from remaining elements in cmdname[], cmdimpl[] vecs.
      bool didInitBias = false;
      stat = UFMCE4Config::cmdDAC(act, readdac, didInitBias, _flush);
      if( stat <= 0 ) {
        string mcemsg = act->cmd_reply.back();
        clog<<"UFGemMCE4Agent::action> "<<mcemsg<<endl;
	int colon = 0;
	if( mcemsg.find(":") != string::npos )
	  colon = (int)mcemsg.find(":") + 1;
        if( car != "" ) sendEpics(car, mcemsg.substr(colon) );
	errmsg += "? Failed: "+cmdname+" => "+cmdimpl;
        clog<<"UFGemMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
	// after error is indicated to Epics,
	//  get MCE status, parse it for info about state of bias voltages,
	//   and send status to epics bias records (if epics db is available):
	checkMCEstatus();
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // should be freed by  calling func...
      }
      if( didInitBias )
	_nDTqueryFail = -1; //causes ancillary() method to check and send the Det.Temp. to MCE4.
      break;
      // we have processed the entire DAC Bias/Preamp Config bundle of directives.
      // assume for simplicity that the cmd-bundle is all dac related.
    } // dac
    else {
      clog<<"UFGemMCE4Agent::action> NO action on command:" << cmdname << " => " << cmdimpl<<endl;
    }
    // store supplementary reply/replies
    if( status && reply_client ) {
      if( cmdimpl.find("FIT") == string::npos ) {
        // client wants full status, include active parameter settings, non-FITS format:
        if( !UFMCE4Config::isConfigured() )
	  act->cmd_reply.push_back("MCE4 is uninitialized/unconfigured");
        else
  	  act->cmd_reply.push_back("MCE4 has been configured at least once");

        vector< string > des;
        int n = UFMCE4Config::getDes(des);
        for( int i = 0; i < n; ++i )
          act->cmd_reply.push_back(des[i]);

        vector< string > actv;
        n = UFMCE4Config::getActive(actv);
        for( int i = 0; i < n; ++i )
          act->cmd_reply.push_back(actv[i]);
      } // non-fits
      else { // FITS status request should be last in bundle
	return _config->statusFITS(this);
      } // fits
    } // status reply to client (if non-epics client)
  } // end for loop over cmd bundle

  // if the client is an epics db (car != "" ) send ok or error
  // completion of all req. in this bundle.
  // if it was a Parm==Hardware, only send back OK or Error
  // if it was a Parm==Physical, send back adjusted physical & hardware parms 
  // if it was a Parm==Meta, also send back adjusted physical & hardware parms 
  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    if( _verbose )
      clog<<"UFGemMCE4Agent::action> one or more cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    if( car != "" ) sendEpics(car, errmsg);
  }
  else {
    if( _verbose )
      clog<<"UFGemMCE4Agent::action> cmds. complete, set CAR to idle... "<<endl;
    act->status_cmd = "succeeded";
    if( parm >= 0 ) {
      // before sending OK, send back values...
      if( car != "" && parm == UFMCE4Config::PhysParm) {
        vector< string > parmvec;
        int n = UFMCE4Config::trecsPhysCAR(parmvec);
	n = sendEpics(car, parmvec);
      }
      else if( car != "" && parm == UFMCE4Config::MetaParm) {
        vector< string > parmvec;
        int n = UFMCE4Config::trecsMetaCAR(parmvec);
	n = sendEpics(car, parmvec);
      }
      if( reply_client ) {
        map< string, string > parmvec;
        int n = UFMCE4Config::getDes(parmvec);
        map< string, string >::iterator itr = parmvec.begin();
        while( itr != parmvec.end() && --n >= 0 ){
          string key = itr->first;
          string val = itr->second;
	  act->cmd_reply.push_back(key + " == " + val);
	  ++itr;
        }
      }
    }
    if( car != "" && readdac ) {
      if( dac == UFMCE4Config::DACBias ) { // dac bias readings requested
	vector< string > dacvec;
	int n = UFMCE4Config::trecsBiasCAR(dacvec);
	n = sendEpics(car, dacvec);
      }
      else if( dac == UFMCE4Config::DACPreamp ) { // dac preamp readings requested
	vector< string > dacvec;
	int n = UFMCE4Config::trecsPreampCAR(dacvec);
	n = sendEpics(car, dacvec);
      }
    }
    if( car != "" ) sendEpics(car, "OK");
  }
 
  // if action was a DAC bias cmd, after success is indicated to Epics,
  //  get MCE status, parse it for info about state of bias voltages,
  //   and send status to epics bias records (if epics db is available):
  if( dac == UFMCE4Config::DACBias )
    checkMCEstatus();

  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client )
    return (UFStrings*) 0;

  return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
} //end of action() method. 

int UFGemMCE4Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

int UFGemMCE4Agent::_acqAttributes(UFDeviceAgent::CmdInfo* act) { 
  int cnt= 0;
  // get acq. attributes
  if( act == 0 ) {
    clog<<"UFGemMCE4Agent::_acqAttributes> null pointer arg?"<<endl;
    return cnt;
  }
  // car, "carval" is optional, followed by
  // acq, "conf"; obsId, "DHS Label"; DHSwrite, "save|discard"; etc...
  int elemcnt = (int)act->cmd_name.size();

  for( int i = 0; i < elemcnt; ++i ) {
    string val = act->cmd_impl[i];
    string key = act->cmd_name[i];
    UFStrings::upperCase( key );

    if( key.find("OBSID") != string::npos || key.find("DHSLABEL") != string::npos ) {
      ++cnt;
      _dhsLabel = val;
    }
    else if( key.find("DHS") != string::npos ) { //the DHSwrite status.
      ++cnt;
      _archive = true; //assume data mode = save
      _dhsWrite = true;
      UFStrings::upperCase( val );
      if( val.find("DISCARD") != string::npos ) { //no saving data 
	_archive = false;
	if( val.find("ALL") != string::npos ) _dhsWrite = false; //do not send to dhs either.
      }
      else if( val.find("NO") != string::npos ) _dhsWrite = false; //archive locally but do not send to dhs.
    }
    else if( key.find("NOD") != string::npos ) {
      ++cnt;
      UFStrings::upperCase( val );
      if( val.find("TRUE") != string::npos )
        _nodHandShake = true;
      else
	_nodHandShake = false;
    }
    else if( key.find("COMMENT") != string::npos ) {
      ++cnt;
      _comment = val;
      _fastmode = false;
      UFStrings::upperCase( val );
      if( val.find("FAST") != string::npos  ) _fastmode = true;
    }
    else if( key.find("ARCHIVE") != string::npos ) {
      if( key.find("HOST") != string::npos ) {
	if( val != _frmhost ) {
	  UFClientSocket* soc = new UFClientSocket;
	  int fd = soc->connect( val, _frmport, false); //do not block on connect.
	  if( fd > 0 ) {
	    if( _frmsoc ) {      //if able to connect to frame acq. server on new host,
	      _frmsoc->close();  // then close old connection.
	      delete _frmsoc;
	      _frmsoc = 0;
	    }
	    _frmhost = val;
	    clog<<"UFGemMCE4Agent::_acqAttributes> new frame server host = "<<_frmhost<<endl;
	  } else {
	    clog<<"UFGemMCE4Agent::_acqAttributes> failed connect to new frame server host = "<<val<<endl;
	    clog<<"UFGemMCE4Agent::_acqAttributes> keeping current frame server host = "<<_frmhost<<endl;
	  }
	  soc->close();
	  delete soc;
	  soc = 0;
	}
      }
    }
  } //end of loop over act->cmd...

  clog<<"UFGemMCE4Agent::_acqAttributes> DHS file => "<<_dhsLabel<<endl;
  clog<<"UFGemMCE4Agent::_acqAttributes> DHS write => "<<_dhsWrite<<endl;
  clog<<"UFGemMCE4Agent::_acqAttributes> archive => "<<_archive<<endl;
  clog<<"UFGemMCE4Agent::_acqAttributes> frame server host => "<<_frmhost<<endl;
  clog<<"UFGemMCE4Agent::_acqAttributes> FastMode => "<<_fastmode<<endl;
  clog<<"UFGemMCE4Agent::_acqAttributes> nod HandShake => "<<_nodHandShake<<endl;
  return cnt;
}

//create socket connection to frame acq. server,
// checking and sending basic parameters in objects:

bool UFGemMCE4Agent::connectFrmAcq()
{
  if( _frmsoc == 0 ) {
    UFClientSocket* soc = new UFClientSocket;
    int fd = soc->connect( _frmhost, _frmport, false); //do not block on connect.

    if( fd > 0 ) {
      _frmsoc = soc;
      clog<<"UFGemMCE4Agent::connectFrmAcq> succeeded on port= "<<_frmport<<", host= "<<_frmhost<<endl;
      string Version = "VERSION";
      _frmsoc->send(new UFTimeStamp(Version));
      UFTimeStamp* frmversion = dynamic_cast<UFTimeStamp*>(UFProtocol::createFrom(*_frmsoc));

      if( frmversion ) {
	Version = frmversion->name();
	clog<<"UFGemMCE4Agent::connectFrmAcq> RCS version: "<<Version<<endl;
	clog<<"UFGemMCE4Agent::connectFrmAcq> FrmAcqServer was started at: "<<frmversion->timeStamp()<<endl;
	if( Version.find("Id:") != string::npos )
	  _FrmAcqVersion = Version.substr(Version.find("Id:")+4);
	else
	  _FrmAcqVersion = Version;
      }
      else {
	clog<<"UFGemMCE4Agent::connectFrmAcq> error in VERSION transaction: NULL reply received."<<endl;
	clog<<"UFGemMCE4Agent::connectFrmAcq> closing socket connection to frameAcqServer."<<endl;
	_frmsoc->close();
	delete _frmsoc;
	_frmsoc = 0;
	return false;
      }

      _sentLUTs = sendFrmAcqLUTs(); //try sending pixel Lookup Tables
    }
    else {
      clog<<"UFGemMCE4Agent::connectFrmAcq> failed on port= "<<_frmport<<", host= "<<_frmhost<<endl;
      delete soc;
      return false;
    }
  }
  return true;
}

//define and send the pixel Lookup Tables (LUTs) to frame acq. server:

bool UFGemMCE4Agent::sendFrmAcqLUTs()
{
  UFFrameConfig* frameConfig = new UFFrameConfig( _arrayColumns, _arrayRows, 32 );
  _frmsoc->send( frameConfig );
  UFStrings* frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
  bool initLUTs = true;

  if( frmreply )
    {
      clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> "<<frmreply->name()<<":\n....."<<*frmreply->stringAt(0)<<endl;

      if( frmreply->name() == "ERROR" ) {
	clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> FrameConfig definition rejected."<<endl;
	initLUTs = false;
      }
      else {
	vector< UFInts* >LUTs = UFMCE4Config::setRaytheonLUTs( _arrayColumns, _arrayRows, _arrayChannels );

	for( int i=0; i < (int)(LUTs.size()); i++ )
	  {
	    UFInts* LUT = LUTs[i];
	    clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> sending the "<<LUT->name()<<endl;
	    _frmsoc->send(LUT);
	    delete frmreply;
	    frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));

	    if( frmreply ) {
	      clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> "<<frmreply->name()
		  <<": "<<*frmreply->stringAt(0)<<endl;
	      if( frmreply->name() == "ERROR" ) {
		clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> pixel map LUT definition rejected."<<endl;
		initLUTs = false;
	      }
	    } else {
	      clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> error in LUT definition: NULL reply received."<<endl;
	      initLUTs = false;
	    }
	    delete LUT;
	  }
      }
    }
  else {
    clog<<"UFGemMCE4Agent::sendFrmAcqLUTs> error in FrameConfig definition: NULL reply received."<<endl;
    initLUTs = false;
  }

  return initLUTs;
}

//Method to poll the Frame Acq. Server for current FrameConfig,
// which contains frame counts and status.
// If cannot connect then return zero,
// if send/recv fails, then try connecting one more time.

UFFrameConfig* UFGemMCE4Agent::pollFrmAcq()
{
  if( !connectFrmAcq() ) return 0;
  UFTimeStamp* pollfc = new UFTimeStamp("FC");
  _frmsoc->send(pollfc);
  UFFrameConfig* frmPollReply = dynamic_cast<UFFrameConfig*>(UFProtocol::createFrom(*_frmsoc));

  if( frmPollReply == 0 ) {
    clog<<"UFGemMCE4Agent::pollFrmAcq> error during FrameConfig polling: null object received"<<endl;
    clog<<"UFGemMCE4Agent::pollFrmAcq> closing & reopening socket to frameAcqServer..."<<endl;
    _frmsoc->close();
    delete _frmsoc;
    _frmsoc = 0;
    if( connectFrmAcq() ) {      //try just one more time...
      _frmsoc->send(pollfc);
      frmPollReply = dynamic_cast<UFFrameConfig*>(UFProtocol::createFrom(*_frmsoc));
    }
  }

  return frmPollReply;
}
 
UFStrings* UFGemMCE4Agent::notifyFrmAcq(const string& directive, UFDeviceAgent::CmdInfo* act) {
  if( !connectFrmAcq() ) return 0;
  UFStrings* frmreply = 0;
  string Directive = directive;
  UFStrings::upperCase( Directive );

  if( _abortedObsConf ) {  //clear out any previously aborted obs info before starting.
    delete _abortedObsConf;
    _abortedObsConf = 0;
  }

  // configure directive: creates FrameConfig and ObsConfig and sends them to the FrmAcq server.
  // Or if directive is "TEST" then execute first part (check EDT camera type) but do not config.

  if( Directive.find("CONF") != string::npos ||
      Directive.find("TEST") != string::npos )
    {
      //request current EDT Camera Type Configuration from acq server and check it:
      UFTimeStamp reqCamType("CT");
      _frmsoc->send( reqCamType );
      UFTimeStamp* CTreply = dynamic_cast<UFTimeStamp*>(UFProtocol::createFrom(*_frmsoc));

      if( !CTreply ) {
	clog<<"UFGemMCE4Agent::notifyFrmAcq> error in "<<Directive<<" transact: NULL reply recvd"<<endl;
	clog<<"UFGemMCE4Agent::notifyFrmAcq> closing frameAcqServer socket and trying again ..."<<endl;
	_frmsoc->close(); delete _frmsoc; _frmsoc = 0;
	if( connectFrmAcq() ) {
	  _frmsoc->send( reqCamType );
	  CTreply = dynamic_cast<UFTimeStamp*>(UFProtocol::createFrom(*_frmsoc));
	}
      }

      if( CTreply ) {
	string cameraType = CTreply->name();
	clog<<"UFGemMCE4Agent::notifyFrmAcq> EDT camera type = "<<cameraType<<endl;
	if( _epics != "false" ) {
	  sendEpics( _epics + ":sad:observationStatus", Directive + ": EDTcamType=" + cameraType );
	  sendEpics( _epics + ":sad:dcState", Directive + ": EDTcamType=" + cameraType );
	  sendEpics( _epics + ":sad:detType", cameraType );
	}
	if( !_sim ) {
	  UFStrings::upperCase( cameraType );
	  if( cameraType.find("RAYTHEON")==string::npos && cameraType.find("CRC-774")==string::npos ) {
	    string message = "INVALID camera type:" + cameraType + ", EDT is not configured?";
	    clog<<"UFGemMCE4Agent::notifyFrmAcq> WARNING: "<<message<<endl;
	    if( _epics != "false" ) {
	      sendEpics( _epics + ":sad:observationStatus", message );
	      sendEpics( _epics + ":sad:dcState", message );
	    }
	    if( Directive.find("TEST") != string::npos )
	      return new UFStrings( "ERROR", &message );
	  }
	}
	if( Directive.find("TEST") != string::npos ) { //if just TEST-ing return now...
	  string message = "EDTcamType=" + cameraType;
	  return new UFStrings( "TEST=OK", &message );
	}
      }
      else {
	string message = "failed obtain current EDT camera type from FrameAcqServer.";
	clog<<"UFGemMCE4Agent::notifyFrmAcq> ERROR: "<<message<<endl;
	if( _epics != "false" ) {
	  sendEpics( _epics + ":sad:observationStatus", message );
	  sendEpics( _epics + ":sad:dcState", message );
	}
	return new UFStrings( "ERROR", &message );
      }

     if( !_sentLUTs ) _sentLUTs = sendFrmAcqLUTs(); //make sure pixel Lookup Tables are Loaded.

      //send current ObsConfig to frame acq. server:
      string obsmode, rdoutmode;
      UFMCE4Config::HdwrParms hp;
      if( UFMCE4Config::getActive(obsmode, rdoutmode, hp) <= 0 ) {
	clog<<"UFGemMCE4Agent::notifyFrmAcq> No active hardware parms, unable to send ObsConfig."<<endl;
	return 0;
      }
      int coaddsPerFrm = hp["FrameCoadds"];
      if( hp["ChopCoadds"] > 0 ) coaddsPerFrm *= hp["ChopCoadds"];
      int saveSets = hp["SaveSets"];
      int nodSets = hp["NodSets"];
      int nodbeams= 1, chopbeams= 1;
      UFObsConfig::beams(obsmode, nodbeams, chopbeams);
      UFStrings::upperCase( rdoutmode );
      //if RAW is requested for multi-sample readout mode, pretend it is chopping and nodding,
      // (note that in RAW case the obsmode should be just "stare")
      // so frame acq. server will process the readout frames seperately (instead of subtracting).
      if( rdoutmode.find("RAW") != string::npos ) {
	if( rdoutmode.find("S1R1") != string::npos || rdoutmode.find("1S1R") != string::npos ) {
	  chopbeams = 2;
	  nodbeams = 1;
	}
	if( rdoutmode.find("S1R3") != string::npos || rdoutmode.find("1S3R") != string::npos ) {
	  chopbeams = 2;
	  nodbeams = 2;
	  saveSets = 1;
	  nodSets = hp["SaveSets"] * hp["NodSets"];
	}
      }
      string dataLabel = _dhsLabel;
      if( !_dhsWrite )
	dataLabel += "(no_dhs)"; //instruct FAS to not send to DHS (could still archive locally).
      else if( !_archive )
	dataLabel += "(discard)"; //data is not saved but still sent to DHS quick-look.
      delete _ObsConf;
      _ObsConf = new UFObsConfig( dataLabel, nodbeams, chopbeams, saveSets, nodSets, coaddsPerFrm );
      _ObsConf->setReadoutMode( rdoutmode );
      clog<<"UFGemMCE4Agent::notifyFrmAcq> ObsConfig: "<<_ObsConf->name()<<endl;
      _frmsoc->send(_ObsConf);
      if( frmreply ) delete frmreply;
      frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));

      if( ! checkFrmAcqReply( frmreply, "ObsConfig" ) )
	return frmreply;

      //send current Frame Configuration (in particular the savePeriod) to acq server:
      UFFrameConfig* frameConfig = new UFFrameConfig( _arrayColumns, _arrayRows, 32 );
      frameConfig->frameCoadds( hp["FrameCoadds"]);
      frameConfig->chopSettleFrms( hp["ChopSettleReads"]);
      frameConfig->chopCoadds( hp["ChopCoadds"]);
      frameConfig->frameTime( UFMCE4Config::getFrameTime() );
      float SaveFreq = UFMCE4Config::getSaveFrequency();
      if( SaveFreq <= 0 ) {
	clog<<"UFGemMCE4Agent::notifyFrmAcq> ??? bad SaveFreq = "<<SaveFreq<<endl;
	SaveFreq = 0.0001;
      }
      frameConfig->savePeriod( 1/SaveFreq );
      _frmsoc->send( frameConfig );
      if( frmreply ) delete frmreply;
      frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));

      if( ! checkFrmAcqReply( frmreply, "FrameConfig" ) )
	return frmreply;

      _ObsCompStat = "CONFIGURED: " + UFRuntime::numToStr( _ObsConf->totFrameCnt() ) + " frames";
      string photonTime, onSrcTime;
      UFMCE4Config::PhysParms php;
      if( UFMCE4Config::getActive(obsmode, rdoutmode, php) > 0 ) {
	onSrcTime = UFRuntime::numToStr( php["OnSourceTime"] );
	photonTime = UFRuntime::numToStr( php["ElapsedTime"] );
	if( onSrcTime.find(".") != string::npos )
	  onSrcTime = onSrcTime.substr( 0, onSrcTime.find(".")+3 );
	if( photonTime.find(".") != string::npos )
	  photonTime = photonTime.substr( 0, photonTime.find(".")+3 );
	_ObsCompStat += ", " + photonTime + " mins total";
      }

      clog<<"UFGemMCE4Agent::notifyFrmAcq> "<<_ObsCompStat<<endl;
      _frmAcqNow = false;
      _BeamSwitching = false;
      _BeamSwitchError = false;
      _prevNodBeam = -1; // nod beam = 0 or 1 during an obs.
      _prevFrmCnt = 0;
      _MCE4waitError = false;
      if( _statRec != "" ) sendEpics(_statRec,"0");
      setEpicsAcqFlags( false, false ); //obs NOT yet in progress.

      if( _epics != "false" ) {
	sendEpics( _epics + ":sad:readoutMode", rdoutmode );
	sendEpics( _epics + ":sad:obsMode", obsmode );
	sendEpics( _epics + ":sad:currentBeam", "A" );
	sendEpics( _epics + ":sad:currentNodCycle", "0" );
	sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
	sendEpics( _epics + ":sad:dcState", _ObsCompStat );
	if( photonTime.length() > 1 ) {
	  sendEpics( _epics + ":sad:photonTime", photonTime );
	  sendEpics( _epics + ":sad:onSourceTime", onSrcTime );
	}
      }

      return frmreply; //return status of CONFIG directive.
    }

  // start/run directive:

  if( Directive.find("START") != string::npos || Directive.find("RUN") != string::npos )
    {
      string dataLabel = _dhsLabel;
      if( !_dhsWrite )
	dataLabel += "(no_dhs)"; //instruct FAS to not send to DHS (could still archive locally).
      else if( !_archive )
	dataLabel += "(discard)"; //data is not saved but still sent to DHS quick-look.

      if( _ObsConf->dataLabel() != dataLabel ) { //if dataLabel has changed then resend it.
	_ObsConf->reLabel( dataLabel );
	clog<<"UFGemMCE4Agent::notifyFrmAcq> ObsConfig: "<<_ObsConf->name()<<endl;
	_frmsoc->send(_ObsConf);
	if( frmreply ) delete frmreply;
	frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	if( ! checkFrmAcqReply( frmreply, "ObsConfig" ) )
	  return frmreply;
      }

      _ObsCompStat = "STARTING: " + _ObsConf->dataLabel();
      if( _epics != "false" ) {
	sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
	sendEpics( _epics + ":sad:dcState", _ObsCompStat );
	string airmass = getEpics( "tcs", ":sad:airMassNow" );
	airMassBeg = ::atof( airmass.c_str() ); //this will go in FITS header.
      }

      // Create FITS header object with NAXIS keywords, ObsId, etc., and send it to FrameAcqServer.
      // First check status of MCE to get current WellDepth and BiasLevel:
      checkMCEstatus();
      // Delete the global FrameConfig so method basicFITSheader will create new object with zeros:
      delete _FrmConf; _FrmConf=0;
      UFFITSheader* DCagentFITSheader = basicFITSheader();
      UFMCE4Config::makeFITSheader( DCagentFITSheader );
      UFSocket::closeAndClear(_agentConnections); //force re-connect to make sure all agents talking.
      UFStrings* agentsFITShdrs = fetchFITSheaders();
      UFStrings* obsFITSheader;

      if( (agentsFITShdrs->name()).find("ERROR") == string::npos )
	obsFITSheader = new UFStrings( DCagentFITSheader->Strings("HEADER"), agentsFITShdrs );
      else {
	DCagentFITSheader->end();
	obsFITSheader = DCagentFITSheader->Strings("HEADER");
	if( _epics != "false" )
	  sendEpics( _epics + ":sad:dcState", "ERR: Failed get FITS info from agents!");
      }
      
      clog<<"UFGemMCE4Agent::notifyFrmAcq> sending FITS header..."<<endl;
      obsFITSheader->rename("HEADER"); //make sure.
      _frmsoc->send( obsFITSheader );
      if( frmreply ) delete frmreply;
      frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
      if( !checkFrmAcqReply( frmreply, "FITS header" ) )
	return frmreply;

      string FITSfile = "none";
      string FITShost = "none";

      if( _archive ) {
	// Open the FITS file by requesting FrmAcqServer to automatically create
	//  the file and directory for current year-day on local disk in specified root dir:
	UFStrings openfits("AUTO_CREATE", &_archvRootDir);
	_frmsoc->send(openfits); // send the open, recv. reply
	if( frmreply ) delete frmreply;
	frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	if( !checkFrmAcqReply( frmreply, "AUTO_CREATE FITS file" ) )
	  return frmreply;
	if( frmreply->elements() > 1 ) 
	  FITSfile = *(frmreply->stringAt(1));
	if( frmreply->elements() > 2 ) 
	  FITShost = *(frmreply->stringAt(2));
      }

      if( _epics != "false" ) {
	sendEpics( _epics + ":sad:localFileName", FITSfile );
	sendEpics( _epics + ":sad:localFileDir", FITShost );
      }

      if( _fastmode ) {
	UFTimeStamp wait("FPWAIT");  // send the directive to delay frame processing/writing.
	_frmsoc->send(wait);
	if( frmreply ) delete frmreply;
	frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	if( !checkFrmAcqReply( frmreply, "FPWAIT" ) )
	  return frmreply;
      }

      UFTimeStamp start("START");    // default (zero) is infinite (no) EDT timeout.
      int TimeOut = 0;
      //MCE waits for stable secondary mirror chopping before data is valid:
      int preValidTime = (int)ceil( UFMCE4Config::getPreValidChopTime() );

      if( _ObsConf->nodBeams() > 1 )  // EDT timeout if nodding:
	    TimeOut = max( preValidTime, (int)ceil( _maxBeamSwitchTime ) );
      else {
	float SaveFreq = UFMCE4Config::getSaveFrequency();
	if( SaveFreq > 0 )
	  TimeOut = (int)ceil( 1/SaveFreq );  //else set EDT timeout > Save Period.
	if( _ObsConf->chopBeams() > 1 )  // if chopping then timeout > Save Period + PreValidChopTime
	  TimeOut += preValidTime;
      }

      //add 7 secs in case of lag between FrmAcq & MCE and rename to start with EDT timeout value:
      if( TimeOut > 0 )
	start.rename("START  " + UFRuntime::numToStr( TimeOut+7 ));

      _frmsoc->send(start); // send the start obs. directive, recv. reply.
      if( frmreply ) delete frmreply;
      frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));

      if( checkFrmAcqReply( frmreply, Directive ) ) {
	_frmAcqNow = true;
	_BeamSwitching = false;  //always set beam switch to false after START
	_BeamSwitchError = false;
	_MCE4waitError = false;
	_prevNodBeam = -1; // nod beam = 0 or 1 during an obs.
	_prevFrmCnt = 0;
	_bgADUStart = 0;
	//request current EDT Camera Type Configuration from acq server and post it:
	_frmsoc->send(new UFTimeStamp("CT"));
	UFTimeStamp* CTreply = dynamic_cast<UFTimeStamp*>(UFProtocol::createFrom(*_frmsoc));
	_ObsCompStat = "OBSERVING: EDTcamType=" + CTreply->name();
	clog<<"UFGemMCE4Agent::notifyFrmAcq> "<<_ObsCompStat<<endl;
	if( _epics != "false" ) {
	  sendEpics( _epics + ":sad:dcState", _ObsCompStat );
	  sendEpics( _epics + ":sad:observationStatus", _ObsCompStat );
	  sendEpics( _epics + ":sad:currentBeam", "A" );
	  if( _ObsConf->nodBeams() > 1 )
	    sendEpics( _epics + ":sad:currentNodCycle", "1" );
	  else
	    sendEpics( _epics + ":sad:currentNodCycle", "0" );
	}
	if( _statRec != "" ) sendEpics(_statRec,"0");
	setEpicsAcqFlags( true, true );  //indicate obs. in progress to other epics systems.
      }
      else if( _archive ) { // Close the FITS file if START did not succeed:
	UFStrings closefits("CLOSE");
	_frmsoc->send(closefits); // send the close, recv. reply
	UFStrings* frmreply2 = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	checkFrmAcqReply( frmreply2, "CLOSE" );
	frmreply = new UFStrings( frmreply, frmreply2 );
	delete frmreply2;
      }

      return frmreply; //return status of START directive.
    }

  // stop/abort/datum/park directive:
  // note that ancillary() will clean up and CLOSE after ABORT.

  if( Directive.find("STOP") != string::npos || Directive.find("ABORT") != string::npos || 
      Directive.find("DATUM") != string::npos || Directive.find("PARK") != string::npos ) {

    UFTimeStamp abort("ABORT");
    if( Directive.find("STOP") != string::npos ) abort.rename("STOP");
    _frmsoc->send(abort); // send abort/stop, recv reply...
    if( frmreply ) delete frmreply;
    frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
    bool FASOK = checkFrmAcqReply( frmreply, Directive );

    if( ! FASOK ) { //note that checkFrmAcqReply() automatically tries to re-connect on error.
      if( connectFrmAcq() ) {
	_frmsoc->send(abort); // try sending abort again, recv reply...
	if( frmreply ) delete frmreply;
	frmreply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
	FASOK = checkFrmAcqReply( frmreply, Directive );
      }
    }

    if( FASOK ) {
      delete _FrmConf;
      _FrmConf = pollFrmAcq();
      if( _FrmConf && _statRec != "" ) //send final frame grab count to epics db:
	sendEpics(_statRec, UFRuntime::numToStr(_FrmConf->frameGrabCnt()) );
    }
    else return frmreply;

    //for datum/park directive do some more resets and re-inits:

    if( Directive.find("DATUM") != string::npos || Directive.find("PARK") != string::npos ) {
      // make sure FITS file is closed:
      UFStrings closefits("CLOSE");
      _frmsoc->send(closefits); // send the close, recv. reply
      UFStrings* frmreply2 = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*_frmsoc));
      checkFrmAcqReply( frmreply2, "CLOSE" );
      if( frmreply && frmreply2 ) {
	frmreply = new UFStrings( frmreply, frmreply2 );
	delete frmreply2;
      }
      // send Pixel Maps to frm.acq.server again:
      _sentLUTs = sendFrmAcqLUTs();
    }

    return frmreply; //return status of ABORT/STOP/DATUM/PARK directive.
  }

  // if we get here it must have been a bad or unknown directive (none of the above):
  clog<<"UFGemMCE4Agent::notifyFrmAcq> ?Bad or unknown directive: "<<Directive<<endl;
  return 0;
}

bool UFGemMCE4Agent::checkFrmAcqReply( UFStrings* frmreply, const string& transact ) {
  if( frmreply ) {
    clog<<"UFGemMCE4Agent::checkFrmAcqReply> "<<frmreply->name()<<":\n";
    for( int i=0; i < frmreply->elements(); i++ )
      clog << ".............." << *(frmreply->stringAt(i)) <<endl;
    if( (frmreply->name()).find("ERROR") != string::npos ) return false;
    return true;
  }
  else {
    clog<<"UFGemMCE4Agent::checkFrmAcqReply> error in "<<transact<<" transact: NULL reply recvd"<<endl;
    // This check for valid connection may not work:
    if( !_frmsoc->validConnection() )
      clog<<"UFGemMCE4Agent::checkFrmAcqReply> socket connection to frameAcqServer NOT valid..."<<endl;
    clog<<"UFGemMCE4Agent::checkFrmAcqReply> closing socket and re-connecting to frameAcqServer..."<<endl;
    _frmsoc->close();
    delete _frmsoc;
    _frmsoc = 0;
    connectFrmAcq();
    return false;
  }
}

// Method to create the basic FITS header which contains NAXIS values,
// defined using the current _FrmConf, _ObsConf, and _ObsCompStat, etc..
// Optionally will use a different abortObsConf if given, describing an aborted observation.

UFFITSheader* UFGemMCE4Agent::basicFITSheader()
{
  if( _FrmConf == 0 ) _FrmConf= new UFFrameConfig( _arrayColumns, _arrayRows, 32 );
  UFFITSheader* FITSheader = 0;
  //use ObsConfig for aborted observation if present:
  if( _abortedObsConf )
    FITSheader = new UFFITSheader( *_FrmConf, *_abortedObsConf, "Written by "+_FrmAcqVersion );
  else
    FITSheader = new UFFITSheader( *_FrmConf, *_ObsConf, "Written by "+_FrmAcqVersion );

  if( FITSheader ) {
    FITSheader->add("INSTRUMENT","T-ReCS",                 "name of instrument");
    FITSheader->add("ARRAY",     "raytheon_774",           "name of detector array & MUX");
    FITSheader->add("DHSLABEL", _dhsLabel,                 "DHS Label (= DHS data file name)");
    FITSheader->add("USRNOTE",  _comment,                  "Optional note made by user about obs.");
    FITSheader->add("COMPSTAT", _ObsCompStat,              "Observation completion status");
    FITSheader->add("SIGFRM",   _FrmConf->sigmaFrmNoise(), "st.dev. of bg+read noise in last frame (ADU)");
    FITSheader->add("SIGFMAX",  _FrmConf->sigmaFrmMax(),   "maximum st.dev. of bg+read noise per frame");
    FITSheader->add("SIGFMIN",  _FrmConf->sigmaFrmMin(),   "minimum st.dev. of bg+read noise per frame");
    FITSheader->add("BKASTART", _bgADUStart,               "Background ADUs at start of obs.");
    FITSheader->add("BKAEND",   _FrmConf->bgADUs(),        "Background ADUs at end of obs.");
    FITSheader->add("BKAMAX",   _FrmConf->bgADUmax(),      "maximum ADUs det. readout during obs.");
    FITSheader->add("BKAMIN",   _FrmConf->bgADUmin(),      "minimum ADUs det. readout during obs.");
    FITSheader->add("BKWSTART", _bgWellStart,              "% filling of det. wells at start of obs.");
    FITSheader->add("BKWEND",   _FrmConf->bgWellpc(),      "% filling of det. wells at end of obs.");
    FITSheader->add("BKWMAX",   _bgWellMax,                "maximum % filling of det. wells during obs.");
    FITSheader->add("BKWMIN",   _bgWellMin,                "minimum % filling of det. wells during obs.");
    FITSheader->add("SIGREAD",  _FrmConf->sigmaReadNoise(),"st.dev. of read noise in last frame (ADU)");
    FITSheader->add("SIGRMAX",  _FrmConf->sigmaReadMax(),  "maximum st.dev. of read noise per frame");
    FITSheader->add("SIGRMIN",  _FrmConf->sigmaReadMin(),  "minimum st.dev. of read noise per frame");
    FITSheader->add("RDASTART", _rdADUStart,               "Det. clamped off readout ADUs at start of obs.");
    FITSheader->add("RDAEND",   _FrmConf->rdADUs(),        "Det. clamped off readout ADUs at end of obs.");
    FITSheader->add("RDAMAX",   _FrmConf->rdADUmax(),      "maximum ADUs det-off readout during obs.");
    FITSheader->add("RDAMIN",   _FrmConf->rdADUmin(),      "minimum ADUs det-off readout during obs.");
    FITSheader->add("WELLDPTH", _wellDepth,                "detector well depth: 0=shallow, 1=deep");
    FITSheader->add("BIASLEVL", _biasLevel,                "bias V_detgrv: 0=off, 1=Low, 2=Med, 3=Hi");
 // FITSheader->add("BIASVDAC", UFMCE4Config::VdetgrvDAC(),"DAC control value for bias V_detgrv");
    FITSheader->add("MJD-OBS",  getEpics( _epics, ":wcs:mjdobs" ), "MJD at observation start");
    FITSheader->add("RADECSYS", getEpics( _epics, ":wcs:radecsys" ), "R.A.-DEC. coordinate system ref.");
    FITSheader->add("EQUINOX",  getEpics( _epics, ":wcs:equinox" ), "Equinox of coordinate system");
    FITSheader->add("CTYPE1",   getEpics( _epics, ":wcs:ctype1" ), "R.A. in tangent plane projection");
    FITSheader->add("CRPIX1",   getEpics( _epics, ":wcs:crpix1" ), "Ref pix of axis 1");
    FITSheader->add("CRVAL1",   getEpics( _epics, ":wcs:crval1" ), "RA at Ref pix in decimal degrees");
    FITSheader->add("CTYPE2",   getEpics( _epics, ":wcs:ctype2" ), "DEC. in tangent plane projection");
    FITSheader->add("CRPIX2",   getEpics( _epics, ":wcs:crpix2" ), "Ref pix of axis 2");
    FITSheader->add("CRVAL2",   getEpics( _epics, ":wcs:crval2" ), "DEC at Ref pix in decimal degrees");
    FITSheader->add("CD1_1",    getEpics( _epics, ":wcs:cd11" ), "WCS matrix element 1,1");
    FITSheader->add("CD1_2",    getEpics( _epics, ":wcs:cd12" ), "WCS matrix element 1,2");
    FITSheader->add("CD2_1",    getEpics( _epics, ":wcs:cd21" ), "WCS matrix element 2,1");
    FITSheader->add("CD2_2",    getEpics( _epics, ":wcs:cd22" ), "WCS matrix element 2,2");
    float airMassEnd = 0.0;
    if( _epics != "false" ) {
      string airmass = getEpics( "tcs", ":sad:airMassNow" );
      airMassEnd = ::atof( airmass.c_str() );
    }
    FITSheader->add("AIRMASS1",  airMassBeg, "Air Mass at start of obs.");
    FITSheader->add("AIRMASS2",  airMassEnd, "Air Mass at end of obs.");
  }
  return FITSheader;
}

// Setup Agent Connections for query of FITS header
// (from agents other than current UFGemMCE4Agent, must NOT connect to itself!).

bool UFGemMCE4Agent::AgentsConnected()
{
  if( !_agentFITSClient ) _agentFITSClient = new UFFITSClient("ufgmce4d");
  bool block = false;

  if( _agentConnections.empty() )
    {
      if( _agentLocs.size() < 4 )
	{
	  if( _devAgsHost.length() < 1 ) _devAgsHost = UFRuntime::hostname();
	  _agentLocs.clear();
	  _agentLocs["ufgls218d"]  = new UFFITSClient::AgentLocation(_devAgsHost, 52002); //Lakeshore 218
	  _agentLocs["ufgls340d"]  = new UFFITSClient::AgentLocation(_devAgsHost, 52003); //Lakeshore 340
	  _agentLocs["ufgvac354d"] = new UFFITSClient::AgentLocation(_devAgsHost, 52004); //pressure gauge
	  _agentLocs["ufgmotord"]  = new UFFITSClient::AgentLocation(_devAgsHost, 52005); //Portescap motors
	}

      clog << "UFGemMCE4Agent::AgentsConnected> connecting to other Device Agents on host: "
	   <<_devAgsHost<<" (block="<<block<<")"<<endl;
      //note that MCE4 agent must NOT connect to itself since it is single thread!!!

      if( _agentFITSClient->connectAgents( _agentLocs, _agentConnections, block ) <= 0 )
	{
	  clog<<"UFGemMCE4Agent::AgentsConnected> ERROR: no connections to other device agents !"<<endl;
	  return false;
	}
    }
  return true;
}

// Fetch other parts of FITS header from the other device agents:

UFStrings* UFGemMCE4Agent::fetchFITSheaders() {
  float timeOut = 4.0;
  string message;

  if( AgentsConnected() ) {
      clog<<"UFGemMCE4Agent::fetchFITSheader> fetching FITS header parts from other Device Agents..."<<endl;
      UFStrings* agentFITSheaders = _agentFITSClient->fetchAllFITS( _agentConnections, timeOut );

      if( agentFITSheaders == 0 ) {
	_agentConnections.clear();
	message = "UFGemMCE4Agent::fetchFITSheader> UFFITSClient::fetchAllFITS from other Agents failed!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
      }

      if( agentFITSheaders->elements() < 2 ) {
	_agentConnections.clear();
	message = "UFGemMCE4Agent::fetchFITSheader> FITS headers from other Agents are NOT valid!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
      }

      agentFITSheaders->rename("HEADER (from other Device Agents)");
      return agentFITSheaders;
    }
  else {
	message = "UFGemMCE4Agent::fetchFITSheader> failed to connect to other agents!";
	clog << "ERROR: " << message <<endl;
	return new (nothrow) UFStrings( "ERROR", &message ) ;
  }
}

void UFGemMCE4Agent::setEpicsAcqFlags(bool acq, bool rdout) {
  if( _epics == "false" ) return;

  string acqdbrec = _epics + ":acq";
  string rdoutdbrec = _epics + ":rdout";

  if( acq )
    sendEpics(acqdbrec, "1");
  else
    sendEpics(acqdbrec, "0");

  if( rdout )
    sendEpics(rdoutdbrec, "1");
  else
    sendEpics(rdoutdbrec, "0");

  string prepdbrec = _epics + ":prep";
  sendEpics(prepdbrec, "0");  //prep should always be false while observing.
}

#endif // UFGemMCE4Agent
