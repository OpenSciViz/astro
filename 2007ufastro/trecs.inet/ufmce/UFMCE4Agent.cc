#if !defined(__UFMCE4Agent_cc__)
#define __UFMCE4Agent_cc__ "$Name:  $ $Id: UFMCE4Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFMCE4Agent_cc__;

#include "UFMCE4Agent.h"
__UFMCE4Agent_H__(MCE4Agent_cc);

#include "UFMCE4Config.h"
__UFMCE4Config_H__(MCE4Agent_cc);

#include "UFEdtDMA.h"
__UFEdtDMA_H__(MCE4Agent_cc);

#include "sys/types.h"
#include "sys/wait.h"

//public:
// statics
bool UFMCE4Agent::_verbose= false;
UFClientSocket* UFMCE4Agent::_frmsoc = 0;
string  UFMCE4Agent::_frmhost = "localhost";
int UFMCE4Agent::_frmport = 52000;

// ctors:
UFMCE4Agent::UFMCE4Agent(int argc, char** argv,
			       char** envp) : UFDeviceAgent(argc, argv, envp) {
  //_Update = 2.0;
  _flush = 0.05; // 0.1; // 0.35; // inherited value is not quite rigth for the 340
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFMCE4Agent::UFMCE4Agent(const string& name, int argc,
			 char** argv, char** envp) : UFDeviceAgent(name, argc, argv, envp) {
  //_Update = 2.0;
  _flush = 0.05; //  0.1; //0.35;
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

int UFMCE4Agent::main(int argc, char** argv, char** envp) {
  UFMCE4Agent ufs("UFMCE4Agent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFMCE4Agent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  string arg;
  
  _config = new UFMCE4Config(); // needs to be proper device subclass
  // defaults for flamingos1:
  _config->_tsport = 7008;
  _config->_tshost = "192.168.111.101";

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-frmport"); 
  if( arg != "false" && arg != "true" )
    _frmport = atoi(arg.c_str());

  arg = findArg("-frmhost");
  if( arg != "false" && arg != "true" )
    _frmhost = arg;
  
  arg = findArg("-sim"); // epics host/db name
  if( arg != "false" )
    _sim = true;

  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52005;
  }
  if( _verbose ) 
    clog<<"UFMCE4Agent::options> set port to MCE4 port "<<port<<endl;

  return port;
}

void UFMCE4Agent::startup() {
  setSignalHandler(UFRndRobinServ::sighandlerDefault);
  clog << "UFMCE4Agent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  init();
  if( _config->_devIO == 0 ) {
    clog<<"UFMCE4Agent::startup> no Device connection, assume simulation? "<<endl;
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFMCE4Agent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;
}

UFTermServ* UFMCE4Agent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( ! _sim ) {
    // connect to the device:
    ts =_config->connect();
    if( _config->_devIO == 0 ) {
      clog<<"UFMCE4Agent> failed to connect to device port..."<<endl;
      return 0;
    }
    //ts->resetEOTS("\r"); // config object does this, and should do:
    ts->resetEOTR(">>"); // mce4 returns prompt
  }

  // test connectivity to mce4, save transaction to a file
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.mce4."<<p<<ends;
  string mcefile = s.str(); delete s.str();
  int fd = ::open(mcefile.c_str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFMCE4Agent> unable to open "<<s.str()<<endl;
    return ts;
  }

  if( _sim ) {
    int  nc = ::write(fd, "UFMCE4Agent> simulation.", strlen("UFMCE4Agent> simulation."));
    if( nc <= 0 ) {
      clog<<"UFMCE4Agent> sim log failed."<<endl;
    }
    ::close(fd);
    return 0;
  }

  // test connectivity to mce4:
  vector< string > mce4buf; // mce4 reply vec 
  string sp(" ");
  //This submit method with vector arg is no longer supported by UFTermServ class.
  //  int nlines = _config->_devIO->submit(sp, mce4buf); // test for a prompt
  int nlines = 1;
  if( nlines <= 0 ) {
    clog<<"UFMCE4Agent> test for prompt failed..."<<endl;
    return _config->_devIO;
  }
  string mce4prompt = mce4buf[nlines-1];

  // get status
  string status("status");
  //This submit method with vector arg is no longer supported by UFTermServ class.
  //  nlines = _config->_devIO->submit(status, mce4buf);
  if( nlines <= 0 ) {
    clog<<"UFMCE4Agent> test for status failed..."<<endl;
    return _config->_devIO;
  }
  for( int i = 0; i < (int)mce4buf.size(); ++i ) {
    string s = mce4buf[i];
    //clog<<s<<ends;
    int n = ::write(fd, s.c_str(), s.length());
    if( n < (int)s.length() )
      clog<<"UFMCE4Agent> unable to write "<<mcefile<<endl;
  }

  ::close(fd);
  return ts;
}

void* UFMCE4Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  int w, h, lutId;
  string t_idle= "false", m_idle= "false";
  string applyingLut = "false";
  if( UFEdtDMA::takeIdle(w, h) )
    t_idle = "true";
  if( UFEdtDMA::applyingLut(lutId) )
    applyingLut = "true";
  if( UFMCE4Config::_idle )
    m_idle = "true";

  if( _verbose ) {
    clog<<"UFMCE4Agent::ancillary> mce4 idle: "<<m_idle
        <<", take idle: "<<t_idle<<", applyingLUT: "<<applyingLut
        <<", frames/total: "<< UFEdtDMA::takeCnt()<<" / "<<UFEdtDMA::takeTotCnt()<<endl;

    clog<<"UFMCE4Agent::ancillary> UFMCE4Config::_takepid: "<<UFMCE4Config::_takepid<<endl;
  }

  if( UFMCE4Config::_takepid > 0 ) {
    // child ufgtake is executing, need to clean up after it when it completes 
    int status;
    pid_t p = ::waitpid(UFMCE4Config::_takepid, &status, WNOHANG);
    while( errno == ECHILD )
      p = ::waitpid(UFMCE4Config::_takepid, &status, WNOHANG);
    int frms = UFEdtDMA::takeCnt();
    int totfrms = UFEdtDMA::takeTotCnt();
    if( p == UFMCE4Config::_takepid ) { // ufgtake has exited
      if( _verbose ) {
        if( totfrms == frms )
          clog<<"UFMCE4Agent::ancillary> all frames acquired: "<<frms<<"/"<<totfrms<<endl;
        else
          clog<<"UFMCE4Agent::ancillary> ?missed frames? "<<frms<<"/"<<totfrms<<endl;
      }
      UFMCE4Config::_idle = true;
      UFMCE4Config::_takepid = (pid_t) -1;
    }
    else if( p < 0 ) { // error, child long gone?
      clog<<"UFMCE4Agent::ancillary> ?take child exited error? "<<errStr()<<endl;
      clog<<"UFMCE4Agent::ancillary> frame status: "<<frms<<"/"<<totfrms<<endl;
      UFMCE4Config::_idle = true;
      UFMCE4Config::_takepid = (pid_t) -1;
    }
    else if( p == 0 && _verbose )
      clog<<"UFMCE4Agent::ancillary> frame acquisition in progress: "<<frms<<"/"<<totfrms<<endl;
  }
  return p;
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// to portescap indexor.
// this version must always return null to (epics) clients, and
// instead send the command completion status to designated CARs,
// if no CAR is desginated, an error /alarm condition should be indicated
int UFMCE4Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFMCE4Agent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_client= true;
  // this simlper/non-gemini version of the agent could
  // still be conacted by an epics client, if so try
  // not to talk back on the socket...
  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    clog<<"UFMCE4Agent::action> No replies will be sent to ini/Epics client: "
        <<act->clientinfo<<endl;
  }
  //if( act->cmd_name.size() % 2 != 0 ) {
  if( _verbose ) {
    clog<<"UFMCE4Agent::action> ?ini/Epics client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFMCE4Agent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
    //return (UFStrings*) 0;      
  }
  string errmsg;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    if( _verbose ) {
      clog<<"UFMCE4Agent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFMCE4Agent::action> cmdimpl: "<<cmdimpl<<endl;
    }
    int errIdx = cmdname.find("!!");
    if( errIdx != (int)string::npos ) 
      errmsg = cmdname.substr(2+errIdx);

    // now check for raw/real or sim commands
    // command-line option can force all actions to be simulated
    bool sim = _sim ||
               cmdname.find("sim") != string::npos ||
	       cmdname.find("Sim") != string::npos ||
               cmdname.find("SIM") != string::npos;
 
    // if not simulation, expect either a (raw) status/info req. or direct command OR
    // a parameter configuration or an observation start/stop/abort:

    bool raw = cmdname.find("raw") != string::npos ||
               cmdname.find("Raw") != string::npos ||
               cmdname.find("RAW") != string::npos;

    bool acq = cmdname.find("acq") != string::npos ||
               cmdname.find("Acq") != string::npos ||
               cmdname.find("ACQ") != string::npos;

    if( !( sim || raw || acq ) ) { // directive not found/supported
      // quit
      errmsg += "Badly formatted directive? " + cmdname + " " + cmdimpl;
      if( _verbose )
	clog<<"UFMCE4Agent::action> "<<errmsg<<endl;
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    int nr = _config->validCmd(cmdimpl);
    if( nr < 0 ) {
      // invalid cmd
      errmsg += "?Invalid MCE4 req? " + cmdname + " " + cmdimpl;
      if( _verbose )
	clog<<"UFMCE4Agent::action> "<<errmsg<<endl;
      //if( car != "" ) setCARError(car, &errmsg); // set error
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    act->time_submitted = currentTime();
    _active = act;
    // deny start/run directive it if mce4 is busy (not idle) from prior start directive
    /* however, sometimes it takes more than one start/run directive to get mce to act...
    if( UFMCE4Config::validAcq(cmdimpl) == 0 && // start directive
        !UFMCE4Config::_idle ) {
        errmsg += " MCE4 not idle, Reject: " + cmdimpl;
        clog<<"UFMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }
    */
    string status= "";
    if( cmdimpl.find("status") != string::npos ||
	cmdimpl.find("Status") != string::npos ||
	cmdimpl.find("STATUS") != string::npos) { // return stopped/started
      status = "status";
    }
    string reply= "";
    int stat = -1;
    if( sim && UFMCE4Config::validAcq(cmdimpl) < 0 ) { 
      // reply default for simple sim status idle/busy
      reply = "sim: " + cmdimpl;
      // simulate mce4 queries:
      strstream s;
      if( UFMCE4Config::_idle )
	s << cmdimpl << " MCE4 is idle" <<ends;
      else
        s << cmdimpl << " MCE4 is cycling" <<ends;

      reply = s.str(); delete s.str();
      if( _verbose )
	clog<<"UFMCE4Agent::action> Sim: "<<reply<<endl;
    }
    // if raw, use the annex/iocomm port to submit the action command and get reply:
    // (should be calling DeviceConfig functions here)
    if( raw || status != "" ) {
      //if( _verbose )
      clog<<"UFMCE4Agent::action> submit raw: "<<cmdimpl<<endl;
      // a raw directive should only be applied to mce4, not to any lingering frame acq.
      if( _config->_devIO ) // submit raw non-acq. command directly
        stat = _config->_devIO->submit(cmdimpl, reply, _flush); // expect single line reply?
      //if( _verbose )
	clog<<"UFMCE4Agent::action> reply: "<<reply<<endl;
      if( _DevHistory.size() >= _DevHistSz )
	_DevHistory.pop_front();
      _DevHistory.push_back(reply);
      if( stat < 0 ) {
	errmsg += "Device communication failed";
	//if( car != "" ) setCARError(car, &errmsg); // set error
        if( _verbose )
	  clog<<"UFMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      } // end failed dev. comm.
    }
    else if( acq ) {
      // this should toggle _idle to false if no error occurs:
      clog<<"UFMCE4Agent::action> submit acq: "<<cmdimpl<<endl;
      stat = UFMCE4Config::acquisition(cmdimpl, reply, _flush);
      if( stat < 0 ) {
	errmsg += "Acquisition directive failed";
        if( _verbose )
	  clog<<"UFMCE4Agent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      clog<<"UFMCE4Agent::action> reply: "<<reply<<endl;
    } // acquisition directive
    // store reply
    act->cmd_reply.push_back(reply);
  } // end for loop of cmd bundle

  // if the client is an epics db (car != "" ) send ok or error
  // completion of all req. in this bundle.
  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    if( _verbose )
    act->status_cmd = "failed";
  }
  else {
    act->status_cmd = "succeeded";
  }
 
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client )
    return (UFStrings*) 0;

  return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
} 

int UFMCE4Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFMCE4Agent
