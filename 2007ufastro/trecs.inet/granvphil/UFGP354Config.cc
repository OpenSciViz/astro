#if !defined(__UFGP354Config_cc__)
#define __UFGP354Config_cc__ "$Name:  $ $Id: UFGP354Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGP354Config_cc__;

#include "UFGP354Config.h"
#include "UFGemGranPhil354Agent.h"
#include "UFFITSheader.h"

UFGP354Config::UFGP354Config(const string& name) : UFDeviceConfig(name) { _devIO = 0; }

UFGP354Config::UFGP354Config(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// GP354 terminator is carriage-retuen + new-line:
string UFGP354Config::terminator() { return "\r"; }

// GP354 prefix is "#" follow by address, since we have only one address:
string UFGP354Config::prefix() { return "#01"; }

vector< string >& UFGP354Config::queryCmds() {
  return _queries;
}

vector< string >& UFGP354Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("RD"); _actions.push_back("rd"); // reading
  _actions.push_back("SE0"); _actions.push_back("se0"); // set emission current to 0.1mA
  _actions.push_back("IG0"); _actions.push_back("ig0"); // turn off ion gauge
  _actions.push_back("IG1"); _actions.push_back("ig1"); // turn on ion gauge
  
  return _actions;
}

int UFGP354Config::validCmd(const string& c) {
  vector< string >& cv = actionCmds();
  int i= 0, cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) != string::npos )
      return 1;
  }
  
  cv = queryCmds();
  i= 0; cnt = (int)cv.size();
  while( i < cnt ) {
    if( c.find(cv[i++]) == 0 )
      return 1;
  }
  clog<<"UFGP354Config::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFGP354Config::status(UFDeviceAgent* da) {
  UFGemGranPhil354Agent* da354 = dynamic_cast< UFGemGranPhil354Agent* > (da);
  if( da354->_verbose )
    clog<<"UFLS354Config::status> "<<da354->name()<<endl;
  string g354 = da354->getCurrent(); 
  string delim = " ";
  int pos0= 1 + g354.find(delim);
  // presumably this is the only pressure value
  string vac= "Vaccum == ";
  vac += g354.substr(pos0);
  vac += " Torr";
  return new UFStrings(da354->name(), (string*) &vac);
}

UFStrings* UFGP354Config::statusFITS(UFDeviceAgent* da) {
  UFGemGranPhil354Agent* da354 = dynamic_cast< UFGemGranPhil354Agent* > (da);
  if( da354->_verbose )
    clog<<"UFLS354Config::status> "<<da354->name()<<endl;
  string g354 = da354->getCurrent(); 
  string delim = " ";
  int pos0= 1 + g354.find(delim);
  // presumably this is the only pressure value
  string vac= g354.substr(pos0);

  map< string, string > valhash, comments;
  valhash["Vacuum"] = vac;
  comments["Vacuum"] = "Torr";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da354->name());

  return ufs;
}


#endif // __UFGP354Config_cc__
