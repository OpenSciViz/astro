#if !defined(__ufgls218d_cc__)
#define __ufgls218d_cc__ "$Name:  $ $Id: ufgls218d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgls218d_cc__;

#include "UFGemLakeShore218Agent.h"

int main(int argc, char** argv) {
  try {
    UFGemLakeShore218Agent::main(argc, argv);
  }
  catch( std::exception& e ) {
    clog<<"ufgls218d> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgls218d> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgls218d_cc__
