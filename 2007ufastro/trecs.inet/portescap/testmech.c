#include "stdio.h"
#include "uftrecsmech.h"

int main(int argc, char** argv, char** envp) {
  int m;
  UFTReCSMech *TheTReCSMech = getTheUFTReCSMech();
  for( m = 0; m < TheTReCSMech->mechCnt; ++m ) {
    printf("%s\n", TheTReCSMech->mech[m].name);
  }

  return 0;
}
