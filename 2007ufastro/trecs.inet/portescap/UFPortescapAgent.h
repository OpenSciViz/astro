#if !defined(__UFPortescapAgent_h__)
#define __UFPortescapAgent_h__ "$Name:  $ $Id: UFPortescapAgent.h,v 0.0 2002/06/03 17:42:20 hon beta $"
#define __UFPortescapAgent_H__(arg) const char arg##UFPortescapAgent_h__rcsId[] = __UFPortescapAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFDeviceAgent.h"
#include "UFTermServ.h"

class UFPortescapAgent: public UFDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFPortescapAgent(int argc, char** argv);
  UFPortescapAgent(const string& name, int argc, char** argv);
  inline virtual ~UFPortescapAgent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  virtual UFTermServ* init(const string& host= "192.168.111.200", int port= 7008);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline virtual int getValue(const string& indexor, double& steps) { 
    steps = UFPortescapAgent::_CurPosition[indexor]; return 0;
  }

protected:
  static vector <string>& _Indexors; // names of All indexors 
  static string& _Motors; // default list of indexor names (ala command line)
  static map<string, double> _CurPosition; // current position (steps from home)
};

#endif // __UFPortescapAgent_h__
      
