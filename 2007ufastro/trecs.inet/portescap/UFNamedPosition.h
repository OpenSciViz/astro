#ifndef __UFNAMEDPOSITION_H
#define __UFNAMEDPOSITION_H "$Id: UFNamedPosition.h,v 1.1 2003/06/02 17:58:29 dan beta $"

#include	"string"

#include	"UFNamedPosition.h"

using std::string ;

class UFNamedPosition
{
public:

	/**
	 * Create a new named position given the string directly
	 * from the motor parameters file.  Using this method
	 * requires that all information in the UFStringTokenizer
	 * be present and correctly formed.
	 */
	UFNamedPosition( const UFStringTokenizer& ) ;
	UFNamedPosition( const UFNamedPosition& ) ;
	virtual ~UFNamedPosition() ;

	UFNamedPosition& operator=( const UFNamedPosition& ) ;

	unsigned int	positionNumber ;
	int		offset ;
	double		throughput ;
	double		lambdaLo ;
	double		lambdaHi ;
	string		positionName ;
	string		comment ;

	friend std::ostream& operator<<( std::ostream& out,
		const UFNamedPosition& rhs )
	{
	out	<< rhs.positionNumber << " "
		<< rhs.offset << " "
		<< rhs.throughput << " "
		<< rhs.lambdaLo << " "
		<< rhs.lambdaHi << " "
		<< rhs.positionName << " "
		<< rhs.comment ;
	return out ;
	}

} ;

#endif // __UFNAMEDPOSITION_H
