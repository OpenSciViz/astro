#if !defined(__UFGemPortescapAgent_cc__)
#define __UFGemPortescapAgent_cc__ "$Name:  $ $Id: UFGemPortescapAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemPortescapAgent_cc__;

// useful gdb cmds:
//b 'UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo *)'
//r -epics trecs -tshost ufannex -tsport 7005 -motors "A B C D E F G H I J"
//b UFGemPortescapAgent::_execMovement

#include "UFGemPortescapAgent.h"
#include "UFPortescapConfig.h"

// statics 
static map<string, bool> _sentOkOrErr;

bool UFGemPortescapAgent::_resetHRC= false;
int UFGemPortescapAgent::_instrumId;
string UFGemPortescapAgent::_currentval;
string UFGemPortescapAgent::_reqval;
time_t UFGemPortescapAgent::_timeOut = 180; // seconds
time_t UFGemPortescapAgent::_clock = 0; // seconds
double UFGemPortescapAgent::_NearHome = 10.0;
double UFGemPortescapAgent::_simSpeed = 1.5;
map<string, string> UFGemPortescapAgent::_Status; // use this instead of _StatList for EDB/CA names
map<string, string> UFGemPortescapAgent::_ErrPosition; // error message
map<string, double> UFGemPortescapAgent::_DesPosition; // desired/requested
map<string, double> UFGemPortescapAgent::_CurPosition; // current position (steps from home)
map<string, double> UFGemPortescapAgent::_Motion; // in-motion desired position (steps from home)
map<string, double> UFGemPortescapAgent::_Homing; // homing desired position should always be 0
map<string, double> UFGemPortescapAgent::_HomingFinal; // home-step-home
map<string, double> UFGemPortescapAgent::_NearHoming; // homing desired position should always be 0
map<string, string> UFGemPortescapAgent::_HomeThenStep; // compound motion seq.: first home then step
map<string, string> UFGemPortescapAgent::_HomeStepHome; // compound motion: home then step then home step parms
map<string, string> UFGemPortescapAgent::_HomeHome; // compound motion: home then step then home 2nd home parms
map<string, string> UFGemPortescapAgent::_InitVelCmd; // last/latest initial velocity command setting

//public:
// ctors:
UFGemPortescapAgent::UFGemPortescapAgent(int argc,
					 char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve Portescap readings
  _flush = 0.1; // 0.05; // 0.35; 
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":cc:configG";
  _heart = _epics + ":cc:heartbeat";
  _statRec = ""; // CAR should be sent at start of each req. packet
}

UFGemPortescapAgent::UFGemPortescapAgent(const string& name,
					 int argc,
				         char** argv) : UFGemDeviceAgent(name, argc, argv) {
  _flush = 0.1; //  0.05; 
  // use ancillary function to retrieve LS340 readings
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":cc:configG";
  _heart = _epics + ":cc:heartbeat";
  _statRec = ""; // CAR should be sent at start of each req. packet
}

int UFGemPortescapAgent::main(int argc, char** argv) {
  UFGemPortescapAgent ufs("UFGemPortescapAgent", argc, argv); // ctor binds to listen socket
  //UFProtocol::_verbose = true;
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemPortescapAgent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  if( clname.find("trecs:") != string::npos )
    _epics = "trecs";
  else if( clname.find("miri:") != string::npos )  
    _epics = "miri";

  if( !( _epics == "trecs" || _epics == "miri") )
    return clname;

  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string linecnt =  _epics + genSub + ".VALA";
  string positions =  _epics + genSub  + ".VALB";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemPortescapAgent::newClient> Epics client." <<clname<<endl;

  /* don't care right now
  int nr = getEpicsOnBoot(_epics, linecnt, _StatList);
  if( nr > 0 && _StatList.size() > 0 ) {
    linecnt = atof(_StatList[0].c_str());
  }

  nr = getEpicsOnBoot(_epics, positions, _StatList);
  */

  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemPortescapAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemPortescapAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemPortescapAgent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);

  // _epics may be reset by above:
  _confGenSub = _epics + ":cc:configG";
  _heart = _epics + ":cc:heartbeat";

  _config = new UFPortescapConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = 7005;
  //_config->_tshost = "ufannex"; // "192.168.111.101";
  _config->_tshost = "flamperle"; // "192.168.111.102"; "192.168.111.125"

  int instrumId= UFDeviceAgent::TReCS;
  string arg;
  arg = findArg("-trecs"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::TReCS;
  arg = findArg("-flam"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos2;
  arg = findArg("-flam1"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos1;
  arg = findArg("-flam2"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::Flamingos2;
  arg = findArg("-canari"); 
  if( arg != "false" && arg != "true" )
    _instrumId = UFDeviceAgent::CanariCam;

  // be sure to set indexor names before parsing ports:
  UFPortescapConfig::setIndexorNames(instrumId);
  arg = findArg("-motor"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-motors"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-indexor"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);
  arg = findArg("-indexors"); // motor names
  if( arg != "false" && arg != "true" )
    UFPortescapConfig::setIndexorNames(instrumId, arg);

  // parse ports to determine if party-line or direct-line mode is in use:
  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" ) {
    int np = 0;
    map< string, int > portmap;
    if( _instrumId == UFDeviceAgent::Flamingos1 )
      np = UFPortescapConfig::parsePorts(arg, portmap, "a");
    else // assume default "A"
      np = UFPortescapConfig::parsePorts(arg, portmap);
    clog<<"UFGemPortescapAgent::options> nports: "<<np<<endl;
    if( np > 1 ) {
      UFPortescapConfig::directLines(portmap);
    }
    else {
      _config->_tsport = atoi(arg.c_str());
      UFPortescapConfig::partyLine(_config->_tsport);
    }
  }
  else if( arg == "false" ) { // perhaps provided by indexor list ala: "A:7010 B:2011 ... N:70NN" 
    map< string, int > portmap;
    int np = UFPortescapConfig::parsePorts(_config->_tsport, portmap);
    if( np > 1 ) {
      UFPortescapConfig::directLines(portmap);
    }
    else {
      _config->_tsport = atoi(arg.c_str());
      UFPortescapConfig::partyLine(_config->_tsport);
    }
  }

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-speed");
  if( arg != "false" && arg != "true" )
    _simSpeed = atof(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-v");
  if( arg != "false" )
    UFPortescapConfig::_verbose = _verbose = true;
  else
    UFPortescapConfig::_verbose = _verbose = false;

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  arg = findArg("-reset");
  if( arg != "false" )
    _resetHRC = true;
 
  if( _config->_tsport > 7000 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52005;
  }

  if( _verbose ) 
    clog<<"UFGemPortescapAgent::options> set port: "<<port<<", epics: "<<_epics<<endl;

  return port;
}

void UFGemPortescapAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  if( _verbose )
    clog << "UFGemPortescapAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  /*
  if( _config->_devIO == 0 ) {
    clog<<"UFGemPortescapAgent::startup> no Device config object?"<<endl;
    init();
  }
  else
  */
  init(_config->_tshost, _config->_tsport);

  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemPortescapAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;
}

UFTermServ* UFGemPortescapAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( _verbose )
    clog<<"UFPortescapAgent> indexor names: "<<UFPortescapConfig::_Motors<<endl;
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.portescap."<<p<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemPortescapAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return ts;
  }
  delete s.str();
  */
  if( _sim ) {
    string motor;
    for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
      motor = UFPortescapConfig::_Indexors[i];
      _HomeThenStep[motor] = "false"; // compound motion seq.: first home then step
      _HomeStepHome[motor] = "false"; // compound motion: home then step then home
      _HomeHome[motor] = "false"; // compound motion: home then step then home
      clog<<"UFGemPortescapAgent::init> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
      clog<<"UFGemPortescapAgent::init> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
      _CurPosition[motor] = 0.0;
      clog<<"Initial indexor position: "<<motor<<" == "<<_CurPosition[motor]<<endl;
    }
    /*
    int  nc = ::write(fd, "UFGemPortescapAgent> simulation.", strlen("UFGemPortescapAgent> simulation."));
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> sim log failed."<<endl;
    }
    ::close(fd);
    */
    return 0;
  }
  
  // connect to the device:
  clog<<"UFGemPortescapAgent::init> connect to device port(s)..."<<endl;
  ts = _config->connect(tshost, tsport);
  if( _config->_devIO == 0 ) {
    clog<<"UFGemPortescapAgent::init> failed to connect to device port..."<<endl;
    return 0;
  }
  // test connectivity to indexors, save transaction to a file
  if( _verbose )
    clog<<"UFGemPortescapAgent> checking availiblity of portescap indexors"<<endl;

  string querystat, portescap; // portescap cmd & reply strings 
  string fullstatus = " [ 1990 23 "; // should be function in UFPortescapConfig
  string motor;
  for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    _sentOkOrErr[motor] = false;
    _HomeThenStep[motor] = "false"; // compound motion seq.: first home then step
    _HomeStepHome[motor] = "false"; // compound motion: home then step then home
    _HomeHome[motor] = "false"; // compound motion: home then step then home
    clog<<"UFGemPortescapAgent::init> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
    clog<<"UFGemPortescapAgent::init> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
    clog<<"UFGemPortescapAgent::init> _HomeHome for "<<motor<<" = "<<_HomeHome[motor]<<endl;
    // perform hard abort/clear/escape of any currently executing indexor nonsense
    //_config->_devIO->escape(motor, 0.3); 
    // get current position
    querystat = motor + "Z";
    int nc = _config->_devIO->submit(querystat, portescap, _flush);
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor position status failed..."<<endl;
    }
    string spos = portescap.substr(1+portescap.rfind("Z"));
    double pos = atof(spos.c_str());
    _CurPosition[motor] = pos;
    clog<<"Initial indexor position: "<<motor<<" == "<<_CurPosition[motor]<<endl;

    /*
    // get full status block
    querystat = motor + fullstatus;
    nc = _config->_devIO->submit(querystat, portescap, _flush); 
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status failed..."<<endl;
      break;
    }
    clog<<"UFPortescapAgent> indexor full status succeeded, nc: "<<nc<<endl;
    */
    /*
    nc = ::write(fd, querystat.c_str(), querystat.length());
    nc = ::write(fd, portescap.c_str(), portescap.length());
    if( nc <= 0 ) {
      clog<<"UFPortescapAgent> indexor full status log failed..."<<endl;
    }
    */
  }
  //::close(fd); // closing it forces io flush

  return ts;
}

void* UFGemPortescapAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemPortescapAgent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemPortescapAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
    if( _verbose && (clck % 2 == 0) )
      clog<<"ancillary> sent Epics heartbeat: "<<clck<<", _heart: "<<_heart<<endl;
  }

  // any motion to monitor?
  if( _Motion.empty() && _Homing.empty() && _HomingFinal.empty() && _NearHoming.empty() ) { // none
    if( _verbose && (clck % 2 == 0) )
      clog<<"ancillary> All Motion lists are empty, clock: "<<clck<<endl;
    return p;
  }

  if( _sim || _config->_devIO == 0 ) {
    if( !_NearHoming.empty() ) // motor near homing is simulated
      _simMovement(_NearHoming);
    if( !_Motion.empty() ) // motor motion is simulated
     _simMovement(_Motion);
    if( !_Homing.empty() ) // motor homing is simulated
      _simMovement(_Homing);
    if( !_HomingFinal.empty() ) // final motor homing is simulated
      _simMovement(_HomingFinal);
    return p;
  }

  // this will check _HomeThenStep compound request, and
  // will submit a step motion cmd if necessary to complete
  // the request
  if( !_NearHoming.empty() ) {
    if( _verbose )
      clog<<"ancillary> near homing "<<endl;
    _execMovement(_NearHoming);
  }
  if( !_Motion.empty() ) {
    if( _verbose )
      clog<<"ancillary> stepping "<<endl;
   _execMovement(_Motion);
  }
  if( !_Homing.empty() ) {
    if( _verbose )
      clog<<"ancillary> homing "<<endl;
    _execMovement(_Homing);
  }
  if( !_HomingFinal.empty() ) {
    if( _verbose )
      clog<<"ancillary> final homing "<<endl;
    _execMovement(_HomingFinal);
  }
  return p;
}

void UFGemPortescapAgent::_execMovement(map<string, double>& moving) {
  if( moving.empty() )
    return;

  vector <string> erase;
  string reply, qstopped = " ^ ", qsteps = " Z "; // steps from home/origin
  map<string, double>::iterator i = moving.begin();
  do {
    string indexor = i->first;
    if( _verbose )
      clog<<"UFGemPortescapAgent::_execMovement> indexor: "<<indexor<<endl;

    // since this function is used for all motion (stepping or homing, or home-then-step, or home-step-home)
    bool stepping = !_Motion.empty();
    if( stepping ) { // is this indexor stepping?
      map<string, double>::iterator fnd = _Motion.find(indexor);
      if( fnd == _Motion.end() )
	stepping = false;
    }
    bool nearhoming = !_NearHoming.empty(); // (diff < 0.01);
    if( nearhoming ) { // is this indexor nearhoming?
      map<string, double>::iterator fnd = _NearHoming.find(indexor);
      if( fnd == _NearHoming.end() )
	nearhoming = false;
    }
    // it is possible to (initially) find indexor in _Homing and _HomingFinal
    // so we need to be carefull what is tested here... 
    bool homing = !_Homing.empty(); // (des > -0.01 && des < 0.01);
    bool homingF = !_HomingFinal.empty(); // (des > -0.01 && des < 0.01);
    if( homing ) { // check this indexor homing?
      map<string, double>::iterator fnd = _Homing.find(indexor);
      if( fnd == _Homing.end() ) // not this indexor
	homing = false;
      else
	homingF = false; // force this to false
    }
    if( homingF ) { // check this indexor homingFinal?
      map<string, double>::iterator fnd = _HomingFinal.find(indexor);
      if( fnd == _HomingFinal.end() ) // not this indexor
	homingF = false;
    }

    _statRec = _Status[indexor];
    string errmsg = _ErrPosition[indexor];
    string query = indexor + qsteps;
    _config->_devIO->submit(query, reply, _flush); // expect single line reply
    if( _verbose )
      clog<<"UFGemPortescapAgent::_execMovement> steps reading: "<<reply<<endl;

    int pos = qsteps.length() + reply.find(qsteps);
    _currentval = reply.substr(pos);
    // send current value to epics
    if( _statRec != "" ) {
      sendEpics(_statRec, _currentval);
      if( _verbose )
        clog<<_statRec<<" == "<<_currentval<<endl;
    }
    // check for completed motion:
    double steps = atof(_currentval.c_str());
    double prev_steps = _CurPosition[indexor];
    // if homing _DesPosition[indexor] == 0.0 
    // in positive direction, steps will simply keep increasing
    double pdiff = ::fabs(steps - prev_steps);
    if( pdiff > 0.01 ) // reset current   
      _CurPosition[indexor] = steps;
    double des = _DesPosition[indexor];
    double diff = ::fabs(steps - des);
    // if homing _DesPosition[indexor] == 0.0 
    // in positive direction, steps will simply keep increasing, (diff > pdiff)
    bool checkForCompletion = false;
    if( des > -0.01 && des < 0.01 && pdiff > diff) // homed?
      checkForCompletion = true;
    else if( diff <= 0.01 || pdiff <= 0.01 ) // stepped or near-homed?
      checkForCompletion = true;

    if( checkForCompletion ) { // check if movement is complete (indexor stationary) 
      query = indexor + qstopped;
      _config->_devIO->submit(query, reply, _flush); // expect single line reply
      if( _verbose )
        clog<<"UFGemPortescapAgent::_execMovement> (check completion) motion query: "<<reply<<endl;

      pos = query.length() + reply.find(query);
      const char* ms = (reply.substr(pos)).c_str();
      int mstat = atoi(ms);
      if( _verbose ) {
        clog<<"UFGemPortescapAgent::_execMovement> _HomeThenStep for "<<indexor<<" = "<<_HomeThenStep[indexor]<<endl;
        clog<<"UFGemPortescapAgent::_execMovement> _HomeStepHome for "<<indexor<<" = "<<_HomeStepHome[indexor]<<endl;
        clog<<"UFGemPortescapAgent::_execMovement> _HomeHome for "<<indexor<<" = "<<_HomeHome[indexor]<<endl;
      }
      // if this part of a compund motion, which type?
      size_t snpos = _HomeThenStep[indexor].find("-");
      size_t sppos = _HomeThenStep[indexor].find("+");
      bool homethenstep = false, homestephome = false;
      if( snpos != string::npos || sppos != string::npos )
	homethenstep = true;
      else {
        snpos = _HomeStepHome[indexor].find("-");
        sppos = _HomeStepHome[indexor].find("+");
        if( snpos != string::npos || sppos != string::npos )
	  homestephome = true;
        // or final home
        sppos = _HomeHome[indexor].find("F");
        if( snpos != string::npos || sppos != string::npos )
	  homestephome = true;
      }

      if( mstat == 0 ) { // completed motion, was it real/near homing or stepping?
        erase.push_back(indexor);
	// check position one last time and...
        query = indexor + qsteps;
        if( _verbose )
	  clog<<"UFGemPortescapAgent::_execMovement> (motionless?) steps reading "<<query<<endl;

	//sleep(0.1);
        reply = "";
        _config->_devIO->submit(query, reply, _flush); // expect single line reply
        if( _verbose )
          clog<<"UFGemPortescapAgent::_execMovement> (really completed?) motion query: "<<reply<<endl;

        pos = qsteps.length() + reply.find(qsteps);
        _currentval = reply.substr(pos);
        if( _statRec != "" ) {
          sendEpics(_statRec, _currentval);
          if( _verbose )
            clog<<_statRec<<" == "<<_currentval<<endl;
        }
        steps = atof(_currentval.c_str());
        prev_steps = _CurPosition[indexor];
        pdiff = ::fabs(steps - prev_steps);
        _CurPosition[indexor] = steps;

        bool athome= false;
	if( homing || homingF || nearhoming ) {
	  if( homing || homingF ) { // must send Origin cmd
            string orign = indexor + "O";
            _config->_devIO->submit(orign, reply, _flush); // expect single line reply
            if( _verbose )
              clog<<"UFGemPortescapAgent::_execMovement> homed, sent origin: "<<reply<<endl;
    	    _DesPosition[indexor] = _CurPosition[indexor] = 0.0;
	    _currentval = "0.0"; // orgined
	    // send epics home/origin position value: 
            if( _statRec != "" ) {
	      sendEpics(_statRec, _currentval);
              if( _verbose )
                clog<<_statRec<<" == "<<_currentval<<endl;
            }
            athome = true;
	    // set the datum cnt for this indexor:
	    int datumcnt = UFPortescapConfig::_DatumCnt[indexor];
            if( datumcnt < 0 )
	      datumcnt = 1;
	    else
	      ++datumcnt;
	    UFPortescapConfig::_DatumCnt[indexor] = datumcnt;

	    if( _resetHRC ) {
	      string holdrunc = indexor + " 0 0";
              _config->_devIO->submit(holdrunc, reply, _flush); // expect single line reply
              if( _verbose )
                clog<<"UFGemPortescapAgent::_execMovement> reset hold-run-current: "<<reply<<endl;
	      holdrunc = indexor + " " + UFPortescapConfig::_HoldRunC[indexor];
              _config->_devIO->submit(holdrunc, reply, _flush); // expect single line reply
              if( _verbose )
                clog<<"UFGemPortescapAgent::_execMovement> reset hold-run-current: "<<reply<<endl;
              }
	  }
	  else if( nearhoming ) {
            _DesPosition[indexor] = _NearHoming[indexor]; // _NearHome;
            if( _verbose )
              clog<<"UFGemPortescapAgent::_execMovement> nearhoming indexor, curr. pos.: "
                  <<indexor<<", "<<_CurPosition[indexor]
                  <<", des. pos.: "<<_DesPosition[indexor]
                  <<", _NearHoming[indexor]= "<<_NearHoming[indexor]
                  <<", _NearHome= "<<_NearHome<<endl;
          }
          if( _statRec != "" && !homethenstep && !homestephome ) { // nearhome only
	    sendEpics(_statRec, "OK"); _sentOkOrErr[indexor] = true;
	  }
	} // set athome
	  
	if( homing || homingF || nearhoming ) { // finished (first) real or near homing
	  // homethenstep applies to nearhome cmd as well as real home cmd
	  // homestephome only applies to real home
	  if( (athome || nearhoming) && !homingF && (homethenstep || homestephome) ) {
	    // (first) real/near homing completed, next send step
	    string stepcmd;
            if( homethenstep )
	      stepcmd = _HomeThenStep[indexor];
	    else if( homestephome )
	      stepcmd = _HomeStepHome[indexor];

            if( stepcmd != "" && stepcmd != "false" ) {
	      _config->_devIO->submit(stepcmd, reply, _flush);
              if( homethenstep ) {
                _HomeThenStep[indexor] = "false"; // reset map
                if( _verbose )
                 clog<<"UFGemPortescapAgent::_execMovement> home then step, sent step: "<<reply<<endl;
              }	
              if( homestephome ) {
                _HomeStepHome[indexor] = "false"; // reset map
                if( _verbose )
                  clog<<"UFGemPortescapAgent::_execMovement> home step home, sent step: "<<reply<<endl;
	      }
              string ss;
              double steps = 0.0;
              if( sppos != string::npos ) { // step forwards
	        ss = stepcmd.substr(1+sppos);
                steps += atof(ss.c_str());
              }
              if( snpos != string::npos ) { // step backwards
	        ss = stepcmd.substr(1+snpos);
                steps -= atof(ss.c_str());
              }
              // put motor into motion list
              _Motion[indexor] = _DesPosition[indexor] += steps;
	    }
	    else
	      clog<<"UFGemPortescapAgent::_execMovement> home then step, steps empty?"<<endl;
	  } // athome or at nearhome
	} // finished first homing
	else if( homestephome && !homingF ) { // near or real home followed by stepping part done, now home
	  // put motor into final homing list, if not already there
	  _HomingFinal[indexor] = _DesPosition[indexor] = 0.0;
          string home2 = _HomeHome[indexor];
          if( home2 != "false" && home2 != "" ) {
	    // since home firmware uses initial velocity, insure it is set here
	    size_t hvpos = home2.rfind("F");
	    string hvel = "100";
	    if( hvpos != string::npos ) {
	      hvel = home2.substr(1+hvpos);
	      char* cs = (char*) hvel.c_str();
	      while( *cs == ' ' ) ++cs; // eliminate whites
	      hvel = cs;
	      hvpos = hvel.find(" ");
	      if( hvpos != string::npos )
		hvel = hvel.substr(0, hvpos);
	    }
	    else {
	      clog<<"UFGemPortescapAgent::_execMovement> home2 lacks velocity? "<<home2<<"use "<<hvel<<endl;
	    }
	    string setvel = indexor + 'I' + hvel;
	    if( _verbose )
	      clog<<"UFGemPortescapAgent::_execMovement> final home/initial velocity: "<<setvel<<endl;
	    _config->_devIO->submit(setvel, reply, _flush);
	    if( _verbose )
	      clog<<"UFGemPortescapAgent::_execMovement> final home: "<<home2<<endl;
	    _config->_devIO->submit(home2, reply, _flush);
            _HomeThenStep[indexor] = "false"; // remove indexor from map
            _HomeStepHome[indexor] = "false"; // remove indexor from map
            _HomeHome[indexor] = "false"; // remove indexor from map
	  }
	  else
	    clog<<"UFGemPortescapAgent::_execMovement> home2 empty?"<<endl;
        }
	else { // (only/final) home or step done, send OK
          _HomeThenStep[indexor] = "false"; // remove indexor from map
          _HomeStepHome[indexor] = "false"; // remove indexor from map
          _HomeHome[indexor] = "false"; // remove indexor from map
	  // restore initial velocity setting 
	  string setvel = _InitVelCmd[indexor];
          if( _verbose ) {
            clog<<"UFGemPortescapAgent::_execMovement> home completed: "<<endl;
	    clog<<"UFGemPortescapAgent::_execMovement> reset initial velocity: "<<setvel<<endl;
	  }
	  _config->_devIO->submit(setvel, reply, _flush);
          if( _statRec != "" ) {
	    sendEpics(_statRec, "OK"); _sentOkOrErr[indexor] = true;
	  }
        }
      } // mstat == 0
      else if( pdiff <= 0.01 ) { // msta != 0, but not moving either!
	// mechanism is stuck?
        // send Error Message, abort homing and remove from homing list?
	errmsg += " Stuck?";
 	if( _statRec != "" ) { sendEpics(_statRec, errmsg); _sentOkOrErr[indexor] = true; }
	erase.push_back(indexor);
        if( homethenstep ) // remove from HomeThenStep
 	  _HomeThenStep[indexor] = "false";
        if( homestephome ) { // remove from HomeStepHome
 	  _HomeStepHome[indexor] = "false";
 	  _HomeHome[indexor] = "false";
	}
        clog<<"UFGemPortescapAgent::_execMovement> mechanism stuck?: "<<indexor<<endl;
      } // pdiff
    } // check for completion
    /*
    if( mstat == 0 || pdiff <= 0.01 ) { 
      if( !homethenstep && !homestephome && !nearhoming ) // step only
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else if( athome && !homethenstep && !homestephome ) // home only
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else if( athome && homingF ) // done homestephome
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      else ( !athome && homethenstep ) // done homestep
        if( _statRec != "" ) sendEpics(_statRec, "OK");
      if( _verbose )
        clog<<_statRec<<" == "<<_currentval<<endl;
    }
    */
  } while( ++i != moving.end() );

  // remove all motion completed indexors from given map (homing or homingFinal or motion)
  // this must be down outside of the loop, otherwise the stlmap gets confused.
  for( int i = 0; i < (int)erase.size(); ++i ) {
    if( _verbose )
      clog<<"_execMovement> remove from motion list: "<<erase[i]<<endl;
    moving.erase(erase[i]); 
  }

  return;
} // _execMovement

void UFGemPortescapAgent::_simMovement(map<string, double>& moving) {
  if( moving.empty() )
    return;

  vector< string > erase;
  string reply, qmotion = " ^ ", qsteps = " Z 0 "; // steps from home/origin
  map<string, double>::iterator i = moving.begin();
  do { 
    string indexor = i->first;
    clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor
	<<", current: "<<_CurPosition[indexor]<<", desired: "<<_DesPosition[indexor]<<endl;

    // this function is used for all simulated motion (stepping or homing,
    // or home-then-step, or home-step-home)
    bool stepping = !_Motion.empty();
    if( stepping ) { // is this indexor stepping?
      map<string, double>::iterator fnd = _Motion.find(indexor);
      if( fnd == _Motion.end() )
	stepping = false;
    }
    bool nearhoming = !_NearHoming.empty(); // (diff < 0.01);
    if( nearhoming ) { // is this indexor nearhoming?
      map<string, double>::iterator fnd = _NearHoming.find(indexor);
      if( fnd == _NearHoming.end() )
	nearhoming = false;
    }
    // it is possible to (initially) find indexor in _Homing and _HomingFinal
    // so we need to be carefull what is tested here... 
    bool homing = !_Homing.empty(); // (des > -0.01 && des < 0.01);
    bool homingF = !_HomingFinal.empty(); // (des > -0.01 && des < 0.01);
    if( homing ) { // check this indexor homing?
      map<string, double>::iterator fnd = _Homing.find(indexor);
      if( fnd == _Homing.end() ) // not this indexor
	homing = false;
      else
	homingF = false; // force this to false
    }
    if( homingF ) { // check this indexor homingFinal?
      map<string, double>::iterator fnd = _HomingFinal.find(indexor);
      if( fnd == _HomingFinal.end() ) // not this indexor
	homingF = false;
    }
    clog<<"UFGemPortescapAgent::_simMovement> indexor: "<<indexor
	<<", stepping: "<<stepping<<", homing: "<<homing<<", homingFinal: "<<homingF<<endl;

    _statRec = _Status[indexor];
    string errmsg = _ErrPosition[indexor];

    // move it part way between current position and desired:
    double val= 0.0;
    double steps = ::fabs(_DesPosition[indexor] - _CurPosition[indexor]) / _simSpeed;
    if( homing ) // go a bit faster
      steps = 2.0 * steps;
    else if( homingF ) // go slower
      steps = steps / _simSpeed;

    if( _DesPosition[indexor] > _CurPosition[indexor] ) 
      val = _CurPosition[indexor] + steps;
    else
      val = _CurPosition[indexor] - steps;

    strstream s;
    s << indexor << qsteps << val << ends;
    reply = s.str(); delete s.str(); // simulate indexor position query/reading reply
    if( _verbose )
      clog<<"UFGemPortescapAgent::_simMovement> steps reading: "<<reply<<endl;

    int pos = qsteps.length() + reply.find(qsteps);
    _currentval = reply.substr(pos);
    if( _statRec != "" ) 
      sendEpics(_statRec, _currentval);

    steps = atof(_currentval.c_str());
    double prev_steps = _CurPosition[indexor];
    double pdiff = ::fabs(steps - prev_steps);
    if( pdiff > 0.01 ) // reset current position   
      _CurPosition[indexor] = steps;

    bool athome = false;
    if( ::fabs(_CurPosition[indexor]) < 0.01 ) { // must be home 
      athome = true;
      // set the datum cnt for this indexor:
      int datumcnt = UFPortescapConfig::_DatumCnt[indexor];
      if( datumcnt < 0 )
	datumcnt = 1;
      else
	++datumcnt;
      UFPortescapConfig::_DatumCnt[indexor] = datumcnt;
    }

    double des = _DesPosition[indexor];
    double diff = ::fabs(steps - des);
    // since this function can be used for either motion/stepping or nearhoming
    // there can be some ambiguity here...
    size_t snpos = _HomeThenStep[indexor].find("-");
    size_t sppos = _HomeThenStep[indexor].find("+");
    bool homethenstep = false, homestephome = false;
    string stepcmd = "";
    if( snpos != string::npos || sppos != string::npos ) {
      homethenstep = true;
      stepcmd = _HomeThenStep[indexor];
    }
    else {
      snpos = _HomeStepHome[indexor].find("-");
      sppos = _HomeStepHome[indexor].find("+");
      if( snpos != string::npos || sppos != string::npos ) {
        homestephome = true;
        stepcmd = _HomeStepHome[indexor];
      }
    }

    // once current motion is complete, should it be followed by more motion?
    if( (diff <= 0.01 || pdiff <= 0.01) ) { // motion is complete, should it be followed by stepping or homing?
      val = _CurPosition[indexor] = _DesPosition[indexor];
      erase.push_back(indexor);
      strstream s;
      s << indexor << qsteps << val << ends;
      reply = s.str(); delete s.str(); // simulate final reading
      int pos = qsteps.length() + reply.find(qsteps);
      _currentval = reply.substr(pos);
      if( homing || nearhoming && (homethenstep || homestephome) ) { // step for HomeThenStep or HomeStepHome
	string ss; 
        double steps = 0.0;
        if( sppos != string::npos ) { // step forwards
	  ss = stepcmd.substr(1+sppos);
          steps += atof(ss.c_str());
        }
        if( snpos != string::npos ) { // setp backwards
	  ss = stepcmd.substr(1+snpos);
          steps -= atof(ss.c_str());
        }
	// put motor into motion list
        _Motion[indexor] = _DesPosition[indexor] += steps;
      }
      else if( stepping && homestephome  ) { // put motor into final homing list, if not already there
        _HomingFinal[indexor] = _DesPosition[indexor] = 0.0;
      }
      else { // (final or only) motion done, send OK
        if( _verbose )
          clog<<"UFGemPortescapAgent::_simMovement> all motion completed, final reading: "<<reply<<endl;
        _HomeThenStep[indexor] = "false"; // remove indexor from homethestep
        _HomeStepHome[indexor] = "false"; // remove indexor from homestephome
        if( _statRec != "" ) {
	  sendEpics(_statRec, _currentval);
          sendEpics(_statRec, "OK"); _sentOkOrErr[indexor] = true;
	}
      }
    }
  } while( ++i != moving.end() );

  // remove all motion completed indexors from given map (motion, homing, or homingF)
  for( int i = 0; i < (int)erase.size(); ++i ) {
    if( _verbose )
      clog<<"_simMovement> remove from motion list: "<<erase[i]<<endl;
    moving.erase(erase[i]);
  }

  return;
} // _simMovement

void UFGemPortescapAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0 && _Motion.size() == 0 )
    sleep(_Update); // no connections, no queued reqs., go to sleep
  else if( (_Motion.size() > 0 || _Homing.size() || _HomingFinal.size() > 0) && !_sim )
    return; // don't hibernate, must monitor motion as best as can
  else
    UFSocket::waitOnAll(_Update); // wait for client req. or for next update tick
}

int UFGemPortescapAgent::_reqHomeThenMore(UFDeviceAgent::CmdInfo* act) {
  map<string, int> home, nearhome;
  map<string, int> step;
  map<string, string> stepcmd;
  map<string, string> home2cmd;
  string cmdname, cmdimpl, motor, car;
  for( int i = 0; i < (int) UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    home[motor] = nearhome[motor] = step[motor] = 0;
    stepcmd[motor] = home2cmd[motor] = "";
  }

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    cmdname = act->cmd_name[i];
    cmdimpl = act->cmd_impl[i];
     if( _verbose ) 
	clog<<"_reqHomeThenMore> "<<cmdname<<", "<<cmdimpl<<endl;
    int cpos = cmdname.find("car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("Car");
    if( cpos == (int)string::npos ) 
      cpos = cmdname.find("CAR");
 
    motor = ""; // which motor is this?
    if( cpos != (int)string::npos ) {
      car = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0 of Car
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
	char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
	motor = indexor;
      }
      continue;
    }
    if( motor == "" ) { // otherwise try first char of cmdimpl:
      char* c = (char*)cmdimpl.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
      char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
      motor = indexor;
    }
    if( cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0 ) { // home req.
      int hcnt = 1 + home[motor];
      home[motor] = hcnt; //
      if( hcnt > 1 ) home2cmd[motor] = cmdimpl;
    }
    else if( cmdimpl.rfind("N") != string::npos && cmdimpl.rfind("N") != 0 ) { // near-home req.
      nearhome[motor] = 1; // any value > 0 will do
      string nhs = cmdimpl.substr(1+cmdimpl.rfind("N"));
      double nh = atof(nhs.c_str());
      if( _verbose )
	clog<<"NearHome req. to: "<<nh<<" ("<<cmdimpl<<")"<<endl;
      if( nh < 1.0 && nh >= 0.0 )
	nh = 1.0;
      if( nh > -1.0 && nh <= 0.0 )
	nh = -1.0;
      double diff = ::fabs(_NearHome - nh);
      if( diff > 0.01 )
	_NearHome = nh;
      if( _verbose )
	clog<<"_NearHome set to: "<<_NearHome<<endl;
    }
    else if( cmdimpl.find("+") != string::npos || cmdimpl.find("-") != string::npos ) { // step req
      step[motor] = 1; // any value > 0 will do
      stepcmd[motor] = cmdimpl;
    }
  } 

  int cnt= 0;
  for( int i = 0; i < (int) UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    if( (home[motor] > 0 || nearhome[motor] > 0 ) && step[motor] > 0 ) {
      ++cnt; 
      if( home[motor] == 1 || nearhome[motor] > 0 ) { // home-step for real or virtual home then step
        _HomeThenStep[motor] = stepcmd[motor]; // steps raw cmd
        if( _verbose )
          clog<<"_reqHomeThenSMore> Motor: "<<motor<<" => "<<_HomeThenStep[motor]<<endl;
      }
      else if( home[motor] > 1 ) { // home-step-home only for real home
        if( stepcmd[motor] != "" )
	  _HomeStepHome[motor] = stepcmd[motor]; // steps raw cmd
        if( home2cmd[motor] != "" )
	  _HomeHome[motor] = home2cmd[motor]; // steps raw cmd
        if( _verbose )
	  clog<<"_reqHomeThenMore> (then home finally) Motor: "<<motor<<" => "<<_HomeStepHome[motor]<<", "<<_HomeHome[motor]<<endl;
      }
    }
  }
  return cnt;
} // _reqHomeThenMore

// the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// to portescap indexor.
// this version must always return null to (epics) clients, and
// instead send the command completion status to designated CARs,
// if no CAR is desginated, an error /alarm condition should be indicated
// new signature for action:
int  UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  delete repv; repv = 0;
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFGemPortescapAgent::action(UFDeviceAgent::CmdInfo* act) {
  int stat = 0;
  bool reply_client= true;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    clog<<"UFGemPortescapAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemPortescapAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemPortescapAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }

  // check the entire bundle for a home-then-step, or home-step-home sequence, and make a
  // note of which motors, if any, were specified
  int nhm = _reqHomeThenMore(act);

  // epics will send a CmdInfo bundle for just one motor indexor at a time...
  // this bit of logic will fail if bundle contains commands for more than one indexor! 
  string motor, car, errmsg;
  bool motion= false, homing= false, homingF= false, nearhoming= false;

  map< string, bool > submittedhome; // if this bundle has compound home requests for more than one index 
  for( int i = 0; i < (int)UFPortescapConfig::_Indexors.size(); ++i ) {
    motor = UFPortescapConfig::_Indexors[i];
    submittedhome[motor] = false;
  }

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    motor = "";
    bool sim = _sim; // command-line option can force all actions to be simulated
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    //if( _verbose ) {
      clog<<"UFGemPortescapAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemPortescapAgent::action> cmdimpl: "<<cmdimpl<<endl;
    //}

    // in principle this could be a one-time init from the cc:configG
    // however, support for multiple epics DBs is possible by simply
    // resetting the CAR _Status name for each request...
    int cpos = cmdname.find("car");
    if( cpos == (int)string::npos ) cpos = cmdname.find("Car");
    if( cpos == (int)string::npos ) cpos = cmdname.find("CAR");
    if( cpos != (int)string::npos ) {
      reply_client= false; // only epics clients send car keyword, no socket reply is expected by epics
      // note that _execMoition and _simMotion may send OK and set this to true...
      car = cmdimpl;
      if( cpos > 0 ) { // assume motor indexor name is at pos 0
        char* c = (char*)cmdname.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
	char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
	motor = indexor;
      }	  
      if( _verbose ) clog<<"UFGemPortescapAgent::action> CAR: "<<car<<endl;
      continue;
    }

    if( reply_client && cmdname.find("stat") != string::npos || 
	cmdname.find("Stat") != string::npos || cmdname.rfind("STAT") != string::npos  ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  )
        return _config->statusFITS(this); 
      else
        return _config->status(this); 
    }

    int errIdx = cmdname.find("!!");
    if( errIdx != (int)string::npos ) 
      errmsg = cmdname.substr(2+errIdx);
    // now check for raw/real or sim commands
    int rpos = cmdname.find("raw");
    if( rpos == (int)string::npos ) rpos = cmdname.find("Raw");
    if( rpos == (int)string::npos ) rpos = cmdname.find("RAW");
    int spos = cmdname.find("sim");
    if( spos == (int)string::npos ) spos = cmdname.find("Sim");
    if( spos == (int)string::npos ) spos = cmdname.find("SIM");
    if( rpos == (int)string::npos && spos == (int)string::npos  && cpos == (int)string::npos ) {
      // quit
      errmsg += "Badly formatted request? " + cmdname + " " + cmdimpl;
      if( _verbose )
	clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
      //if( car != "" ) setCARError(car, &errmsg); // set error
      if( car != "" ) { sendEpics(car, errmsg); _sentOkOrErr[motor] = true; }
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    if( motor == "" ) { // try first char of cmdimpl:
      char* c = (char*)cmdimpl.c_str(); while( *c == ' ' || *c == '\t' ) ++c;
      char indexor[2]; indexor[0] = *c; indexor[1] = '\0';
      motor = indexor;
    }
      
    int nr = _config->validCmd(cmdimpl);
    if( nr < 0 ) { // invalid cmd
      errmsg += "?Invalid Portescap req? " + cmdname + " " + cmdimpl;
      if( _verbose )
	clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
      //if( car != "" ) setCARError(car, &errmsg); // set error
      if( car != "" ) { sendEpics(car, errmsg); _sentOkOrErr[motor] = true; }
      act->cmd_reply.push_back(errmsg);
      if( !reply_client )
        return (UFStrings*) 0;
      return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }

    // check if status genSub channel name has been established
    // this should be done by newClient, but allow it
    // to be reset by Car directive
    if( car != "" ) {
      clog<<motor<<" car: "<<car<<endl;
      _Status[motor] = car;
    }
    else {
      clog<<"No CAR? motor: "<<motor<<endl;
    }
 
    act->time_submitted = currentTime();
    _active = act;
    size_t sppos = cmdimpl.find("+");
    size_t snpos = cmdimpl.find("-");

    if( cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0 ) {
      // this is a set of initial velocity, make a record of it
      _InitVelCmd[motor] = cmdimpl;
    }
 
    // this rejection logic implicitly assumes this bundle is devoted to just 1 indexor...
    if( cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0 ) {
      snpos = sppos = string::npos;
      // this is a home req, which is OK if currently homing, but not OK if stepping
      map<string, double>::iterator h = _Motion.find(motor);
      if( h != _Motion.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject home req., still stepping: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      _DesPosition[motor] = 0.0;
/*
      double hdif = ::fabs(_CurPosition[motor] - _DesPosition[motor]);
      // if this motor has been datumed or origined at least once and it is currently
      // in the datum position, treat this as an OK no-op, else reject it...
      if( hdif < 0.01 && UFPortescapConfig::_DatumCnt[motor] != 0 ) {
        clog<<"UFGemPortescapAgent::action> "<<motor<<" is at datum, assume No-Op..."<<endl;
        act->cmd_reply.push_back("No-Op Home");
        if( car != "" ) sendEpics(car, "OK");
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
*/	
      if( !homing ) { // this logic assumes the compound motion cmd bundles are only sent for single motors...
        homing = true;
        _Homing.insert(map<string, double>::value_type(motor, 0.0));
      }
      // _HomingFinal must be set later (by _exec or _sim Movement?
      else if( !homingF) {
        homingF = true;
        //  _HomingFinal.insert(map<string, double>::value_type(motor, 0.0));
      }
    }
    if( cmdimpl.rfind("N") != string::npos && cmdimpl.rfind("N") != 0 ) {
      snpos = sppos = string::npos;
     // this is a near-home req, which is OK if currently near-homing? but not OK if stepping
      map<string, double>::iterator h = _Motion.find(motor);
      if( h != _Motion.end() ) { // motor is already/still stepping, reject this
        errmsg += " Reject near-home req. motor still stepping: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      _NearHoming.insert(map<string, double>::value_type(motor, _NearHome));
      _DesPosition[motor] = _NearHome;
      nearhoming = true;
    }

    string reply= cmdimpl;
    if( _verbose ) {
      clog<<"UFGemPortescapAgent::action> _HomeThenStep for "<<motor<<" = "<<_HomeThenStep[motor]<<endl;
      clog<<"UFGemPortescapAgent::action> _HomeStepHome for "<<motor<<" = "<<_HomeStepHome[motor]<<endl;
      clog<<"UFGemPortescapAgent::action> _HomeHome for "<<motor<<" = "<<_HomeHome[motor]<<endl;
    }
    bool homethenstep = false, homestephome = false;
    if( nhm > 0 ) {
      homethenstep = (_HomeThenStep[motor] != "false");
      homestephome = (_HomeStepHome[motor] != "false");
    }
    if( (homethenstep || homestephome) && (sppos != string::npos || snpos!= string::npos) ) {
      // step request following the home request is handled by the ancillary function
      //reply += " Ok";
      _sentOkOrErr[motor] = false;  // epics clients need "OK" sent to CAR upon succesfull command completion
      act->cmd_reply.push_back(reply);
      continue;
    }
    if( !homethenstep && !homestephome && (sppos != string::npos || snpos!= string::npos) ) {
    //if( !homethenstep && !homestephome || (sppos != string::npos || snpos!= string::npos) ) {
      // this is a step only or home only req. bundle?
      map<string, double>::iterator s = _Motion.find(motor);
      if( s != _Motion.end() ) { // motor is already/still in (stepping) motion, reject this
        errmsg += " Reject step req. motor in motion: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator h = _Homing.find(motor);
      if( h != _Homing.end() ) { // motor is already/still in (homing) motion, reject this
        errmsg += " Reject step req. motor homing: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      map<string, double>::iterator nh = _NearHoming.find(motor);
      if( nh != _NearHoming.end() ) { // motor is already/still in (homing) motion, reject this
        errmsg += " Reject step req. motor near-homing: " + cmdimpl;
        clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        act->cmd_reply.push_back(errmsg);
        // why not do this?
        if( car != "" ) sendEpics(car, errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
      }
      string ss;
      double steps = 0.0;
      if( sppos != string::npos ) { // step forwards
	ss = cmdimpl.substr(1+sppos);
        steps += atof(ss.c_str());
      }
      if( snpos != string::npos ) { // setp backwards
	ss = cmdimpl.substr(1+snpos);
        steps -= atof(ss.c_str());
      }
      // put motor into motion list and clear its Ok message
      _Motion.insert(map<string, double>::value_type(motor, steps));
      _DesPosition[motor] = _DesPosition[motor] + steps;
      motion = true;
      _sentOkOrErr[motor] = false;  // epics clients need "OK" sent to CAR upon succesfull command completion
    }
    // finally, no motion parameter (re)set should be submitted
    // at this point or the portescap rs422 bus will become locked (busy).
    // the only possible exception to this would be a home request when we
    // are already busy homing, in which case we could treat it as a no-op.
    // this really should be a function in the Config class:
    /*
    bool busbusy = ( motion || homing || nearhoming || homethenstep || homestephome );
    if( busbusy ) {
      if( (cmdimpl.rfind("B") != string::npos && cmdimpl.rfind("B") != 0) ||
	  (cmdimpl.rfind("E") != string::npos && cmdimpl.rfind("E") != 0) ||
          (cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0) ||
	  (cmdimpl.rfind("K") != string::npos && cmdimpl.rfind("K") != 0) ||
	  (cmdimpl.rfind("V") != string::npos && cmdimpl.rfind("V") != 0) ||
	  (cmdimpl.rfind("Y") != string::npos && cmdimpl.rfind("Y") != 0) )
	// assume this cmd bundle is bogus and reject thw whole thing
        errmsg += "RS422 Bus busy (Portescap), reject: " + cmdname + " " + cmdimpl;
        if( _verbose )
	  clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        //if( car != "" ) setCARError(car, &errmsg); // set error
        if( car != "" ) sendEpics(car, errmsg);
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
    }
    */
    map<string, string>::iterator ep = _ErrPosition.find(motor);
    if( ep != _ErrPosition.end() ) // replace errmsg
      _ErrPosition.erase(ep);
    _ErrPosition.insert(map<string, string>::value_type(motor, errmsg));

    map<string, double>::iterator mm = _Motion.find(motor);
    map<string, double>::iterator hm = _Homing.find(motor);
    map<string, double>::iterator hmF = _HomingFinal.find(motor);
    map<string, double>::iterator nrhm = _NearHoming.find(motor);
    if( spos != (int)string::npos || _sim ) { // sim request
      sim = true;
      bool abort = false, origin= false;
      reply = "sim: " + cmdimpl;
      // simulate position & motion queries:
      strstream s;
      if( cmdimpl.find("Z") != string::npos ) {
	// return current position
	s << cmdimpl << " " << _CurPosition[motor] <<ends;
	reply = s.str(); delete s.str();
        strstream sp;
        sp<<_CurPosition[motor]<<ends; 
        if( car != "" ) {
	  sendEpics(car, sp.str());
	  sendEpics(car,"OK");
	  _sentOkOrErr[motor] = true;
	}
	delete sp.str();
      }
      else if( cmdimpl.find("^") != string::npos ) {
	// return 0 if stationary, otherwise current position
        if( mm != _Motion.end() || nrhm != _NearHoming.end() ||
	    hm != _Homing.end() || hmF != _HomingFinal.end() )
	  // motor is already/still in motion
	  s << cmdimpl << " " << _CurPosition[motor] <<ends;
	else
	  s << cmdimpl << " " << 0 <<ends;
	reply = s.str(); delete s.str();
      }
      else if( ( cmdimpl.find("O") != string::npos && cmdimpl.find("O") > 0) ||
	       (cmdimpl.find("o") != string::npos && cmdimpl.find("o") > 0) ) {
	// clear the step counts (origin)
        origin = true;
        if( mm != _Motion.end() || nrhm != _NearHoming.end() ||
	    hm != _Homing.end() || hmF != _HomingFinal.end() )
	  // motor is still in motion
	  s << cmdimpl << " " << _CurPosition[motor] <<ends;
	else {
          _CurPosition[motor] = 0;
	  s << cmdimpl << " " << 0 <<ends;
        }
	reply = s.str(); delete s.str();
	// and return current position
        strstream sp;
        sp<<_CurPosition[motor]<<ends; 
        if( car != "" ) sendEpics(car, sp.str()); delete sp.str();
      }
      else if( cmdimpl.find("@") != string::npos ) { // stop/abort command:
        abort = true;
	s << cmdimpl << " (abort) " << _CurPosition[motor] <<ends;
        // clear HomeThenStep
 	_HomeThenStep[motor] = "false";
        // clear HomeStepHome
 	_HomeStepHome[motor] = "false";
 	_HomeHome[motor] = "false";
        if( !_Motion.empty() && mm != _Motion.end() ) {// motor is still in motion
	  _Motion.erase(mm); // remove it from motion list
	}
        if( !_Homing.empty() && hm != _Homing.end() ) {// motor is still in motion
	  _Homing.erase(hm); // remove it from homing list
	}
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) {// motor is still in motion
	  _HomingFinal.erase(hmF); // remove it from homing list
	}
        if( !_NearHoming.empty() && nrhm != _NearHoming.end() ) {// motor is still in motion
	  _NearHoming.erase(nrhm); // remove it from homing list
	}
	reply = s.str(); delete s.str();
      }
      if( _verbose )
	clog<<"UFGemPortescapAgent::action> Sim: "<<reply<<endl;
    } // end sim request

    // if not sim. use the annex/iocomm port to submit the action command and get reply:
    // (should be calling Config functions here)
    // if a command bundle includes a home followed by a step for a given
    // motor, only perform the home. must defer submitting the step req.
    // until home completes
    if( !sim && _config->_devIO != 0 ) {
      bool abort= false, aborthome= false, origin= false;
      if( cmdimpl.find("@") != string::npos ) {
	abort = true;
        if( !_Homing.empty() && hm != _Homing.end() ) 
	  aborthome = true;
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) 
	  aborthome = true;
      }
      else if( ( cmdimpl.find("O") != string::npos && cmdimpl.find("O") > 0) ||
	       (cmdimpl.find("o") != string::npos && cmdimpl.find("o") > 0) ) {
	// clear the step counts (origin)
        origin = true;
      }
      // if cmdimpl is a near-home replace pseudo command with the appropriate +/- request
      bool nearH = _nearHome(motor, cmdimpl);
      bool realH = (cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0);
      bool submittedH = submittedhome[motor];
      clog<<"nearH, submittedH for "<<motor<<": "<<nearH<<", "<<submittedH << endl;
      if( submittedH && (nearH || (cmdimpl.rfind("F") != string::npos && cmdimpl.rfind("F") != 0) ) ) {
	if( _verbose )
	  clog<<"already submitted home or near/not-home: "<<motor << endl;
	continue; // already submitted (first) home req. for this motor
      }
      if( _verbose ) {
	clog<<"UFGemPortescapAgent::action> submit: "<<cmdimpl<<endl;
	//clog<<"UFGemPortescapAgent::action> submit: (nearHome? : ) "<<nearH<<" ), "<<cmdimpl<<endl;
      }
      if( aborthome ) { // no reply expected
        if( _verbose )
	  clog<<"UFGemPortescapAgent::action> aborthome, no device reply expected for escape."<<endl;
        stat = _config->_devIO->escape(motor, 0.3); 
        if( _verbose )
	  clog<<"UFGemPortescapAgent::action> aborthome, stat: "<<stat<<endl;
	reply = cmdimpl;
      }
      else { // reply expected
	if( realH ) { // reset initial velocity according to requested home speed to make them congruent (while homing)
	  // since home firmware uses initial velocity, insure it is set here
	  size_t hvpos = cmdimpl.rfind("F");
	  string hvel = "100";
	  if( hvpos != string::npos ) {
	    hvel = cmdimpl.substr(1+hvpos);
	    char* cs = (char*) hvel.c_str();
	    while( *cs == ' ' ) ++cs; // eliminate whites
	    hvel = cs;
	    hvpos = hvel.find(" ");
	    if( hvpos != string::npos )
	      hvel = hvel.substr(0, hvpos);
	  }
	  else {
	    clog<<"UFGemPortescapAgent::action> homelacks velocity? "<<cmdimpl<<"use "<<hvel<<endl;
	  }
	  string setvel = motor + 'I' + hvel;
	  if( _verbose )
	    clog<<"UFGemPortescapAgent::action> set home/initial velocity: "<<setvel<<endl;
	  _config->_devIO->submit(setvel, reply, _flush);
	} // realH homing resets init. vel.
 
        // if this motor has never been datumed or origined its current position
	// may be ambiguous, so we must perform an inital step in the oposite direction
        // of the first home commad to deal with the pathelogical behavior of
        // of some indexors when asked to home when already at home
        bool sendcmd = UFPortescapConfig::newSettingCmd(motor, cmdimpl);
        // send command to indexor and get reply
        if( sendcmd ) {  // send command to indexor and get reply
/*
          string jogcmd;
          double curpos = _CurPosition[motor];
          UFPortescapConfig::jogMotor(motor, cmdimpl, curpos, jogcmd);
          if( !jogcmd.empty() ) { // first jog the indexor
            stat = _config->_devIO->submit(jogcmd, reply, _flush); 
            if( _verbose )
	      clog<<"UFGemPortescapAgent::action> device reply: "<<reply<<endl;
	  }
*/
          stat = _config->_devIO->submit(cmdimpl, reply, _flush); 
          if( _verbose )
	    clog<<"UFGemPortescapAgent::action> device reply: "<<reply<<endl;
	}
        else {
	  clog<<"UFGemPortescapAgent::action> (existing setting) cmd does not need to be sent: "<<cmdimpl<<endl;
	}
        if( origin && stat >= 0 ) { // this was an origin command that presumably suceeded
	  // clear and return current position
	  _CurPosition[motor] = 0;
          strstream sp;
          sp<<_CurPosition[motor]<<ends; 
          if( car != "" ) sendEpics(car, sp.str()); delete sp.str();
	}
      }
      if( nearH || realH ) {
        submittedhome[motor] = true;
	if( _verbose )
         clog<<"set submittedH for "<<motor<<": nearH= "<<nearH<< endl;
	//break; // submitted near home or home, ancillary takes care of all other request in this bundle?
	continue; // submitted near home or home, ancillary takes care of all other request in this bundle?
      }
      if( _DevHistory.size() >= _DevHistSz )
	_DevHistory.pop_front();

      _DevHistory.push_back(reply);

      if( stat < 0 ) {
	errmsg += "Device communication failed";
        //if( car != "" ) setCARError(car, &errmsg); // set error
        if( car != "" ) { sendEpics(car, errmsg); _sentOkOrErr[motor] = true; }
        if( _verbose )
          clog<<"UFGemPortescapAgent::action> "<<errmsg<<endl;
        if( !_Motion.empty() && mm != _Motion.end() ) // remove motor from motion list
          _Motion.erase(mm);
        act->cmd_reply.push_back(errmsg);
        if( !reply_client )
          return (UFStrings*) 0;
        return new UFStrings(act->clientinfo, act->cmd_reply); // freed by the calling func...
      } // end failed dev. comm.
      if( abort || aborthome ) {
        // motion was aborted
        // clear  HomeThenStep
 	_HomeThenStep[motor] = "false";
        // clear  HomeStepHome
 	_HomeStepHome[motor] = "false";
 	_HomeHome[motor] = "false";
        if( !_Motion.empty() && mm != _Motion.end() ) 
          _Motion.erase(mm); // remove from motion list
        if( !_Homing.empty() && hm != _Homing.end() ) 
	  _Homing.erase(hm); // remove from motion list
        if( !_HomingFinal.empty() && hmF != _HomingFinal.end() ) 
	  _HomingFinal.erase(hmF); // remove from motion list
        if( !_NearHoming.empty() && nrhm != _NearHoming.end() ) 
	  _NearHoming.erase(nrhm); // remove from motion list
      }
      if( stat > 0 &&  car != "" ) { // device replied:
        string s = reply.substr(cmdimpl.length());
        // currently only origin or abort or position request by epics client requires immediate car:
	bool reqpos = (reply.find("Z") != string::npos || reply.find("z") != string::npos);
        if( origin || abort || (reqpos  && s.length() > 0) ) {
	  sendEpics(car, s); sendEpics(car, "OK"); _sentOkOrErr[motor] = true;
	}
      } // reply sent back to epics
    } // non sim.
    act->cmd_reply.push_back(reply);
  } // end for loop of cmd bundle

  // if the client is an epics db (car != "" ) send ok or error
  // completion of all req. in this bundle.
  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    if( _verbose )
      clog<<"UFGemPortescapAgent::action> one or more cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    //if( car != "" ) sendEpicsCAR(car, 3, &errmsg);p
    if( car != "" ) sendEpics(car, errmsg); 
  }
  else {
    //if( _verbose ) {
	clog<<"UFGemPortescapAgent::action> cmd. complete, set CAR to idle, OkOrErr flag: "<<_sentOkOrErr[motor]<<endl;
	clog<<"UFGemPortescapAgent::action> motor: "<<motor<<", CAR: "<<car<<endl;
    //} 
    act->status_cmd = "succeeded";
    //if( car != "" ) setCARIdle(car);
    //if( car != "" && !motion && !homing && !homingF && !nearhoming ) sendEpics(car, "OK");
    if( car != "" && !_sentOkOrErr[motor] && !motion && !homing && !homingF && !nearhoming ) {
      // presumable this OK is only being sent in response to a non-motion request (parameter reset, abort, etc).
      sendEpics(car, "OK"); // motion command OKs handled by _exec/_simMovement
      _sentOkOrErr[motor]= true;
    }
  }
 
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client )
    return (UFStrings*) 0;

  return new UFStrings(act->clientinfo, act->cmd_reply); // this should be freed by the calling func...
} // action


bool UFGemPortescapAgent::_nearHome(const string& motor, string& cmdimpl) {
  if( _verbose )
    clog<<"_nearHome> NearHome= "<<_NearHome<<", cmdimpl=> "<<cmdimpl<<endl;

  if( cmdimpl.rfind("N") == string::npos || cmdimpl.rfind("N") == 0 )
    return false;

  double diff = ::fabs(_CurPosition[motor] - _NearHome);
  strstream s;
  if( _CurPosition[motor] >= _NearHome )
    s<<motor<<"-"<<(int)diff<<ends;
  else
    s<<motor<<"+"<<(int)diff<<ends;

  cmdimpl = s.str(); delete s.str();
  //if( _verbose )
    clog<<"_nearHome> motor: "<<motor<<", _curPos= "<<_CurPosition[motor]<<", NearHome= "<<_NearHome<<", cmdimpl=> "<<cmdimpl<<endl;

  return true;
}

int UFGemPortescapAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFGemPortescapAgent
