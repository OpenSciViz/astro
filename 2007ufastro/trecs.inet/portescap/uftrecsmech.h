#if !defined(__UFTRECSMECH_H__)
#define __UFTRECSMECH_H__
/* for use by C & C++ to describe mechanism names & positions */
/* assume Epics-like string length limits */

#include "stdlib.h"
#include "string.h"
#include "values.h"
#include "math.h"

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

static const int __UFEPICSSLEN_ = 40; 
static const int __UFMAXPOSTRECS_ = 40; 
/* static const int __UFMAXINDXTRECS_ = 10; */
static const int __UFMAXINDXTRECS_ = 9; /* eliminated the cold clamp */
static const char* _nonexist = "NonExistent";

typedef struct {
  int posCnt; /* number of (<= max) positions (including home) in use */
  char *name; /* name[__UFEPICSSLEN_]; */ /* Gemini keyord for mech. named pos */
  char *posname; /* posname[__UFEPICSSLEN_]; */ /* Gemini keyward for pos name step cnt */
  char **positions; /* positions[__UFMAXPOSTRECS_][__UFEPICSSLEN_]; */
  float *steps; /* steps[__UFMAXPOSTRECS_]; + steps from home */
} UFTReCSMechPos;

typedef struct __UFTReCSMech_ {
  int mechCnt;
  char **indexor; /* indexor[__UFMAXINDXTRECS_][__UFEPICSSLEN_]; */
  UFTReCSMechPos *mech; /* mech[__UFMAXINDXTRECS_]; */
} UFTReCSMech;

/* non-reentrant non-thread-safe: */
static UFTReCSMech* getTheUFTReCSMech() {
  /*  static char* _indexors[]= { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" }; */
  static char* _indexors[]= { "A", "B", "C", "D", "E", "F", "G", "H", "I" };
  static UFTReCSMech _TheTReCSMech;
  static UFTReCSMechPos* _mech= 0; /* compile time init. */

  // force names to match Gemini DHS conf. (unless truncation accomplishes same effect)...
  // note Gemini expects separate keywords for the position name and set cnt
  static char* _nameSW = "Sector"; // "SectorWheel"; 
  static char* _posSW = "SectPos";
  static char* _positionsSW[] = { "Poly_115", "Black_Plate", "Open", "Poly_105", "Datum" }; 
  static float   _stepSW[] = { 268, 556, 841, 1126, 0 };
  static UFTReCSMechPos sw;

  static char* _nameWC = "Window"; // "WindowChanger"; 
  static char* _posWC = "WindPos"; 
  static char* _positionsWC[] = { "Block", "KRS-5", "ZnSe", "KBr", "KBrC", "Datum" }; 
  static float   _stepWC[] = { 0, 1289, 2578, 3862, 5150, 0 };
  static UFTReCSMechPos wc;

  static char* _nameAW = "Aperture"; // "ApertureWheel"
  static char* _posAW = "AperPos"; 
  static char* _positionsAW[] = { "Grid_Mask", "Matched", "Oversized", "Window_Imager", "Spot_Mask" , "Datum" }; 
  static float   _stepAW[] = { 21, 167, 313, 460, 607, 0 };
  static UFTReCSMechPos aw;

  static char* _nameFW1 = "Filter1"; // "Filter1Wheel";
  static char* _posFW1 = "Filt1Pos"; 
  static char* _positionsFW1[] = { "Open", "Qw-20.8um", "Si1-7.9um", "PAH1-8.6um", "Si2-8.8um",
				   "Block", "Si3-9.7um", "PAH2-11.3um", "Si4-10.4um", "ArIII-9.0um",
				   "Si5-11.7um", "Si6-12.3um", "Align-Spot", "Datum" }; 
  static float _stepFW1[] = { -72, 15, 102, 189, 276, 363, 451, 538, 625, 712, 799, 886, 973, 0 };
  static UFTReCSMechPos fw1;

  static char* _nameLW = "Lyot"; // "LyotWheel"; 
  static char* _posLW = "LyotPos"; 
  static char* _positionsLW[] = { "Grid_Mask", "Spot_Mask", "Ciardi", "Open", "Quakham_Mask", "Polystyrene",
                                  "Circ-2", "Circ-4", "Circ+2", "Circ+4", "Circ+6",
				  "Circ+8", "Block", "Datum" }; 
  static float   _stepLW[] = { 809, 896, 982, 1069, 1156, 1243, 1331, 1418, 1506, 1592, 1680, 1767, 1849, 0 };
  static UFTReCSMechPos lw;

  static char* _nameFW2 = "Filter2"; // "Filter2Wheel"; 
  static char* _posFW2 = "Filt2Pos"; 
  static char* _positionsFW2[] = { "Open", "K", "L", "M", "NeII-12.8um", "NeII_ref2-13.1um", "SIV-10.5um", "Qs-18.3um",
                                   "Qone-17.8um", "Ql-24.5um", "N", "Block", "Align-Spot", "Datum" }; 
  static float _stepFW2[] =  { 530, 1574, 1487, 1400, 1313, 1226, 1139, 1052, 965, 878, 791, 704, 617, 0 };
  static UFTReCSMechPos fw2;

  static char* _namePIW = "PupilIma"; // "PupilImagingWheel";
  static char* _posPIW = "PupilPos";
  static char* _positionsPIW[] = { "Open-1",  "Pupil_Image", "Open-2", "Open-3", "Datum" }; 
  static float   _stepPIW[] = { 35, 249, 462, 675, 0 };
  static UFTReCSMechPos piw;

  static char* _nameSLW = "Slit"; // "SlitWheel"; 
  static char* _posSLW = "SlitPos";
  static char* _positionsSLW[] = { "Open", "1.32\"", "0.72\"", "0.66\"", "0.36\"", "0.31\"", 
				   "0.26\"", "0.21\"", "Datum" }; 
  static float _stepSLW[] = { -2316, 195, 2661, 5189, 7662, 10190, 12634, 15104, 0 };
  static UFTReCSMechPos slw;

  static char* _nameGW = "Grating"; // "GratingWheel"; 
  static char* _posGW = "GratPos";
  static char* _positionsGW[] = { "Mirror", "LowRes-10", "LR_Ref_Mirror", "LowRes-20", "HighRes-10", "HR_Ref_Mirror", "Datum" }; 
  static float   _stepGW[] = { -1006, 1216, 2392, 3519, 5716, 6972, 0 };
  static UFTReCSMechPos gw;
  /*
  static char* _nameCC = "ColdClamp";
  static char* _posCC = "ColdPos";
  static char* _positionsCC[] = { "Open", "Clamped", "Park" }; 
  static float   _stepCC[] = { 0, 100, 0 };
  static UFTReCSMechPos cc;
  */

  if( _mech != 0 ) {
    return &_TheTReCSMech;
  }  
  else {
    _mech = (UFTReCSMechPos*) calloc(__UFMAXINDXTRECS_, sizeof(UFTReCSMechPos));
  }

  sw.name = _nameSW;
  sw.posname = _posSW;
  sw.positions = _positionsSW;
  sw.steps = _stepSW;
  sw.posCnt = _ArrayLen__(_stepSW);

  wc.name = _nameWC;
  wc.posname = _posWC;
  wc.positions = _positionsWC;
  wc.steps = _stepWC;
  wc.posCnt = _ArrayLen__(_stepWC);

  aw.name = _nameAW;
  aw.posname = _posAW;
  aw.positions = _positionsAW;
  aw.steps = _stepAW;
  aw.posCnt = _ArrayLen__(_stepAW);

  fw1.name = _nameFW1;
  fw1.posname = _posFW1;
  fw1.positions = _positionsFW1;
  fw1.steps = _stepFW1;
  fw1.posCnt = _ArrayLen__(_stepFW1);

  lw.name = _nameLW;
  lw.posname = _posLW;
  lw.positions = _positionsLW;
  lw.steps = _stepLW;
  lw.posCnt = _ArrayLen__(_stepLW);

  fw2.name = _nameFW2;
  fw2.posname = _posFW2;
  fw2.positions = _positionsFW2;
  fw2.steps = _stepFW2;
  fw2.posCnt = _ArrayLen__(_stepFW2);

  piw.name = _namePIW;
  piw.posname = _posPIW;
  piw.positions = _positionsPIW;
  piw.steps = _stepPIW;
  piw.posCnt = _ArrayLen__(_stepPIW);

  slw.name = _nameSLW;
  slw.posname = _posSLW;
  slw.positions = _positionsSLW;
  slw.steps = _stepSLW;
  slw.posCnt = _ArrayLen__(_stepSLW);

  gw.name = _nameGW;
  gw.posname = _posGW;
  gw.positions = _positionsGW;
  gw.steps = _stepGW;
  gw.posCnt = _ArrayLen__(_stepGW);

  /*
  cc.name = _nameCC;
  cc.posname = _posCC;
  cc.positions = _positionsCC;
  cc.steps = _stepCC;
  cc.posCnt = _ArrayLen__(_stepCC);
  */

  _TheTReCSMech.mechCnt = __UFMAXINDXTRECS_;
  _TheTReCSMech.indexor = _indexors; 
  _TheTReCSMech.mech = _mech;

  _TheTReCSMech.mech[0] = sw;
  _TheTReCSMech.mech[1] = wc;
  _TheTReCSMech.mech[2] = aw;
  _TheTReCSMech.mech[3] = fw1;
  _TheTReCSMech.mech[4] = lw;
  _TheTReCSMech.mech[5] = fw2;
  _TheTReCSMech.mech[6] = piw;
  _TheTReCSMech.mech[7] = slw;
  _TheTReCSMech.mech[8] = gw;
  /*
  _TheTReCSMech.mech[9] = cc;
  */
  return &_TheTReCSMech;
}

/* this assumes that char** names never been initialized
   and is passed via &names: */
static int mechNamesTReCS(char*** names, char*** posnames) {
  static char **_names= 0, **_posnames= 0;
  UFTReCSMech* _TheTReCSMech= getTheUFTReCSMech();
  int i;
  if( _names == 0 ) {
    _names = (char**) calloc(__UFMAXINDXTRECS_, sizeof(char*));
    _posnames = (char**) calloc(__UFMAXINDXTRECS_, sizeof(char*));
    for( i = 0; i < _TheTReCSMech->mechCnt; ++i ) {
      _names[i] = _TheTReCSMech->mech[i].name;
      _posnames[i] = _TheTReCSMech->mech[i].posname;
    }
  }

  *names = _names;
  *posnames = _posnames;
  return _TheTReCSMech->mechCnt;
}

/* return position name steps value */
static double stepsFromHomeTReCS(const char* mechName, char* posName) {
  int m= 0, p= 0;
  char* s= 0;
  UFTReCSMechPos mech;
  UFTReCSMech *_TheTReCSMech = getTheUFTReCSMech();

  for( m = 0; m < _TheTReCSMech->mechCnt; ++m ) {
    mech = _TheTReCSMech->mech[m];
    if( (s = strstr(mech.name, mechName)) != 0 ) 
      break;
  }
  if( s == 0 )
    return 0;

  /* check for exact match first */
  for( p = 0; p < mech.posCnt; ++p ) {
    if( strcmp(mech.positions[p], posName) == 0 ) 
      break;
  }
  if( s == 0 ) { /* try substr */
    for( p = 0; p < mech.posCnt; ++p ) {
      if( (s = strstr(mech.positions[p], posName)) != 0 ) 
        break;
    }
  }
  if( s == 0 )
    return 0;

  return mech.steps[p];
};

/* return position name associated with nearest match to steps value */
static char* posNameNearTReCS(const char* mechName, double stepsFromHome) {
  static char* s= 0;
  int m= 0, p= 0;
  double diffmin= MAXINT, diffs[__UFMAXPOSTRECS_];
  UFTReCSMechPos mech;
  UFTReCSMech *_TheTReCSMech = getTheUFTReCSMech();

  /* try exact match */
  for( m = 0; m < _TheTReCSMech->mechCnt; ++m ) {
    mech = _TheTReCSMech->mech[m];
    if( strcmp(mech.name, mechName) == 0 ) {
      s = (char*)mechName; 
      break;
    }
  }
  if( s == 0 ) { /* try substring */
    for( m = 0; m < _TheTReCSMech->mechCnt; ++m ) {
      mech = _TheTReCSMech->mech[m];
      if( (s = strstr(mech.name, mechName)) != 0 ) 
        break;
    }
  }
  if( s == 0 )
    return (char*)_nonexist;

  for( p = 0; p < mech.posCnt; ++p ) {
    diffs[p] = fabs(stepsFromHome - mech.steps[p]);
    if( diffs[p] < diffmin ) {
      diffmin = diffs[p];
      s = mech.positions[p]; /* s should point to static memory */
    }
  }
  if( s == 0 )
    return (char*)_nonexist;

  return s;
}

#endif /* __UFTRECSMECH_H__ */
