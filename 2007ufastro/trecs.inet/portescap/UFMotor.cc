
#include	<string>
#include	<iostream>

#include	<cstdlib> // atoi()

#include	"UFStringTokenizer.h"
#include	"UFMotor.h"

using std::clog ; 
using std::endl ;
using std::pair ;

UFMotor::UFMotor( const UFStringTokenizer& st )
 : motorNumber( static_cast< unsigned int> (
	atoi( st[ 0 ].c_str() ) ) ),
   initialSpeed( atoi( st[ 1 ].c_str() ) ),
   terminalSpeed( atoi( st[ 2 ].c_str() ) ),
   acceleration( atoi( st[ 3 ].c_str() ) ),
   deceleration( atoi( st[ 4 ].c_str() ) ),
   holdCurrent( atoi( st[ 5 ].c_str() ) ),
   driveCurrent( atoi( st[ 6 ].c_str() ) ),
   axisName( st[ 7 ][ 0 ] ),
   homingSpeed( atoi( st[ 8 ].c_str() ) ),
   finalHomingSpeed( atoi( st[ 9 ].c_str() ) ),
   homingDirection( st[ 10 ][ 0 ] ),
   backlash( atoi( st[ 11 ].c_str() ) ),
   epicsName( st[ 12 ] ),
   motorName( st.assemble( 13 ) )
{}

UFMotor::UFMotor( const UFMotor& rhs )
 : namedPositionMap( rhs.namedPositionMap ),
   motorNumber( rhs.motorNumber ),
   initialSpeed( rhs.initialSpeed ),
   terminalSpeed( rhs.terminalSpeed ),
   acceleration( rhs.acceleration ),
   deceleration( rhs.deceleration ),
   holdCurrent( rhs.holdCurrent ),
   driveCurrent( rhs.driveCurrent ),
   axisName( rhs.axisName ),
   homingSpeed( rhs.homingSpeed ),
   finalHomingSpeed( rhs.finalHomingSpeed ),
   homingDirection( rhs.homingDirection ),
   backlash( rhs.backlash ),
   epicsName( rhs.epicsName ),
   motorName( rhs.motorName )
{
//clog	<< "UFMotor> CC, old map size: "
//	<< rhs.namedPositionMap.size()
//	<< ", new map size: "
//	<< namedPositionMap.size()
//	<< endl ;
}

UFMotor::~UFMotor()
{
/* No heap space allocated */
//clog	<< "~UFMotor" << endl ;
}

UFMotor& UFMotor::operator=( const UFMotor& rhs )
{
namedPositionMap = rhs.namedPositionMap ;
motorNumber = rhs.motorNumber ;
initialSpeed = rhs.initialSpeed ;
terminalSpeed = rhs.terminalSpeed ;
acceleration = rhs.acceleration ;
deceleration = rhs.deceleration ;
holdCurrent = rhs.holdCurrent ;
driveCurrent = rhs.driveCurrent ;
axisName = rhs.axisName ;
homingSpeed = rhs.homingSpeed ;
finalHomingSpeed = rhs.finalHomingSpeed ;
homingDirection = rhs.homingDirection ;
backlash = rhs.backlash ;
epicsName = rhs.epicsName ;
motorName = rhs.motorName ;

return *this ;
}


void UFMotor::addNamedPosition( const UFNamedPosition& thePos )
{
pair< unsigned int, UFNamedPosition > thePair( thePos.positionNumber,
	thePos ) ;
if( !namedPositionMap.insert( thePair ).second )
	{
	clog	<< "UFMotor::addNamedPosition> Failed to add "
		<< "position: "
		<< thePos
		<< endl ;
	}
else
	{
//	clog	<< "UFMotor::addNamedPosition> Successfully added: "
//		<< thePos
//		<< ", new map size: "
//		<< namedPositionMap.size()
//		<< endl ;
	}
}
