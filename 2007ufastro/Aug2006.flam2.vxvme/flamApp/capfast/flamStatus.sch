[schematic2]
uniq 45
[tools]
[detail]
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Added exposure status
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/02/20
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos System Status
s 2320 -240 100 1792 Rev: C
s 2096 -240 100 1792 2002/11/11
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamStatus.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2000/11/18
s 2016 2000 100 1792 C
s 2240 2000 100 1792 Added observeC
s 2480 2000 100 1792 WNR
s 2624 2000 100 1792 2002/11/11
[cell use]
use changeBar 1984 1991 100 0 changeBar#42
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#41
xform 0 2336 2064
use changeBar 1984 1959 100 0 changeBar#44
xform 0 2336 2000
use flamObserveC 352 807 100 0 flamObserveC#43
xform 0 544 1024
use flamExposure 896 807 100 0 flamExposure#40
xform 0 1088 1024
use flamHeartbeat 2112 903 100 0 flamHeartbeat#39
xform 0 2240 1024
use flamState 1568 855 100 0 flamState#38
xform 0 1664 1024
use flamApplyC -112 783 100 0 flamApplyC#37
xform 0 0 1024
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamStatus.sch,v 0.0 2006/06/21 15:20:21 hon Exp $
