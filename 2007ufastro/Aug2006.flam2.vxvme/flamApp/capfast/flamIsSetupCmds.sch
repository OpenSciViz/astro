[schematic2]
uniq 105
[tools]
[detail]
w 328 1419 100 0 MESS eapply.eapply#21.MESS 384 1184 480 1184 480 1408 224 1408 224 1536 320 1536 outhier.MESS.p
w 296 1355 100 0 VAL eapply.eapply#21.VAL 384 1216 448 1216 448 1344 192 1344 192 1664 320 1664 outhier.VAL.p
w -8 1419 100 0 CLID eapply.eapply#21.CLID 0 1184 -96 1184 -96 1408 128 1408 128 1536 64 1536 inhier.CLID.P
w 24 1355 100 0 DIR eapply.eapply#21.DIR 0 1216 -64 1216 -64 1344 160 1344 160 1664 64 1664 inhier.DIR.P
w 840 843 100 0 n#93 eapply.eapply#21.OCLE 384 832 1344 832 1344 672 1472 672 flamObsSetup.flamObsSetup#104.ICID
w 1160 907 100 0 n#92 eapply.eapply#21.OCLD 384 896 1984 896 1984 672 2112 672 flamSetDhsInfo.flamSetDhsInfo#103.ICID
w 1144 971 100 0 n#91 eapply.eapply#21.OCLC 384 960 1952 960 1952 1632 2112 1632 flamSetWcs.flamSetWcs#102.ICID
w 824 1035 100 0 n#90 eapply.eapply#21.OCLB 384 1024 1312 1024 1312 1632 1472 1632 flamDataMode.flamDataMode#101.ICID
w 504 1099 100 0 n#89 eapply.eapply#21.OCLA 384 1088 672 1088 672 1632 832 1632 flamInsSetup.flamInsSetup#100.ICID
w 1080 -53 100 0 n#88 eapply.eapply#21.INPD 0 928 -320 928 -320 -64 2528 -64 2528 736 2304 736 flamSetDhsInfo.flamSetDhsInfo#103.VAL
w 1080 -21 100 0 n#87 eapply.eapply#21.INMD 0 896 -288 896 -288 -32 2496 -32 2496 672 2304 672 flamSetDhsInfo.flamSetDhsInfo#103.MESS
w 792 11 100 0 n#86 eapply.eapply#21.INPE 0 864 -256 864 -256 0 1888 0 1888 736 1664 736 flamObsSetup.flamObsSetup#104.VAL
w 792 43 100 0 n#85 eapply.eapply#21.INME 0 832 -224 832 -224 32 1856 32 1856 672 1664 672 flamObsSetup.flamObsSetup#104.MESS
w 1032 1963 100 0 n#82 eapply.eapply#21.INMC 0 960 -320 960 -320 1952 2432 1952 2432 1632 2304 1632 flamSetWcs.flamSetWcs#102.MESS
w 1032 1931 100 0 n#81 eapply.eapply#21.INPC 0 992 -288 992 -288 1920 2400 1920 2400 1696 2304 1696 flamSetWcs.flamSetWcs#102.VAL
w 744 1899 100 0 n#80 eapply.eapply#21.INMB 0 1024 -256 1024 -256 1888 1792 1888 1792 1632 1664 1632 flamDataMode.flamDataMode#101.MESS
w 744 1867 100 0 n#79 eapply.eapply#21.INPB 0 1056 -224 1056 -224 1856 1760 1856 1760 1696 1664 1696 flamDataMode.flamDataMode#101.VAL
w 456 1835 100 0 n#78 eapply.eapply#21.INMA 0 1088 -192 1088 -192 1824 1152 1824 1152 1632 1024 1632 flamInsSetup.flamInsSetup#100.MESS
w 456 1803 100 0 n#77 eapply.eapply#21.INPA 0 1120 -160 1120 -160 1792 1120 1792 1120 1696 1024 1696 flamInsSetup.flamInsSetup#100.VAL
w 856 875 100 0 n#75 eapply.eapply#21.OUTE 384 864 1376 864 1376 736 1472 736 flamObsSetup.flamObsSetup#104.DIR
w 1176 939 100 0 n#74 eapply.eapply#21.OUTD 384 928 2016 928 2016 736 2112 736 flamSetDhsInfo.flamSetDhsInfo#103.DIR
w 1128 1003 100 0 n#73 flamSetWcs.flamSetWcs#102.DIR 2112 1696 1920 1696 1920 992 384 992 eapply.eapply#21.OUTC
w 488 1131 100 0 n#72 flamInsSetup.flamInsSetup#100.DIR 832 1696 640 1696 640 1120 384 1120 eapply.eapply#21.OUTA
w 808 1067 100 0 n#71 flamDataMode.flamDataMode#101.DIR 1472 1696 1280 1696 1280 1056 384 1056 eapply.eapply#21.OUTB
s 2624 2032 100 1792 2000/12/08
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2528 -240 100 1792 flamIsSetupCmds.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/08
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos Setup Commands
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use flamObsSetup 1472 199 100 0 flamObsSetup#104
xform 0 1568 512
use flamSetDhsInfo 2112 199 100 0 flamSetDhsInfo#103
xform 0 2208 512
use flamSetWcs 2112 1159 100 0 flamSetWcs#102
xform 0 2208 1472
use flamDataMode 1472 1159 100 0 flamDataMode#101
xform 0 1568 1472
use flamInsSetup 832 1159 100 0 flamInsSetup#100
xform 0 928 1472
use inhier 48 1495 100 0 CLID
xform 0 64 1536
use inhier 48 1623 100 0 DIR
xform 0 64 1664
use outhier 288 1495 100 0 MESS
xform 0 304 1536
use outhier 288 1623 100 0 VAL
xform 0 304 1664
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use eapply 0 583 100 0 eapply#21
xform 0 192 944
p 192 576 100 1024 1 name:$(top)apply2
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamIsSetupCmds.sch,v 0.0 2006/06/21 15:20:21 hon Exp $
