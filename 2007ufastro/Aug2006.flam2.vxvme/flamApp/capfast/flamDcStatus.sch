[schematic2]
uniq 297
[tools]
[detail]
w 4162 2635 100 0 n#296 egenSubB.$(top)$(dev)actEffG.OUTE 3872 2688 3968 2688 3968 2624 4416 2624 hwout.hwout#291.outp
w 4178 2699 100 0 n#295 egenSubB.$(top)$(dev)actEffG.OUTD 3872 2720 4000 2720 4000 2688 4416 2688 hwout.hwout#290.outp
w 4162 2891 100 0 n#294 egenSubB.$(top)$(dev)actEffG.OUTA 3872 2816 3968 2816 3968 2880 4416 2880 hwout.hwout#289.outp
w 4178 2827 100 0 n#293 egenSubB.$(top)$(dev)actEffG.OUTB 3872 2784 4000 2784 4000 2816 4416 2816 hwout.hwout#288.outp
w 4114 2763 100 0 n#292 egenSubB.$(top)$(dev)actEffG.OUTC 3872 2752 4416 2752 hwout.hwout#287.outp
w 2562 747 100 0 n#280 ebos.ebos#266.OUT 2528 736 2656 736 hwout.hwout#275.outp
w 2562 523 100 0 n#279 ebos.ebos#270.OUT 2528 512 2656 512 hwout.hwout#276.outp
w 2562 299 100 0 n#278 ebos.ebos#273.OUT 2528 288 2656 288 hwout.hwout#277.outp
w 2194 363 100 0 n#274 ebis.$(top)rdout.VAL 2144 288 2176 288 2176 352 2272 352 ebos.ebos#273.DOL
w 2178 331 100 0 n#272 ebis.$(top)rdout.FLNK 2144 320 2272 320 ebos.ebos#273.SLNK
w 2178 555 100 0 n#271 ebis.$(top)acq.FLNK 2144 544 2272 544 ebos.ebos#270.SLNK
w 2194 587 100 0 n#269 ebis.$(top)acq.VAL 2144 512 2176 512 2176 576 2272 576 ebos.ebos#270.DOL
w 2194 811 100 0 n#268 ebis.$(top)prep.VAL 2144 736 2176 736 2176 800 2272 800 ebos.ebos#266.DOL
w 2178 779 100 0 n#267 ebis.$(top)prep.FLNK 2144 768 2272 768 ebos.ebos#266.SLNK
s 2240 1084 120 0 dac value 42
s 2240 1116 120 0 dac value 41
s 2240 1148 120 0 dac value 40
s 2240 1181 120 0 dac value 39
s 2240 1213 120 0 dac value 38
s 2240 1245 120 0 dac value 37
s 2240 1277 120 0 dac value 36
s 2240 1310 120 0 dac value 35
s 2240 1341 120 0 dac value 34
s 2240 1372 120 0 dac value 33
s 2240 1405 120 0 dac value 32
s 2240 1437 120 0 dac vlaue 31
s 2240 1469 120 0 dac value 30
s 2240 1502 120 0 dac value 29
s 2240 1534 120 0 dac value 28
s 2240 1566 120 0 dac value 27
s 2240 1598 120 0 dac value 26
s 2240 1631 120 0 dac value 25
s 2240 1662 120 0 dac value 24
s 2240 1693 120 0 dac value 23
s 2240 1725 120 0 dac value 22
s 1408 1082 120 0 dac value 21
s 1408 1114 120 0 dac value 20
s 1408 1146 120 0 dac value 19
s 1408 1179 120 0 dac value 18
s 1408 1211 120 0 dac value 17
s 1408 1243 120 0 dac value 16
s 1408 1275 120 0 dac value 15
s 1408 1308 120 0 dac value 14
s 1408 1339 120 0 dac value 13
s 1408 1370 120 0 dac value 12
s 1408 1403 120 0 dac value 11
s 1408 1435 120 0 dac value 10
s 1408 1467 120 0 dac value 9
s 1408 1500 120 0 dac value 8
s 1408 1532 120 0 dac value 7
s 1408 1564 120 0 dac value 6
s 1408 1596 120 0 dac value 5
s 1408 1629 120 0 dac value 4
s 1408 1660 120 0 dac value 3
s 1408 1691 120 0 dac value 2
s 844 1441 120 0 input array
s 1408 1723 120 0 dac value 1
s 1676 1441 120 0 input array
s 3072 1723 120 0 well Current
s 2508 1441 120 0 input array
s 2508 2529 120 0 input array
s 3360 2544 120 0 input array
s 4561 467 210 0 mce4_status_06.sch 3/28/2002 3:30 PM EST
s 3074 2812 120 0 chopCoaddUC
s 3074 2776 120 0 chopSettleUF
s 3074 2746 120 0 frameCoaddUF
s 3074 2716 120 0 nodSets
s 3074 2681 120 0 nodSettleUC
s 3074 2653 120 0 nodsettleUF
s 3074 2617 120 0 pixelClocks
s 3074 2587 120 0 postChopsUC
s 3074 2557 120 0 preChopsUC
s 3074 2522 120 0 saveSets
s 3074 2494 120 0 readoutMode
s 4032 2896 120 0 chop Efficiency
s 4032 2832 120 0 frame Efficiency
s 4032 2768 120 0 nod Efficiency
s 4032 2704 120 0 on Src Efficiency
s 4032 2640 120 0 photon Efficiency
s 4832 1723 120 0 chop Delay
s 4832 1693 120 0 chop Frequency
s 4832 1659 120 0 frame Time
s 4832 1627 120 0 nod Delay
s 4832 1595 120 0 nod Time
s 4832 1563 120 0 save Frequency
s 3073 1691 120 0 well Minimum
s 3073 1659 120 0 well Maximum
s 3073 1627 120 0 nod Cycle
s 3073 1596 120 0 nod Beam
s 3073 1563 120 0 save Set
s 2240 2172 120 0 dac value 42
s 2240 2204 120 0 dac value 41
s 2240 2236 120 0 dac value 40
s 2240 2269 120 0 dac value 39
s 2240 2301 120 0 dac value 38
s 2240 2333 120 0 dac value 37
s 2240 2365 120 0 dac value 36
s 2240 2398 120 0 dac value 35
s 2240 2429 120 0 dac value 34
s 2240 2460 120 0 dac value 33
s 2240 2493 120 0 dac value 32
s 2240 2525 120 0 dac vlaue 31
s 2240 2557 120 0 dac value 30
s 2240 2590 120 0 dac value 29
s 2240 2622 120 0 dac value 28
s 2240 2654 120 0 dac value 27
s 2240 2686 120 0 dac value 26
s 2240 2719 120 0 dac value 25
s 2240 2750 120 0 dac value 24
s 2240 2781 120 0 dac value 23
s 2240 2813 120 0 dac value 22
s 1408 2170 120 0 dac value 21
s 1408 2202 120 0 dac value 20
s 1408 2234 120 0 dac value 19
s 1408 2267 120 0 dac value 18
s 1408 2299 120 0 dac value 17
s 1408 2331 120 0 dac value 16
s 1408 2363 120 0 dac value 15
s 1408 2396 120 0 dac value 14
s 1408 2427 120 0 dac value 13
s 1408 2458 120 0 dac value 12
s 1408 2491 120 0 dac value 11
s 1408 2523 120 0 dac value 10
s 1408 2555 120 0 dac value 9
s 1408 2588 120 0 dac value 8
s 1408 2620 120 0 dac value 7
s 1408 2652 120 0 dac value 6
s 1408 2684 120 0 dac value 5
s 1408 2717 120 0 dac value 4
s 1408 2748 120 0 dac value 3
s 1408 2779 120 0 dac value 2
s 844 2529 120 0 input array
s 1408 2811 120 0 dac value 1
s 1676 2529 120 0 input array
s 3372 1441 120 0 input array
s 3937 1726 120 0 chop Efficiency
s 3937 1694 120 0 frame Efficiency
s 3937 1661 120 0 nod Efficiency
s 3937 1628 120 0 on Src Efficiency
s 3937 1596 120 0 photon Efficiency
[cell use]
use hwout 2656 695 100 0 hwout#275
xform 0 2752 736
p 2853 730 100 0 -1 val(outp):$(sad)prep.VAL PP NMS
use hwout 2656 471 100 0 hwout#276
xform 0 2752 512
p 2856 504 100 0 -1 val(outp):$(sad)acq.VAL PP NMS
use hwout 2656 247 100 0 hwout#277
xform 0 2752 288
p 2856 279 100 0 -1 val(outp):$(sad)rdout.VAL PP NMS
use hwout 4416 2711 100 0 hwout#287
xform 0 4512 2752
p 4640 2752 100 0 -1 val(outp):$(sad)nodEfficiency PP NMS
use hwout 4416 2775 100 0 hwout#288
xform 0 4512 2816
p 4640 2816 100 0 -1 val(outp):$(sad)frameEfficiency PP NMS
use hwout 4416 2839 100 0 hwout#289
xform 0 4512 2880
p 4640 2880 100 0 -1 val(outp):$(sad)chopEfficiency PP NMS
use hwout 4416 2647 100 0 hwout#290
xform 0 4512 2688
p 4640 2688 100 0 -1 val(outp):$(sad)onSrcEfficiency PP NMS
use hwout 4416 2583 100 0 hwout#291
xform 0 4512 2624
p 4640 2624 100 0 -1 val(outp):$(sad)photonEfficiency PP NMS
use egenSubB 1056 935 -100 0 $(top)$(dev)dacActVal
xform 0 1200 1360
p 899 1675 100 0 0 DESC:DAC Actual Values
p 1168 928 100 1024 -1 name:$(dc)dacActVal
use egenSubB 1888 935 -100 0 $(top)$(dev)dacActVal2
xform 0 2032 1360
p 1731 1675 100 0 0 DESC:DAC Actual Values 2
p 2000 928 100 1024 -1 name:$(dc)dacActVal2
use egenSubB 2720 935 -100 0 $(top)$(dev)obsStatusG
xform 0 2864 1360
p 2563 1675 100 0 0 DESC:Observation Status
p 2832 928 100 1024 -1 name:$(dc)obsStatusG
use egenSubB 2720 2023 -100 0 $(top)$(dev)actCfgHrdwrG
xform 0 2864 2448
p 2563 2763 100 0 0 DESC:Actual Configuration - Hardware
p 2832 2016 100 1024 -1 name:$(dc)actCfgHrdwrG
use egenSubB 3584 2023 -100 0 $(top)$(dev)actEffG
xform 0 3728 2448
p 3427 2763 100 0 0 DESC:Actual Efficiency
p 3696 2016 100 1024 -1 name:$(dc)actEffG
p 3872 2826 75 0 -1 pproc(OUTA):PP
p 3872 2794 75 0 -1 pproc(OUTB):PP
p 3872 2762 75 0 -1 pproc(OUTC):PP
p 3872 2730 75 0 -1 pproc(OUTD):PP
p 3872 2698 75 0 -1 pproc(OUTE):PP
use egenSubB 4480 935 -100 0 $(top)$(dev)physCfgG
xform 0 4624 1360
p 4323 1675 100 0 0 DESC:Physical Configuration
p 4592 928 100 1024 -1 name:$(dc)physCfgG
use egenSubB 1056 2023 -100 0 $(top)$(dev)dacActVolt
xform 0 1200 2448
p 899 2763 100 0 0 DESC:DAC Actual Voltage
p 1168 2016 100 1024 -1 name:$(dc)dacActVolt
use egenSubB 1888 2023 -100 0 $(top)$(dev)dacActVolt2
xform 0 2032 2448
p 1731 2763 100 0 0 DESC:DAC Actual Voltage 2
p 2000 2016 100 1024 -1 name:$(dc)dacActVolt2
use egenSubB 3584 935 -100 0 egenSubB#285
xform 0 3728 1360
p 3427 1675 100 0 0 DESC:Calculated Efficiency
p 3696 928 100 1024 -1 name:$(dc)calcEffG
use ebos 2272 679 100 0 ebos#266
xform 0 2400 768
p 2040 924 100 0 0 DESC:Prep Flag BO
p 2304 640 100 0 1 OMSL:closed_loop
p 1952 622 100 0 0 ONAM:TRUE
p 1952 654 100 0 0 ZNAM:FALSE
p 2304 672 100 768 -1 name:$(top)prepBO
p 2528 736 75 768 -1 pproc(OUT):PP
use ebos 2272 455 100 0 ebos#270
xform 0 2400 544
p 2040 700 100 0 0 DESC:ACQ Flag BO
p 2304 416 100 0 1 OMSL:closed_loop
p 1952 398 100 0 0 ONAM:TRUE
p 1952 430 100 0 0 ZNAM:FALSE
p 2304 448 100 768 -1 name:$(top)acqBO
p 2528 512 75 768 -1 pproc(OUT):PP
use ebos 2272 231 100 0 ebos#273
xform 0 2400 320
p 2040 476 100 0 0 DESC:Read Out BO
p 2304 192 100 0 1 OMSL:closed_loop
p 1952 174 100 0 0 ONAM:TRUE
p 1952 206 100 0 0 ZNAM:FALSE
p 2384 224 100 1024 -1 name:$(top)rdoutBO
p 2528 288 75 768 -1 pproc(OUT):PP
use bd200tr 448 -24 -100 0 frame
xform 0 3088 1680
use eais 1504 455 -100 0 $(top)$(dev)TempSet
xform 0 1632 528
p 1293 601 100 0 0 DESC:Detector Temperature
p 1616 448 100 1024 -1 name:$(dc)TempSet
use ebis 1888 455 100 0 $(top)acq
xform 0 2016 528
p 1715 603 100 0 0 DESC:ACQ Flag
p 1664 366 100 0 0 ONAM:TRUE
p 1664 398 100 0 0 ZNAM:FALSE
p 2000 448 100 1024 0 name:$(top)acq
use ebis 1888 679 100 0 $(top)prep
xform 0 2016 752
p 1715 827 100 0 0 DESC:Prep Flag
p 1664 590 100 0 0 ONAM:TRUE
p 1664 622 100 0 0 ZNAM:FALSE
p 2000 672 100 1024 0 name:$(top)prep
use ebis 1888 227 120 0 $(top)rdout
xform 0 2016 304
p 1715 379 100 0 0 DESC:Read Out Flag
p 1664 142 100 0 0 ONAM:TRUE
p 1664 174 100 0 0 ZNAM:FALSE
p 2000 224 100 1024 0 name:$(top)rdout
use estringins 1120 675 -120 0 $(top)$(dev)state
xform 0 1248 752
p 1174 918 100 0 0 DESC:State 
p 1232 672 100 1024 -1 name:$(dc)state
use estringins 1504 679 -100 0 $(top)$(dev)log
xform 0 1632 752
p 1558 918 100 0 0 DESC:DC Log
p 1616 672 100 1024 -1 name:$(dc)log
[comments]
RCS: "$Name:  $ $Id: flamDcStatus.sch,v 0.0 2006/06/21 15:19:29 hon Exp $"
