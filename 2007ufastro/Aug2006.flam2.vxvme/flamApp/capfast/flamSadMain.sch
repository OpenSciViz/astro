[schematic2]
uniq 55
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos SAD Overview
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamSadMain.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2000/11/18
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Added EC status
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/01/25
[cell use]
use flamSadFits 2240 135 100 0 flamSadFits#54
xform 0 2384 288
use flamSadCcEngineering 128 135 100 0 flamSadCcEngineering#34
xform 0 272 288
p 148 108 100 0 1 setDev:dev DECKER
use flamSadCcEngineering -304 135 100 0 flamSadCcEngineering#47
xform 0 -160 288
p -284 108 100 0 1 setDev:dev MOS
use flamSadCcEngineering -320 -249 100 0 flamSadCcEngineering#48
xform 0 -176 -96
p -300 -276 100 0 1 setDev:dev FILT1
use flamSadCcEngineering 128 -249 100 0 flamSadCcEngineering#49
xform 0 272 -96
p 148 -276 100 0 1 setDev:dev FILT2
use flamSadCcEngineering 480 135 100 0 flamSadCcEngineering#50
xform 0 624 288
p 500 108 100 0 1 setDev:dev LYOT
use flamSadCcEngineering 480 -249 100 0 flamSadCcEngineering#51
xform 0 624 -96
p 500 -276 100 0 1 setDev:dev GRISM
use flamSadCcEngineering -352 -633 100 0 flamSadCcEngineering#52
xform 0 -208 -480
p -332 -660 100 0 1 setDev:dev FOCUS
use flamSadCcEngineering 480 -665 100 0 flamSadCcEngineering#53
xform 0 624 -512
p 500 -692 100 0 1 setDev:dev WINDOW
use flamSadBcrStatus 2240 487 100 0 flamSadBcrStatus#46
xform 0 2384 640
use flamSadBcrConfig 2240 839 100 0 flamSadBcrConfig#45
xform 0 2384 992
use flamSadEcEngineering 896 135 100 0 flamSadEcEngineering#42
xform 0 1040 288
use flamSadEcStatus 896 487 100 0 flamSadEcStatus#41
xform 0 1040 640
use flamSadEcConfig 896 839 100 0 flamSadEcConfig#40
xform 0 1040 992
use changeBar 1984 2023 100 0 changeBar#38
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#39
xform 0 2336 2032
use flamSadDcStatus 1664 487 100 0 flamSadDcStatus#36
xform 0 1808 640
use flamSadCcStatus 128 487 100 0 flamSadCcStatus#35
xform 0 272 640
use flamSadCcConfig 128 839 100 0 flamSadCcConfig#33
xform 0 272 992
use flamSadDcEngineering 1664 135 100 0 flamSadDcEngineering#32
xform 0 1808 288
use flamSadDcConfig 1664 839 100 0 flamSadDcConfig#31
xform 0 1808 992
use flamSadInsStatus 1152 1223 100 0 flamSadInsStatus#30
xform 0 1296 1376
use flamSadObsStatus 640 1223 100 0 flamSadObsStatus#29
xform 0 784 1376
use flamSadInsConfig 1152 1575 100 0 flamSadInsConfig#28
xform 0 1296 1728
use flamSadObsConfig 640 1575 100 0 flamSadObsConfig#27
xform 0 784 1728
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadMain.sch,v 0.0 2006/06/21 15:20:21 hon Exp $
