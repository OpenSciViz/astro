[schematic2]
uniq 116
[tools]
[detail]
w 738 11 100 0 n#115 flamContinue.flamContinue#106.VAL 1664 704 1792 704 1792 0 -256 0 -256 928 0 928 eapply.eapply#21.INPD
w 738 43 100 0 n#114 flamContinue.flamContinue#106.MESS 1664 640 1760 640 1760 32 -224 32 -224 896 0 896 eapply.eapply#21.INMD
w 450 75 100 0 n#113 flamPause.flamPause#111.VAL 1024 704 1152 704 1152 64 -192 64 -192 864 0 864 eapply.eapply#21.INPE
w 450 107 100 0 n#112 flamPause.flamPause#111.MESS 1024 640 1120 640 1120 96 -160 96 -160 832 0 832 eapply.eapply#21.INME
w 514 843 100 0 n#110 eapply.eapply#21.OCLE 384 832 704 832 704 640 832 640 flamPause.flamPause#111.ICID
w 530 875 100 0 n#109 eapply.eapply#21.OUTE 384 864 736 864 736 704 832 704 flamPause.flamPause#111.DIR
w 834 907 100 0 n#108 eapply.eapply#21.OCLD 384 896 1344 896 1344 640 1472 640 flamContinue.flamContinue#106.ICID
w 850 939 100 0 n#107 eapply.eapply#21.OUTD 384 928 1376 928 1376 704 1472 704 flamContinue.flamContinue#106.DIR
w 328 1419 100 0 MESS eapply.eapply#21.MESS 384 1184 480 1184 480 1408 224 1408 224 1536 320 1536 outhier.MESS.p
w 296 1355 100 0 VAL eapply.eapply#21.VAL 384 1216 448 1216 448 1344 192 1344 192 1664 320 1664 outhier.VAL.p
w -8 1419 100 0 CLID eapply.eapply#21.CLID 0 1184 -96 1184 -96 1408 128 1408 128 1536 64 1536 inhier.CLID.P
w 24 1355 100 0 DIR eapply.eapply#21.DIR 0 1216 -64 1216 -64 1344 160 1344 160 1664 64 1664 inhier.DIR.P
w 1176 971 100 0 n#91 eapply.eapply#21.OCLC 384 960 2016 960 2016 1600 2112 1600 flamAbort.flamAbort#102.ICID
w 856 1035 100 0 n#90 eapply.eapply#21.OCLB 384 1024 1376 1024 1376 1600 1472 1600 flamStop.flamStop#101.ICID
w 536 1099 100 0 n#89 eapply.eapply#21.OCLA 384 1088 736 1088 736 1600 832 1600 flamObserve.flamObserve#100.ICID
w 1032 1963 100 0 n#82 eapply.eapply#21.INMC 0 960 -320 960 -320 1952 2432 1952 2432 1600 2304 1600 flamAbort.flamAbort#102.MESS
w 1032 1931 100 0 n#81 eapply.eapply#21.INPC 0 992 -288 992 -288 1920 2400 1920 2400 1664 2304 1664 flamAbort.flamAbort#102.VAL
w 744 1899 100 0 n#80 eapply.eapply#21.INMB 0 1024 -256 1024 -256 1888 1792 1888 1792 1600 1664 1600 flamStop.flamStop#101.MESS
w 744 1867 100 0 n#79 eapply.eapply#21.INPB 0 1056 -224 1056 -224 1856 1760 1856 1760 1664 1664 1664 flamStop.flamStop#101.VAL
w 456 1835 100 0 n#78 eapply.eapply#21.INMA 0 1088 -192 1088 -192 1824 1152 1824 1152 1600 1024 1600 flamObserve.flamObserve#100.MESS
w 456 1803 100 0 n#77 eapply.eapply#21.INPA 0 1120 -160 1120 -160 1792 1120 1792 1120 1664 1024 1664 flamObserve.flamObserve#100.VAL
w 1160 1003 100 0 n#73 flamAbort.flamAbort#102.DIR 2112 1664 1984 1664 1984 992 384 992 eapply.eapply#21.OUTC
w 520 1131 100 0 n#72 flamObserve.flamObserve#100.DIR 832 1664 704 1664 704 1120 384 1120 eapply.eapply#21.OUTA
w 840 1067 100 0 n#71 flamStop.flamStop#101.DIR 1472 1664 1344 1664 1344 1056 384 1056 eapply.eapply#21.OUTB
s 2624 2032 100 1792 2002/11/11
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Added Pause and Continue
s 2016 2032 100 1792 B
s 2624 2064 100 1792 2000/12/08
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2528 -240 100 1792 flamIsObserveCmds.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2002/11/11
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Flamingos Observation Commands
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use flamPause 832 167 100 0 flamPause#111
xform 0 928 480
use flamContinue 1472 167 100 0 flamContinue#106
xform 0 1568 480
use changeBar 1984 1991 100 0 changeBar#104
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#103
xform 0 2336 2064
use flamAbort 2112 1127 100 0 flamAbort#102
xform 0 2208 1440
use flamStop 1472 1127 100 0 flamStop#101
xform 0 1568 1440
use flamObserve 832 1127 100 0 flamObserve#100
xform 0 928 1440
use inhier 48 1495 100 0 CLID
xform 0 64 1536
use inhier 48 1623 100 0 DIR
xform 0 64 1664
use outhier 288 1495 100 0 MESS
xform 0 304 1536
use outhier 288 1623 100 0 VAL
xform 0 304 1664
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use eapply 0 583 100 0 eapply#21
xform 0 192 944
p 192 576 100 1024 1 name:$(top)apply3
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamIsObserveCmds.sch,v 0.0 2006/06/21 15:20:21 hon Exp $
