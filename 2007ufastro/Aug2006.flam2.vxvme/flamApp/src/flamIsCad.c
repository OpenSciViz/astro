
/* ===================================================================== */
/* INDENT OFF */
/*+
 *
 * FILENAME
 * -------- 
 * flamIsCad.c
 *
 *
 * PURPOSE
 * -------
 * Support code for T-ReCS instrument command CAD records.
 *
 * 
 * FUNCTION NAME(S)
 * ---------------- 
 * flamIsAbortProcess        - abort command processing
 * flamIsContinueProcess     - continue command processing
 * flamIsDataModeProcess     - dataMode command processing
 * flamIsDatumProcess        - datum command processing
 * flamIsDebugProcess        - debug command processing
 * flamIsInitProcess         - init command processing
 * flamIsInsSetupProcess     - instrumentSetup command processing
 * flamIsNullInit            - generic NULL initialization function
 * flamIsObsSetupProcess     - observiationSetup command processing
 * flamIsObserveProcess      - observe command processing
 * flamIsParkProcess         - park command processing
 * flamIsPauseProcess        - pause command processing
 * flamIsRebootProcess       - reboot command processing
 * flamIsSetDhsInfoProcess   - setDhsInfo command processing
 * flamIsSetWcsProcess       - setWcs command processing
 * flamIsStopProcess         - stop command processing
 * flamIsTestProcess         - test command processing
 *
 *
 * DEPENDENCIES
 * ------------
 * None
 *
 *
 * LIMITATIONS
 * -----------
 * None
 *
 * 
 * AUTHOR
 * ------
 * William Rambold  (wrambold@gemini.edu)
 * 
 *
 * HISTORY
 * -------
 * 2000/12/11  WNR  Template coding
 * 2001/12/24  WNR  Changed filter and device lookup file handling.
 */
/* INDENT ON */
/* ===================================================================== */


/*
 *  Include files
 */

#include <stdioLib.h>
#include <stdlib.h>
#include <string.h>

#include <sysLib.h>
#include <logLib.h>
#include <tickLib.h>

#include <dbEvent.h>
#include <recSup.h>

#include <cad.h>
#include <cadRecord.h>
#include <cadef.h>


#include "ufdbl.h"
#include <flam.h>
#include <flamIsCad.h>

#include <flamPositionLookup.h>

#include "gem_wcs.h"


/*
 *  Define a set of bit manipulation macros to keep track of the 
 *  configuration being applied so that conflicting commands can
 *  be rejected.
 */

#define TRX_INIT            0x00000001
#define TRX_DATUM           0x00000002
#define TRX_PARK            0x00000004
#define TRX_TEST            0x00000008
#define TRX_REBOOT          0x00000010
#define TRX_DEBUG           0x00000020
#define TRX_DATA_MODE       0x00000100
#define TRX_DHS_INFO        0x00000200
#define TRX_SET_WCS         0x00000400
#define TRX_INS             0x00001000
#define TRX_OBS             0x00002000
#define TRX_START_OBS       0x00010000
#define TRX_STOP_OBS        0x00020000
#define TRX_ABORT_OBS       0x00040000
#define TRX_CONTINUE_OBS    0x00080000

#define TRX_SETUP TRX_DEBUG|TRX_DATA_MODE|TRX_DHS_INFO|TRX_SET_WCS|TRX_INS|TRX_OBS

#define TRX_MARK_COMMAND(a)   flamCommandMask |= (a);
#define TRX_CLEAR_COMMAND(a)  flamCommandMask &= ~(a);
#define TRX_CMD_CONFLICT(a)   flamCommandMask & ~(a)


/*
 *  Define a macro to print debugging information to the VxWorks logging
 *  system.  If the current debugging level set by the debug command is
 *  greater than or equal to the debugging threshold given to the macro then 
 *  the given information message string is sent to the logging task.
 *
 *  The string consists of the system tick counter followd by the
 *  name of the record and then a formatted string containing one
 *  integer variable.   For example:
 *
 *  DEBUG(TRX_DEBUG_FULL,
 *        "<%d> %s:initializing with %s simulation\n", pcr->a);
 *  
 *  Would result in the following log message if debugging is set to FULL:
 *
 *  <312456> flam:init: Starting FULL simulation initialization
 *
 *  WARNING!! assumes that the CAD record struct pointer is called "pcr"
 */

#define DEBUG(l,FMT,V) if (l <= flamIsDebugLevel)             \
                            logMsg (FMT,                       \
                                    (int) tickGet(),           \
                                    (int) pcr->name,           \
                                    (int) V, 0, 0, 0);


#define NAME_SIZE 32



typedef struct
{
    char cameraMode[NAME_SIZE];
    char imagingMode[NAME_SIZE];
    char aperture[NAME_SIZE];
    char filter[NAME_SIZE];
    char grating[NAME_SIZE];
    char wavelength[NAME_SIZE];
    char lyot[NAME_SIZE];
    char lens[NAME_SIZE];
    char sector[NAME_SIZE];
    char slit[NAME_SIZE];
    char window[NAME_SIZE];
    char filter1[NAME_SIZE];
    char filter2[NAME_SIZE];
    float lambdaMin;
    float lambdaMax;
    float throughput;
} FLAM_CONFIG;


typedef struct
{
    char beamMode[NAME_SIZE];
    char window[NAME_SIZE];
    char decker[NAME_SIZE];
    char mosSlit[NAME_SIZE];
    char filter[NAME_SIZE];
    char lyot[NAME_SIZE];
    char grism[NAME_SIZE];
    char detPos[NAME_SIZE];
    char filter1[NAME_SIZE];
    char filter2[NAME_SIZE];
    long overrideWindow;
    long overrideMos;
    long overrideDecker;
    long overrideLyot;
    long overrideGrism;
    long overrideDetPos;
    long overrideDetBias;
    float lambdaMin;
    float lambdaMax;
    float throughput;
    char spare[NAME_SIZE];
} FLAM2_CONFIG;

/*
 *  Local data definitions
 */

static unsigned long flamCommandMask = 0;  /* command tracking word    */
static long flamIsDebugLevel = 0;          /* current debugging level  */

static char errorMessage[64];               /* error message buffer     */


/*
 *  ----------------- Public Access Functions ------------------
 */
    

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsAbortProcess
 * 
 * Purpose:
 * Check that it is safe abort an exposure in progress at this time and, 
 * if so, start the abort observation sequence.
 *
 * Invocation:
 * status = flamIsAbortProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long flamIsAbortProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_ABORT_OBS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_ABORT_OBS);

            if (TRX_CMD_CONFLICT(TRX_ABORT_OBS))
            {
                strncpy (pcr->mess,
                         "Command conflict, clear configuration", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_ABORT_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting abort command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsContinueProcess
 * 
 * Purpose:
 * Check that it is safe to continue an observation at this time and,
 * if so, signal that it can be continued.
 *
 * Invocation:
 * status = flamIsContinueProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2002/11/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsContinueProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_CONTINUE_OBS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_CONTINUE_OBS);

            if (TRX_CMD_CONFLICT(TRX_CONTINUE_OBS))
            {
                strncpy (pcr->mess,
                         "Can not mix configuration and observation", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_CONTINUE_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting Continue command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsDataModeProcess
 * 
 * Purpose:
 * Check that the requested data saving mode is valid and, if so, launch 
 * the data saving mode change sequence.
 *
 * Invocation:
 * status = flamIsDataModeProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired save or discard data flag
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated save or discard data flag
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsDataModeProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_DATA_MODE);
            break;

        case menuDirectiveSTOP:
            break;

        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:

            if ( strcmp (pcr->a, "save") == 0 ||
                 strcmp (pcr->a, "no-dhs") == 0 ||
                 strcmp (pcr->a, "discard") == 0 ||
                 strcmp (pcr->a, "discard-all") == 0 )
            {
                strcpy (pcr->vala, pcr->a);
            }
            else
            {
                strcpy (pcr->mess, "Data Mode must be: save/discard/discard-all/no-dhs");
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_DATA_MODE);

             DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting dataMode %s command\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsDatumProcess
 * 
 * Purpose:
 * Check that it is safe to send all devices to their reference positions
 * at this time and, if so, launch the system datumming sequence.
 * 
 * Invocation:
 * status = flamIsDatumProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsDatumProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_DATUM);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks the validity of the command
         */

        case menuDirectivePRESET:        

            TRX_MARK_COMMAND(TRX_DATUM);

            if (TRX_CMD_CONFLICT(TRX_DATUM))
            {
                strncpy (pcr->mess,
                         "Command conflict, clear configuration", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't datum while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:

            TRX_CLEAR_COMMAND(TRX_DATUM);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting datum command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsDebugProcess
 * 
 * Purpose:
 * Check that the requested debugging level is valid and, if so, launch
 * the system debugging level changing sequence.
 *
 * Invocation:
 * status = flamIsDebugProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired debug level
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated debug level
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  ! flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word

 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsDebugProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_DEBUG);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_DEBUG);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->a, "NONE") == 0)
            {
                flamIsDebugLevel = TRX_DEBUG_NONE;
            }

            else if (strcmp (pcr->a, "MIN") == 0)
            {
                flamIsDebugLevel = TRX_DEBUG_MIN;
            }

            else if (strcmp (pcr->a, "FULL") == 0)
            {
                flamIsDebugLevel = TRX_DEBUG_FULL;
            }

            else
            {
                strcpy (pcr->mess, "Debug must be [NONE|MIN|FULL]");
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_DEBUG);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: starting debug %s command\n", pcr->a);

            strcpy (pcr->vala, pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsInitProcess
 * 
 * Purpose:
 * Check that it is safe to re-initialize the controller at this time and, 
 * if so, launch the initialization sequence.
 *
 * Invocation:
 * status = flamIsInitProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired simulation level
 *      > pcr->b     string  system state
 *      > pcr->c     string  pvload file name
 *      > pcr->d     string  filter lookup file name 
 *      > pcr->e     string  device lookup file name
 *
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  simulation level
 *      < pcr->valb  string  pvload file name
 *      < pcr->valc  string  filter lookup file name
 *      < pcr->vald  string  device lookup file name
 *      < pcr->vale  string  simulation level for DC
 *      < pcr->valf  string  simulation level for CC
 *      < pcr->valg  string  simulation level for EC
 *      < pcr->valh  string  simulation level for ENG
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Allowed init in DISCONNECTED and UNINITIALIZED states
 * 2001/09/14  WNR  Changed DC initialization to match CC and EC.
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsInitProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    char simmLevel[8];

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_INIT);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_INIT);

            if (TRX_CMD_CONFLICT(TRX_INIT))
            {
                strncpy (pcr->mess,
                         "Clear configuration before initializing",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && 
                strcmp (pcr->b, "ERROR") &&
                strcmp (pcr->b, "UNINITIALIZED") &&
                strcmp (pcr->b, "ADJUSTING") &&
                strcmp (pcr->b, "DISCONNECTED"))
            {
                strcpy (errorMessage, "Can't initialize while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }


            /*
             *  Match input simulation level and reject if not valid.
             */

            if (strcmp (pcr->a, "NONE") == 0)
            {
                strcpy (simmLevel, "0");
            }

            else if (strcmp (pcr->a, "VSM") == 0)
            {
                strcpy (simmLevel, "1");
            }

            else if (strcmp (pcr->a, "FAST") == 0)
            {
                strcpy (simmLevel, "2");
            }

            else if (strcmp (pcr->a, "FULL") == 0)
            {
                strcpy (simmLevel, "3");
            }

            else
            {
                strncpy (pcr->mess, 
                         "Mode must be [NONE|VSM|FAST|FULL]",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }


            /*
             *  Copy to the outputs so that the sub-systems can
             *  check the validity as well.
             */

            /*
             *  Copy to the outputs so that the sub-systems can
             *  check the validity as well.
             */
                strcpy (pcr->vala, pcr->a);
                strcpy (pcr->valb, simmLevel);
                strcpy (pcr->valc, pcr->d);
                strcpy (pcr->vald, pcr->e);
                strcpy (pcr->vale, simmLevel);
                strcpy (pcr->valf, simmLevel);
                strcpy (pcr->valg, simmLevel);
                strcpy (pcr->valh, simmLevel);

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_INIT);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting %s simulation initialization\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsInsSetupProcess
 * 
 * Purpose:
 * Check that the requested optical element names are valid and that it
 * is safe to re-configure the elements in the optical path at this time.  
 * If so, launch the re-configuration sequence.
 *
 * Invocation:
 * status = flamIsInsSetupProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired beam mode
 *      > pcr->b     string  desired window name
 *      > pcr->c     string  desired mos slit name
 *      > pcr->d     string  desired decker name
 *      > pcr->e     string  desired filter name
 *      > pcr->g     string  desired lyot stop name
 *      > pcr->h     string  desired grism name
 *      > pcr->i     string  desired detector position name
 *      > pcr->h     string  desired bias mode
 *      > pcr->i     string  window override flag
 *      > pcr->j     string  mos override flag
 *      > pcr->k     string  decker override flag
 *      > pcr->l     string  lyot override flag
 *      > pcr->m     string  grism override flag
 *      > pcr->n     string  detector position override flag
 *      > pcr->o     string  bias override flag
 *
 *      > pcr->t     string  current instrument state
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated beam mode
 *      < pcr->valb  string  validated window name
 *      < pcr->valc  string  validated decker name
 *      < pcr->vald  string  validated mos slit name
 *      < pcr->vale  string  validated filter name
 *      < pcr->valf  string  translated decker name
 *      < pcr->valg  string  translated mos slit name
 *      < pcr->valh  string  translated filter 1 name
 *      < pcr->vali  string  translated filter 2 name
 *      < pcr->valh  string  translated lyot stop name
 *      < pcr->valj  string  translated grism name
 *      < pcr->valk  string  translated focus mode
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2002/01/10  WNR  Replaced filter and position lookup
 *                  code with final versions.
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsInsSetupProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    FLAM2_CONFIG config;        /* configuration structure          */
    char position[NAME_SIZE];   /* position string return           */
    long status;                /* function status return           */
    float lambdaMin;
    float lambdaMax;
    float throughput;
    int datumcnt = ufGetDatumCnt("sys");

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_INS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_INS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,"Can not mix setup and action commands",
                         	MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->t, "IDLE") && strcmp (pcr->t, "ERROR"))
            {
                strcpy (errorMessage, "Can't re-configure while ");
                strcat (errorMessage, pcr->t);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

	    /* require initial datum before first mechanism configuration */
            if( datumcnt == 0 ) {
                strncpy (pcr->mess, "Can't configure before first datum.", 
				MAX_STRING_SIZE);
                return CAD_REJECT;
            }
 
            /*
             *  Start by copying the input attributes into a configuration
             *  structure.
             */

            strcpy (config.beamMode, pcr->a);
            strcpy (config.window, pcr->b);
            strcpy (config.mosSlit, pcr->c);
            strcpy (config.decker, pcr->d);
            strcpy (config.filter, pcr->e);
            strcpy (config.lyot, pcr->g);
            strcpy (config.grism, pcr->h);
            strcpy (config.detPos, pcr->i);

            /*
             *  Now analyze the configuration...
             */

            /*
             *  beam mode overrides detector position and lyot stop
             */

            if (strcmp (config.beamMode, "f/16") == 0)
            {
                strcpy (config.detPos, "f/16");
                strcpy (config.lyot,   "f/16");
            }

            else if (strcmp (config.beamMode, "MCAO_over") == 0)
            {
                strcpy (config.detPos, "MCAO");
                strcpy (config.lyot,   "MCAO_over");
            }

            else if (strcmp (config.beamMode, "MCAO_under") == 0) 
            {
                strcpy (config.detPos, "MCAO");
                strcpy (config.lyot,   "MCAO_under");
            }

            else
            {
                strncpy (pcr->mess,
                         "beam mode not [f/16 | MCAO_over | MCAO_under]",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            /*
             *  wheel bias mode overrides decker, mos and grism selection
             */

/*             if (strcmp(config.wheelBiasMode, "imaging") == 0)
            {
                strcpy(config.decker,    "Open/Image");
                strcpy(config.mosSlit,   "imaging");
                strcpy(config.grism,     "open");
                strcpy(config.lyot,      "f/16");
                strcpy(config.wheelBiasMode, "imaging");
            }

            else if (strcmp(config.wheelBiasMode, "long_slit") == 0)
            {
                strcpy(config.decker,   "LongSlit");
                strcpy(config.wheelBiasMode, "spectroscopy");
            }

            else if (strcmp(config.wheelBiasMode, "mos") == 0)
            {
                strcpy(config.decker,   "MOS");
                strcpy(config.wheelBiasMode, "spectroscopy");
            }

            else
            {
                strncpy (pcr->mess,
                         "wheeel bias mode not [imaging | long_slit | mos]",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }  */

            /*
             *  check for manual overrides.  
             */
/*
	    config.overrideWindow = FALSE;
            if (strcmp (pcr->j, "TRUE") == 0)
            {
                strcpy(config.window, pcr->c);
	        config.overrideWindow = TRUE;
            }


            config.overrideMos = FALSE;
            if (strcmp (pcr->k, "TRUE") == 0)
            {
                strcpy(config.mosSlit, pcr->e);
	        config.overrideMos = TRUE;
            }


	    config.overrideDecker = FALSE;
            if (strcmp (pcr->l, "TRUE") == 0)
            {
                strcpy(config.decker, pcr->d);
	        config.overrideDecker = TRUE;
            }

	    config.overrideLyot = FALSE;
            if (strcmp (pcr->m, "TRUE") == 0)
            {
                strcpy(config.lyot, pcr->g);
	        config.overrideLyot = TRUE;
            }

	    config.overrideGrism = FALSE;
            if (strcmp (pcr->n, "TRUE") == 0)
            {
                strcpy (config.grism, pcr->h);
	        config.overrideGrism = TRUE;
            }

	    config.overrideDetPos = FALSE;
            if (strcmp (pcr->o, "TRUE") == 0)
            {
                strcpy (config.detPos, pcr->i);
	        config.overrideDetPos = TRUE;
            }

	    config.overrideDetBias = FALSE;
            if (strcmp (pcr->p, "TRUE") == 0)
            {
                strcpy (config.wheelBiasMode, pcr->b);
	        config.overrideDetBias = TRUE;
            }   
*/


            /*
             *  Split filter name into two filter wheel components and
             *  select the appropriate window via the filter lookup
             *  table;
             */

            status = flamFilterLookup (config.filter,
					config.filter1,
					config.filter2,
					config.spare);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter 0: ");
                 strcat (errorMessage, config.filter);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }

            config.throughput = 1.0;
            config.lambdaMin = 0.0;
            config.lambdaMax = 1000000000000.0;


            status = flamPositionLookup ("Filter1",
                                          config.filter1,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter 1: ");
                 strcat (errorMessage, config.filter1);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = flamPositionLookup ("Filter2",
                                          config.filter2,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized filter 2: ");
                 strcat (errorMessage, config.filter2);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = flamPositionLookup ("Grism",
                                          config.grism,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized grism: ");
                 strcat (errorMessage, config.grism);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = flamPositionLookup ("Lyot",
                                          config.lyot,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized Lyot: ");
                 strcat (errorMessage, config.lyot);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            status = flamPositionLookup ("MOS",
                                          config.mosSlit,
                                          position,
                                          &throughput,
                                          &lambdaMin,
                                          &lambdaMax);
            if (status)
            {
                 strcpy (errorMessage, "unrecognized mos slit: ");
                 strcat (errorMessage, config.mosSlit);
                 strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE); 
                 return CAD_REJECT;
            }
            config.throughput *= throughput;
            if (lambdaMin > config.lambdaMin) config.lambdaMin = lambdaMin;
            if (lambdaMax < config.lambdaMax) config.lambdaMax = lambdaMax;

            /*
             *  Everything is valid, update the input fields so that
             *  the user sees the results of all the translations and the
             *  output fields so that the sub-systems can check
             *  the results as well.
             */

            strcpy (pcr->vala, config.beamMode);
            strcpy (pcr->valb, config.window);
            strcpy (pcr->valc, config.mosSlit);
            strcpy (pcr->vald, config.decker);
            strcpy (pcr->vale, config.filter);
            strcpy (pcr->valg, config.lyot);
            strcpy (pcr->valh, config.grism);
            strcpy (pcr->vali, config.detPos);

            sprintf(pcr->valj, "%ld", config.overrideWindow);
            sprintf(pcr->valk, "%ld", config.overrideMos);
            sprintf(pcr->vall, "%ld", config.overrideDecker);
            sprintf(pcr->valm, "%ld", config.overrideLyot);
            sprintf(pcr->valn, "%ld", config.overrideGrism);
            sprintf(pcr->valo, "%ld", config.overrideDetPos);
            sprintf(pcr->valp, "%ld", config.overrideDetBias);

            strcpy (pcr->valq, config.filter1);
            strcpy (pcr->valr, config.filter2);

            if (strcmp (config.window, pcr->c) != 0)
            {
                strcpy (pcr->c, config.window);
                db_post_events(pcr, &pcr->c, DBE_VALUE);
            }

            if (strcmp (config.decker, pcr->d) != 0)
            {
                strcpy (pcr->d, config.decker);
                db_post_events(pcr, &pcr->d, DBE_VALUE);
            }

            if (strcmp (config.mosSlit, pcr->e) != 0)
            {
                strcpy (pcr->e, config.mosSlit);
                db_post_events(pcr, &pcr->e, DBE_VALUE);
            }

            if (strcmp (config.filter, pcr->f) != 0)
            {
                strcpy (pcr->f, config.filter);
                db_post_events(pcr, &pcr->f, DBE_VALUE);
            }

            if (strcmp (config.lyot, pcr->g) != 0)
            {
                strcpy (pcr->g, config.lyot);
                db_post_events(pcr, &pcr->g, DBE_VALUE);
            }

            if (strcmp (config.grism, pcr->h) != 0)
            {
                strcpy (pcr->h, config.grism);
                db_post_events(pcr, &pcr->h, DBE_VALUE);
            }

            if (strcmp (config.detPos, pcr->i) != 0)
            {
                strcpy (pcr->i, config.detPos);
                db_post_events(pcr, &pcr->i, DBE_VALUE);
            }

            break;

        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_INS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting instrument re-configuration %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}
     
 

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsNullInit
 * 
 * Purpose:
 * Generic do-nothing CAD record initialization function.
 *
 * Invocation:
 * status = flamIsNullInit (cadRecord *pcr);
 *
 * Parameters in:
 * 
 * Parameters out:
 * 
 * Return value:
 *      < status     long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsNullInit
(
    cadRecord *pcr              /* cad record structure             */
)
{
    
    return OK;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsObsSetupProcess
 * 
 * Purpose:
 * Check that all requested configuration parameters are valid and that
 * it is safe to set up an observation configuration at this time.  If 
 * so, launch the observation configuration sequence.
 *
 * Invocation:
 * status = flamIsObsSetupProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired observing mode
 *      > pcr->b     string  desired photon collecting time
 *      > pcr->c     string  desired secondary throw setting
 *      > pcr->d     string  desired sky noise setting
 *      > pcr->e     string  desired sky background setting
 *      > pcr->f     string  desired sky airmass setting
 *      > pcr->g     string  desired temperature setting
 *      > pcr->h     string  desired emissivity setting
 *      > pcr->i     string  desired rotator rate setting
 *      > pcr->j     string  desired nod dwell time
 *      > pcr->k     string  desired nod settling time 
 *      > pcr->l     string  desired readout mode
 *      > pcr->m     string  desired save frequency 
 *      > pcr->n     string  desired frame time
 *      > pcr->o     string  desired auto temp update flag 
 *      > pcr->p     string  desired override nod parameters
 *      > pcr->q     string  desired override save frequency 
 *      > pcr->r     string  desired override frame time
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated observing mode
 *      < pcr->valb  string  validated photon collecting time
 *      < pcr->valc  string  validated secondary throw setting 
 *      > pcr->vald  string  validated sky noise setting
 *      > pcr->vale  string  validated sky background setting
 *      > pcr->valf  string  validated sky airmass setting
 *      > pcr->valg  string  validated temperature setting
 *      > pcr->valh  string  validated emissivity setting
 *      > pcr->vali  string  validated rotator rate setting
 *      > pcr->valj  string  spare 
 *      > pcr->valk  string  validated readout mode 
 *      > pcr->vall  string  nod dwell time override
 *      > pcr->valm  string  nod settling time override
 *      > pcr->valn  string  save frequency override
 *      > pcr->valo  string  frame time override 
 *      > pcr->valp  string  spare 
 *      > pcr->valq  string  spare 
 *      > pcr->valr  string  array temperature update flag
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Added secondary throw input
 * 2002/01/23  WNR  Removed flux background and flux noise inputs
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsObsSetupProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    double fval;
    char *endPtr;

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_OBS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_OBS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands",
                         MAX_STRING_SIZE);
                return CAD_REJECT;
            }


            /*
             *  Sanity check for exposure time
             */

            fval = strtod (pcr->a, &endPtr);
	    printf("exposure time (fval) %g \n",fval);
            if (*endPtr != NULL) 
            {
                strcpy (pcr->mess, "Exposure time must be a number");
                return CAD_REJECT;
            }

            /*
             *  Sanity check for number of reads
             */

            fval = strtod (pcr->b, &endPtr);
	    printf("number of reads (fval) %g \n",fval);
            if (*endPtr != NULL) 
            {
                strcpy (pcr->mess, "number of readout must be a number");
                return CAD_REJECT;
            }

            /*
             *  Sanity check for readout mode   
             */

            if (strcmp (pcr->c, "ENG") != 0 &&
                strcmp (pcr->c, "SCI") != 0)
            {
                strcpy (pcr->mess, "Readout mode must be [SCI|ENG]");
                return CAD_REJECT;
            }


            /* 
             *  Sanity check for Bias mode
             */

            if (strcmp (pcr->d, "imaging") != 0 &&
                strcmp (pcr->d, "LongSlit") != 0 &&
                strcmp (pcr->d, "mos") != 0)
            {
                strcpy (pcr->mess,"Bias mode not [imaging | long_slit | mos]");
		return CAD_REJECT;
            }


            /* 
             *  Sanity check for dither mode
             */

            if (strcmp (pcr->f, "stare") != 0 &&
                strcmp (pcr->f, "nod") != 0)
            {
                strcpy (pcr->mess, "Dithering mode must be [stare|nod]");
                return CAD_REJECT;
            }

            /* 
             *  Sanity check for temperature update enable 
             */

            if (strcmp (pcr->o, "TRUE") != 0 &&
                strcmp (pcr->o, "FALSE") != 0)
            {
                strcpy (pcr->mess, "Temp control must be [TRUE|FALSE]");
                return CAD_REJECT;
            }

            strcpy (pcr->vala, pcr->a);
            strcpy (pcr->valb, pcr->b);
            strcpy (pcr->valc, pcr->c);
            strcpy (pcr->vald, pcr->d);
            strcpy (pcr->vale, pcr->e);
            strcpy (pcr->valf, pcr->f);

            strcpy (pcr->valk, pcr->l);  /* readout mode */
            strcpy (pcr->vall, "");
            strcpy (pcr->valm, "");
            strcpy (pcr->valn, "");
            strcpy (pcr->valo, "");

            break;

        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_OBS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting observation re-configuration %c\n", ' ');

            if (strcmp (pcr->o, "TRUE") == 0) 
            {
                *(long *)(pcr->valr) = 0;
            }
            else
            {
                *(long *)(pcr->valr) = 1;
            }

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }

    return CAD_ACCEPT;
}

     
/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsObserveProcess
 * 
 * Purpose:
 * Check that the requested data label is valid and that it is safe to
 * start a new observation at this time.  If so, launch the start-of-
 * observation sequence.
 *
 * Invocation:
 * status = flamIsObserveProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired data label
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated data label
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsObserveProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_START_OBS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_START_OBS);

            if (TRX_CMD_CONFLICT(TRX_START_OBS))
            {
                strncpy (pcr->mess,
                         "Can not mix configuration and observation", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't expose while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

          
            /*
             *  how do we check the data mode???
             */

            strcpy (pcr->vala, pcr->a);

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_START_OBS);

	    semGive(semComputeWcs);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting observation %s\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}
       

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name: 
 * flamIsParkProcess
 * 
 * Purpose:
 * Check that it is safe to park all moving devices and leave the 
 * instrument in a shut-down condition at this time.  If so, launch the
 * parking sequence.
 *
 * Invocation:
 * status = flamIsParkProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsParkProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_PARK)
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_PARK);

            /*
             *  ***** command verification stuff goes here ******
             */

            if (TRX_CMD_CONFLICT(TRX_PARK))
            {
                strncpy (pcr->mess,
                         "Clear configuration before parking", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't park while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:

            TRX_CLEAR_COMMAND(TRX_PARK);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting park command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsPauseProcess
 * 
 * Purpose:
 * Reject all pause requests.
 *
 * Invocation:
 * status = flamIsPauseProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2002/11/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsPauseProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset rejects the command.
         */

        case menuDirectivePRESET:
            strncpy (pcr->mess,
                     "Exposures can not be paused!", MAX_STRING_SIZE);
            return CAD_REJECT;
            break;


        /*
         *  Start should never happen! 
         */

        case menuDirectiveSTART:
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name: 
 * flamIsRebootProcess
 * 
 * Purpose:
 * Check that it is safe to reboot the system at this time and, if so,
 * launch the system reboot sequence.
 *
 * Invocation:
 * status = flamIsRebootProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 * 
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsRebootProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_REBOOT);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks the input attributes 
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_REBOOT);

            if (TRX_CMD_CONFLICT(TRX_REBOOT))
            {
                strncpy (pcr->mess,
                         "Clear configuration before rebooting",
                          MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_REBOOT);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting reboot command %c\n", ' ');
 
            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsSetDhsInfoProcess
 * 
 * Purpose:
 * Check that the requested DHS quick-look stream enable is valid and, 
 * if so, launch stream request sequence.
 *
 * Invocation:
 * status = flamIsSetDhsInfoProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 *      > pcr->a     string  desired source quick-look stream enable 
 *      > pcr->b     string  desired refrernce quick-look stream enable 
 *      > pcr->c     string  desired diff1 quick-look stream enable 
 *      > pcr->d     string  desired diff2 quick-look stream enable 
 *      > pcr->e     string  desired signal quick-look stream enable 
 *      > pcr->f     string  desired signal_a quick-look stream enable 
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *      < pcr->vala  string  validated quick-look stream ID
 *      > pcr->valb  string  validated refrernce quick-look stream enable 
 *      > pcr->valc  string  validated diff1 quick-look stream enable 
 *      > pcr->vald  string  validated diff2 quick-look stream enable 
 *      > pcr->vale  string  validated signal quick-look stream enable 
 *      > pcr->valf  string  validated signal_a quick-look stream enable 
 *
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 * 2001/09/12  WNR  Added five more quick look enables.
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsSetDhsInfoProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{
    char bitmap[TRX_MAX_QL_STREAMS + 1];
    char *pField;
    int i;

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_DHS_INFO);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:

            /*
             * Insure that quick look stream enable inputs all have
             * valid values.  At the same time pack the individual
             * enable flags into a single bitmap string that will be
             * sent to the DC.  There are a total of 14 possible quick-look
             * streams.  The cad record fields are used to enable or
             * disable each of these streams individually by mapping the
             * .A field to first output and so on...  Writing
             * the string "enable" to the field will tell the detector 
             * controller to send the associated data to the pre-defined
             * quick-look stream.  Writing the string "disable" will
             * tell the detector controller to stop sending data on the
             * associated stream. 
             */

             pField = pcr->a;

             for (i = 0; i < TRX_MAX_QL_STREAMS; i++)
             {
                 if (strcmp(pField, "enable") == 0)
                 {
                     bitmap[i] = '1';
                 }
                 else if (strcmp(pField, "disable") == 0)
                 {
                     bitmap[i] = '0';
                 }
                 else
                 {
                     sprintf (pcr->mess, "%s %d %s",
                          "QL stream", i, "must be [enable | disable]");
                     return CAD_REJECT;
                 }

                 pField++;
             }

             bitmap[i] = '\0';

             strcpy (pcr->vala, bitmap);

             break;


        /*
         *  Start executes the command by triggering the start link 
         */
 
        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_DHS_INFO);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting setDhsInfo %s command\n", pcr->a);

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsSetWcsProcess
 * 
 * Purpose:
 * Check that it is safe to update the world coordinate system information
 * at this time and, if so, launch the WCS updating sequence.
 *
 * Invocation:
 * status = flamIsSetWcsProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsSetWcsProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_SET_WCS);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_SET_WCS);

            if (TRX_CMD_CONFLICT(TRX_SETUP))
            {
                strncpy (pcr->mess,
                         "Can not mix setup and action commands", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:
            TRX_CLEAR_COMMAND(TRX_SET_WCS);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting setWcs command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsStopProcess
 * 
 * Purpose:
 * Check that it is safe to stop an observation in progress at this time 
 * and, if so, launch the stop exposure sequence.
 *
 * Invocation:
 * status = flamIsStopProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsStopProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_STOP_OBS);
            break;

        case menuDirectiveSTOP:
            break;

        /*
         *  Preset checks command validity
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_STOP_OBS);

            if (TRX_CMD_CONFLICT(TRX_STOP_OBS))
            {
                strncpy (pcr->mess,
                         "Clear configuration before stopping", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:

            TRX_CLEAR_COMMAND(TRX_STOP_OBS);
            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting stop command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 * flamIsTestProcess
 * 
 * Purpose:
 * Check that it is safe to test the entire system at this time 
 * and, if so, launch the testing sequence.
 *
 * Invocation:
 * status = flamIsTestProcess (cadRecord *pcr);
 *
 * Parameters in:
 *      > pcr->dir   long    command directive
 * 
 * Parameters out:
 *      < pcr->mess  string  status message
 *  
 * Return value:
 *      < status     long    CAD_ACCEPT or CAD_REJECT
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  > flamIsDebugLevel       module debugging level flag
 *  ! flamCommandMask        command tracking word
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/11  WNR  Template coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long flamIsTestProcess
(
    cadRecord *pcr              /* cad record structure             */
)
{

    /*
     *  Process according to the directive given
     */

    switch (pcr->dir)
    {
        /*
         *  Mark, Clear and Stop do nothing so can be accepted immediately
         */

        case menuDirectiveMARK:
            break;

        case menuDirectiveCLEAR:
            TRX_CLEAR_COMMAND(TRX_TEST);
            break;

        case menuDirectiveSTOP:
            break;


        /*
         *  Preset checks command validity 
         */

        case menuDirectivePRESET:
            TRX_MARK_COMMAND(TRX_TEST);

            if (TRX_CMD_CONFLICT(TRX_TEST))
            {
                strncpy (pcr->mess,
                         "Clear configuration before testing", MAX_STRING_SIZE);
                return CAD_REJECT;
            }

            if (strcmp (pcr->b, "IDLE") && strcmp (pcr->b, "ERROR"))
            {
                strcpy (errorMessage, "Can't test while ");
                strcat (errorMessage, pcr->b);
                strncpy (pcr->mess, errorMessage, MAX_STRING_SIZE);           
                return CAD_REJECT;
            }
            break;


        /*
         *  Start executes the command by triggering the start link 
         */

        case menuDirectiveSTART:

            TRX_CLEAR_COMMAND(TRX_TEST);

            DEBUG(TRX_DEBUG_FULL,
                  "<%d> %s: Starting test command %c\n", ' ');

            break;

        default:
            strncpy (pcr->mess,
                     "Invalid directive received", MAX_STRING_SIZE);
            return CAD_REJECT;
    }
     
    return CAD_ACCEPT;
}






