#if !defined(__UFCATEST_cc__)
#define __UFCATEST_h__ "$Id: ufcatest.cc 14 2008-06-11 01:49:45Z hon $"
static const char rcsId[] = __UFCATEST_h__;
#include "UFCAwrap.h"
__UFCAwrap_H__(__UFCAwrap_cc__)

//#include "iostream.h"

int main(int argc, char** argv) {
  cout<<"I am running scared"<<endl;
  if(argc <= 1 ) {
    clog<<UFCAwrap::usage<<endl;
    return 1;
  }

  UFCAwrap cat(argc, argv); // ctor inits channel access, must check for errors
  if( cat.hasErrors ) {
    cerr<<"aborting, init. channel access failed..."<<endl;
    return 2;
  }

  string* namelist= 0;
  string* vallist= 0;
  int num = cat.cmdLine(namelist, vallist);
  cout << "The number of variables is: "<< num ;

  if( cat.checkOpt("-g") ) cat.get(namelist, vallist, num);

  if( cat.checkOpt("-p") ) {
    cat.put(namelist, vallist, num);
    cat.get(namelist, vallist, num);
  }
  //cat.get("miri:tempCont:array", 1);
  // this enters infinite event loop
  if( cat.checkOpt("-m") ) {
    cat.mon(namelist, num);
  }
  return 0;
}

#endif //  __UFCATEST_cc__ 
