#if !defined(__UFDHSwrap_h__)
#define __UFDHSwrap_h__ "$Id: UFDHSwrap.h,v 0.0 2002/06/03 17:42:20 hon beta $"
#define __UFDHSwrap_H__(arg) const char arg##DHSwrap_h__rcsId[] = __UFDHSwrap_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx

#endif // __UFDHSwrap_h__
