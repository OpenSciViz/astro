#include "string"
//#include "vector"
//#include "stdio.h"
#include "UFCAwrap.h"

int main(int argc, char** argv) {
  
  string line;
  char motor_index ;
  string position_str ;
  double position ;
  string record_name ;
  int error = 0 ;
  string* namelist= 0;
  string* vallist= 0;
  line  ="" ;
  UFCAwrap cawrap(argc, argv);
  if( cawrap.hasErrors ) {
    cerr<<"aborting, init. channel access failed..."<<endl;
    exit(2);
  }
  while (line != "exit") {
    getline(cin, line);
    cout << "you typed : " << line << " " << line.length() << endl ;
    if (line.length() > 5) {
      motor_index = line[0] ;
      position_str = line.substr(5,line.length()-5) ;
      cout << motor_index << " " << position_str << endl ;
      record_name = "trecs:" ;
      switch (motor_index) {
        case 'a' : record_name = "trecs:sectWhl:currPos" ;
                   break ;
        case 'b' : record_name = "trecs:winChngr:currPos" ;
                   break ;
        case 'c' : record_name = "trecs:aprtrWhl:currPos" ;
                   break ;
        case 'd' : record_name = "trecs:fltrWhl1:currPos" ;
                   break ;
        case 'e' : record_name = "trecs:lyotWhl:currPos" ;
                   break ;
        case 'f' : record_name = "trecs:fltrWhl2:currPos" ;
                   break ;
        case 'g' : record_name = "trecs:pplImg:currPos" ;
                   break ;
        case 'h' : record_name = "trecs:slitWhl:currPos" ;
                   break ;
        case 'i' : record_name = "trecs:grating:currPos" ;
                   break ;
        case 'j' : record_name = "trecs:coldClmp:currPos" ;
                   break ;
        default  : cout << "Motor index is not valid" << endl ;	  
                   error = 1 ;
      }
      if (error == 0) {
        position = atof(position_str.c_str()) ; 
        cout << record_name << " " << position << endl;
        namelist = &record_name ;
        if (position_str[0] == ' ') {
	  position_str = position_str.substr(1,position_str.length()-1);
        }
        vallist = &position_str ;
        int num = 1;
        cawrap.get(namelist, num);
        cawrap.put(namelist, vallist, num);
        cawrap.get(namelist, num);
      }
    }
  }
  return 0;
}
