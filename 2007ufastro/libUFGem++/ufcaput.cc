#if !defined(__UFCAput_cc__)
#define __UFCAput_cc__ "$Name:  $ $Id: ufcaput.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAput_cc__;

#include "string"
#include "UFDaemon.h"
#include "UFCAwrap.h"

// this & UFCAWrap are in a bad need of rewrite to use vector< string >& instead of
// pointers. UFCAWrap would benefit from inheritance from UFDaemon...
// note that the ctor for UFCAwrap will attempt a connection to epics chan. acc. service:
class UFCAput : public UFDaemon, public UFCAwrap {
public:
  inline UFCAput(int argc, char** argv) : UFDaemon(argc, argv), UFCAwrap(argc, argv),
					  _once(""),
                                          _verbose(false), _confirm(false), _notrunc(false) {}
  inline virtual ~UFCAput() {}
  inline virtual string description() const { return string(rcsId); }

  static int main(int argc, char** argv);
  virtual void* exec(void* p = 0);
  static int readline(string& line); // read stdin

  string _once;

private:
  bool _verbose;
  bool _confirm;
  bool _notrunc;
};

int UFCAput::main(int argc, char** argv) {
  UFCAput ufc(argc, argv);
  if( argc > 1 ) ufc._once = argv[1];
  ufc.exec();
  return 0;
}

int UFCAput::readline(string& line) { // read stdin
  line = "";
  char c[2] = "\n";
  do {
    read(0, c, 1);
    line += c;
  } while( c[0] != '\n' );

  return (int)c[0];
}

void* UFCAput::exec(void* p) {
  // check if epics channel acc. connect failed
  if( UFCAwrap::hasErrors > 0 ) {
    clog<<"aborting, init. channel access failed..."<<endl;
    return 0;
  }
  string arg;
  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  arg = findArg("-notrunc");
  if( arg != "false" )
    _notrunc = true;

  string myname = (*_args)[0];
  string instrm_name= "flam:";
  string record_name= instrm_name+"heartbeat";
  string *namelist= 0, *vallist= 0;
  string puts;
  time_t t = time(0);
  strstream s;
  s<<"flam:heartbeat.inp="<<t<<ends;
  string line= s.str(); delete s.str(); // just 1 at a time for now

  // check for one-time put:
  arg = findArg("-p");
  if( arg != "false" && arg != "true" ) {
    line = puts = arg;
  }
  else if( arg == "true" ) { // no value provided for option, use heartbeat=clock
    puts = line;
  }
  else { // allow -p to be default option:
    if( _once.find("=") != string::npos ) {
      puts = line = _once;
    }
  } 
  // or expect input from stdin...
  while ( !cin.eof() ) {
    if( puts != line ) {
      getline(cin, line);
    }
    else if( line.find("=") == string::npos || line.find("quit") == 0 || line.find("exit") == 0) {
      //if( _verbose ) clog<<"UFCAput> quit..."<<endl;
      return 0;
    }
    if( _verbose ) 
      clog<<"UFCAput> '" << line << "'" << endl;
    string putlist;
    if( line != "" ) {
      // line should really be split up into "-p drec=val" pairs... 
      putlist = myname + " -p " + line; // for now assume single put
      int num = UFCAwrap::cmdLine(putlist, namelist, vallist); // alloc. & return name & val lists
      if( num <= 0 ) {
	clog<<"UFCAput> ?badly formmatted input, no dbrec specified?"<<endl;
	return 0;
      }
      put(namelist, vallist, num);
      if( _confirm ) {
        string* vnamelist= 0;
        string* vvallist= 0;
        int vnum = UFCAwrap::cmdLine(putlist, vnamelist, vvallist); // alloc. & return  name & val lists
	get(vnamelist, vvallist, vnum);
	if( _verbose ) {
	  for( int i = 0; i<num; ++i )
	    clog<<"UFCAput> confirm(got): "<<vnamelist[i]<<"='" <<vvallist[i]<<"'"<<endl;
	}
      }
    }
    if( puts == line ) // assume -p option, all done
      return 0;
  }
  if( _verbose ) clog<<"UFCAput> quit..."<<endl;
  return 0;
}

int main(int argc, char** argv) {
  return UFCAput::main(argc, argv);
}

#endif //  __UFCAput_cc__
