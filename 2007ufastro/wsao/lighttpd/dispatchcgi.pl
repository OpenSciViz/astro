#!/usr/local/bin/perl -w
use strict;
use CGI::Fast;
use Embed::Persistent;
{
  my $p = Embed::Persistent->new();
  while ( my $fcgi = new CGI::Fast) {
    my $filename = $ENV{SCRIPT_FILENAME};
    my $package = $p->valid_package_name($filename);
    my $mtime;
    print $fcgi->header; # $fcgi->header("text/plain");
    print $fcgi->start_html("Fast CGI: ENV{SCRIPT_FILENAME} = $ENV{SCRIPT_FILENAME}");
    print $fcgi->body("filename = $filename, package = $package, mtime = $mtime");
    print $fcgi->end_html;
#    sleep 1;
#    if ($p->cached($filename, $package, \$mtime)) {
#      eval {$package->handler;};
#    }
#    else {
      $p->eval_file($ENV{SCRIPT_FILENAME});
#    }
  }
}
