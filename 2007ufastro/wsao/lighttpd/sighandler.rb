#!/usr/local/bin/ruby
# signal handling example
def sethandler( sig )
  trap(sig) { print "sig = ",sig,"\n"; if sig == "SIGINT"; exit; end; }
  return;
end

#inthandler = proc{ puts "^C was pressed."; exit; }
#trap "SIGINT", inthandler;
#trap("SIGTSTP") { puts "^Z was pressed."; exit; }

sethandler("SIGINT");
sethandler("SIGTSTP");

while true
  print "waiting for signal...\n";
  sleep 0.5;
end
