#!/usr/bin/perl -w

use strict;
use MT::Bootstrap;
use CGI::Fast;

# preload app packages
use MT::App::CMS;
use MT::App::Comments;
use MT::App::Trackback;
use MT::App::Search;
## uncomment if necessary, but this adds a lot of
## overhead since it loads up LibXML.
##use MT::AtomServer;


my $handlers = {
    'mt.fcgi' => { class => 'MT::App::CMS', name => 'AdminScript' },
    'mt-comments.fcgi' => { class => 'MT::App::Comments', name => 'CommentScript' },
    'mt-tb.fcgi' => { class => 'MT::App::Trackback', name => 'TrackbackScript' },
    'mt-search.fcgi' => { class => 'MT::App::Search', name => 'SearchScript' },
## See note above about this...
##    'mt-atom.fcgi' => { class => 'MT::AtomServer', name => 'AtomScript' },
};

eval {
    while (my $q = new CGI::Fast) {
        my $cgi = $q->script_name;
        $cgi =~ s!.*/!!;
        my $pkg = $handlers->{$cgi}{class};
        die "Invalid handler for $cgi" unless $pkg;
        my $app = $pkg->new(CGIObject => $q) or die $pkg->errstr;
        local $SIG{__WARN__} = sub { $app->trace($_[0]) };
        $app->init_request(CGIObject => $q) unless $app->{init_request};
        fixup_script_names($app);
        $app->run;
        my $mode = $app->mode || '';
        if ("$pkg->$mode" eq 'MT::App::CMS->plugin_control') {
            exit; # allows server to recycle after changing plugin switches
        }
    }
};
if ($@) {
    print "Content-Type: text/html\n\n";
    print "Got an error: $@";
}

sub fixup_script_names {
    my ($app) = @_;
    $app->config($handlers->{$_}{name}, $_) foreach keys %$handlers;
}
