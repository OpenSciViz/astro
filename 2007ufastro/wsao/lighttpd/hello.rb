#!/usr/local/bin/ruby
rcsId = '$Name:  $ $Id: hello.rb 14 2008-06-11 01:49:45Z hon $';
require 'cgi'

# create a cgi object, with HTML 4 generation methods.
cgi = CGI.new("html4")
# use cgi object to send some text out to the browser.
cgi.out {
  cgi.html {
    cgi.body { cgi.h1 { "Hello Ruby!" } }
  }
}
