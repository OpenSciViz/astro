#!/usr/local/bin/ruby
#!/bin/env EPICS_CA_AUTO_ADDR_LIST=NO EPICS_CA_ADDR_LIST=hon00grid /usr/local/bin/ruby
rcsId = '$Name:  $ $Id: fuedt.rb 14 2008-06-11 01:49:45Z hon $';
require 'cgi'
require 'UFCA'

# create a cgi object, with HTML 4 generation methods.
cgi = CGI.new("html4")
# use cgi object to send some text out to the browser.

chid = UFCA.connectPV('fu:sad:EDTFRAME', 0.5)
#UFCA.putInt(chid, 200)
edt = UFCA.getInt(chid)
sedt = UFCA.getAsString(chid)
#UFCA.putInt(chid, 500)
#edt = UFCA.getInt(chid)

cgi.out {
  cgi.html {
    cgi.body { cgi.h1 { "EPICS status from fu:sad:EDTFRAME == "+sedt } }
  }
}
