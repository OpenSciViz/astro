#if !defined(__UFBytes_h__)
#define __UFBytes_h__ "$Name:  $ $Id: UFBytes.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFBytes_H__(arg) const char arg##Bytes_h__rcsId[] = __UFBytes_h__;

#include "UFTimeStamp.h"

class UFBytes: public virtual UFTimeStamp {
protected:

  inline UFBytes() {}

public:

  UFBytes(const UFProtocolAttributes& pa);

  UFBytes(bool shared, int length=_MinLength_);

  UFBytes(const string& name, bool shared=false, int length=_MinLength_);

  UFBytes(const string& name, char* vals, int elem=1, bool shared=false);

  UFBytes(const string& name, const char* vals, int elem=1, bool shared=false);

  UFBytes(const string& name, UFProtocolAttributes* pa, int elem, bool shared=true);

  UFBytes(const UFBytes& rhs);

  inline virtual ~UFBytes() {}

  inline virtual string description() const
    { return string(__UFBytes_h__); }

  inline virtual int valSize(int elemIdx=0) const
    { return sizeof(char); }

  inline virtual const char* valData(int elemIdx=0) const
    { return (const char*) &((char*)_pa->_values)[elemIdx]; }

  virtual void deepCopy(const UFProtocol& rhs);

 inline virtual void copyVal(char* dst,int elemIdx=0)
    { memcpy(dst, &((char*)_pa->_values)[elemIdx], sizeof(char)); }  

  virtual int writeValues(int fd) const;

  virtual int readValues(int fd); 

  virtual int writeTo(int fd) const; 

  virtual int readFrom(int fd);

  virtual int sendValues(UFSocket& soc) const;

  virtual int recvValues(UFSocket& soc);

  virtual int sendTo(UFSocket& soc) const; 

  virtual int recvFrom(UFSocket& soc);
};

#endif // __UFBytes_h__


/**
 * Bytes Protocol Msg contains one or more (variable) length ints
 */
  /**
   * Only allow subclasses to call default constructor.
   */
  /**
   * Instantiate a UFBytes object given the UFProtocolAttributes object.
   * This is the constructor used to initialize the UFBytes object by the
   * class factory.
   * @param pa The attributes for initializing this object
   * @return Nothing
   */
  /**
   * Constructor for shared memory applications
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor performs a shallow copy of the vals array.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param elem The length of the array, default to 1
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and an array of values.
   * This method performs a shallow copy of vals
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @param shared True if this object resides in shared memory, false otherwise
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param pa The UFProtocolAttributes object from which to copy data
   * @param elem The length of the array
   * @param shared True if this object resides in shared memory, false otherwise
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * Copy constructor, performs a shallow copy of the given UFShorts object.
   * @param rhs The source UFBytes object to copy into this object.
   */
  /**
   * UFProtocol dtor uses switch/case factory.
   */
  /**
   * Return a description of the current object.
   * This method is meant to be overloaded by subclasses.
   * @return A copy of a std::string containing the object's description.
   */
  /**
   * Return size of an element of the values array
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return The size of the element at index elemIdx
   */
  /**
   * Return the value of the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return const char* pointer to first byte of _values (data).
   */
  /**
   * Perform a full value copy of rhs into the current object's UFProtocolAttribute object.
   * @param rhs The source UFProtocol object from which to obtain new values.
   * @see UFprotocol
   * @see UFProtocolAttribute
   */
  /**
   * Copy internal values to external buff
   * @param dst The destination buffer to which to write the value
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return Nothing
   */
  /**
   * Write out only the values array to the given file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out the internal data representation to file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes output on success
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Write the internal data representation out to a socket
   * @return 0 on failure, number of bytes written on success
   * @param soc The UFSocket to which to write
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
