#if !defined(__UFTimeStamp_h__)
#define __UFTimeStamp_h__ "$Name:  $ $Id: UFTimeStamp.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFTimeStamp_H__(arg) const char arg##TimeStamp_h__rcsId[] = __UFTimeStamp_h__;

#include "string"

#include "UFProtocol.h"
#include "ctime"

using std::string ;

class UFTimeStamp: public virtual UFProtocol {
protected:
  inline UFTimeStamp() {}
  void _currentTime();

public:
  UFTimeStamp(const UFProtocolAttributes& pa);
  UFTimeStamp(bool shared, int length=_MinLength_);
  UFTimeStamp(const char* name, bool shared=false, int length=_MinLength_);
  UFTimeStamp(const string& name, bool shared=false, int length=_MinLength_);

  inline virtual ~UFTimeStamp() {}

  inline virtual string description() const { return string(__UFTimeStamp_h__); }

  inline virtual int valSize(int elemIdx=0) const { return 0; }

  inline virtual const char* valData(int elemIdx=0) const { return 0; }

  inline virtual void copyVal(char* dst, int elemIdx=0) {}

  virtual void deepCopy(const UFProtocol& rhs);

  virtual int writeTo(int fd) const; 

  virtual int writeValues(int fd) const;

  virtual int readFrom(int fd);

  virtual int readValues(int fd); 

  virtual int sendTo(UFSocket& soc) const; 

  virtual int sendValues(UFSocket& soc) const;

  virtual int recvFrom(UFSocket& soc);

  virtual int recvValues(UFSocket& soc);
};

#endif // __UFTimeStamp_h__


/**
 * Simplest realization of the abstract protocal class,
 * timestamps are essentially empty messages
 * Note that timestamp "value" operations are NOOP's.
 * @see UFProtocol
 */
  /**
   * The default constructor is hidden to support the ObjectFactory pattern
   */
  /**
   * Obtain the current UTC time from UFRuntime::currentTime()
   */
  /**
   * Instantiate a UFTimeStamp object given the UFProtocolAttributes object.
   * This is the constructor used to initialize the UFTimeStamp object by the
   * class factory.
   * @param pa The attributes for initializing this object
   * @return Nothing
   */
  /**
   * Constructor for shared memory applications
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * Destructor.  Since no heap space has been allocated, this is a NOOP.
   */
  /**
   * Return a description of the current object.
   * This method is meant to be overloaded by subclasses.
   * @return A copy of a std::string containing the object's description.
   */
  /**
   * Return the size of the element at elemIdx (not it's name!)
   * @param elemIdx The element index, default to 0
   * @return Either string length, or sizeof(float), sizeof(frame).
   * This method is meant to be overloaded by subclasses.
   * @return TimeStamp returns 0.
   */
  /**
   * Return the value of the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return const char* pointer to first byte of _values (data), but TimeStamps have none.
   * @return NULL for UFTimeStamp
   * This method is meant to be overloaded by subclasses.
   */
  /**
   * Copy internal values to external buff: NOOP for UFTimeStamp
   * @param dst The destination buffer to which to write the value
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return Nothing
   * This method is meant to be overloaded by subclasses.
   */
  /**
   * Perform a full value copy of rhs into the current object's UFProtocolAttribute object.
   * @param rhs The source UFProtocol object from which to obtain new values.
   * @see UFprotocol
   * @see UFProtocolAttribute
   */
  /**
   * Write out the internal data representation to file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes output on success
   */
  /**
   * Write out only the values array to the given file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   * @return UFTimeStamp always returns 0 since there is no data array.
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   * @return UFTimeStamp always returns 0 since there is no data array.
   */
  /**
   * Write the internal data representation out to a socket
   * @return 0 on failure, number of bytes written on success
   * @param soc The UFSocket to which to write
   * @see UFSocket
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
