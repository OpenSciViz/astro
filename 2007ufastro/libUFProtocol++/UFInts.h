#if !defined(__UFInts_h__)
#define __UFInts_h__ "$Name:  $ $Id: UFInts.h,v 0.21 2006/04/26 16:22:46 hon Exp $"
#define __UFInts_H__(arg) const char arg##Ints_h__rcsId[] = __UFInts_h__;

#include "UFTimeStamp.h"

class UFInts: public virtual UFTimeStamp {
protected:
  inline UFInts() {}

public:
  UFInts(const UFProtocolAttributes& pa);
  UFInts(bool shared, int length=_MinLength_);
  UFInts(const string& name, bool shared=false, int length=_MinLength_);
  UFInts(const string& name, int* vals,
	 int elem=1, bool shared=false, int* indexMap=0, bool invertMap=false);
  UFInts(const string& name, int* vals, int* indexMapFrom, int* indexMapTo, int elem=1 );
  UFInts(const string& name, int elem=1, bool shared=false);
  
  // const int* indicates shallow values ptr assignment
  UFInts(const string& name, const int* vals, int elem=1, bool shared=false);
  UFInts(const string& name, UFProtocolAttributes* pa, int elem, bool shared=true);
  UFInts(const UFInts& rhs);

  inline virtual ~UFInts() {}

  UFInts& operator+=( const UFInts& rhs );
  UFInts& operator-=( const UFInts& rhs );
  UFInts& operator*=( const UFInts& rhs );
  UFInts& operator/=( const UFInts& rhs );

  int sum(const UFInts& Left, const UFInts& Right );
  int diff(const UFInts& Left, const UFInts& Right );

  friend UFInts* operator+( const UFInts& Left, const UFInts& Right );
  friend UFInts* operator-( const UFInts& Left, const UFInts& Right );

  inline virtual string description() const { return string(__UFInts_h__); }
  inline virtual int valSize(int elemIdx=0) const { return sizeof(int); }

  inline virtual const char* valData(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return (char*) &((int*)_pa->_values)[elemIdx]; }
  inline int* valInts(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return &((int*)_pa->_values)[elemIdx]; }
  inline int valInt(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return ((int*)_pa->_values)[elemIdx]; }
  inline int operator[](int elemIdx) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return valInt(elemIdx); }

  int reverseByteOrder();
  int maxVal();
  int minVal();

  //void minmax( int& minVal, int& maxVal, int& minIndex, int& maxIndex );
  virtual void deepCopy(const UFProtocol& rhs);
  inline virtual void copyVal(char* dst, int elemIdx= 0) {
    if( elemIdx < 0 || elemIdx >= elements() ) return; 
    memcpy(dst, &((int*)_pa->_values)[elemIdx], sizeof(int));
  }  

  virtual int writeValues(int fd) const;
  virtual int readValues(int fd); 
  virtual int writeTo(int fd) const; 
  virtual int readFrom(int fd);
  virtual int sendValues(UFSocket& soc) const;
  virtual int recvValues(UFSocket& soc);
  virtual int sendTo(UFSocket& soc) const; 
  virtual int recvFrom(UFSocket& soc);
};

#endif // __UFInts_h__


/**
 * Ints Protocol Msg contains one or more (variable) length ints
 */
  /**
   * Default constructor is hidden to support the object factory Pattern.
   */
  /**
   * Instantiate a UFInts object given the UFProtocolAttributes object.
   * This is the constructor used to initialize the UFInts object by the
   * class factory.
   * @param pa The attributes for initializing this object
   * @return Nothing
   */
  /**
   * Constructor for shared memory applications
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @param shared True if this object resides in shared memory, false otherwise
   * @param indexMap If pointer indexMap is nonzero (default is zero),
   * then the array vals are copied with reindexing via index map.
   * @param invertMap TBD
   * @return Nothing
   */
  /**
   * Deep copy ctor of vals array, with reindexing elements using From and To maps
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param indexMapFrom TBD
   * @param indexMapTo TBD
   * @param elem The length of the array, default to 1
   * @return Nothing
   */
  /**
   * Create a new UFInts object with a values array of size elem, all zero initially.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param elem The length of the array, default to 1
   * @param shared True if this object resides in shared memory, false otherwise
   */
  /**
   * This constructor performs a shallow copy of the vals array.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @param shared True if this object resides in shared memory, false otherwise
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param pa The UFProtocolAttributes object from which to copy data
   * @param elem The length of the array
   * @param shared True if this object resides in shared memory, false otherwise
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * Copy constructor, performs a shallow copy of the given UFInts object.
   * @param rhs The source UFInts object to copy into this object.
   */
  /**
   * Perform usual operator+= on each element of the values array.
   *@param rhs The source object whose values are to be added
   *@return The updated current object (lvalue)
   */
  /**
   * Perform usual operator-= on each element of the values array.
   *@param rhs The source object whose values are to be added
   *@return The updated current object (lvalue)
   */
  /**
   * Perform usual operator*= on each element of the values array.
   *@param rhs The source object whose values are to be added
   *@return The updated current object (lvalue)
   */
  /**
   * Perform usual operator/= on each element of the values array.
   *@param rhs The source object whose values are to be added
   *@return The updated current object (lvalue)
   */
  /**
   * Perform usual operator+= on each element of the two arrays
   * given, and stores the results in this object's values array.
   * @param Left The first object whose values are to be used as source
   * @param Right The second object whose values are to be used as source
   *@return The number of elements computed
   */
  /**
   * Perform usual operator-= on each element of the two arrays
   * given, and stores the results in this object's values array.
   * @param Left The first object whose values are to be used as source
   * @param Right The second object whose values are to be used as source
   *@return The number of elements computed
   */
  /**
   * Create a new UFInts object and perform usual operator+= on each element
   * of the two arrays given, and stores the results in the new object's values array.
   * @param Left The first object whose values are to be used as source
   * @param Right The second object whose values are to be used as source
   *@return The new UFInts object, or NULL on failure.
   */
  /**
   * Create a new UFInts object and perform usual operator+= on each element
   * of the two arrays given, and stores the results in the new object's values array.
   * @param Left The first object whose values are to be used as source
   * @param Right The second object whose values are to be used as source
   *@return The new UFInts object, or NULL on failure.
   */
  /**
   * Rely on base class destructor(s).
   */
  /**
   * Return a description of the current object.
   * This method is meant to be overloaded by subclasses.
   * @return A copy of a std::string containing the object's description.
   */
  /**
   * Return size of an element of the values array
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return The size of the element at index elemIdx
   */
  /**
   * Return the value of the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return const char* pointer to first byte of _values (data).
   */
  /**
   * Return a pointer to the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return A pointer to the element at elemIdx.
   */
  /**
   * Return the value of the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return The value of the element at elemIdx
   */
  /**
   * Reverse the byte ordering of each element of the values array
   *@return The number of bytes swapped.
   */
  /**
   * Return the maximum value of all elements in the values array
   *@return The maximum value of all elements in the values array
   */
  /**
   * Return the minimum value of all elements in the values array
   *@return The minimum value of all elements in the values array
   */
  /**
   * Perform a full value copy of rhs into the current object's UFProtocolAttribute object.
   * @param rhs The source UFProtocol object from which to obtain new values.
   * @see UFprotocol
   * @see UFProtocolAttribute
   */
  /**
   * Copy internal values to external buff
   * @param dst The destination buffer to which to write the value
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return Nothing
   */
  /**
   * Write out only the values array to the given file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out the internal data representation to file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes output on success
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Write the internal data representation out to a socket
   * @return 0 on failure, number of bytes written on success
   * @param soc The UFSocket to which to write
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
