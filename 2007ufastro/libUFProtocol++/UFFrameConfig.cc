#if !defined(__UFFrameConfig_cc__)
#define __UFFrameConfig_cc__ "$Name:  $ $Id: UFFrameConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFrameConfig_cc__;

#include "UFFrameConfig.h"

void UFFrameConfig::_init(int w, int h, int d, bool little, int dmaCnt, int imageCnt,
			  int coAdds, int pixelSort, int frameObsSeqNo, int frameObsSeqTot,
	                  int ChopBeam, int SaveSet, int NodBeam, int NodSet, 
			  int offset, int pixcnt, int* vals) {

  Status* _data = (Status*) vals;
  
  if( vals == 0 && _pa->_values == 0 ) { // must allocate
    _data = new Status;
    _pa->_values = (void*) _data;
    _pa->_elem = statusElements();
  }
  else if( vals != (void*) 0 && _pa->_values != 0 ) { // user provided allocation
    delete [] ((int*) _pa->_values);
    _pa->_values = (void*) _data;
    _pa->_elem = statusElements();
  }
  else if( vals == (void*) 0 && _pa->_values != 0 && _pa->_elem == statusElements() ) {
    _data = (Status*) _pa->_values;
  }
  else {
    clog<<"UFFrameConfig::_init> ? wrong allocation."<<endl;
    return;
  }

  _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
  
  if( _pa->_elem != statusElements() ) {
    clog << "UFFrameConfig::_init> ? ctor error in _elem= " << elements()
	 << ", statusElements= " << statusElements() <<endl;
    return;
  }

  _data->littleEnd = little ? 1 : 0;
  _data->w= w; _data->h= h; _data->d= d;
  _data->coAdds= coAdds; _data->pixelSort= pixelSort;
  _data->frameObsSeqNo= frameObsSeqNo; _data->frameObsSeqTot= frameObsSeqTot;
  _data->ChopBeam= ChopBeam; _data->SaveSet= SaveSet;
  _data->NodBeam= NodBeam;   _data->NodSet= NodSet;
  _data->frameWriteCnt= 0;   _data->frameSendCnt= 0;
  _data->bgADUs= 0;   _data->bgWellpc= 0.0;   _data->sigmaFrmNoise= 0.0;
  _data->offADUs= 0;   _data->offWellpc= 0.0;   _data->sigmaReadNoise= 0.0;
  _data->frameCoadds= 0; _data->chopSettleFrms= 0; _data->chopCoadds= 0;
  _data->frameTime= 0.0; _data->savePeriod= 0.0;
  _data->bgADUmin= 0; _data->bgADUmax= 0; _data->sigmaFrmMin= 0.0; _data->sigmaFrmMax= 0.0;
  _data->rdADUmin= 0; _data->rdADUmax= 0; _data->sigmaReadMin= 0.0; _data->sigmaReadMax= 0.0;

  if( offset < 0 || offset >= w*h ) offset = 0;
  _data->offset= offset;
  
  if( pixcnt <= 0 || pixcnt >= w*h ) pixcnt = w*h;
 _data->pixcnt = pixcnt;

  _currentTime();
}

// Deep vals copy ctor.
// If pointer vals is null, it just set values to zeros.

UFFrameConfig::UFFrameConfig( const string& name, const char* vals,
			      const string& timeStamp, bool shared ) : UFTimeStamp() {
  _paInit(_FrameConfig_, name);
  _shared = shared;
  _pa->_elem = statusElements();
  _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(int);
  _pa->_values = (void*) new (nothrow) int[_pa->_elem];

  if( _pa->_values == 0 ) {
    clog<<"UFFrameConfig (deep vals copy ctor)> new alloc failed."<<endl;
    _pa->_elem = 0;
    _pa->_length = minLength();
    return;
  }
  
  if( vals )
    ::memcpy( _pa->_values, vals, _pa->_elem*sizeof(int) );
  else
    ::memset( _pa->_values, 0, _pa->_elem*sizeof(int) );

  stampTime(timeStamp);
}

// static functions
int UFFrameConfig::writeConfig(int fd, const UFFrameConfig& c) {
  return c.writeTo(fd);
}

int UFFrameConfig::readConfig(int fd, UFFrameConfig& c) {
  return c.readFrom(fd); // resets c
}

int UFFrameConfig::sendConfig(UFSocket& soc, const UFFrameConfig& c) {
  return c.sendTo(soc);
}

// for use in server
int UFFrameConfig::recvConfig(UFSocket& soc, UFFrameConfig& c) {
  return c.recvFrom(soc); // resets c
}

// for use in client
int UFFrameConfig::reqConfig(UFSocket& soc, UFFrameConfig& c) {
  // negative duration is used to ID the transaction as a request
  int nb = c.sendAsReqTo(soc);
  if( nb <= 0 ) { 
    clog<<"UFFrameConfig::reqConfig> ? unable to send request to server."<<endl;
    return nb;
  }
  return c.recvFrom(soc); // resets c
}

// for use in client
int UFFrameConfig::getUpdatedBuffNames(vector< string >& bufnames, const string& delim) {
  bufnames.clear();
  string fcname = name();
  size_t dpos = fcname.find(delim);
  if( dpos == string::npos )
    return 0;

  size_t pos = 0;
  string name = fcname.substr(pos, dpos);
  bufnames.push_back(name);
  pos = 1 + dpos;
  dpos = fcname.find(delim, pos);
  while( dpos != string::npos ) {
    name = fcname.substr(1+dpos, dpos-pos);
    bufnames.push_back(name);
    pos = 1 + dpos;
    dpos = fcname.find(delim, pos);
  }
  if( pos < fcname.length() - 1 ) { // last element in string should not have trailing delimitor
    name = fcname.substr(pos);
    bufnames.push_back(name);
  }
  return (int) bufnames.size();
}

// for use in client
int UFFrameConfig::getUpdatedBuffNames(const string& instrum, vector< string >& bufnames, const string& delim) {
  int n = getUpdatedBuffNames(bufnames, delim);
  if( n <= 0 )
    return 0;

  for( int i = 0; i < n; ++i )
    bufnames[i] = instrum + ":" + bufnames[i];

  return n;
}

// for use in server
int UFFrameConfig::setUpdatedBuffNames(const vector< string >& bufnames, const string& delim) {
  int cnt = (int) bufnames.size();
  if( cnt <= 0 )
    return 0;

  string newname = bufnames[0];
  for( int i = 1; i < cnt; ++i )
    newname += delim + bufnames[i];

  rename(newname);
  return cnt;
}

#endif // UFFrameConfig
    
