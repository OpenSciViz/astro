#if !defined(__UFProtocol_cc__)
#define __UFProtocol_cc__ "$Name:  $ $Id: UFProtocol.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFProtocol_cc__;

#include "algorithm" // c++ STL min, max, etc.

#include "UFProtocol.h"
__UFProtocol_H__(Protocol_cc);
// create functions must have access to subclass ctors!
#include "UFTimeStamp.h"
__UFTimeStamp_H__(Protocol_cc);
#include "UFStrings.h"
__UFStrings_H__(Protocol_cc);
#include "UFBytes.h"
__UFBytes_H__(Protocol_cc);
#include "UFShorts.h"
__UFShorts_H__(Protocol_cc);
#include "UFInts.h"
__UFInts_H__(Protocol_cc);
#include "UFFloats.h"
__UFFloats_H__(Protocol_cc);
#include "UFFrames.h"
__UFFrames_H__(Protocol_cc);
#include "UFFrameConfig.h"
__UFFrameConfig_H__(Protocol_cc);
#include "UFObsConfig.h"
__UFObsConfig_H__(Protocol_cc);
#include "UFFlamObsConf.h"
__UFFlamObsConf_H__(Protocol_cc);

// global:
bool UFProtocol::_verbose = false;
bool UFProtocol::_sendverbose = false;
bool UFProtocol::_recvverbose = false;

// reference timestamp format
static const char _thetimeformat[] = "yyyy:ddd:hh:mm:ss.uuuuuu";

// hidden mutex
static pthread_mutex_t _creatmutex;
//static pthread_mutex_t _stdlibmutex;

////////////////////////////////////// UFProtocolAttributes public //////////////////////////////////////
UFProtocolAttributes::~UFProtocolAttributes() {}

UFProtocolAttributes::UFProtocolAttributes() : _length(0),
                                               _type(UFProtocol::_TimeStamp_),
                                               _elem(0), 
                                               _seqCnt(1),
                                               _seqTot(1),
                                               _duration(0.0),
                                               _values((void*) 0) {
  strcpy(_timestamp,_thetimeformat );
  ::memset(_name, 0, sizeof(_name)); // insure null termination
}

// shallow copy ctor()
UFProtocolAttributes::UFProtocolAttributes(const UFProtocolAttributes& rhs) { *this = rhs; }
 
UFProtocolAttributes& UFProtocolAttributes::operator=(const UFProtocolAttributes& rhs) {
  _length = rhs._length;
  _type = rhs._type; 
  _elem = rhs._elem;
  _seqCnt = rhs._seqCnt;
  _seqTot  = rhs._seqTot;
  _duration  = rhs._duration;
  _values = rhs._values; // shallow assign!

  // in case rhs has improper timestamp
  if( isdigit(rhs._timestamp[0]) && strlen(rhs._timestamp) == strlen(_thetimeformat) )
    strcpy(_timestamp, rhs._timestamp);

  // don't change my name if already set?
  if( strlen(_name) == 0 )
    strncpy(_name, rhs._name, sizeof(_name)-1);

  return *this; 
}


// this provides the "in memory size" of the header
// please do not confuse this with the "transmission length"
int UFProtocolAttributes::headerSize() {
  size_t retval = sizeof(struct UFProtocolAttributes);
  return static_cast< int >( retval );
}

/////////////////////////////////////// UFProtocol public //////////////////////////////////////

// although timestamp is a string, it is fixed length, so don't transmit "slen"
int UFProtocol::headerLength(const UFProtocolAttributes& pa) {
  int hl = sizeof(pa._length)
    + sizeof(pa._type)
    + sizeof(pa._elem)
    + sizeof(pa._seqCnt)
    + sizeof(pa._seqTot)
    + sizeof(pa._duration)
    + (sizeof(pa._timestamp)-1)
    + sizeof(int)
    + strlen(pa._name);
  return hl;
}

int UFProtocol::sizeOf(const vector< UFProtocol* >& m) {
  vector< UFProtocol* >::size_type nm = m.size();
  int sz=0;
  for( vector< UFProtocol* >::size_type i=0; i<nm; i++ ) {
    // clog << "UFProtocol::sizeOf> item " << i << " name= "
    // << m[i]->name() << " " << m[i]->length() << endl;
    sz += m[i]->size();
  }
  return sz ;
}

int UFProtocol::lengthOf(const vector< UFProtocol* >& m) {
  vector< UFProtocol* >::size_type nm = m.size();
  int sz=0;
  for( vector< UFProtocol* >::size_type i=0; i<nm; i++ ) {
    // clog << "UFProtocol::sizeOf> item " << i << " name= "
    // << m[i]->name() << " " << m[i]->length() << endl;
    sz += m[i]->length();
  }
  return sz ;
}

int UFProtocol::elementsOf(const vector< UFProtocol* >& m) {
  vector< UFProtocol* >::size_type nm = m.size();
  int el=0;
  for( vector< UFProtocol* >::size_type i=0; i<nm; i++ ) {
    el += m[i]->elements();
  }
  return el;
}


int UFProtocol::typeId() const {
  if( _pa )
    return _pa->_type;

  return -1;
}

int UFProtocol::resetId(int typ) {
  if( _pa ) {
    _pa->_type = typ;
    return _pa->_type;
  }

  return -1;
}

// total length of the message:
int UFProtocol::length() const {
  if( _pa )
    return _pa->_length;

  return -1;
}

// fetch name
const char* UFProtocol::cname() const {
  if( _pa )
    return _pa->_name;
  return 0;
}

string UFProtocol::name() const {
  if( _pa )
    return string(_pa->_name); 

  return string("error: _pa=NULL");
}

// reset name
string UFProtocol::rename(const string& newname) {
  if( _pa ) {
    string orig = _pa->_name;
    size_t slen = orig.length();
    if( _verbose )
      clog<<"UFProtocol::rename> _length: "<<_pa->_length<<endl;
    _pa->_length -= slen;
    if( _verbose )
      clog<<"UFProtocol::rename> _length: "<<_pa->_length<<endl;
    memset( _pa->_name, 0, sizeof( _pa->_name ) );
    slen = min(sizeof(_pa->_name)-1, newname.length());
    strncpy(_pa->_name, newname.c_str(), slen);
    _pa->_length += slen;
    if( _verbose )
      clog<<"UFProtocol::rename> _length: "<<_pa->_length<<endl;
    return orig;
  }
  return string("error: _pa=NULL");
}

/// dataLabel is any substring at end of name string that is demarked by '||'
/// (if present) i.e. name == "whichever || whatever || dataLabel"
string UFProtocol::datalabel() {
  if( _pa == 0 )
    return string("error: _pa=0");  
  string label = _pa->_name;
  size_t pos = label.rfind("||");
  if( pos == string::npos )
    return string("");
  pos += 2;
  if( pos >= label.length() )
    return string("");
  return label.substr(pos);
}

/// reset the dataLabel if present, or init. it
string UFProtocol::relabel(const string& label) {
  if( _pa == 0 )
    return string("error: _pa=0");  
  string orig = _pa->_name;
  size_t slen = orig.length();
  string newnam = _pa->_name;
  size_t pos = newnam.rfind("||");
  //clog<<"UFProtocol::relabel> || pos: "<<pos<<", name: "<<newnam<<endl;
  if( pos == string::npos ) {
    //clog<<"UFProtocol::relabel> || not found..."<<endl;
    newnam += "||"; newnam += label;
    memset( _pa->_name, 0, sizeof( _pa->_name ) ) ;
    _pa->_length -= slen;
    slen = min(sizeof(_pa->_name)-1, newnam.length());
    strncpy(_pa->_name, newnam.c_str(), slen);
    _pa->_length += slen;
    //clog<<"UFProtocol::relabel> inserted || and renamed with datalabel append, typId: "<<typeId()<<endl;
    //clog<<"UFProtocol::relabel> name: "<<name()<<", datalabel: "<<datalabel()<<endl;  
    return string("");
  }

  pos += 2;
  if( pos >= sizeof(_pa->_name) ) {
    orig = "";
  }
  else {
    orig = newnam.substr(pos);
    newnam = newnam.substr(0, pos);
  }

  newnam += label;
  memset( _pa->_name, 0, sizeof( _pa->_name ) ) ;
  _pa->_length -= slen;
  slen = min(sizeof(_pa->_name)-1, newnam.length());
  strncpy(_pa->_name, newnam.c_str(), slen);
  _pa->_length += slen;
  //clog<<"UFProtocol::relabel> renamed with datalabel append, typeId: "<<typeId()<<endl;
  //clog<<"UFProtocol::relabel> orig: "<<orig<<", new name: "<<name()<<", datalabel: "<<datalabel()<<endl;
  return orig;
}

// fetch timestamp
const char* UFProtocol::timestamp() const {
  if( _pa )
    return _pa->_timestamp;
  return 0;
}

string UFProtocol::timeStamp() const {
  if( _pa )
    return string(_pa->_timestamp);
  return string("error: _pa=NULL");
}

// reset timestamp
string UFProtocol::stampTime(const string& newtime) {
  if( _pa == 0 )
    return string("error: _pa=NULL");

  char* nt = (char*)newtime.c_str();
  size_t tlen = strlen(_thetimeformat);
  if( isdigit(nt[0]) && newtime.length() == tlen) {
    strncpy(_pa->_timestamp, newtime.c_str(), tlen );
    return newtime;
  }

  return string(_pa->_timestamp);
}

// fetch element count
int UFProtocol::elements() const {
  if( _pa )
    return _pa->_elem;
  return -1;
}

int UFProtocol::numVals() const {
  if( _pa )
    return _pa->_elem;
  return -1;
}

unsigned short UFProtocol::seqCnt() const {
  if( _pa )
    return _pa->_seqCnt;
  return 0;
}
 
unsigned short UFProtocol::seqTot() const {
  if( _pa )
    return _pa->_seqTot;
  return 0;
}

void UFProtocol::setSeq(unsigned short cnt, unsigned short tot) {
  if( _pa ) {
    if( tot > 0 ) _pa->_seqTot = tot;
    _pa->_seqCnt = cnt;
  }
}

float UFProtocol::duration() const {
  if( _pa )
    return _pa->_duration;
  return 0;
}

float UFProtocol::setDuration(float d) {
  if( _pa )
    return _pa->_duration = d;
  return 0;
}

int UFProtocol::writeFully(int fd, const unsigned char* buf, size_t sz) {
  //try a few times: EAGAIN, EINTR
  int attempts = 3;
  int nbresult = 0, nbtowrite = sz;
  do {
    nbresult = ::write(fd, (const void*)(buf + nbresult), nbtowrite);
    nbtowrite -= nbresult;
  } while( --attempts >= 0 && nbresult >= 0 && nbtowrite > 0 && 
           (errno == 0 || errno == EINTR || errno == EAGAIN) );

  // quit: ENOSPC no free space left on the device.
  //       EDQUOT - no quota, EFBIG - too big a file
  //       ENOLCK, EDEADLK - file locked by another
  if( nbtowrite > 0 || nbresult < 0 ) {
    switch(errno) {

    case ENOSPC: // fall thru
    case EDQUOT:
      clog << "UFProtocol::writeFully> disk full or quota "
	   << "exceeded on write"<<endl;
      break;
      
    case ENOLCK: // fall thru
    case EDEADLK:
      clog << "UFProtocol::writeFully> file locked by another, "
	   << "can't write"<<endl;
      break;
    default:
      clog << "UFProtocol::writeFully> unknown error occured "
	   << "on write" << endl;
      break ;
    }
    // Return error condition?
    return -1 ;
  }
  // read() successful
  return (sz - nbtowrite);
}

int UFProtocol::readFully(int fd, unsigned char* buf, size_t sz) {
  // try a few times: EAGAIN, EINTR
  unsigned short attempts = 0 ;
  int nbresult = 0, nbtoread = sz;
  do {
    nbresult = ::read(fd, (void*)(buf + nbresult), nbtoread);
    nbtoread -= nbresult;
  } while( ++attempts <= 3 && nbresult >= 0 && nbtoread > 0 && 
           (errno == 0 || errno == EINTR || errno == EAGAIN) );

  // quit: ENOLCK, EDEADLK - file locked by another
  if( nbtoread > 0 || nbresult < 0 ) {
    switch(errno) {
      case ENOLCK: // fall thru
      case EDEADLK:
	clog << "UFProtocol::readFully> file locked by another, "
	     << "can't write" << endl;
	break;
      default:
	clog << "UFProtocol::readFully> unknown error occured "
	     << "on read" << endl;
	break ;
    }
    // Return an error condition?
    return -1;
  }

  // read() successful
  return (sz - nbtoread);
}

int UFProtocol::writeHeader(int fd, const UFProtocolAttributes& pa) {
  // read "persistent" attributes in order : pa._length, pa._type, etc.
  if( fd < 0 ) {
    clog<<"UFProtocol::writeHeader> bad fd= "<<fd<<endl;
    return fd;
  }

  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFProtocol::writeHeader> bad stat on fd= "<<fd<<endl;
    return -1;
  }
  int rb = writeFully(fd, (const unsigned char*) &pa._length,
		      sizeof(pa._length));
  rb += writeFully(fd, (const unsigned char*) &pa._type,
		   sizeof(pa._type));
  rb += writeFully(fd, (const unsigned char*) &pa._elem,
		   sizeof(pa._elem));
  rb += writeFully(fd, (const unsigned char*) &pa._seqCnt,
		   sizeof(pa._seqCnt));
  rb += writeFully(fd, (const unsigned char*) &pa._seqTot,
		   sizeof(pa._seqTot));
  rb += writeFully(fd, (const unsigned char*) &pa._duration,
		   sizeof(pa._duration));

  // presumably ctor set timestamp== "yyyy:ddd:hh:mm:ss.uuuuuu"
  int slen = sizeof(pa._timestamp)-1;

  // dont need to write timestamp length if it is fixed
  // fixed-length timestamp
  rb += writeFully(fd, (const unsigned char*) pa._timestamp, slen);

  // do need to write out name length
  slen = strlen(pa._name);

  rb += writeFully(fd, (const unsigned char*) &slen, sizeof(slen));
  rb += writeFully(fd, (const unsigned char*) pa._name, slen);
  
  return rb;
}

int UFProtocol::readHeader(int fd, UFProtocolAttributes& pa) {
  // read "persistent" attributes in order : pa._length, pa._type, etc...
  if( fd < 0 ) {
    clog<<"UFProtocol::readHeader> bad fd= "<<fd<<endl;
    return fd;
  }

  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFProtocol::readHeader> bad stat on fd= "<<fd<<endl;
    return -1;
  }
  int retval = 0;
  int rb = retval = readFully(fd, (unsigned char*) &pa._length,
			      sizeof(pa._length));
  if( retval <= 0 ) return retval;

  rb += retval = readFully(fd, (unsigned char*) &pa._type,
			   sizeof(pa._type));
  if( retval <= 0 ) return retval;

  rb += retval = readFully(fd, (unsigned char*) &pa._elem,
			   sizeof(pa._elem));
  if( retval <= 0 ) return retval;

  rb += retval = readFully(fd, (unsigned char*) &pa._seqCnt,
			   sizeof(pa._seqCnt));
  if( retval <= 0 ) return retval;

  rb += retval = readFully(fd, (unsigned char*) &pa._seqTot,
			   sizeof(pa._seqTot));
  if( retval <= 0 ) return retval;

  rb += retval = readFully(fd, (unsigned char*) &pa._duration,
			   sizeof(pa._duration));
  if( retval <= 0 ) return retval;

  // presumably ctor set timestamp== "yyyy:ddd:hh:mm:ss.uuuuuu"
  int slen = sizeof(pa._timestamp)-1;

  // dont need to read timestamp length if it is fixed
  // fixed-length timestamp
  rb += retval = readFully(fd, (unsigned char*) pa._timestamp, slen);
  if( retval <= 0 ) return retval;

  // do need to read name length
  rb += retval = readFully(fd, (unsigned char*) &slen, sizeof(slen));
  if( retval <= 0 ) return retval;

  if( slen > 0 )
  {
    int truncate = 0;
    // name must be truncated to fit:
    if( slen >= (int) sizeof(pa._name) )
      truncate = 1 + sizeof(pa._name) - slen; 
    rb += retval = readFully(fd, (unsigned char*) pa._name, slen - truncate);
    if( retval <= 0 ) return retval;

    if( truncate > 0 ) { // finish off the read
      unsigned char* tmp = new (nothrow) unsigned char[truncate];
      if( NULL == tmp ) clog << "UFProtocol::readHeader> Memory allocation failure\n" ;
      rb += retval = readFully(fd, tmp, truncate);
      delete [] tmp;
      if( retval <= 0 ) return retval;
    }
  }
  
  if( rb < headerLength(pa) ) {
    clog << "UFProtocol::readHeader>? bad readFully(rb= "
	 << rb << ") on fd= " << fd << endl;
  }
  
  return rb;
}

void UFProtocol::printHeader(const UFProtocolAttributes* pa) {
  // print "persistent" attributes in order : pa->_length, pa->_type, etc.
  clog << "UFProtocol::printHeader>" ;
  clog << " length=" << pa->_length << ", type=" << pa->_type << ", #elem=" << pa->_elem
       << ", seqCnt=" <<pa->_seqCnt<<", seqTot="<<pa->_seqTot<<", duration="<<pa->_duration
       << ", values="<<pa->_values<<endl;
  clog << "UFProtocol::printHeader> time: "<<pa->_timestamp<<", name: "<<pa->_name <<endl;
}

int UFProtocol::sendHeader(UFSocket& soc, const UFProtocolAttributes& pa) {
  if( _verbose || _sendverbose )
    printHeader(&pa);

  int retval = 0;
  // send "persistent" attributes in order : pa._length, pa._type, pa._elem,..
  int rb = retval = soc.send(pa._length);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send(pa._type);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send(pa._elem);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send((short)pa._seqCnt);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send((short)pa._seqTot);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send(pa._duration);
  if( retval <= 0 ) return retval;

  // presumably ctor set timestamp== "yyyy:ddd:hh:mm:ss.uuuuuu"
  int slen = sizeof(pa._timestamp)-1;

  // dont need to write timestamp length if it is fixed
  // fixed-length timestamp
  rb += retval = soc.send((const unsigned char*) pa._timestamp, slen);
  if( retval <= 0 ) return retval;

  slen = strlen(pa._name); // do need to write out name length
  rb += retval = soc.send(slen);
  if( retval <= 0 ) return retval;

  rb += retval = soc.send((const unsigned char*) pa._name, slen);
  if( retval < 0 ) return retval;
  if( slen > 0 && retval == 0 ) return retval;

  if( rb < headerLength(pa) ) {
    clog << "UFProtocol::sendHeader>? bad send (rb= "
	 << rb << ") on socket= " << soc.description() << endl;
  }
  if( _verbose ) 
    clog << "UFProtocol::sendHeader> sent " << rb
         << " bytes on socket= " << soc.description() << endl;

  return rb;
}

int UFProtocol::recvHeader(UFSocket& soc, UFProtocolAttributes& pa) {
  int retval= 0, trycnt= 3;
  float to = -1; // infinite timeout (blocking)
  to = soc.getTimeOut();
  //clog<<"UFProtocol::recvHeader> timeout: "<<to<<" check available on "<<soc.description()<<endl;
  // new version of availabel calls readable first...
  int avail = soc.available(to, trycnt);
  if( avail <= 0 ) {
    clog<<"UFProtocol::recvHeader> "<<avail<<" available on "<<soc.description()<<endl;   
    return avail;
  }
  if( _verbose || _recvverbose )
    clog<<"UFProtocol::recvHeader> "<<avail<<" available on "<<soc.description()<<endl;   

  // recv "persistent" attributes in order : pa._length, pa._type, pa._elem,..
  int rb = retval = soc.recv(pa._length);
  if( retval <= 0 ) {
    clog<<"UFProtocol::recvHeader>? bad length recv (rb= "
	<<rb<< ") on socket= "<<soc.description() << endl;
    return retval;
  }
  else if( pa._length > _MaxLength_ ) {
    clog<<"UFProtocol::recvHeader>? bad length recv (length= "
	<<rb<< ") on socket= "<<soc.description() << endl;
    return -1;
  }
  //clog << "UFProtocol::recvHeader> pa._length= "<<pa._length<<endl;
  rb += retval = soc.recv(pa._type);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._type= "<<pa._type<<endl;

  rb += retval = soc.recv(pa._elem);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._elem= "<<pa._elem<<endl;

  rb += retval = soc.recv((short&)pa._seqCnt);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._seqCnt= "<<pa._seqCnt<<endl;

  rb += retval = soc.recv((short&)pa._seqTot);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._seqTot= "<<pa._seqTot<<endl;

  rb += retval = soc.recv(pa._duration);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._duration= "<<pa._duration<<endl;

  // presumably ctor set timestamp== "yyyy:ddd:hh:mm:ss.uuuuuu"
  int slen = sizeof(pa._timestamp)-1; int tlen = strlen("yyyy:ddd:hh:mm:ss.uuuuuu");
  if( slen != tlen ) {
    clog << "UFProtocol::recvHeader> ctor set inproper tlen:"<<slen<<" should be "<<tlen<<endl;
    slen = tlen;
  }
  // dont need to recv timestamp length if it is fixed
  // fixed-length timestamp
  rb += retval = soc.recv((unsigned char*) pa._timestamp, slen);
  if( retval <= 0 ) return retval;
  //clog << "UFProtocol::recvHeader> pa._timestamp= "<<pa._timestamp<<endl;

  // do need to recv name length
  // but name length may be shorter than allocation
  rb += retval = soc.recv(slen);
  if( retval <= 0 ) return retval;

  if( slen > 0 ) {
    int truncate = 0;
    if( slen >= (int) sizeof(pa._name) ) // name must be truncated to fit:
       truncate = 1 + sizeof(pa._name) - slen; 
    rb += retval = soc.recv((unsigned char*) pa._name, slen - truncate);
    if( retval <= 0 ) return retval;
  
    if( truncate > 0 ) { // finish off the recv
       unsigned char tmp[(const int)truncate];
       //unsigned char* tmp = new unsigned char[truncate];
       rb += retval = soc.recv(tmp, truncate);
       //delete [] tmp;
       if( retval <= 0 ) return retval;
    }
  }
  //clog << "UFProtocol::recvHeader> pa._name= "<<pa._name<<endl;

  if( rb < headerLength(pa) ) {
    clog << "UFProtocol::recvHeader>? bad recv(rb= "
	 << rb << ") on socket= " << soc.description() << endl;
  }

  if( _verbose || _recvverbose )
    printHeader(&pa);

  return rb;
}

int UFProtocol::sendAsReqTo(UFSocket& soc) { 
  setDuration(-1.0);
  return sendHeader(soc, attributesRef());
}

UFProtocol* UFProtocol::createFrom(int fd) {
  UFProtocolAttributes pa;
  // Attempt to read the type and length header info
  int rb = readHeader(fd, pa);

  // Were we successful?
  if( rb < headerLength(pa) ) { // Error occured during read, return NULL
    clog << "UFProtocol::createFrom(fd)> bad header read? rb="<<rb
           <<" < headerLength="<<headerLength(pa)
           <<", tot.length="<<pa._length<<", elem="<<pa._elem<<", type="<<pa._type<<endl;
    return 0;
  }

  // Create a new object of the specified type
  UFProtocol* retval = create(pa);

  // Try to read the rest of the object
  // note that this must read length - sizeof(msgtyp) bytes!
  if( retval ) {
    rb += retval->readValues(fd);
    if( rb < pa._length ) {
      clog << "UFProtocol::createFrom(fd)> bad data read? rb="<<rb
           <<" < length="<<pa._length<<", elem="<<pa._elem<<", type="<<pa._type<<endl;
      //delete retval;
      //retval = 0;
    }
  }
  return retval;
}

UFProtocol* UFProtocol::createFrom(UFSocket& soc, float timeOut) {
  soc.setTimeOut(timeOut);
  UFProtocolAttributes pa;

  // Attempt to read the type and length header info
  int rb = recvHeader(soc, pa);
  //clog<<"UFProtocol::createFrom(soc)> recv'd header "<<pa._name<<endl;

  if( rb < headerLength(pa) ) { // Error occured while reading header info
    clog<<"UFProtocol::createFrom(soc)> bad header read? rb="<<rb
        <<" < headerLength="<<headerLength(pa)
        <<", tot.length="<<pa._length<<", elem="<<pa._elem<<", type="<<pa._type<<endl;
    return 0;
  }

  // Create an object of the given type and length
  UFProtocol* retval = create(pa);

  // Attempt to read the rest of the object
  // note that this must read length - sizeof(msgtyp) bytes!
  if( retval ) {
    //clog<<"UFProtocol::createFrom> recv. "<<retval->elements()<<" values for type: "<<retval->typeId()<<endl;
    rb += retval->recvValues(soc);
    if( rb < pa._length ) {
      clog << "UFProtocol::createFrom(soc)> bad data read? rb="<<rb
           <<" < length="<<pa._length<<", elem="<<pa._elem<<", type="<<pa._type<<endl;
    }
  }
  //clog<<"UFProtocol::createFrom(soc)> created type: "<<retval->typeId()<<", "<<retval->name()<<endl;
  return retval;
}

//////////////////////////////////  protected /////////////////////////////////////////////
// since default ctors do nothing, provide this
// helper for all ctors. this sets everything except 
// the _values ptr, which must be set by the ctor!

void UFProtocol::_paInit(int typ, const string& name, int len) {
  _shared = _shallow = false;
  _pa = new (nothrow) UFProtocolAttributes; // new default ctor 
  if( _pa == 0 ) {
    clog << "UFProtocol::_paInit> Memory allocation failure\n" ;
    return;
  }
  _pa->_type = typ;
  _pa->_length = len;
  int minlen = minLength();
  if( len < minlen )
     _pa->_length = minlen;

  if( name.length() != 0 ) {
    int nl = name.length(), nmax=sizeof(_pa->_name) - 1;
    if( nl < nmax ) 
      strcpy(_pa->_name, name.c_str());
    else // truncate the name to fit the allocation
      strncpy(_pa->_name, name.c_str(), nmax);
    if( _pa->_length == minlen )
      _pa->_length += strlen(_pa->_name);
  }
}

// helper for all ctors
void UFProtocol::_paInit(const UFProtocolAttributes& rhs) {
  _shared = _shallow = false;
  _pa = new (nothrow) UFProtocolAttributes; // new default ctor 
  if( _pa == 0 ) {
    clog << "UFProtocol::_paInit> Memory allocation failure\n" ;
    return;
  }
  *_pa = rhs;
}

// potentially useful for shallow copy ctors
void UFProtocol::_paInit(const UFProtocolAttributes* pa) {
  _shallow = true;
  _pa = const_cast< UFProtocolAttributes* >( pa );
}  

// potentially useful for shallow copy convertor
void UFProtocol::_paInit(int typ, const UFProtocolAttributes* pa) {
  _shallow = true;
  _pa = const_cast< UFProtocolAttributes* >( pa );
  if( _pa->_type != typ ) {
    clog<<"UFProtocol::_paInit> possible bad type conversion!"<<endl;
    _pa->_type = typ;
  }
}  

// constructor for the object "factory"
UFProtocol* UFProtocol::create(const UFProtocolAttributes& pa) {
  if( ::pthread_mutex_lock(&_creatmutex) < 0 ) { // not yert init'ed?
    if( errno == EINVAL ) 
      ::pthread_mutex_init(&_creatmutex, 0);
    // try one more time
    if( ::pthread_mutex_lock(&_creatmutex) < 0 )
      return 0;
  }
  UFProtocol* retval=0;
  bool zip = true;
  int msgtyp = pa._type;
  //clog<<"UFProtocol::create> type: "<<msgtyp<<endl;
  switch(msgtyp) {
    case _Strings_: zip = false;
    case _ZippedStrings_: retval = new (nothrow) UFStrings(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedStrings_);
         break;

    case _Bytes_: zip = false;
    case _ZippedBytes_: retval = new (nothrow) UFBytes(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedBytes_);
         break;

    case _Shorts_: zip = false;
    case _ZippedShorts_: retval = new (nothrow) UFShorts(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedShorts_);
         break;

    case _Ints_: zip = false;
    case _ZippedInts_: retval = new (nothrow) UFInts(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedInts_);
         break;

    case _Floats_: zip = false;
    case _ZippedFloats_: retval = new (nothrow) UFFloats(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedFloats_);
         break;

    case _FlamObsConf_: retval = new (nothrow) UFFlamObsConf(pa);
         break;

    case _ObsConfig_: retval = new (nothrow) UFObsConfig(pa);
         break;

    case _FrameConfig_: retval = new (nothrow) UFFrameConfig(pa);
         break;

    case _ByteFrames_: zip = false;
    case _ZippedByteFrames_: retval = new (nothrow) UFFrames(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedByteFrames_);
         break;

    case _ShortFrames_: zip = false;
    case _ZippedShortFrames_: retval = new (nothrow) UFFrames(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedShortFrames_);
         break;

    case _IntFrames_: zip = false;
    case _ZippedIntFrames_: retval = new (nothrow) UFFrames(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedIntFrames_);
         break;

    case _FloatFrames_: zip = false;
    case _ZippedFloatFrames_: retval = new (nothrow) UFFrames(pa);
         if( zip && retval != 0 ) retval->resetId(_ZippedFloatFrames_);
         break;

    case _TimeStamp_: zip = false; // default

    default: retval = new (nothrow) UFTimeStamp(pa);
         break;
  }
  ::pthread_mutex_unlock(&_creatmutex);
  //clog<<"UFProtocol::create> name: "<<retval->name()<<endl;
  return retval;
}

/////////////////////////// public (just keep it close to create so that switch statement is consistent) ///
// size & create functions should have congruent  switch/case statement in the protocol hierarchy
// for compressed _values, this should still indicate the decompression size; compression
// should only affect the _length (persistence) attribute used in transactions (network or file)
int UFProtocol::size() {
  int retval = _pa->headerSize();
  switch(_pa->_type) {
    case _ZippedStrings_:
    case _Strings_: for( int i = 0; i < elements(); i++ )
                      retval += (((string*)_pa->_values)[i]).size();
         break;

    case _ZippedBytes_:
    case _ByteFrames_: 
    case _Bytes_: retval += numVals()*sizeof(char);
         break;

    case _ZippedShorts_:
    case _ShortFrames_:
    case _ObsConfig_:
    case _Shorts_: retval += numVals()*sizeof(short);
         break;

    case _ZippedInts_:
    case _IntFrames_:
    case _FrameConfig_:
    case _Ints_: retval += numVals()*sizeof(int);
         break;

    case _ZippedFloats_:
    case _FloatFrames_:
    case _Floats_: retval += numVals()*sizeof(float);
         break;

    case _TimeStamp_: // default (has no _values)

    default:
         break;
  }

  return retval;
}

// don't require sub-classes to delete _values, just do it here, again there
// is should only be an enumerated (finite) set of subclasses, so use the switch/case:
UFProtocol::~UFProtocol() { _delete(); }

void UFProtocol::_delete() {
  if( _pa == 0 ) return;
  if( _shared ) return;
  if( _shallow ) {
    // clog<<"UFProtocol::_delete> shallow values (undeleted)..."<<endl;
    delete _pa;
    return;
  }

  switch(_pa->_type) {
    case _ZippedStrings_:
    case _Strings_: { string* p = static_cast< string* > (_pa->_values); delete [] p; }
         break;

    case _ZippedBytes_:
    case _ByteFrames_: 
    case _Bytes_: { char* p = static_cast< char* > (_pa->_values); delete [] p; }
         break;

    case _ZippedShorts_:
    case _ShortFrames_:
    case _ObsConfig_:
    case _Shorts_: { short* p = static_cast< short* > (_pa->_values); delete [] p; }
         break;

    case _ZippedInts_:
    case _FrameConfig_: 
    case _IntFrames_:
    case _Ints_: { int* p = static_cast< int* > (_pa->_values); delete [] p; }
         break;

    case _ZippedFloats_:
    case _FloatFrames_:
    case _FlamObsConf_:
    case _Floats_: { 
      float* p = static_cast< float* > (_pa->_values);
      //clog<<"UFProtocol::_delete> float* p= "<<(int)p<<endl;
      delete [] p;
    }
         break;

    case _TimeStamp_: // default (has no _values)

    default:
         break;
  }

  delete _pa;
  _pa = 0;
}

// shallow copy default behavior is simpler with new assignment operator:
void UFProtocol::shallowCopy(const UFProtocol& rhs) {
  if( _pa != 0 && rhs._pa != 0 ) {
    _delete();
    *_pa = *rhs._pa;
    _shallow = true;
  }
}

// can provide default deep copy behavior also with switch/case?
// ...
// ...


// append & prepend signatures
int UFProtocol::append(const UFProtocol& ufp) {
  if( _pa == 0 ) return -1;
  if( _pa->_type != ufp._pa->_type ) return -2;
  if( _shallow || _shared ) return 0;

  switch(ufp._pa->_type) {
    case _ZippedStrings_:
    case _Strings_: { 
      string* old = static_cast< string* > (_pa->_values);
      string* apnd = static_cast< string* > (ufp._pa->_values);
      int n = numVals(), na = ufp.numVals();
      int nv = n + na;
      string* newp = new (nothrow) string [nv];
      if( newp == 0 ) return 0;
      _pa->_values = (void*) newp;
      for( int i = 0; i < n; ++i ) newp[i] = old[i]; // re-store old values
      for( int i = 0; i < na; ++i ) newp[i+n] = apnd[i]; // append ufp values
      delete [] old;
    }
    break;

    case _ZippedBytes_:
    case _ByteFrames_: 
    case _Bytes_: {
      char* old = static_cast< char* > (_pa->_values);
      char* apnd = static_cast< char* > (ufp._pa->_values);
      int n = numVals(), na = ufp.numVals();
      int nv = n + na;
      char* newp = new (nothrow) char [nv];
      if( newp == 0 ) return 0;
      _pa->_values = (void*) newp;
      for( int i = 0; i < n; ++i ) newp[i] = old[i]; // re-store old values
      for( int i = 0; i < na; ++i ) newp[i+n] = apnd[i]; // append ufp values
      delete [] old;
    }
    break;

    case _ZippedShorts_:
    case _ShortFrames_:
    case _ObsConfig_:
    case _Shorts_: {
      short* old = static_cast< short* > (_pa->_values);
      short* apnd = static_cast< short* > (ufp._pa->_values);
      int n = numVals(), na = ufp.numVals();
      int nv = n + na;
      short* newp = new (nothrow) short [nv];
      if( newp == 0 ) return 0;
      _pa->_values = (void*) newp;
      for( int i = 0; i < n; ++i ) newp[i] = old[i]; // re-store old values
      for( int i = 0; i < na; ++i ) newp[i+n] = apnd[i]; // append ufp values
      delete [] old;
    }
    break;

    case _ZippedInts_:
    case _FrameConfig_: 
    case _IntFrames_:
    case _Ints_: {
      int* old = static_cast< int* > (_pa->_values);
      int* apnd = static_cast< int* > (ufp._pa->_values);
      int n = numVals(), na = ufp.numVals();
      int nv = n + na;
      int* newp = new (nothrow) int [nv];
      if( newp == 0 ) return 0;
      _pa->_values = (void*) newp;
      for( int i = 0; i < n; ++i ) newp[i] = old[i]; // re-store old values
      for( int i = 0; i < na; ++i ) newp[i+n] = apnd[i]; // append ufp values
      delete [] old;
    }
    break;

    case _ZippedFloats_:
    case _FloatFrames_:
    case _Floats_: {
      float* old = static_cast< float* > (_pa->_values);
      float* apnd = static_cast< float* > (ufp._pa->_values);
      int n = numVals(), na = ufp.numVals();
      int nv = n + na;
      float* newp = new (nothrow) float [nv];
      if( newp == 0 ) return 0;
      _pa->_values = (void*) newp;
      for( int i = 0; i < n; ++i ) newp[i] = old[i]; // re-store old values
      for( int i = 0; i < na; ++i ) newp[i+n] = apnd[i]; // append ufp values
      delete [] old;
    }
    break;

    case _TimeStamp_: // default (has no _values)
    default:
    break;
  }

  _pa->_duration += ufp._pa->_duration;
  _pa->_elem += ufp._pa->_elem;
  _pa->_length += (ufp._pa->_length - headerLength(*ufp._pa));

  return numVals();
}

int UFProtocol::append(const UFProtocol* ufp) {
  if( ufp == 0 ) return -1;
  return append(*ufp);
}

#endif // __UFProtocol_cc__

