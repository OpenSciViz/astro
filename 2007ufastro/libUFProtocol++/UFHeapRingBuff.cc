#if !defined(__UFHeapRingBuff_cc__)
#define __UFHeapRingBuff_cc__ "$Name:  $ $Id: UFHeapRingBuff.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFHeapRingBuff_cc__;

#include "UFHeapRingBuff.h"
__UFHeapRingBuff_H__(__UFHeapRingBuff_cc);

#include "UFPosixRuntime.h"

const pthread_t pnot = (pthread_t) -1;

// public ctors
UFHeapRingBuff::UFHeapRingBuff(const string& name) : UFRingBuff(name), _theFlag(pnot) { _init(); }

UFHeapRingBuff::UFHeapRingBuff(vector<string>& names,
			       int bufsize) : UFRingBuff(), _theFlag(pnot) { init(names, bufsize); }


UFHeapRingBuff::UFHeapRingBuff(UFProtocol* external,
			       int bufsize, int numbuf,
			       const string& name) : UFRingBuff(external, bufsize, numbuf, name), _theFlag(pnot) {
  _init();
}

UFHeapRingBuff::UFHeapRingBuff(UFProtocol** external,
			       int bufsize, int numbuf,
			       const string& name) : UFRingBuff(external, bufsize, numbuf, name), _theFlag(pnot)  {
  _init();
}

UFHeapRingBuff::~UFHeapRingBuff() { 
  if( _externalData ) {
    return;
  }
  pthread_mutex_lock(&_theMutex);
  for(_activeIdx = 0; _activeIdx < (int) size(); _activeIdx++) {
    delete (*this)[_activeIdx];
  }
  pthread_mutex_unlock(&_theMutex);
  pthread_mutex_destroy(&_theMutex);
}


// protected:
void UFHeapRingBuff::_init() {
  pthread_mutex_init(&_theMutex, (const pthread_mutexattr_t *) 0);
}

int UFHeapRingBuff::_init(vector<string>& names, int bufsize, int type) {
  _bufsize = bufsize; // byte cnt
  if( (int) names.size() != size() ) resize(names.size());
 
  clog<<"UFRingBuff::_init> allocating "<<size()<<" buffers of size: "
	<< _bufsize<<endl;

  pthread_mutex_lock(&_theMutex);
  switch( type ) {
  case UFProtocol::_Bytes_: {
    const int elem = _bufsize;
    for(_activeIdx = 0; _activeIdx < (int) size(); _activeIdx++) {
      // use deep copy ctor, passing null val pointer should cause arrays vals to be set to default (0)
      _activeBuf = new UFBytes(names[_activeIdx], (unsigned char*)0, elem);
      (*this)[_activeIdx] = _activeBuf;
    }
  }
  break;
  case UFProtocol::_Shorts_: {
    const int elem = _bufsize / sizeof(short);
    for(_activeIdx = 0; _activeIdx < (int) size(); _activeIdx++) {
      // use deep copy ctor, passing null val pointer should cause arrays vals to be set to default (0)
      _activeBuf = new UFShorts(names[_activeIdx], (unsigned short*)0, elem);
      (*this)[_activeIdx] = _activeBuf;
    }
  }
  break;
  case UFProtocol::_Floats_: {
    const int elem = _bufsize / sizeof(float);
    for(_activeIdx = 0; _activeIdx < (int) size(); _activeIdx++) {
      // use deep copy ctor, passing null val pointer should cause arrays vals to be set to default (0)
      _activeBuf = new UFFloats(names[_activeIdx], (float*)0, elem);
      (*this)[_activeIdx] = _activeBuf;
    }
  }
  break;
  case UFProtocol::_Ints_: // ints are the default
  default: {
    const int elem = _bufsize / sizeof(int);
    for(_activeIdx = 0; _activeIdx < (int) size(); _activeIdx++) {
      // use deep copy ctor, passing null val pointer should cause arrays vals to be set to default (0)
      _activeBuf = new UFInts(names[_activeIdx], (int*)0, elem);
      (*this)[_activeIdx] = _activeBuf;
    }
  }
  break;
  }
  _activeIdx = 0;
  _activeBuf = (*this)[_activeIdx];
  pthread_mutex_unlock(&_theMutex);
  return size();
}

int UFHeapRingBuff::_setActiveIndex(int idx) {
  if( idx >= (int) size() ) idx = 0;

  pthread_mutex_lock(&_theMutex);
  _activeIdx = idx;
  pthread_mutex_unlock(&_theMutex);

  return _activeIdx;
} 

int UFHeapRingBuff::_getActiveIndex(bool& isLocked) {
  pthread_mutex_lock(&_theMutex);
  isLocked = (_theFlag != pnot) ? true : false;
  pthread_mutex_unlock(&_theMutex);

  return _activeIdx;
}  

// public:
int UFHeapRingBuff::init(vector<string>& names, int bufsize, int type) {
  _init();
  if( names.size() <= 0 ) { // default to 2
    names.push_back("#1"); names.push_back("#2");
  }
  resize(names.size());
  _externalData = false;
  return _init(names, bufsize, type);
}

int UFHeapRingBuff::activeIndex(bool& locked) { 
  return _getActiveIndex(locked); 
}

int UFHeapRingBuff::activeIndex() {
  bool locked;
  return _getActiveIndex(locked);
}

UFProtocol* UFHeapRingBuff::lockActiveBuf(int& index) {
  pthread_mutex_lock(&_theMutex);
  // indicate locked
  _theFlag = pthread_self();

  // (reset) the active index if indicated 
  int sz = size();
  if( index >= 0 ) {
    if( index >= sz )
      index = 0;
    _activeIdx = index;
  }
  pthread_mutex_unlock(&_theMutex);
  return activeBuf(index);
}

int UFHeapRingBuff::unlockActiveBuf() {
  int retval= -1;
  pthread_mutex_lock(&_theMutex);
  // indicate locked
  _theFlag = pnot;
  retval = _activeIdx;
  pthread_mutex_unlock(&_theMutex);
  return retval;
}

int UFHeapRingBuff::nextActiveBuf(){
  _setActiveIndex(1 + _activeIdx);
  return unlockActiveBuf();
}  

// convenience func. returns buffer that was last written
UFProtocol* UFHeapRingBuff::waitForNewData(int& index) {
  bool locked = true;
  UFProtocol* ufp = 0;
  // wait for the framegrabber to finish and unlock the buffer
  int  _index = activeIndex(locked);
  do {
    index = activeIndex(locked);
    //clog<<"UFShMemRingBuff::waitForNewData> activeIndex= "<<index<<", locked= "<<locked<<endl;
    if( locked ) UFPosixRuntime::sleep(0.01); //UFPosixRuntime::yield();
  } while( locked && index == _index );
  if( --index < 0 ) index = 0;
  return ufp = bufAt(index);
}
 
int UFHeapRingBuff::write(const UFProtocol* data, int index) {
  // this assumes that only one process can write
  // lockActiveBuff also sets _activeIdx to index if the index supplied is >= 0
  _activeBuf = lockActiveBuf(index); // this blocks until the active index mutex is released
  //_activeBuf->deepCopy(*data);
  _activeBuf->shallowCopy(*data);
  // increment the buffer index to make the next buffer active
  // and unlocks
  return nextActiveBuf();
}

int UFHeapRingBuff::read(UFProtocol* data, int index) {
  int retval = index;
  bool locked;
  if( index < 0 )
    index = _getActiveIndex(locked); // this blocks until the active index mutex is released 

  UFProtocol* readBuf = bufAt(index);
  if( index != _activeIdx || !locked ) {
    //data->deepCopy(*readBuf);
    data->shallowCopy(*readBuf);
    retval = index;
  }

  return retval;
}

#endif // __UFHeapRingBuff_cc__
