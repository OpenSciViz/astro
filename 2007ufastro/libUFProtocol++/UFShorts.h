#if !defined(__UFShorts_h__)
#define __UFShorts_h__ "$Name:  $ $Id: UFShorts.h,v 0.3 2006/04/26 16:22:46 hon Exp $"
#define __UFShorts_H__(arg) const char arg##Shorts_h__rcsId[] = __UFShorts_h__;

#include "UFTimeStamp.h"

class UFShorts: public virtual UFTimeStamp {
protected:
  inline UFShorts() {}

public:
  UFShorts(const UFProtocolAttributes& pa);
  UFShorts(bool shared, int length=_MinLength_);
  UFShorts(const string& name, bool shared=false, int length=_MinLength_);
  UFShorts(const string& name, short* vals, int elem=1, bool shared=false);
  UFShorts(const string& name, const short* vals, int elem=1, bool shared=false);
  UFShorts(const string& name, UFProtocolAttributes* pa, int elem, bool shared=true);
  UFShorts(const UFShorts& rhs);

  inline virtual ~UFShorts() {}

  inline virtual string description() const { return string(__UFShorts_h__); }
  inline virtual int valSize(int elemIdx=0) const { return sizeof(short); }

  inline virtual const char* valData(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return (char*) &((short*)_pa->_values)[elemIdx]; }
  inline short* valShorts(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return &((short*)_pa->_values)[elemIdx]; }
  inline short valShort(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return ((short*)_pa->_values)[elemIdx]; }
  inline short operator[](int elemIdx) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return valShort(elemIdx); }

  virtual void deepCopy(const UFProtocol& rhs);

  inline virtual void copyVal(char* dst, int elemIdx= 0) {
    if( elemIdx < 0 || elemIdx >= elements() ) return;
    memcpy(dst, &((short*)_pa->_values)[elemIdx], sizeof(short));
  }  

  virtual int writeValues(int fd) const;
  virtual int readValues(int fd); 
  virtual int writeTo(int fd) const; 
  virtual int readFrom(int fd);
  virtual int sendValues(UFSocket& soc) const;
  virtual int recvValues(UFSocket& soc);
  virtual int sendTo(UFSocket& soc) const; 
  virtual int recvFrom(UFSocket& soc);
};

#endif // __UFShorts_h__


/**
 * Shorts Protocol Msg contains one or more (variable) length ints
 * @see UFProtocol
 * @see UFTimeStamp
 */
  /**
   * Default constructor is hidden to support the object factory Pattern.
   */
  /**
   * Instantiate a UFShorts object given the UFProtocolAttributes object.
   * This is the constructor used to initialize the UFShort object by the
   * class factory.
   * @param pa The attributes for initializing this object
   * @return Nothing
   */
  /**
   * Constructor for shared memory applications
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor performs a shallow copy of the vals array.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param shared True if this object resides in shared memory, false otherwise
   * @param elem The length of the array, default to 1
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and an array of values.
   * This method performs a shallow copy of vals
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @param shared True if this object resides in shared memory, false otherwise
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param pa The UFProtocolAttributes object from which to copy data
   * @param elem The length of the array
   * @param shared True if this object resides in shared memory, false otherwise
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * Copy constructor, performs a shallow copy of the given UFShorts object.
   * @param rhs The source UFShorts object to copy into this object.
   */
  /**
   * Default UFShorts destructor relies on the UFProtocol destructor.
   */
  /**
   * Return a description of the current object.
   * This method is meant to be overloaded by subclasses.
   * @return A copy of a std::string containing the object's description.
   */
  /**
   * Return size of an element of the values array
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return The size of the element at index elemIdx
   */
  /**
   * Return the value of the element at elemIdx
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return const char* pointer to first byte of _values (data).
   */
  /**
   * Perform a full value copy of rhs into the current object's UFProtocolAttribute object.
   * @param rhs The source UFProtocol object from which to obtain new values.
   * @see UFprotocol
   * @see UFProtocolAttribute
   */
  /**
   * Copy internal values to external buff
   * @param dst The destination buffer to which to write the value
   * @param elemIdx The index of the value array to be examined, defaults to 0
   * @return Nothing
   */
  /**
   * Write out only the values array to the given file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out the internal data representation to file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes output on success
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Write the internal data representation out to a socket
   * @return 0 on failure, number of bytes written on success
   * @param soc The UFSocket to which to write
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
