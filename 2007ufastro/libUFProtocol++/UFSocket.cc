#if !defined(__UFSocket_cc__)
#define __UFSocket_cc__ "$Name:  $ $Id: UFSocket.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSocket_cc__;

#include "UFSocket.h"
__UFSocket_H__(UFSocket_cc);

#include "UFPosixRuntime.h"
__UFPosixRuntime_H__(UFSocket_cc);

#include "UFRuntime.h"
__UFRuntime_H__(UFSocket_cc);

#include "UFProtocol.h"
__UFProtocol_H__(UFSocket_cc);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif

//#include "new"
#include "cstdio"
#include "cstring"
#include "cerrno"
#include "cstdlib"
#include "unistd.h"
#include "netdb.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "sys/uio.h"
#include "sys/ioctl.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/socket.h"
#include "arpa/inet.h"
#include "netinet/in.h" // IP_
#include "netinet/tcp.h" // TCP_

#if !defined(CYGWIN)
#include "stropts.h"
#endif
#if defined(LINUX)
#include "asm/ioctls.h" // FIONREAD
#endif

// the following are Solaris 7 and Cygwin omissions:
#if !defined(NI_MAXHOST) || !defined(NI_NUMERICHOST) || !defined(NI_MAXSERV) || !defined(NI_NUMERICSERV)
#define NI_MAXHOST 1025
#define NI_NUMERICHOST 0x0002
#define NI_MAXSERV 32
#define NI_NUMERICSERV 0x0008
#endif

#if defined (SOLARIS)
#include "xti_inet.h" // more TCP_
extern int getaddrinfo(const char *nodename, const char  *servname,
                       const struct addrinfo *hints, struct addrinfo **res);

extern int getnameinfo(const struct sockaddr *sa, socklen_t  salen,
                       char  *host, size_t hostlen, char *serv, size_t servlen, int
                       flags);
#endif

// c++ streams
#include "iostream"
#include "fstream"
#include "strstream"

bool UFSocket::_verbose= false;

// ctor helper can also be used by sendThread
bool UFSocket::_create(bool threaded) {
  _socmutex = _sendmutex = _recvmutex = 0;

  if( !threaded )
    return true;

  // with or without mutex protection...
  if( _socmutex == 0 ) {
    _socmutex = new (nothrow) pthread_mutex_t; 
    if( _socmutex != 0 )
      ::pthread_mutex_init(_socmutex, 0);
    else
      return false;
  }

  if( _sendmutex == 0 ) {
    _sendmutex = new (nothrow) pthread_mutex_t; 
    if( _sendmutex != 0 )
      ::pthread_mutex_init(_sendmutex, 0);
    else
      return false;
  }

  if( _recvmutex == 0 ) {
    _recvmutex = new (nothrow) pthread_mutex_t; 
    if( _recvmutex != 0 )
      ::pthread_mutex_init(_recvmutex, 0);
    else
      return false;
  }
  
  return true;
}

// former inline:
UFSocketInfo::UFSocketInfo(int socFd) : fd(socFd), listenFd(-1), maxFd(2), _timeOut(-1.0), addr(0) {
  if( socFd >= 0 ) // special case of socket provided by inetd or other parent ?
    maxFd = 3;
}


/// deep copy ctor allows a function to safely return a UFSocket by val or ref.?:
UFSocketInfo::UFSocketInfo(const UFSocketInfo& rhs) : fd(rhs.fd), listenFd(rhs.listenFd),
                                                      maxFd(rhs.maxFd), _timeOut(-1.0),
						      addr(0) {
  if( rhs.addr ) {
    addr = new (nothrow) sockaddr_in;
    if( 0 == addr )
      clog << "UFSocketInfo::> Memory allocation failure\n" ;
    else
     ::memcpy(addr, rhs.addr, sizeof(sockaddr_in));
  }
}

bool UFSocketInfo::isSock(int fd) {
  struct stat st;
  ::fstat(fd, &st);
  return S_ISSOCK(st.st_mode);
}

#if defined(SOLARIS) || defined(LINUX)
string UFSocketInfo::peerIP(int fd) {
  string pn;
  struct sockaddr sa;
  socklen_t sl = sizeof(sa);
  int peer = ::getpeername(fd, &sa, &sl);
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  peer = ::getnameinfo(&sa, sl, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf),
		       NI_NUMERICHOST | NI_NUMERICSERV);
  if( peer != 0 ) {
    if( UFSocket::_verbose )
      clog<<"UFSocketInfo::peerIP> unable to get peerIP."<<endl;
    return pn;
  }
  pn = hbuf;
  return pn;
}

string UFSocketInfo::peerName(int fd) {
  string ip = UFSocketInfo::peerIP(fd);
  if( ip == "" )
    return "";

  struct hostent *h = ::gethostbyaddr(ip.c_str(), ip.length(), AF_INET);

  if( h == 0 )
    return "";

  string pn = h->h_name;
  return pn;
}
#endif

int UFSocketInfo::setBlocking(bool blocking) {
  int flags = ::fcntl(fd, F_GETFL, 0 );
  if( flags < 0 )
    clog << "UFSocket::setBlocking> failed to get sock flags" << endl;

  if( blocking ) {
    // explicitly set to blocking (uflib default)
    flags = ::fcntl(fd, F_SETFL, flags & ~O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setBlocking> failed to set O_NONBLOCK" << endl;
  }
  else {
    // explicitly set to non-blocking
    flags = ::fcntl(fd, F_SETFL, flags | O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setBlocking> failed to set O_NONBLOCK" << endl;
  }

  return flags;
}

int UFSocketInfo::setBlocking(int socfd, bool blocking) {
  int flags = ::fcntl(socfd, F_GETFL, 0 );
  if( flags < 0 )
    clog << "UFSocket::setBlocking> failed to get sock flags" << endl;

  if( blocking ) {
    // explicitly set to blocking (uflib default)
    flags = ::fcntl(socfd, F_SETFL, flags & ~O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setBlocking> failed to set O_NONBLOCK" << endl;
  }
  else {
    // explicitly set to non-blocking
    flags = ::fcntl(socfd, F_SETFL, flags | O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setBlocking> failed to set O_NONBLOCK" << endl;
  }

  return flags;
}

// moved this into UFSocketInfo so that listen/accept funcs. can use them
int UFSocketInfo::close() {
  if( fd < 0 || addr == 0 ) // already closed?
    return fd;

  int _fd = fd; // use tmp
  fd = -1;
  //if( UFSocket::_verbose )
    clog<<"UFSocketInfo::close> fd: "<<_fd<<" (listenFd: "<<listenFd<<")"<<endl;
  delete addr; addr = 0;
  return ::close(_fd);
}

// stop listening
int UFSocketInfo::closeFully() {
  // close i/o socket:
  close();
  // close listener
  int _fd = listenFd; // use tmp
  listenFd  = -1;

  //if( UFSocket::_verbose )
    clog<<"UFSocketInfo::closeFully> listenFd: "<<_fd<<endl;
  delete addr; addr = 0;
  if( _fd > 0 )
    return ::close(_fd);
  return 0;
}
 
// check the socket (0 timeout select and msg_peek attempts
bool UFSocketInfo::valid(int socFd) {
  if( socFd < 0 ) {
    clog << "UFSocketInfo::valid> not initialized "
	 << "or no longer open!" << endl;
    return false;
  }
  if ( !isSock(socFd) ) {
    clog << "UFSocketInfo::valid> not a socket Fd: "<< socFd << endl;
    return false;
  }
  if( UFSocket::_verbose )
    clog<<"UFSocketInfo::valid> check UFRuntime::available on socFd: "<<socFd<<endl;
  int na = UFRuntime::available(socFd);
  if( na < 0 ) {
    clog<<"UFSocketInfo::valid> available: "<<na<<", "<<strerror(errno)<<", socFd= "<<socFd<<endl;
    return false;
  }

  // if Linux does not support peerName yet?
#if !defined(LINUX)
  setBlocking(socFd, false);
  string peer = peerName(socFd);
  setBlocking(socFd, true);
  if( peer == ""  && UFSocket::_verbose ) {
    clog<<"UFSocketInfo::valid> unable to getpeername for socket fd: "<<socFd<<endl;
    //setBlocking(socFd, true);
    //return false; // should not be fatal?
  }
  else if( UFSocket::_verbose ) {
    clog<<"UFSocketInfo::valid> socFd: "<<socFd<<", getpeername: "<<peer<<endl;
  }
#endif
  // if Linux still does not support peek, move the #endif
  if( na > 0 ) {
    char c= '!';
    int n1 = ::recv(socFd, &c, 1, MSG_PEEK);
    //clog<<"UFSocketInfo::valid> msg peek c: "<<c<<", na, n1: "<<na<<", "<<n1<<", err: "<<strerror(errno)<<", socFd= "<<socFd<<endl;
    if( n1 != 1 ) { 
      //setBlocking(socFd, true);
      //if( _recvmutex ) ::pthread_mutex_unlock(_recvmutex);
      clog<<"UFSocketInfo::valid> peek 1 failed, na: "<<na<<", socFd= "<<socFd<<" "<<strerror(errno)<<endl;
      //return false; // rather than continue on to select call, since na > 0 -- this is a problem on linux?
    }
    if( UFSocket::_verbose )
      clog<<"UFSocketInfo::valid> peek 1 succeeded, na: "<<na<<", socFd= "<<socFd<<endl;
    return true;
  }
 
  fd_set readfds, writefds, excptfds;
  FD_ZERO(&readfds); FD_ZERO(&writefds); FD_ZERO(&excptfds);
  FD_SET(socFd, &readfds); FD_SET(socFd, &writefds); FD_SET(socFd, &excptfds);

  struct timeval poll = { 0, 0 }; // don't block 
  //struct timeval tenthsec = { 0L, 100000L }; // 0.1 sec. block
  int fdcnt= -1, cnt= 3; // UFPosixRuntime::_MaxInterrupts_;
  if( UFSocket::_verbose )
    clog<<"UFSocketInfo::valid> perform select r/w/err on all fd <= socFd: "<<socFd<<endl;
  do {
    // since select may modify this area of memory, always provide fresh input:
    struct timeval timeout = poll; //tenthsec; // poll;
    fdcnt = ::select(1+socFd, &readfds, &writefds, &excptfds, &timeout);
    //fdcnt = select(FD_SETSIZE, &readfds, &writefds, &excptfds, &timeout);
  } while( (errno == 0 || errno == EINTR) && --cnt >= 0 );
  if( UFSocket::_verbose )
    clog<<"UFSocketInfo::valid> select result: "<<fdcnt<<", "<<strerror(errno)<<endl;

  if( fdcnt <= 0 ) {
    clog << "UFSocketInfo::valid> assume valid?, select fdcnt: "<<fdcnt<<", "<<strerror(errno)<<endl;
    return true;
  }

  if( FD_ISSET(socFd, &excptfds) ) {
    clog << "UFSocketInfo::valid> excptfds! socket fd= " << socFd << " fdcnt= " << fdcnt << endl;
    return false;
  }
  // according to stevens (pg 154) select can indicate the
  // socket has an error by indicating that it is both readselct and writeselct
  bool readselct= false, writeselct= false;
  if( FD_ISSET(socFd, &readfds) ) {
    readselct = true;
    if( UFSocket::_verbose )
      clog<<"UFSocketInfo::valid> select indicates readfds set, socket fd= " << socFd << " fdcnt= " << fdcnt << endl;
  }

  // if socket is readable, client is still connected... 
  if( FD_ISSET(socFd, &writefds) ) {
    // but if client is backlogged, server side write may block
    if( UFSocket::_verbose )
      clog<<"UFSocketInfo::valid> select indicates writefds set, socket fd= " << socFd << " fdcnt= " << fdcnt << endl;
    writeselct = true;
  }

  // but this seems to be misleading (untrue?)...
  /*
  if( readselct && writeselct ) {
    clog<<"UFSocketInfo::valid> select indicates socket both read & write, could be error? "<<endl;
       return false;
  }
  */

  #if !defined(LINUX)
  if( readselct ) {
    // one last try -- has other side closed soc and select indicates a readable byte?
    char c= '?';
    int n1 = ::recv(socFd, &c, 1, MSG_PEEK); 
    if( n1 != 1 ) {
      clog<<"UFSocketInfo::valid> select indicates readfds set, msg peek failed, c: "<<c
	  <<", n1: "<<n1<<", "<<strerror(errno)<<", socFd= "<<socFd<<endl;
      return false;
    }
  }
  #endif

  if( UFSocket::_verbose )
    clog << "UFSocketInfo::valid> valid socket fd= "<< socFd << " fdcnt= " << fdcnt << endl;

  return true;
} //UFSocketInfo::valid return true or false

bool UFSocketInfo::validConnection() const {
  if( fd < 0 ) {
    clog << "UFSocketInfo::validConnection> socket not initialized, "
	 << "or bad socket file descriptor." << endl;
    return false;
  }
  // not that this only make sens for non listenFd fds...
  return valid(fd); // assume fd != listenFd
}

UFSocket::UFSocket(int port, bool threaded) : _portNo(port), _sockinfo(), 
	                                      _socmutex(0), _recvmutex(0), _sendmutex(0) {
  _create(threaded);
}

UFSocket::UFSocket(int fd, int port, bool threaded) : _portNo(port), _sockinfo(fd),
						      _socmutex(0),_recvmutex(0), _sendmutex(0) {
  _create(threaded);
}

UFSocket::UFSocket(UFSocketInfo connected, 
		   int port, bool threaded) : _portNo(port), _sockinfo(connected),
					      _socmutex(0),_recvmutex(0), _sendmutex(0) {
  _create(threaded);
}

UFSocket::UFSocket(const UFSocket& rhs) : _portNo(rhs._portNo), _sockinfo(rhs._sockinfo),
					  _socmutex(0),_recvmutex(0), _sendmutex(0) {
  _create( rhs._socmutex && rhs._sendmutex && rhs._recvmutex );
}

UFSocket::~UFSocket() {
  lock();

  if( _recvmutex ) 
    ::pthread_mutex_destroy(_recvmutex);
  if( _sendmutex )
    ::pthread_mutex_destroy(_sendmutex);

  unlock();

  if( _socmutex ) 
    ::pthread_mutex_destroy(_socmutex);

  delete _socmutex; delete _sendmutex; delete _recvmutex;
  _socmutex = _sendmutex = _recvmutex = 0;

  _portNo = -1;
}

// static methods
string UFSocket::ipAddrOf(const string& host) {
  struct hostent *hostEntry = ::gethostbyname(host.c_str());
  if( hostEntry == NULL) {
    clog << "UFSocket::ipAddrOf> unable to find host name in service! "
	 << host << endl;
    return host;
  }
  struct in_addr in;
  char **p = hostEntry->h_addr_list;
  ::memcpy(&in.s_addr, *p, sizeof (in.s_addr));
  char ipAddr[] = "000.000.000.000";
  sprintf(ipAddr,"%s", inet_ntoa(in));

  return string(ipAddr);
}

string UFSocket::description() const {
  strstream s;
  s << "socket(listen file descr.= " << _sockinfo.listenFd
    << ", connect file descr.= " << _sockinfo.fd;
  if( 0 != _sockinfo.addr ) {
    s << " ): " << "_portNo= " << _sockinfo.addr->sin_port << " ip= "
      << inet_ntoa( _sockinfo.addr->sin_addr ) << ends;
  }
  string retval(s.str());
  delete[] s.str();
  return retval;
}

// reset socket to initial default state? can't recall why I need this...
//int UFSocket::resetSocket(UFSocketInfo& socket, bool blocking) {
//  return setSocket(socket, block);
//}

int UFSocket::setSocket(UFSocketInfo& socket, bool blocking) {
  if( _sockinfo.fd < 0 && socket.fd > 0 ) {
    _sockinfo = socket;
    _portNo = socket.addr->sin_port;
  }
  else if( _sockinfo.fd < 0 ) {
    clog << "UFSocket::setSocket> socket not initialized!"<<endl;
    return 0;
  }

  if( _sockinfo.addr == 0 && _portNo > 0 ) {
    _sockinfo.addr = new (nothrow) sockaddr_in;
    if( NULL == _sockinfo.addr ) {
      clog << "UFSocket::setSock> Memory allocation failure"<<endl;
      return -1;
    }
    ::memset(reinterpret_cast< char * >( _sockinfo.addr ),
	     0, sizeof(sockaddr_in));
    _sockinfo.addr->sin_family = AF_INET;
    _sockinfo.addr->sin_port = htons((u_short)_portNo);
  }

  if( _sockinfo.addr != 0 && _portNo > 0 ) {
    struct linger setLinger;
    setLinger.l_onoff = 0; // disable linger
    setLinger.l_linger = 0;
    
    if( ::setsockopt(_sockinfo.fd, SOL_SOCKET, SO_LINGER,
		     reinterpret_cast< const char * >( &setLinger ),
		     sizeof(setLinger) ) < 0 )
      clog << "UFSocket::setSocket> failed to set SO_LINGER" << endl;
  }

  int optval = 1, stat, flags;
  // detect closed connection
  stat = ::setsockopt( _sockinfo.fd, SOL_SOCKET, SO_KEEPALIVE,
		       reinterpret_cast< const char * >( &optval ),
		       sizeof(optval) );
  if( stat < 0 )
    clog << "UFSocket::setSocket> failed to set SO_KEEPALIVE" << endl;

  // immediately deliver msg
  stat = ::setsockopt( _sockinfo.fd, IPPROTO_TCP, TCP_NODELAY,
		       reinterpret_cast< const char * >( &optval ),
		       sizeof(optval) );
  if( stat < 0 )
    clog << "UFSocket::setSocket> failed to set TCP_NODELAY" << endl;

  // allow fast re-binds
  stat = ::setsockopt( _sockinfo.fd, SOL_SOCKET, SO_REUSEADDR,
		     (const char *)&optval, sizeof(optval) );
  if( stat < 0 )
    clog << "UFSocket::setSocket> failed to set SO_REUSEADDR" << endl;

  // use max. buffer size
  optval = 1024*1024;

  stat = ::setsockopt( _sockinfo.fd, SOL_SOCKET, SO_SNDBUF,
		       reinterpret_cast< const char * >( &optval ),
		       sizeof(optval) );
  if( stat < 0 )
    clog << "UFSocket::setSocket> failed to set SO_SNDBUF" << endl;

  stat = ::setsockopt( _sockinfo.fd, SOL_SOCKET, SO_RCVBUF,
		       reinterpret_cast< const char * >( &optval ),
		       sizeof(optval) );
 
  if( stat  < 0 )
    clog << "UFSocket::setSocket> failed to set SO_RCVBUF" << endl;

  flags = ::fcntl( _sockinfo.fd, F_GETFL, 0 );
  if( flags < 0 )
    clog << "UFSocket::setSocket> failed to get sock flags" << endl;

  if( blocking ) {
    // explicitly set to blocking (default)
    flags = ::fcntl( _sockinfo.fd, F_SETFL, flags & ~O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setSocket> failed to set O_NONBLOCK" << endl;
  }
  else {
    // explicitly set to non-blocking
    flags = ::fcntl( _sockinfo.fd, F_SETFL, flags | O_NONBLOCK );
    if( flags < 0 )
      clog << "UFSocket::setSocket> failed to set O_NONBLOCK" << endl;
  }
  return _portNo;
}

int UFSocket::socBufSizes(int& snd, int& rcv) const {
  int optval = 0;
  socklen_t len = static_cast< socklen_t >( sizeof(optval) );

  int stat = ::getsockopt(_sockinfo.fd, SOL_SOCKET, SO_SNDBUF, &optval, &len);
  if( stat < 0 )
    clog << "UFSocket::socBufSizes> failed to get SO_SNDBUF" << endl;
  else
    snd = optval;

  stat = ::getsockopt(_sockinfo.fd, SOL_SOCKET, SO_RCVBUF, &optval, &len);
  if( stat < 0 )
    clog << "UFSocket::socBufSizes> failed to get SO_RCVBUF" << endl;
  else
    rcv = optval;

  return stat;
}

int UFSocket::hasError(int error_no) {
  switch(error_no) {
  case EBADF:
    clog << "UFSocket::hasError> EBADF: invalid file descriptor." << endl;
    break;
  case EINTR:
    clog << "UFSocket::hasError> EINTR: The operation was interrupted "
	 << "by delivery of a signal before any data could be buffered "
	 << "to be received/sent." << endl;
    break;
  case EIO:
    clog << "UFSocket::hasError> EIO: An I/O error occurred while "
	 << "reading from or writing to the file system." << endl;
    break;
  case ENOMEM:
    clog << "UFSocket::hasError> ENOMEM: There was insufficient "
	 << "user memory available for the operation to complete." << endl;
    break;
  case ENOSR:
    clog << "UFSocket::hasError> ENOSR: There were insufficient "
	 << "STREAMS resources available for the operation to "
	 << "complete." << endl;
    break;
  case ENOTSOCK:
    clog << "UFSocket::hasError> ENOTSOCK: not a socket." << endl;
    break;
  case ESTALE:
    clog << "UFSocket::hasError> ESTALE: A stale NFS file handle "
	 << "exists." << endl;
    break;
  case EWOULDBLOCK:
    clog << "UFSocket::hasError> EWOULDBLOCK: The socket is marked "
	 << "non-blocking and the requested operation would block." << endl;
    break;
  case EINVAL:
    clog << "UFSocket::hasError> EINVAL: Not the size of a valid "
	 << "address for the specified address family." << endl;
    break;
  case EMSGSIZE:
    clog << "UFSocket::hasError> EMSGSIZE: The socket requires "
	 << "that message be sent atomically, and the message was "
	 << "too long." << endl;
    break;
  default:
    if( !error_no )
      return error_no;
    clog << "UFSocket::hasError> unknown/unexpected socket error. = "
	 << error_no<<", " << strerror(errno) <<  endl;
    break;
  }
  return error_no;
}

// static
bool UFSocket::isIPAddress(const string& h) {
  unsigned int count = 0;

  for( string::const_iterator currPtr = h.begin(), endPtr = h.end() ;
       currPtr != endPtr ; ++currPtr ) {
      // characters in an IP address may only be
      // digits or decimal points
      if( '.' == *currPtr )
	count++;
      else if( ! isdigit( *currPtr ) )
	return false;
  }

  // there are exactly three decimal points in an IP address
  return (count == 3);
}

int UFSocket::writableList(const vector< socketFd >& socFds, 
			   vector< socketFd >& writeList,
			   float timeOut) {
  if( _verbose ) {
    clog << "UFSocket::writeableList> timeOut= "<<timeOut
         <<" checking file descriptors "<<ends;

    for( int ifd = 0; ifd < (int)socFds.size(); ++ifd ) {
      clog << socFds[ifd] << ", "<<ends;
    }
    clog<<endl;
  }

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  fd_set writefds;
  FD_ZERO(&writefds);
  int maxFd=0;
  for( vector< socketFd >::size_type i = 0; i < socFds.size(); ++i ) {
    if( isSock(socFds[i]) ) {
      FD_SET(socFds[i], &writefds);
      if( socFds[i] > maxFd ) maxFd = socFds[i];
    }
  }
  int fdcnt, cnt= UFPosixRuntime::_MaxInterrupts_;
  do  {
    // since select may modify this area of memory, always provide fresh input:
    struct timeval to, *to_p= 0;
    if( usetimeout ) {
      to = *usetimeout;
      to_p = &to;
    }
    errno = 0;
    fdcnt = ::select(1+maxFd, 0, &writefds, 0, to_p);
    //fdcnt = ::select(FD_SETSIZE, 0, &writefds, 0, to_p);
  } while( errno == EINTR && --cnt >= 0 );

  if( fdcnt < 0 ) {
    clog << "UFSocket::writeableList> error occured on select, fdcnt= "
	 << fdcnt<<endl;
    hasError();
    return fdcnt;
  }

  if( fdcnt == 0 ) {
    clog << "UFSocket::writeableList> timed-out: "
	 << timeOut<<" (sec.), all output buffers still full, can't write"
	 << endl;
    return fdcnt;
  }

  for( vector< socketFd >::size_type i = 0; i < socFds.size(); ++i ) {
    if( FD_ISSET(socFds[i], &writefds) ) writeList.push_back(socFds[i]);
  }
  
  return fdcnt;
}

int UFSocket::writableList(const UFSocket::ConnectTable& connections,
			   vector< UFSocket* >& wlist, pthread_mutex_t* mutex) {
  wlist.clear();
  
  if( mutex ) 
    if( ::pthread_mutex_lock(mutex) < 0 ) 
      return -1;
 
  UFSocket::ConnectTable::const_iterator i = connections.begin();
  for( ; i != connections.end(); ++i ) {
    string name = i->first; 
    UFSocket* socp = i->second; 
    if( socp != 0 )
      if( socp->writable() >= 0 )
        wlist.push_back(socp);
  }
  if( mutex ) ::pthread_mutex_unlock(mutex); 
  return (int)wlist.size();
}

int UFSocket::readableList(const vector< socketFd >& socFds, 
			   vector< socketFd >& pendingList,
			   float timeOut) { // timeOut < 0 ==> infinite/block
  if( _verbose ) {
    clog << "UFSocket::readableList> timeOut= "<<timeOut<<" checking "
         << socFds.size()<<" file descriptors "<<ends;

    for( vector< socketFd >::size_type ifd = 0; ifd < socFds.size(); ++ifd ) {
      clog << socFds[ifd] << ", "<<ends;
    }
    clog<<endl;
  }

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  fd_set readfds;
  FD_ZERO(&readfds);
  int maxFd=0;
  for( vector< socketFd >::size_type i = 0; i < socFds.size(); ++i ) {
    if( isSock(socFds[i]) ) {
      FD_SET(socFds[i], &readfds);
      if( socFds[i] > maxFd ) maxFd = socFds[i];
    }
  }
  int fdcnt= -1, trycnt= 3; //UFPosixRuntime::_MaxInterrupts_;
  do {
    // since select may modify this area of memory, always provide fresh input:
    struct timeval to, *pto= 0;
    if( usetimeout ) { to = *usetimeout; pto = &to; }
    fdcnt = ::select(1+maxFd, &readfds, 0, 0, pto);
    //fdcnt = ::select(FD_SETSIZE, &readfds, 0, 0, pto);
  } while( (errno == 0 || errno == EINTR) && --trycnt >= 0 );

  if( fdcnt < 0 ) {
    clog << "UFSocket::readableList> error occured on select, fdcnt= "
	 << fdcnt << endl;
    hasError();
    return fdcnt;
  }
  if( fdcnt == 0 ) {
    clog << "UFSocket::readableList> timed-out: "<<timeOut
	 << " (sec.), no input pending" << endl;
    return fdcnt;
  }
  for( vector< socketFd >::size_type i = 0; i < socFds.size(); ++i ) {
    if( FD_ISSET(socFds[i], &readfds) ) pendingList.push_back(socFds[i]);
  }
  
  return fdcnt;
}

int UFSocket::readableList(const UFSocket::ConnectTable& connections,
			   vector< UFSocket* >& rlist,  pthread_mutex_t* mutex) {
  rlist.clear();
  
  if( mutex ) 
    if( ::pthread_mutex_lock(mutex) < 0 ) 
      return -1;
  
  UFSocket::ConnectTable::const_iterator i = connections.begin();
  for( ; i != connections.end(); ++i ) {
    string name = i->first; 
    UFSocket* socp = i->second; 
    if( socp != 0 )
      if( socp->readable() > 0 )
        rlist.push_back(socp);
  }
  if( mutex ) ::pthread_mutex_unlock(mutex); 
  return (int)rlist.size();
}

int UFSocket::availableList(const UFSocket::ConnectTable& connections, std::map< UFSocket*,
			    int >& alist,  pthread_mutex_t* mutex) {
  alist.clear();
  
  if( mutex ) 
    if( ::pthread_mutex_lock(mutex) < 0 ) 
      return -1;
  
  UFSocket::ConnectTable::const_iterator i = connections.begin();
  for( ; i != connections.end(); ++i ) {
    string name = i->first; 
    UFSocket* socp = i->second;
    if( socp != 0 ) {
      int nb =  socp->available();
      //if( nb != 0 )
        alist.insert( std::map< UFSocket*, int >::value_type(socp, nb) );
    }
  }
  if( mutex ) ::pthread_mutex_unlock(mutex); 
  return (int)alist.size();
}

// does socket peak of one byte succeed or fail?
bool UFSocket::peekable(float timeOut, socketFd fd) {
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog << "UFSocket::peekable> not initialized or no longer open"<<endl;
    return false;
  }
  if ( !isSock(fd) ) {
    clog << "UFSocket::peekable> not a socket Fd: "<< fd <<endl;
    return false;
  }
  int na = UFRuntime::available(fd); // cygwin does not support this (on w32 this will return 0)
  if( na < 0 ) {
    if( _verbose )
      clog<<"UFSocket::peekable> available: "<<na<<", "<<strerror(errno)<<", socFd= "<<fd<<endl;
    return false;  
  }
  else {  
    char c='?';                  
    setBlocking(false);
    na = ::recv(fd, &c, 1, MSG_PEEK);                  
    setBlocking(true);
    if( na <= 0 ) {                                         
      clog<<"UFSocket::peakable> failed msg peek (1 char): "<<na<<", "<<strerror(errno)<<", socFd= "<<fd<<endl;                  
      return false;                                                                                
    }
    if( _verbose )
      clog<<"UFSocket::peakable> msg peek (1 char): "<<na<<", c: "<<c<<", socFd= "<<fd<<endl;
  }
  return true;
} // peekable

// if available (ioctl) does not work (cygwin), berhaps this will:
int UFSocket::peekCnt(float timeOut, socketFd fd) {
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog << "UFSocket::peekable> not initialized or no longer open"<<endl;
    return -1;
  }
  if ( !isSock(fd) ) {
    clog << "UFSocket::peekable> not a socket Fd: "<< fd <<endl;
    return -1;
  }
  
  // cygwin does not support this (on w32 this will return 0)
  int na = UFRuntime::available(fd, timeOut);
  if( na < 0 ) {
    //if(_verbose )
      clog<<"UFSocket::peekCnt> "<<na<<", "<<strerror(errno)<<", socFd= "<<fd<<endl;
    return na;  
  }
  char buf[BUFSIZ]; memset(buf, 0, sizeof(buf));
  int cnt= 1;
  setBlocking(false);
  int np = ::recv(fd, buf, 1, MSG_PEEK); // | MSG_DONTWAIT);
  while( np == cnt && cnt <= BUFSIZ ) {
    np = ::recv(fd, buf, ++cnt, MSG_PEEK);
  }
  setBlocking(true);
  --cnt;
  if( _verbose )
    clog<<"UFSocket::peakable> np: "<<np<<", avail na: "<<na<<", socFd= "<<fd<<endl;
  
  return cnt;
}

int UFSocket::readable(float timeOut, socketFd fd) { 
  // this should block (select) only for timeOut or less...
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  if( fd < 0 || _portNo < 0 ) {
    clog<<"UFSocket::readable> soc not initialized or no longer open"<<endl;
    return -2;
  }
  if ( !isSock(fd) ) {
    clog << "UFSocket::readable> not a socket Fd: "<< fd <<endl;
    return -1;
  }

  fd_set readfds, excptfds;
  FD_ZERO(&readfds); FD_ZERO(&excptfds);
  FD_SET(fd, &readfds); FD_SET(fd, &excptfds);
 
  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  int fdcnt= -1, cnt= 3; //UFPosixRuntime::_MaxInterrupts_;
  struct timeval to, *pto= 0;
  do {
    // since select may modify this area of memory, always provide fresh input:
    if( usetimeout ) { to = *usetimeout; pto = &to; }
    fdcnt = ::select(1+fd, &readfds, 0, &excptfds, pto);
    //fdcnt = select(FD_SETSIZE, &readfds, 0, &excptfds, pto);
  } while( --cnt >= 0 && (errno == EINTR || errno == 0) );

  if( fdcnt > 0 &&  FD_ISSET(fd, &excptfds) ) {
    clog << "UFSocket::readable> exception present on Fd: "<<fd<<endl;
    return -1;
  }
  if( fdcnt < 0 ) {
    clog << "UFSocket::readable> fd: "<<fd<<strerror(errno);
    if( usetimeout )
      clog << ", timeout: "<<timeOut<<endl;
    else
      clog<<endl;
  }
  if( fdcnt == 0 ) {
  if( UFSocket::_verbose )
    clog << "UFSocket::readable> timed-out, no input yet, fdcnt: "<<fdcnt<<endl;
    return fdcnt;
  }
 
  // if fd is selected, test it?
  // this may not be used while accepting a connection
  // on a listen socket (linux, cygwin solaris)?
  if( fdcnt > 0 && FD_ISSET(fd, &readfds) ) {
    if( fd != _sockinfo.listenFd ) {
      int pcnt = peekCnt(timeOut, fd);
      if( _verbose )
        clog<<"UFSocket::readable> peek cnt: "<<pcnt<<endl;
      if( pcnt > 0 ) return pcnt;
    }
  }

  return 0;
} // readable

int UFSocket::pendingInput(socketFd fd, float timeOut) { return readable(timeOut, fd); }

// like the java feature, return # bytes available to be read from socket/fd
int UFSocket::available(float wait, int trycnt, int fd) {
  int retval= 0;
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog<<"UFSocket::available> not initialized or no longer open"<<endl;
    return -2;
  }
  if ( !isSock(fd) ) {
    clog<<"UFSocket::available> not a socket Fd: "<< fd <<endl;
  }
  // readable should return available peekCnt for socket fd:
  // rather than using ioctl, use peekCnt...
  int peekcnt = readable(wait, fd);
  if( _verbose )
    clog<<"UFSocket::available> readable peekcnt: "<<peekcnt<<", soc: "<<description()<<endl;
#if defined(CYGWIN) 
  retval = peekcnt;
#else
  // this is redundant if readable invoked peekCnt which in tern invokes this,
  // but may help deal with socket errors...
  retval = UFRuntime::available(fd, wait, trycnt);
  if( _verbose && retval != peekcnt )
    clog<<"UFSocket::available> ret: "<<retval<<", peekCnt: "<<peekcnt<<endl;
  if( retval <= 0 ) {
    if( _verbose )
      clog<<"UFSocket::available> ret: "<<retval<<", "<<strerror(errno)<<", fd= "<<fd<<endl;
  }
#endif
  return retval;
}

int UFSocket::writable(float timeOut, socketFd fd) { 
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog<<"UFSocket::writable> not initialized or no longer open"<<endl;
    return -3;
  }
  if ( !isSock(fd) ) {
    clog<<"UFSocket::writable> not a socket Fd: "<<fd<<endl;
    return -2;
  }
  else if( _verbose )
    clog<<"UFSocket::writable> select poll on socket Fd: "<<fd<<endl;

  fd_set writefds, excptfds;
  FD_ZERO(&writefds); FD_ZERO(&excptfds);
  FD_SET(fd, &writefds); FD_SET(fd, &excptfds);

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  int fdcnt= -1, cnt= UFPosixRuntime::_MaxInterrupts_;
  do {
    // since select may modify this area of memory, always provide fresh input:
    struct timeval to, *top= 0;
    if( usetimeout ) { to = *usetimeout; top = &to; }
    errno = 0;
    fdcnt = ::select(1+fd, 0, &writefds, &excptfds, top);
    //fdcnt = select(FD_SETSIZE, 0, &writefds, &excptfds, top);
  } while( errno == EINTR && --cnt >= 0 );

  if( fdcnt < 0 ) {
    if( errno == EBADF ) 
      clog << "UFSocket::writable> invalid (EBADF) socket fd= "<<fd<<endl;
    else 
      clog << "UFSocket::writable> error occured on select" << endl;
    return fdcnt;
  }
  if( fdcnt == 0 ) {
    clog << "UFSocket::writable> timed-out, write may block..." << endl;
    return fdcnt;
  }
  if( FD_ISSET(fd, &excptfds) ) {
    clog << "UFSocket::writable> exception present on Fd: "<<fd<< endl;
    return -1;
  }
  //clog << "UFSocket::writable> ok" << endl;
  return abs(FD_ISSET(fd, &writefds));
}

// this is a static function (so it can be used on external Fds)...
// if timeOut is supplied, block until timed-out or interrupted
// if fd is not supplied, select on ALL open Fds, except
// stdin, stdout & stderr, and any non- socket;
// its behavior is similar to readable()
int UFSocket::pendingIO(float timeOut, socketFd fd) {
  fd_set readfds, excptfds;
  FD_ZERO(&readfds); FD_ZERO(&excptfds);

  int fdmax = FD_SETSIZE-1; // All Fds if fd < 0
  // figure out what fdmax is from open Fds using dup
  // on stderror:
  int fdse = fileno(stderr);
  int dupM = dup(fdse);
  if( dupM < 0 ) { // max Fds reached?
    clog<<"UFSocket::pendingIO> dup returned dupM= "<<dupM<<ends;
    dupM = FD_SETSIZE;
    clog<<", reset dubM= "<<dupM<<endl;
  }
  if( fd >= 0 && fd <= fdmax ) { // just check the specified fd
    // don't bother unless fd is really a socket:
    if( ! isSock(fd) )
      return 0;
    fdmax = fd; // set fdmax
    FD_SET(fd, &readfds); FD_SET(fd, &excptfds);
    clog<<"UFSocket::pendingIO> select on fd= "<<fd<<endl;
  }
  else { // check all/any open Fds
    //fdmax = dupM - 1; // set fdmax to the Fd below the duplicate of stderr
    for( int i = 1+fdse ; i <= fdmax; ++i ) {
      if( isSock(i) ) {
	FD_SET(i, &readfds); FD_SET(i, &excptfds);
      }
    }
    if( _verbose )
      clog<<"UFSocket::pendingIO> select all open Fds; fdmax= "<<fdmax<<endl;
  }

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  int fdcnt= -1, trycnt= 3;
  struct timeval to, *pto= 0;
  do {
    // since select may modify this area of memory, always provide fresh value:
    if( usetimeout ) { to = *usetimeout; pto = &to; }
    errno = 0; // clear last error
    fdcnt = ::select(1+fdmax, &readfds, 0, &excptfds, pto);
  } while( fdcnt >= 0 && errno == EINTR && --trycnt >= 0 ); // ignore iterrupts?

  if( fdcnt < 0 ) {
    clog << "UFSocket::pendingIO> error occured on select (fdmax: " << fdmax<< "): "
	 << strerror(errno) << endl;
  }
  if( 0 == fdcnt ) {
    clog << "UFSocket::pendingIO> timed-out, no IO pending on fd= "<<fd<<endl;
  }

  // free the dup'ed fd for the next iteration
  int dupfree = ::close(dupM);
  if( dupfree < 0 ) {
    clog << "UFSocket::pendingIO> error free dup'ed fd, "<<strerror(errno)<<endl;
  }

  for( int i = 1+fdse ; i <= fdmax; ++i ) {
    if( FD_ISSET(i, &readfds) )
      clog<<"UFSocket::pendingIO> select READ true for fd=  "<<i<<endl;
    if( FD_ISSET(i, &excptfds) )
      clog<<"UFSocket::pendingIO> select EXCEPTION for fd=  "<<i<<endl;
  }

  if( fd >= 0 && fd <= fdmax ) { // just check the specified fd
    if( FD_ISSET(fd, &excptfds ) )
      return -1; // return error
    else
      return abs(FD_ISSET(fd, &readfds));
  }
  // checked all/any and return cnt:
  return fdcnt;
} // pendingIO returns -1, 0, or abs(FD_ISSET(fd, &readfds));

int UFSocket::waitOnAll(float timeOut) {
  fd_set readfds, excptfds;
  FD_ZERO(&readfds); FD_ZERO(&excptfds);

  struct timeval *infinit=0; // tenthsec = { 0L, 100000L }; // 0.1 sec.
  struct timeval *usetimeout= infinit, timeout, poll = { 0, 0 }; // don't block
  if( timeOut > -0.00000001 && timeOut < 0.00000001) {
    usetimeout = &poll; // ~zero timout
  } 
  else if( timeOut >  -0.00000001 ) {
    timeout.tv_sec = (long) floor(timeOut);
    timeout.tv_usec = (unsigned long) floor(1000000*(timeOut-timeout.tv_sec));
    usetimeout = &timeout;
  }

  int fdcnt= 0;
  struct timeval to, *pto= infinit;
  // since select may modify this area of memory, always provide fresh value:
  if( usetimeout ) { to = *usetimeout; pto = &to; }
  fdcnt = ::select(FD_SETSIZE, &readfds, 0, &excptfds, pto); // allow interrupts

  if( fdcnt < 0 ) {
    clog << "UFSocket::waitOnAll> error or iterrupt on select: "
	 << strerror(errno) << endl;
    return fdcnt;
  }
  if( _verbose && fdcnt == 0 ) {
   clog << "UFSocket::waitOnAll timed-out? "<<endl;
  }
  /* force an exception?
  for( int fd = 0 ; fd < FD_SETSIZE; ++fd ) {
    if( FD_ISSET(fd, &readfds) ) {
      unsigned char c= '!';
      int nr = ::recv(fd, &c, 1, MSG_PEEK);
      clog<<"UFSocket::pendingIO> MSG_PEEK on fd: "<<fd<<", nr: "<<nr<<", c: "<<c<<endl;
    }
    if( usetimeout ) { to = *usetimeout; pto = &to; }
    fdcnt = ::select(FD_SETSIZE, &readfds, 0, &excptfds, pto); // allow interrupts
    if( FD_ISSET(fd, &excptfds) )
      clog<<"UFSocket::pendingIO> EXCEPTION on fd: "<<fd<<endl;
  }
  */
  // checked all/any and return cnt:
  return fdcnt;
} // waitOnAll

// close entire table and clear it (shutdown)
int UFSocket::closeAndClear(UFSocket::ConnectTable& connections, pthread_mutex_t* mutex) {
  size_t nc = connections.size();
  if( nc <= 0 )
    return 0;
  
  if( mutex ) 
    if( ::pthread_mutex_lock(mutex) < 0 ) 
      return -1;
  
  UFSocket::ConnectTable::iterator i = connections.begin();
  int cnt;
  for( cnt= 0; i != connections.end(); ++i, ++cnt ) {
    string client = i->first; 
    UFSocket* socp = i->second; 
    if( socp != 0 ) {
      //if( _verbose )
        clog<<"UFSocket::closeAndClear> closing: "<<client<<", on soc: "<<socp->description()<<endl;
      if( socp ) socp->close();
      delete socp; 
    }
  }
  //connections.clear();
  
  if( mutex )
    if( ::pthread_mutex_unlock(mutex) < 0 )
      return -2;
 
  return cnt;
}

int UFSocket::findStale(const UFSocket::ConnectTable& connections, vector<string>& stale, pthread_mutex_t* mutex) {
  stale.clear();
  size_t nc = connections.size();
  if( nc <= 0 )
    return 0;

  if( _verbose )
    clog<<"UFSocket::findStale> checking for stale connections, nc: "<<nc<<endl;
  
  if( mutex )
    if( ::pthread_mutex_lock(mutex) < 0 )
      return -1;
  
  UFSocket::ConnectTable::const_iterator i = connections.begin();
  for( ; i != connections.end(); ++i ) {
    string name = i->first; 
    UFSocket* socp = i->second; 
    if( socp != 0 ) {
      if( !socp->validConnection() ) {
        //if( _verbose )
	  clog<<"UFSocket::findStale> invalid connection: "<<name<<" stale..."<<endl;
        stale.push_back(name);
      }
      else if( _verbose ) {
	clog<<"UFSocket::findStale> still connected: "<<name<<endl;
      }
    }
  }
  
  if( mutex )
    if( ::pthread_mutex_unlock(mutex) < 0 )
      return -2;
 
  return (int)stale.size();
}

// close invalid/stale connections (used with above)
int UFSocket::closeAndClear(UFSocket::ConnectTable& connections, vector<string>& stale, pthread_mutex_t* mutex) {
  int cnt = (int)stale.size();
  
  if( mutex )
    if( ::pthread_mutex_lock(mutex) < 0 )
      return -1;
  
  vector< string > erasable;
  for( int i = 0; i < cnt; i++ ) {
    string client = stale[i];
    UFSocket* socp = connections[client];
    if( client.empty() || socp == 0 ) {
      clog<<"UFSocket::closeAndClear> (noname/nosoc) skip client name: "<<client<<", soc: "<<(size_t)socp<<endl;
      continue;
    }
    //if( _verbose )
      clog<<"UFSocket::closeAndClear> closing: "<<client<<" , on soc: "<<socp->description()<<endl;
    socp->close(); delete socp;
    erasable.push_back(client);
  }
  
  for( size_t i = 0; i < erasable.size(); ++i ) {
    string client = erasable[i];
    //if( _verbose )
      clog<<"UFSocket::closeAndClear> rm/erase from connection table: "<<client<<endl;
    connections.erase(client);
  }
  
  if( mutex )
    if( ::pthread_mutex_unlock(mutex) < 0 )
      return -2;
  
  stale.clear();
  return cnt;
}

// close a stale connection
int UFSocket::closeAndClear(UFSocket::ConnectTable& connections, UFSocket*& stale, pthread_mutex_t* mutex) {
  size_t sz = connections.size();
  if( sz <= 0 ) return sz;
  if( mutex )
    if( ::pthread_mutex_lock(mutex) < 0 )
      return -1;
  UFSocket::ConnectTable::const_iterator i = connections.begin();
  string name; 
  UFSocket* socp= 0; 
  for( ; i != connections.end(); ++i ) {
    name = i->first; 
    socp = i->second; 
    if( socp == 0 ) continue;
    if( socp == stale ) {
      //if( _verbose )
        clog<<"UFSocket::closeAndClear> closing stale connection: "<<name<<endl;
      socp->close(); 
      delete socp; 
      socp = stale = 0;
      break;
    }
  }
  if( !name.empty() ) connections.erase(name);
  if( mutex )
    if( ::pthread_mutex_unlock(mutex) < 0 )
      return -2;
 
  if( name.empty() ) {
    clog<<"UFSocket::closeAndClear> (stale) connection not found in table? name: "
        <<name<<", closing non-tabled socket..."
        <<stale->description()<<endl;
    stale->close(); 
    delete stale; 
    stale = 0;
  }
  return 1;
}

///////////////////////////////// all send signatures ////////////////////////////////////

UFSocket::ThreadArgs::~ThreadArgs() { 
  // do not delete the soc*, assume signal handerl closeAndClear called eventually... 
  if( _msgs ) {
    for( int i= 0; i < (int) _msgs->size(); ++i )
      delete (*_msgs)[i];
  }
  delete _msgs;
}

void* UFSocket::_sendThread(void* p) {
  int exitstat;
  if( p == 0 ) {
    clog<<"UFSocket::_sendThread> null arg!"<<endl;
    ::pthread_exit(&exitstat);
    return p;
  }

  UFSocket::ThreadArgs* args = static_cast< UFSocket::ThreadArgs* > (p);
  if( args == 0 ) {
    clog<<"UFSocket::_sendThread> no msgs to send!"<<endl;
    ::pthread_exit(&exitstat);
    return 0;
  }
  UFSocket* soc = args->_soc;
  if( soc == 0 ) {
    clog<<"UFSocket::_sendThread> null soc ptr."<<endl;
    delete args; // will destroy all args except the socket, which will be dealt with later?
    ::pthread_exit(&exitstat);
    return 0;
  }
  
  if( soc->lock() < 0 )  // prevent this socket from being close/deleted by another thread  
    return 0;
  
  //if( soc->peerPort() <= 0 || soc->writable() <= 0 ) {
  if( soc->writable() < 0 ) {
    soc->unlock();
    delete args; // will destroy all args except the socket, which will be dealt with later?
    clog<<"UFSocket::_sendThread> soc. not writable?"<<endl;
    ::pthread_exit(&exitstat);
    return 0;
  }

  if( args->_msgs == 0 ) {
    clog<<"UFSocket::_sendThread> no msgs to send!"<<endl;
    delete args; // will destroy all args except the socket, which will be dealt with later?
    soc->unlock();
    return 0;
  }
  if( args->_msgs->size() <= 0 ) {
      clog<<"UFSocket::_sendThread> no msgs to send!"<<endl;
    soc->unlock();
    delete args; // will destroy all args except the socket, which will be dealt with later?
    ::pthread_exit(&exitstat);
    return 0;
  }

  int cnt = (int) args->_msgs->size();
  //if( _verbose )
    clog<<"UFSocket::_sendThread> cnt= "<<cnt<<endl;
  //for( int i = 1; i <= cnt; ++i ) {
  for( int i = 0; i < cnt; ++i ) {
    UFProtocol* ufp = (*(args->_msgs))[i];
    if( ufp != 0 ) {
      //if( _verbose )
        clog<<"UFSocket::_sendThread> i: "<<i<<", type: "<<ufp->typeId()<<", name: "<<ufp->name()<<", elem: "<<ufp->elements()<<" ("<<ufp->description()<<")"<<endl;
      ufp->setSeq(i, cnt);
      //if( soc->peerPort() > 0 && soc->writable() > 0 ) {
      if( soc->writable() >= 0 ) {
	int stat = soc->send(ufp); // ufprotocol object deleted by ThreadArgs dtor...
	if( stat < 0 ) {
	  clog<<"UFSocket::_sendThread> socket closed?"<<endl;
	  break;
	}
      }
      else {
	clog<<"UFSocket::_sendThread> socket not writable?"<<endl;
	//soc->close(); delete soc;
	break;
      }
    }
    else if( _verbose ) {
      clog<<"UFSocket::_sendThread> i: "<<i<<", bad UFProtocol* "<<endl;
    }
  }

  //if( _verbose )
    clog<<"UFSocket::_sendThread> completed. cnt: "<<cnt<<endl;

  soc->unlock();
  delete args; // will destroy all args except the socket, which will be dealt with later
  ::pthread_exit(&exitstat);

  return p;
}

pthread_t UFSocket::sendThread(vector< UFProtocol* >* msgs) {
  if( msgs == 0 || msgs->size() <= 0 ) {
    clog<<"UFSocket::sendThread> UFPorotocl vec null!"<<endl;
    return 0;
  }
  UFSocket::ThreadArgs* args = new (nothrow) UFSocket::ThreadArgs(this, msgs);
  if( args == 0 ) {
    clog<<"UFSocket::sendThread> unable to allocate new thread args, memory exhausted?"<<endl;
    return 0;
  }

  // insure this socket knows it is now being used in threaded app.:
  /*
  bool threaded = true; // ok, create & run send thread
  if( _create(threaded) == false ) {
    clog<<"UFSocket::sendThread> unable to allocate mutex(es), memory exhausted?"<<endl;
    return 0;
  }
  */
  return UFPosixRuntime::newThread(_sendThread, (void*) args);
}

// close should allow any current recv or send to complete:
int UFSocket::close() {
  if( _socmutex )
    if( ::pthread_mutex_lock(_socmutex) < 0 )
      return -1;

  if( _recvmutex )
    if( ::pthread_mutex_lock(_recvmutex) < 0 )
      return -2;

  if( _sendmutex )
    if( ::pthread_mutex_lock(_sendmutex) < 0 )
      return -3;

  _portNo = -1;
  int c = _sockinfo.close();

  if( _socmutex )
    ::pthread_mutex_unlock(_socmutex);

  if( _recvmutex )
    ::pthread_mutex_unlock(_recvmutex);

  if( _sendmutex )
    ::pthread_mutex_unlock(_sendmutex);

  return c;
}

// close should allow any current recv or send to complete:
int UFSocket::closeFully() {
  if( _socmutex )
    if( ::pthread_mutex_lock(_socmutex) < 0 )
      return -1;

  if( _recvmutex )
    if( ::pthread_mutex_lock(_recvmutex) < 0 )
      return -2;

  if( _sendmutex )
    if( ::pthread_mutex_lock(_sendmutex) < 0 )
      return -3;

  _portNo = -1;
  int c = _sockinfo.closeFully();

  if( _socmutex )
    ::pthread_mutex_unlock(_socmutex);

  if( _recvmutex )
    ::pthread_mutex_unlock(_recvmutex);

  if( _sendmutex )
    ::pthread_mutex_unlock(_sendmutex);

  return c;
}

int UFSocket::send(const unsigned char* buf, int nb, socketFd fd) {
  if( buf == 0 ) {
    clog<<"UFSocket::send> null char*!"<<endl;
    return 0;
  }
  if( nb <= 0 ) {
    clog<<"UFSocket::send> sorry, no bytes to send; nb= "<<nb<<endl;
    return 0;
  }
  // support output to an externally defined socket via optional fd
  // for non-external fd (< 0) use UFSocket::UFScketInfo
  if( _sockinfo.fd > 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog << "UFSocket::send> not initialized or no longer open"<<endl;
    return -1;
  }
  if( !isSock(fd) ) {
    clog<<"UFSocket::send> not a socket fd: "<<fd<<endl;
    return -1;
  }
  if( _sendmutex ) {
    if( ::pthread_mutex_lock(_sendmutex) < 0 ) {
      clog<<"UFSocket::send> unable to lock sendmutex..."<<endl;
      return 0;
    }
  }
  if( writable() < 0 ) {
    clog<<"UFSocket::send> bad socket, not writable!"<<endl;
    if( _sendmutex )
      ::pthread_mutex_unlock(_sendmutex);
    return -1;
  }
  //if( _verbose )
  //  clog<<"UFSocket::send> fd: "<<fd<<", first byte: "<<buf[0]<<endl;
  int cnt, maxcnt, nbtowrite = nb;
  maxcnt = cnt = _MaxAttempts_; // limit number of attempts
  int nbresult = 0, offset = 0;
  do {
    nbresult = ::send(fd, reinterpret_cast< const char *>(buf + offset), nbtowrite, 0);
    if( nbresult > 0 ) { nbtowrite -= nbresult; offset += nbresult; }
  } while( --cnt > 0 && nbresult >= 0 && nbtowrite > 0 && writable() >= 0 &&
	  (errno == 0 || errno == EINTR) );

  // if socket is complient with X/Open standard, ::send can return negative of:
  // EMGSIZE, ENOTCONN, EPIPE, ECONNRESET, ENETDOWN, ENETUNREACH, ...
  if( nbtowrite > 0 && errno != 0 && errno != EINTR ) {
    clog << "UFSocket::send> error: " <<strerror(errno)<< ", fd= "<<fd
	 <<", nbtowrite= "<<nbtowrite<<endl;
    if( _sendmutex )
      ::pthread_mutex_unlock(_sendmutex);
    return -(errno);
  }
  if( cnt == 0 ) {
    clog << "UFSocket::send> iterations: " << (maxcnt - cnt)
	 << strerror(errno) << ", nbtowrite= " << endl;
  }
  if( _sendmutex )
    ::pthread_mutex_unlock(_sendmutex);
  return (nb - nbtowrite);
} // basic send

int UFSocket::send(const char* s, socketFd fd) {
  if( s == 0 ) {
    clog<<"UFSocket::send> null char*..."<<endl;
    return 0;
  }  
  int nb = ::strlen(s);
  if( nb == 0 ) {
    clog<<"UFSocket::send> empty string char* ?"<<endl;
  }
  int retval = send(nb, fd); // support sending empty string?
  if( nb > 0 ) retval += send(reinterpret_cast< const unsigned char* >( s ), nb, fd);
  return retval;
}

int UFSocket::send(short val, socketFd fd) {
  u_short netord = static_cast< u_short >( val );
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  netord = ::htons(static_cast< u_short >( val ));
#endif
  return send( reinterpret_cast< const unsigned char* >( &netord ), sizeof(netord), fd);
}

int UFSocket::send(short* val, int elem, socketFd fd) {
  int retval=0;
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // convert values to network order
  u_short* tmp = reinterpret_cast< u_short* >( val );
  for( int i = 0; i < elem; ++i ) { *tmp = ::htons(*tmp);  tmp++; }
#endif

  retval += send(reinterpret_cast< const unsigned char* >( val ), elem*sizeof(u_short), fd);
  //clog<<"UFSocket::send> sent "<<retval/sizeof(int)<<" ints."<<endl;

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // restore host order
  tmp = reinterpret_cast< u_short* >( val );
  for( int i = 0; i < elem; ++i ) { *tmp = ::ntohs(*tmp);  tmp++; }
#endif
  return retval;
}

int UFSocket::send(int val, socketFd fd) {
  u_long netord = val;
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  netord = ::htonl((u_long) val);
#endif
  return send(reinterpret_cast< const unsigned char* >( &netord ), sizeof(netord), fd);
}

int UFSocket::send(int* val, int elem, socketFd fd) {
  int retval=0;
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // convert values to network order
  //  reverseLongByteOrder( elem, val );
  u_long* tmp = (u_long*) val;
  for( int i = 0; i < elem; i++ ) { *tmp = ::htonl(*tmp);  tmp++; }
#endif

  retval += send(reinterpret_cast< const unsigned char* >( val ), elem*sizeof(u_long), fd);
  //clog<<"UFSocket::send> sent "<<retval/sizeof(int)<<" ints."<<endl;

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // restore host order
  //  reverseLongByteOrder( elem, val );
  tmp = (u_long*) val;
  for( int i = 0; i < elem; ++i ) { *tmp = ::ntohl(*tmp);  tmp++; }
#endif
  return retval;
}

int UFSocket::send(float val, socketFd fd) {
  u_long* tmp = (u_long*) &val;
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  u_long netord = ::htonl(*tmp);
#else
  u_long netord = *tmp;
#endif
  return send(reinterpret_cast< const unsigned char* >( &netord ), sizeof(netord), fd);
}

int UFSocket::send(float* val, int elem, socketFd fd) {
  //clog<<"UFSocket::send> (floats) elem: "<<elem<<endl;
  int nb=0;
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // convert values to network order
  u_long* tmp = reinterpret_cast< u_long* > (val);
  for( int i = 0; i < elem; i++ ) { *tmp = ::htonl(*tmp);  tmp++; }
#endif

  nb += send(reinterpret_cast< const unsigned char* >(val), elem*sizeof(float), fd);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // restore host order
  tmp = reinterpret_cast< u_long* > (val);
  for( int i = 0; i < elem; ++i ) { *tmp = ::ntohl(*tmp);  tmp++; }
#endif
  /*
  clog<<"UFSocket::send> (floats) sent "<<nb/sizeof(float)<<endl;
  for( int i = 0; i < elem; ++i )
    clog<<"UFSocket::send> (floats): "<<i<<"  "<<val[i]<<endl;
  */
  return nb;
}

// write stdc++ string out to socket, returns null on failure
int UFSocket::send(const string* val, int elem, socketFd fd) {
  if( val == 0 ) {
    clog<<"UFSocket::send> null string*..."<<endl;
    return 0;
  }  
  int nb = 0 ;
  const string* s = val ;
  for( int cnt = 0 ; cnt < elem ; ++cnt, ++s ) {
    // use char* signature above
    int ns = val->length() ;
    if( ns == 0 )
     clog<<"UFSocket::send> empty string?"<<endl;
    nb += send(s->c_str(), fd);
  }
  return nb;
}

// support call by reference
int UFSocket::send(const string& val, socketFd fd) { return send(&val, 1, fd); }

// write stdc++ string out to http socket, returns null on failure
int UFSocket::sendHTTP(const string* val, int elem, socketFd fd) {
  int nb = 0 ;
  const string* s = val ;
  for( int cnt = 0 ; cnt < elem ; ++cnt, ++s ) {
    // use char* signature above
    nb += send((unsigned char *)s->c_str(), (int)s->size(), fd);
  }
  return nb;
}

// support call by reference
int UFSocket::sendHTTP(const string& val, socketFd fd) { return sendHTTP(&val, 1, fd); }

// write protocol msg out to socket, returns null on failure
int UFSocket::send(const UFProtocol* msg) {
  return send(*msg);
}

// support call by reference
int UFSocket::send(const UFProtocol& msg) {
  // a protocal message should know how to send itself out
  //clog<<"UFSocket::send> type: "<<msg.typeId()<<", name: "<<msg.name()<<endl;
  int nb = msg.sendTo(*this); 
  if( nb < msg.length() )
    clog<<"UFSocket::send> msg length mismatch? length: "<<msg.length()<<", sent: "<<nb<<endl;

  return nb;
}

////////////////////////////////////////////////////// all recv signatures ///////////////////////////
// might want to check pendingIO before calling this
// because the recv might block...
int UFSocket::recv(unsigned char* buf, int nb, socketFd fd) {
  // support output to an externally defined socket via optional fd
  // for non-external fd (< 0) use UFSocket::UFScketInfo
  if( _sockinfo.fd > 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog << "UFSocket::recv> soc fd not initialized or no longer open"<<endl;
    return -1;
  }
  if( !isSock(fd) ) {
    clog<<"UFSocket::recv> not a socket fd: "<<fd<<endl;
    return -2;
  }
  if( _sendmutex ) {
    if( ::pthread_mutex_lock(_sendmutex) < 0 ) {
      clog<<"UFSocket::recv> unable to lock recvmutex..."<<endl;
      return 0;
    }
  }

// Note: Serious hackery here for Linux
// Works for 2.2.5/12 at least, I hope they don't change the #define
// ==> The #define for MSG_WAITALL can be found in:
// /usr/include/linux/socket.h, but can't be used:
// #if defined(__KERNEL__) || !defined(__GLIBC__) || (__GLIBC__ < 2)

#if !defined( MSG_WAITALL ) && defined( LINUX )
  #define MSG_WAITALL 0x100
#endif

  unsigned int socflags= 0;

#if defined(MSG_WAITALL)
  socflags |= MSG_WAITALL;
#endif
/* eh?
#if defined(MSG_NOSIGNAL)
  socflags |= MSG_NOSIGNAL;
#endif
*/

  int maxcnt, cnt, nbtoread = nb;
  maxcnt = cnt = _MaxAttempts_; // limit number of attempts
  int nbresult = 0, offset = 0;
  do {
    nbresult = ::recv(fd, reinterpret_cast< char* >( buf + offset ), nbtoread, socflags );
    if( nbresult > 0 ) { nbtoread -= nbresult; offset += nbresult; }
#if defined( LINUX )
    if( nbtoread > 0 ) {
      //clog<<"UFSocket::recv> nbtoread= "<<nbtoread<<endl;
      UFPosixRuntime::sleep(0.05);
    }
#endif
  } while( --cnt > 0 && nbresult >= 0 && nbtoread > 0 &&
	  (errno == 0 || errno == EINTR) && readable() > 0 );

  if( cnt == 0 ) {
    clog << "UFSocket::recv> iterations: " << (maxcnt - cnt)
	 << ", " << strerror(errno) << ", nbtoread= " << nbtoread << endl;
  }
  if( _recvmutex )
    ::pthread_mutex_unlock(_recvmutex);

  return (nb - nbtoread);
} // basic recv

// allocate & recv a string (and insure result is null terminated):
int UFSocket::recv(char*& s, socketFd fd) {
  // protocal should be to first read a int that
  // indicates the size of the string that follows.
  int slen = 0;
  int nb = recv(slen, fd);
  if( nb <= 0 )
    return nb;

  // allocate extra byte for null terminator
  char* rdbuff = new (nothrow) char[1+slen];
  if( NULL == rdbuff ) {
    clog << "UFSocket::recv> Memory allocation failure\n" ;
    errno = ENOMEM ;
    return -1 ;
  }
  ::memset(rdbuff, 0, 1+slen);
  nb += recv(reinterpret_cast< unsigned char* >( rdbuff ), slen, fd);
  s = rdbuff;
  return nb;
}

// more recv signatures:
int UFSocket::recv(short& val, socketFd fd) {
  int retval = recv(reinterpret_cast< unsigned char* >( &val ), sizeof(val), fd); 
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = ::ntohs(val);
#endif
  return retval;
}

int UFSocket::recv(short* val, int elem, socketFd fd) {
  int nb = recv(reinterpret_cast< unsigned char* >( val ),
		elem*sizeof(short), fd);
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // restore host order
  u_short* tmp = reinterpret_cast< u_short* >( val );
  for( int i = 0; i < elem; ++i ) {
    tmp[ i ] = ::ntohs( tmp[ i ] ) ;
  }
#endif
  return nb;
}

int UFSocket::recv(int& val, socketFd fd) {
  int retval = recv(reinterpret_cast< unsigned char* >( &val ), sizeof(val), fd); 
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = ::ntohl(val);
#endif
  return retval;
}

int UFSocket::recv(int* val, int elem, socketFd fd) {
  int nb = recv(reinterpret_cast< unsigned char* >( val ), elem*sizeof(int), fd);
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  u_long* tmp = reinterpret_cast< u_long* >( val );
  // restore host order
  for( int i = 0; i < elem; ++i ) {
    tmp[ i ] = ::ntohl( tmp[ i ] );
  }
#endif
  return nb;
}

int UFSocket::recv(float& val, socketFd fd) {
  int retval = recv(reinterpret_cast< unsigned char* >( &val ), sizeof(val), fd); 
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  u_long* tmp = reinterpret_cast< u_long* >( &val );
  val = ::ntohl(*tmp);
#endif
  return retval;
}

int UFSocket::recv(float* val, int elem, socketFd fd) {
  //clog<<"UFSocket::recv> (floats) elem: "<<elem<<endl;
  int nb = recv(reinterpret_cast< unsigned char* >( val ), elem*sizeof(float), fd);
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  // restore host order
  u_long* tmp = reinterpret_cast< u_long* >( val );
  for( int i = 0; i < elem; ++i ) { *tmp = ::ntohl(*tmp);  tmp++; }
#endif
  /*
  clog<<"UFSocket::recv> (floats) nb: "<<nb<<", nb/sizeof(float): "
      <<nb/sizeof(float)<<endl;
  for( int i = 0; i < elem; ++i )
    clog<<"UFSocket::recv> (floats): "<<i<<"  "<<val[i]<<endl;
  */
  return nb;
}

// recv stdc++ string from socket, returns null on failure
int UFSocket::recv(string* val, int elem, socketFd fd) {
  // use char* signature above
  char* tmp= 0;
  string* s= val; 
  int nb= 0, cnt= 0, nbtot= 0;
  do {
    nb = recv(tmp, fd);
    if( 0 == tmp || nb < 0 ) {
      clog << "UFSocket::recv> string recv failure\n" ;
      return nb;
    }
    else {
      string stmp(tmp);
      *s = stmp;
      delete[] tmp;
      s++; cnt++; nbtot += nb;
    }
  } while( cnt < elem && nb >= 0 );

  return nbtot;
}

// support call by reference
int UFSocket::recv(string& val, socketFd fd) { return recv(&val, 1, fd); }

// recv stdc++ string from http socket, returns null on failure
// note that this method is not really thread safe. consider using
// a different recv method if this is a problem
int UFSocket::recvHTTP(string* val, int elem, socketFd fd) {
  char buffer[8192];
  string* s = val;
  int nb=0, cnt=0, nbtot=0;
  unsigned int n=0;

  if( _recvmutex )
    if( ::pthread_mutex_lock(_recvmutex) < 0 )
      return 0;
  // support output to an externally defined socket via optional fd
  // for non-external fd (< 0) use UFSocket::UFScketInfo
  if( _sockinfo.fd > 0 && fd < 0 ) 
    fd = _sockinfo.fd;
  else if( fd < 0 || _portNo < 0 ) {
    clog << "UFSocket::recv> not initialized or no longer open"<<endl;
    if( _recvmutex )
      ::pthread_mutex_unlock(_recvmutex);
    return -1;
  }
  if( !isSock(fd) ) {
    clog<<"UFSocket::recv> not a socket fd: "<<fd<<endl;
    if( _recvmutex )
      ::pthread_mutex_unlock(_recvmutex);
    return -2;
  }

  do {
    string temp = "";
    do {
      memset(buffer,0,8192); // clear buffer of any left-over junk
      do {
	errno = 0;
	nbtot += (nb = ::recv(fd,(char *)buffer,(int)sizeof(buffer),0));
      } while (errno == EINTR);
      if (nb < 0) {
	clog << "UFSocket::recvHTTP> string recv failure\n";
	return nb;
      }
      temp += buffer;
      if (temp.find("</html>") != string::npos) break;
    } while (true);
    if ((n = temp.find("<html>")) != string::npos) temp = temp.substr(n,temp.size()-n);
    *s = temp;
    s++; cnt++;
  } while (cnt < elem && nb >= 0);

  if( _recvmutex )
    ::pthread_mutex_unlock(_recvmutex);

  return nbtot;
}

// support call by reference
int UFSocket::recvHTTP(string& val, socketFd fd) { return recvHTTP(&val, 1, fd); }

// read/restore UFProtocol msg from socket, this creates a new instance on heap, returns null on failure
int UFSocket::recv(UFProtocol*& retval) {
  // virtual ctor:
  UFProtocol* p = UFProtocol::createFrom(*this);
  if( hasError() || p == 0 )
    return 0;

  retval = p;
  clog<<"UFSocket::recv> type: "<<p->typeId()<<", name: "<<p->name();
  return p->length();
}

// read/restore from socket, this reuses instance
int UFSocket::recv(UFProtocol& reuse) {
   // make use of friendship to save current setting
  UFProtocolAttributes pa = *reuse._pa;

  // resets everything except _values ptr 
  int nb = UFProtocol::recvHeader(*this, *reuse._pa);
  if( nb <= 0 ) {
    clog<<"UFSocket::recv> socket recv failed?"<<endl;
    return nb;
  }
  if( pa._type != reuse.typeId() ) {
    clog<<"UFSocket::recv> reuse problem! current type= "<<pa._type
        <<", transmitted type= "<<reuse.typeId()<<endl;
    // consider dynamic recast to new type (use conversion ctor)
    // later...
    return nb;
  }
  // TimeStamp element count & duration have special meaning to support client/server
  // request mechanism (_elem can != 0 while _values == 0 for TimeStamps)
  if( pa._type != UFProtocol::_TimeStamp_ && pa._elem != reuse.elements() ) {
    clog<<"UFSocket::recv> reuse problem! current elements= "<<pa._elem
        <<", transmitted elements= "<<reuse.elements()<<endl;
    // need to check memory allocations,
    // later...
    return nb;
  }

  // virtual recv
  // must complete transaction, otherwise client/server synch is lost
  if( reuse.typeId() != UFProtocol::_TimeStamp_ )
    nb += reuse.recvValues(*this);
  
  //if( hasError() )
  //  return 0;
  if( nb < reuse.length() )
    clog<<"UFSocket::recv(UFProtocol&) nb= "<<nb<<", expected: "<<reuse.length()<<endl;

  return nb;
}

#endif // __UFSocket_cc__
