#if !defined(__UFServerSocket_h__)
#define __UFServerSocket_h__ "$Name:  $ $Id: UFServerSocket.h,v 0.6 2006/02/21 16:57:11 hon Exp $"
#define __UFServerSocket_H__(arg) const char arg##UFServerSocket_h__rcsId[] = __UFServerSocket_h__; 

#include "string"

#include "UFSocket.h" // inherit send, recv, status methods
#include "cstring"

using std::string ;

class UFServerSocket : public UFSocket {
protected:
  string _host;
  string _ipAddr;
  bool _ephem;

public:

  UFServerSocket(int port= -1);

  UFServerSocket(const UFServerSocket& rhs);

  inline virtual ~UFServerSocket() {}

  UFSocketInfo listen(int port= -1, int backlog= 1);

  bool ephemPort(int& port);  // if bound with ephemeral port, kernal provided portNo

  int acceptClient(UFSocket& client, float timeOut= -1.0);

  int acceptClient(UFSocket*& client, float timeOut= -1.0);

  int accept(UFSocket& client);

  int accept(UFSocket*& client);

  bool pendingConnection(float timeOut);

  UFSocket listenAndAccept(int port= -1);
};

#endif // __UFServerSocket_h__
/**
 * A class designed to be used to receive incoming connections.
 */
  /**
   * Default constructor, initializes internal variables and invokes
   * base class constructor.
   *@param port The port on which to listen, default to -1, this
   * does not begin listening on the port.
   */
  /**
   * Deep copy constructor
   *@param rhs The source UFServerSocket to copy.
   */
  /**
   * Destructor, NOOP, rely on base class destructor.
   */
  /**
   * Open, bind, and listen on a new socket on the given port,
   * with the given backlog.
   *@param port The server port no which to listen
   *@param backlog The backlog to pass to listen()
   *@return A UFSocketInfo structure describing the new socket, empty
   * if failure.
   */
  /**
   * Non-blocking accept
   *@param client The socket to populate with information about the new
   * connection, if present.
   *@param timeOut The length of time to wait for a connection to appear
   *@return A non-negative socket descriptor on success, -1 on failure
   */
  /**
   * Non-blocking accept, internally heap allocated UFSocket.
   *@param client The socket to populate with information about the new
   * connection, if present.  The UFSocket will be allocated if the
   * accept succeeds.
   *@param timeOut The length of time to wait for a connection to appear
   *@return A non-negative socket descriptor on success, -1 on failure
   */
  /**
   * Blocking accept, must be provided with values set by listen().
   *@param client The UFSocket describing the socket that is listening
   *@return A non-negative socket descriptor on success, -1 on failure
   */
  /**
   * Blocking accept, uses socket info internal to this object.
   *@param client The UFSocket describing the socket that is listening, will
   * be internally allocated on the heap if accept() succeeds, otherwise NULL.
   *@return A non-negative socket descriptor on success, -1 on failure
   */
  /**
   * This method will continue to listen() and accept() until a new
   * connection is successfully created.
   *@param port The port on which to listen, default to -1 (invalid).
   *@return A non-negative socket descriptor on success, -1 on failure
   */
