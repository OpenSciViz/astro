#if !defined(__UFProtocol_h__)
#define __UFProtocol_h__ "$Name:  $ $Id: UFProtocol.h,v 0.17 2006/02/07 16:55:51 hon Exp $"
#define __UFProtocol_H__(arg) const char arg##Protocol_h__rcsId[] = __UFProtocol_h__;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif

#include "unistd.h"
#include "cerrno"
#include "strings.h"
#include "signal.h"
#include "ctime"
#include "sys/types.h"
#include "sys/stat.h"

#include "cstdio"
#include "cmath"
#include "iostream"
#include "strstream"
#include "vector"
#include "map"
#include "string"

#include "UFSocket.h"
#include "UFRuntime.h"

using namespace std ;

struct UFProtocolAttributes {
  // total transmission lenght byte count
  int _length; //4
  int _type; //8
  int _elem; //12
  unsigned short _seqCnt; //14
  unsigned short _seqTot; //16
  float _duration; //20
  // t-stamp is fixed length; 25th char is null and not included in transaction:
  char _timestamp[25]; //45
  // allocate 128 bytes total for the object "header" in memory (not transmission length!)
  char _name[83]; // again, null terminated and null is not transmitted

  // variable length data buff:
  void* _values;
  
  UFProtocolAttributes();
  UFProtocolAttributes(const UFProtocolAttributes& rhs);
  UFProtocolAttributes& operator=(const UFProtocolAttributes& rhs);
  ~UFProtocolAttributes();

  static int headerSize();
};

class UFProtocol {
  friend class UFSocket;

public:
  typedef std::vector< string > StrVector;
  typedef std::map< string, string > NameTable;

  // allow printout of transactions:
  static bool _verbose,_sendverbose, _recvverbose;
  
  enum MsgTyp { _TimeStamp_=0, 
		_Strings_=1, _ZippedStrings_=-1,
		_Bytes_=2, _ZippedBytes_=-2,
		_Shorts_=3, _ZippedShorts_=-3,  
		_Ints_=4, _ZippedInts_=-4,  
		_Floats_=5, _ZippedFloats_=-5,
		_FlamObsConf_=7,
		_ObsConfig_=8, _TReCSObsConf_=8,
                _FrameConfig_=9, _TReCSFrameConf_=9,
		_ByteFrames_=11, _ZippedByteFrames_=-11,
		_ShortFrames_=12, _ZippedShortFrames_=-12,
		_IntFrames_=13, _ZippedIntFrames_=-13,
		_FloatFrames_=14, _ZippedFloatFrames_=-14,
		_BoeingFrames_=21, _ZippedBoeingFrames_=-21,
		_RaytheonFrames_=31 , _ZippedRaytheonFrames_=-31 };

  
  enum FrameDim { _Boeing128_=128, _Boeing256_=256,
		  _RaytheonW_=320, _RaytheonH_=240,
		  _TReCSW_=320, TReCSH_=240, 
                  _Aires_=128, _Flamingos_=2048 };

  
  enum ClientTyp { _Notify_=-1, _Pull_=0, _Push_=1 };

  enum ExecMode {_Sim_=-1, _Ops_=0, _Test1_=1, _Test2_=2, _Test3_=3};

  // if a timestamp is unnamed, 
  // if an instrument containes 4 hawaii-2 type arrays and an
  // exposure produces up to 4 frames:
  enum { _MinLength_=44, _MaxLength_=_MinLength_+4*4*4*2048*2048 }; 

  virtual ~UFProtocol();

  inline static bool isValid(UFProtocol* ufp) { 
   if( ufp == 0 ) return false;
   if( ufp->_pa == 0 ) return false;
   if( ufp->_pa->_elem > 0 && ufp->_pa->_values == 0 ) return false;
   if( ufp->_pa->_elem <= 0 && ufp->_pa->_values != 0 ) return false;
   return true;
  }

  inline static bool validElemCnt(UFProtocol* ufp) {
    if( !isValid(ufp) ) return false;
    if( ufp->typeId() != 0 && ufp->elements() <= 0 ) return false;
    return true;
  }

  virtual int typeId() const;
  virtual int resetId(int typ);
  virtual int length() const;
  virtual const char* cname() const;
  virtual string name() const;
  virtual string rename(const string& newname);
  virtual string datalabel();
  virtual string relabel(const string& label);
  virtual const char* timestamp() const;
  virtual string timeStamp() const;
  virtual string stampTime(const string& newtime);

  inline virtual string adjTime(float delta) {
    string t = timeStamp(); t = UFRuntime::adjTime(t, delta); return stampTime(t);
  }

  virtual unsigned short seqCnt() const;
  virtual unsigned short seqTot() const;
  virtual void setSeq(unsigned short cnt, unsigned short tot=0);
  virtual float duration() const;
  virtual float setDuration(float d=0.0);

  inline virtual bool isData() const { int typ= abs(typeId()); return (typ >= _Bytes_ && typ <= _Floats_); }

  inline virtual bool isFrmConf() const { return (typeId() == _FrameConfig_); }

  inline virtual bool isNotice() const { return (typeId() == _TimeStamp_); }

  inline virtual bool isStrings() const { 
    //clog<<"UFProtocol::isStrings> typeId: "<<typeId()<<endl;
    return (typeId() == _Strings_);
  }
  inline virtual bool isFITS() const { return ((typeId() == _Strings_) && (valSize() == 80)) ; }

  inline virtual bool isObsConf() const {
    //clog<<"UFProtocol::isObsConf> typeId: "<<typeId()<<endl;
    return ((typeId()==_ObsConfig_) || (typeId()==_FlamObsConf_));
  }

  inline virtual bool isRequest() const { return (duration() < 0.0); }

  virtual string description() const = 0;

  // since people (including myself, over time) keep getting
  // confused about the virtual meaning of the element count
  // let's introduce an alternative name:
  inline virtual int blocks() { return elements(); }

  inline virtual int blockSize() { return numVals() / elements(); }

  // however, due to the new append function, a _blocks counter
  // will need to be added to the attributes to properly support
  // this notion. this also complicates things relating to the possibility
  // of redefining the block-size, appends of fractional blocks, etc.
  // deal with this later...

  virtual int elements() const;
  virtual int numVals() const;
  virtual int valSize(int elemIdx=0) const = 0; 
  virtual const char* valData(int elemIdx=0) const = 0;
  virtual void shallowCopy(const UFProtocol& rhs);
  virtual void deepCopy(const UFProtocol& rhs) = 0;
  virtual void copyVal(char* dst, int elemIdx=0) = 0;
  virtual int size();
  static int sizeOf(const vector< UFProtocol* >& m);
  static int lengthOf(const vector< UFProtocol* >& m);
  static int elementsOf(const vector< UFProtocol* >& m);

  // helpers to send/recv the non-variable parts of the protocol attributes
  static int writeHeader(int fd, const UFProtocolAttributes& pa); 
  static int readHeader(int fd, UFProtocolAttributes& pa);
  static void printHeader(const UFProtocolAttributes* pa); 

  virtual int writeTo(int fd) const = 0;
  virtual int readFrom(int fd) = 0;
  virtual int writeValues(int fd) const = 0;
  virtual int readValues(int fd) = 0;

  static int sendHeader(UFSocket& soc, const UFProtocolAttributes& pa);
  static int recvHeader(UFSocket& soc, UFProtocolAttributes& pa);

  virtual int sendTo(UFSocket& soc) const = 0;
  virtual int sendAsReqTo(UFSocket& soc);
  virtual int recvFrom(UFSocket& soc) = 0;
  virtual int sendValues(UFSocket& soc) const = 0; 
  virtual int recvValues(UFSocket& soc) = 0;

  static UFProtocol* createFrom(int fd);

  virtual int append(const UFProtocol& ufp);
  virtual int append(const UFProtocol* ufp);

  static UFProtocol* createFrom(UFSocket& soc, float timeOut = -1.0);
  static int headerLength(const UFProtocolAttributes& pa);

  inline const UFProtocolAttributes* attributesPtr() const { return _pa; }

  inline const UFProtocolAttributes& attributesRef() const { return *_pa; }

  inline int minLength() const { if( _pa ) return headerLength(*_pa); return _MinLength_; }

  bool _shared,_shallow;

protected:
  UFProtocolAttributes* _pa;
  inline UFProtocol() : _pa(0) {}
  static int writeFully(int fd, const unsigned char* buf, size_t sz);
  static int readFully(int fd, unsigned char* buf, size_t sz);
  static UFProtocol* create(const UFProtocolAttributes& pa);

  // helper performs default allocation & init.
  void _paInit(int typ= _TimeStamp_, const string& name = "_paInit", int len= _MinLength_);
  void _paInit(const UFProtocolAttributes& pa);
  void _paInit(const UFProtocolAttributes* pa);
  void _paInit(int typ, const UFProtocolAttributes* pa);
  void _delete();
};

#endif // __UFProtocol_h__

/**
 * This structure is used as the internal representation
 * of all UFProtocol (and descendent) classes.  This permits
 * serialization due to its fixed dimensions.
 * Need this for shared memory usage, this works for
 * all protocol objects that are fixed length  -- i.e.
 * not for varialbe length strings.
 * All protocal message packets have a transmission/storage length,
 * type, timestamp, name, # of elements, and zero or more values.
 */
  /**
   * Total transmission length of message (byte count) == 
   * sizeof(_timestamp)-1 + sizeof(int -- contains name length) + strlen(_name) +
   * sizeof(_length) + sizeof(_type) + sizeof(_elem) + 2*sizeof(_SeqCnt) +
   * total length of value elements
   */
  /**
   * type must be one of enumation UFProtocol::MsgTyp
   * @see UFProtocol::MsgTyp
   */
  /**
   * (min.) number of _value elements
   * _elem == 0 for timestamp
   * _elem >= 1 for strings or Floats or Frames
   */
  /**
   * Sequence count (1-n of Total) allows grouping of multiple
   * protocol objects 
   */
  /**
   * This is the total number of UFProtocol objects being
   * transferred.
   */
  /**
   * In order to used shared memory, these need to be fixed length
   * (or we need to write a custom STL::Allocator for shared memory use)
   * timesamp: "yyyy:ddd:hh:mm:ss.uuuuuu" format is 24 char string
   * duration of data in seconds
   * normally duration is positive definite (>=0), however, we need
   * to support the concept of an "empty" protocol message with 
   * _elem > 0 to be used as a "client request" object for that protocol object
   * of _elem; so we'll just encode that as a negative _duration?
   */
  /**
   * name is optional for timestamps, could indicate locale
   * all these structure elements can be considererd the header
   */
  /**
   * A generic pointer to the values array.  This is class dependent.
   */
  /**
   * This provides the "in memory size" of the header
   * please do not confuse this with the "transmission length"
   */
/**
 * Abstract base class of message protocal.
 * Subclasses are TimeStamp, Strings, Bytes, Shorts, Ints, Floats, Frames.
 * Table types are meant to be used for client/server connection associations:
 * clients can indicate what type of data they want and whether they
 * will pull or be pushed data...
 */
  /**
   * For test support: operations, simulation, or 
   * test (use case/scenario) modes.
   */
  /**
   * minimum transmission _length should be congruent with
   * _minlength() for empty name
   * and assume new header i/o of fixed/constant length timestring (20+14)
   * maximum length is unlikely to exceed 2 flamingos frames! (units in bytes)
   */
  /**
   * Virtual destructor, handles the deallocation of the internal UFProtocolAttributes
   * instance.
   */
  /**
   * @return true if the given UFProtocol object/pointer is as valid as
   * we can determine
   * @return false otherwise.
   */
  /**
   * @return true if the UFProtocol object is valid and has a valid
   * element count.
   * @return false otherwise.
   */
  /**
   * Fetch type
   * @return The type of the instantiated object.
   */
  /**
   * Set the type of the instantiated object.
   * @param typ The new type of the objcet.
   * @return The new type of the object, or -1 on error.
   */
  /**
   * Retrieve the total length of the UFProtocolAttribute object.
   * @return The size of the UFProtocolAttribute, or -1 if the UFProtocolAttribute
   * instance is invalid.
   */
  /**
   * Fetch name
   * @return A C null terminated string of this object's name.
   */
  /**
   * Fetch name
   * @return A copy of a C++ STL string of this object's name.
   */
  /**
   * Reset name
   * @param newname A C++ STL string of the object's new name.
   * @return The object's (new) name.
   */
  /**
   * dataLabel is any substring at end of name string that is demarked by '||'
   * (if present) i.e. name == "whichever || whatever || dataLabel"
   * @return A copy of a C++ STL string representing the data label.
   */
  /**
   * Set the dataLabel, if present or init. it
   * @param label The new data label for this object.
   */
  /**
   * Fetch timestamp.
   * @return A C null terminated string representing this object's timestamp.
   */
  /**
   * Fetch timestamp.
   * @return A copy of a C++ STL string representing this object's timestamp.
   */
  /**
   * Set timestamp
   * @param newtime The new timestamp for this object.
   * @return Return a copy of the (updated) timestamp.
   */
  /**
   * Adjust timestamp by delta amount.
   * @param delta The amount by which to change the timestamp.
   * @return A copy of the updated timestamp.
   */
  /**
   * Fetch sequence count
   * @return The sequence count, 0 if invalid UFProtocolAttribute.
   */
  /**
   * Fetch sequence total
   * @return The sequence total, 0 if invalid UFProtocolAttribute.
   */
  /**
   * Set sequence count and optionally sequence total as well
   * @param cnt The new sequence count
   * @param tot The new sequence total if nonzero, default to 0.
   */
  /**
   * Return the duration.
   * @return The duration, 0.0 if invalid UFProtocolAttribute.
   */
  /**
   * Set the duration.
   * @param d The new duration, defaults to 0.0
   * @return The new duraction, 0.0 if invalid UFProtocolAttribute.
   */
  /**
   * @return true if the instantiated object is a data type (UFInts, UFShorts, UFBytes, etc)
   */
  /**
   * @return true if the instantiated object is of type _FrameConfig_
   * @see _FrameConfig_
   */
  /**
   * @return true if the instantiated object is of type _TimeStamp_
   * @see _TimeStamp_
   */
  /**
   * @return true if the instantiated object is of type _Strings_
   * @see _Strings_
   */
  /**
   * @return true if the instantiated object is of type _ObsConfig_
   * @see _ObsConfig_
   */
  /**
   * @return true if duration() is less than 0.0, false otherwise.
   */
  /**
   * Retrieve the object's description.
   * This method is pure virtual, it must be defined by at least the first
   * instantiable subclass.
   * @return A copy of a C++ STL string containing the object's description.
   */
  /**
   * Return the element count of this object.
   * @return The element count of this object.
   */
  /**
   * Return the block size, which is equivalent to numVals() / elements()
   * @return The block size, which is equivalent to numVals() / elements()
   */
  /**
   * Fetch element count; an "element" can be a block of one or
   * more values.
   * @return The element count
   */
  /**
   * Number of values should not be confused with number of elements,
   * this must be congruent with the memory allocation addressed by the
   * _values pointer.
   * @return The number of values in this object.
   */
  /**
   * Retrieve the size of the element at the given element index
   * @param elemIdx The index of the element to examine.
   * @return Return size of the element value:
   * either string length, or sizeof(float), sizeof(frame), etc.:
   */
  /**
   * Retrive a pointer to the first byte of data at index elemIdx
   * @param elemIdx The index of the element to examine.
   * @return const char pointer to first byte of element value (data)
   */
  /**
   * Perform a shallow copy of the given UFProtocol source object.
   * This method just copies the UFProtocolAttributes object, without its
   * values array.
   * @param rhs The object source from which to copy values.
   */
  /**
   * Perform a deep copy of the given UFProtocol source object.
   * This method copies the UFProtocolAttribute object, including
   * its internal values array.
   * This method is pure virtual.
   * @param rhs The UFProtocol source object from which to copy.
   */
  /**
   * Copy internal values to external buff.
   * This method is pure virtual.
   * @param dst The destination buffer
   * @param elemIdx The index of the element to copy.
   */
  /**
   * Retrieve the size of object in memory, not to be confused
   * with transmission length.
   * @return The size of the object in memory
   */
  /**
   * Computes the total size (using UFProtocol::size()) of all UFProtocol
   * objects passed.
   * @param m A vector of pointers to UFProtocol to examine
   * @return The total size of all UFProtocol objects passed.
   */
  /**
   * Computes the total length (using UFProtocol::length()) of all UFProtocol
   * objects passed.
   * @param m A vector of pointers to UFProtocol to examine
   * @return The total length of all UFProtocol objects passed.
   */
  /**
   * Computes the total number of elements (using UFProtocol::elements())
   * of all UFProtocol objects passed.
   * @param m A vector of pointers to UFProtocol to examine
   * @return The total number of elements of all UFProtocol objects passed.
   */
  /**
   * Write the header of a UFProtocolAttributes object.
   * @param fd The file descriptor to which to write
   * @param pa The UFProtocolAttributes object to write
   * @return The number of bytes written.
   */
  /**
   * Read the header of a UFProtocolAttributes object.
   * @param fd The file descriptor from which to read
   * @param pa The UFProtocolAttributes object to read
   * @return The number of bytes read.
   */
  /**
   * Helper to print (clog) the non-variable parts of the protocol attributes
   * @param pa A pointer to the UFProtocolAttributes object to print.
   */
  /**
   * Write msg out to file descriptor.
   * This is a pure virtual method.
   * @param fd The file descriptor to which to write.
   * @return 0 on failure, num. bytes output on success
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * This is a pure virtual method.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Write out only the values array to the given file descriptor.
   * This is a pure virtual method.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * This is a pure virtual method.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Send a UFProtocolAttributes header to a UFSocket
   * @param soc The UFSocket to which to write
   * @param pa The UFProtocolAttributes object to write
   * @return The number of bytes written.
   */
  /**
   * Read a UFProtocolAttributes header from a UFSocket
   * @param soc The UFSocket from which to read
   * @param pa The UFProtocolAttributes object to read
   * @return The number of bytes read.
   */
  /**
   * Write msg out to socket.
   * This is a pure virtual method.
   * @param soc The UFSocket to which to write.
   * @return 0 on failure, num. bytes on success
   */
  /**
   * Set duration to -1.0 and send header to the UFSocket.
   * @param soc The UFSocket to which to write.
   * @return The number of bytes written.
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Creates a new instance on heap, from a file descriptor/
   * This is a virtual constructor.
   * @param fd The file descriptor from which to read the UFProtocol object.
   * @return A new UFProtocol object on the heap, or NULL if failure.
   */
  /**
   * Append to ufp._pa->_values of this (and sum durations).
   * Note that this should cause the block count to increment
   * which means that _blocks needs to be added to the attributes! (later)
   * @param ufp A reference to the source UFProtocol.
   * @return The new values count
   */
  /**
   * Append to ufp._pa->_values of this (and sum durations).
   * Note that this should cause the block count to increment
   * which means that _blocks needs to be added to the attributes! (later)
   * @param ufp A pointer to the source UFProtocol.
   * @return The new values count
   */
  /**
   * Creates a new instance on heap, from a socket.
   * This is a virtual constructor.
   * @param soc The UFSocket from which to read the new UFProtocol object.
   * @param timeOut The length of time (in seconds) to wait for the object
   * to be read; defaults  to infinity -- block until something shows up
   * @return A new (heap allocated) UFProtocol object, or null on failure.
   */
  /**
   * Indicate if shared memory is being used.
   */
  /**
   * Indicate if _pa->_values is a shallow copy.
   */
  /**
   * This is the transmission length of a protocol message header
   * congruent with minimum length of an object
   * @param pa The UFProtocolAttributes object to examine.
   * @return The number of bytes of the UFProtocolAttributes' header.
   */
  /**
   * @return A pointer to this object's const UFProtocolAttributes object.
   */
  /**
   * @return A const reference to this object's UFProtocolAttributes object.
   */
  /**
   * Return the minimum length of this object's header.
   * @return The minimumlength of this object's header.
   * _pa->_length must be >= _minLength!
   * strlen(_pa->_name) (could be 0) +  
   * strlen(_pa->_timestamp) + sizeof(int) (the name string lengths)
   * + sizeof(_pa->_type) + sizeof(_pa->_elem) + sizeof(_pa->_length) 
   * + sizeof(_pa->seqCnt) + sizeof(_pa->seqTot) + sizeof(float)
   */
  /**
   * This is the internal representation of this object.
   * Make this a pointer so that all attributes can be
   * stored in shared memory.
   */
  /**
   * Attempt to write the all sz bytes of buf to fd.
   * This assumes blocking (synchronous) I/O.
   * @param fd The file descriptor to which to write
   * @param buf The buffer of bytes to write
   * @param sz The number of bytes to write
   * @return Number of bytes written, or -1 on error.
   */
  /**
   * Attempt to read the all sz bytes from fd into buf
   * This assumes blocking (synchronous) I/O.
   * @param fd The file descriptor from which to read
   * @param buf The buffer of bytes into which to read
   * @param sz The number of bytes to read
   * @return Number of bytes read, or -1 on error.
   */
  /**
   * Virtual constructor.
   * This method must have knowledge of all possible descendents in hierarchy.
   * @param pa The UFProtocolAttributes object from which to create a UFProtocol object.
   * @return A new UFProtocol object, or NULL on failure.
   */
  /**
   * Initialize this object's UFProtocolAttributes object.
   * _shared and _shallow default to false
   * @param typ The UFProtocol object type, default to _TimeStamp_
   * @param name The name of the UFProtocol object, defalt to "_paInit"
   * @param len The length of the header, default to _MinLength_
   */
  /**
   * Initialize a UFProtocolAttributes object.
   * _shared and _shallow default to false
   * @param pa The UFProtocolAttributes object to initialize.
   */
  /**
   * Initialize a UFProtocolAttributes object.
   * _shared and _shallow default to false
   * @param pa The UFProtocolAttributes object to initialize.
   */
  /**
   * Initialize a UFProtocolAttributes object.
   * _shared and _shallow default to false
   * @param typ The type to which to initialize the UFProtocolAttributes object.
   * @param pa The UFProtocolAttributes object to initialize.
   */
  /**
   * Deallocates this object's UFProtocolAttributes object.
   */
