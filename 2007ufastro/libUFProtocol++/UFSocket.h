#if !defined(__UFSocket_h__)
#define __UFSocket_h__ "$Name:  $ $Id: UFSocket.h,v 0.26 2006/02/21 16:57:12 hon Exp $"
#define __UFSocket_H__(arg) const char arg##Socket_h__rcsId[] = __UFSocket_h__;

#include "sys/types.h"
#include "arpa/inet.h"
#include "pthread.h"
#include "cerrno"

#if defined(LINUX)
#define __restrict 
#include "sys/time.h"
#undef __restrict
#endif
#if defined(SOLARIS)
#include "sys/filio.h"
#include "sys/time.h"
#endif
#if defined(CYGWIN)
#include "fileio.h"
#include "time.h"
#endif

#include "iostream"
#include "new"
#include "map"
#include "string"
#include "vector"

using namespace std ;
class UFProtocol;
const int __DefaultTestServerPortNo_ = 57575;
const int __DefaultImageServerPortNo_ = 57577;
const int __DefaultCommandServerPortNo_ = 57579;
//const char __ETB__ = 0x27;

struct UFSocketInfo {
  int fd;
  int listenFd;
  int maxFd;
  float _timeOut;
  struct sockaddr_in* addr;

  UFSocketInfo(int socFd= -1);
  UFSocketInfo(const UFSocketInfo& rhs);
  inline ~UFSocketInfo() {}

  bool validConnection() const;
  static bool valid(int socFd);
  static bool isSock(int fd);
  int setBlocking(bool blocking= true);
  static int setBlocking(int socFd, bool blocking= true);
  int close();
  int closeFully();
  static string hostIP(const string& hostname);
  //static bool selectable(int fd, float timeOut); // later...

#if !defined(SunOS5_7) && !defined(CYGWIN) || defined(LINUX)
  static string peerIP(int fd);
  static string peerName(int fd);
#endif
};

class UFSocket {
public:
  typedef int socketFd;
  typedef std::map< string, UFSocket* > ConnectTable;
  typedef std::map< UFSocket*, int > AvailTable;
  
  struct ThreadArgs { 
    UFSocket* _soc;
    vector< UFProtocol* >* _msgs;
    inline ThreadArgs(UFSocket* s= 0, vector< UFProtocol* >* m= 0) : _soc(s), _msgs(m) {} 
    virtual ~ThreadArgs();
  };

  static bool _verbose;
  UFSocket(int port= -1, bool threaded= false);
  UFSocket(int fd, int port, bool threaded= false);
  UFSocket(UFSocketInfo connection, int port, bool threaded= false);
  UFSocket(const UFSocket& rhs); 
  virtual ~UFSocket();

  inline int setBlocking(bool block= true) { return _sockinfo.setBlocking(block); }
  inline void setTimeOut(float to) { _sockinfo._timeOut = to; }
  inline float getTimeOut() { return _sockinfo._timeOut; }
  static string ipAddrOf(const string& host);
  inline static bool isSock(int fd) { return UFSocketInfo::isSock(fd); }
  virtual string description() const;
  virtual int close();
  virtual int closeFully();

  virtual int send(const unsigned char* b, int nb, socketFd fd = -1);
  virtual int recv(unsigned char* b, int nb, socketFd fd = -1);

  virtual int send(const char* s, socketFd fd = -1); 
  virtual int recv(char*& s, socketFd fd = -1);

  virtual int send(short val, socketFd fd = -1); 
  virtual int send(short* val, int elem, socketFd fd = -1); 
  virtual int recv(short& val, socketFd fd = -1); 
  virtual int recv(short* val, int elem, socketFd fd = -1);

  virtual int send(int val, socketFd fd = -1); 
  virtual int send(int* val, int elem, socketFd fd = -1); 
  virtual int recv(int& val, socketFd fd = -1); 
  virtual int recv(int* val, int elem, socketFd fd = -1);

  virtual int send(float val, socketFd fd = -1); 
  virtual int send(float* val, int elem,  socketFd fd = -1);
  virtual int recv(float& val, socketFd fd = -1); 
  virtual int recv(float* val, int elem, socketFd fd = -1);

  virtual int send(const string& val, socketFd fd = -1); 
  virtual int send(const string* val, int elem,  socketFd fd = -1);
  virtual int recv(string& val, socketFd fd = -1); 
  virtual int recv(string* val, int elem, socketFd fd = -1);

  virtual int sendHTTP(const string& val, socketFd fd = -1);
  virtual int sendHTTP(const string* val, int elem, socketFd fd = -1);
  virtual int recvHTTP(string& val, socketFd fd = -1);
  virtual int recvHTTP(string* val, int elem, socketFd fd = -1);

  // soc ctor/dtor mutex:
  inline int lock() { if( _socmutex == 0 ) return 0; return ::pthread_mutex_lock(_socmutex); }
  inline int unlock() { if( _socmutex == 0 ) return 0; return ::pthread_mutex_unlock(_socmutex); }

  // convenience func. creates a daemon thread for pocessing a
  // potentially long list of long messages. vector* should be
  // destroyed by the thread: 
  static void* _sendThread(void* p= 0); // pthread
  // sets void* arg via UFSocket::ThreadArgs:
  pthread_t sendThread(vector< UFProtocol* >* msgs);

  // UFProtocol(s) should not support external sockets
  // send individual msgs by pointer or ref:
  virtual int send(const UFProtocol* msg);
  virtual int send(const UFProtocol& msg);
  // read/restore from socket, this creates a new instance on heap,
  // resets msg pointer, sets to null on failure
  virtual int recv(UFProtocol*& msg);
  // allow reuse of protocol object (fixed type & length applications)
  virtual int recv(UFProtocol& msg); 

  inline const UFSocketInfo& getInfo() const { return _sockinfo; }

  int setSocket(UFSocketInfo& sock, bool blocking= true);
  // this resets socket to defaults?
  //int resetSocket(UFSocketInfo& sock, bool blocking= true);
  int socBufSizes(int& snd, int& rcv) const;
  
  static bool isIPAddress(const string& h);

  inline static bool valid(int socFd) { return UFSocketInfo::valid(socFd); }

  inline bool validConnection() const {
    if( _portNo < 0 ) {
      clog<<"UFSocket::validConnection>?bad/unset port: "<<_portNo<<endl;
      //return false;
    }
    return _sockinfo.validConnection();
  }
  
  static int pendingIO(float timeOut= 0.0, socketFd fd = -1);
  
  static int waitOnAll(float timeOut= -1.0);

  bool peekable(float timeOut=0.0, socketFd fd= -1);
  int peekCnt(float timeOut=0.0, socketFd fd= -1);

  // returns -n, 0, or +n 
  int readable(float timeOut= 0.01, socketFd fd= -1) ;

  // now just an alias for readable
  int pendingInput(socketFd fd= -1, float timeOut= 0.0) ;

  // like the java feature, return # bytes available to be read from socket/fd
  int available(float wait, int trycnt, int fd = -1);
  inline int available(int fd = -1) { return available(0.01, 3, fd); }
 
  // returns -n, 0, or n
  int writable(float timeOut= 0.01, socketFd fd= -1) ;
 
  // default forever any negative time
  static int readableList(const vector< socketFd >& socFds,
			  vector< socketFd >& rlist,
			  float timeOut= 0.01); 

  static int availableList(const vector< socketFd >& socFds,
			   std::map< socketFd, int >& socbytes,
			   float timeOut= 0.01); 

  static int writableList(const vector< socketFd >& socs,
			  vector< socketFd >& wlist,
			  float timeOut= 0.01); 

  static int readableList(const UFSocket::ConnectTable& connections,vector< UFSocket* >& rlist, pthread_mutex_t* mutex= 0); 

  static int availableList(const UFSocket::ConnectTable& connections, std::map< UFSocket*, int >& alist, pthread_mutex_t* mutex= 0);

  static int writableList(const UFSocket::ConnectTable& connections, vector< UFSocket* >& wlist, pthread_mutex_t* mutex= 0);

  // when used in threaded service, use mutex, if provide, to synchronize access to connection table
  // close a stale connection
  static int closeAndClear(UFSocket::ConnectTable& table, UFSocket*& stale, pthread_mutex_t* mutex= 0);

  // find all stale connections
  static int findStale(const UFSocket::ConnectTable& table, vector<string>& stale, pthread_mutex_t* mutex= 0);
  // close all stale connections
  static int closeAndClear(UFSocket::ConnectTable& table, vector<string>& stale, pthread_mutex_t* mutex= 0);

  // close entire table and clear it (shutdown)
  static int closeAndClear(UFSocket::ConnectTable& table, pthread_mutex_t* mutex= 0);

  static int hasError(int error_no = errno);

  inline int peerPort() { return _portNo; }

#if !defined(SunOS5_7) && !defined(CYGWIN) || defined(LINUX)
  inline string peerIP() { return UFSocketInfo::peerIP(_sockinfo.fd); }
  inline string peerName() { return UFSocketInfo::peerName(_sockinfo.fd); }
#endif

protected:
  // attributes inherited by ServerSocket and ClientSocket
  // and their respective children:
  enum            { _MaxAttempts_ = 16 }; // max trys for send/recv.
  int 		  _portNo; 
  UFSocketInfo    _sockinfo;
  pthread_mutex_t  *_socmutex, *_recvmutex, *_sendmutex;
  bool _create(bool threaded= false); // helper for ctors & sendThread 
};

#endif // __UFSocket_h__
/**
 * The internal repsentation of a socket.  This is used
 * to support reuse of the UFSocket classes, and completely
 * encapsulates the aggregate variables neecessary to
 * represent a socket.
 */
  /**
   * Initialize the UFSocketInfo object with a given fd
   * @param socFd The initial socket file descriptor, defaults to -1
   */
  /**
   * Deep copy constructor.
   * @param rhs The UFSocketInfo source
   */
  /**
   * Destructor, performs NOOP at this time.
   */
  /**
   * Test if the connection represented by this UFSocketInfo object is
   * valid.
   * @return true if connection is valid
   * @return false otherwise.
   */
  /**
   * Test is the connection represented by the given file descriptor
   * is valid.
   * @return true if connection is valid
   * @return false otherwise.
   */
  /**
   * Test if the given file descriptor corresponds to a socket.
   * @return true if the fd is a socket, false otherwise.
   */
  /**
   * Change the the socket attribute to blocking or non-blocking
   * @param blocking Set to true to set to blocking, false for
   * non-blocking. Defaults to true (blocking).
   * @return Greater than zero if success, -1 on failure
   */
  /**
   * Same as setBlocking() above, but also receives a socket descriptor.
   * @param socFd The socket descriptor to modify
   * @param blocking Set to true to set to blocking, false for
   * non-blocking. Defaults to true (blocking).
   * @return Greater than zero if success, -1 on failure
   */
  /**
   * Close the file descriptor for this object.
   * @return -1 on error, otherwise success.
   */
  /**
   * Close the main file descriptor and the listen file descriptor
   * @return The result of close() on the listenfd
   */
  /**
   * Retrieve a dotted decimal string of the given hostname.
   * @param hostname The hostname for which to find an IP
   * @return The dotted deciman string of the hostname, or
   * an empty string on error.
   */
  /**
   * Lookup the IP of the peer specified by the socket descriptor fd.
   * @param fd The socket descriptor to examine
   * @return A string with dotted decimal of the peer's IP, or empty
   * string if an error occurs.
   */
  /**
   * Lookup the name of the peer specified by the socket descriptor fd.
   * @param fd The socket descriptor to examine
   * @return A string with the name of the peer's IP, or empty
   * string if an error occurs.
   */
/**
 * This class wraps a BSD type socket.
 */
  /**
   * Initialize a new UFSocket object
   * @param port The port number to which it is connected, default to -1
   * @param threaded True if the UFSocket is threaded with other UFSockets,
   * default to false.
   */
  /**
   * Initialize a new UFSocket object
   * @param fd The file descriptor for the new UFSocket
   * @param port The port number to which it is connected, default to -1
   * @param threaded True if the UFSocket is threaded with other UFSockets,
   * default to false.
   */
  /**
   * Initialize a new UFSocket object
   * @param connection The UFSocketInfo structure representing the connection
   * @param port The port number to which it is connected, default to -1
   * @param threaded True if the UFSocket is threaded with other UFSockets,
   * default to false.
   */
  /**
   * Copy a UFSocket.
   * @param rhs The source UFSocket whose values to copy
   */
  /**
   * Destructor.
   */
  /**
   * Change the the socket attribute to blocking or non-blocking
   * @param block Set to true to set to blocking, false for
   * non-blocking. Defaults to true (blocking).
   * @return Greater than zero if success, -1 on failure
   */
  /**
   * Change the timeout value used for select()
   * @param to The new timeout value.
   */
  /**
   * Return the current socket timeout value.
   * @return The current socket timeout value.
   */
  /**
   * Retrieve a dotted decimal string of the given hostname.
   * @param host The hostname for which to find an IP
   * @return The dotted deciman string of the hostname, or
   * an empty string on error.
   */
  /**
   * Test if the given file descriptor corresponds to a socket.
   * @return true if the fd is a socket, false otherwise.
   */
  /**
   * Provide portNo and ip address as one string
   * @return The portNo and IP address
   */
  /**
   * Close the current socket file descriptor.
   * dtors should not call this. app must call this before
   * ufsocket instance is deleted or goes out-of-scope
   * @return The result of UFSocketInfo::close()
   */
  /**
   * Send an array of unsigned char, in its entirety.  This method will
   * continue sending bytes until all nb bytes are sent, or an error
   * occurs.
   * @param b The buffer to write to the socket
   * @param nb The number of bytes to send
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Recv an array of unsigned char, in its entirety.  This method will
   * continue receiving bytes until all nb bytes are read, or an error
   * occurs.
   * Note that the buffer (b) must already be allocated and sized correctly.
   * @param b The buffer into which to read from the socket
   * @param nb The number of bytes to read
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send an array of char, preceeded by its length, in its entirety.  This method will
   * continue sending bytes until all nb bytes are sent, or an error
   * occurs.
   * @param s The buffer to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Recv an array of unsigned char, preceeded by its size, in its entirety.
   * This method will continue receiving bytes until all nb bytes are read,
   * or an error occurs.
   * Note that the buffer (s) will be allocated in this method.
   * @param s The buffer into which to read from the socket
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send a short int, preceeded by its size, in its entirety.  This method will
   * continue sending bytes until all nb bytes are sent, or an error
   * occurs.
   * @param val The value to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Send an array of short, in its entirety.
   * @param val The variable from which to write data
   * @param elem The number of values to write
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Recv a short, in its entirety.
   * @param val The variable into which to read the data
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Recv an array of short, in its entirety.
   * @param val The variable into which to read the data
   * @param elem The number of values to receive
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send an int, preceeded by its size, in its entirety.  This method will
   * continue sending bytes until all nb bytes are sent, or an error
   * occurs.
   * @param val The value to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Send an array of int, in its entirety.
   * @param val The variable from which to write data
   * @param elem The number of values to write
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Recv an int, in its entirety.
   * @param val The variable into which to read the data
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Recv an array of int, in its entirety.
   * @param val The variable into which to read the data
   * @param elem The number of values to receive
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send a float, preceeded by its size, in its entirety.  This method will
   * continue sending bytes until all nb bytes are sent, or an error
   * occurs.
   * @param val The value to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Send an array of float, in its entirety.
   * @param val The variable from which to write data
   * @param elem The number of values to write
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Recv a float, in its entirety.
   * @param val The variable into which to read the data
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Recv an array of float, in its entirety.
   * @param val The variable into which to read the data
   * @param elem The number of values to receive
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send a std::string, preceeded by its size, in its entirety.
   * @param val The value to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Send an array of string, in its entirety.  Each string will be preceeded
   * by its length.
   * @param val The variable from which to write data
   * @param elem The number of values to write
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Receive a std::string, preceeded by its size, in its entirety.
   * @param val The variable into which to read the data
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Recv an array of string, in its entirety.  Each string will be preceeded
   * by its length.
   * @param val The variable into which to read the data
   * @param elem The number of values to receive
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Send a std::string to the socket. 
   * @param val The value to write to the socket
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Send an array of string, in raw form.
   * @param val The variable from which to write data
   * @param elem The number of values to write
   * @param fd The socket descriptor to which to write, defaults to the
   * current socket.
   * @return Number of bytes written, or a negative error code otherwise.
   */
  /**
   * Receive a std::string.  This method will search for a beginning and
   * ending html tags, and strip them off.
   * @param val The variable into which to read the data
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Recv an array of string.  The processing of each std::string will follow
   * the semantics of the first recvHTTP().
   * @param val The variable into which to read the data
   * @param elem The number of values to receive
   * @param fd The socket descriptor from which to read, defaults to the
   * current socket.
   * @return Number of bytes read, or a negative error code otherwise.
   */
  /**
   * Retrieve the UFSocketInfo structure for this UFsocket object.
   * @return This object's socketInfo structure
   */
