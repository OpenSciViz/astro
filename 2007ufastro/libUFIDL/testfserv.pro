; Procedures to test ufIDL.c transaction functions with FrameAcqServer.
; written by Frank Varosi Aug.2000 -> July 2001.

; To compile and use these test/util pros, first connect to FrameAcqServer (ufacqframed) using:
;IDL> testfserv, HOST=host, PORT=port

; include utility UFIO functions for robust socket recv of UFLib objects:
@UFIOutil

;-------- Request, fetch, store, and print FrameConfig structure from frame server -------

pro tfcon, frame_config, VERBOSE=verbose, STATUS=status, QUIET=quiet

  common tfcon, frameconf

	fcon = UFIO_Fetch_FrameConf()

	frame_config = fcon   ;make copy of fcon structure because it is static in libUFIDL.so
	if( fcon.type ne 9 ) then return

	frameconf = fcon   ;make copy of fcon structure because it is static in libUFIDL.so

	if( not keyword_set( quiet ) ) then UFIO_print_FrameConf, fcon, VERBOSE=verbose, STATUS=status

	if( fcon.d ne 32 ) then $
	  print," depth =", fcon.d," bits,  should be 32 bits!"+string(7b)
end

;-------- Request notification from server of every frame grabbed via stream of FrameConfigs:
;         client should recv the stream by: IDL> for i=1,nframes do fcon = UFrecv_FrameConf()

pro tfnotify, nbsent, OFF=off, AGENT=agent

	if keyword_set( agent ) then begin
		if keyword_set( off ) then $
			nbsent = UF_request("NO-NOTIFY",agent) $
		else $
			nbsent = UF_request("NOTIFY",agent)
	 endif else begin
		if keyword_set( off ) then $
			nbsent = UF_request("NO-NOTIFY") $
		else $
			nbsent = UF_request("NOTIFY")
	  endelse
end

;-------- Request data replication stream from server of every frame grabbed
;         client should recv the stream using pro tfrecvDataStream (next, below).

pro tfDataStream, nbsent, OFF=off, AGENT=agent

	if keyword_set( agent ) then begin
		if keyword_set( off ) then $
			nbsent = UF_request("NO-DATASTREAM",agent) $
		else $
			nbsent = UF_request("DATASTREAM",agent)
	 endif else begin
		if keyword_set( off ) then $
			nbsent = UF_request("NO-DATASTREAM") $
		else $
			nbsent = UF_request("DATASTREAM")
	  endelse
end

pro tfrecvDataStream, DISPLAY=display, MIN=dmin, MAX=dmax

  common tfcon, FrameConfig

	oc = UFrecv_ObsConf( osf )
	while( (oc.length LE 0) or (oc.elem LE 0) or (oc.type ne 8) ) do oc = UFrecv_ObsConf( osf )
	fhdr = oc
	fhdr.seqcnt = 0
	fhdr.seqtot = oc.chopBeams * oc.saveSets * oc.nodBeams * oc.nodSets
	help,/st,fhdr

	fc = UFrecv_FrameConf()
	frame = lonarr( fc.w, fc.h )
	print," frameCoadds=",fc.frameCoadds,", chopCoadds=",fc.chopCoadds
	print," frameTime=",fc.frameTime,", savePeriod=",fc.savePeriod
	FrameConfig = fc ;copy into common.

	FITSheader = UFIO_Recv_Strings( phdr )
	help,FITSheader
	tfphdr,phdr
	Nrecvd = 0

	while( fhdr.seqcnt LT fhdr.seqtot AND fhdr.Length GT 0 ) do begin
		npix = UFIO_Recv_LongArray( frame, fhdr )
		if npix GT 0 then Nrecvd = Nrecvd+1
		sname = string(fhdr.name)
		print, sname,"> ",string(fhdr.timestamp), fhdr.seqcnt, fhdr.seqtot, Nrecvd
	       	if keyword_set( display ) and (npix GT 0) then begin
			if strpos( sname, "ref" ) GE 0 then $
			     get_window,1,XS=fc.w+100,YS=fc.h>300,TIT=sname $
			else get_window,0,XS=fc.w+100,YS=fc.h>300,TIT=sname
			tvs, frame, MIN=dmin, MAX=dmax, COL=2,/ERAS, MAG=1
			empty
		   endif
	  endwhile

	FITSheader = UFIO_Recv_Strings( phdr )
	help,FITSheader
	tfphdr,phdr
end
;--------------------------------------------------------------------------------
;+ 
; NAME: 
;	grabrate
; PURPOSE:
;	Compute frame grab rate of an observation.
; CALLING SEQUENCE: 
;	fgrate = grabrate( fc1, fc2, /BYTES ) 
; INPUTS: 
; 	fc1 = FrameConfig (struct) at beginning of observation (or struct from pro tfct).
; 	fc2 = FrameConfig (struct) at end of observation (both can be obtained with pro tfcon).
; KEYWORD PARAMETERS: 
; 	/BYTES : computes rate in Bytes/sec (default is frame/sec).
; 	/MB    : computes rate in MegaBytes/sec (default is frame/sec).
; OUTPUTS: 
;	Function returns the frame grab rate: frames/sec, or bytes/sec if keyword BYTES is set.
; MODIFICATION HISTORY: 
;        Written by: Frank Varosi Oct.2000.
;- 

function grabrate, fc1, fc2, BYTES=bytes, MB=megabytes, DMACNT_USE=useDMAcnt

	ts1 = float( get_words( string( fc1.timestamp ), DEL=":" ) )
	ts2 = float( get_words( string( fc2.timestamp ), DEL=":" ) )

	t1 = ts1[4] + 60*ts1[3] + 3600 * ts1[2]
	t2 = ts2[4] + 60*ts2[3] + 3600 * ( ts2[2] + 24 * (ts2[1]-ts1[1]) )

	if keyword_set( useDMAcnt ) then begin

		if max( tag_names( fc1 ) eq "DMACNT" ) then begin

			Frate = ( fc2.DMAcnt - fc1.DMAcnt )/( t2 - t1 )

		 endif else Frate = ( fc2.DMAcnt )/( t2 - t1 )

	 endif else begin

		if max( tag_names( fc1 ) eq "FRAMEGRABCNT" ) then begin

			Frate = ( fc2.frameGrabCnt - fc1.frameGrabCnt )/( t2 - t1 )

		 endif else Frate = ( fc2.frameGrabCnt )/( t2 - t1 )
	  endelse

	if keyword_set( bytes ) then begin
		return, Frate * (4L * fc2.H * fc2.W)
	 endif else if keyword_set( megabytes ) then begin
		return, Frate * (4L * fc2.H * fc2.W)/1024/1024
	  endif else return, Frate
end

;----Test UFsend_FrameConf and UFrecv_FrameConf ------------

pro tfsr_fc, fc_send, fc_recv

	nbsent = UFsend_FrameConf( fc_send )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	print, string( phdr.name )
	print, message

	fc_recv = UFIO_Fetch_FrameConf()
end

;----Test UFsend_ObsConf and UFrecv_ObsConf ------------

pro tfsr_oc, oc_send, oc_recv, SEND_OBSFLAGS=ofs, RECV_OBSFLAGS=ofr

  common ObsConfig, ObsConfig

	ofs = Obs_BufSeq_Flags( oc_send )

	nbsent = UFsend_ObsConf( oc_send, ofs )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	print, string( phdr.name )
	print, message

	nbsent = UF_request("OC")
	oc_recv = UFrecv_ObsConf( ofr )
	ObsConfig = oc_recv
	print," Ndiff = ", strtrim( fix( total( ofs ne ofr ) ), 2 )
end

pro tfsoc, oc_send, SEND_OBSFLAGS=ofs

	ofs = Obs_BufSeq_Flags( oc_send )

	nbsent = UFsend_ObsConf( oc_send, ofs )

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	print, string( phdr.name )
	print, message
end

pro tfroc, oc_recv, RECV_OBSFLAGS=ofr

  common ObsConfig, ObsConfig

	nbsent = UF_request("OC")
	oc_recv = UFrecv_ObsConf( ofr )
	ObsConfig = oc_recv
end

;-------- Test sending Int frames to server (function UFsend_Ints) -------

pro tfsend, fsrc, fref, WSAV=wsec, WNOD=wnod, FACTOR=srcFac, $
			PSF_FWHM=psf_fwhm, NOISE_FACTOR=rf, SKY=sky, SWAP_BYTES=swapb

	tfroc, ObsConfig
	tfcon, fcon,/VERBOSE

	if N_elements( psf_fwhm ) ne 1 then psf_fwhm = (fcon.w<fcon.h)/3
	if N_elements( srcFac ) ne 1 then srcFac = 2.^10
	if N_elements( sky ) ne 1 then sky=2.^16
	if N_elements( rf ) ne 1 then rf = sqrt(sky)
	fsig = round( srcFac * psf_gaussian( NPIX=[fcon.w, fcon.h], FWHM=psf_fwhm ))
	help, rf, fsig, psf_fwhm, sky, srcFac

	if N_elements( wsec ) ne 1 then wsec = 0.5
	if N_elements( wnod ) ne 1 then wnod = 4.

	for nods=1,ObsConfig.nodsets do begin
	   for nod=1,ObsConfig.nodbeams do begin
	     for nsav=1,ObsConfig.savesets do begin

		bufsend = "src" + strtrim(nod,2)
		fsrc = round( fsig + rf * randomn( seed, fcon.w, fcon.h ) + sky )
		if keyword_set( swapb ) then byteorder, fsrc,/LSWAP
		nsent = UFsend_Ints( fsrc, bufsend )
		print, nods, nod, nsav, "   Nsent =", nsent, "   ", bufsend
		if( nsent ne N_elements( fsrc ) ) then $
			message,"failed to send all elements!"

		bufsend = "ref" + strtrim(nod,2)
		fref = round( rf * randomn( seed, fcon.w, fcon.h ) + sky )
		if keyword_set( swapb ) then byteorder, fref,/LSWAP
		nsent = UFsend_Ints( fref, bufsend )
		print, nods, nod, nsav, "   Nsent =", nsent, "   ", bufsend
		if( nsent ne N_elements( fref ) ) then $
			message,"failed to send all elements!"
		wait, wsec

	      endfor
	     wait, wnod
	    endfor
	 endfor
end

;-------- Fetch buffer names from frame server ------------------

pro tfbnams, bufnams, phdr

	nbsent = UF_request("BN")
	bns = reverse( UFIO_Recv_Strings( phdr, NBSENT=nbsent ) )

	print, string( phdr.name ), "> ", string( phdr.timestamp )
	help, bns
	print," Buffer Names:"
	print, bns
	bufnams = bns
end

;-------- Request and receive Int frames from server using UFIO functions-------
;-------- Note that FrameServer first sends corresponding FrameConfig of the Frame Buffer:

pro tfrecv, Last_frecv, Last_fcon, Last_fhdr, BUF_TO_RECV=bufrecv, ACCUM_BUF=accum_buf, $
			MIN=dmin, MAX=dmax, NTIMES=ntimes, QUIET=quiet, GET_FRAMECONFIG=getfc, $
			WAIT_SEC=wsec, DISPLAY=display, ERRMAX=errmax

  common tfcon, FrameConfig

	if keyword_set( getfc ) then   tfcon, fcon, QUIET=quiet   else  fcon = FrameConfig
	frame = lonarr( fcon.w, fcon.h )

	if N_elements( ntimes ) ne 1 then ntimes = 1
	if N_elements( wsec ) ne 1 then begin
		if ntimes GT 1 then  wsec=1 else wsec=0
	   endif

	if N_elements( bufrecv ) LE 0 then bufrecv = "src1"
	if keyword_set( accum_buf ) then bufrecv = "accum(" + bufrecv + ")"
	nbuf = N_elements( bufrecv )

	for n=1,ntimes do begin
	   for i=0,nbuf-1 do begin

		frameconf = UFIO_Fetch_FrameConf( REQ=bufrecv[i] )

		fcname = string( frameconf.name )
		fcerror = strpos( fcname, "ERROR" ) ge 0
		frame(*)=0

		if (frameconf.type eq 9) and (not keyword_set(quiet)) then begin
			print,  "FrameObsSeqNo=",strtrim( frameconf.FrameObsSeqNo, 2 ), $
				"  CoAdds=",strtrim( frameconf.CoAdds, 2 ), $
				"  ChopBeam=",strtrim( frameconf.ChopBeam, 2 ), $
				"  NodBeam=",strtrim( frameconf.NodBeam, 2 ), $
				"  SaveSet=",strtrim( frameconf.SaveSet, 2 ), $
				"  NodSet=",strtrim( frameconf.NodSet, 2 )
		  endif

		if( frameconf.type ne 9 ) or ( fcerror ) then begin

			fhdr=0
			print,n,i,"  " + fcname + "> " + string( frameconf.timestamp )

		 endif else begin

			nr = UFIO_Recv_LongArray( frame, fhdr )

			if not keyword_set(quiet) then $
			  print,n,i,"  Nrecvd = ",strtrim(nr,2),"  ",bufrecv[i],"  ",string(fhdr.name)

			if strpos( string(fhdr.name), bufrecv[i] ) LT 0 then begin
				message,"did not recv requested buffer!",/INFO
				stop
			   endif

			if( fhdr.type ne 4 ) then message,"frame type not UFInts"+string(7b),/INFO $
			 else begin
	        		if keyword_set( display ) then begin
					tvs, frame, MIN=dmin, MAX=dmax, COL=1
					empty
				   endif
			   endelse
		   endelse

		if keyword_set( errmax ) then begin
			maf = max(abs( frame ))
			if not keyword_set(quiet) then print," max(abs( frame )) =",maf
			if maf gt errmax then stop
		   endif
	    endfor
	   wait, wsec
	 endfor

Last_frecv = frame
Last_fcon = frameconf
Last_fhdr = fhdr
end

;-------- Test send/recv/compare Int frames to/from server:
;                             (functions  UFsend_Ints and  UFrecv_Ints)------

pro tfsendrecv, Last_fsent, Last_frecv, Last_fcon, Last_fhdr, KEEP_RUNNING=keeprun, $
					NTIMES=ntimes, WAIT_SEC=wsec, SWAP_BYTES=swapb, $
					FACTOR_RAND_ADD=rf, BUF_TO_SEND=bufsend
	tfcon, fcon,/VERBOSE,/STATUS
	fs = lonarr( fcon.w, fcon.h )
	frame = fs
	fs[0,0] = round( dist( fcon.w < fcon.h ) )
	help, fs, frame

	if keyword_set( keeprun ) then minfo=1 else minfo=0
	if N_elements( wsec ) ne 1 then wsec = 0.
	if N_elements( ntimes ) ne 1 then ntimes = 1
	if N_elements( rf ) ne 1 then rf=9.

	if N_elements( bufsend ) LE 0 then $
		bufsend = [ "src1", "ref1" ]
	nbuf = N_elements( bufsend )

	for n=1,ntimes do begin
	   for i=0,nbuf-1 do begin

		fsend = round( fs + rf * randomn( seed, fcon.w, fcon.h ) )
		if keyword_set( swapb ) then byteorder, fsend,/LSWAP

		nsent = UFsend_Ints( fsend, bufsend[i] )

		if keyword_set( swapb ) then byteorder, fsend,/LSWAP
		print, n, i, "   Nsent =", nsent, "   ", bufsend[i]
		if( nsent ne N_elements( fsend ) ) then $
			message,"failed to send all elements!"

		frameconf = UFIO_Fetch_FrameConf( REQ=bufsend[i] )

		fcname = string( frameconf.name )
		fcerror = strpos( fcname, "ERROR" ) ge 0
		frame(*)=0

		if ( frameconf.type ne 9 ) or ( fcerror ) then begin
			message,"recvd empty FrameConfig:",/INFO
			print, string( frameconf.name ),": ",bufsend[i]
			fhdr = 0
		 endif else begin
			nrecv = UFIO_Recv_LongArray( frame, fhdr )
			if( fhdr.type ne 4 ) then message,"frame type NE 4"+string(7b),INFO=minfo
		  endelse

		Ndiff = Long( total( frame NE fsend ) )
		print, n, i,"  Nrecvd =",nrecv, "  Ndiff = ",strtrim(Ndiff,2)
	        if Ndiff gt 0 then message,"send/recv error!"+string(7b),INFO=minfo
	    endfor
	   wait, wsec
	 endfor

Last_fsent = fsend
Last_frecv = frame
Last_fcon = frameconf
Last_fhdr = fhdr
end
;--------------------------------------

pro tfphdr, phdr, message
	print, string( phdr.name ) + "> " + string( phdr.timestamp )
	if N_elements( message ) gt 0 then print, message
end

;------- Test send/recv the Pixel Map: -----------

pro tfsr_pm, pm_send, pm_recv, PMphdr, MESSAGE=message, MPHDR=Mphdr

	sz = size( pm_send )

	if sz[0] ne 2 then begin
		message,"first arg. must be the Pixel Mapping array!",/INFO
		return
	   endif

	tfcon,fc,/QUIET

	if (sz[1] ne fc.W) or (sz[2] ne fc.H) then begin
		message,"Pixel Mapping array size does not match FrameConfig!",/INFO
		return
	   endif

	nbsent = UFsend_Ints( pm_send, "PIXELMAP" )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
	if string(Mphdr.name) eq "ERROR" then return

	nbsent = UF_request("PM")
	sz = size( pm_send )
	pm_recv = Lonarr( sz[1], sz[2] )

	print," Nrecv =", UFIO_Recv_LongArray( pm_recv, phdr, NBSENT=nbsent )
	tfphdr,phdr
	PMphdr = phdr
	print," Ndiff = ", strtrim( fix( total( pm_send NE pm_recv ) ), 2 )
end
;-----------------------------------------------

pro tfspm, pm_send, PMphdr, NAME=mapname, GET_FRAMECONFIG=getfc

	sz = size( pm_send )

	if sz[0] ne 2 then begin
		message,"first arg. must be the Pixel Mapping array!",/INFO
		return
	   endif

	tfcon,fc,/QUIET

	if (sz[1] ne fc.W) or (sz[2] ne fc.H) then begin
		message,"Pixel Mapping array size does not match FrameConfig!",/INFO
		return
	   endif

	if NOT keyword_set( mapname ) then mapname = ""

	nbsent = UFsend_Ints( pm_send, "PIXELMAP" + mapname )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	PMphdr = phdr
end

;-----------------------------------------------

pro tfrpm, pm_recv, PMphdr, NAME=mapname, GET_FRAMECONFIG=getfc

  common tfcon, FrameConfig

	if keyword_set( getfc ) then   tfcon, fcon, QUIET=quiet   else  fcon = FrameConfig
	pm_recv = Lonarr( fcon.w, fcon.h )

	if keyword_set( mapname ) then begin
		nbsent = UF_request("PM"+mapname)
	 endif else nbsent = UF_request("PM")

	print," Nrecv =", UFIO_Recv_LongArray( pm_recv, phdr, NBSENT=nbsent )
	tfphdr,phdr
	PMphdr = phdr
end

;------- Test sending new Nbuf_EDTpdv value: -----------

pro tfset_nbuf, Nbuf_EDTpdv, phdr, message

	nbsent = UFsend_Ints( [Long(Nbuf_EDTpdv)], "NBUF_EDTPDV" )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
end

pro tfget_nbuf, Nbuf_EDTpdv, phdr, message

	nbsent = UF_request("NBUFEDT")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Nbuf_EDTpdv = fix( strmid( message, strpos(message,"=")+1 ) )
	Nbuf_EDTpdv = Nbuf_EDTpdv[0]
end

pro tfset_maxtimeouts, maxTimeOuts, phdr, message

	nbsent = UFsend_Ints( [Long(maxTimeOuts)], "MAX_TIMEOUTS" )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
end

pro tfget_maxtimeouts, maxTimeOuts, phdr, message

	nbsent = UF_request("MAX_TIMEOUTS")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	maxTimeOuts = fix( strmid( message, strpos(message,"=")+1 ) )
	maxTimeOuts = maxTimeOuts[0]
end

pro tfdmalag, max_DMA_Lag, phdr, message

	nbsent = UF_request("MAXDMALAG")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	max_DMA_Lag = fix( strmid( message, strpos(message,"=")+1 ) )
	max_DMA_Lag = max_DMA_Lag[0]
end

pro tfset_nodPause, nodPause, phdr, message

	nbsent = UFsend_Ints( [Long(nodPause)], "NODPAUSE" )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
end

pro tfset_Region, Region, phdr, message

	if N_elements( Region ) LT 4 then begin
		print," *** need at least 4 elements for Region"
		return
	   endif

	nbsent = UFsend_Ints( Long(Region), "REGION" )
	print," Nsent = ", strtrim(nbsent,2)

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
end

pro tfget_Region, Region, phdr, message

	nbsent = UF_request("REGION")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
end

;------- Test start/abort: ----------------

pro tfstart, message, Mphdr, TIMEOUT=timeout

	if keyword_set( timeout ) then begin
		nbsent = UF_request( "START  " + strtrim( timeout, 2 ) )
	 endif else nbsent = UF_request("START")

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
end

pro tfabort, message, Mphdr
	nbsent = UF_request("ABORT")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
end

pro tfstop, message, Mphdr
	nbsent = UF_request("STOP")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
end

pro tfkill, message, Mphdr
	nbsent = UF_request("KILL")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
end

pro tfct, CameraType, CTphdr
	nbsent = UF_request("CT")
	nothing = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	CameraType = string( phdr.name )
	tfphdr,phdr
	CTphdr = phdr
end

pro tfversion, FGS_version, Vphdr
	nbsent = UF_request("VERSION")
	nothing = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	FGS_version = string( phdr.name )
	tfphdr,phdr
	Vphdr = phdr
end

pro tfhost, FGS_host, Hphdr
	print,"Local client host = " + ufhostname() + ",  requesting> " + ufhosttime()
	nbsent = UF_request("HOST")
	print,"Local client host = " + ufhostname() + ",   receiving> " + ufhosttime()
	nothing = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	ht = ufhosttime()
	FGS_host = string( phdr.name )
	print,"FrameAcqServer host = " + FGS_host + "           >      "+ string(phdr.timestamp)
	print,"Local client host = " + ufhostname() + ",        done> " + ht
	Hphdr = phdr
end

pro tfiostat, message, Mphdr
	nbsent = UF_request("IOSTAT")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	print, message
	Mphdr = phdr
end

pro tfmp, message, Mphdr, GRABMAP=grabmap

	if keyword_set( grabmap ) then begin
		    nbsent = UF_request("MPG")
	 endif else nbsent = UF_request("MPP")

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	Mphdr = phdr
	print, message
end

pro tfpwait, message, Mphdr
	nbsent = UF_request("FPWAIT")
	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr,phdr
	Mphdr = phdr
	print, message
end

pro tffile, FITS_header, Mphdr, message, APPEND=append, GET_HEADER=geth, NAME=filename, $
	ROOT_DIR=rootDir, QUERY_FITS_HEADER=qfith, CLOSE=close, AUTO_CLOSE=autoclose, HISTORY=history

	if keyword_set( close ) then begin

		if N_elements( FITS_header ) LE 0 then FITS_header="nothing"

		if keyword_set( qfith ) then begin
			nbsent = UFagentSend( FITS_header, "QUERY_FITS_CLOSE" )
		 endif else if keyword_set( append ) then begin
			nbsent = UFagentSend( FITS_header, "APPEND_CLOSE" )
		  endif else $
			nbsent = UFagentSend( FITS_header, "CLOSE" )

	 endif else if keyword_set( rootDir ) then begin

		nbsent = UFagentSend( rootDir, "AUTO_CREATE" )

	 endif else if keyword_set( filename ) then begin

		if keyword_set( qfith ) then begin
			nbsent = UFagentSend( filename, "QUERY_FITS_OPEN" )
		 endif else if keyword_set( autoclose ) then begin
			nbsent = UFagentSend( filename, "OPEN_CLOSE" )
		  endif else $
			nbsent = UFagentSend( filename, "OPEN" )

	  endif else if keyword_set( geth ) then begin

		nbsent = UF_request("FH")

	   endif else if keyword_set( history ) then begin

		nbsent = UF_request("LastFile")

	    endif else if keyword_set( append ) then begin

		nbsent = UFagentSend( FITS_header, "APPEND_HEADER" )

	     endif else nbsent = UFagentSend( FITS_header, "HEADER" )

	message = UFIO_Recv_Strings( phdr, NBSENT=nbsent )
	tfphdr, phdr
	Mphdr = phdr

	if keyword_set( geth ) $
	  and (string( phdr.name ) ne "ERROR") then  FITS_header = message  $
         else  print, message
end

;------- Test all basic frame server commands: ----------------

pro testall

	tfversion	& print," "
	tfhost		& print," "
	tfroc		& print," "
	tfct		& print," "
	tfcon,/S,/V	& print," "
	tfiostat
end

;------- Close frame server connection: ----------------

pro tfclose, status
	status = ufframeclose()
	print, status
end

;------- Connect to frame server: --------------------------

pro testfserv, socfd_fserv, HOST=host, PORT=port
 
 print," Local hostname = ", ufhostname()
 
 if N_elements( host ) ne 1 then host = ufhostname()
 if N_elements( port ) ne 1 then port = 52000

 socfd_fserv = ufframeconnect( host, port )

end
