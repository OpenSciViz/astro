;+
; NAME:
;   Frame_Sim_Send
;
; PURPOSE:
;   Create simulated IR data and send it to FrameAcqServer.
;   First checks and uses the frame, obs, and pixel mapping configurations of server.
;
; CALLING SEQUENCE:
;   Frame_Sim_Send
;
; INPUTS:
;   None.
;
; KEYWORD PARAMETERS:
;
;   /SWAP_BYTES : causes byteorder,/LSWAP to be applied to frames, to simulate EDT/PCI on Sparc arch.
;   COADDS = # coadds per frame, used as factor times sim data, default = 4*4 = 16.
;   NOISE_SIGMA = std.dev. of added noise, default = 2000.
;   RADOFF_FACTOR = radiative offset factor, default = 300.
;   BACKGROUND = sky background offset, default = 3e4.
;   SRC_FACTOR = factor applied to source flux only, default = 1.
;   WAIT_SEC = seconds to wait between savesets, default = 0.
;   WAIT_NOD = seconds to wait between nods, default = 7.
;
; ASSUMPTIONS:
;	Must have a client socket connection with FrameAcqServer (use testfserv.pro).
;
; EXTERNAL CALLS:
;	Calls functions in UFIOutil.pro which call DLMs in libUFIDL.so,
;	that call functions in libUFClient.so and libUFLog.so
;	which should be located in /usr/local/uf$USER/lib.
;	Source code in ~varosi/uf2001/libUFIDL/ufIDL.c
;		and in ~varosi/uf2001/libUF/ufClient*.c
;
; MODIFICATION HISTORY:
;    Written:  F.Varosi Oct.2000.
;    Sim data code from gr_sim.pro which was written by Robert Pina, Aug.1999.
;-
;---------include code for functions:
;                                     UFIO_Fetch_FrameConf
;                                     UFIO_Recv_LongArray
;                                     UFIO_Recv_Strings
@UFIOutil
;         These functions provide an intelligent/interactive method for
;         receiving UFLib objects and handling socket recv timeouts/errors.
;---------------------------------------------------------------------------------------------

pro Frame_Sim_Send, NOISE_SIGMA=sigman, RADOFF_FACTOR=radfact, SRC_FACTOR=sfac, $
			WAIT_SEC=wsec, WAIT_NOD=wnod, SWAP_BYTES=swap_bytes, BACKGROUND=backgd

	if N_elements( radfact ) ne 1 then radfact = 10.
	if N_elements( sfac ) ne 1 then sfac = 0.02
	if N_elements( backgd ) ne 1 then backgd = 5e4
	if N_elements( sigman ) ne 1 then sigman = sqrt( backgd )
	if N_elements( wsec ) ne 1 then wsec = 0
	if N_elements( wnod ) ne 1 then wnod = 7
	message,'Generating and Sending simulated data to FrameAcqServer...',/INFO

    frameconf = UFIO_Fetch_FrameConf()  ;get expected frame size from FrameAcqServer.
    UFIO_print_FrameConf, frameconf,/VERB,/STAT

    xsize   = frameconf.w
    ysize   = frameconf.h
    if( frameconf.d ne 32 ) then $
	  print," depth =", frameconf.d, " bits,  should be 32 bits!" + string(7b)

    print," getting Pixel Map..."
    PixMap = Lonarr(xsize,ysize)
    nbsent = UF_request("PM")
    Nrecvd = UFIO_Recv_LongArray( PixMap, phdr, NBSENT=nbsent )

    if( Nrecvd EQ xsize*ysize ) then begin
	rawpix = 1
	print," using: ",string(phdr.name)
     endif

    ; Parameters for wavefront simulation
    L      = xsize>ysize
    L_not  = 0.8*L
    r_aper = L/6.0
   
    ind = lindgen(xsize,ysize)
    kx  = ind mod xsize    &  kx = kx - xsize/2
    ky  = ind  /  xsize    &  ky = ky - ysize/2

    fac = (( (L/L_not)^2 + kx^2 + ky^2 )^(-11.0/12.0))
   
    nmax = xsize > ysize
    aper = replicate(0.0,nmax,nmax)
    d    = shift(dist(nmax),nmax/2,nmax/2)

    aper[where(d lt r_aper)] = 1.0
    aper = aper[(nmax-xsize)/2:(nmax+xsize-2)/2,(nmax-ysize)/2:(nmax+ysize-2)/2]

    ; Create polynomial surface to simulate radiative offset.
    x = (findgen(xsize) # replicate(1.0,ysize))/(xsize-1.0)
    y = (replicate(1.0,xsize) # findgen(ysize))/(ysize-1.0)

    coeff1 =[  [-8.16426,     36.4623,     17.7096,    -47.1553], $
               [ 23.1350,    -152.443,    -66.6735,     257.818], $
               [-70.7148,     221.515,     166.625,    -431.371], $
               [ 58.2848,    -186.062,     3.72234,     169.728]  ]

    coeff2 =[  [ 5.28957,    -30.3977,    -13.3824,     45.7128], $
               [-22.9504,     155.483,     64.3807,    -260.328], $
               [ 69.3895,    -225.485,    -163.348,     440.134], $
               [-60.9384,     183.233,    -2.54839,    -160.572]  ]

    radoff1 = fltarr(xsize,ysize)
    radoff2 = fltarr(xsize,ysize)
    xo = 3
    yo = 3

    for j=0,yo do begin
      yj=y^j
      for i=0,xo do begin
        radoff1 = temporary(radoff1) + coeff1[i,j]*((x^i)*yj)
        radoff2 = temporary(radoff2) + coeff2[i,j]*((x^i)*yj)
        endfor
      endfor

    radoff1 = radfact*radoff1 + backgd
    radoff2 = radfact*radoff2 + backgd
    frameindex = 0L

	nbsent = uf_Request( "OC" )       ;get Obs.Config. from FrameAcqServer.
	obsconfig = ufRecv_ObsConf( ofr )

	ncoadd = ObsConfig.coaddsPerFrm
	print,"FrameObsSeqTot = ",strtrim( frameconf.frameobsseqtot, 2 ), $
		" : ",string( obsconfig.name ),"> ",string( obsconfig.timestamp )

	zero = Lonarr(xsize,ysize)

	for nods = 1, obsconfig.nodsets do begin

	   for nod = 0, obsconfig.nodbeams-1 do begin

		CASE nod OF
		  0: BEGIN
		   radoffref = radoff2
		   radoffsrc = radoff1
		  END
		  1: BEGIN
		   radoffref = radoff1
		   radoffsrc = radoff2
		  END
		 endcase

		for nsav = 1, obsconfig.savesets do begin

			; Create simulated data
			theta = (2*!PI)*(randomn(seed,xsize,ysize,/uniform)-0.5)
			arg   = fac * complex(cos(theta),sin(theta))
			phase = fft( arg,/inverse )
			src   = sfac * (xsize*ysize) * abs( fft( aper*phase ) )
			noise = sqrt(ncoadd) * sigman * randomn( seed, xsize, ysize )
			src = round( ncoadd*(src + radoffsrc) + noise ) > 0
			noise = sqrt(ncoadd) * sigman * randomn( seed, xsize, ysize )
			ref  = round( ncoadd*radoffref + noise ) > 0

			for chop = 0, obsconfig.chopbeams-1 do begin

				; Send simulated data

				CASE ((chop + nod) MOD 2) OF
			 	  0: BEGIN
					frame = src
					name = "simSRC"
				       END
				  1: BEGIN
					frame = ref
					name = "simREF"
				       END
				 endcase

				if keyword_set( rawpix ) then frame = frame[PixMap]
				if( string( ObsConfig.ReadOutMode ) eq "S1R1") then begin
					frame = [zero,frame]
				   endif
				if keyword_set( swap_bytes ) then byteorder, frame,/LSWAP

				nbsent = UFsend_Ints( frame, name )  ;"ints" in C are 32-bits.

				frameindex = frameindex + 1
				print, frameindex, nods, nod, nsav, chop,"   nbsent =", nbsent
				if( nbsent LT N_elements( frame ) ) then $
						message,"failed to send all elements!"
			   endfor
			wait, wsec
		   endfor
		wait, wnod
	    endfor
	 endfor
end


