MODULE UFIDL
DESCRIPTION Client bindings/interfaces to UF Agents/Servers

VERSION $Name:  $ $Revision: 0.2 $ $RCSfile: libUFIDL.dlm,v $ $State: beta $ $Author: varosi $

BUILD_DATE $Date: 2003/12/30 23:12:12 $

SOURCE University of Florida Infrared Astronomy Group
#
# more complete list of bindings to libUF (Client & Serial):
#
FUNCTION UFhostname	  0 0
FUNCTION UFhostTime	  0 1
FUNCTION UFservices	  1 1
FUNCTION UFannexopen	  2 2 
FUNCTION UFannexclose	  2 2
FUNCTION UFcheckfifo	  1 1
FUNCTION UFchecksocket	  1 1
FUNCTION UFset_timeout	  1 1
FUNCTION UFframeConnect	  2 2
FUNCTION UFframeClose	  0 0
FUNCTION UF_request	  1 2
FUNCTION UFrecv_FrameConf 0 1
FUNCTION UFrecv_obsConf	  0 2
FUNCTION UFrecv_Ints	  2 3
FUNCTION UFsend_FrameConf 1 2
FUNCTION UFsend_ObsConf   2 3
FUNCTION UFsend_Ints	  1 3
FUNCTION UFbytesAvailable 1 1
FUNCTION UFagentConnect	  3 3
FUNCTION UFagentSend	  1 3
FUNCTION UFagentRecv	  1 2
FUNCTION UFagentClose	  1 1
FUNCTION UFagentNames	  0 0
FUNCTION UFttysend	  2 2
FUNCTION UFttyrecv	  1 1


