testfserv,PORT=port,HOST=host

tfsr_fc,fctrecs

occhop=ocs
occhop.nodbeams=1
occhop.nodsets=1
tfsoc,occhop

f=findfile("/var/tmp/Testacq/Test*.fits",COUNT=nf)

tffile,nam="/var/tmp/Testacq/Test" + strtrim(nf+1,2) + ".fits",/AUTO_CLOSE

tfstart

frame_sim_send,/swap,wait_s=0
