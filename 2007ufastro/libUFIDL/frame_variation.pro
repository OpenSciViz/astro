function Frame_Variation, frame

	sz = size(frame)
	fs = frame - min( frame )

	dsum = fltarr( sz[2]-1 )

	for c=0,15 do begin
		sc = total( fs[20*c:20*(c+1)<(sz[1]-1),*], 1 )
		dsum = dsum + abs( sc[1:*] - sc )
	  endfor

return, max( dsum )
end
