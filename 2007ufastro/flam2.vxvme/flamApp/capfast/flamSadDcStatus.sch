[schematic2]
uniq 68
[tools]
[detail]
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos DC Status
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/12
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamSadDcStatus.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2000/11/12
[cell use]
use esirs 1856 1671 100 0 esirs#67
xform 0 2064 1824
p 1920 1632 100 0 1 SCAN:Passive
p 2016 1664 100 1024 -1 name:$(top)CAMABENC
use esirs 1856 839 100 0 esirs#66
xform 0 2064 992
p 1920 800 100 0 1 SCAN:Passive
p 2016 832 100 1024 -1 name:$(top)CAMBDETC
use esirs 1856 1255 100 0 esirs#65
xform 0 2064 1408
p 1920 1216 100 0 1 SCAN:Passive
p 2016 1248 100 1024 -1 name:$(top)BIASVCC
use esirs -192 839 100 0 esirs#62
xform 0 16 992
p -256 544 100 0 0 FTVL:LONG
p 0 640 100 0 0 SNAM:ufSetDatumCnt
p -80 832 100 1024 1 name:$(top)dcDatumCnt
use esirs 544 1255 100 0 esirs#43
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)dcName
use esirs -192 1255 100 0 esirs#42
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)dcHeartbeat
use esirs 1248 1671 100 0 esirs#41
xform 0 1456 1824
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)dcHealth
use esirs 544 423 100 0 esirs#40
xform 0 752 576
p 480 128 100 0 0 FTVL:FLOAT
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)vBias
use esirs -192 1671 100 0 esirs#37
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)detiD
use esirs 544 1671 100 0 esirs#36
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)detType
use esirs -192 423 100 0 esirs#33
xform 0 16 576
p -256 128 100 0 0 FTVL:FLOAT
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)detTempErrorBand
use esirs 544 7 100 0 esirs#31
xform 0 752 160
p 608 -32 100 0 1 SCAN:Passive
p 704 0 100 1024 -1 name:$(top)exposed
use esirs -192 7 100 0 esirs#30
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)elapsed
use esirs 544 839 100 0 esirs#29
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)dataLabel
use esirs 1248 839 100 0 esirs#28
xform 0 1456 992
p 1312 800 100 0 1 SCAN:Passive
p 1408 832 100 1024 -1 name:$(top)bunit
use esirs 1248 1255 100 0 esirs#46
xform 0 1456 1408
p 1312 1216 100 0 1 SCAN:Passive
p 1408 1248 100 1024 -1 name:$(top)dcState
use esirs 1248 423 100 0 esirs#64
xform 0 1456 576
p 1312 384 100 0 1 SCAN:Passive
p 1408 416 100 1024 -1 name:$(top)MCEReply
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadDcStatus.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
