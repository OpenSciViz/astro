[schematic2]
uniq 1195
[tools]
[detail]
w 988 4907 100 0 n#1129 elongouts.elongouts#1036.FLNK 448 5888 992 5888 992 3936 576 3936 576 3840 640 3840 flamLinkEnable.flamLinkEnable#946.ENABLE
w 908 5771 100 0 n#1129 elongouts.elongouts#914.FLNK 896 5760 992 5760 junction
w 684 5579 100 0 n#1129 estringouts.estringouts#919.FLNK 448 5568 992 5568 junction
w 908 5483 100 0 n#1129 elongouts.elongouts#915.FLNK 896 5472 992 5472 junction
w 732 5291 100 0 n#1129 elongouts.elongouts#916.FLNK 448 5312 544 5312 544 5280 992 5280 junction
w 732 4971 100 0 n#1129 elongouts.elongouts#963.FLNK 448 4992 544 4992 544 4960 992 4960 junction
w 684 4683 100 0 n#1129 estringouts.estringouts#1123.FLNK 448 4672 992 4672 junction
w 908 4875 100 0 n#1129 elongouts.elongouts#971.FLNK 896 4864 992 4864 junction
w 892 4579 100 0 n#1129 elongouts.elongouts#1185.FLNK 864 4576 992 4576 junction
w 716 4387 100 0 n#1129 estringouts.estringouts#1125.FLNK 512 4384 992 4384 junction
w 908 4299 100 0 n#1129 estringouts.estringouts#1184.FLNK 896 4288 992 4288 junction
w 1036 4267 100 0 n#1189 estringouts.estringouts#1184.OUT 896 4256 1248 4256 1248 4896 1472 4896 egenSubFLAM.egenSubFLAM#1118.L
w -1092 3075 100 0 n#1187 ecad2.ecad2#1156.VALA -1696 2720 -1600 2720 -1600 3072 -512 3072 -512 3136 320 3136 320 3872 0 3872 0 4688 192 4688 estringouts.estringouts#1123.DOL
w -1060 3043 100 0 n#1186 ecad2.ecad2#1156.STLK -1696 2432 -1568 2432 -1568 3040 -480 3040 -480 3104 352 3104 352 3904 32 3904 32 4656 192 4656 estringouts.estringouts#1123.SLNK
w -2372 3139 100 0 n#1171 ecad2.ecad2#1156.MESS -1696 2880 -1632 2880 -1632 3136 -3040 3136 -3040 2880 -2912 2880 eapply.eapply#1082.INMA
w -2372 3107 100 0 n#1170 ecad2.ecad2#1156.VAL -1696 2912 -1664 2912 -1664 3104 -3008 3104 -3008 2912 -2912 2912 eapply.eapply#1082.INPA
w -2308 2915 100 0 n#1169 eapply.eapply#1082.OUTA -2528 2912 -2016 2912 ecad2.ecad2#1156.DIR
w -2308 2883 100 0 n#1168 eapply.eapply#1082.OCLA -2528 2880 -2016 2880 ecad2.ecad2#1156.ICID
w -2940 3307 100 0 n#1167 eapply.$(top)$(dev)motorApply.OUTH -2816 3776 -2720 3776 -2720 3616 -2944 3616 -2944 3008 -2912 3008 eapply.eapply#1082.DIR
w -2972 3307 100 0 n#1166 eapply.$(top)$(dev)motorApply.OCLH -2816 3744 -2752 3744 -2752 3648 -2976 3648 -2976 2976 -2912 2976 eapply.eapply#1082.CLID
w 380 3499 100 0 n#1172 ecad2.ecad2#1079.VALA -1088 2720 -992 2720 -992 3008 -384 3008 -384 3072 384 3072 384 3936 64 3936 64 4512 480 4512 480 4576 608 4576 elongouts.elongouts#1185.DOL
w 412 3499 100 0 n#1173 ecad2.ecad2#1079.STLK -1088 2432 -960 2432 -960 2976 -352 2976 -352 3040 416 3040 416 3968 96 3968 96 4480 512 4480 512 4544 608 4544 elongouts.elongouts#1185.SLNK
w 476 3499 100 0 n#1175 ecad2.ecad2#1080.STLK -480 2432 -288 2432 -288 2976 480 2976 480 4032 160 4032 160 4368 256 4368 estringouts.estringouts#1125.SLNK
w 444 3499 100 0 n#1183 ecad2.ecad2#1080.VALA -480 2720 -320 2720 -320 3008 448 3008 448 4000 128 4000 128 4400 256 4400 estringouts.estringouts#1125.DOL
w 540 3347 100 0 n#1177 ecad2.ecad2#1081.STLK 192 2432 544 2432 544 4272 640 4272 estringouts.estringouts#1184.SLNK
w 508 3507 100 0 n#1180 ecad2.ecad2#1081.VALA 192 2720 512 2720 512 4304 640 4304 estringouts.estringouts#1184.DOL
w -980 3491 100 0 n#1149 ecad2.ecad2#1095.STLK -1920 3712 -1760 3712 -1760 3488 -128 3488 -128 4768 544 4768 544 4832 640 4832 elongouts.elongouts#971.SLNK
w -980 3523 100 0 n#1148 ecad2.ecad2#1095.VALA -1920 4000 -1728 4000 -1728 3520 -160 3520 -160 4800 512 4800 512 4864 640 4864 elongouts.elongouts#971.DOL
w -196 4251 100 0 n#1147 ecad2.ecad2#1078.STLK -1312 3712 -1152 3712 -1152 3552 -192 3552 -192 4960 192 4960 elongouts.elongouts#963.SLNK
w -228 4283 100 0 n#1146 ecad2.ecad2#1078.VALA -1312 4000 -1120 4000 -1120 3584 -224 3584 -224 4992 192 4992 elongouts.elongouts#963.DOL
w -260 4379 100 0 n#1145 ecad2.ecad2#1077.STLK -704 3712 -256 3712 -256 5056 544 5056 544 5120 640 5120 eaos.eaos#917.SLNK
w -292 4507 100 0 n#1144 ecad2.ecad2#1077.VALB -704 3936 -288 3936 -288 5088 512 5088 512 5152 640 5152 eaos.eaos#917.DOL
w -324 4651 100 0 n#1143 ecad2.ecad2#1077.VALA -704 4000 -320 4000 -320 5312 192 5312 elongouts.elongouts#916.DOL
w -1220 5635 100 0 n#1142 ecad2.ecad2#1031.VALA -2304 4960 -2208 4960 -2208 5632 -160 5632 -160 5888 192 5888 elongouts.elongouts#1036.DOL
w -1188 5603 100 0 n#1141 ecad2.ecad2#1031.STLK -2304 4672 -2176 4672 -2176 5600 -128 5600 -128 5856 192 5856 elongouts.elongouts#1036.SLNK
w -852 5539 100 0 n#1140 ecad2.$(top)$(dev)test.STLK -1696 4672 -1568 4672 -1568 5536 -64 5536 -64 5664 544 5664 544 5728 640 5728 elongouts.elongouts#914.SLNK
w -884 5571 100 0 n#1139 ecad2.$(top)$(dev)test.VALA -1696 4960 -1600 4960 -1600 5568 -96 5568 -96 5696 512 5696 512 5760 640 5760 elongouts.elongouts#914.DOL
w -516 5475 100 0 n#1138 ecad2.ecad2#837.STLK -1088 4672 -960 4672 -960 5472 0 5472 0 5552 192 5552 estringouts.estringouts#919.SLNK
w -548 5507 100 0 n#1137 ecad2.ecad2#837.VALA -1088 4960 -992 4960 -992 5504 -32 5504 -32 5584 192 5584 estringouts.estringouts#919.DOL
w 60 5379 100 0 n#1136 ecad2.ecad2#1076.STLK -480 4672 -352 4672 -352 5376 544 5376 544 5440 640 5440 elongouts.elongouts#915.SLNK
w 28 5411 100 0 n#1135 ecad2.ecad2#1076.VALA -480 4960 -384 4960 -384 5408 512 5408 512 5472 640 5472 elongouts.elongouts#915.DOL
w -2628 3587 100 0 n#1134 ecad2.ecad2#1095.MESS -1920 4160 -1824 4160 -1824 3584 -3360 3584 -3360 3808 -3200 3808 eapply.$(top)$(dev)motorApply.INMG
w -2628 3555 100 0 n#1133 ecad2.ecad2#1095.VAL -1920 4192 -1792 4192 -1792 3552 -3392 3552 -3392 3840 -3200 3840 eapply.$(top)$(dev)motorApply.INPG
w -2260 4395 100 0 n#1132 eapply.$(top)$(dev)motorApply.OCLD -2816 4000 -2528 4000 -2528 4384 -1920 4384 -1920 4448 -1440 4448 -1440 4512 -896 4512 -896 5120 -800 5120 ecad2.ecad2#1076.ICID
w -2292 4419 100 0 n#1131 eapply.$(top)$(dev)motorApply.OUTD -2816 4032 -2560 4032 -2560 4416 -1952 4416 -1952 4480 -1472 4480 -1472 4544 -928 4544 -928 5152 -800 5152 ecad2.ecad2#1076.DIR
w 828 4363 100 0 n#1128 estringouts.estringouts#1125.OUT 512 4352 1216 4352 1216 4928 1472 4928 egenSubFLAM.egenSubFLAM#1118.K
w 988 4523 100 0 n#1127 elongouts.elongouts#1185.OUT 864 4512 1184 4512 1184 4960 1472 4960 egenSubFLAM.egenSubFLAM#1118.J
w 764 4643 100 0 n#1126 estringouts.estringouts#1123.OUT 448 4640 1152 4640 1152 4992 1472 4992 egenSubFLAM.egenSubFLAM#1118.I
w -2084 3363 100 0 n#1116 ecad2.ecad2#1077.VAL -704 4192 -576 4192 -576 3360 -3520 3360 -3520 3968 -3200 3968 eapply.$(top)$(dev)motorApply.INPE
w -2084 3395 100 0 n#1115 ecad2.ecad2#1077.MESS -704 4160 -608 4160 -608 3392 -3488 3392 -3488 3936 -3200 3936 eapply.$(top)$(dev)motorApply.INME
w -2356 3459 100 0 n#1114 ecad2.ecad2#1078.MESS -1312 4160 -1216 4160 -1216 3456 -3424 3456 -3424 3872 -3200 3872 eapply.$(top)$(dev)motorApply.INMF
w -2356 3427 100 0 n#1113 ecad2.ecad2#1078.VAL -1312 4192 -1184 4192 -1184 3424 -3456 3424 -3456 3904 -3200 3904 eapply.$(top)$(dev)motorApply.INPF
w -3332 3035 100 0 n#1110 eapply.eapply#1082.VAL -2528 3008 -2400 3008 -2400 2304 -3328 2304 -3328 3776 -3200 3776 eapply.$(top)$(dev)motorApply.INPH
w -3300 3035 100 0 n#1109 eapply.eapply#1082.MESS -2528 2976 -2432 2976 -2432 2336 -3296 2336 -3296 3744 -3200 3744 eapply.$(top)$(dev)motorApply.INMH
w -2004 5443 100 0 n#1108 ecad2.ecad2#1076.MESS -480 5120 -416 5120 -416 5440 -3520 5440 -3520 4000 -3200 4000 eapply.$(top)$(dev)motorApply.INMD
w -2004 5411 100 0 n#1130 ecad2.ecad2#1076.VAL -480 5152 -448 5152 -448 5408 -3488 5408 -3488 4032 -3200 4032 eapply.$(top)$(dev)motorApply.INPD
w -2628 3843 100 0 n#1106 eapply.$(top)$(dev)motorApply.OUTG -2816 3840 -2368 3840 -2368 4192 -2240 4192 ecad2.ecad2#1095.DIR
w -2612 3811 100 0 n#1105 eapply.$(top)$(dev)motorApply.OCLG -2816 3808 -2336 3808 -2336 4160 -2240 4160 ecad2.ecad2#1095.ICID
w -2084 4259 100 0 n#1101 eapply.$(top)$(dev)motorApply.OCLF -2816 3872 -2400 3872 -2400 4256 -1696 4256 -1696 4160 -1632 4160 ecad2.ecad2#1078.ICID
w -2084 4291 100 0 n#1100 eapply.$(top)$(dev)motorApply.OUTF -2816 3904 -2432 3904 -2432 4288 -1664 4288 -1664 4192 -1632 4192 ecad2.ecad2#1078.DIR
w -1812 4323 100 0 n#1099 eapply.$(top)$(dev)motorApply.OCLE -2816 3936 -2464 3936 -2464 4320 -1088 4320 -1088 4160 -1024 4160 ecad2.ecad2#1077.ICID
w -1812 4355 100 0 n#1098 eapply.$(top)$(dev)motorApply.OUTE -2816 3968 -2496 3968 -2496 4352 -1056 4352 -1056 4192 -1024 4192 ecad2.ecad2#1077.DIR
w -1524 3331 100 0 n#1094 ecad2.ecad2#1081.MESS 192 2880 256 2880 256 3328 -3232 3328 -3232 2688 -2912 2688 eapply.eapply#1082.INMD
w -1524 3299 100 0 n#1093 ecad2.ecad2#1081.VAL 192 2912 224 2912 224 3296 -3200 3296 -3200 2720 -2912 2720 eapply.eapply#1082.INPD
w -1828 3267 100 0 n#1092 ecad2.ecad2#1080.MESS -480 2880 -416 2880 -416 3264 -3168 3264 -3168 2752 -2912 2752 eapply.eapply#1082.INMC
w -1828 3235 100 0 n#1091 ecad2.ecad2#1080.VAL -480 2912 -448 2912 -448 3232 -3136 3232 -3136 2784 -2912 2784 eapply.eapply#1082.INPC
w -1636 2147 100 0 n#1090 eapply.eapply#1082.OCLC -2528 2752 -2304 2752 -2304 2144 -896 2144 -896 2880 -800 2880 ecad2.ecad2#1080.ICID
w -1636 2179 100 0 n#1089 eapply.eapply#1082.OUTC -2528 2784 -2272 2784 -2272 2176 -928 2176 -928 2912 -800 2912 ecad2.ecad2#1080.DIR
w -1332 2115 100 0 n#1088 eapply.eapply#1082.OUTD -2528 2720 -2336 2720 -2336 2112 -256 2112 -256 2912 -128 2912 ecad2.ecad2#1081.DIR
w -1332 2083 100 0 n#1087 eapply.eapply#1082.OCLD -2528 2688 -2368 2688 -2368 2080 -224 2080 -224 2880 -128 2880 ecad2.ecad2#1081.ICID
w -2100 3203 100 0 n#1086 ecad2.ecad2#1079.MESS -1088 2880 -1024 2880 -1024 3200 -3104 3200 -3104 2816 -2912 2816 eapply.eapply#1082.INMB
w -2100 3171 100 0 n#1085 ecad2.ecad2#1079.VAL -1088 2912 -1056 2912 -1056 3168 -3072 3168 -3072 2848 -2912 2848 eapply.eapply#1082.INPB
w -1908 2211 100 0 n#1084 eapply.eapply#1082.OCLB -2528 2816 -2240 2816 -2240 2208 -1504 2208 -1504 2880 -1408 2880 ecad2.ecad2#1079.ICID
w -1908 2243 100 0 n#1083 eapply.eapply#1082.OUTB -2528 2848 -2208 2848 -2208 2240 -1536 2240 -1536 2912 -1408 2912 ecad2.ecad2#1079.DIR
w 700 5251 100 0 n#1049 elongouts.elongouts#916.OUT 448 5248 1024 5248 1024 5120 1472 5120 egenSubFLAM.egenSubFLAM#1118.E
w 1148 5091 100 0 n#1053 eaos.eaos#917.OUT 896 5088 1472 5088 egenSubFLAM.egenSubFLAM#1118.F
w 732 4931 100 0 n#1122 elongouts.elongouts#963.OUT 448 4928 1088 4928 1088 5056 1472 5056 egenSubFLAM.egenSubFLAM#1118.G
w 1260 5027 100 0 n#1047 elongouts.elongouts#971.OUT 896 4800 1120 4800 1120 5024 1472 5024 egenSubFLAM.egenSubFLAM#1118.H
w 1244 5155 100 0 n#1121 elongouts.elongouts#915.OUT 896 5408 1088 5408 1088 5152 1472 5152 egenSubFLAM.egenSubFLAM#1118.D
w 780 5835 100 0 n#1039 elongouts.elongouts#1036.OUT 448 5824 1184 5824 1184 5248 1472 5248 egenSubFLAM.egenSubFLAM#1118.A
w -2820 5251 100 0 n#1035 ecad2.ecad2#1031.MESS -2304 5120 -2240 5120 -2240 5248 -3328 5248 -3328 4192 -3200 4192 eapply.$(top)$(dev)motorApply.INMA
w -2820 5219 100 0 n#1034 ecad2.ecad2#1031.VAL -2304 5152 -2272 5152 -2272 5216 -3296 5216 -3296 4224 -3200 4224 eapply.$(top)$(dev)motorApply.INPA
w -2724 4651 100 0 n#1033 eapply.$(top)$(dev)motorApply.OCLA -2816 4192 -2720 4192 -2720 5120 -2624 5120 ecad2.ecad2#1031.ICID
w -2756 4683 100 0 n#1032 eapply.$(top)$(dev)motorApply.OUTA -2816 4224 -2752 4224 -2752 5152 -2624 5152 ecad2.ecad2#1031.DIR
w 748 5539 100 0 n#1051 estringouts.estringouts#919.OUT 448 5536 1120 5536 1120 5184 1472 5184 egenSubFLAM.egenSubFLAM#1118.C
w 1148 5451 100 0 n#1052 elongouts.elongouts#914.OUT 896 5696 1152 5696 1152 5216 1472 5216 egenSubFLAM.egenSubFLAM#1118.B
w 652 5219 100 0 n#1120 eaos.eaos#917.FLNK 896 5152 928 5152 928 5216 448 5216 448 5120 96 5120 96 5280 192 5280 elongouts.elongouts#916.SLNK
w 1276 4523 100 0 n#1046 estringouts.$(top)$(dev)debug.OUT 896 4032 1024 4032 1024 4224 1280 4224 1280 4832 1472 4832 egenSubFLAM.egenSubFLAM#1118.N
w 1532 3875 100 0 n#1045 flamLinkEnable.flamLinkEnable#946.FLINK 960 3808 1248 3808 1248 3872 1888 3872 1888 4416 1408 4416 1408 4544 1472 4544 egenSubFLAM.egenSubFLAM#1118.SLNK
w -1502 2019 100 0 n#947 eapply.$(top)$(dev)motorApply.FLNK -2816 4352 -2784 4352 -2784 4416 -3552 4416 -3552 2016 608 2016 608 3776 640 3776 flamLinkEnable.flamLinkEnable#946.SLINK
w -2270 5379 100 0 n#858 ecad2.ecad2#837.MESS -1088 5120 -1024 5120 -1024 5376 -3456 5376 -3456 4064 -3200 4064 eapply.$(top)$(dev)motorApply.INMC
w -2270 5347 100 0 n#857 ecad2.ecad2#837.VAL -1088 5152 -1056 5152 -1056 5344 -3424 5344 -3424 4096 -3200 4096 eapply.$(top)$(dev)motorApply.INPC
w -2318 4451 100 0 n#856 eapply.$(top)$(dev)motorApply.OCLC -2816 4064 -2592 4064 -2592 4448 -1984 4448 -1984 4512 -1504 4512 -1504 5120 -1408 5120 ecad2.ecad2#837.ICID
w -2350 4483 100 0 n#855 eapply.$(top)$(dev)motorApply.OUTC -2816 4096 -2624 4096 -2624 4480 -2016 4480 -2016 4544 -1536 4544 -1536 5152 -1408 5152 ecad2.ecad2#837.DIR
w -2542 5315 100 0 n#854 ecad2.$(top)$(dev)test.MESS -1696 5120 -1632 5120 -1632 5312 -3392 5312 -3392 4128 -3200 4128 eapply.$(top)$(dev)motorApply.INMB
w -2542 5283 100 0 n#853 ecad2.$(top)$(dev)test.VAL -1696 5152 -1664 5152 -1664 5280 -3360 5280 -3360 4160 -3200 4160 eapply.$(top)$(dev)motorApply.INPB
w -2116 4811 100 0 n#852 eapply.$(top)$(dev)motorApply.OCLB -2816 4128 -2656 4128 -2656 4512 -2112 4512 -2112 5120 -2016 5120 ecad2.$(top)$(dev)test.ICID
w -2148 4843 100 0 n#851 eapply.$(top)$(dev)motorApply.OUTB -2816 4160 -2688 4160 -2688 4544 -2144 4544 -2144 5152 -2016 5152 ecad2.$(top)$(dev)test.DIR
s 1825 4875 100 0 max values
s 1825 4906 100 0 min values
s 1825 4939 100 0 default values
s 1828 5258 100 0 command mode
s 1826 5228 100 0 socket number
s 1902 1963 210 0 mce4_bias_08.sch 11/19/2001 11:32 AM EST
s 1824 5192 100 0 Power
s 1827 5162 100 0 M (#/volt)
s 1824 5129 100 0 B (#)
s 1824 5099 100 0 volt values
s 1824 5067 100 0 control values
s 1826 5032 100 0 vGate
s 1824 5002 100 0 vWell
s 1824 4970 100 0 vBias
[cell use]
use elongouts 192 5191 100 0 elongouts#916
xform 0 320 5280
p 125 5158 100 0 1 OMSL:closed_loop
p 125 5187 100 768 1 name:$(dc)bidacIDSetW
use elongouts 640 5351 100 0 elongouts#915
xform 0 768 5440
p 573 5314 100 0 1 OMSL:closed_loop
p 573 5340 100 768 1 name:$(dc)biParkW
use elongouts 640 5639 100 0 elongouts#914
xform 0 768 5728
p 573 5603 100 0 1 OMSL:closed_loop
p 573 5629 100 768 1 name:$(dc)biDatumW
use elongouts 192 4871 100 0 elongouts#963
xform 0 320 4960
p 128 4832 100 0 1 OMSL:closed_loop
p 128 4864 100 768 1 name:$(dc)biLatchIDW
use elongouts 640 4743 100 0 elongouts#971
xform 0 768 4832
p 576 4704 100 0 1 OMSL:closed_loop
p 576 4736 100 768 -1 name:$(dc)biRdAllW
use elongouts 192 5767 100 0 elongouts#1036
xform 0 320 5856
p 128 5728 100 0 1 OMSL:closed_loop
p 128 5760 100 768 -1 name:$(dc)biInitW
use elongouts 608 4455 100 0 elongouts#1185
xform 0 736 4544
p 544 4416 100 0 1 OMSL:closed_loop
p 544 4448 100 768 -1 name:$(dc)biDetTempW
use estringouts 192 5479 100 0 estringouts#919
xform 0 320 5552
p 125 5451 100 0 1 OMSL:closed_loop
p 125 5472 100 768 1 name:$(dc)biPowerW
use estringouts 640 3975 -100 0 $(top)$(dev)debug
xform 0 768 4048
p 667 4088 100 0 0 DESC:Debug Mode (NONE,MIN,FULL)
p 752 3968 100 1024 -1 name:$(dc)biDebug
use estringouts 192 4583 100 0 estringouts#1123
xform 0 320 4656
p 128 4544 100 0 1 OMSL:closed_loop
p 128 4576 100 768 -1 name:$(dc)vPasswdW
use estringouts 256 4295 100 0 estringouts#1125
xform 0 384 4368
p 192 4256 100 0 1 OMSL:closed_loop
p 192 4288 100 768 -1 name:$(dc)vWellW
use estringouts 640 4199 100 0 estringouts#1184
xform 0 768 4272
p 576 4160 100 0 1 OMSL:closed_loop
p 576 4192 100 768 -1 name:$(dc)vBiasW
use ecad2 -2016 4583 -100 0 $(top)$(dev)test
xform 0 -1856 4896
p -1920 5056 100 0 0 DESC:Motor Test CAD
p -1920 4896 100 0 0 FTVA:LONG
p -1920 4608 100 0 0 SNAM:DCbipaDatumCommand
p -1904 4576 100 1024 -1 name:$(dc)biDatum
use ecad2 -1408 4583 100 0 ecad2#837
xform 0 -1248 4896
p -1312 5056 100 0 0 DESC:Motor Init CAD
p -1312 4896 100 0 0 FTVA:STRING
p -1312 4608 100 0 0 SNAM:DCbipaPowerCommand
p -1296 4576 100 1024 -1 name:$(dc)biPower
use ecad2 -2624 4583 100 0 ecad2#1031
xform 0 -2464 4896
p -2528 4896 100 0 0 FTVA:LONG
p -2528 4608 100 0 0 SNAM:DCbipaInitCommand
p -2512 4576 100 1024 -1 name:$(dc)biInit
use ecad2 -800 4583 100 0 ecad2#1076
xform 0 -640 4896
p -704 4896 100 0 0 FTVA:LONG
p -704 4608 100 0 0 SNAM:DCbipaParkCommand
p -688 4576 100 1024 -1 name:$(dc)biPark
use ecad2 -1024 3623 -100 0 ecad2#1077
xform 0 -864 3936
p -928 3936 100 0 0 FTVA:LONG
p -928 3904 100 0 0 FTVB:DOUBLE
p -928 3648 100 0 0 SNAM:DCbipaSetCommand
p -912 3616 100 1024 -1 name:$(dc)biSet
use ecad2 -1632 3623 100 0 ecad2#1078
xform 0 -1472 3936
p -1536 3936 100 0 0 FTVA:LONG
p -1536 3648 100 0 0 SNAM:DCbipaLatchCommand
p -1520 3616 100 1024 -1 name:$(dc)biLatch
use ecad2 -1408 2343 100 0 ecad2#1079
xform 0 -1248 2656
p -1312 2656 100 0 0 FTVA:LONG
p -1312 2368 100 0 0 SNAM:
p -1296 2336 100 1024 -1 name:$(dc)biDetTemp
use ecad2 -800 2343 -100 0 ecad2#1080
xform 0 -640 2656
p -704 2368 100 0 0 SNAM:DCbipaVWellCommand
p -688 2336 100 1024 -1 name:$(dc)bivWell
use ecad2 -128 2343 100 0 ecad2#1081
xform 0 32 2656
p -32 2560 100 0 0 PHAS:0
p -32 2368 100 0 0 SNAM:DCbipaVBiasCommand
p -16 2336 100 1024 -1 name:$(dc)bivBias
use ecad2 -2240 3623 100 0 ecad2#1095
xform 0 -2080 3936
p -2144 3936 100 0 0 FTVA:LONG
p -2144 3648 100 0 0 SNAM:DCbipaReadAllCommand
p -2128 3616 100 1024 -1 name:$(dc)biReadAll
use ecad2 -2016 2343 100 0 ecad2#1156
xform 0 -1856 2656
p -1920 2368 100 0 0 SNAM:DCbiPasswordCommand
p -1904 2336 100 1024 -1 name:$(dc)biPassword
use egenSubFLAM 1472 4455 100 0 egenSubFLAM#1118
xform 0 1616 4880
p 1249 4229 100 0 0 FTA:LONG
p 1249 4229 100 0 0 FTB:LONG
p 1249 4197 100 0 0 FTC:STRING
p 1249 4165 100 0 0 FTD:LONG
p 1249 4133 100 0 0 FTE:LONG
p 1249 4069 100 0 0 FTG:LONG
p 1249 4037 100 0 0 FTH:LONG
p 1249 4005 100 0 0 FTI:LONG
p 1249 3973 100 0 0 FTJ:LONG
p 1249 3973 100 0 0 FTK:LONG
p 1249 3973 100 0 0 FTL:LONG
p 1249 3973 100 0 0 FTM:LONG
p 1249 3973 100 0 0 FTN:STRING
p 1249 3973 100 0 0 FTO:STRING
p 1249 4229 100 0 0 FTVA:LONG
p 1249 4229 100 0 0 FTVB:LONG
p 1249 4197 100 0 0 FTVC:STRING
p 1249 4133 100 0 0 FTVE:LONG
p 1249 4069 100 0 0 FTVG:LONG
p 1249 4037 100 0 0 FTVH:STRING
p 1249 4005 100 0 0 FTVI:STRING
p 1249 3973 100 0 0 FTVJ:STRING
p 1249 3973 100 0 0 FTVK:LONG
p 1249 3973 100 0 0 FTVL:LONG
p 1249 3973 100 0 0 FTVM:LONG
p 1184 4862 100 0 0 INAM:ufDCBiasGinit
p 1249 3589 100 0 0 NOJ:24
p 1249 3781 100 0 0 NOVD:24
p 1249 3749 100 0 0 NOVE:24
p 1249 3717 100 0 0 NOVF:24
p 1249 3685 100 0 0 NOVG:24
p 1249 3589 100 0 0 NOVK:24
p 1249 3589 100 0 0 NOVL:24
p 1249 3589 100 0 0 NOVM:24
p 1184 5006 100 0 0 PREC:4
p 1184 5150 100 0 0 SCAN:Passive
p 1184 4830 100 0 0 SNAM:ufDCBiasGproc
p 1584 4448 100 1024 -1 name:$(dc)DCBiasG
use be200tr -3808 1480 -100 0 frame
xform 0 -368 4144
use eapply -3200 3687 -100 0 $(top)$(dev)motorApply
xform 0 -3008 4048
p -3104 4032 100 0 0 DESC:DC Bias Low Level Apply
p -3088 3680 100 1024 -1 name:$(dc)BiApply
use eapply -2912 2375 100 0 eapply#1082
xform 0 -2720 2736
p -2800 2368 100 1024 -1 name:$(dc)BiApply2
use flamLinkEnable 640 3687 100 0 flamLinkEnable#946
xform 0 800 3808
p 640 3680 100 0 1 setSystem:system $(dc)bi
use eaos 640 5031 100 0 eaos#917
xform 0 768 5120
p 573 4996 100 0 1 OMSL:closed_loop
p 573 5024 100 768 1 name:$(dc)biVoltW
use ecars 832 3143 -100 0 $(top)$(dev)motorC
xform 0 992 3312
p 928 3360 100 0 0 DESC:Motor CAR Record
p 944 3136 100 1024 -1 name:$(dc)DCBiasC
[comments]
RCS: "$Name:  $ $Id: flamDcBias.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $"
