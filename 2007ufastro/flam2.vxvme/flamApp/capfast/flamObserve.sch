[schematic2]
uniq 98
[tools]
[detail]
w 88 907 100 0 n#96 hwin.hwin#95.in 64 896 160 896 ecad2.ecad2#48.INPB
w 1736 747 100 0 n#94 flamSubSysCommand.flamSubSysCommand#57.CAR_IMSS 1632 736 1888 736 ecars.ecars#53.IMSS
w 1736 811 100 0 n#93 flamSubSysCommand.flamSubSysCommand#57.CAR_IVAL 1632 800 1888 800 ecars.ecars#53.IVAL
w 176 1323 -100 0 c#89 ecad2.ecad2#48.DIR 160 1184 96 1184 96 1312 304 1312 304 1632 192 1632 inhier.DIR.P
w 144 1355 -100 0 c#90 ecad2.ecad2#48.ICID 160 1152 64 1152 64 1344 272 1344 272 1504 192 1504 inhier.ICID.P
w 416 1323 -100 0 c#91 ecad2.ecad2#48.VAL 480 1184 544 1184 544 1312 336 1312 336 1632 480 1632 outhier.VAL.p
w 448 1355 -100 0 c#92 ecad2.ecad2#48.MESS 480 1152 576 1152 576 1344 368 1344 368 1504 480 1504 outhier.MESS.p
w 1096 731 100 0 n#76 efanouts.efanouts#13.LNK3 896 720 1344 720 1344 704 flamSubSysCommand.flamSubSysCommand#57.START
w 1048 1371 100 0 n#73 ecad2.ecad2#48.VALA 480 992 832 992 832 1360 1312 1360 estringouts.estringouts#68.DOL
w 984 763 100 0 n#74 efanouts.efanouts#13.LNK2 896 752 1120 752 1120 1104 1312 1104 estringouts.estringouts#69.SLNK
w 1176 1339 100 0 n#71 efanouts.efanouts#13.LNK1 896 784 1088 784 1088 1328 1312 1328 estringouts.estringouts#68.SLNK
w 1560 1099 100 0 n#64 estringouts.estringouts#69.OUT 1568 1088 1600 1088 hwout.hwout#65.outp
w 1560 1323 100 0 n#59 estringouts.estringouts#68.OUT 1568 1312 1600 1312 hwout.hwout#60.outp
w 544 715 100 0 n#49 ecad2.ecad2#48.STLK 480 704 656 704 efanouts.efanouts#13.SLNK
s -96 992 100 0 Observation ID
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos observe Command
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/12/05
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamObserve.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2000/12/05
[cell use]
use hwin -128 855 100 0 hwin#95
xform 0 -32 896
p -288 896 100 0 -1 val(in):$(top)state
use outhier 496 1504 100 0 MESS
xform 0 464 1504
use outhier 496 1632 100 0 VAL
xform 0 464 1632
use inhier 112 1504 100 0 ICID
xform 0 192 1504
use inhier 128 1632 100 0 DIR
xform 0 192 1632
use estringouts 1312 1255 100 0 estringouts#68
xform 0 1440 1328
p 1392 1216 100 0 1 OMSL:closed_loop
p 1504 1248 100 1024 1 name:$(top)dcObserveId
use estringouts 1312 1031 100 0 estringouts#69
xform 0 1440 1104
p 1376 992 100 0 0 OMSL:supervisory
p 1376 992 100 0 1 VAL:START
p 1504 1024 100 1024 1 name:$(top)dcStartCmd
use hwout 1600 1271 100 0 hwout#60
xform 0 1696 1312
p 1824 1312 100 0 -1 val(outp):$(top)dc:acqControl.B
use hwout 1600 1047 100 0 hwout#65
xform 0 1696 1088
p 1824 1088 100 0 -1 val(outp):$(top)dc:acqControl.A
use flamSubSysCommand 1344 519 100 0 flamSubSysCommand#57
xform 0 1488 704
p 1344 480 100 0 1 setCommand:cmd observe
p 1344 512 100 0 1 setSystem:sys dc
use ecars 1888 519 100 0 ecars#53
xform 0 2048 688
p 2048 512 100 1024 1 name:$(top)observeCmdC
use ecad2 160 615 100 0 ecad2#48
xform 0 320 928
p 224 576 100 0 1 INAM:flamIsNullInit
p 224 528 100 0 1 SNAM:flamIsObserveProcess
p 320 608 100 1024 1 name:$(top)observe
p 496 704 75 1024 -1 pproc(STLK):PP
use efanouts 656 567 100 0 efanouts#13
xform 0 776 720
p 800 560 100 1024 1 name:$(top)observeFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamObserve.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
