[schematic2]
uniq 80
[tools]
[detail]
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Removed EC records
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2001/01/25
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos CC Configure
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 flamSadCcConfig.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2000/11/12
[cell use]
use esirs 1248 423 100 0 esirs#79
xform 0 1456 576
p 1312 384 100 0 1 SCAN:Passive
p 1408 416 100 1024 -1 name:$(top)WindlwStatus
use esirs 1984 423 100 0 esirs#78
xform 0 2192 576
p 2048 384 100 0 1 SCAN:Passive
p 2160 416 100 1024 -1 name:$(top)LyotStatus
use esirs 544 423 100 0 esirs#55
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 688 416 100 1024 -1 name:$(top)GrismPos
use esirs 2000 1255 100 0 esirs#53
xform 0 2208 1408
p 2064 1216 100 0 1 SCAN:Passive
p 2160 1248 100 1024 -1 name:$(top)Filter1Status
use esirs 2000 1671 100 0 esirs#52
xform 0 2208 1824
p 2064 1632 100 0 1 SCAN:Passive
p 2160 1664 100 1024 -1 name:$(top)MOSStatus
use esirs 1264 839 100 0 esirs#51
xform 0 1472 992
p 1328 800 100 0 1 SCAN:Passive
p 1424 832 100 1024 -1 name:$(top)GrismStatus
use esirs 1264 1255 100 0 esirs#50
xform 0 1472 1408
p 1328 1216 100 0 1 SCAN:Passive
p 1424 1248 100 1024 -1 name:$(top)FocusStatus
use esirs 1264 1671 100 0 esirs#49
xform 0 1472 1824
p 1328 1632 100 0 1 SCAN:Passive
p 1424 1664 100 1024 -1 name:$(top)DeckerStatus
use esirs 2000 839 100 0 esirs#48
xform 0 2208 992
p 2064 800 100 0 1 SCAN:Passive
p 2176 832 100 1024 -1 name:$(top)Filter2Status
use esirs -192 423 100 0 esirs#34
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)FocusPos
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)LyotPos
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)Filter1Pos
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)MOSPos
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)Filter2Pos
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)DeckerPos
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)WindowPos
use changeBar 1984 1991 100 0 changeBar#47
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadCcConfig.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
