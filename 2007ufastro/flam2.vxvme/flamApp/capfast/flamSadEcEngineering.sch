[schematic2]
uniq 58
[tools]
[detail]
s 2624 2032 100 1792 2001/01/25
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2528 -240 100 1792 flamSadEcEngineering.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos EC Engineering
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs -192 1671 100 0 esirs#27
xform 0 16 1824
p -256 1376 100 0 0 FTVL:FLOAT
p -128 1632 100 0 1 SCAN:Passive
p 0 1664 100 1024 -1 name:$(top)tSMosColdhead
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -256 960 100 0 0 FTVL:FLOAT
p -128 1216 100 0 1 SCAN:Passive
p -16 1248 100 1024 -1 name:$(top)tSCamColdhead
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -256 544 100 0 0 FTVL:FLOAT
p -128 800 100 0 1 SCAN:Passive
p -16 832 100 1024 -1 name:$(top)tSDetFanout
use esirs -192 423 100 0 esirs#30
xform 0 16 576
p -256 128 100 0 0 FTVL:FLOAT
p -128 384 100 0 1 SCAN:Passive
p -16 416 100 1024 -1 name:$(top)camCryosPressure
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 480 1376 100 0 0 FTVL:FLOAT
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)tSMosWSurface
use esirs 544 1239 100 0 esirs#33
xform 0 752 1392
p 480 944 100 0 0 FTVL:FLOAT
p 608 1200 100 0 1 SCAN:Passive
p 752 1232 100 1024 -1 name:$(top)tSCamWSurface1
use esirs 544 823 100 0 esirs#34
xform 0 752 976
p 480 528 100 0 0 FTVL:STRING
p 608 784 100 0 1 SCAN:Passive
p 704 816 100 1024 -1 name:$(top)INTERLCK
use esirs 1248 839 100 0 esirs#38
xform 0 1456 992
p 1184 544 100 0 0 FTVL:FLOAT
p 1312 800 100 0 1 SCAN:Passive
p 1392 832 100 1024 -1 name:$(top)mosCryosPressure
use esirs 1248 1255 100 0 esirs#39
xform 0 1456 1408
p 1184 960 100 0 0 FTVL:FLOAT
p 1312 1216 100 0 1 SCAN:Passive
p 1392 1248 100 1024 -1 name:$(top)tSCamWSurface2
use esirs 1248 1671 100 0 esirs#40
xform 0 1456 1824
p 1184 1376 100 0 0 FTVL:FLOAT
p 1312 1632 100 0 1 SCAN:Passive
p 1408 1664 100 1024 -1 name:$(top)tSMosSpare
use esirs 528 423 100 0 esirs#46
xform 0 736 576
p 464 128 100 0 0 FTVL:STRING
p 592 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)LS218K1
use esirs 1248 423 100 0 esirs#47
xform 0 1456 576
p 1184 128 100 0 0 FTVL:STRING
p 1312 384 100 0 1 SCAN:Passive
p 1424 416 100 1024 -1 name:$(top)LS218K5
use esirs -192 7 100 0 esirs#48
xform 0 16 160
p -256 -288 100 0 0 FTVL:STRING
p -128 -32 100 0 1 SCAN:Passive
p -16 0 100 1024 -1 name:$(top)LS218K2
use esirs 544 7 100 0 esirs#49
xform 0 752 160
p 480 -288 100 0 0 FTVL:STRING
p 608 -32 100 0 1 SCAN:Passive
p 720 0 100 1024 -1 name:$(top)LS218K3
use esirs 1248 7 100 0 esirs#50
xform 0 1456 160
p 1184 -288 100 0 0 FTVL:STRING
p 1312 -32 100 0 1 SCAN:Passive
p 1424 0 100 1024 -1 name:$(top)LS218K6
use esirs 1824 423 100 0 esirs#51
xform 0 2032 576
p 1936 416 100 1024 1 name:$(top)LS218K7
use esirs 1824 7 100 0 esirs#52
xform 0 2032 160
p 1760 -288 100 0 0 FTVL:STRING
p 1888 -32 100 0 1 SCAN:Passive
p 2000 0 100 1024 -1 name:$(top)LS218K8
use esirs 1824 839 100 0 esirs#53
xform 0 2032 992
p 1760 544 100 0 0 FTVL:STRING
p 1888 800 100 0 1 SCAN:Passive
p 2000 832 100 1024 -1 name:$(top)LS218K4
use esirs 1824 1255 100 0 esirs#54
xform 0 2032 1408
p 1760 960 100 0 0 FTVL:FLOAT
p 1888 1216 100 0 1 SCAN:Passive
p 2000 1248 100 1024 -1 name:$(top)LS332Kelvin2
use esirs 1824 1671 100 0 esirs#55
xform 0 2032 1824
p 1760 1376 100 0 0 FTVL:FLOAT
p 1888 1632 100 0 1 SCAN:Passive
p 2000 1664 100 1024 -1 name:$(top)LS332Kelvin1
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadEcEngineering.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
