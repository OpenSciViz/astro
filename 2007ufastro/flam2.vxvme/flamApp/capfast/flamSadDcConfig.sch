[schematic2]
uniq 124
[tools]
[detail]
s 4192 -1856 100 0 FLAMINGOS
s 4048 -1904 200 1792 FLAMINGOS
s 4384 -1920 100 256 Flamingos DC Configuration
s 4272 -1968 100 1792 Rev: A
s 4368 -2000 100 1792 2000/11/12
s 4048 -2000 100 1792 Author: NWR
s 4464 -1968 100 1792 flamSadDcConfig.sch
[cell use]
use esirs 2048 -313 100 0 esirs#123
xform 0 2256 -160
p 2128 -352 100 0 1 SCAN:Passive
p 2208 -320 100 1024 -1 name:$(top)CDSREADS
use bd200 -544 -2136 -100 0 frame
xform 0 2096 -432
use esirs 2048 839 100 0 esirs#122
xform 0 2256 992
p 1984 544 100 0 0 FTVL:LONG
p 2112 800 100 0 1 SCAN:Passive
p 2208 832 100 1024 -1 name:$(top)DACPRE05
use esirs 2784 -313 100 0 esirs#117
xform 0 2992 -160
p 2720 -608 100 0 0 FTVL:LONG
p 2848 -352 100 0 1 SCAN:Passive
p 2944 -320 100 1024 -1 name:$(top)POSTSETS
use esirs 3408 -729 100 0 esirs#116
xform 0 3616 -576
p 3344 -1024 100 0 0 FTVL:LONG
p 3472 -768 100 0 1 SCAN:Passive
p 3568 -736 100 1024 -1 name:$(top)BIASGATE
use esirs 3408 -1145 100 0 esirs#115
xform 0 3616 -992
p 3344 -1440 100 0 0 FTVL:LONG
p 3472 -1184 100 0 1 SCAN:Passive
p 3568 -1152 100 1024 -1 name:$(top)BIASPWR
use esirs 2768 839 100 0 esirs#114
xform 0 2976 992
p 2704 544 100 0 0 FTVL:LONG
p 2832 800 100 0 1 SCAN:Passive
p 2928 832 100 1024 -1 name:$(top)BIASVDD
use esirs 2768 455 100 0 esirs#113
xform 0 2976 608
p 2704 160 100 0 0 FTVL:LONG
p 2832 416 100 0 1 SCAN:Passive
p 2928 448 100 1024 -1 name:$(top)BIASVRES
use esirs 2768 71 100 0 esirs#112
xform 0 2976 224
p 2704 -224 100 0 0 FTVL:LONG
p 2832 32 100 0 1 SCAN:Passive
p 2928 64 100 1024 -1 name:$(top)PRESETS
use esirs -96 -1497 100 0 esirs#111
xform 0 112 -1344
p -160 -1792 100 0 0 FTVL:LONG
p -32 -1536 100 0 1 SCAN:Passive
p 64 -1504 100 1024 -1 name:$(top)DACPRE25
use esirs 640 -1497 100 0 esirs#110
xform 0 848 -1344
p 576 -1792 100 0 0 FTVL:LONG
p 704 -1536 100 0 1 SCAN:Passive
p 800 -1504 100 1024 -1 name:$(top)DACPRE26
use esirs 1344 -1497 100 0 esirs#109
xform 0 1552 -1344
p 1280 -1792 100 0 0 FTVL:LONG
p 1408 -1536 100 0 1 SCAN:Passive
p 1504 -1504 100 1024 -1 name:$(top)DACPRE27
use esirs 2080 -1497 100 0 esirs#108
xform 0 2288 -1344
p 2016 -1792 100 0 0 FTVL:LONG
p 2144 -1536 100 0 1 SCAN:Passive
p 2240 -1504 100 1024 -1 name:$(top)DACPRE28
use esirs 2080 -1881 100 0 esirs#107
xform 0 2288 -1728
p 2016 -2176 100 0 0 FTVL:LONG
p 2144 -1920 100 0 1 SCAN:Passive
p 2240 -1888 100 1024 -1 name:$(top)DACPRE29
use esirs 1344 -1881 100 0 esirs#106
xform 0 1552 -1728
p 1280 -2176 100 0 0 FTVL:LONG
p 1408 -1920 100 0 1 SCAN:Passive
p 1504 -1888 100 1024 -1 name:$(top)DACPRE30
use esirs 640 -1881 100 0 esirs#105
xform 0 848 -1728
p 576 -2176 100 0 0 FTVL:LONG
p 704 -1920 100 0 1 SCAN:Passive
p 800 -1888 100 1024 -1 name:$(top)DACPRE31
use esirs -96 -1881 100 0 esirs#104
xform 0 112 -1728
p -160 -2176 100 0 0 FTVL:LONG
p -32 -1920 100 0 1 SCAN:Passive
p 64 -1888 100 1024 -1 name:$(top)DACPRE00
use esirs -96 -1113 100 0 esirs#103
xform 0 112 -960
p -160 -1408 100 0 0 FTVL:LONG
p -32 -1152 100 0 1 SCAN:Passive
p 64 -1120 100 1024 -1 name:$(top)DACPRE24
use esirs 640 -1113 100 0 esirs#102
xform 0 848 -960
p 576 -1408 100 0 0 FTVL:LONG
p 704 -1152 100 0 1 SCAN:Passive
p 800 -1120 100 1024 -1 name:$(top)DACPRE23
use esirs 1344 -1113 100 0 esirs#101
xform 0 1552 -960
p 1280 -1408 100 0 0 FTVL:LONG
p 1408 -1152 100 0 1 SCAN:Passive
p 1504 -1120 100 1024 -1 name:$(top)DACPRE22
use esirs 2080 -1113 100 0 esirs#100
xform 0 2288 -960
p 2016 -1408 100 0 0 FTVL:LONG
p 2144 -1152 100 0 1 SCAN:Passive
p 2240 -1120 100 1024 -1 name:$(top)DACPRE21
use esirs 2080 -729 100 0 esirs#99
xform 0 2288 -576
p 2016 -1024 100 0 0 FTVL:LONG
p 2144 -768 100 0 1 SCAN:Passive
p 2240 -736 100 1024 -1 name:$(top)DACPRE20
use esirs 1344 -729 100 0 esirs#98
xform 0 1552 -576
p 1280 -1024 100 0 0 FTVL:LONG
p 1408 -768 100 0 1 SCAN:Passive
p 1504 -736 100 1024 -1 name:$(top)DACPRE19
use esirs 640 -729 100 0 esirs#97
xform 0 848 -576
p 576 -1024 100 0 0 FTVL:LONG
p 704 -768 100 0 1 SCAN:Passive
p 800 -736 100 1024 -1 name:$(top)DACPRE18
use esirs -96 -729 100 0 esirs#96
xform 0 112 -576
p -160 -1024 100 0 0 FTVL:LONG
p -32 -768 100 0 1 SCAN:Passive
p 64 -736 100 1024 -1 name:$(top)DACPRE17
use esirs -96 -329 100 0 esirs#95
xform 0 112 -176
p -160 -624 100 0 0 FTVL:LONG
p -32 -368 100 0 1 SCAN:Passive
p 64 -336 100 1024 -1 name:$(top)DACPRE16
use esirs 640 -329 100 0 esirs#94
xform 0 848 -176
p 576 -624 100 0 0 FTVL:LONG
p 704 -368 100 0 1 SCAN:Passive
p 800 -336 100 1024 -1 name:$(top)DACPRE15
use esirs 1344 -329 100 0 esirs#93
xform 0 1552 -176
p 1280 -624 100 0 0 FTVL:LONG
p 1408 -368 100 0 1 SCAN:Passive
p 1504 -336 100 1024 -1 name:$(top)DACPRE14
use esirs 2048 71 100 0 esirs#92
xform 0 2256 224
p 1984 -224 100 0 0 FTVL:LONG
p 2112 32 100 0 1 SCAN:Passive
p 2208 64 100 1024 -1 name:$(top)DACPRE13
use esirs 2048 455 100 0 esirs#91
xform 0 2256 608
p 1984 160 100 0 0 FTVL:LONG
p 2112 416 100 0 1 SCAN:Passive
p 2208 448 100 1024 -1 name:$(top)DACPRE12
use esirs 1344 71 100 0 esirs#90
xform 0 1552 224
p 1280 -224 100 0 0 FTVL:LONG
p 1408 32 100 0 1 SCAN:Passive
p 1504 64 100 1024 -1 name:$(top)DACPRE11
use esirs 640 71 100 0 esirs#89
xform 0 848 224
p 576 -224 100 0 0 FTVL:LONG
p 704 32 100 0 1 SCAN:Passive
p 800 64 100 1024 -1 name:$(top)DACPRE10
use esirs -96 71 100 0 esirs#88
xform 0 112 224
p -160 -224 100 0 0 FTVL:LONG
p -32 32 100 0 1 SCAN:Passive
p 64 64 100 1024 -1 name:$(top)DACPRE09
use esirs -96 455 100 0 esirs#87
xform 0 112 608
p -160 160 100 0 0 FTVL:LONG
p -32 416 100 0 1 SCAN:Passive
p 64 448 100 1024 -1 name:$(top)DACPRE08
use esirs 640 455 100 0 esirs#86
xform 0 848 608
p 576 160 100 0 0 FTVL:LONG
p 704 416 100 0 1 SCAN:Passive
p 800 448 100 1024 -1 name:$(top)DACPRE07
use esirs 1344 455 100 0 esirs#85
xform 0 1552 608
p 1280 160 100 0 0 FTVL:LONG
p 1408 416 100 0 1 SCAN:Passive
p 1504 448 100 1024 -1 name:$(top)DACPRE06
use esirs 1344 839 100 0 esirs#82
xform 0 1552 992
p 1280 544 100 0 0 FTVL:LONG
p 1408 800 100 0 1 SCAN:Passive
p 1504 832 100 1024 -1 name:$(top)DACPRE04
use esirs 640 839 100 0 esirs#81
xform 0 848 992
p 576 544 100 0 0 FTVL:LONG
p 704 800 100 0 1 SCAN:Passive
p 800 832 100 1024 -1 name:$(top)DACPRE03
use esirs -96 839 100 0 esirs#80
xform 0 112 992
p -160 544 100 0 0 FTVL:LONG
p -32 800 100 0 1 SCAN:Passive
p 64 832 100 1024 -1 name:$(top)DACPRE02
use esirs 3904 -361 100 0 esirs#42
xform 0 4112 -208
p 3840 -656 100 0 0 FTVL:LONG
p 3968 -400 100 0 1 SCAN:Passive
p 4064 -368 100 1024 -1 name:$(top)DACPRE01
use esirs 3904 55 100 0 esirs#41
xform 0 4112 208
p 3840 -240 100 0 0 FTVL:LONG
p 3968 16 100 0 1 SCAN:Passive
p 4064 48 100 1024 -1 name:$(top)edtTimeout
use esirs 3408 55 100 0 esirs#40
xform 0 3616 208
p 3344 -240 100 0 0 FTVL:LONG
p 3472 16 100 0 1 SCAN:Passive
p 3568 48 100 1024 -1 name:$(top)pixelBaseClock
use esirs 3408 -361 100 0 esirs#39
xform 0 3616 -208
p 3472 -400 100 0 1 SCAN:Passive
p 3568 -368 100 1024 -1 name:$(top)timer
use esirs 3920 423 100 0 esirs#32
xform 0 4128 576
p 3856 128 100 0 0 FTVL:LONG
p 3984 384 100 0 1 SCAN:Passive
p 4080 416 100 1024 -1 name:$(top)CYCLETYP
use esirs 3920 839 100 0 esirs#31
xform 0 4128 992
p 3856 544 100 0 0 FTVL:LONG
p 3984 800 100 0 1 SCAN:Passive
p 4080 832 100 1024 -1 name:$(top)postResets
use esirs 3408 423 100 0 esirs#27
xform 0 3616 576
p 3344 128 100 0 0 FTVL:LONG
p 3472 384 100 0 1 SCAN:Passive
p 3568 416 100 1024 -1 name:$(top)lutTimeout
use esirs 3408 839 100 0 esirs#7
xform 0 3616 992
p 3344 544 100 0 0 FTVL:LONG
p 3472 800 100 0 1 SCAN:Passive
p 3568 832 100 1024 -1 name:$(top)preResets
use esirs 2880 -1497 100 0 esirs#118
xform 0 3088 -1344
p 2816 -1792 100 0 0 FTVL:LONG
p 2944 -1536 100 0 1 SCAN:Passive
p 3040 -1504 100 1024 -1 name:$(top)EDTACTN
use esirs 2880 -1113 100 0 esirs#119
xform 0 3088 -960
p 2816 -1408 100 0 0 FTVL:LONG
p 2944 -1152 100 0 1 SCAN:Passive
p 3040 -1120 100 1024 -1 name:$(top)EDTFRAME
use esirs 2880 -729 100 0 esirs#120
xform 0 3088 -576
p 2816 -1024 100 0 0 FTVL:LONG
p 2944 -768 100 0 1 SCAN:Passive
p 3040 -736 100 1024 -1 name:$(top)EDTTOTAL
use esirs 2880 -1865 100 0 esirs#121
xform 0 3088 -1712
p 2816 -2160 100 0 0 FTVL:LONG
p 2944 -1904 100 0 1 SCAN:Passive
p 3040 -1872 100 1024 -1 name:$(top)FOCUSPOS
use rb200abc 3936 -2041 100 0 rb200abc#0
xform 0 4288 -1920
[comments]
RCS: $Name:  $ $Id: flamSadDcConfig.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
