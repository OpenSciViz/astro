[schematic2]
uniq 80
[tools]
[detail]
s 2624 2064 100 1792 2000/11/11
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamSadObsConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2002/05/02
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 SAD Observation Configuration
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s 2624 2032 100 1792 2002/05/02
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Added local file and dir names
s 2016 2032 100 1792 B
[cell use]
use esirs 672 1255 100 0 esirs#7
xform 0 880 1408
p 736 1216 100 0 1 SCAN:Passive
p 832 1248 100 1024 -1 name:$(top)externalTemp
use esirs -64 1255 100 0 esirs#27
xform 0 144 1408
p 0 1216 100 0 1 SCAN:Passive
p 96 1248 100 1024 -1 name:$(top)beamMode
use esirs 672 1671 100 0 esirs#31
xform 0 880 1824
p 736 1632 100 0 1 SCAN:Passive
p 832 1664 100 1024 -1 name:$(top)cameraMode
use esirs -64 1671 100 0 esirs#32
xform 0 144 1824
p 0 1632 100 0 1 SCAN:Passive
p 96 1664 100 1024 -1 name:$(top)obsType
use esirs 1376 1255 100 0 esirs#39
xform 0 1584 1408
p 1312 960 100 0 0 FTVL:DOUBLE
p 1440 1216 100 0 1 SCAN:Passive
p 1536 1248 100 1024 -1 name:$(top)EXPTIME
use esirs 1376 1671 100 0 esirs#40
xform 0 1584 1824
p 1440 1632 100 0 1 SCAN:Passive
p 1536 1664 100 1024 -1 name:$(top)dataMode
use esirs 2080 1655 100 0 esirs#41
xform 0 2288 1808
p 2144 1616 100 0 1 SCAN:Passive
p 2240 1648 100 1024 -1 name:$(top)obsID
use esirs 2080 1239 100 0 esirs#42
xform 0 2288 1392
p 2016 944 100 0 0 FTVL:LONG
p 2144 1200 100 0 1 SCAN:Passive
p 2240 1232 100 1024 -1 name:$(top)numReads
use changeBar 1984 2023 100 0 changeBar#51
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#52
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadObsConfig.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
