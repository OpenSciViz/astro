[schematic2]
uniq 110
[tools]
[detail]
w 1426 1611 100 0 n#109 hwin.hwin#108.in 1312 1600 1600 1600 egenSub.egenSub#94.INPB
w 1426 1675 100 0 n#107 hwin.hwin#106.in 1312 1664 1600 1664 egenSub.egenSub#94.INPA
w 50 875 100 0 n#103 hwin.hwin#102.in 32 864 128 864 ecad2.ecad2#48.INPB
w 1944 555 100 0 n#78 junction 2016 512 2016 544 1920 544 eseqs.eseqs#93.LNK1
w 1992 523 100 0 n#78 eseqs.eseqs#93.LNK2 1920 512 2112 512 ecars.ecars#53.IVAL
w 1522 523 100 0 n#100 eseqs.eseqs#93.DOL2 1600 512 1504 512 1504 480 1472 480 hwin.hwin#98.in
w 1528 555 100 0 n#99 hwin.hwin#97.in 1472 576 1504 576 1504 544 1600 544 eseqs.eseqs#93.DOL1
w 1304 235 100 0 n#96 efanouts.efanouts#13.LNK2 864 720 1056 720 1056 224 1600 224 eseqs.eseqs#93.SLNK
w 1298 1003 100 0 n#101 efanouts.efanouts#13.LNK1 864 752 1056 752 1056 992 1600 992 egenSub.egenSub#94.SLNK
w 144 1291 -100 0 c#89 ecad2.ecad2#48.DIR 128 1152 64 1152 64 1280 272 1280 272 1600 160 1600 inhier.DIR.P
w 112 1323 -100 0 c#90 ecad2.ecad2#48.ICID 128 1120 32 1120 32 1312 240 1312 240 1472 160 1472 inhier.ICID.P
w 384 1291 -100 0 c#91 ecad2.ecad2#48.VAL 448 1152 512 1152 512 1280 304 1280 304 1600 448 1600 outhier.VAL.p
w 416 1323 -100 0 c#92 ecad2.ecad2#48.MESS 448 1120 544 1120 544 1312 336 1312 336 1472 448 1472 outhier.MESS.p
w 512 683 100 0 n#49 ecad2.ecad2#48.STLK 448 672 624 672 efanouts.efanouts#13.SLNK
s 2624 2032 100 1792 2003/02/09
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Added host name
s 2016 2032 100 1792 B
s 1328 1696 100 0 Agent Host Name
s 2624 2064 100 1792 2000/12/05
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamReboot.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2003/02/10
s 2320 -240 100 1792 REV: B
s 2432 -192 100 256 FLAMINGOS reboot Command
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use hwin -160 823 100 0 hwin#102
xform 0 -64 864
p -320 864 100 0 -1 val(in):$(top)state
use hwin 1280 535 100 0 hwin#97
xform 0 1376 576
p 1120 576 100 0 -1 val(in):$(CAR_BUSY)
use hwin 1280 439 100 0 hwin#98
xform 0 1376 480
p 1104 480 100 0 -1 val(in):$(CAR_ERROR)
use hwin 1120 1623 100 0 hwin#106
xform 0 1216 1664
p 752 1664 100 0 -1 val(in):$(sad)agenthostIP.VAL NPP NMS
use hwin 1120 1559 100 0 hwin#108
xform 0 1216 1600
p 1134 1528 100 0 0 typ(in):val
p 1123 1592 100 0 -1 val(in):$(sad)Simulation NPP NMS
use changeBar 1984 1991 100 0 changeBar#105
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#104
xform 0 2336 2064
use egenSub 1600 903 100 0 egenSub#94
xform 0 1744 1328
p 1377 677 100 0 0 FTA:STRING
p 1377 677 100 0 0 FTB:STRING
p 1664 864 100 0 1 INAM:flamIsRebootGInit
p 1664 832 100 0 1 SNAM:flamIsRebootGProcess
p 1776 896 100 1024 1 name:$(top)rebootG
use eseqs 1600 135 100 0 eseqs#93
xform 0 1760 384
p 1664 96 100 0 1 DLY1:0.0e+00
p 1664 64 100 0 1 DLY2:5.0e+00
p 1792 128 100 1024 1 name:$(top)rebootBusy
p 1936 544 75 1024 -1 pproc(LNK1):PP
p 1936 512 75 1024 -1 pproc(LNK2):PP
use outhier 464 1600 100 0 VAL
xform 0 432 1600
use outhier 464 1472 100 0 MESS
xform 0 432 1472
use inhier 96 1600 100 0 DIR
xform 0 160 1600
use inhier 80 1472 100 0 ICID
xform 0 160 1472
use ecars 2112 231 100 0 ecars#53
xform 0 2272 400
p 2272 224 100 1024 1 name:$(top)rebootC
use ecad2 128 583 100 0 ecad2#48
xform 0 288 896
p 192 544 100 0 1 INAM:flamIsNullInit
p 192 496 100 0 1 SNAM:flamIsRebootProcess
p 288 576 100 1024 1 name:$(top)reboot
p 464 672 75 1024 -1 pproc(STLK):PP
use efanouts 624 535 100 0 efanouts#13
xform 0 744 688
p 768 528 100 1024 1 name:$(top)rebootFanout
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamReboot.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $
