
/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * flamBogusLookup.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument dummy lookup
 * table module
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2001/01/14  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


#define NAME_SIZE  16


/*
 *
 *  Public function prototypes
 *
 */

long flamInitBogusLookup (void);
long flamReloadBogusLookup (void);
long flamBogusFilterLookup (char *, char *, char *, char *);
long flamBogusNameLookup (char *, char *, float *);
