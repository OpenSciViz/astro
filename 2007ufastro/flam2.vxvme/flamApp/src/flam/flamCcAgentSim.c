

/*
 *
 *  Header stuffs for ccAgentSim.c
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <semLib.h>
#include <taskLib.h>
#include <sysLib.h>
#include <tickLib.h>
#include <logLib.h>
#include <msgQLib.h>

#include <recSup.h>
#include <recGbl.h>
#include <dbAddr.h>
#include <db_access.h>
#include <car.h> 

#include <flamCcAgentSim.h>




/*
 *   Local defines
 */

#define CC_AGENT_PRIORITY          80	/* timer task
					   priority */
#define CC_AGENT_STACK             0x1000	/* task
						   stack
						   size */
#define CC_AGENT_SCAN_RATE         10	/* task repeat per
					   sec */
#define CC_AGENT_OPTIONS           0	/* no extra task
					   options */

#define CC_AGENT_IDLE              0	/* waiting for a
					   command */
#define CC_AGENT_STARTING          1	/* starting a new
					   command */
#define CC_AGENT_EXECUTING         2	/* executing a
					   command */


/*
 *
 *  Data structures
 *
 */

static char buffer[MAX_STRING_SIZE];	/* scratchpad
					   buffer */
static long ccAgentState;	/* command progress flag */
static long ccActionState;	/* system busy state */
static ccAgentCommand command;	/* agent command to process 
				 */

static long scanDelayTicks;	/* ticks between task scans 
				 */
static long elapsedCommandTime;	/* elpased command time */
static long elapsedHeartbeatTime;	/* elapsed
					   heartbeat time */
static long heartbeat;		/* heartbeat counter */

static struct dbAddr carIval;	/* place to write CAR value 
				 */
static struct dbAddr carImss;	/* place to write CAR
				   message */
static struct dbAddr heartbeatVal;	/* place to write
					   heartbeat */

long flamCcDebug = FALSE;

static MSG_Q_ID ccCommandQ = NULL;	/* commands arrive
					   here */

/*
 *
 *  Private function prototypes
 *
 */

static int ccAgentSimTask (int, int, int, int, int, int, int, int, int, int);



/*
 *
 *  Public functions
 *
 */


/*
 *  Add header for initCcAgentSim
 */

long
initCcAgentSim (MSG_Q_ID messageQ	/* queue to wait
					   for commands on */
  )
{
  int taskId;			/* timer task ID */
  long status;			/* function return status */


  /* 
   *  Make sure that this function is only processed once.
   */

  if (ccCommandQ)
    {
      return OK;
    }


  /* 
   *  Initialize the agent
   */

  ccCommandQ = messageQ;

  scanDelayTicks = sysClkRateGet () / CC_AGENT_SCAN_RATE;
  elapsedCommandTime = 0;
  elapsedHeartbeatTime = 0;
  heartbeat = 0;
  ccAgentState = CC_AGENT_IDLE;
  ccActionState = CAR_IDLE;


  /* 
   *  Connect to the agent heartbeat record
   */

  strcpy (buffer, "flam2:cc:heartbeat.VAL");
  status = dbNameToAddr (buffer, &heartbeatVal);
  if (status)
    {
      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
      return status;
    }


  /* 
   *  Create the timekeeping task
   */

  taskId = taskSpawn ("tCcAgentSim",	/* task name */
		      CC_AGENT_PRIORITY,	/* set
						   priority 
						 */
		      CC_AGENT_OPTIONS,	/* set options */
		      CC_AGENT_STACK,	/* set stack size */
		      ccAgentSimTask,	/* function to
					   spawn */
		      0, 0, 0, 0, 0, 0, 0, 0, 0, 0);	/* invocation 
							   args 
							 */
  if (taskId == NULL)
    {
      status = -1;
      logMsg ("can't create CC agent simulator", 0, 0, 0, 0, 0, 0);
      return status;
    }

  return OK;
}


/*
 *  Header for cc agent simulator task
 */


int ccAgentSimTask
  (int a1, int a2, int a3, int a4, int a5,
   int a6, int a7, int a8, int a9, int a10)
{
  long startingTicks;		/* system ticks at start of 
				   task */
  long elapsedTicks;		/* system ticks elapsed
				   during task */
  long status;			/* function return status */


  while (TRUE)
    {
      /* 
       *  Get the time at task start
       */

      startingTicks = tickGet ();


      /* 
       *  Process according to the current state of the command
       *  simulation state machine.
       */

      switch (ccAgentState)
	{
	  /* 
	   *  In IDLE state wait for a new command to arrive
	   */

	case CC_AGENT_IDLE:
	  status = msgQReceive (ccCommandQ,
				(char *) &command, sizeof (command), NO_WAIT);

	  if (status == ERROR && errno == S_objLib_OBJ_UNAVAILABLE)
	    {
	      break;
	    }

	  else if (status == sizeof (command))
	    {
	      ccAgentState = CC_AGENT_STARTING;
	    }

	  else
	    {
	      logMsg ("ccAgentSim:msgQReceive failed\n", 0, 0, 0, 0, 0, 0);
	    }

	  break;


	  /* 
	   *  In STARTING state we locate the car record and set it to BUSY
	   */

	case CC_AGENT_STARTING:

	  /* 
	   *  Locate the CAR record associated with this command
	   */

	  strcpy (buffer, command.carName);
	  strcat (buffer, ".IVAL");

	  status = dbNameToAddr (buffer, &carIval);
	  if (status)
	    {
	      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
	      ccAgentState = CC_AGENT_IDLE;
	      break;
	    }


	  strcpy (buffer, command.carName);
	  strcat (buffer, ".IMSS");

	  status = dbNameToAddr (buffer, &carImss);
	  if (status)
	    {
	      logMsg ("can't locate PV: %s", (int) &buffer, 0, 0, 0, 0, 0);
	      ccAgentState = CC_AGENT_IDLE;
	      break;
	    }


	  /* 
	   *  Make sure that the CAR record is BUSY while executing
	   *  commands.
	   */

	  if (ccActionState != CAR_BUSY)
	    {
	      ccActionState = CAR_BUSY;
	      strcpy (buffer, "");

	      status = dbPutField (&carImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IMSS", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&carIval, DBR_LONG, &ccActionState, 1);
	      if (status)
		{
		  logMsg ("can't write to CAR.IVAL", 0, 0, 0, 0, 0, 0);
		  ccAgentState = CC_AGENT_IDLE;
		  break;
		}
	    }


	  /* 
	   *  Start executing the received command
	   */

	  if (flamCcDebug)
	    logMsg ("<%ld> %s starting execution->%s\n",
		    (int) tickGet (),
		    (int) &command.commandName,
		    (int) &command.attributeA, 0, 0, 0);


	  ccAgentState = CC_AGENT_EXECUTING;
	  break;


	  /* 
	   *  In EXECUTING state we fake execution by waiting for the
	   *  given execution time then setting the CAR to IDLE
	   */

	case CC_AGENT_EXECUTING:

	  /* 
	   *  If another command is ready stay busy and
	   *  go back to execute it immediately...
	   */

	  if (msgQNumMsgs (ccCommandQ) != 0)
	    {
	      status = msgQReceive (ccCommandQ,
				    (char *) &command,
				    sizeof (command), NO_WAIT);

	      if (status == sizeof (command))
		{
		  if (flamCcDebug)
		    {
		      elapsedCommandTime = 0;
		      logMsg ("<%ld> %s starting execution->%s\n",
			      (int) tickGet (),
			      (int) &command.commandName,
			      (int) &command.attributeA, 0, 0, 0);
		    }
		}

	      else
		{
		  logMsg ("ccAgentSim:msgQReceive failed\n", 0, 0, 0, 0, 0,
			  0);
		}
	    }


	  /* 
	   *  Wait for the execution time to expire
	   */

	  if (elapsedCommandTime++ >= command.executionTime)
	    {
	      elapsedCommandTime = 0;

	      if (flamCcDebug)
		logMsg ("<%ld> cc finished execution\n",
			(int) tickGet (), 0, 0, 0, 0, 0);


	      /* 
	       *  Throw in some fake execution errors sometimes
	       */

	      if (strcmp (command.attributeA, "ERROR") == 0 ||
		  tickGet () % 1000 == 0)
		{
		  logMsg ("Nasty component controller fault\n",
			  0, 0, 0, 0, 0, 0);

		  ccActionState = CAR_ERROR;
		  strcpy (buffer, "Nasty component controller fault....");
		}
	      else
		{
		  ccActionState = CAR_IDLE;
		  strcpy (buffer, "");
		}


	      /* 
	       *  Update the component controller activeC record
	       */

	      status = dbPutField (&carImss, DBR_STRING, &buffer, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IMSS", 0, 0, 0, 0, 0, 0);
		}

	      status = dbPutField (&carIval, DBR_LONG, &ccActionState, 1);
	      if (status)
		{
		  logMsg ("can't access CAR IVAL", 0, 0, 0, 0, 0, 0);
		}
	      ccAgentState = CC_AGENT_IDLE;
	    }

	  break;

	}			/* end switch */


      /* 
       *  Update the heartbeat counter once a second and write the
       *  new value to the agent heatbeat record....
       */

      if (elapsedHeartbeatTime++ >= CC_AGENT_SCAN_RATE - 1)
	{
	  elapsedHeartbeatTime = 0;
	  heartbeat++;
	  status = dbPutField (&heartbeatVal, DBR_LONG, &heartbeat, 1);
	  if (status)
	    {
	      logMsg ("can't access heartbeatVal", 0, 0, 0, 0, 0, 0);
	    }
	}

      /* 
       *  Task finished .... wait for a bit then run it again
       */

      elapsedTicks = tickGet () - startingTicks;
      if (elapsedTicks >= scanDelayTicks)
	{
	  logMsg ("ccAgentSim took too long\n", 0, 0, 0, 0, 0, 0);
	}
      else
	{
	  taskDelay (scanDelayTicks - elapsedTicks);
	}

    }				/* end while TRUE */

  return OK;			/* for completeness.... */
}
