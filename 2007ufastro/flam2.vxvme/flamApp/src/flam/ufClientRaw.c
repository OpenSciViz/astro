#if !defined(__UFClientRaw_c__)
#define __UFClientRaw_c__ "$Name:  $ $Id: ufClientRaw.c,v 0.0 2007/02/02 21:09:55 aaguayo Developmental $"
static const char rcsIdUFClientRaw[] = __UFClientRaw_c__;

#include "ufClient.h"
__UFClient_H__ (ClientRaw_c)
#include "ufLog.h"
  __UFLog_H__ (ClientRaw_c)
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
#define  MAXNAMLEN  512
#define  MSG_WAITALL    0x40	/* wait for full request or 
				   error in recv (ufRecv) */
#include "flam.h"
#include "fcntl.h"
#include "sys/fcntlcom.h"
#else
#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "netdb.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "inttypes.h"
#include "math.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "sys/socket.h"
#include "netinet/in.h"		/* IP_ */
#include "string.h"
#ifdef LINUX
#include "sys/ioctl.h"
#include "asm/ioctls.h"		/* FIONREAD */
#include "netinet/tcp.h"	/* TCP_ */
#if !defined(MSG_WAITALL)
#define MSG_WAITALL 0x100
#endif
#endif
#ifdef SOLARIS
#include "sys/filio.h"		/* FIONREAD */
#include "xti_inet.h"		/* TCP_ */
#endif
#endif

/* #include "ufdbl.h" */

static int maxtry = 30;	/* max # of checks for
			   socket data available in 
			   ufRecv */
static float sleeptime = 0.1;	/* default sleep
				   time (seconds)
				   between each
				   check */

static char _UFIPAddr[] = "000.000.000.000";

/***************************** basic os functions **************************/

/* this can be used for sockets or fifos */

int ufAvailable (int fd)
{
  int retval = 0, stat = -1;

  stat = ioctl (fd, FIONREAD, (int) &retval);

  if (stat != 0)
    {
      sprintf (_UFerrmsg, "ufAvailable> fd= %d, error= %s", fd,
	       strerror (errno));
      ufLog (_UFerrmsg);
      return (-1);
    }
  return retval;
}

int
ufFifoAvailable (const char *fifoname)
{
  int fd, retval = 0;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  fd = open (fifoname, O_RDONLY | O_NONBLOCK, 0777);
  NumFiles++;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */
#else
  fd = open (fifoname, O_RDONLY | O_NONBLOCK);
#endif

  if (fd >= 0)
    {
      int stat = ioctl (fd, FIONREAD, (int) &retval);
      close (fd);
      NumFiles--;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files 
         is %d\n \n",NumFiles) ; */
      if (stat != 0)
	retval = -1;
    }
  sprintf (_UFerrmsg, "ufFifoAvailable> fd= %d, error= %s", fd,
	   strerror (errno));
  ufLog (_UFerrmsg);

  return retval;
}

const char *
ufHostTime (char *tz)
{
  static char *s = 0, ts[] = "::yyyy:ddd:hh:mm:ss.uuuuuu";
  struct tm *t, tbuf;
  struct timespec tspec;
  time_t sec;
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  int rv;
#endif

#if defined(SOLARIS)
  int stat = clock_gettime (CLOCK_REALTIME, &tspec);
  if (stat != 0)
    ufLog ("ufHostTime> failed to get system time");
  sec = tspec.tv_sec;
#else
  sec = time (0);
#endif

  if (tz == 0)
    {
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
      rv = localtime_r (&sec, &tbuf);
      t = &tbuf;
#else
      t = localtime_r (&sec, &tbuf);
#endif
      tz = getenv ("TZ");
    }
  else if (strcmp ("utc", tz) == 0 || strcmp ("UTC", tz) == 0 ||
	   strcmp ("gmt", tz) == 0 || strcmp ("GMT", tz) == 0)
    {				/* use localtime */
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
      rv = localtime_r (&sec, &tbuf);	/* mt-safe */
      t = &tbuf;
#else
      t = localtime_r (&sec, &tbuf);	/* mt-safe */
#endif
      tz = getenv ("TZ");
    }
  else
    {
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
      rv = gmtime_r (&sec, &tbuf);	/* mt-safe */
      t = &tbuf;
#else
      t = gmtime_r (&sec, &tbuf);	/* mt-safe */
#endif
      tz = "UTC";
    }

  if (tz == 0)
    tz = "LOCAL";		/* if this is still empty,
				   don't guess... */

  if (s == 0)
    s = calloc (1 + strlen (tz) + strlen (ts), sizeof (char));

  sprintf (s, "%s::%04d:%03d:%02d:%02d:%02d.%06d", tz,
	   1900 + t->tm_year, t->tm_yday, t->tm_hour,
	   t->tm_min, t->tm_sec, (int) tspec.tv_nsec / 1000);

  return s;
}

const char *
ufHostName ()
{
  static char host[MAXNAMLEN + 1];
  memset (host, 0, sizeof (host));
  gethostname (host, sizeof (host));
  return host;
}

/* posix nanosleep */

void
ufSleep (float time)
{
  struct timespec timeout;
  timeout.tv_sec = (long) floor (time);
  timeout.tv_nsec = (long) floor (999999999 * (time - timeout.tv_sec));
  nanosleep (&timeout, 0);
}

/******************************** bsd socket i/o *****************************/

int
ufIsIPAddress (const char *host)
{
  unsigned short int count = 0;

  for (; host && *host; ++host)
    {
      if ('.' == *host)
	++count;
      else if (!isdigit ((int) *host))
	return 0;
    }

  return (3 == count) ? 1 : 0;
}

int
ufSetSocket (int socFd, struct sockaddr_in *addr, int portNo)
{
  int stat, flags;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
#else
  int optval = 1;
  struct linger setLinger;
#endif

  memset (addr, 0, sizeof (struct sockaddr_in));
  addr->sin_family = AF_INET;
  addr->sin_port = htons (portNo);

  /* explicitly set socket to blocking */

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  /* flags = fcntl( socFd, F_GETFL, 0 ); */
#else
  flags = fcntl (socFd, F_GETFL, 0);
#endif

  if (flags < 0)
    {
      sprintf (_UFerrmsg, "ufSetSocket> Failed to get sock flags "
	       " because %s", strerror (errno));
      ufLog (_UFerrmsg);
    }

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  /* flags = fcntl( socFd, F_SETFL, flags & ~O_NONBLOCK ) ; 
   */
#else
  flags = fcntl (socFd, F_SETFL, flags & ~O_NONBLOCK);
#endif

  if (flags < 0)
    {
      ufLog ("ufSetSocket> failed to set ~O_NONBLOCK");
    }
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  stat = 1;
#else
  setLinger.l_onoff = 0;
  setLinger.l_linger = 0;

  stat = setsockopt (socFd, SOL_SOCKET, SO_LINGER,
		     (char *) &setLinger, sizeof (setLinger));
  /* detect dead connection */
  stat = setsockopt (socFd, SOL_SOCKET, SO_KEEPALIVE,
		     (char *) &optval, sizeof (optval));
  /* immediately deliver msg */
  /* stat = setsockopt(socFd, IPPROTO_TCP, TCP_NODELAY,
     (char *)&optval, sizeof (optval)); */
  /* allow fast re-binds */
  stat = setsockopt (socFd, SOL_SOCKET, SO_REUSEADDR,
		     (char *) &optval, sizeof (optval));

  /* use max. buffer size */
  optval = 1024 * 1024;
  stat = setsockopt (socFd, SOL_SOCKET, SO_SNDBUF,
		     (char *) &optval, sizeof (optval));
  stat = setsockopt (socFd, SOL_SOCKET, SO_RCVBUF,
		     (char *) &optval, sizeof (optval));
#endif
  if (stat < 0)
    {
      sprintf (_UFerrmsg, "ufSetSocket> failed to set an option "
	       "because %s, errno= %d", strerror (errno), errno);
      ufLog (_UFerrmsg);
    }
  else
    {
      ufLog ("ufSetSocket> set options succeeded.");
    }

  return stat;
}

int
ufClose (int socFd)
{
  if (socFd > 0)
    {
      NumFiles--;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files 
         is %d\n \n",NumFiles) ; */
    }
  return close (socFd);
}

int
ufConnect (const char *host, int portNo)
{
  int conStat, socFd;
  struct sockaddr_in addr;
#if !defined(vxWorks) && !defined(VxWorks) && !defined(VXWORKS) && !defined(Vx)
  struct hostent *hostEntry = 0;
  struct in_addr in;
  char **paddr;
#else
  struct timeval wait;
#endif

  sprintf (_UFerrmsg,
	   "ufConnect> attempting connect to host: %s, via port: %d", host,
	   portNo);
  ufLog (_UFerrmsg);

  socFd = socket (AF_INET, SOCK_STREAM, 0);
  NumFiles++;
  /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is
     %d\n \n",NumFiles) ; */
  if (socFd < 0)
    {
      ufLog ("ufConnect> unable to create socket!");
      return socFd;
    }

  ufSetSocket (socFd, &addr, portNo);	/* allocates
					   _socket.addr */

  if (ufIsIPAddress (host))
    {				/* host string is actually
				   IP addr. string... */
      strcpy (_UFIPAddr, host);
    }
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  else
    {
      ufLog ("ufConnect> must specify host by IP address!");
    }
#else
  else
    {				/* find host in domain and
				   use system call
				   inet_ntoa */

      hostEntry = gethostbyname (host);

      if (hostEntry == NULL)
	{
	  ufLog ("ufConnect> unable to find host name in domain!");
	  close (socFd);
	  socFd = -1;
	  return socFd;
	}
      else
	ufLog ("ufConnect> found host name in domain...");

      paddr = hostEntry->h_addr_list;
      memcpy (&in.s_addr, *paddr, sizeof (in.s_addr));
      sprintf (_UFIPAddr, "%s", inet_ntoa (in));
      sprintf (_UFerrmsg, "ufConnect> host IP address: %s", _UFIPAddr);
      ufLog (_UFerrmsg);
    }
#endif

  addr.sin_addr.s_addr = inet_addr (_UFIPAddr);
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
  wait.tv_sec = 5;
  wait.tv_usec = 0;
  conStat =
    connectWithTimeout (socFd, (struct sockaddr *) &addr, sizeof (addr),
			&wait);
#else
  conStat = connect (socFd, (struct sockaddr *) &addr, sizeof (addr));
#endif

  if (conStat < 0)
    {
      sprintf (_UFerrmsg, "ufConnect> unable to connect to %s : %d", host,
	       portNo);
      ufLog (_UFerrmsg);
      close (socFd);
      NumFiles--;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files 
         is %d\n \n",NumFiles) ; */
      socFd = -1;
    }
  else
    {
      sprintf (_UFerrmsg, "ufConnect> connected to %s : %d", host, portNo);
      ufLog (_UFerrmsg);
    }

  return socFd;
}

/************* RECEIVE functions: ****************/

int
ufRecv (int socFd, unsigned char *rbuf, int len)	/* raw 
							   bytes 
							   recv 
							 */
{
  int nbtorecv = len;
  int nbrecvd = 0, nbad = 0;
  size_t offset = 0 ;

  /* count number of attempts to check for socket data
     available */
  int try = 0;
  /* this is limited by global variable: maxtry (set in
     ufSetTimeout) */
  int cnt = 32;			/* limit number of attempts 
				   to read socket */
  int socflags = 0;

  socflags = socflags | MSG_WAITALL;

  while (try < maxtry && ufAvailable (socFd) <= 0)
    {
      try++;
      ufSleep (sleeptime);
    }

  if (try >= maxtry)
    {
      sprintf (_UFerrmsg,
	       "ufRecv> nothing available from socket %d after %4.1f sec.",
	       socFd, sleeptime * try);
      ufLog (_UFerrmsg);
      return (-1);
    }
  else if (sleeptime * try > 1.0)
    {
      sprintf (_UFerrmsg, "ufRecv> data available after %4.1f sec...",
	       sleeptime * try);
      ufLog (_UFerrmsg);
    }

  do
    {
      nbrecvd = recv (socFd, rbuf + offset, nbtorecv, socflags);
      if (nbrecvd > 0)
	{
	  nbtorecv -= nbrecvd;
	  offset += nbrecvd;
	}
      if (nbrecvd < 0)
	nbad++;
    }
  while (--cnt >= 0 && nbtorecv > 0 && (errno == 0 || errno == EINTR));

  if (cnt < 30 || nbad > 0)
    {
      sprintf (_UFerrmsg, "ufRecv> recvd from socket %d times (%d bad)",
	       32 - cnt, nbad);
      ufLog (_UFerrmsg);
      sprintf (_UFerrmsg, "ufRecv> nbLastRecvd= %d, nbtorecv= %d, error: %s",
	       nbrecvd, nbtorecv, strerror (errno));
      ufLog (_UFerrmsg);
    }

  return (len - nbtorecv);
}

int
ufSetTimeout (float timeout)
{
  if (timeout < 1.0)
    timeout = 1.0;
  maxtry = (int) (timeout / sleeptime);
  if (maxtry < 2)
    maxtry = 2;
  if (maxtry >= 1000)
    maxtry = 999;
  sprintf (_UFerrmsg, "ufSetTimeout> timeout = %4.1f sec.",
	   maxtry * sleeptime);
  ufLog (_UFerrmsg);
  return maxtry;
}

short
ufRecvShort (int socFd)
{
  short val = 0;
  int nb = ufRecv (socFd, (unsigned char *) &val, sizeof (val));

  if (nb < sizeof (val))
    {
      ufLog ("ufRecShort> failed to recv value.");
      sprintf (_UFerrmsg, "ufRecvShort> read error: %s", strerror (errno));
      ufLog (_UFerrmsg);
      return (-1);
    }
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = ntohs ((const unsigned short) val);
#endif
  return val;
}

int
ufRecvShorts (int socFd, short *buf, int cnt)
{
  int nb = ufRecv (socFd, (unsigned char *) buf, cnt * sizeof (short));

  if (nb < cnt * sizeof (short))
    {
      ufLog ("ufRecShorts> failed to recv all values");
    }
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while (--cnt >= 0)
    buf[cnt] = ntohs ((const unsigned short) buf[cnt]);
#endif

  if (nb < 0)
    return nb;
  else
    return (nb / sizeof (short));
}

int
ufRecvInt (int socFd)
{
  int val = -1;
  int nb = ufRecv (socFd, (unsigned char *) &val, sizeof (val));

  if (nb < sizeof (val))
    {
      ufLog ("ufRecInt> failed to recv value.");
      sprintf (_UFerrmsg, "ufRecvInt> read error: %s", strerror (errno));
      ufLog (_UFerrmsg);
      return (-1);
    }
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (int) ntohl ((const unsigned long) val);
#endif

  return val;
}

int
ufRecvInts (int socFd, int *buf, int cnt)
{
  int nb = ufRecv (socFd, (unsigned char *) buf, cnt * sizeof (int));

  if (nb < cnt * sizeof (int))
    {
      ufLog ("ufRecvInts> failed to recv all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while (--cnt >= 0)
    buf[cnt] = ntohl ((const unsigned long) buf[cnt]);
#endif

  if (nb < 0)
    return nb;
  else
    return (nb / sizeof (int));
}

float
ufRecvFloat (int socFd)
{
  float val = 0;
  int nb = ufRecv (socFd, (unsigned char *) &val, sizeof (val));

  if (nb < sizeof (val))
    {
      ufLog ("ufRecvFloat> failed to recv value");
      sprintf (_UFerrmsg, "ufRecvFloat> read error: %s", strerror (errno));
      ufLog (_UFerrmsg);
    }
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = ntohl ((unsigned long) val);
#endif

  return val;
}

int
ufRecvFloats (int socFd, float *buf, int cnt)
{
  int nb = ufRecv (socFd, (unsigned char *) buf, cnt * sizeof (float));

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  unsigned long *tmp = (unsigned long *) buf;
#endif

  if (nb < cnt * sizeof (int))
    {
      ufLog ("ufRecvFloats> failed to recv all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while (--cnt >= 0)
    tmp[cnt] = ntohl (tmp[cnt]);
#endif

  if (nb < 0)
    return nb;
  else
    return (nb / sizeof (float));
}

int
ufRecvCstr (int socFd, char *rbuf, int len)	/* recv
						   var.len. 
						   char.
						   string */
{
  int nb = 0, slen;
  char *tmpbuf = 0;

  slen = ufRecvInt (socFd);	/* first read the strlen */

  if (slen < 0 || slen > USHRT_MAX)
    {
      sprintf (_UFerrmsg, "ufRecvCstr> bad strlen: %d, from socFd: %d.", slen,
	       socFd);
      ufLog (_UFerrmsg);
      return (-1);
    }
  nb = 4;
  /* ok to read the string */
  tmpbuf = calloc (1 + slen, sizeof (char));	/* alloc &
						   clear
						   string
						   memory */

  if (NULL == tmpbuf)
    {
      sprintf (_UFerrmsg, "ufRecvCstr> Memory allocation failure");
      ufLog (_UFerrmsg);
      return (-1);
    }

  nb += ufRecv (socFd, tmpbuf, slen);

  /* make sure to clear any old stuff */
  memset (rbuf, 0, len);

  /* truncate if tmpbuf is longer than rbuf */
  if (len > slen ) len = slen;
  strncpy (rbuf, tmpbuf, len);

  free (tmpbuf);
  return nb;
}

/* for use with UFStrings: caller must free memory allocated here: */

int
ufRecvNewCstr (int socFd, char **rbuf)
{
  int nb = 0, slen;

  slen = ufRecvInt (socFd);	/* first read the strlen */

  if (slen < 0 || slen > USHRT_MAX)
    {
      *rbuf = 0;
      sprintf (_UFerrmsg, "ufRecvNewCstr> bad strlen: %d, from socFd: %d.",
	       slen, socFd);
      ufLog (_UFerrmsg);
      return (-1);
    }
  nb = 4;
  *rbuf = calloc (1 + slen, sizeof (char));	/* alloc
						   memory
						   for
						   string */

  if (NULL == *rbuf)
    {
      sprintf (_UFerrmsg, "ufRecvNewCstr> Memory allocation failure");
      ufLog (_UFerrmsg);
      return (-1);
    }
  return ufRecv (socFd, *rbuf, slen) + nb;
}

/********** SEND functions: ***********/

int
ufSend (int socFd, const unsigned char *sndbuf, int nb)	/* raw 
							   bytes 
							   send 
							 */
{
  int nbtosend = nb;
  int nbsent = 0, offset = 0, nbad = 0;
  int cnt = 32;			/* limit number of attempts 
				 */

  do
    {
      nbsent = send (socFd, sndbuf + offset, nbtosend, 0);
      if (nbsent > 0)
	{
	  nbtosend -= nbsent;
	  offset += nbsent;
	}
      if (nbsent < 0)
	nbad++;
      if (nbtosend > 0)
	ufSleep (0.01);		/* give it a rest */
    }
  while ((--cnt >= 0) && (nbtosend > 0) && (errno == 0 || errno == EINTR));

  if (cnt < 31)
    {
      sprintf (_UFerrmsg, "ufSend> sent to socket %d times (%d bad)",
	       32 - cnt, nbad);
      ufLog (_UFerrmsg);
      sprintf (_UFerrmsg, "ufSend> nbLastSent= %d, nbtosend= %d, error: %s",
	       nbsent, nbtosend, strerror (errno));
      ufLog (_UFerrmsg);
    }

  return (nb - nbtosend);
}

int
ufSendCstr (int socFd, const char *cstr)	/* send
						   var.len. 
						   char.
						   string */
{
  int nb, len = strlen (cstr);

  nb = ufSendInt (socFd, len);	/* first send int length,
				   then send string */

  /*
    sprintf( _UFerrmsg, __HERE__ "> %s", cstr ) ;
    ufLog( _UFerrmsg ) ;
  */

  return ufSend (socFd, (const unsigned char *) cstr, len) + nb;
}

int
ufSendShort (int socFd, short val)
{
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (unsigned short) htons ((const unsigned short) val);
#endif
  return ufSend (socFd, (const unsigned char *) &val, sizeof (val));
}

int
ufSendShorts (int socFd, short *buf, int cnt)
{
  int nb, nbc = cnt * sizeof (short);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = htons ((const unsigned short) buf[icnt]);
#endif

  nb = ufSend (socFd, (const unsigned char *) buf, nbc);

  if (nb < nbc)
    ufLog ("ufSendShorts> failed to send all values");

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = ntohs ((const unsigned short) buf[icnt]);
#endif

  return (nb / sizeof (short));
}

int
ufSendInt (int socFd, int val)
{
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (int) htonl ((const unsigned long) val);
#endif
  return ufSend (socFd, (const unsigned char *) &val, sizeof (val));
}

int
ufSendInts (int socFd, int *buf, int cnt)
{
  int nb, nbc = cnt * sizeof (int);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = htonl ((const unsigned long) buf[icnt]);
#endif

  nb = ufSend (socFd, (const unsigned char *) buf, nbc);

  if (nb < nbc)
    ufLog ("ufSendInts> failed to send all values");

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = ntohl ((const unsigned long) buf[icnt]);
#endif
  return (nb / sizeof (int));
}

int
ufSendFloat (int socFd, float val)
{
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (float) htonl ((unsigned long) val);
#endif
  return ufSend (socFd, (const unsigned char *) &val, sizeof (val));
}

int
ufSendFloats (int socFd, float *buf, int cnt)
{
  int nb, nbc = cnt * sizeof (int);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = (float) htonl ((unsigned long) buf[icnt]);
#endif

  nb = ufSend (socFd, (const unsigned char *) buf, nbc);

  if (nb < nbc)
    ufLog ("ufSendFloats> failed to send all values");

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while (--icnt >= 0)
    buf[icnt] = (float) ntohl ((unsigned long) buf[icnt]);
#endif
  return (nb / sizeof (float));
}

int ufInetStartSim(const char* host) {
  char authenticate[128];
  int slen = sizeof(authenticate);
  int soc = ufConnect(host, 3720);
  if( soc < 0 ) 
    return soc;

  memset(authenticate, 0, slen);
  strcpy(authenticate, "UF FLAM BOOT SIM @ ");
  strcat(authenticate, ufHostName());
  slen = ufSendCstr(soc, authenticate);
  ufClose(soc);
  return slen;
}

int ufInetStartup(const char* host) {
  char authenticate[128];
  int slen = sizeof(authenticate);
  int soc = ufConnect(host, 3720);
  if( soc < 0 ) 
    return soc;

  memset(authenticate, 0, slen);
  strcpy(authenticate, "UF TRECS BOOT @ ");
  strcat(authenticate, ufHostName());
  slen = ufSendCstr(soc, authenticate);
  ufClose(soc);
  return slen;
}

int ufInetShutdown(const char* host) {
  char authenticate[128];
  int slen = sizeof(authenticate);
  int soc = ufConnect(host, 3720);
  if( soc < 0 ) 
    return soc;

  memset(authenticate, 0, slen);
  strcpy(authenticate, "UF TRECS SHUTDOWN @ ");
  strcat(authenticate, ufHostName());
  slen = ufSendCstr(soc, authenticate);
  ufClose(soc);
  return slen;
}

#endif /* __UFClientRaw_c__ */
