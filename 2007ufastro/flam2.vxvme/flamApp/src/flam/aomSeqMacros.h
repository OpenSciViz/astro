/** \file aomSeqMacros.h
 *  \brief This file contaons useful macros for AOM sequence programs.
 *  \author Alan Greer (originally from Keck) (ajg@observatorysciences.co.uk)
 *  \date 09-11-2004
 *
 *  \par Macro Names
 *  \li ASSVAR   - Assigns to record\n
 *  \li ASSNOVAR - Assigns to nothing\n
 *  \li MONVAR   - Monitors record\n
 *
 *  \n $Log: aomSeqMacros.h,v $
 *  \n Revision 0.0  2007/02/02 21:09:55  aaguayo
 *  \n initial checkin
 *  \n
 *  \n Revision 1.1.1.1  2005/05/23 10:47:10  cjm
 *  \n Beta Release under GEM9.0
 *  \n
 *  \n Revision 1.1  2004/11/09 15:18:26  gemini
 *  \n Useful macros that can be used in sequence programs.
 *  \n
 */
#ifndef AOMSEQMACROSH
#define AOMSEQMACROSH

#define ASSVAR( _type, _intvar, _pvname) \
        _type _intvar; assign _intvar to _pvname

#define ASSNOVAR( _type, _intvar) \
        _type _intvar; assign _intvar to ""

#define MONVAR( _type, _intvar, _pvname) \
        ASSVAR( _type, _intvar, _pvname); monitor _intvar

#endif
