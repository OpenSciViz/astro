pro Monitor_Envir, INTERVAL=interval, HOST=host, $
			FILE=MonFile, DIRECTORY=directory, FDATE=fdate, FTYPE=ftype

	if N_elements( MonFile ) eq 1 then begin
		if N_elements( directory ) ne 1 then directory ="/net/newton/data"
		if N_elements( ftype ) ne 1 then ftype ="logidl"
		if N_elements( fdate ) ne 1 then begin
			date = strcompress( systime(), /REM )
			fdate = strmid( date, strpos(date,":",/REVERSE_SEARCH)+3, 4 ) $
				 + "-" + strmid( date, 3, strpos(date,":")-5 )
				
		   endif
		sz = size( MonFile )
		if sz(sz(0)+1) ne 7 then MonFile = "trecsCryoTest"
		LogFile = MonFile + fdate + "." + ftype
		message,"creating/appending LogFile: " + LogFile,/INFO
		openw,LunLoc,/get, LogFile,/APPEND
		LogFile = directory + "/" + LogFile
		message,"creating/appending LogFile: " + LogFile,/INFO
		openw,LunRem,/get, LogFile,/APPEND
	   endif

	if N_elements( interval ) ne 1 then interval = 59
	if N_elements( host ) ne 1 then host = "ufkepler"

	while (interval ge 0) do begin

		GPvac = '0'
		LS218 = '0'
		LS340 = '0'
		LS340V = '0'
		nbrecvd = 0
		stime = systime()

		if ConnectAgent( AGENT="GranPhil", PORT=52004, HOST=host ) gt 0 then begin
			nbsent = UFagentSend( ["hist",'1'], "value", "GranPhil" )
		 endif else nbsent = 0

		if nbsent GT 0 then begin
			srecvd = UFagentRecv( phdr, "GranPhil" )
			nbrecvd = nbrecvd + phdr.Length
			if phdr.Length LE 0 then begin
				CloseAgent,"GranPhil"
			  endif else GPvac = srecvd
		 endif else CloseAgent,"GranPhil"

		if ConnectAgent( AGENT="LakeShore218", PORT=52002, HOST=host ) gt 0 then begin
			nbsent = UFagentSend( ["hist",'1'], "value", "LakeShore218" )
		 endif else begin
			nbsent = 0
		  endelse

		if nbsent GT 0 then begin
			srecvd = UFagentRecv( phdr, "LakeShore218" )
			nbrecvd = nbrecvd + phdr.Length
			if phdr.Length gt 0 then begin
				LS218 = strmid( srecvd, 0, strlen(srecvd)-1 )
			 endif else CloseAgent,"LakeShore218"
		 endif else CloseAgent,"LakeShore218"

		if ConnectAgent( AGENT="LakeShore340", PORT=52003, HOST=host ) gt 0 then begin
			nbsent = UFagentSend( ["hist",'1'], "value", "LakeShore340" )
		 endif else nbsent = 0

		if nbsent GT 0 then begin
			srecvd = UFagentRecv( phdr, "LakeShore340" )
			nbrecvd = nbrecvd + phdr.Length
			if phdr.Length gt 0 then begin
				LS340 = strmid( srecvd, 0, strlen(srecvd)-1 )
				nbsent = UFagentSend( ["raw",'srdg?a;srdg?b'],"value","LakeShore340" )
				srecvd = UFagentRecv( phdr, "LakeShore340" )
				LS340V = strmid( srecvd, 0, strlen(srecvd)-1 )
			 endif else CloseAgent,"LakeShore340"
		 endif else CloseAgent,"LakeShore340"

		print,stime
		print,"Vacuum in mTorr:        " + GPvac
		print,"LakeShore 218 8 chan:   " + LS218
		print,"LakeShore 340 2 chan:   " + LS340
		print,"LakeShore 340  volts:   " + LS340V
		print,"IDL>================================================="

		if (N_elements( LunRem ) eq 1) and (nbrecvd gt 0) then begin
			printf,LunRem,stime
			printf,LunRem,"Vacuum in mTorr:        " + GPvac[0]
			printf,LunRem,"LakeShore 218 8 chan:   " + LS218
			printf,LunRem,"LakeShore 340 2 chan:   " + LS340
			printf,LunRem,"LakeShore 340  volts:   " + LS340V
			printf,LunRem,"================================================="
			flush,LunRem
		   endif

		if (N_elements( LunLoc ) eq 1) and (nbrecvd gt 0) then begin
			printf,LunLoc,stime
			printf,LunLoc,"Vacuum in mTorr:        " + GPvac[0]
			printf,LunLoc,"LakeShore 218 8 chan:   " + LS218
			printf,LunLoc,"LakeShore 340 2 chan:   " + LS340
			printf,LunLoc,"LakeShore 340  volts:   " + LS340V
			printf,LunLoc,"================================================="
			flush,LunLoc
		   endif

		wait, interval
	  endwhile
end
