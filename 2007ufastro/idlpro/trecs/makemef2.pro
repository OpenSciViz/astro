; Create an example T-ReCS MEF file
; 1 extension per 320x240 x nchop x nsaveset data block
; T. Hayward, 2002 April 17

function readhdef, fname 

   nlines = numlines(fname)
   temp = ' '
   openr, lun, fname, /get_lun

   for i=0, nlines-1 do begin
      readf, lun, temp
      if (strmid(temp, 0, 1) NE '#') then begin
         len = strlen(temp)
         keyword = strmid(temp, 0, 8)
         value   = strtrim(strmid(temp, 10, 32), 2)
         comment = strtrim(strmid(temp, 36, len-36), 2)
         sxaddpar, hdr, keyword, value, comment
      endif
   end

   return, hdr
end

pro makemef2

ncols     = 320
nrows     = 240
nchops    = 2
nsavesets = 3  
nnods     = 2
nnodsets  = 4 

; Create the 4-D data array
chopb = indgen(ncols, nrows, /long)
chopa = chopb + 100
data = lonarr(ncols, nrows, nchops, nsavesets)
for i=0,nsavesets-1 do begin
   data[*,*,0,i] = chopa
   data[*,*,1,i] = chopb
endfor

; Create the primary header string array
phdr = readhdef('phdr.txt')
sxaddpar, phdr, 'NNODS', nnods 
sxaddpar, phdr, 'NNODSETS', nnodsets

; Open the FITS file w/ write permission
fits_open, 'trecs2.fits', fcb, /write

; Write the primary FITS header with no data
fits_write, fcb, 0, phdr

nodindex = 0
nodval = ['A', 'B', 'B', 'A']

; Append the FITS extensions
for i1=0, nnodsets-1 do begin
   sxaddpar, shdr, 'NODSET', i1+1
   for i2=0, nnods-1 do begin
      sxaddpar, shdr, 'NOD', nodval[nodindex MOD 4], BEFORE='NODSET'
      fits_write, fcb, data, shdr, xtension='IMAGE'
      nodindex = nodindex + 1
   endfor
endfor

; Close the FITS file
fits_close, fcb

end
