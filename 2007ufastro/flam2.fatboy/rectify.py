#!/usr/bin/python -u
from numarray import *
import numarray.strings
import scipy
from scipy.optimize import leastsq
import pyfits
from UFPipelineOps import *
import curses.ascii
from pyraf import iraf
from datetime import datetime
import Numeric

class rectify:

   def __init__(self, infile, logfile, guiMessages):
      #Read input parameter file
      self.guiMessages = guiMessages
      if (self.guiMessages):
	print "STATUS: Initialization..."
	print "PROGRESS: 0"
      f = open(infile, "rb")
      self.params = f.read()
      f.close()
      #Setup log, warning files
      self.logfile = logfile
      f = open(self.logfile,'wb')
      f.write('LOG of '+infile+' at ' +  str(datetime.today()) + '\n')
      f.write('Run at path: '+os.getcwd()+'\n')
      f.write('--------Parameter List--------\n')
      f.write(self.params)
      f.write('------------------------------\n')
      f.close()
      self.params = self.params.split("\n")
      try: self.params.remove('')
      except ValueError:
        dummy = ''
      for j in range(len(self.params)-1, -1, -1):
        if (self.params[j].strip() == ''): self.params.pop(j)
        elif (not curses.ascii.isalpha(self.params[j][0])): self.params.pop(j)
      self.params = dict([(j[0:j.find("=")].strip().upper(), j[j.find("=")+1:].strip()) for j in self.params])
      self.setDefaultParams()
      self.files = []
      self.ycen = []
      if (guiMessages): print "PROGRESS: 5"
      if (self.params['SETUP_LIST'].lower() == "yes"):
	if (guiMessages): print "STATUS: Finding continua..."
	self.readFiles(self.params['FILE_LIST'], self.params['DARK_FRAME'])
      if (guiMessages): print "PROGRESS: 15"
      if (self.params['DO_RECTIFY'].lower() == "yes"):
        if (guiMessages): print "STATUS: Rectifying continua..."
	speclsq = self.doRectify()
      else:
	speclsq = [0,0,1]
      if (guiMessages):
	print "PROGRESS: 50"
	print "STATUS: Combining sky files..."
      image = self.combineSkyFiles(self.params['SKY_FILE_LIST'], self.params['SKY_DARK_FRAME'])
      if (guiMessages): print "PROGRESS: 55"
      if (self.params['SKY_SETUP_LIST'].lower() == "yes"):
        if (guiMessages): print "STATUS: Finding sky lines..."
	self.readSkyFiles(image)
      if (guiMessages): print "PROGRESS: 60"
      if (self.params['SKY_DO_RECTIFY'].lower() == "yes"):
        if (guiMessages): print "STATUS: Rectifying sky lines..."
	skylsq = self.doSkyRectify()
      else:
	syklsq = [0,1,0]
      if (guiMessages):
        print "PROGRESS: 95"
      if (os.access(self.comfilename, os.F_OK)):
	os.unlink(self.comfilename)
      self.outputFit(speclsq, skylsq)

   def setDefaultParams(self):
      #Set Default Parameters
      self.params.setdefault('FILE_LIST','list.dat')
      self.params.setdefault('DARK_FRAME','')
      self.params.setdefault('SETUP_LIST','yes')
      self.params.setdefault('SPECTRAL_ORIENTATION','horizontal')
      self.params.setdefault('X_CENTER','1024')
      self.params['X_CENTER'] = int(self.params['X_CENTER'])
      self.params.setdefault('LIST_OUTPUT','list.out')
      self.params.setdefault('DO_RECTIFY','yes')
      self.params.setdefault('LIST_INPUT','')
      self.params.setdefault('FIT_ORDER','3')
      self.params['FIT_ORDER'] = int(self.params['FIT_ORDER'])
      self.params.setdefault('SKY_FILE_LIST','list.dat')
      self.params.setdefault('SKY_DARK_FRAME','')
      self.params.setdefault('SKY_SETUP_LIST','yes')
      self.params.setdefault('SKY_SIGMA','3')
      self.params.setdefault('SKY_TWO_PASS_DETECTION','yes')
      self.params.setdefault('SKY_BOX_SIZE','6')
      self.params['SKY_SIGMA'] = int(self.params['SKY_SIGMA'])
      self.params['SKY_BOX_SIZE'] = int(self.params['SKY_BOX_SIZE'])
      self.params.setdefault('SKY_LIST_OUTPUT','list.out')
      self.params.setdefault('SKY_DO_RECTIFY','yes')
      self.params.setdefault('SKY_LIST_INPUT','')
      self.params.setdefault('SKY_FIT_ORDER','5')
      self.params['SKY_FIT_ORDER'] = int(self.params['SKY_FIT_ORDER'])
      self.params.setdefault('FIT_OUTPUT','rect.dat')

   def readFiles(self, infile, dark):
      print "Reading Files..."
      f = open(self.logfile,'ab')
      f.write("Reading Files...\n")
      f.close()
      d = pyfits.open(dark)
      self.xsize = d[0].data.shape[1]
      self.ysize = d[0].data.shape[0]
      #Create Filelist
      if (os.path.isdir(infile)):
        flist = os.listdir(filelist)
      elif (os.path.isfile(infile)):
        f = open(infile, "rb")
        flist = f.read().split('\n')
        f.close()
      elif (infile.count(',') != 0):
        flist = filelist.split(',')
        try: self.filenames.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(flist)):
           flist[j] = flist[j].strip()
      else:
        flist = glob.glob(infile)
      try: flist.remove('')
      except ValueError:
        dummy = ''
      ycenters = []
      yboxsize = []
      for j in range(len(flist)):
	temp = pyfits.open(flist[j])
	temp[0].data = temp[0].data.astype("Float64")
	temp[0].data -= d[0].data
	x = sum(temp[0].data, 1)
	y = extractSpectra(x,3,5)
	if (len(y) > 1):
	   maxval = -1e+6
	   for l in range(len(y)):
	      #currMed = arraymedian(x[y[l][0]:y[l][1]])
	      currMean = x[y[l][0]:y[l][1]].mean()
	      if (currMean > maxval):
		maxval = currMean
		ind = l
	   y = array([y[ind]])
	ycenters.append((y[0][0]+y[0][1])/2)
	yboxsize.append((y[0][1]-y[0][0])/2)
	temp.close()
	print j, flist[j], y
      d.close()
      ycenters = array(ycenters)
      flist = numarray.strings.array(flist)
      yboxsize = array(yboxsize)
      b = where(abs(ycenters-self.ysize/2) == min(abs(ycenters-self.ysize/2)))
      self.ycen = [ycenters[b[0][0]]]
      self.files = [flist[b[0][0]]]
      self.ybox = [yboxsize[b[0][0]]]
      c = where(ycenters != ycenters[b[0][0]])
      ycenters = ycenters[c]
      flist = flist[c]
      yboxsize = yboxsize[c]
      yold = self.ycen[0]
      for j in range(self.ycen[0]-20,0,-20):
	b = where(abs(ycenters-j) == min(abs(ycenters-j)))
	if (ycenters[b[0][0]] > yold): continue
	self.ycen.append(ycenters[b[0][0]])
	self.files.append(flist[b[0][0]])
	self.ybox.append(yboxsize[b[0][0]])
	c = where(ycenters != ycenters[b[0][0]])
	ycenters = ycenters[c]
	flist = flist[c]
	yboxsize = yboxsize[c]
	yold = self.ycen[len(self.ycen)-1]
	if (len(ycenters) == 0): break
      yold = self.ycen[0]
      for j in range(self.ycen[0]+20,self.ysize-1,20):
        b = where(abs(ycenters-j) == min(abs(ycenters-j)))
        if (ycenters[b[0][0]] < yold): continue
        self.ycen.append(ycenters[b[0][0]])
        self.files.append(flist[b[0][0]])
        self.ybox.append(yboxsize[b[0][0]])
        c = where(ycenters != ycenters[b[0][0]])
        ycenters = ycenters[c]
        flist = flist[c]
        yboxsize = yboxsize[c]
        if (len(ycenters) == 0): break
      f = open(self.params['LIST_OUTPUT'],'wb')
      for j in range(len(self.ycen)):
	f.write(self.files[j]+'\t'+str(self.ycen[j])+'\t'+str(self.ybox[j])+'\n')
      f.close()

   def gaussResiduals(self, p, x, out):
      f = zeros(len(x))+0.
      z = (x-p[1])/p[2]
      f = p[3]+p[0]*math.e**(-z**2/2)
      err = out-f
      err = Numeric.array(err.tolist())
      return err

   def surfaceResiduals(self, p, x, y, out, order):
      f = zeros(len(x))+0.
      n = 1
      for j in range(1,order+1):
        for l in range(j+1):
           f+=p[n]*x**(j-l)*y**l
           n+=1
      err = out - f
      err = Numeric.array(err.tolist())
      return err

   def polyResiduals(self, p, x, y, out, order):
      f = zeros(len(x))+0.
      f+=p[0]*y
      for j in range(1,order+1):
	f+=p[j]*x**j
      err = out-f
      err = Numeric.array(err.tolist())
      return err

   def doRectify(self):
      print "Performing rectification..."
      f = open(self.logfile,'ab')
      f.write("Performing rectification...\n")
      f.close()
      if (self.params['LIST_INPUT'] != ''):
	f = open(self.params['LIST_INPUT'],'rb')
	temp = f.read().split('\n')
	f.close()
	try: temp.remove('')
        except ValueError:
           dummy = ''
	self.files = []
	self.ycen = []
	self.ybox = []
	for j in range(len(temp)):
	   t = temp[j].split()
	   self.files.append(t[0])
	   self.ycen.append(float(t[1]))
	   self.ybox.append(float(t[2]))
	temp = pyfits.open(self.files[0])
        self.xsize = temp[0].data.shape[1]
        self.ysize = temp[0].data.shape[0]
	temp.close()
      print "Using "+str(len(self.files))+" files..."
      f = open(self.logfile,'ab')
      f.write("Using "+str(len(self.files))+" files...\n")
      f.close()
      d = pyfits.open(self.params['DARK_FRAME'])
      d[0].data = d[0].data.astype("Float64")
      xs = [self.params['X_CENTER']]
      for j in range(self.params['X_CENTER']-5,20,-5):
	xs.append(j)
      for j in range(self.params['X_CENTER']+5,self.xsize-20,5):
	xs.append(j)
      xin = zeros((len(xs), len(self.files)),"Float64")
      yin = zeros((len(xs), len(self.files)),"Float64")
      yout = zeros((len(xs), len(self.files)),"Float64")
      for j in range(len(self.files)):
	if (guiMessages):
	   print "PROGRESS: "+str(int(15+(j+0.)/len(self.files)*35))
	temp = pyfits.open(self.files[j])
	temp[0].data = temp[0].data.astype("Float64")
	temp[0].data -= d[0].data
	currY = self.ycen[j]
	for l in range(len(xs)):
	   if (xs[l] == xs[0]+5): currY = self.ycen[j]
	   ylo = int(currY - self.ybox[j])
	   yhi = int(currY + self.ybox[j])
	   y = sum(temp[0].data[:,xs[l]-2:xs[l]+3], 1)+0.
	   x = arange(len(y))+0.
	   p = zeros(4)+0.
	   p[0] = max(y[ylo:yhi])
	   p[1] = (where(y[ylo:yhi] == p[0]))[0][0]+ylo
	   p[2] = self.ybox[j]/2
	   p[3] = arraymedian(y)
	   p = Numeric.array(p.tolist());
	   lsq = leastsq(self.gaussResiduals, p, args=(x[ylo:yhi], y[ylo:yhi]))
           if (l == 0):
              currY = lsq[0][1]
              xin[0][j] = xs[0]
              yin[0][j] = lsq[0][1]
              yout[0][j] = yin[0][j]
           else:
              if (abs(lsq[0][1] - currY) < 2):
                currY = lsq[0][1]
                xin[l][j] = xs[l]
                yin[l][j] = currY
                yout[l][j] = yin[0][j]
              elif (xs[l] == xs[0]+5 and abs(lsq[0][1] - yin[0][j]) < 2):
                currY = lsq[0][1]
                xin[l][j] = xs[l]
                yin[l][j] = currY
                yout[l][j] = yin[0][j]
	temp.close()
	print j,yin[0][j]
        f = open(self.logfile,'ab')
        f.write(self.files[j]+'\tCenter: '+str(yin[0][j])+'\n')
        f.close()
      d.close()
      order = self.params['FIT_ORDER']
      terms = 0
      for j in range(order+2):
	terms+=j
      p = zeros(terms)
      p[2] = 1
      xin = array(xin)
      yin = array(yin)
      yout = array(yout)
      xin = reshape(xin, xin.nelements())+0.
      yin = reshape(yin, yin.nelements())+0.
      yout = reshape(yout, yout.nelements())+0.
      center = float(self.ysize-1)*0.5
      b = where(xin != 0)
      yout = array(yout)-center
      xin = array(xin)-center
      yin = array(yin)-center
      #yout = array(yout)
      #xin = array(xin)
      #yin = array(yin)
      p = Numeric.array(p.tolist());
      lsq = leastsq(self.surfaceResiduals, p, args=(xin[b], yin[b], yout[b], order))
      print lsq[0]
      f = open(self.logfile,'ab')
      f.write("Fit Coeffs: "+str(lsq[0])+"\n")
      f.close()
      yfit = yin*0.
      n = 0
      for j in range(order+1):
        for l in range(j+1):
           yfit[b]+=lsq[0][n]*xin[b]**(j-l)*yin[b]**l
           n+=1
      tempmean = (yout[b]-yfit[b]).mean()
      tempstddev = (yout[b]-yfit[b]).stddev()
      print "Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)
      f = open(self.logfile,'ab')
      f.write("Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)+"\n")
      f.close()
      bad = where(abs(yout[b]-yfit[b]-tempmean)/tempstddev > 2)
      if (len(bad[0]) > 0):
	good = where(abs(yout[b]-yfit[b]-tempmean)/tempstddev <=2)
        p = zeros(terms)
        p[2] = 1
        p = Numeric.array(p.tolist());
	lsq = leastsq(self.surfaceResiduals, p, args=(xin[b][good], yin[b][good], yout[b][good], order))
	print 'REDO: ',lsq[0]
        f = open(self.logfile,'ab')
        f.write("Throwing away outliers and refitting...\n")
	f.write("New fit: "+str(lsq[0])+'\n')
        f.close()
	yfit = yin*0.
	n = 0
	for j in range(order+1):
           for l in range(j+1):
              yfit[b]+=lsq[0][n]*xin[b]**(j-l)*yin[b]**l
              n+=1
        tempmean = (yout[b][good]-yfit[b][good]).mean()
        tempstddev = (yout[b][good]-yfit[b][good]).stddev()
        print "Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)
        f = open(self.logfile,'ab')
        f.write("Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)+"\n")
        f.close()
      return lsq[0]

   def combineSkyFiles(self, infile, dark):
      print "Combining sky files and subtracting darks..."
      f = open(self.logfile,'ab')
      f.write("Combining sky files and subtracting darks...\n")
      f.close()
      d = pyfits.open(dark)
      self.comfilename = "TempSkyRect.fits"
      if (os.access(self.comfilename, os.F_OK)): os.unlink(self.comfilename)
      if (infile.lower().find('.fit') == -1):
	if (os.path.isdir(infile)):
	   flist = os.listdir(filelist)
        elif (os.path.isfile(infile)):
           f = open(infile, "rb")
           flist = f.read().split('\n')
           f.close()
        elif (infile.count(',') != 0):
           flist = filelist.split(',')
           try: self.filenames.remove('')
           except ValueError:
              dummy = ''
           for j in range(len(flist)):
              flist[j] = flist[j].strip()
        else:
           flist = glob.glob(infile)
        try: flist.remove('')
        except ValueError:
           dummy = ''
	frames = ""
	for j in range(len(flist)):
	   temp = pyfits.open(flist[j])
	   temp[0].data -= d[0].data
	   if (os.access('TMPskyRect'+str(j)+'.fits', os.F_OK)):
	      os.unlink('TMPskyRect'+str(j)+'.fits')
	   temp.writeto('TMPskyRect'+str(j)+'.fits')
	   temp.close()
	   frames+=","+"TMPskyRect"+str(j)+'.fits'
	frames = frames.strip(",")
	iraf.unlearn('imcombine')
	if (len(frames) > 1023):
	   tmpFile = open("tmpIRAFFile.dat", "wb")
           tmpFile.write(frames.replace(",","\n"))
           tmpFile.close()
           iraf.images.immatch.imcombine("@tmpIRAFFile.dat", self.comfilename, combine="median")
           os.unlink("tmpIRAFFile.dat")
	else:
           iraf.images.immatch.imcombine(frames, self.comfilename, combine="median")
	for j in range(len(flist)):
	   os.unlink('TMPskyRect'+str(j)+'.fits')
      else:
	temp = pyfits.open(infile)
	temp[0].data -= d[0].data
	temp.writeto(self.comfilename)
	temp.close()
      d.close()
      return self.comfilename

   def readSkyFiles(self, image):
      print "Reading Sky Files..."
      f = open(self.logfile,'ab')
      f.write("Reading Sky Files...\n")
      f.close()
      xcenters = []
      xboxsize = []
      newflist = []
      temp = pyfits.open(image)
      self.xsize = temp[0].data.shape[1]
      self.ysize = temp[0].data.shape[0]
      x = sum(temp[0].data, 0)
      z = smooth1d(x,7,1)
      med = arraymedian(z[where(z != 0)])
      sig = z[where(z != 0)].stddev()
      xcenters = []
      z[0:15] = 0.
      z[-15:] = 0.
      while max(z) > med+self.params['SKY_SIGMA']*sig:
	b = (where(z == max(z)))[0][0]
        if (max(z) <= 0): break
	if (z[b] > med+self.params['SKY_SIGMA']*sig):
	   valid = True
	   for l in range(b-5,b+6):
             if (z[l] == 0): valid = False
	   if valid: xcenters.append(b)
	   z[b-15:b+15] = 0
	   med = arraymedian(z[where(z != 0)])
	   sig = z[where(z != 0)].stddev()
      ys = []
      if (self.params['SKY_TWO_PASS_DETECTION'].lower() == 'yes'):
	for k in range(100,self.ysize-99,(self.ysize-200)/4):
	   ys.append(k)
      for k in range(len(ys)-1):
	az = z[ys[k]:ys[k+1]]
	az[0:15] = 0.
	az[-15:] = 0.
	med = arraymedian(az[where(az != 0)])
        if len(where(az != 0)[0]) != 0:
	   sig = az[where(az != 0)].stddev()
	else: sig=10000.
	firstPass = True
        while max(az) > med+2.5*sig or firstPass:
	   if (max(az) <= 0): break
	   b = (where(az == max(az)))[0][0]
           if (az[b] > med+2.5*sig or firstPass):
             valid = True
             for l in range(b-5,b+6):
                if (az[l] == 0): valid = False
	     #print b+ys[k], len(where(az != 0)[0]), (az[b]-med)/sig
	     firstPass = False
             if valid:
		xcenters.append(b+ys[k])
             az[b-15:b+15] = 0
	     if (len(where(az != 0)[0]) == 0):
		break
             med = arraymedian(az[where(az != 0)])
             #sig = az[where(az != 0)].stddev()
             if len(where(az != 0)[0]) != 0:
		sig = az[where(az != 0)].stddev()
             else: sig=10000.
      for l in range(len(xcenters)):
	xboxsize.append(self.params['SKY_BOX_SIZE'])
      temp.close()
      #print xcenters
      xcenters = array(xcenters)
      xboxsize = array(xboxsize)
      self.xcen = xcenters
      self.xbox = xboxsize
      self.image = image
      f = open(self.params['SKY_LIST_OUTPUT'],'wb')
      f.write(self.image + '\n')
      for j in range(len(self.xcen)):
	f.write(str(self.xcen[j])+'\t'+str(self.xbox[j])+'\n')
      f.close()

   def doSkyRectify(self):
      print "Performing rectification of sky..."
      f = open(self.logfile,'ab')
      f.write("Performing rectification of sky...\n")
      f.close()
      if (self.params['SKY_LIST_INPUT'] != ''):
	f = open(self.params['SKY_LIST_INPUT'],'rb')
	temp = f.read().split('\n')
	f.close()
	try: temp.remove('')
        except ValueError:
           dummy = ''
	self.image = temp[0]
	self.xcen = []
	self.xbox = []
	for j in range(1,len(temp)):
	   t = temp[j].split()
	   self.xcen.append(float(t[0]))
	   self.xbox.append(float(t[1]))
	temp = pyfits.open(self.image)
        self.xsize = temp[0].data.shape[1]
        self.ysize = temp[0].data.shape[0]
	temp.close()
      print "Using "+str(len(self.xcen))+" lines..."
      f = open(self.logfile,'ab')
      f.write("Using "+str(len(self.xcen))+" lines...\n")
      f.close()
      temp = pyfits.open(self.image)
      bcen = int(self.xcen[0])
      bline = sum(temp[0].data[:,bcen-2:bcen+3],1)+0.
      blmed = arraymedian(bline)
      blsig = bline.stddev()
      isylo = False
      currY = self.ysize/2
      ylo = 0
      yhi = len(bline)-1
      while (not isylo and currY > 5):
	if (alltrue(bline[currY-4:currY+1] < blmed-2*blsig)):
	   ylo = currY
	   isylo = True
	currY-=1
      isyhi = False
      currY = self.ysize/2
      while (not isyhi and currY < self.ysize-5):
        if (alltrue(bline[currY:currY+5] < blmed-2*blsig)):
           yhi = currY
           isyhi = True
	currY+=1
      #print ylo,yhi
      ys = [self.ysize/2]
      for j in range(self.ysize/2-10,ylo,-10):
	ys.append(j)
      for j in range(self.ysize/2+10,yhi,10):
	ys.append(j)
      xin = zeros((len(ys), len(self.xcen)),"Float64")
      yin = zeros((len(ys), len(self.xcen)),"Float64")
      xout = zeros((len(ys), len(self.xcen)),"Float64")
      for j in range(len(self.xcen)):
        if (guiMessages):
           print "PROGRESS: "+str(int(60+(j+0.)/len(self.xcen)*35))
	currX = self.xcen[j]
	for l in range(len(ys)):
	   if (ys[l] == ys[0]+10): currX = self.xcen[j]
	   xlo = int(currX - self.xbox[j])
	   xhi = int(currX + self.xbox[j])
	   x = sum(temp[0].data[ys[l]-2:ys[l]+3,:], 0)+0.
	   y = arange(len(x))+0.
	   p = zeros(4)+0.
	   p[0] = max(x[xlo:xhi])
	   p[1] = (where(x[xlo:xhi] == p[0]))[0][0]+xlo
	   p[2] = self.xbox[j]/2
	   p[3] = arraymedian(x)
           p = Numeric.array(p.tolist());
	   lsq = leastsq(self.gaussResiduals, p, args=(y[xlo:xhi], x[xlo:xhi]))
	   if (l == 0):
	      currX = lsq[0][1]
	      yin[0][j] = ys[0]
	      xin[0][j] = lsq[0][1]
	      xout[0][j] = xin[0][j]
	   else:
	      if (abs(lsq[0][1] - currX) < 2):
		currX = lsq[0][1]
	        yin[l][j] = ys[l]
	        xin[l][j] = currX
	        xout[l][j] = xin[0][j]
	      elif (ys[l] == ys[0]+10 and abs(lsq[0][1] - xin[0][j]) < 2):
                currX = lsq[0][1]
                yin[l][j] = ys[l]
                xin[l][j] = currX
                xout[l][j] = xin[0][j]
	temp.close()
      order = self.params['SKY_FIT_ORDER']
      terms = 0
      for j in range(order+2):
	terms+=j
      p = zeros(terms)
      p[1] = 1
      xin = array(xin)
      yin = array(yin)
      xout = array(xout)
      xin = reshape(xin, xin.nelements())+0.
      yin = reshape(yin, yin.nelements())+0.
      xout = reshape(xout, xout.nelements())+0.
      center = float(self.ysize-1)*0.5
      b = where(xin != 0)
      xout = array(xout)-center
      xin = array(xin)-center
      yin = array(yin)-center
      p = Numeric.array(p.tolist());
      lsq = leastsq(self.surfaceResiduals, p, args=(xin[b], yin[b], xout[b], order))
      print lsq[0]
      f = open(self.logfile,'ab')
      f.write("Fit Coeffs: "+str(lsq[0])+"\n")
      f.close()
      xfit = xin*0.
      n = 0
      for j in range(order+1):
	for l in range(j+1):
	   xfit[b]+=lsq[0][n]*xin[b]**(j-l)*yin[b]**l
	   n+=1
      tempmean = (xout[b]-xfit[b]).mean()
      tempstddev = (xout[b]-xfit[b]).stddev()
      print "Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)
      f = open(self.logfile,'ab')
      f.write("Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)+"\n")
      f.close()
      bad = where(abs(xout[b]-xfit[b]-tempmean)/tempstddev > 2)
      if (len(bad[0]) > 0):
        good = where(abs(xout[b]-xfit[b]-tempmean)/tempstddev <=2)
        p = zeros(terms)
        p[2] = 1
        p = Numeric.array(p.tolist());
        lsq = leastsq(self.surfaceResiduals, p, args=(xin[b][good], yin[b][good], xout[b][good], order))
        print 'REDO: ',lsq[0]
        f = open(self.logfile,'ab')
        f.write("Throwing away outliers and refitting...\n")
        f.write("New fit: "+str(lsq[0])+'\n')
        f.close()
        xfit = xin*0.
        n = 0
        for j in range(order+1):
           for l in range(j+1):
              xfit[b]+=lsq[0][n]*xin[b]**(j-l)*yin[b]**l
              n+=1
        tempmean = (xout[b][good]-xfit[b][good]).mean()
        tempstddev = (xout[b][good]-xfit[b][good]).stddev()
        print "Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)
        f = open(self.logfile,'ab')
        f.write("Data - fit    mean: "+str(tempmean) + "   sigma: "+str(tempstddev)+"\n")
        f.close()
      return lsq[0]

   def outputFit(self, speclsq, skylsq):
      order = max(self.params['FIT_ORDER'], self.params['SKY_FIT_ORDER'])
      terms = 0
      for j in range(order+2):
        terms+=j
      f = open(self.params['FIT_OUTPUT'], 'wb')
      f.write('poly ' + str(order) + '\n')
      n = 0
      for j in skylsq:
        f.write(str(j)+'\n')
	n+=1
      for j in range(n,terms):
	f.write('0\n')
      f.write('\n')
      n = 0
      for j in speclsq:
        f.write(str(j)+'\n')
        n+=1
      for j in range(n,terms):
        f.write('0\n')
      f.close()

#Main
if (len(sys.argv) > 1): infile = sys.argv[1]
else: infile = 'rectparams.dat'
if (len(sys.argv) > 2): logfile = sys.argv[2]
else: logfile = 'rectpipeline.log'
if (len(sys.argv) > 3):
   if (sys.argv[3] == "-gui"): guiMessages = True
   else: guiMessages = False
else: guiMessages = False
a = rectify(infile, logfile, guiMessages)
if (guiMessages):
   print "PROGRESS: 100"
   print "STATUS: Done!"
f = open(a.logfile,'ab')
f.write('End time: '+str(datetime.today())+'\n')
f.close()
