#!/usr/bin/python -u
import math, pyfits, sys, os
from numarray import *
import numarray.strings
from UFPipelineOps import *
from pyraf import iraf
import glob
from datetime import datetime

class f2astrometry:

   #Members:
   #list filenames
   #dict params

   def __init__(self, infile, logfile, warnfile, guiMessages):
      self.guiMessages = guiMessages
      if (self.guiMessages):
        print "STATUS: Initialization..."
        print "PROGRESS: 0"
      f = open(infile, "rb")
      self.params = f.read()
      f.close()
      #Setup log, warning files
      self.logfile = logfile
      self.warnfile = warnfile
      f = open(self.logfile,'wb')
      f.write('LOG of '+infile+' at ' +  str(datetime.today()) + '\n')
      f.write('Run at path: '+os.getcwd()+'\n')
      f.write('--------Parameter List--------\n')
      f.write(self.params)
      f.write('------------------------------\n')
      f.close()
      self.params = self.params.split("\n")
      try: self.params.remove('')
      except ValueError:
        dummy = ''
      self.params = dict([(j[0:j.find("=")].strip().upper(), j[j.find("=")+1:].strip()) for j in self.params])
      self.setDefaultParams()
      f = open(self.warnfile,'wb')
      f.write('Warnings and errors for '+infile+' at '+ str(datetime.today())+'\n')
      f.close()
      #Create Filelist
      filelist = self.params['FILES']
      if (os.path.isdir(filelist)):
        self.filenames = os.listdir(filelist)
      elif (os.path.isfile(filelist)):
        f = open(filelist, "rb")
        self.filenames = f.read()
        f.close()
        self.filenames = self.filenames.split()
      elif (filelist.count(',') != 0):
        self.filenames = filelist.split(',')
        try: self.filenames.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(self.filenames)):
           self.filenames[j] = self.filenames[j].strip()
      else:
        self.filenames = glob.glob(filelist)
      print "Found ", len(self.filenames), " files..."
      f = open(self.logfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      f = open(self.warnfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      if (not os.access("final", os.F_OK)): os.mkdir("final",0755)
      for j in range(len(self.filenames)):
	temp = 'final'+self.filenames[j][self.filenames[j].rfind('/'):]
        if (os.access(temp, os.F_OK) and self.params['OVERWRITE_FILES'].lower()== 'yes'): os.unlink(temp)
	if (not os.access(temp, os.F_OK)):
           iraf.images.imutil.imcopy(self.filenames[j],temp)
        self.filenames[j] = temp

   def setDefaultParams(self):
      #Set Default Parameters
      self.params.setdefault('FILES','files.dat')
      self.params.setdefault('OVERWRITE_FILES','no')
      self.params.setdefault('RA_KEYWORD','RA')
      self.params.setdefault('DEC_KEYWORD','DEC')
      self.params.setdefault('FILTER_KEYWORD','FILTER')
      self.params.setdefault('PIXSCALE_KEYWORD','PIXSCALE')
      self.params.setdefault('PIXSCALE_UNITS','arcsec')
      self.params.setdefault('ROT_PA_KEYWORD','ROT_PA')
      self.params.setdefault('SEXTRACTOR_PATH','sex')
      self.params.setdefault('SEXTRACTOR_CONFIG_PATH', 'sextractor-2.3.2/config')
      self.params.setdefault('SEXTRACTOR_CONFIG_PREFIX','default')
      self.params.setdefault('CATALOG','auto')
      self.params.setdefault('RADIUS','15')
      self.params['RADIUS'] = float(self.params['RADIUS'])
      self.params.setdefault('N_OBJECTS_IMAGE','100')
      self.params.setdefault('N_OBJECTS_CATALOG','100')
      self.params['N_OBJECTS_IMAGE'] = int(self.params['N_OBJECTS_IMAGE'])
      self.params['N_OBJECTS_CATALOG'] = int(self.params['N_OBJECTS_CATALOG'])
      self.params.setdefault('XYXYMATCH_TOLERANCE','3.0')
      self.params['XYXYMATCH_TOLERANCE'] = float(self.params['XYXYMATCH_TOLERANCE'])
      self.params.setdefault('XYXYMATCH_NMATCH','30')
      self.params['XYXYMATCH_NMATCH'] = float(self.params['XYXYMATCH_NMATCH'])
      self.params.setdefault('UPDATE_RA_DEC','yes')
      self.params.setdefault('DO_PLATE_SOLUTION','yes')
      self.params.setdefault('TWO_PASS_SOLUTION','yes')
      self.params.setdefault('MATCHING_TYPE','tolerance')
      self.params.setdefault('CCXYMATCH_TOLERANCE','2.0')
      self.params['CCXYMATCH_TOLERANCE'] = float(self.params['CCXYMATCH_TOLERANCE'])
      self.params.setdefault('DEFAULT_PLATE_SOLUTION','')


   def doAstrometry(self, infile):
      if (self.guiMessages):
	print "PROGRESS: 10"
      guiCount = -1.0
      for j in self.filenames:
	guiCount+=1.
        if (self.guiMessages):
	   print "STATUS: Processing file "+str(int(guiCount+1))+"/"+str(len(self.filenames))+"..."
	   print "PROGRESS: "+str(int(10+guiCount/len(self.filenames)*90))
	f = open(self.logfile,'ab')
	f.write('Processing Object '+j+'...\n')
	f.close()
	sxtCat = j[:j.rfind('.')] + '_SXT.dat'
	wgetCat = j[:j.rfind('.')] + '_WGET.dat'
	xyCat = j[:j.rfind('.')] + '_XYXY.dat'
	err = self.findObjects(j,sxtCat)
	if (err == -1): continue
        try:
           temp = pyfits.open(j)
        except Exception:
           print 'Error: file '+j+' not found!  Ignoring file.'
           f = open(self.logfile,'ab')
           f.write('Error: file '+j+' not found!  Ignoring file.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: file '+j+' not found!  Ignoring file.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	imageDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile)
        if (type(temp[0].header[self.params['DEC_KEYWORD']]) == type(0.)):
	   imageDec *= 15
	imageRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile)*15
	filter = temp[0].header[self.params['FILTER_KEYWORD']]
        pixscale = float(temp[0].header[self.params['PIXSCALE_KEYWORD']])
        if (self.params['PIXSCALE_UNITS'].lower() == "degrees"):
           pixscale*=3600.
	theta=float(temp[0].header[self.params['ROT_PA_KEYWORD']])*math.pi/180
	xcen = temp[0].data.shape[1]/2
        ycen = temp[0].data.shape[0]/2
	temp.close()
        catType = self.queryCat(j, imageRA, imageDec, wgetCat, filter)
	if (catType == '-1'): continue
        self.convertCatXY(imageRA, imageDec, pixscale, theta, xcen, ycen)
        err = self.crossCorrelate(j, xyCat, xcen, ycen)
	if (err == -1): continue
	if (self.params['UPDATE_RA_DEC'].lower() == 'yes'):
	   self.updateRADec(j, imageRA, imageDec, catType)
        if (self.params['DO_PLATE_SOLUTION'].lower() == 'yes'):
	   platesol = self.doPlateSolution(j, theta)
	   if (platesol == '-1'):
	      continue
        else:
	   platesol = self.params['DEFAULT_PLATE_SOLUTION']
	   if (platesol == ''):
	      print "No default plate solution given."
      	      f = open(self.logfile,'ab')
      	      f.write('No default plate solution given.\n')
      	      f.close()
              if (guiMessages):
                print "INPUT: File: Enter a filename for a plate solution:"
	      platesol = raw_input("Enter a filename for a plate solution: ")
	      if (guiMessages): print platesol
	if (os.access(platesol, os.F_OK)):
	   self.applyPlateSolution(j, platesol)
	else:
	   print "File not found.  Plate solution not applied."
           f = open(self.logfile,'ab')
           f.write('File not found.  Plate solution not applied.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: Plate solution not found for file '+j+'.  Plate not applied.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	if (os.access(sxtCat,os.F_OK)): os.unlink(sxtCat)
        #if (os.access(wgetCat,os.F_OK)): os.unlink(wgetCat)
        if (os.access(xyCat,os.F_OK)): os.unlink(xyCat)
        paramfile = j[:j.rfind('.')] + '.config'
        f = open(infile, "rb")
        f2 = open(paramfile, "wb")
        f2.write(f.read())
        f.close()
        f2.close()

   def findObjects(self, filename, sxtCat):
      print "Using sextractor to find objects in image."
      f = open(self.logfile,'ab')
      f.write("Using sextractor to find objects in image " + filename + '\n')
      f.close()
      if (os.access(sxtCat, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == 'yes'): os.unlink(sxtCat)
      sxtConf = self.params['SEXTRACTOR_CONFIG_PATH']+"/"+self.params['SEXTRACTOR_CONFIG_PREFIX']
      sxtCom = self.params['SEXTRACTOR_PATH']+" " + filename + " -c " + sxtConf + ".sex -CATALOG_NAME " + sxtCat + " -PARAMETERS_NAME " + sxtConf + ".param -FILTER_NAME " + sxtConf + ".conv -STARNNW_NAME " + sxtConf + ".nnw -CHECKIMAGE_TYPE NONE"
      if (guiMessages):
	sxtCom+=" -VERBOSE_TYPE QUIET"
	print "Sextracting "+filename
      if (not os.access(sxtCat, os.F_OK)): os.system(sxtCom)
      if (not os.access(sxtCat, os.F_OK)):
	print 'ERROR: sextractor not successful!  Objects can not be found in the image!'
	print 'Skipping '+filename+' and continuing with next image.' 
	f = open(self.warnfile,'ab')
        f.write("ERROR: sextractor not successful!  Check to see that it is properly\n")
        f.write('\tinstalled.  As a result, objects can not be found in the image\n')
        f.write('\t'+filename+'.  Skipping this image and continuing with the next one.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      f = open(sxtCat, 'rb')
      s = f.read()
      f.close()
      s = s.split('\n')
      xcol = -1
      ycol = -1
      racol = -1
      deccol = -1
      fcol = -1
      ferrcol = -1
      for j in range(len(s)-1, -1, -1):
	if (s[j] == '' or s[j][0] == '#'):
	   temp = s.pop(j)
	   if (temp.find('X_IMAGE') != -1): xcol = j
	   if (temp.find('Y_IMAGE') != -1): ycol = j
	   if (temp.find('ALPHA_J2000') != -1): racol = j
	   if (temp.find('DELTA_J2000') != -1): deccol = j
	   if (temp.find('FLUX_AUTO') != -1): fcol = j
	   if (temp.find('FLUXERR_AUTO') != -1): ferrcol = j
      if (xcol == -1):
	print 'Error: Could not find X_IMAGE in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find X_IMAGE in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find X_IMAGE in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      if (ycol == -1):
	print 'Error: Could not find Y_IMAGE in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find Y_IMAGE in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find Y_IMAGE in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      if (racol == -1):
	print 'Error: Could not find ALPHA_J2000 in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find ALPHA_J2000 in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find ALPHA_J2000 in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      if (deccol == -1):
	print 'Error:Could not find DELTA_J2000 in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find DELTA_J2000 in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find DELTA_J2000 in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      if (fcol == -1):
	print 'Error: Could not find FULX_AUTO in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find FLUX_AUTO in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find FLUX_AUTO in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      if (ferrcol == -1):
	print 'Error: Count not find FLUXERR_AUTO in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: Could not find FLUXERR_AUTO in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not find FLUXERR_AUTO in sextractor catalog.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      self.sxtXs = []
      self.sxtYs = []
      self.sxtRA = []
      self.sxtDec = []
      self.sxtFlux = []
      self.sxtFerr = []
      for j in s:
        self.sxtXs.append(float(j.split()[xcol]))
        self.sxtYs.append(float(j.split()[ycol]))
	self.sxtRA.append(float(j.split()[racol]))
        self.sxtDec.append(float(j.split()[deccol]))
        self.sxtFlux.append(float(j.split()[fcol]))
	self.sxtFerr.append(float(j.split()[ferrcol]))
      self.sxtXs = array(self.sxtXs)
      self.sxtYs = array(self.sxtYs)
      self.sxtRA = array(self.sxtRA)
      self.sxtDec = array(self.sxtDec)
      self.sxtFlux = array(self.sxtFlux)
      self.sxtFerr = array(self.sxtFerr)
      if (len(self.sxtXs) == 0):
        print 'Error: No objects found in sextractor catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: No objects found in sextractor catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: No objects found in sextractor catalog.\n')
	f.write('File: '+filename+'\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      return 0
      
   def queryCat(self, filename, imageRA, imageDec, wgetCat, filter):
      print "Querying catalog."
      f = open(self.logfile,'ab')
      f.write('Querying catalog.\n') 
      f.close()
      if (os.access(wgetCat, os.F_OK) and self.params['OVERWRITE_FILES'].lower()== 'yes'): os.unlink(wgetCat)
      catType = self.params['CATALOG'].lower()
      if (catType == 'auto' or catType == '2mass'):
	wgetCom = 'wget -O ' + wgetCat + ' "http://vizier.cfa.harvard.edu/viz-bin/asu-tsv?-oc.form=dec&-c.r='+str(self.params['RADIUS'])+'&-c.u=arcmin&-c.geom=r&-out.max=9999&-source=II/246&-c.eq=J2000&-c='+str(imageRA)+'+'+str(imageDec)+'"'
        if (not os.access(wgetCat, os.F_OK)): os.system(wgetCom)
	if (not os.access(wgetCat, os.F_OK)):
	   print 'Error: wget failed to query catalog.'
           f = open(self.logfile,'ab')
           f.write('Error: wget failed to query catalog.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: wget failed to query catalog.\n')
	   f.write('\tEnsure that wget is installed on your system and that you are\n')
	   f.write('\tconnected to the internet.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           return '-1'
	f = open(wgetCat, 'rb')
	s = f.read()
	f.close()
	s = s.split('\n')
	for j in range(len(s)-1, -1, -1):
	   if (s[j] == '' or s[j][0] == '#'):
	      s.pop(j)
	for j in range(3): s.pop(0)
	if (catType == 'auto'):
	   if (len(s) < 3):
	      catType = 'usno'
	      os.unlink(wgetCat)
           else:
	      catType = '2mass'
	if (catType == '2mass'):
	   self.catRA = []
	   self.catDec = []
	   self.catMag = []
	   self.catErr = []
	   if (filter == 'J'): fcol = 2
	   elif (filter == 'H'): fcol = 4
	   else: fcol = 6
	   for j in s:
	      self.catRA.append(float(j.split('\t')[0]))
	      self.catDec.append(float(j.split('\t')[1]))
              if (len(j.split('\t')) > fcol):
		if (j.split('\t')[fcol].strip() != ''):
		   self.catMag.append(float(j.split('\t')[fcol]))
                else: self.catMag.append(0.0)
              else: self.catMag.append(0.0)
              if (len(j.split('\t')) > fcol+1):
		if (j.split('\t')[fcol+1].strip() != ''):
		   self.catErr.append(float(j.split('\t')[fcol+1]))
	        else: self.catErr.append(0.0)
              else: self.catErr.append(0.0)
      if (catType == 'usno'):
	wgetCom = 'wget -O ' + wgetCat + ' "http://vizier.cfa.harvard.edu/viz-bin/asu-tsv?-oc.form=dec&-c.r='+str(self.params['RADIUS'])+'&-c.u=arcmin&-c.geom=r&-out.max=9999&-source=USNO-B1.0&-c.eq=J2000&-c='+str(imageRA)+'+'+str(imageDec)+'"'
	if (not os.access(wgetCat, os.F_OK)): os.system(wgetCom)
        if (not os.access(wgetCat, os.F_OK)):
           print 'Error: wget failed to query catalog.'
           f = open(self.logfile,'ab')
           f.write('Error: wget failed to query catalog.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: wget failed to query catalog.\n')
           f.write('\tEnsure that wget is installed on your system and that you are\n')
           f.write('\tconnected to the internet.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           return '-1'
        f = open(wgetCat, 'rb')
        s = f.read()
	f.close()
	s = s.split('\n')
	for j in range(len(s)-1, -1, -1):
	   if (s[j] == '' or s[j][0] == '#'):
	      s.pop(j)
	for j in range(3): s.pop(0)                                             
        self.catRA = []
        self.catDec = []
        self.catMag = []
        for j in s:
	   self.catRA.append(float(j.split('\t')[1]))
           self.catDec.append(float(j.split('\t')[2]))
	   if (len(j.split('\t')) > 13):
	      if (j.split('\t')[13].strip() != ''):
                self.catMag.append(float(j.split('\t')[13]))
	      else: self.catMag.append(0.0)
	   else: self.catMag.append(0.0)
	self.catErr = zeros(len(self.catMag))
      self.catRA = array(self.catRA)
      self.catDec = array(self.catDec)
      self.catMag = array(self.catMag)
      self.catErr = array(self.catErr)
      if (len(self.catRA) == 0):
        print 'Error: No objects found in '+catType+' catalog.'
        f = open(self.logfile,'ab')
        f.write('Error: No objects found in '+catType+' catalog.\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: No objects found in '+catType+' catalog.\n')
        f.write('File: '+filename+'\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
        return '-1'
      print "Using catalog: " + catType
      f = open(self.logfile,'ab')
      f.write("Using catalog: " + catType + "\n")
      f.close()
      ds9Reg = filename[:filename.rfind('.')] + '_cat_ds9.reg'
      f = open(ds9Reg,'wb')
      f.write('# Region file format: DS9 version 3.0\n')
      f.write('# Filename: '+filename+'\n')
      f.write('global color=blue font="helvetica 10 normal" select=1 edit=1 move=1 delete=1 include=1 fixed=0 source\n')
      for j in range(len(self.catRA)):
        s = 'fk5;point('+str(self.catRA[j])+','+str(self.catDec[j])+') # point=circle text={'+str(self.catMag[j])+'}\n'
        f.write(s)
      f.close()
      return catType

   def convertCatXY(self, imageRA, imageDec, pixscale, theta, xcen, ycen):
      print "Converting catalog RA and Dec into X and Y."
      f = open(self.logfile,'ab')
      f.write("Converting catalog RA and Dec into X and Y.\n")
      f.close()
      deltaDec=3600*(self.catDec-imageDec)/pixscale
      deltaRA=3600*(self.catRA-imageRA)*math.cos(imageDec*math.pi/180)/pixscale
      self.catXs=xcen - deltaDec*math.sin(theta)+deltaRA*math.cos(theta)
      self.catYs=ycen + deltaDec*math.cos(theta)+deltaRA*math.sin(theta)
      b = where(self.catXs > 0, 1, 0)*where(self.catXs < xcen*2-1, 1, 0)*where(self.catYs > 0, 1, 0)*where(self.catYs < ycen*2-1, 1, 0)
      self.catXs = compress(b, self.catXs)
      self.catYs = compress(b, self.catYs)
      self.catRA = compress(b, self.catRA)
      self.catDec = compress(b, self.catDec)
      self.catMag = compress(b, self.catMag)
      self.catErr = compress(b, self.catErr)

   def crossCorrelate(self, filename, xyCat, xcen, ycen):
      print "Using xyxymatch to cross correlate image and catalog."
      f = open(self.logfile,'ab')
      f.write("Using xyxymatch to cross correlate image and catalog.\n")
      f.close()
      self.starCat = filename[:filename.rfind('.')] + '_STARS.dat'
      ds9Reg = filename[:filename.rfind('.')] + '_match_ds9.reg'
      b = (self.sxtFlux.argsort()[::-1])
      self.sxtFlux = self.sxtFlux[b]
      self.sxtFerr = self.sxtFerr[b]
      self.sxtRA = self.sxtRA[b]
      self.sxtDec = self.sxtDec[b]
      self.sxtXs = self.sxtXs[b]
      self.sxtYs = self.sxtYs[b]
      b = (self.catMag.argsort())
      self.catMag = self.catMag[b]
      self.catRA = self.catRA[b]
      self.catDec = self.catDec[b]
      self.catXs = self.catXs[b]
      self.catYs = self.catYs[b]
      self.catErr = self.catErr[b]
      f = open('sxtcoords.dat','wb')
      for j in range(min(len(self.sxtXs), self.params['N_OBJECTS_IMAGE'])):
	s = str(self.sxtXs[j])+'\t'+str(self.sxtYs[j])+'\n'
	f.write(s)
      f.close()
      f = open('catcoords.dat','wb')
      for j in range(min(len(self.catXs), self.params['N_OBJECTS_CATALOG'])):
	s = str(self.catXs[j])+'\t'+str(self.catYs[j])+'\n'
	f.write(s)
      f.close()
      #Use IRAF's xyxymatch to cross-correlate coordinates
      iraf.unlearn('xyxymatch')
      if (os.access(xyCat, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == 'yes'): os.unlink(xyCat)
      if (not os.access(xyCat, os.F_OK)):
	iraf.images.immatch.xyxymatch('sxtcoords.dat','catcoords.dat',xyCat,self.params['XYXYMATCH_TOLERANCE'],xrotation=180,yrotation=180,xref=xcen*2,yref=ycen*2, matching='triangles',nmatch=self.params['XYXYMATCH_NMATCH'])
      if (os.access('sxtcoords.dat',os.F_OK)): os.unlink('sxtcoords.dat')
      if (os.access('catcoords.dat',os.F_OK)): os.unlink('catcoords.dat')
      f = open(xyCat, 'rb')
      s = f.read()
      f.close()
      s = s.split('\n')
      for j in range(len(s)-1, -1, -1):
	if (s[j] == '' or s[j][0] == '#'):
	   s.pop(j)
      if (len(s) < 1):
	print 'Error: Could not cross-correlate objects in image with those from catalog!'
        f = open(self.warnfile,'ab')
        f.write('ERROR: Could not cross-correlate objects in image with those from catalog!\n')
	f.write('Image: '+filename+'\n')
	f.write('xyxymatch may have difficulties with crowded fields.\n')
	f.write('Try varying the parameters N_OBJECTS_IMAGE, N_OBJECTS_CATALOG,\n')
	f.write('\tXYXYMATCH_TOLERANCE, and XYXYMATCH_NMATCH.\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return -1
      isxt = []
      icat = []
      for j in s:
	isxt.append(int(j.split()[5])-1)
        icat.append(int(j.split()[4])-1)
      f = open(self.starCat,'wb')
      f.write('# Star X    Star Y      RA (deg)    Dec (deg)   Magnitude   Error       SXT Flux    Error       SXT Mag     Error\n')
      for j in range(len(isxt)):
	tempmag = str(2.5*log10(self.sxtFlux[isxt[j]]))[:10]
	tempmerr = str(math.sqrt((2.5/math.log(10.0)*self.sxtFerr[isxt[j]]/self.sxtFlux[isxt[j]])**2))[:10]
        s = str(self.sxtXs[isxt[j]])+space(12-len(str(self.sxtXs[isxt[j]])))+str(self.sxtYs[isxt[j]])+space(12-len(str(self.sxtYs[isxt[j]])))+str(self.catRA[icat[j]])[0:10]+space(12-len(str(self.catRA[icat[j]])[0:10]))+str(self.catDec[icat[j]])+space(12-len(str(self.catDec[icat[j]])))+str(self.catMag[icat[j]])
	s+=space(12-len(str(self.catMag[icat[j]])))+str(self.catErr[icat[j]])+space(12-len(str(self.catErr[icat[j]])))+str(self.sxtFlux[isxt[j]])+space(12-len(str(self.sxtFlux[isxt[j]])))+str(self.sxtFerr[isxt[j]])+space(12-len(str(self.sxtFerr[isxt[j]])))+tempmag+space(12-len(tempmag))+tempmerr+'\n'
        f.write(s)
      f.close()
      f = open(ds9Reg,'wb')
      f.write('# Region file format: DS9 version 3.0\n')
      f.write('# Filename: '+filename+'\n')
      f.write('global color=green font="helvetica 10 normal" select=1 edit=1 move=1 delete=1 include=1 fixed=0 source\n')
      for j in range(len(isxt)):
	s = 'image;point('+str(self.sxtXs[isxt[j]])+','+str(self.sxtYs[isxt[j]])+') # point=circle text={'+str(self.catMag[icat[j]])+'}\n'
	f.write(s)
      f.close()


   def updateRADec(self, filename, imageRA, imageDec, catType):
      print "Updating RA and Dec in image header."
      f = open(self.logfile,'ab')
      f.write("Updating RA and Dec in image header.\n")
      f.close()
      image = pyfits.open(filename, 'update')
      if (not image[0].header.has_key('RA_ORIG')):
	image[0].header.update('RA_ORIG', image[0].header[self.params['RA_KEYWORD']])
      if (not image[0].header.has_key('DEC_ORIG')):
	image[0].header.update('DEC_ORIG', image[0].header[self.params['DEC_KEYWORD']])
      image[0].header.update('ASTR_CAT', catType)
      finalRA = (imageRA+(arraymedian(self.catRA)-arraymedian(self.sxtRA)))/15.
      finalDec = imageDec+(arraymedian(self.catDec)-arraymedian(self.sxtDec))
      hrs = int(finalRA)
      min = abs(int((finalRA-hrs)*60))
      sec = abs(finalRA*3600-hrs*3600)-min*60
      hrs = str(hrs)
      if (min < 10): min = '0'+str(min)
      else: min = str(min)
      if (sec < 10): sec = '0'+str(sec)
      else: sec = str(sec)
      finalRA = hrs+':'+min+':'+sec
      deg = int(finalDec)
      min = abs(int((finalDec-deg)*60))
      sec = abs(finalDec*3600-deg*3600)-min*60
      deg = str(deg)
      if (min < 10): min = '0'+str(min)
      else: min = str(min)
      if (sec < 10): sec = '0'+str(sec)
      else: sec = str(sec)
      finalDec = deg+':'+min+':'+sec
      image[0].header.update(self.params['RA_KEYWORD'], finalRA)
      image[0].header.update(self.params['DEC_KEYWORD'], finalDec)
      image.verify('silentfix')
      image.flush()
      image.close()

   def doPlateSolution(self, filename, theta):
      print "Calculating plate solution."
      f = open(self.logfile,'ab')
      f.write("Calculating plate solution for " + filename + ".\n")
      f.close()
      ccinput = self.starCat
      ccxcol = 1
      ccycol = 2
      ccracol = 3
      ccdeccol = 4
      platesol = filename[:filename.rfind('.')] + '_CCMAP'
      if (self.params['TWO_PASS_SOLUTION'].lower() == 'yes'):
	print "Using two passes."
        f = open(self.logfile,'ab')
        f.write("Using two passes.\n")
        f.close()
	#Use IRAF's ccmap to find rough plate solution
        iraf.unlearn('ccmap')
        if (os.access('ccrough.db', os.F_OK)): os.unlink('ccrough.db')
        iraf.images.imcoords.ccmap(ccinput,'ccrough.db',solutions='ccrough',xcolumn=ccxcol,ycolumn=ccycol,lngcolumn=ccracol,latcolumn=ccdeccol,lngunits='degrees',latunits='degrees',xxorder=2,xyorder=2,xxterms='half',yxorder=2,yyorder=2,yxterms='half',interactive='no')
	#Read results from ccmap
	f = open('ccrough.db')
        s = f.read()
        f.close()
        s = s.split('\n')
        for j in s:
	   temp = j.split()
	   if (len(temp) < 2): continue
	   if (temp[0] == 'xpixref'): ccxref = float(temp[1])
	   if (temp[0] == 'ypixref'): ccyref = float(temp[1])
	   if (temp[0] == 'xmag'): ccxmag = float(temp[1])
	   if (temp[0] == 'ymag'): ccymag = float(temp[1])
           if (temp[0] == 'xrotation'): ccxrot = -1*float(temp[1])
           if (temp[0] == 'yrotation'): ccyrot = -1*float(temp[1])

	   if (temp[0] == 'lngref'): ccraref = float(temp[1])
	   if (temp[0] == 'latref'): ccdecref = float(temp[1])
        if (os.access('ccrough.db',os.F_OK)): os.unlink('ccrough.db')
	#Write all sextractor and catalog objects to files
        f = open('sxtcoords.dat','wb')
        for j in range(len(self.sxtXs)):
           s = str(self.sxtXs[j])+'\t'+str(self.sxtYs[j])+'\n'
           f.write(s)
        f.close()
        f = open('catcoords.wcs','wb')
        for j in range(len(self.catRA)):
           s = str(self.catRA[j])+'\t'+str(self.catDec[j])+'\n'
           f.write(s)
        f.close()
	#Use IRAF's ccxymatch to match additional stars
	iraf.unlearn('ccxymatch')
	if (os.access('ccxymatch.dat', os.F_OK)): os.unlink('ccxymatch.dat')
	try:
	   iraf.images.imcoords.ccxymatch('sxtcoords.dat','catcoords.wcs','ccxymatch.dat',tolerance=self.params['CCXYMATCH_TOLERANCE'],ptolerance=3.0, xin=ccxref, yin=ccyref, xmag=ccxmag, ymag=ccymag, xrotation=ccxrot, yrotation=ccyrot, lngref=ccraref, latref=ccdecref, lngunits='degrees', latunits='degrees', matching=self.params['MATCHING_TYPE'])
	except Exception:
	   print 'Error: ccxymatch failed.'
           f = open(self.warnfile,'ab')
           f.write('ERROR: ccxymatch failed.  This may be due to erroneous results from\n')
	   f.write('\tccmap.  Also check the parameters MATCHING_TYPE and CCXYMATCH_TOLERANCE.\n')
           f.write('Image: '+filename+'\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           return '-1' 
	ccinput = 'ccxymatch.dat'
	ccxcol = 3
	ccycol = 4
	ccracol = 1
	ccdeccol = 2
	#Write new version of STARS.dat
        f = open('ccxymatch.dat', 'rb')
        s = f.read()
        f.close()
        s = s.split('\n')
        for j in range(len(s)-1, -1, -1):
           if (s[j] == '' or s[j][0] == '#'):
              s.pop(j)
        isxt = []
        icat = []
        for j in s:
           isxt.append(int(j.split()[5])-1)
           icat.append(int(j.split()[4])-1)
        f = open(self.starCat,'wb')
        f.write('# Star X    Star Y      RA (deg)    Dec (deg)   Magnitude   Error       SXT Flux    Error       SXT Mag     Error\n')
        for j in range(len(isxt)):
           tempmag = str(2.5*log10(self.sxtFlux[isxt[j]]))[:10]
           tempmerr = str(math.sqrt((2.5/math.log(10.0)*self.sxtFerr[isxt[j]]/self.sxtFlux[isxt[j]])**2))[:10]
           s = str(self.sxtXs[isxt[j]])+space(12-len(str(self.sxtXs[isxt[j]])))+str(self.sxtYs[isxt[j]])+space(12-len(str(self.sxtYs[isxt[j]])))+str(self.catRA[icat[j]])[0:10]+space(12-len(str(self.catRA[icat[j]])[0:10]))+str(self.catDec[icat[j]])+space(12-len(str(self.catDec[icat[j]])))+str(self.catMag[icat[j]])
           s+=space(12-len(str(self.catMag[icat[j]])))+str(self.catErr[icat[j]])+space(12-len(str(self.catErr[icat[j]])))+str(self.sxtFlux[isxt[j]])+space(12-len(str(self.sxtFlux[isxt[j]])))+str(self.sxtFerr[isxt[j]])+space(12-len(str(self.sxtFerr[isxt[j]])))+tempmag+space(12-len(tempmag))+tempmerr+'\n'
           f.write(s)
        f.close()
      #Use IRAF's ccmap to find rough plate solution
      iraf.unlearn('ccmap')
      if (os.access(platesol+'.db', os.F_OK)): os.unlink(platesol+'.db')
      iraf.images.imcoords.ccmap(ccinput,platesol+'.db',solutions=platesol,xcolumn=ccxcol,ycolumn=ccycol,lngcolumn=ccracol,latcolumn=ccdeccol,lngunits='degrees', latunits='degrees',xxorder=4,xyorder=4,xxterms='full',yxorder=4,yyorder=4,yxterms='full',interactive='no')
      if (os.access('sxtcoords.dat',os.F_OK)): os.unlink('sxtcoords.dat')
      if (os.access('catcoords.wcs',os.F_OK)): os.unlink('catcoords.wcs')
      if (os.access('ccxymatch.dat',os.F_OK)): os.unlink('ccxymatch.dat')
      return platesol+'.db'

   def applyPlateSolution(self, filename, platesol):
      print "Applying plate solution."
      f = open(self.logfile,'ab')
      f.write("Applying plate solution to "+filename+".\n")
      f.close()
      #Use IRAF's ccsetwcs to apply plate solution
      solname = platesol[0:platesol.rfind('.')]
      iraf.unlearn('ccsetwcs')
      iraf.images.imcoords.ccsetwcs(filename, platesol, solname)

#Main
if (len(sys.argv) > 1): infile = sys.argv[1]
else: infile = 'astromparams.dat'
if (len(sys.argv) > 2): logfile = sys.argv[2]
else: logfile = 'astrompipeline.log'
if (len(sys.argv) > 3): warnfile = sys.argv[3]
else: warnfile = 'astrompipeline.warn'
if (len(sys.argv) > 4):
   if (sys.argv[4] == "-gui"): guiMessages = True
   else: guiMessages = False
else: guiMessages = False
a = f2astrometry(infile,logfile,warnfile, guiMessages)
a.doAstrometry(infile)
if (guiMessages):
   print "PROGRESS: 100"
   print "STATUS: Done!"
f = open(a.logfile,'ab')
f.write('End time: '+str(datetime.today())+'\n')
f.close()
