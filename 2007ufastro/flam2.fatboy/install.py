#!/usr/bin/python
import os,shutil,glob
print 'Welcome to the FATBOY Installation program'
print '***Note: It is strongly recommended that your install directory be in'
print '\tyour PATH.  This will enable you to run FATBOY from any directory.'
print '\tOtherwise, you will have to install into each directory you'
print '\tintend to run it in.'
print ''
doInstall = True
path = '/usr/local/bin'
tmp = raw_input('Enter an install directory [default /usr/local/bin]: ')
if (tmp != ''): path = tmp
if (not os.access(path, os.F_OK)):
   print 'The directory '+path+' does not exist.'
   tmp = raw_input('Do you want to create it? (Y/N): ')
   if (tmp.lower() == 'y'):
      try: os.mkdir(path)
      except OSError:
	print 'You do not have permission to write to this directory!'
	doInstall = False
   else:
      doInstall = False
elif (not os.access(path, os.W_OK)):
   print 'You do not have permission to write to this directory!'
   doInstall = False
a = glob.glob('*.py')
if (os.access('fatboy', os.F_OK)):
   a.append('fatboy')
else:
   print 'File: fatboy is missing!  Cannot install.'
   print 'Get the latest tarball from http://www.astro.ufl.edu/fatboy and try again.'
   doInstall = False
if (os.access('FATBOY_GUI.jar', os.F_OK)):
   a.append('FATBOY_GUI.jar')
else:
   print 'File: FATBOY_GUI.jar is missing!  Cannot install.'
   print 'Get the latest tarball from http://www.astro.ufl.edu/fatboy and try again.'
f = open('fatboy','rb')
fb = f.read().split('\n')
f.close()
f = open('fatboy','wb')
try: fb.remove('')
except Exception: dummy=''
for j in range(len(fb)):
   if (not fb[j].startswith('INSTDIR')):
      f.write(fb[j] + '\n')
   else:
      f.write('INSTDIR=' + path + '/fatboy\n')
f.close()
f = open('fatboy.config','wb')
f.write('PATH=' + path + '\n')
f.close()
a.append('fatboy.config')
success = False
if (doInstall):
   success = True
   for j in a:
      print '\tInstalling file '+j+'...'
      try: shutil.copy(j, path)
      except Exception:
	print 'Error copying file '+j+'!'
        success=False
   if (os.access('distortionMaps', os.F_OK)):
      print '\tInstalling distortion maps in '+path+'/distortionMaps'
      if (not os.access(path+'/distortionMaps', os.F_OK)):
	os.mkdir(path+'/distortionMaps')
      dm = glob.glob('distortionMaps/*')
      for j in dm:
	try: shutil.copy(j, path+'/distortionMaps')
	except Exception:
           print 'Error copying file '+j+'!'
           success=False
if (success):
   print 'FATBOY has been successfully installed in '+path+'!'
   print 'To run, type "fatboy" at a command prompt.'
else:
   print 'Installation of FATBOY failed.'
