import java.io.*;
import javax.imageio.stream.*;

public class FATBOYfloats extends FATBOYdata
{
    protected float[] _values=null;

    private void _init() {
	_type = MsgTyp._Floats_;
    }

    public FATBOYfloats() {
	_init();
    }

    public FATBOYfloats(String name, int nelem) {
	_init();
	_name = new String(name);
	_elem = nelem;
    }

    public FATBOYfloats(String name, int nelem, int width, int height) {
        this(name, nelem);
        this.width = width;
        this.height = height;
    }

    public FATBOYfloats(String name, float[] vals) {
	_init();	
	_name = new String(name);
	_elem = vals.length;
	_values = vals;
    }

    public FATBOYfloats(String name, float[] vals, int width, int height) {
        this(name, vals);
        this.width = width;
        this.height = height;
    }

    // all methods declared abstract by UFProtocol can be defined here:

    public String description() { return new String("FATBOYfloats"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 4;
    }

    public double[] values() { return UFArrayOps.castAsDoubles(_values); }

    public float valData(int index) { return _values[index]; }

    public float maxVal() {
	float max=-1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public float minVal() {
	float min=1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	int retval=0;
	try {
	    _values = new float[_elem];
/*
	    for( int elem=0; elem<_elem; elem++ ) {
		_values[elem] = inp.readFloat();
		retval += 4;
	    }
*/
	    MemoryCacheImageInputStream mciis = new MemoryCacheImageInputStream(inp);
	    mciis.readFully( _values, 0, _elem );
	    mciis.close();
            return 4*_elem;
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYfloats::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("FATBOYfloats::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYfloats::recvData> "+e.toString());
	}
	return retval;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	int retval=0;
	try {
	    if (_values != null && _values.length > 0) {
		ByteArrayOutputStream outArray = new ByteArrayOutputStream(_elem*4);
		DataOutputStream outToArray = new DataOutputStream(outArray);
		for( int elem=0; elem<_elem; elem++ ) {
		    outToArray.writeFloat(_values[elem]);
		    retval += 4;
		}
		outArray.writeTo(out); // write all floats as bytes in one chunk
	    }
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYfloats::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("FATBOYfloats::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYfloats::sendData> "+e.toString());
	}
	return retval;
    }

    public void flipFrame()
    {
        int nrow = height;
        int ncol = width;

        for( int irow=0; irow < (nrow/2); irow++ )
            {
                int iflip = nrow - irow - 1;
                int kp = irow * ncol;
                int kf = iflip * ncol;

                for( int k=0; k<ncol; k++ ) {
                    float pixval = _values[kp];
                    _values[kp++] = _values[kf];
                    _values[kf++] = pixval;
                }
            }
    }

}
