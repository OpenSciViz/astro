import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

class FATBOYImagePanel extends JPanel implements KeyListener {

    protected final FATBOYDisplayFrame displayFrame;
    public int width, height;
    protected double oMin, oMax;
    protected double newMin, newMax;
    protected byte scaledPixels[];
    protected Image pixImage = null;
    protected ImageBuffer imageBuffer = null;
    protected ImageBuffer imageBuffer1k = null;
    protected ImageBuffer savedImgBuff = null;
    protected boolean displSrc = true;
    protected IndexColorModel colorModel;
    protected Color transLucent = new Color(128, 255, 128, 33);
    protected Color transLucent2 = new Color(255, 128, 128, 33);
    protected int xRulerInit, xRulerFin;
    protected int yRulerInit, yRulerFin;
    protected int xisav, yisav;
    protected String[] dragObjectStatus = {"Hidden", "Hidden", "Hidden", "Hidden"};
    protected Color[] dragObjectColor = {Color.RED, Color.YELLOW, Color.GREEN, Color.CYAN};
    protected int[] dragObjectX = {100, 150, 200, 250};
    protected int[] dragObjectY = {100, 150, 200, 250};
    protected boolean[] dragObject = {false, false, false, false};
    protected boolean showRuler= false;
    protected int mouseButton = 0;
    protected int xInit = 0, yInit = 0;
    protected float minInit = 0, maxInit = 0;	
    protected boolean showMOS = false;   
    boolean doLinear = true;   
    boolean doLog    = false;	
    boolean doPower  = false;	
    protected int oldWidth = 0;
    protected int oldHeight = 0;
    protected MOSMask[] mosMasks;
    protected boolean drawCoords = false;

    public FATBOYImagePanel(FATBOYDisplayFrame dispFrame, IndexColorModel colorModelPtr)
    {
	this.displayFrame = dispFrame;
        this.colorModel = colorModelPtr;
	imageBuffer = new ImageBuffer();
	width = imageBuffer.width;
	height = imageBuffer.height;
        this.setMinimumSize(new Dimension(width, height));
        this.scaledPixels = new byte[ width * height ];

        
	if(oldWidth != width || oldHeight != height) {
	repaint();
	int oldWidth = width;
	int oldHeight = height;
	}

	this.addKeyListener(this);
	
        addMouseListener(new MouseAdapter() {
	
		public void mousePressed(MouseEvent mev)
		{
		    if( (mev.getModifiers() & InputEvent.BUTTON2_MASK) != 0 ) 
		    {		    
			mouseButton = 2;
			xInit = mev.getX();
			yInit = mev.getY();

			for (int j = 0; j < 4; j++) {
			   if (dragObjectStatus[j].equals("V Line") && Math.abs(xInit-dragObjectX[j]) < 50) {
			      xisav = dragObjectX[j];
			      yisav = 0;
			      for (int l = 0; l < 4; l++) {
				if (l == j) dragObject[l] = true; else dragObject[l] = false;
			      }
			   } else if (dragObjectStatus[j].equals("H Line") && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = 0;
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("+") && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("X") && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
			   } else if (dragObjectStatus[j].equals("Circle")  && Math.abs(xInit-dragObjectX[j]) < 50 && Math.abs(yInit-dragObjectY[j]) < 50) {
                              xisav = dragObjectX[j];
                              yisav = dragObjectY[j];
                              for (int l = 0; l < 4; l++) {
                                if (l == j) dragObject[l] = true; else dragObject[l] = false;
                              }
                           } else dragObject[j] = false; 
			}
	            }
		    if( (mev.getModifiers() & InputEvent.BUTTON1_MASK) != 0 ) 
		    {
			requestFocus();
			mouseButton = 1;
			xInit = mev.getX();
			yInit = mev.getY();
			
		 	if( showRuler ) {
			   xisav = xInit;
			   yisav = yInit;      
			}
	            }  

		    if( (mev.getModifiers() & InputEvent.BUTTON3_MASK) != 0 ) 
		    {
			mouseButton = 3;
			xInit = mev.getX();
			yInit = mev.getY();
			minInit = Float.parseFloat(displayFrame.zMin.getText());
			maxInit = Float.parseFloat(displayFrame.zMax.getText());
	            }  
  
		}
		public void mouseEntered(MouseEvent mev) {
		}
    		public void mouseExited(MouseEvent mev) {}
    		public void mouseReleased(MouseEvent mev) {
		   if (drawCoords) {
			drawCoords = false;
			repaint();
		   }
		}
    		public void mouseClicked(MouseEvent mev) {}
	    });
	    
        addMouseMotionListener(new MouseMotionAdapter() {
		
		public void mouseMoved(MouseEvent mev) {
		    int width = displayFrame.imPanelsHolder.getWidth();
		    int height = displayFrame.imPanelsHolder.getHeight();
		    //double scale = 2048.0/(Math.min(width, height));
		    double scale = Math.max((double)imageBuffer.width/width, (double)imageBuffer.height/height);
		    int xz = mev.getX();
		    int yz = mev.getY();
		    int xLoc = (int)(xz*scale);
		    //int yLoc = (int)(2047-yz*scale);
                    int yLoc = (int)(imageBuffer.height-1-yz*scale);
		    double data = 0;
		    try {
			//if (imageBuffer != null && imageBuffer.pixels != null) data = imageBuffer.pixels[xLoc+2048*(2047-yLoc)];
                        if (imageBuffer != null && imageBuffer.pixels != null) {
			    if (imageBuffer.width == imageBuffer.height) data = imageBuffer.pixels[xLoc+imageBuffer.width*(imageBuffer.height-yLoc-1)]; else data = imageBuffer.pixels[xLoc+imageBuffer.width*(yLoc)];
			}
			displayFrame.pixelDataVal.setText(" x="+xLoc+", y="+yLoc+", Data = "+data);
		    } catch(ArrayIndexOutOfBoundsException e) {
			displayFrame.pixelDataVal.setText(" x=NA, y=NA, Data = NA");
		    }
		}

		public void mouseDragged(MouseEvent mev) 
		{
                        int width = displayFrame.imPanelsHolder.getWidth();
                        int height = displayFrame.imPanelsHolder.getHeight();
			int xz = mev.getX();
			int yz = mev.getY();

			int xOffset = mev.getX()-xInit;
			int yOffset = mev.getY()-yInit;
			int xi = xisav + xOffset;
			int yi = yisav + yOffset;

		    if( mouseButton == 2 ) {
			for (int j = 0; j < 4; j++) {
			    if (dragObject[j]) {
				dragObjectX[j] = xi;
				dragObjectY[j] = yi;
			    }
			}
 		    }
		    else if( mouseButton == 1 ) {
		 
		 	if( showRuler) {
			  xRulerInit = xisav;
			  yRulerInit = yisav;
			  xRulerFin  = xi;
			  yRulerFin  = yi;
			}
		    }
		    else if( mouseButton == 3 ) {
	                String zModeBoxSel = (String)(displayFrame.zModeBox.getSelectedItem());
	                String scaleBoxSel = (String)(displayFrame.scaleBox.getSelectedItem());
	               if (zModeBoxSel.indexOf("Auto") >= 0) { //nothing to do
 	               } else if (zModeBoxSel.indexOf("Manual") >= 0) {
			 //old imagePanels[0].adjustPanel.updateMinMax();
		         int imMin = (int)imageBuffer.s_min;
			 int imMax = (int)imageBuffer.s_max;
                         int cx = xz - xInit;
                         int cy = yz - yInit;
		         if (scaleBoxSel.indexOf("Linear") >= 0) {
                           int min = (int)(minInit-cy*(0.3*(imMax-imMin))/height);
                           int max = (int)(maxInit+cx*(imMax-imMin)/width);
			   displayFrame.zMax.setText(String.valueOf(max));
			   displayFrame.zMin.setText(String.valueOf(min));
			   applyLinearScale(min, max);
			 } else if (scaleBoxSel.indexOf("Log") >= 0) {
	                   int min = (int)(maxInit+cx*(0.1*(imMax-imMin))/width);
			   displayFrame.zMax.setText(String.valueOf(min));
			   applyLogScale(min);
			 } else if (scaleBoxSel.indexOf("Power") >= 0) {
	                   float power = minInit-(float)cy*5/height;
	                   int min = (int)(maxInit+cx*(0.1*(imMax-imMin))/width);
			   displayFrame.zMax.setText(String.valueOf(min));
			   String powerString = String.valueOf(power);
			   if (powerString.length() > 5)
			      powerString = powerString.substring(0,5);
			   displayFrame.zMin.setText(powerString);
			   applyPowerScale(min, power);
			 }
                       }
		    }
		    repaint();
		}
	    });      
    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void updateImage(ImageBuffer imgBuff)
    {
        if( imgBuff == null ) {
	    imageBuffer1k = null;
	    pixImage = null;
	}
	else {
	    scaledPixels = new byte[ imgBuff.width * imgBuff.height ];

	    savedImgBuff = imgBuff;
	    
            String zModeBoxSel = (String)(displayFrame.zModeBox.getSelectedItem());
            float min = Float.parseFloat(displayFrame.zMin.getText());
            float max = Float.parseFloat(displayFrame.zMax.getText());
	    
	    int width = displayFrame.imPanelsHolder.getWidth();
	    int height = displayFrame.imPanelsHolder.getHeight();
	    int size = Math.min(width, height);
	    
	    if (displSrc) 
	    {
                imageBuffer        = imgBuff;
		imageBuffer1k = imageBuffer.rescale(size);

	        oMax = imageBuffer1k.max;
	        oMin = imageBuffer1k.min;
		
	    } 
	}
    }
    
    public synchronized void updateImage( IndexColorModel icm )
    {
        this.colorModel = icm;
        
        if( imageBuffer1k == null )
	    pixImage = null;
        else
            pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        
	String zModeBoxSel = (String)(displayFrame.zModeBox.getSelectedItem());
	float min = Float.parseFloat(displayFrame.zMin.getText());
	float max = Float.parseFloat(displayFrame.zMax.getText());
	
        repaint();
    }
    
//------------------------------------------------------------------------------------------

    public synchronized void applyLinearScale() { applyLinearScale( oMin, oMax ); }

    public synchronized void applyLinearScale(double minv, double maxv)
    {
        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

        newMin = minv;
        newMax = maxv;
        double range = maxv - minv;
	scaledPixels = new byte[imageBuffer1k.pixels.length];
        
        for(int i=0; i < imageBuffer1k.pixels.length; i++)
	    {
		double f = ( imageBuffer1k.pixels[i] - minv )/range;

		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }
	String s = String.valueOf(maxv);
	int epos = s.indexOf("E");
	if (epos != -1) {
	   s = s.substring(0,Math.min(5,epos))+s.substring(epos);
	}
        displayFrame.zMax.setText(s);
        s = String.valueOf(minv);
        epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
        displayFrame.zMin.setText(s);
	pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height,	colorModel, scaledPixels, 0, imageBuffer1k.width));

        repaint();

    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void applyLogScale(double threshold) {

        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
	double maxv = Math.log( (double)oMax );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer1k.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer1k.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.log( pval ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();
    }
//-------------------------------------------------------------------------------------------------------
    
    public synchronized void applyPowerScale(double threshold, double power) {

        if( imageBuffer1k == null ) {
	    pixImage = null;
	    repaint();
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
	if( power <= 0 ) power = 0.5;
	if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
	double maxv = Math.pow( (double)oMax, power );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer1k.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer1k.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.pow( pval, power ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();
    }
//-------------------------------------------------------------------------------------------------------

    public synchronized void applyZScale() {
	this.applyZScale("Zscale");
    }

    public synchronized void applyZScale(String zMode)
    {
        if( imageBuffer1k == null ) {
            pixImage = null;
            repaint();
            return;
        }
	imageBuffer.Zscale();
	double minv, maxv;
	if (zMode.equals("Zmax")) {
	    minv = imageBuffer.ZscaleRange[0];
	    maxv = oMax;
	} else if (zMode.equals("Zmin")) {
	    minv = oMin;
	    maxv = imageBuffer.ZscaleRange[1];
	} else {
	    minv = imageBuffer.ZscaleRange[0];
            maxv = imageBuffer.ZscaleRange[1];
	}

	newMin = minv;
	newMax = maxv;
        double range = maxv - minv;
        for(int i=0; i < imageBuffer1k.pixels.length; i++)
            {
                double f = ( imageBuffer1k.pixels[i] - minv )/range;

                if( f <= 0 )
                    scaledPixels[i] = 0;
                else if( f >= 1 )
                    scaledPixels[i] = (byte)255;
                else
                    scaledPixels[i] = (byte)Math.round(f*255);
            }


        String s = String.valueOf(newMax);
        int epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
	else if (s.length() > 6 && s.indexOf(".") != -1) {
	   s = s.substring(0,s.indexOf(".")+2);
	}
        displayFrame.zMax.setText(s);
        s = String.valueOf(newMin);
        epos = s.indexOf("E");
        if (epos != -1) {
           s = s.substring(0,Math.min(5,epos))+s.substring(epos);
        }
        else if (s.length() > 6 && s.indexOf(".") != -1) {
           s = s.substring(0,s.indexOf(".")+2);
        }
        displayFrame.zMin.setText(s);
        pixImage = createImage(new MemoryImageSource(imageBuffer1k.width, imageBuffer1k.height, colorModel, scaledPixels, 0, imageBuffer1k.width));
        repaint();

    }

//-------------------------------------------------------------------------------------------------------

    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        if( pixImage == null ) {
	    g.setColor( Color.lightGray );
	    g.fillRect( 0, 0, width, height );
	    g.setColor( Color.BLACK );
            g.drawString("Buffer  is  Empty", 20, height/2);
        }
	else {
            g.drawImage( pixImage, 0, 0, null );
	    g.setColor( Color.GREEN );
	    g.drawLine( 20, 20, 20, 100);
 	    g.drawLine( 20, 20, 100, 20);
	    g.drawString("S", 15, 120);
	    g.drawString("W", 115, 25);

	    g.drawLine( 100, 20, 90, 10);
	    g.drawLine( 100, 20, 90, 30);
	    g.drawLine( 20, 100, 10, 90);
	    g.drawLine( 20, 100, 30, 90);

	    for (int j = 0; j < 4; j++) {
                g.setColor(dragObjectColor[j]);
		if (dragObjectStatus[j].equals("V Line")) {
		   g.drawLine(dragObjectX[j], 0, dragObjectX[j], height);
		} else if (dragObjectStatus[j].equals("H Line")) {
		   g.drawLine(0, dragObjectY[j], width, dragObjectY[j]);
		} else if (dragObjectStatus[j].equals("+")) {
		    int xc = dragObjectX[j];
		    int yc = dragObjectY[j];
		    g.drawLine(xc,yc-20,xc,yc+20);
		    g.drawLine(xc-20,yc,xc+20,yc);
		    g.setColor(Color.BLACK);
		    g.drawLine(xc-1,yc-20,xc-1,yc+20);
		    g.drawLine(xc+1,yc-20,xc+1,yc+20);
		    g.drawLine(xc-20,yc-1,xc+20,yc-1);
		    g.drawLine(xc-20,yc+1,xc+20,yc+1);
		    g.setColor(dragObjectColor[j]);
		} else if (dragObjectStatus[j].equals("X")) {
                    int xc = dragObjectX[j];
                    int yc = dragObjectY[j];
                    g.drawLine(xc-20,yc-20,xc+20,yc+20);
                    g.drawLine(xc+20,yc-20,xc-20,yc+20);
                    g.setColor(Color.BLACK);
                    g.drawLine(xc-21,yc-20,xc+19,yc+20);
                    g.drawLine(xc-19,yc-20,xc+21,yc+20);
                    g.drawLine(xc+21,yc-20,xc-19,yc+20);
                    g.drawLine(xc+19,yc-20,xc-21,yc+20);
                    g.setColor(dragObjectColor[j]);
		} else if (dragObjectStatus[j].equals("Circle")) {
		    g.drawOval(dragObjectX[j]-10, dragObjectY[j]-10, 20, 20);
                    g.setColor(Color.BLACK);
                    g.drawOval(dragObjectX[j]-11, dragObjectY[j]-11, 22, 22);
                    g.drawOval(dragObjectX[j]-9, dragObjectY[j]-9, 18, 18);
                    g.setColor(dragObjectColor[j]);
		}
	    }
	    if( showRuler ) {
	        int width = displayFrame.imPanelsHolder.getWidth();
	        int height = displayFrame.imPanelsHolder.getHeight();
                g.setColor( transLucent2 );
		int x1 = xRulerInit;
		int y1 = yRulerInit;
		int x2 = xRulerFin;
		int y2 = yRulerInit;
		int x3 = xRulerFin;
		int y3 = yRulerFin;
		
		String xEW = new String();
		String yNS = new String();		
		double xShift = (368.64/height)*(x1-x3); // 0.18 arcsec/pix
		double yShift = (368.64/width)*(y1-y3); // 0.18 arcsec/pix
		float xpxShift = (int)(10*((float)imageBuffer.width/width)*(x1-x3))/10; //pixel shift
                float ypxShift = (int)(10*((float)imageBuffer.height/height)*(y1-y3))/10; //pixel shift
		if ( xShift < 0 ) xEW = "W"; 
		   else xEW = "E";
		if ( yShift < 0 ) yNS = "S"; 
		   else yNS = "N";
		   
		float xShiftLabel = (int)(10*Math.abs(xShift))/10;
		float yShiftLabel = (int)(10*Math.abs(yShift))/10;
		
		String ylabel = new String(" (" + ypxShift+" px; "+ yShiftLabel + "'' " + yNS + ","); 
		String xlabel = new String(" " + xpxShift+" px; "+ xShiftLabel + "'' " + xEW + ")" );
		int[] xPoints = {x1, x2, x3};
		int[] yPoints = {y1, y2, y3};
		g.fillPolygon( xPoints, yPoints, 3 );
                g.setColor( Color.WHITE );
		g.drawPolygon( xPoints, yPoints, 3 );
		g.drawString(ylabel, x3, y3);
		g.drawString(xlabel, x3, y3+12);
	    }
	    if( showMOS && mosMasks != null) {
	        g.setColor( Color.ORANGE );
		for (int i=0; i < mosMasks.length; i++) {
		   if (mosMasks[i] != null) {
		      mosMasks[i].setScale((float)imageBuffer1k.width/this.width,(float)imageBuffer1k.height/this.height, (float)this.height);
		      mosMasks[i].drawMOSMask(g);
		   }
		}
	    }
        }
    }

    public void keyPressed(KeyEvent kev) {
        moveMouse(kev);
    }
    public void keyTyped(KeyEvent kev) { }
    public void keyReleased(KeyEvent kev) {}

    private void moveMouse(KeyEvent kev) {
        int xMove = 0, yMove = 0;
        switch (kev.getKeyCode()) {
            case KeyEvent.VK_LEFT:  xMove = -1; break;
            case KeyEvent.VK_RIGHT: xMove = 1; break;
            case KeyEvent.VK_UP:    yMove = -1; break;
            case KeyEvent.VK_DOWN:  yMove = 1; break;
            case KeyEvent.VK_KP_LEFT:  xMove = -1; break;
            case KeyEvent.VK_KP_RIGHT: xMove = 1; break;
            case KeyEvent.VK_KP_UP:    yMove = -1; break;
            case KeyEvent.VK_KP_DOWN:  yMove = 1; break;
            default: return;
        }
        try {
           Robot r = new Robot();
           Point p = MouseInfo.getPointerInfo().getLocation();
           r.mouseMove(p.x+xMove, p.y+yMove);
        } catch(Exception e) {}
    }

    public void setZoomBoxColor(int i) {
	if (i == 1) transLucent = new Color(128,255,128,16);
	else if (i == 2) transLucent = new Color(128,255,128,0);
	else transLucent = new Color(128,255,128,33);
	repaint();
    }

    public int getZoomBoxColor() {
	if (transLucent == new Color(128,255,128,16)) return 1;
	else if (transLucent == new Color(128,255,128,0)) return 2;
	else return 0;
    }
}
