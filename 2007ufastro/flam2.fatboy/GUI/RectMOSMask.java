/**
 * Title:        RectMOSMask.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner
 * Company:      University of Florida
 * Description:  Creates Masks to overlay on top of the data in jdd
 */

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class RectMOSMask extends MOSMask {

   float[] xs, ys;
   int n;
   int[] imgxs, imgys;

   public RectMOSMask(float x, float y, float w, float h) {
      //Correct for ds9 starting with (1,1) instead of (0,0)
      x--;
      y--;
      this.shape = "Rectangle";
      this.xcen = x;
      this.ycen = y;
      float[] xs = {x-w/2, x+w/2, x+w/2, x-w/2};
      float[] ys = {y-h/2, y-h/2, y+h/2, y+h/2};
      this.xs = xs;
      this.ys = ys;
      this.n = 4;
      this.text = "";
      setScale(1,1);
   }

   public RectMOSMask(float x, float y, float w, float h, String text) {
      x--;
      y--;
      this.shape = "Rectangle";
      this.xcen = x;
      this.ycen = y;
      float[] xs = {x-w/2, x+w/2, x+w/2, x-w/2};
      float[] ys = {y-h/2, y-h/2, y+h/2, y+h/2};
      this.xs = xs;
      this.ys = ys;
      this.n = 4;
      this.text = text;
      setScale(1,1);
   }

   public RectMOSMask(float x, float y, float w, float h, float t) {
      x--;
      y--;
      this.shape = "Rectangle";
      this.xcen = x;
      this.ycen = y;
      float theta = (float)(t*Math.PI/180.0);
      float[] xs = {(float)(x-w/2*Math.cos(theta)-h/2*Math.sin(theta)), (float)(x+w/2*Math.cos(theta)-h/2*Math.sin(theta)), (float)(x+w/2*Math.cos(theta)+h/2*Math.sin(theta)), (float)(x-w/2*Math.cos(theta)+h/2*Math.sin(theta))};
      float[] ys = {(float)(y+w/2*Math.sin(theta)-h/2*Math.cos(theta)), (float)(y-w/2*Math.sin(theta)-h/2*Math.cos(theta)), (float)(y-w/2*Math.sin(theta)+h/2*Math.cos(theta)), (float)(y+w/2*Math.sin(theta)+h/2*Math.cos(theta))};
      this.xs = xs;
      this.ys = ys;
      this.n = 4;
      this.text = "";
      setScale(1,1);
   }

   public RectMOSMask(float x, float y, float w, float h, float t, String text) {
      x--;
      y--;
      this.shape = "Rectangle";
      this.xcen = x;
      this.ycen = y;
      float theta = (float)(t*Math.PI/180.0);
      float[] xs = {(float)(x-w/2*Math.cos(theta)-h/2*Math.sin(theta)), (float)(x+w/2*Math.cos(theta)-h/2*Math.sin(theta)), (float)(x+w/2*Math.cos(theta)+h/2*Math.sin(theta)), (float)(x-w/2*Math.cos(theta)+h/2*Math.sin(theta))};
      float[] ys = {(float)(y+w/2*Math.sin(theta)-h/2*Math.cos(theta)), (float)(y-w/2*Math.sin(theta)-h/2*Math.cos(theta)), (float)(y-w/2*Math.sin(theta)+h/2*Math.cos(theta)), (float)(y+w/2*Math.sin(theta)+h/2*Math.cos(theta))};
      this.xs = xs;
      this.ys = ys;
      this.n = 4;
      this.text = text;
      setScale(1,1);
   }

   public void drawMOSMask(Graphics g) {
      Color oldColor = g.getColor();
      if (color != null) g.setColor(color);
      g.drawPolygon(imgxs, imgys, n);
      if (displayText) {
	g.setFont(f);
	g.drawString(text, imgxcen, imgycen);
      }
      if (color != null) g.setColor(oldColor);
   }

   public void setScale(float xscale, float yscale) {
      this.imgxs = UFArrayOps.roundAsInts(UFArrayOps.multArrays(xs,xscale));
      this.imgys = UFArrayOps.roundAsInts(UFArrayOps.multArrays(ys,yscale));
      this.imgxcen = Math.round(xcen*xscale);
      this.imgycen = Math.round(ycen*yscale);
   }

   public void setScale(float xscale, float yscale, float ysize) {
      this.imgxs = UFArrayOps.roundAsInts(UFArrayOps.multArrays(xs,xscale));
      this.imgys = UFArrayOps.roundAsInts(UFArrayOps.multArrays(UFArrayOps.addArrays(UFArrayOps.multArrays(ys,-1),ysize),yscale));
      this.imgxcen = Math.round(xcen*xscale);
      this.imgycen = Math.round((ysize-ycen)*yscale);
   }

   public void addText(String text) {
      this.text = text;
   }

   public void setTextMode(boolean b) {
      this.displayText = b;
   }

   public void setFont(Font f) {
      this.f = f;
   }

   public void setColor(String s) {
      if (s.toLowerCase().equals("green")) this.color =  Color.GREEN;
      else if (s.toLowerCase().equals("red")) this.color = Color.RED;
      else if (s.toLowerCase().equals("blue")) this.color = Color.BLUE;
      else if (s.toLowerCase().equals("black")) this.color = Color.BLACK;
      else if (s.toLowerCase().equals("white")) this.color = Color.WHITE;
      else if (s.toLowerCase().equals("cyan")) this.color = Color.CYAN;
      else if (s.toLowerCase().equals("magenta")) this.color = Color.MAGENTA;
      else if (s.toLowerCase().equals("yellow")) this.color = Color.YELLOW;
   }
}
