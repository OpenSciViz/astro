import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class FATBOYHelpBrowser extends javax.swing.JFrame {
   JEditorPane browser;
   JPanel topPanel;
   JButton back, forward, closeButton;
   JScrollPane sp;
   Vector history;
   int current;

   public FATBOYHelpBrowser(URL url) {
      super("FATBOY Help");
      setSize(1024,880);
      history = new Vector();
      history.add(url);
      current = 0;
      topPanel = new JPanel();
      topPanel.setPreferredSize(new Dimension(1024,50));
      back = new JButton(" <--  BACK ");
      back.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   try {
	      browser.setPage((URL)history.get(--current));
	      if (current == 0) back.setEnabled(false);
	      forward.setEnabled(true);
	   } catch (IOException e) { System.err.println(e.toString()); }
	}
      });
      back.setEnabled(false);
      forward = new JButton(" FORWARD  --> ");
      forward.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           try {
	      browser.setPage((URL)history.get(++current));
              if (current == history.size()-1) forward.setEnabled(false);
              back.setEnabled(true);
           } catch (IOException e) { System.err.println(e.toString()); }
        }
      });
      forward.setEnabled(false);
      closeButton = new JButton(" Close Window ");
      closeButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
	   FATBOYHelpBrowser.this.dispose();
        }
      });
      try {
	browser = new JEditorPane(url);
	browser.setContentType("text/html;");
	browser.setEditable(false);
	browser.addHyperlinkListener(new HyperlinkListener() {
	   public void hyperlinkUpdate(HyperlinkEvent ev) {
	      if (ev.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
		try {
		   browser.setPage(ev.getURL());
		   if (current == history.size()-1) {
		      history.add(++current, ev.getURL());
		   } else {
		      history.set(++current, ev.getURL());
		   }
		   if (current == 1) back.setEnabled(true);
		   forward.setEnabled(false);
		} catch(IOException e) { System.err.println(e.toString());}
	   }
	});
      } catch(IOException e) { 
	System.err.println(e.toString());
	browser = new JEditorPane();
      }
      topPanel.add(back);
      topPanel.add(forward);
      topPanel.add(closeButton);
      sp = new JScrollPane(browser);
      sp.setPreferredSize(new Dimension(1024,830));
      Container content = getContentPane();
      content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
      content.add(topPanel);
      content.add(sp);
      pack();
      setVisible(true);
   }
}
