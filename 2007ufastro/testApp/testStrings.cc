

#include "UFRuntime.h"
__UFRuntime_H__(__testStrings_cc__);

#include "UFStrings.h"

#include "string"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

extern int errno;

int main(int, char**) {

  int length = 244;

  string s[3];
  s[0] = "string1";
  s[1] = "This is string 2";
  s[2] = "Why do we need string 3?";

  cout << "s[0] = " << s[0] << endl;
  cout << "s[1] = " << s[1] << endl;
  cout << "s[2] = " << s[2] << endl;

  UFStrings ufs("strings to file", s, 3);

  cout << "ufs.valSize() = " << ufs.valSize() << endl;
  cout << "ufs.name() = " << ufs.name() << endl;
  cout << "ufs.elements() = " << ufs.elements() << endl;


  int numelems = ufs.elements();
  for (int i = 0; i < numelems; i++) {
      cout << "ufs.stringAt(" << i << ") = " << (ufs.stringAt(i))->c_str() << endl;
  }

  int myfd = 0;
  int numWrote = 0;
  if ((myfd = open("s.dat", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) == -1) {
      cout << "Sorry, can't open file. errno =  " << errno << endl;
  }
  else {
      numWrote = ufs.writeTo(myfd);
      cout << "writeTo wrote " << numWrote << " bytes" << endl;
//       numWrote = ufs.writeValues(myfd);
//       cout << "writeValues wrote " << numWrote << " bytes" << endl;
      close(myfd);
  }

  UFStrings ufsFromFile("UFStrings from file",20);

  int fdin = 0;
  int numRead = 0;
  if ((fdin = open("t.dat", O_RDONLY )) == -1) {
      cout << "Sorry, can't open file. errno =  " << errno << endl;
  }
  else {
      numRead = ufsFromFile.readFrom(fdin);
      cout << "readFrom read " << numRead << " bytes" << endl;
      close(fdin);
  }

  numelems = ufsFromFile.elements();
  for (int i = 0; i < numelems; i++) {
      cout << "ufsFromFile.stringAt(" << i << ") = " << (ufsFromFile.stringAt(i))->c_str() << endl;
  }
}

