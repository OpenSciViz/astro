const char rcsId[] = "$Id: ufints.cc 14 2008-06-11 01:49:45Z hon $";

#include "new"
#include "iostream"
#include "strstream"
#include "string"

#include "cerrno"
#include "csignal"

#include "UFClientSocket.h"
#include "UFPosixRuntime.h"
#include "UFRuntime.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"
#include "UFFrameConfig.h"
#include "UFObsConfig.h"

static UFClientSocket _client;
volatile bool keepRunning = true ;

static void sigHandler( int )
{
  keepRunning = false ;
  clog << "Shutting down...\n" ;
}

// test UFInts
UFProtocol* ints(UFClientSocket& _client, size_t cnt)
{
  string name = "moo" ;

  int *vals = new (nothrow) int[ cnt ];
  if( NULL == vals )
    {
      clog << "ints> Memory allocation failure\n" ;
      keepRunning = false ;
    }

  for( size_t i = 0; i < cnt; ++i )
    {
      vals[i] = i;
    }

  UFInts ping(name, (const int*) vals, cnt, false);
  clog << "pinguf::ints> send name= " << ping.cname()
       << ", time= " << ping.timestamp() << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "ints> Failed to send object\n" ;
      keepRunning = false ;
      delete[] vals ;
      return 0 ;
    }

  delete [] vals;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 )
    {
      clog << "ints> Failed to receive object\n" ;
      return 0;
    }

  string echo_t = ufp->timestamp();
  string t0 = ping.timestamp();

  UFInts *ufi = dynamic_cast<UFInts*>(ufp);
  if( ufi )
    {
    clog << "pinguf::ints> received reply:\n name= "
	 << ufi->cname() << ", time= " << ufi->timestamp()
	 << ", elems= " << ufi->elements() << ", nvals= "
	 << ufi->numVals() << endl;
    clog << "pinguf::ints> " << *((int*)ufi->valData(0))
	 << endl;
    clog << "pinguf::ints> " << *((int*)ufi->valData(ufi->elements()/2))
	 << endl;
    clog << "pinguf::ints> " << *((int*)ufi->valData(ufi->elements()-1))
	 << endl;
    //clog<<"pinguf::ints> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "pinguf::ints> server did not return UFInts?, typ= "
	   << ufp->typeId() << endl;
    }
  return ufp;
}

int main(int argc, const char** argv) 
{
  if( argc != 3 )
    {
      clog << "Usage: " << argv[ 0 ] << " <server> <port>\n" ;
      return 0 ;
    }

  string host = argv[ 1 ] ;
  unsigned short int portNo =
    static_cast< unsigned short int >( atoi( argv[ 2 ] ) ) ;

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "main> Unable to establish signal handler for SIGINT: "
	   << strerror( errno ) << endl ;
      return 0 ;
    }

  size_t cnt = 0;
  size_t numFrames = 1 ;

  while( keepRunning )
    { 
      if( !_client.validConnection() )
	{
	  // test if server has closed connection
	  clog << "pinguf> connecting to host: " << host
	       << "on port # " << portNo << endl;
	  while( _client.connect( host, portNo ) <= 0 )
	    {
	      clog << "Unable to connect, sleeping...\n" ;
	    sleep(10);
	    }
	  clog << "pinguf> connected to port # " << portNo << endl ;
	}
    
      cnt += 100000 ;
      if( cnt > 4000000 )
	{
	  cnt = 100000 ;
	}

      clog << numFrames++ << ": Sending UFInts of size "
	   << (sizeof(int)*cnt) << " bytes\n" ;

      UFProtocol* reply = ints( _client, cnt ) ;
      if( NULL == reply )
	{
	  clog << "main> Read error: " << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}

      delete reply ;

    } // while()
}

