#include	"iostream"

#include	"cstdlib"

#include	"UFSerialPortServer.h"

int main( int argc, const char** argv )
{

if( argc != 2 )
	{
	clog << "Usage: " << argv[ 0 ] << " <portnum>" << endl ;
	return 0 ;
	}

int portNo = atoi( argv[ 1 ] ) ;

clog << "Starting server on port " << portNo << endl ;

UFSerialPortServer s( portNo ) ;
s.run() ;

return 0 ;

}
