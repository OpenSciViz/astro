
#include "new"
#include "iostream"

#include "unistd.h"
#include "cstdlib"
#include "csignal"
#include "cstring"
#include "cerrno"

#include "UFProtocol.h"
#include "UFInts.h"
#include "UFClientSocket.h"

using std::clog ;
using std::endl ;

void readImage() ;
void writeImage() ;

volatile bool keepRunning = true ;
unsigned int numReads = 0 ;
unsigned short int port = 0 ;

UFClientSocket* sock = 0 ;
UFInts* image = 0 ;
int* data = 0 ;

void sigHandler( int )
{
  clog << "Caught signal, shutting down\n" ;
  keepRunning = false ;
}

int main( int argc, const char** argv )
{

  if( argc != 3 || 0 == ::atoi( argv[ 2 ] ) )
    {
      clog << "Usage: " << argv[ 0 ] << " <server> <port>\n" ;
      return 0 ;
    }

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "Unable to establish signal handler for SIGINT\n" ;
      return 0 ;
    }

  port = static_cast< unsigned short int >( ::atoi( argv[ 2 ] ) ) ;

  size_t imageSize = 128 * 128 ;
  try
    {
      sock = new UFClientSocket( port ) ;
      data = new int[ imageSize ] ;
      image = new UFInts( "blah", (const int*) data, 128 * 128 ) ;
    }
  catch( std::bad_alloc )
    {
      if( data != 0 )
	{
	  delete[] data ;
	  data = 0 ;
	}
      if( image != 0 )
	{
	  delete image ;
	  image = 0 ;
	}
      clog << "Memory allocation failure\n" ;
      return 0 ;
    }

  for( size_t i = 0 ; i < imageSize ; ++i )
    {
      data[ i ] = i ;
    }

  if( sock->connect( argv[ 1 ], port ) < 0 )
    {
      clog << "Unable to connect to: " << argv[ 1 ]
	   << ':' << port << endl ;
      delete sock ; sock = 0 ;
      delete[] data ; data = 0 ;
      delete image ; image = 0 ;
      return 0 ;
    }
  clog << "Connected\n" ;

  while( keepRunning )
    {
      writeImage() ;
      readImage() ;
    }

  clog << "Shutting down...\n" ;

  delete image ;
  delete[] data ;

  sock->close() ;
  delete sock ; sock = 0 ;
  return 0 ;

}

void readImage()
{

  bool done = false ;
  while( keepRunning && !done )
    {

      ::usleep( 10000 ) ;

      int readable = sock->readable() ;
      if( readable < 0 )
	{
	  clog << "readImage> Error in readable: "
	       << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}
      else if( 0 == readable )
	{
	  continue ;
	}

      clog << "readImage> Attempting to read image...\n" ;

      if( image->recvFrom( *sock ) <= 0 )
	{
	  clog << "readImage> Read error in recvFrom: "
	       << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}

      clog << "readImage> Read object successfully! ["
	   << ++numReads << "]\n" ;
      done = true ;

    }

}

void writeImage()
{

  bool done = false ;
  while( keepRunning && !done )
    {

      ::usleep( 10000 ) ;

      int writable = sock->writable() ;
      if( writable < 0 )
	{
	  clog << "writeImage> writable returned error: "
	       << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}
      else if( 0 == writable )
	{
	  continue ;
	}

      clog << "writeImage> Writing image...\n" ;

      if( image->sendTo( *sock ) <= 0 )
	{
	  clog << "writeImage> Write error to socket: "
	       << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}

      clog << "writeImage> Successfully wrote image!\n" ;
      done = true ;

    }

}
