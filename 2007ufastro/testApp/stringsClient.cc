/* stringsClient.cc
 */

#include	"iostream"
#include	"string"
#include	"vector"

#include	"cstdlib"
#include	"csignal"

#include	"UFClientSocket.h"
#include	"UFStrings.h"

using namespace std ;

void		sigHandler( int ) ;
int		sendToServer( UFClientSocket&, const vector< string >& ) ;
UFStrings*	readFromServer( UFSocket& ) ;

std::ostream& operator<<( std::ostream& out, const UFStrings& strings )
{
for( int i = 0 ; i < strings.elements() ; ++i )
	{
	out << strings[ i ] << endl ;
	}
return out ;
}


int main( int argc, const char** argv )
{

if( argc != 3 )
	{
	clog << "Usage: " << argv[ 0 ] << " <server> <port>\n" ;
	return 0 ;
	}

string server = argv[ 1 ] ;
int portNo = atoi( argv[ 2 ] ) ;

signal( SIGINT, sigHandler ) ;

UFClientSocket uplink ;

clog << "Connecting to: " << server << ", port " << portNo << endl ;

if( uplink.connect( server, portNo ) < 0 )
	{
	clog << "Failed to connect to server\n" ;
	return 0 ;
	}

vector< string > outBuffer ;
unsigned int lineCount = 1 ;

clog << "Connected\n" ;
clog << "\nWelcome to the UFStrings test client.\n" ;
clog << "Please enter text to send to the server.\n" ;
clog << "Enter \"flush\" to flush the buffered lines of text\n" ;
clog << "to the server.\n" ;
clog << "Enter \"recv\" to read a UFStrings object from the "
     << "server\n" ;
clog << "Enter \"quit\" to end\n\n" ;

while( true )
	{
	char buffer[ 1000 ] ;

	cout << "[" << lineCount << "]> " ;
	cin.getline( buffer, 999 ) ;

	string command = buffer ;

	if( command == "quit" )
		{
		break ;
		}

	if( command == "flush" )
		{
		if( sendToServer( uplink, outBuffer ) < 0 )
			{
			clog << "Failed to send to server\n" ;
			}
		else
			{
			clog	<< "Sent " << outBuffer.size() << " lines to "
				<< "server\n" ;
			}
		outBuffer.clear() ;
		lineCount = 1 ;
		continue ;
		}

	if( command == "recv" )
		{
		UFStrings* strings = readFromServer( uplink ) ;
		if( NULL == strings )
			{
			clog	<< "Unable to recv UFStrings object\n" ;
			}
		else
			{
			clog	<< "Received object: " << *strings << endl ;
			delete strings ;
			}
		outBuffer.clear() ;
		lineCount = 1 ;
		continue ;
		}

	// Else, buffer the line
	outBuffer.push_back( command ) ;

	lineCount++ ;

	}

uplink.close() ;
return 0 ;

}

UFStrings* readFromServer( UFSocket& uplink )
{
return dynamic_cast< UFStrings* >( UFProtocol::createFrom( uplink ) ) ;
}

int sendToServer( UFClientSocket& uplink, const vector< string >& outBuffer )
{
strstream s ;
s << time( 0 ) ;
string name ;
s >> name ;
delete[] s.str() ;

UFStrings outStrings( name, outBuffer ) ;
return outStrings.sendTo( uplink ) ;
}

void sigHandler( int )
{
cout << "\nEnter \"quit\" to end\n" ;
}
