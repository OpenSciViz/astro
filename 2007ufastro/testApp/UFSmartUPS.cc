#include <cstdio>
#include <errno.h>
//#include <sys/time.h>

//#ifdef _WIN32
/* Header files for Windows */
//#include <winsock.h>
//#include <io.h>

//#else
/* Header files for Linux */
//#include <sys/types.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <netdb.h>
#include <unistd.h>
//#endif

#include "UFSmartUPS.h"

UFSmartUPS::UFSmartUPS() : UFClientSocket() {
  reset();
}

void UFSmartUPS::reset() {
  content_len = 0;
  message = "";
}

int UFSmartUPS::requestPage(const char * pagename, const char * hostname) {
    /* create and send the http GET request */
  //char buffer[100];
  //sprintf( buffer, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n",
  //	     pagename,hostname);
  string buffer = "GET ";
  buffer+=pagename;
  buffer+=" HTTP/1.1\r\nHost: ";
  buffer+=hostname;
  buffer+="\r\n\r\n";
  return send( buffer );
}

int UFSmartUPS::postLogon(const char * username, const char * password, const char * hostname) {
    /* try to post username and password */
  string buffer = "POST /PROCESS-LOGON HTTP/1.1\r\nHost: ";
  buffer+=hostname;
  buffer+="\r\nUser-Agent: Mozilla/4.7(X11;Linux)\r\nAccept: */*\r\nContent-type: application/x-www-form-urlencoded\r\nContent-length: 28\r\n\r\nUserName=";
  buffer+=username;
  buffer+="&PassWord=";
  buffer+=password;
  buffer+="\r\n\r\n";
  return send( buffer ,sock);
}

int UFSmartUPS::getResponse() {
  string buffer="";
  unsigned int count=0;
  unsigned int n=0,m=0;
  string temp="";
  bool keepGoing = true;
  reset(); // reset message and content_len
  do {
    buffer = "";
    count += recv( buffer,sock );
    if (count > 0) {
      temp += buffer;
      if ( (n = temp.find("Content-Length:",0)) != string::npos) 
	if ( (m = temp.find("\n",n)) != string::npos) {
	  content_len = atoi(temp.substr(n+15,m-n-15).c_str());
	  keepGoing = false;
	} 
    }
  } while (keepGoing);
  // now read the rest of the post.
  // first find the <html> tag
  keepGoing = true;
  do {
    buffer = "";
    count += recv(buffer,sock);
    temp += buffer;
    if ( (n = temp.find("<html>",0)) != string::npos) {
      count -= n;
      message = temp.substr(n,count);
      while (count < content_len) {
	//while (message.find("</html>",0) == string::npos) {
	buffer = "";
	count += recv(buffer,sock);
	message += buffer;
      }
      keepGoing = false;
      if (message.size() > content_len) 
	message.erase(content_len,message.size()-content_len);
      //cout << count << endl;
    }
  } while (keepGoing);
  return content_len;
}

int UFSmartUPS::parseMessage() {
  unsigned int i=0,j=0;
  if ( (i = message.find("Input Voltage:",0)) != string::npos)
    if ( (j = message.find("V",i+92)) != string::npos)
      params.inpVolt = atoi(message.substr(i+92,i+92-j).c_str());
  if ( (i = message.find("Battery Voltage:",0)) != string::npos)
    if ( (j = message.find("V",i+94)) != string::npos)
      params.battVolt = (double)atof(message.substr(i+94,i+94-j).c_str());
  if ( (i = message.find("Input Frequency:",0)) != string::npos)
    if ( (j = message.find("Hz",i+94)) != string::npos)
      params.inpFreq = (double)atof(message.substr(i+94,i+94-j).c_str());
  if ( (i = message.find("Battery Capacity:",0)) != string::npos)
    if ( (j = message.find("%",i+95)) != string::npos)
      params.battCap = atoi(message.substr(i+95,i+95-j).c_str());
  if ( (i = message.find("Load Percent:",0)) != string::npos)
    if ( (j = message.find("%",i+91)) != string::npos)
      params.load = atoi(message.substr(i+91,i+91-j).c_str());
  return 1;
}

void UFSmartUPS::printParams() {
  cout.setf(ios::fixed);
  cout.precision(1);
  cout << "Input Voltage:    " << params.inpVolt  << " V" << endl 
       << "Battery Voltage:  " << params.battVolt << " V"<< endl 
       << "Input Frequency:  " << params.inpFreq  << " Hz"<< endl  
       << "Battery Capacity: " << params.battCap  << " %"<< endl
       << "Load Percent:     " << params.load     << " %"<< endl;
}

//functions to inspect message
bool UFSmartUPS::loginRequired() {
  if (message.find("Unauthorised",0) != string::npos)
    return true;
  else return false;
}
bool UFSmartUPS::loginFailed() {
  if (message.find("Login Error",0) != string::npos)
    return true;
  else return false;
}

// static funcs;

int UFSmartUPS::main(int argc, char ** argv) {
  //char HOST_NAME[] = "192.168.111.231";
  char HOST_NAME[] = "192.168.111.40";
  int HTTP_PORT = 80;
  UFSmartUPS us;
  if ((us.connect(HOST_NAME,HTTP_PORT,false)) < 0) {
    fprintf(stderr,"Uh oh, better exit\n");
    exit(1);
  }
  us.setTimeOut(10);
  us.requestPage("/status.htm",HOST_NAME);
  us.getResponse();  
  if (us.loginRequired()) { //check to see if we need to send a login message
    us.postLogon("admin","1234",HOST_NAME);
    us.getResponse();
    if (us.loginFailed()) {
      fprintf(stderr,"Login failed.  Aborting...\n");
      return -1;
    }    
  }
  while (true) { // enter polling loop
    us.requestPage("/status.htm",HOST_NAME);
    us.getResponse();
    us.parseMessage();
    //us.printParams();
    cout << us.params.battCap << " " << time(NULL) << endl;
    //cout << "Trying to sleep" << endl;
    sleep(10);//sleep for 3 seconds
    //cout << "Done Sleeping" << endl;
  }
}
