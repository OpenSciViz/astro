const char rcsId[] = "$Id: LS_208poll2.cc 14 2008-06-11 01:49:45Z hon $";

// This program constantly monitors the Lakeshore 208
// Temperature Monitor and prints to stdout.
//
// A program can exec this process with popen to get the
// output stream and grab all the data.
// 
// Data is constantly output in this format:
//     Ch1temp Ch2temp Ch3temp Ch4temp Ch5temp Ch6temp Ch7temp Ch8temp
// , where the temperature is in Kelvin
// 
//
// ASCII text strings are used to communicate with the LS 208.
//
// The LS 208 must use a certain line terminator?
//   -- LF, CR, CR/LF, or LF/CR ?
// (LF = ascii value 10, CR = ascii value 13)
// 


#include "iostream.h"
#include "string"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFClientSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"

#include "UFStringTokenizer.h"

static UFSocket theConnection; // globally accessed by sigHandler
static UFClientSocket _client;
static double LS_208_WAIT_TIME = 4.0;


static void lostConnection(int sig) {
  if( sig == SIGPIPE ) {
    cerr << "LS_208poll2::lostConnection> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
  }
  if( sig == SIGINT ) {
    cerr << "LS_208poll2::lostConnection> sig == SIGINT (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
    exit(0);
  }
  UFPosixRuntime::defaultSigHandler(sig);
}

char* getData() {
  unsigned char* retVal = 0;
  int numAvail = 0;
  int numRead = 0;


  // We will never get more than this from the device
  retVal = new unsigned char[256];

  bool keepGoing = true;
  while (keepGoing) {

    // See how many bytes are available on socket
    numAvail = _client.available();

    if ((numAvail+numRead) > 256) {
      cerr<<"getData> numAvail too much! numAvail = "<<numAvail<<endl;
      return (char *)retVal = 0;
    }

    if (numAvail < 0)  // If nothing there, wait for one only (Blocking I/O)
      numRead += _client.recv(retVal+numRead, 1);
    else
      numRead += _client.recv(retVal+numRead, numAvail);

    if (retVal[numRead-1] == '\n')
      keepGoing = false;

  }
  return (char*)retVal;
}

void uflisten(UFServerSocket& server) {
  cerr<<"LS_340serv> waiting for client connection on: "<<server.description()<<endl;
  theConnection = server.listenAndAccept();
  cerr<<"LS_340serv> got client connection..."<<endl;

  theConnection.readable();
  return;
}

void changeChannel(int channelNum) {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "YC%d\n", channelNum);
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  
  return;
}

void requestInfo() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "WS\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  
  return;
}

void toCelsius() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0C\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toFahrenheit() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0F\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toKelvin() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0K\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

void toVolts() {
  char sendString[255] = "";  // string to send to annex
  sprintf(sendString, "F0V\n");
  cerr << "Sent string " << sendString << endl;
  _client.send((unsigned char*)sendString, strlen(sendString));
  return;
}

// Strip CR's and LF's off end of char array
// void stripCR_LF(char* data) {
//   while ((data[strlen(data)] == 13) || (data[strlen(data)] == 10)) {
//     data[strlen(data)] = '\0';
//   }
// }

// Strip CR's and LF's off end of char array
void stripCR_LF(char* data) {
  cerr << "stripCR_LF> in function" << endl;

  if (strlen(data) < 1)
    return;
  while ((data[strlen(data)-1] == 13) || (data[strlen(data)-1] == 10)) {
    data[strlen(data)-1] = '\0';
    if (strlen(data) < 1)
      break;
  }
}

bool formatData(char*& theString) {
  bool retVal = true;

  stripCR_LF(theString);

  cerr << "formatData> theString = " << theString << endl;

  // set up string for easier handling
  string s(theString);
  // deallocate original mem
  delete[] theString;

  UFStringTokenizer ufst(s,',');

//   string s2 = "";
//   for (int i = 0; i < ufst.size(); i++) {
//     s2 += ufst[i];
//     if (i != ufst.size()-1)
//       s2 += " ";
//   }

  if (ufst.size() != 3) {
    cerr << "formatData> bad data: tokens != 3" << endl;
    return false;
  }

  string s2 = ufst[0] + " ";
  if (ufst[1].size() == 6)
    s2 += string(ufst[1],0,ufst[1].size()-1);
  else {
    cerr << "formatData> bad data: second token length not 6" << endl;
    return false;
  }

  cerr << "s2 = " << s2 << endl;

  // set ptr to point to newly formatted char* string
  theString = new char[s2.size()+1];
  strcpy(theString,s2.c_str());

  return retVal;
}
  
int main(int argc, char** argv) {

  cerr << "LS_208poll2> getpgrp = " << getpgrp() << endl;

  // define defaults
  string annexIP("192.168.111.101");
  string annexPort("7003");

//   if( argc > 1 ) { //port supplied
//     port = argv[1];
//   }

  if( argc < 3) {
    cerr << "usage> LS_208poll2 annexIP annexPort" << endl;
    // Output message for program that is reading stdout of this program
    cout << "ERROR" << endl;
    exit(0);
  }

  // Should we check for arguments on command line and use defaults
  // if arguments not there?  Maybe later...
  annexIP = argv[1];
  annexPort = argv[2];

  int annexPortNo = atoi(annexPort.data());
  cerr << "annexIP = " << annexIP << endl;
  cerr << "annexPort = " << annexPortNo << endl;

  cerr << "LS_208poll> connecting to Lakeshore 208 at IP " << annexIP
       << " on port " << annexPortNo << endl ;
  if (_client.connect( annexIP.c_str(), annexPortNo ) < 0 ) {
    cerr << "LS_208poll> Can't connect to Lakeshore 208" << endl;
    // Output message for program that is reading stdout of this program
    cout << "ERROR" << endl;
    exit(0);
  }
  else {
    // Output message for program that is reading stdout of this program
    cout << "OK" << endl;
  }

  UFPosixRuntime::setSignalHandler(lostConnection);
  cerr << "established signal handler for SIGPIPE = " << SIGPIPE << endl;
  cerr << "established signal handler for SIGINT = " << SIGINT << endl;

  // enter recv/send loop
  //  unsigned long msgcnt=0;
  string newnam;
  char sendString[255] = "";  // string to send to annex
  char* retData = 0;     // will hold return data from annex

  int channel = 1;
  // Start with first channel
  changeChannel(channel);
  UFPosixRuntime::sleep(LS_208_WAIT_TIME); // sleep for 3 seconds
  // We will only be using Kelvin readings
  toKelvin();
  UFPosixRuntime::sleep(LS_208_WAIT_TIME); // sleep for 3 seconds
  cerr << "Set to Kelvin" << endl;

  do {

    cerr << "Requesting info..." << endl;
    requestInfo();
    UFPosixRuntime::sleep(LS_208_WAIT_TIME); // sleep for 3 seconds

    if ((retData = getData()) == 0) {
      cerr<<"Can't get any data from device"<<endl;
    }
    else {
//       cout<<"Got reply from device: "<< retData << endl;
      // This line should be the only output from this program on stdout
      // so another program can read it in
      if (formatData(retData))
 	cout << "LS_208poll "  << retData << endl;
      delete[] retData;
    }

    channel++;
    // If we have incremented past end of last channel (channel 8),
    // go back to first channel
    if (channel == 9)
      channel = 1;
    changeChannel(channel);
    UFPosixRuntime::sleep(LS_208_WAIT_TIME); // sleep for 3 seconds to permit setup
    
  } while( true );  // loop forever

} // end of main


      
