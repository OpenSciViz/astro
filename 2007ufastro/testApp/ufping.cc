const char rcsId[] = "$Id: ufping.cc 14 2008-06-11 01:49:45Z hon $";

#include "iostream"
#include "strstream"
#include "string"
#include "new"

#include "unistd.h"
#include "csignal"

#include "UFClientSocket.h"
#include "UFRuntime.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFByteFrames.h"
#include "UFInts.h"
#include "UFIntFrames.h"
#include "UFFloats.h"
#include "UFFloatFrames.h"

void	sigHandler( int ) ;
void	usage( char** ) ;

bool keepRunning = true ;

void sigHandler( int )
{
clog << "Received SIGNAL: Attemping to abort gently\n" ;
keepRunning = false ;
}

// return diff == echo - ping
double timediff(string& echo, string& ping, int cnt) {
  // the string time format should always be: "yyyy:ddd:hh:mm:ss.uuuuuu"
  char eval[] = "yyyydddhhmmss.uuuuuu";
  char pval[] = "yyyydddhhmmss.uuuuuu";
  int slen = strlen(eval);
  for( int n= 0, i= 0; i < slen; ++i, ++n ) {
    if( echo[n] == ':' )
      ++n;
    eval[i] = echo[n];
    pval[i] = ping[n];
  }
  double diff = atof(eval) - atof(pval);
  clog<<"ufping::timediff> cnt: "<<cnt<<", diff= "<<diff<<" --------------------------------"<<endl;
  return diff;
}

UFProtocol* timestamp( UFClientSocket& _client, int cnt) {
  strstream s;
  s << "ufping::timestamp #"<<cnt<<" "<<ends;
  char* name = s.str();
  UFTimeStamp ping(name, UFProtocol::_MinLength_);
  //UFTimeStamp ping(string(name), UFProtocol::_MinLength_);
  clog<<"ufping::timestamp> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);
  delete[] s.str() ;
  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 ) return 0;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFTimeStamp *uft = dynamic_cast<UFTimeStamp*>(ufp);
  if( uft ) {
    clog<<"ufping::timestamp> received reply:\n name= "
	<<uft->cname()<<", time= "<<uft->timestamp()<<endl;
    //clog<<"ufping::timestamp> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::timestamp> server did not return UFTimeStamp?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}

UFProtocol* strings(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::strings #"<<cnt<<" "<<ends;
  string name = sn.str();
  string *vals = new string[cnt];
  for( int i = 0; i < cnt; ++i ) {
    strstream s;
    s <<i<<" -- "<<name<<ends;
    vals[i] = s.str();
    delete[] s.str();
  }
  UFStrings ping(name, vals, cnt);
  clog<<"ufping::strings> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 )
	{
	return 0;
	}

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFStrings *ufs = dynamic_cast<UFStrings*>(ufp);
  if( ufs ) {
    clog<<"ufping::strings> received reply:\n name= "
	<<ufs->cname()<<", time= "<<ufs->timestamp()
	<<", elems= "<<ufs->elements()<<", nvals= "<<ufs->numVals()<<endl;
    for( int i = 0; i < ufs->elements(); ++i )
      clog<<"ufping::strings> "<<ufs->valData(i)<<endl;
    //clog<<"ufping::strings> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::strings> server did not return UFStrings?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}

UFProtocol* bytes(UFClientSocket& _client, int cnt) {
  clog<<"ufping::bytes> entering function" << endl;
  strstream sn;
  sn << "ufping::bytes #"<<cnt<<" "<<ends;
  string name = sn.str();
  cnt *= 1000;
  unsigned char *vals = new (nothrow) unsigned char[cnt];
  if (vals == 0)
	{
	clog<<"ufping::bytes> vals == 0" << endl;
	return NULL ;
	}
  for( unsigned int i = 0; i < (unsigned)cnt; ++i ) {
    vals[i] = i % 256;
  }
  clog<<"ufping::bytes> after assigning values" << endl;
  bool notshared = false;
  UFBytes ping(name, (const unsigned char*) vals, cnt, notshared);
  clog<<"ufping::bytes> _shared = "<< ping._shared << " _shallow = " << ping._shallow << endl;
  clog<<"ufping::bytes> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  clog<<"ufping::bytes> length()= "<<ping.length()<<endl;
  _client.send(&ping);

  delete[] vals;
  delete[] sn.str();

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 ) return 0;
  ufp->_shallow = false; // make sure _values is deleted!
  clog<<"ufping::bytes> after createFrom: _shared = "<< ufp->_shared << " _shallow = " << ufp->_shallow << endl;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFBytes *ufb = dynamic_cast<UFBytes*>(ufp);
  if( ufb ) {
    clog<<"ufping::bytes> received reply:\n name= "
	<<ufb->cname()<<", time= "<<ufb->timestamp()
	<<", elems= "<<ufb->elements()<<", nvals= "<<ufb->numVals()<<endl;
    clog<<"ufping::bytes> "<<(int)*((char*)ufb->valData(0))<<endl;
    clog<<"ufping::bytes> "<<(int)*((char*)ufb->valData(ufb->elements()/2))<<endl;
    clog<<"ufping::bytes> "<<(int)*((char*)ufb->valData(ufb->elements()-1))<<endl;
    //clog<<"ufping::bytes> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::bytes> server did not return UFBytes?, typ= "
	<<ufp->typeId()<<endl;
  }
  clog<<"ufping::bytes> before returning ufp" << endl;
  return ufp;
}

UFProtocol* byteframes(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::byteframes #"<<cnt<<" "<<ends;
  string name = sn.str();
  unsigned int nvals = cnt*200*100;
  unsigned char *vals = new unsigned char[nvals];
  for( unsigned int i = 0; i < (unsigned int) nvals; ++i ) {
    vals[i] = (unsigned char)(i % 256);
  }
  UFBytes tmp(name, vals, nvals, false);
  UFByteFrames ping(tmp, 200, 100, cnt);

  clog<<"ufping::byteframes> length()= "<<ping.length()<<endl;

  clog<<"ufping::byteframes> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 ) return 0;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFByteFrames *ufb = dynamic_cast<UFByteFrames*>(ufp);
  if( ufb ) {
    clog<<"ufping::byteframes> received reply:\n name= "
	<<ufb->cname()<<", time= "<<ufb->timestamp()
	<<", elems= "<<ufb->elements()<<", nvals= "<<ufb->numVals()<<endl;
//    for( int i = 0; i < ufb->elements(); ++i ) {
//      clog<<"ufping::byteframes> "<<(unsigned int)*((char*)ufb->valData(i));
//      clog<<" address =  "<<(unsigned int)ufb->valData(i)<<endl;
//    }
    //clog<<"ufping::byteframes> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::byteframes> server did not return UFByteFrames?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}


UFProtocol* ints(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::ints #"<<cnt<<" "<<ends;
  string name = sn.str();
  cnt *= 10;
  int *vals = new int[cnt];
  for( unsigned int i = 0; i < (unsigned)cnt; ++i ) {
    vals[i] = i;
  }
  UFInts ping(name, vals, cnt, false);
  clog<<"ufping::ints> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 ) return 0;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFInts *ufi = dynamic_cast<UFInts*>(ufp);
  if( ufi ) {
    clog<<"ufping::ints> received reply:\n name= "
	<<ufi->cname()<<", time= "<<ufi->timestamp()
	<<", elems= "<<ufi->elements()<<", nvals= "<<ufi->numVals()<<endl;
    clog<<"ufping::ints> "<<*((int*)ufi->valData(0))<<endl;
    clog<<"ufping::ints> "<<*((int*)ufi->valData(ufi->elements()/2))<<endl;
    clog<<"ufping::ints> "<<*((int*)ufi->valData(ufi->elements()-1))<<endl;
    //clog<<"ufping::ints> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::ints> server did not return UFInts?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}

UFProtocol* intframes(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::intframes #"<<cnt<<" "<<ends;
  string name = sn.str();
  unsigned int nvals = cnt*200*100;
  int *vals = new int[nvals];
  for( unsigned int i = 0; i < (unsigned int) nvals; ++i ) {
    vals[i] = i;
  }
  UFInts tmp(name, vals, nvals, false);
  UFIntFrames ping(tmp, 200, 100, cnt);

  clog<<"ufping::intframes> length()= "<<ping.length()<<endl;

  clog<<"ufping::intframes> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 ) return 0;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFIntFrames *ufif = dynamic_cast<UFIntFrames*>(ufp);
  if( ufif ) {
    clog<<"ufping::intframes> received reply:\n name= "
	<<ufif->cname()<<", time= "<<ufif->timestamp()
	<<", elems= "<<ufif->elements()<<", nvals= "<<ufif->numVals()<<endl;
//    for( int i = 0; i < ufif->elements(); ++i ) {
//      clog<<"ufping::intframes> "<<*((int*)ufif->valData(i)+2)<<endl;
//    }
    //clog<<"ufping::intframes> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::intframes> server did not return UFIntFrames?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}


UFProtocol* floats(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::floats #"<<cnt<<" "<<ends;
  string name = sn.str();
  cnt *= 10;
  float *vals = new float[cnt];
  for( unsigned int i = 0; i < (unsigned)cnt; ++i ) {
    vals[i] = i*1.25;
  }
  UFFloats ping(name, vals, cnt, false);
  clog<<"ufping::floats> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 )
	{
	return 0;
	}

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFFloats *uff = dynamic_cast<UFFloats*>(ufp);
  if( uff ) {
    clog<<"ufping::floats> received reply:\n name= "
	<<uff->cname()<<", time= "<<uff->timestamp()
	<<", elems= "<<uff->elements()<<", nvals= "<<uff->numVals()<<endl;
    clog<<"ufping::floats> "<<*((float*)uff->valData(0))<<endl;
    clog<<"ufping::floats> "<<*((float*)uff->valData(uff->elements()/2))<<endl;
    clog<<"ufping::floats> "<<*((float*)uff->valData(uff->elements()-1))<<endl;
    //clog<<"ufping::floats> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::floats> server did not return UFFloats?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}

UFProtocol* floatframes(UFClientSocket& _client, int cnt) {
  strstream sn;
  sn << "ufping::floatframes #"<<cnt<<" "<<ends;
  string name = sn.str();
  unsigned int nvals = cnt*200*100;

  float *vals = new (nothrow) float[ nvals ] ;
  if( NULL == vals )
	{
	clog << "Error: Unable to allocate memory\n" ;
	return NULL ;
	}

  for( unsigned int i = 0; i < (unsigned int) nvals; ++i ) {
    vals[i] = i % 256;
  }
  UFFloats tmp(name, vals, nvals, false);
  UFFloatFrames ping(tmp, 200, 100, cnt);

  clog<<"ufping::floatframes> length()= "<<ping.length()<<endl;

  clog<<"ufping::floatframes> send name= "<<ping.cname()<<", time= "<<ping.timestamp()<<endl;
  _client.send(&ping);

  delete[] sn.str();
  delete[] vals ;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( ufp == 0 )
	{
	return 0;
	}

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFFloatFrames *ufff = dynamic_cast<UFFloatFrames*>(ufp);
  if( ufff ) {
    clog<<"ufping::floatframes> received reply:\n name= "
	<<ufff->cname()<<", time= "<<ufff->timestamp()
	<<", elems= "<<ufff->elements()<<", nvals= "<<ufff->numVals()<<endl;
//    for( int i = 0; i < ufff->elements(); ++i ) {
//      clog<<"ufping::floatframes> "<<*((float*)ufff->valData(i))<<endl;
//    }
    //clog<<"ufping::floatframes> hit any key to continue..."<<endl;
    //getchar();  //so we can see data on screen
  }
  else {
    clog<<"ufping::floatframes> server did not return UFFloatFrames?, typ= "
	<<ufp->typeId()<<endl;
  }
  return ufp;
}

void usage( char** argv )
{
clog << "Usage: " << argv[ 0 ] << " [options] <server> <port>\n" ;
clog << "Options are any of the following:\n" ;
clog << " -a\tSend all objects\n" ;
clog << " -b\tSend UFBytes objects\n" ;
clog << " -c\tSend UFByteFrames objects\n" ;
clog << " -f\tSend UFFloats objects\n" ;
clog << " -g\tSend UFFloatFrames obejcts\n" ;
clog << " -i\tSend UFInts objects\n" ;
clog << " -h\tSend UFIntFrames objects\n" ;
clog << " -s\tSend UFStrings objects\n" ;
clog << " -t\tSend UFTimeStamp objects\n\n" ;
}

int main(int argc, char** argv)
{

  if( argc < 3 )
	{
	usage( argv ) ;
	return 0 ;
	}

  string host = argv[ argc - 2 ] ;
  for( size_t i = 0 ; i < strlen( argv[ argc - 1 ] ) ; i++ )
	{
	if( !isdigit( argv[ argc - 1 ][ i ] ) )
		{
		clog << "Inalid port number: " << argv[ argc - 1 ] << endl ; 
		return 0 ;
		}
	}
  int portNo = atoi( argv[ argc - 1 ] ) ;

  // This is a brilliant piece of coding I tell ya what -- NOT!
  bool useA = false ;
  bool useB = false ;
  bool useC = false ;
  bool useF = false ;
  bool useG = false ;
  bool useI = false ;
  bool useH = false ;
  bool useS = false ;
  bool useT = false ;

  // Parse command line args
  int c = EOF ;
  while( (c = getopt( argc, argv, "abcfghist" ) ) != EOF )
	{
	switch( c )
		{
		case 'a':	useA = true ; break ;
		case 'b':	useB = true ; break ;
		case 'c':	useC = true ; break ;
		case 'f':	useF = true ; break ;
		case 'g':	useG = true ; break ;
		case 'i':	useI = true ; break ;
		case 'h':	useH = true ; break ;
		case 's':	useS = true ; break ;
		case 't':	useT = true ; break ;
		}
	} // close while

  signal( SIGINT, sigHandler ) ;

  UFClientSocket _client;
  clog << "ufping> connecting to port # " << portNo << endl ;
  while( _client.connect( host.c_str(), portNo ) < 0 && keepRunning )
	{
	sleep( 10 ) ;
	}

  if( !keepRunning )
	{
	clog << "Aborting\n" ;
	return 0 ;
	}

  clog << "ufping> connected to port # " << portNo << endl ;

  int cnt = 650 ;
  UFProtocol* reply = NULL ;

  while( keepRunning )
	{
//        if( ++cnt > 1000 ) cnt = 1;

	if( useA )
		{
		reply = timestamp(_client, cnt); delete reply;
		reply = strings(_client, cnt); delete reply;
		reply = bytes(_client, cnt); delete reply;
		reply = byteframes(_client, cnt); delete reply;
		reply = ints(_client, cnt); delete reply;
		reply = intframes(_client, cnt); delete reply;
		reply = floats(_client, cnt); delete reply;
		reply = floatframes(_client, cnt); //delete reply;
		}
	else if( useT )
		{
		reply = timestamp(_client, cnt);
		}
	else if( useT )
		{
		reply = strings(_client, cnt);
		}
	else if( useB )
		{
		reply = bytes(_client, cnt);
		}
	else if( useC )
		{
		reply = byteframes(_client, cnt);
		}
	else if( useI )
		{
		reply = ints(_client, cnt);
		}
	else if( useH )
		{
		reply = intframes(_client, cnt);
		}
	else if( useF )
		{
		reply = floats(_client, cnt);
		}
	else if( useG )
		{
		reply = floatframes(_client, cnt);
		}

	sleep( 1 ) ;
	delete reply;
	reply = NULL ;

	} // close while

return 0 ;

}
