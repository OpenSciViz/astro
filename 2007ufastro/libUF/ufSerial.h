#if !defined(__UFSerial_h__)
#define __UFSerial_h__ "$Name:  $ $Id: ufSerial.h,v 0.0 2002/06/03 17:42:20 hon beta $"
#define __UFSerial_H__(arg) const char arg##Serial_h__rcsId[] = __UFSerial_h__;
#include "sys/types.h"

/* convenience func. run a UFAnnexIO child process that
creates the named pipe/fifo, reads from it, and writes
to stdout. stdout is redirected to file desc. returned here: */

extern pid_t ufOpenAnnexFifo(const char* fifoname, int port);
extern int ufCloseAnnexFifo(const char* fifoname, pid_t pid);

/******************************** serial ports i/o ***********************************/
extern int ufttyOpen(int portNo, int baud);
extern int ufttyClose(int portNo);
extern char ufttySend1(int portNo, char c); /* 0 == ttya, 1 == ttyb */
extern char* ufttySend(int portNo, char* s);
extern char ufttyRecv1(int portNo);
extern char* ufttyRecv(int portNo);
extern char* ufttyGatirSend(int ttyNo, char* s);
extern char* ufttyGatirRecv(int ttyNo);

#endif /* __UFSerial_h__ */
