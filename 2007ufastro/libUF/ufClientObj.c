#if !defined(__UFClientObj_c__)
#define __UFClientObj_c__ "$Name:  $ $Id: ufClientObj.c,v 0.6 2003/12/30 00:48:07 varosi beta $"
static const char rcsIdUFClientObj[] = __UFClientObj_c__;

#include "ufClient.h"
__UFClient_H__(ClientObj_c);

#include "ufLog.h"
__UFLog_H__(ClientObj_c);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 

#define  MAXNAMLEN  512
#define  INT_MIN   (-2147483647-1)

#define isnanf(x)       (((*(long *)&(x) & 0x7f800000L) == 0x7f800000L) && \
                            ((*(long *)&(x) & 0x007fffffL) != 0x00000000L))
#else

#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "inttypes.h"
#include "math.h"
#ifdef SOLARIS
  #include "ieeefp.h" /* isnanf */
#endif
#include "sys/uio.h"
#include "sys/stat.h"
#include "string.h"

#endif

static char _UFerrmsg[MAXNAMLEN + 1];

static int _ufmin2i(int a, int b) { return ((a < b) ? a : b); }

/********* UF Protocol Header functions ****************/

int ufHeaderLength( const ufProtocolHeader* hdr )
{
  return( sizeof(ufProtocolHeader) - 1
        - sizeof(hdr->name) + sizeof(int) + strlen(hdr->name) );
}/*--------------------------------------------------------------------------*/

int ufHeaderSend(int socFd, ufProtocolHeader* hdr)
{
  int nbsent= 0;
  
  nbsent += ufSendInt(socFd, hdr->length);
  nbsent += ufSendInt(socFd, hdr->type);
  nbsent += ufSendInt(socFd, hdr->elem);
  nbsent += ufSendShort(socFd, hdr->seqCnt);
  nbsent += ufSendShort(socFd, hdr->seqTot);
  nbsent += ufSendFloat(socFd, hdr->duration);
  
  /* fixed-length timestamp */
  nbsent += ufSend(socFd,(const unsigned char*)hdr->timestamp,
		   sizeof(hdr->timestamp)-1);

  /* variable length name (sends int length first) */
  nbsent += ufSendCstr(socFd, hdr->name);

  return nbsent;
}/*--------------------------------------------------------------------------*/

int ufHeaderSendAgent( const char* agent, ufProtocolHeader* hdr, int* socFd )
{
  /* socket FD is returned for later use */

  if((*socFd = ufAgentSocket( agent )) > 0) return ufHeaderSend( *socFd, hdr );

  sprintf(_UFerrmsg,"ufHeaderSendAgent> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
}/*--------------------------------------------------------------------------*/

int ufHeaderRecv(int socFd, ufProtocolHeader* hdr)
{
  int nb= 0;

  hdr->length = ufRecvInt(socFd);

  if( hdr->length > 0 )
    nb += sizeof(int);
  else
    return hdr->length;

  hdr->type = ufRecvInt(socFd); if( hdr->type > INT_MIN ) nb += sizeof(int);
  hdr->elem = ufRecvInt(socFd); if( hdr->elem >= 0 ) nb += sizeof(int);
  hdr->seqCnt = ufRecvShort(socFd); if( hdr->seqCnt >= 0 ) nb += sizeof(short);
  hdr->seqTot = ufRecvShort(socFd); if( hdr->seqTot >= 0 ) nb += sizeof(short);
  hdr->duration = ufRecvFloat(socFd);
  if( ! isnanf(hdr->duration) ) nb += sizeof(float);
  
  nb += ufRecv(socFd, hdr->timestamp, sizeof(hdr->timestamp)-1);
  nb += ufRecvCstr(socFd, hdr->name, sizeof(hdr->name));

  return nb;
}/*-------------------------------------------------------------------------*/

int ufHeaderRecvAgent(const char* agent, ufProtocolHeader* hdr, int* socFd )
{
  /* socket FD is returned for later use */

  if((*socFd = ufAgentSocket( agent )) > 0) return ufHeaderRecv( *socFd, hdr );

  sprintf(_UFerrmsg,"ufHeaderRecvAgent> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
}/*--------------------------------------------------------------------------*/

/***************** UFStrings: ***************/

char** ufStringsRecv( int socFd, int* Nstrp, ufProtocolHeader* hdr, int* nbtp )
{
  int nb=0, nbt=0, cnt=0, Nstrings;
  char **stp, **strings;
  
  if( (nb = ufHeaderRecv( socFd, hdr )) <= 0 ) {
      _uflog("ufStringsRecv> failed to recieve header! (zero bytes recvd)");
      return( NULL );
  }

  nbt += nb;
  *Nstrp = 0;
  *nbtp = nbt;
  Nstrings = hdr->elem;
  if( (Nstrings <= 0) || (nb <= 0) ) return( NULL );   /* case of a TimeStamp hdr */

  /* alloc array of pointers to strings */
  strings = (char** ) calloc( Nstrings, sizeof(char*) );
  stp = strings;

  if( strings == NULL ) {
      sprintf(_UFerrmsg, "ufStringsRecv> Memory allocation failure" );
      _uflog(_UFerrmsg);
      return( NULL );
  }
  
  do {	nb = ufRecvNewCstr( socFd, stp );
  	nbt += nb;
  	stp++;
  	cnt++; }  while( (cnt < Nstrings) && (nb > 0) );

  if( cnt < Nstrings || nbt != hdr->length )
    {
     _uflog("ufStringsRecv> received less than expected:");
     sprintf( _UFerrmsg,
        "ufStringsRecv> nbrecv=%d, length=%d, type=%d, nstrecv=%d, elem=%d, name=%s",
                        nbt, hdr->length, hdr->type, cnt, hdr->elem, hdr->name );
     _uflog(_UFerrmsg);
    }

  *Nstrp = cnt;
  *nbtp = nbt;
  return( strings );
}/*--------------------------------------------------------------------------*/

char** ufStringsRecvAgent( const char* agent, int* Nstrp,
                           ufProtocolHeader* hdr, int* nbtp ) {
  int socFd;

  *Nstrp = 0;
  
  if( (socFd = ufAgentSocket( agent )) > 0 )
  {
  	return ufStringsRecv( socFd, Nstrp, hdr, nbtp );
  }
  sprintf(_UFerrmsg,"ufStringsRecvAgent> unknown agent name: %s.", agent);
  _uflog(_UFerrmsg);
  return NULL;
}/*--------------------------------------------------------------------------*/

/* send UFStrings: */

int ufStringsSendAgent( const char* agent, char** strings,
			int Nstrings, const char* name )
{
  int socFd;

  if( (socFd = ufAgentSocket( agent )) > 0 )
  {
  	return ufStringsSend( socFd, strings, Nstrings, name );
  }
  sprintf(_UFerrmsg,"ufStringsSendAgent> unknown agent name: %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
}/*--------------------------------------------------------------------------*/

int ufStringsSend( int socFd, char** strings, int Nstrings, const char* name )
{
  int i, nsent=0, nb, nbh, nbst=0, slen=0;
  char** stp = strings;
  static ufProtocolHeader hdr;

  strcpy( hdr.name, name );
  hdr.seqCnt = 1;
  hdr.seqTot = 1;
  hdr.type = 1;
  hdr.elem = Nstrings;
  nbh = ufHeaderLength( &hdr );
  hdr.length = nbh + Nstrings * sizeof(int);

/* add length of strings: */
  for( i=0; i<Nstrings; i++ ) slen += strlen( *(stp++) );
  hdr.length += slen;

  nb = ufHeaderSend( socFd, &hdr );

  if( nb <= 0 ) {
  	_uflog("ufStringsSend> failed to send header! (zero bytes sent)");
  	return nb;
  }

  if( nb < nbh ) {
    _uflog("ufStringsSend> failed to send complete header ?");
    sprintf( _UFerrmsg,
            "ufStringsSend> hdr: nbsent=%d, nbhdr=%d, type=%d, elem=%d, name=%s",
                                 nb, nbh, hdr.type, hdr.elem, hdr.name );
    _uflog(_UFerrmsg);
  }
  
  do {	nb = ufSendCstr( socFd, *strings );
  	if( nb >= 4 ) nbst += (nb-4);
  	nsent++;
	strings++;
  }  while( (nsent < Nstrings) && (nb > 0) );

  return nbst;
}/*--------------------------------------------------------------------------*/

/* ufRequest:
 *    request = "buffer name" for frame (ufInts) from a buffer,
 * or request = "BN" for buffer names (ufStrings),
 * or request = "FC" for ufFrameConfig,
 * or request = "OC" for ufObsConfig.
 * or request = "PC" for ufPixConfig.
 * handshake: client sends ProtocolHeader to server
 *       with request string in "name" field, requesting a data object.
 * server replies with data object: ProtoHdr and data array (e.g. ufFetchFrame).
 */

int ufRequest( const char* agent, const char* request )
{
  int nb, socFd;
  static ufProtocolHeader hdr;
  
  strcpy( hdr.name, request );
  hdr.seqCnt = 1;
  hdr.seqTot = 1;
  hdr.type = 0;
  hdr.elem = 0;
  hdr.length = ufHeaderLength( &hdr );
  
  nb = ufHeaderSendAgent( agent, &hdr, &socFd );
  
  if( nb < hdr.length ) {
    sprintf(_UFerrmsg, "ufRequest> failed to send complete header: nb=%d < %d",
    							nb, hdr.length);
    _uflog(_UFerrmsg);
    sprintf(_UFerrmsg, "ufRequest> request for: %s, from agent: %s, failed!",
    							request, agent );
    _uflog(_UFerrmsg);
  }
  return nb;
}/*--------------------------------------------------------------------------*/

/* recv ints frame: bufsiz = # ints that buffer will hold */

int ufIntsRecv( const char* agent, int* bufptr, int bufsiz,
                                               ufProtocolHeader* hdr ) {
  int nb=0, socFd, nrecvd, ntorecv, *tmpbuf;

  memset( hdr, 0, sizeof(ufProtocolHeader) );  /* clear all */

  nb = ufHeaderRecvAgent( agent, hdr, &socFd );  /* also gets socket FD */
  
  if( nb <= 0 ) {
  	_uflog("ufIntsRecv> failed to recieve header!");
  	sprintf(_UFerrmsg,"ufIntsRecv> nbrecv=%d, type:%d, elem:%d, name:%s",
                                       nb, hdr->type, hdr->elem, hdr->name);
  	_uflog(_UFerrmsg);
    	return -1;
  }
  
  if( hdr->elem <= 0 ) {
  	sprintf(_UFerrmsg,"ufIntsRecv> no data to recv: elem=%d", hdr->elem);
  	_uflog(_UFerrmsg);
  	return hdr->elem;
  }
  
  ntorecv = _ufmin2i( bufsiz, hdr->elem );

  nrecvd = ufRecvInts( socFd, bufptr, ntorecv );

  if( nrecvd < ntorecv ) {
    ufSleep( 1.0 );
    if( nrecvd < 0 ) nrecvd = 0;
    sprintf(_UFerrmsg,"ufIntsRecv> attempting to recv the remaining %d elements.",ntorecv-nrecvd);
    _uflog(_UFerrmsg);
    nrecvd += ufRecvInts( socFd, bufptr+nrecvd, ntorecv-nrecvd );
    if( nrecvd < ntorecv ) {
      _uflog("ufIntsRecv> Failed to recv the remaining elements!");
      return nrecvd;
    }
  }

  if( bufsiz < hdr->elem ) {
    sprintf(_UFerrmsg,"ufIntsRecv> bufsiz=%d not enough to hold elem=%d", bufsiz, hdr->elem );
    _uflog(_UFerrmsg);
    _uflog("ufIntsRecv> receiving and discarding extra elements!");
    tmpbuf = (int* )malloc( (hdr->elem - bufsiz)*sizeof(int) );
    nb = ufRecvInts( socFd, tmpbuf, hdr->elem - bufsiz );
    free( tmpbuf );
  }
  
  return nrecvd;
}/*--------------------------------------------------------------------------*/

/* send int frame */

int ufIntsSend( const char* agent, int* bufptr, int bufsiz, const char* fname )
{
  int nb=0, nsent=0, nbh, socFd;
  static ufProtocolHeader hdr;

  strcpy( hdr.name, fname );
  hdr.seqCnt = 1;
  hdr.seqTot = 1;
  hdr.type = 4;
  hdr.elem = bufsiz;
  nbh = ufHeaderLength( &hdr );
  hdr.length = nbh + bufsiz * sizeof(int);

  sprintf(_UFerrmsg,"ufIntsSend> to agent: %s,  fname: %s", agent, fname );
  _uflog(_UFerrmsg);

  nb = ufHeaderSendAgent( agent, &hdr, &socFd );
  
  if( nb < nbh ) {
  	_uflog("ufIntsSend> failed to send complete header?");
     sprintf(_UFerrmsg,"ufIntsSend> sent header: nbsent=%d, nbhdr=%d", nb, nbh);
     _uflog(_UFerrmsg);
  }
  
  if( nb <= 0 ) {
  	_uflog("ufIntsSend> failed to send header!");
  	return nb;
  }

  nsent = ufSendInts( socFd, bufptr, bufsiz );
  
  if( nsent < hdr.elem ) {
  	_uflog("ufIntsSend> failed to send all values!");
  	sprintf(_UFerrmsg,"ufIntsSend> nsent=%d, elem=%d, length=%d",
                                        nsent, hdr.elem, hdr.length );
  	_uflog(_UFerrmsg);
  }
  return nsent;
}/*---------------------------------------------------------------------------*/

/****** Frame and Obs config UF protocol objects **************************/

int ufFrameConfRecv( const char* agent, ufFrameConfig* uffc )
{
  int nb, nrecv, socFd, nextra, *tmpbuf;
  int nelem = ( sizeof(ufFrameConfig) - sizeof(ufProtocolHeader) )/sizeof(int);

  memset( uffc, 0, sizeof(ufFrameConfig) );   /* clear all */

  nb = ufHeaderRecvAgent( agent, &uffc->hdr, &socFd );  /*also returns socket FD*/

  if( nb <= 0 ) {
    if( nb < 0 ) _uflog("ufFrameConfRecv> failed to recieve header!");
    return nb;
  }
  
  if( uffc->hdr.elem <= 0 ) {
    _uflog("ufFrameConfRecv> no data to recv:");
    sprintf(_UFerrmsg,"ufFrameConfRecv> nbrecv=%d, type:%d, elem:%d, name:%s",
	                  nb, uffc->hdr.type, uffc->hdr.elem, uffc->hdr.name);
    _uflog(_UFerrmsg);
    return uffc->hdr.elem;
  }
  
  if( nelem > uffc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufFrameConfRecv> object has # elements = %d < %d expected",
	    uffc->hdr.elem, nelem);
    _uflog(_UFerrmsg);
    nelem = uffc->hdr.elem;
  }

  nrecv = ufRecvInts( socFd, &uffc->w, nelem );   /* place data after header */

  if( nelem < uffc->hdr.elem ) {
    nextra = uffc->hdr.elem - nelem;
    sprintf(_UFerrmsg, "ufFrameConfRecv> recv and discard %d extra elements...", nextra);
    _uflog(_UFerrmsg);
    tmpbuf = (int* )malloc( nextra * sizeof(int) );
    nrecv += ufRecvInts( socFd, tmpbuf, nextra );
    free( tmpbuf );
  }

  if( nrecv < uffc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufFrameConfRecv> incomplete Recv: nrecv: %d < elem: %d",
    							nrecv, uffc->hdr.elem );
    _uflog(_UFerrmsg);
  }
  return nb + sizeof(int)*nrecv;
}/*--------------------------------------------------------------------------*/

int ufFrameConfSend( const char* agent, ufFrameConfig* uffc )
{
  int nb, nsent, socFd;

  uffc->hdr.seqCnt = 1;
  uffc->hdr.seqTot = 1;
  uffc->hdr.type = 9;
  uffc->hdr.elem = (sizeof(ufFrameConfig)-sizeof(ufProtocolHeader))/sizeof(int);
  uffc->hdr.length = ufHeaderLength( &uffc->hdr ) + uffc->hdr.elem*sizeof(int);
  
  nb = ufHeaderSendAgent( agent, &uffc->hdr, &socFd );

  sprintf(_UFerrmsg, "ufFrameConfSend> hdr: nbsent=%d, type:%d, elem:%d, name:%s",
                            nb, uffc->hdr.type, uffc->hdr.elem, uffc->hdr.name);
  _uflog(_UFerrmsg);

  if( nb <= 0 ) {
  	_uflog("ufFrameConfSend> failed to send header!");
  	return nb;
  }
  nsent = ufSendInts( socFd, &uffc->w, uffc->hdr.elem );

  if( nsent < uffc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufFrameConfSend> incomplete Send: nsent: %d != elem: %d",
                                                        nsent, uffc->hdr.elem );
    _uflog(_UFerrmsg);
  }
  return nb + sizeof(int)*nsent;
}/*--------------------------------------------------------------------------*/

int ufObsConfRecv( const char* agent, ufObsConfig* ufoc, short** obsFlags )
{
  int nrecv= 0, socFd, Nframe;
  char *info, seps[]="|", name[83], *rdout, *dLab;
  ufProtocolHeader hdr;

  if( ufoc == NULL ) {
    _uflog("ufObsConfRecv> NULL pointer to ObsConf structure!");
    return -1;
  }

  if( (socFd = ufAgentSocket( agent )) <= 0 ) {
  	sprintf(_UFerrmsg,"ufObsConfRecv> unknown agent name: %s.", agent);
 	_uflog(_UFerrmsg);
	return socFd;
  }
  
  nrecv = ufHeaderRecv( socFd, &hdr );
  ufoc->hdr = hdr;   /* set new header, even if errors, just to see */

  sprintf(_UFerrmsg, "ufObsConfRecv> # elem=%d, name=%s", hdr.elem, hdr.name);
  _uflog(_UFerrmsg);

  if( nrecv <= 0 ) {
    sprintf(_UFerrmsg, "ufObsConfRecv> failed to recv hdr: nrecv=%d !", nrecv );
    _uflog(_UFerrmsg);
    return nrecv;
  }

  if( hdr.type != 8 ) {
    sprintf(_UFerrmsg,"ufObsConfRecv> bad protocol hdr: type=%d != 8",hdr.type);
    _uflog(_UFerrmsg);
    return -1;
  }

  if( hdr.elem <= 0 ) {
    sprintf(_UFerrmsg, "ufObsConfRecv> bad protocol header: elem=%d", hdr.elem);
    _uflog(_UFerrmsg);
    return hdr.elem;
  }

  strcpy( name, hdr.name );
  info = strstr( name, "||" );
  
  if( info != NULL ) {
    ufoc->nodBeams = atoi( strtok( info+2, seps ) );
    ufoc->chopBeams = atoi( strtok( NULL, seps ) );
    ufoc->saveSets = atoi( strtok( NULL, seps ) );
    ufoc->nodSets = atoi( strtok( NULL, seps ) );
    ufoc->coaddsPerFrm = atoi( strtok( NULL, seps ) );
    memset( ufoc->readoutMode, 0, 8 );
    rdout = strtok( NULL, seps );
    dLab = strtok( NULL, seps );
    if( rdout > 0 && dLab > 0 )
      memcpy( ufoc->readoutMode, rdout, dLab-rdout-1 );
    memset( ufoc->dataLabel, 0, 40 );
    if( dLab > 0 )
      memcpy( ufoc->dataLabel, dLab, 39 );  /* data Label comes after "||" */
  }

  Nframe = ufoc->nodBeams * ufoc->chopBeams * ufoc->saveSets * ufoc->nodSets;

  if( Nframe != hdr.elem ) {
    sprintf(_UFerrmsg, "ufObsConfRecv> inconsistency: Nframe:%d != elem:%d",
                                                      Nframe, hdr.elem);
    _uflog(_UFerrmsg);
  }

  *obsFlags = (short *)calloc( hdr.elem, sizeof(short) );
  nrecv = ufRecvShorts( socFd, *obsFlags, hdr.elem );
  
  if( nrecv != hdr.elem ) {
    sprintf(_UFerrmsg, "ufObsConfRecv> nrecv: %d != elem: %d !", nrecv, hdr.elem);
    _uflog(_UFerrmsg);
  }

  return nrecv;
}/*--------------------------------------------------------------------------*/

int ufObsConfSend( const char* agent, ufObsConfig* ufoc, short* obsFlags )
{
  int nb, nsent, socFd, Nframe;

  if( (socFd = ufAgentSocket( agent )) <= 0 ) {
  	sprintf(_UFerrmsg,"ufObsConfSend> unknown agent name: %s.", agent);
 	_uflog(_UFerrmsg);
	return socFd;
  }
  Nframe = ufoc->nodBeams * ufoc->chopBeams * ufoc->saveSets * ufoc->nodSets;

  if( Nframe != ufoc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufObsConfSend> inconsistency: Nframe:%d != elem:%d",
                                                      Nframe, ufoc->hdr.elem);
    _uflog(_UFerrmsg);
  }                    /* ufoc->hdr.elem must = # shorts in obsFlags array */
  
  sprintf(ufoc->hdr.name, "||%d|%d|%d|%d|%d|%s||%s",
	  ufoc->nodBeams, ufoc->chopBeams, ufoc->saveSets, ufoc->nodSets,
	  ufoc->coaddsPerFrm, ufoc->readoutMode, ufoc->dataLabel );

  ufoc->hdr.seqCnt = 1;
  ufoc->hdr.seqTot = 1;
  ufoc->hdr.type = 8;
  ufoc->hdr.length = ufHeaderLength(&ufoc->hdr) + ufoc->hdr.elem*sizeof(short);

  nb = ufHeaderSend( socFd, &ufoc->hdr );

  sprintf(_UFerrmsg, "ufObsConfSend> hdr: nbsent=%d, type:%d, elem:%d, name:%s",
                            nb, ufoc->hdr.type, ufoc->hdr.elem, ufoc->hdr.name);
  _uflog(_UFerrmsg);

  if( nb <= 0 ) {
    _uflog("ufObsConfSend> failed to send header!");
    return nb;
  }

  nsent = ufSendShorts( socFd, obsFlags, ufoc->hdr.elem );
  
  if( nsent != ufoc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufObsConfSend> nsent: %d != elem: %d !",
                                       nsent, ufoc->hdr.elem);
    _uflog(_UFerrmsg);
  }

  return nsent;
}/*--------------------------------------------------------------------------*/

#endif /* __UFClientObj_c__ */
