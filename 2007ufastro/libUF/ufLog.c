#if !defined(__UFLog_c__)
#define __UFLog_c__ "$Name:  $ $Id: ufLog.c,v 0.0 2002/06/03 17:42:20 hon beta $"
static const char rcsIdUFLog[] = __UFLog_c__;

#include "ufLog.h"
__UFLog_H__(Log_c);

#include "stdio.h"

#if defined(_IDL_)
/* for IDL messages: */
#include "idl/external/export.h"
void _uflog(const char* msg) {
  /* if called from IDL, must log error messages this way: */
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, msg);
  return;
}
#else
void _uflog(const char* msg) {
  fprintf(stderr,"%s\n",msg);
}

#endif /* _IDL_ */
#endif /* __UFLog_c__ */
