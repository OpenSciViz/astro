#include "ufClient.h" /* all UFLib socket transaction functions */
#include "ufLog.c" /* log (info/error messages, status & alarms?) functions */
#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "inttypes.h"
#include "math.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "string.h"

extern int keepLooking( char** bufferNames, int Nbufs ); /* gives list of buffers to display */
extern void displayBuffer( char* bufName, int* frame );  /* method to display a frame */

void updateBufferDisplays( char* host, int portNo )
{
  float timeout = 9.0;
  int i, sockFrame, sockNotify, frameSize, Nbufs;
  char** bufferNames;

  static ufProtocolHeader ufphdr;
  static ufObsConfig ufoc;
  static ufFrameConfig uffc;
  int* frameBuffer = 0;

  if( portNo <= 0 ) portNo = 52000;
  sockFrame = ufConnectAgent( "frame", host, portNo );
  sockNotify = ufConnectAgent( "FrameNotify", host, portNo );

  ufSetTimeout( timeout );

  if( ufRequest("frame","FC") > 0 )
    {
      if( ufFrameConfRecv( "frame", &uffc ) > 0 )
	{
	  frameSize = uffc.w *  uffc.h;
	  frameBuffer = (int* )calloc( frameSize, sizeof(int) );
	  printf("Allocated frame buffer of size %d ints",frameSize);
	}
      else printf("FAILED to Recv first FrameConfig from FrameAcqServer.");    
    }
  else printf("ERROR: Request for FrameConfig from FrameAcqServer FAILED!");

  if( ufRequest("FrameNotify","NOTIFY") <= 0 ) {
    printf("ERROR: Request for notification stream from FrameAcqServer FAILED!");
    return;
  }

  while( keepLooking( bufferNames, Nbufs ) )
    {
      if( ufFrameConfRecv( "FrameNotify", &uffc ) > 0 )
	{
	  for( i=0; i<Nbufs; i++ )
	    {
	      if( strstr( bufferNames[i], uffc.hdr.name ) > 0 ) 
		{
		  if( ufRequest( "frame", bufferNames[i] ) > 0 )
		    {
		      if( ufFrameConfRecv( "frame", &uffc ) > 0 )
			{
			  if( uffc.hdr.type == 9 )
			    {
			      if( ufIntsRecv("frame", frameBuffer, frameSize, &ufphdr) > 0 )
				{
				  displayBuffer( bufferNames[i], frameBuffer );
				}
			    }
			}
		    }
		}
	    }
	}
      else printf("FAILED to Recv FrameConfig from notification stream...");
    }

  if( ufRequest("FrameNotify","NONOTIFY") <= 0 ) {
    printf("ERROR on Request to stop notification stream from FrameAcqServer!");
    return;
  }
}
