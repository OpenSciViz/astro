/*--------------------------------------------------------------------------*/

int ufRecvAgent(const char* agent, unsigned char* rbuf, int len)
{
  int* socFd= 0;
  int i= _UFStandardAgentList_;
  
  while( --i >= 0 ) {
    if( strcmp(agent, _theAgentNames[i]) == 0 ) {
      socFd = _theAgentSockFdVec[i];
      return ufRecv(*socFd, rbuf, len);
    }
  }
  sprintf(_UFerrmsg,"ufRecvAgent> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
} /* RecvAgent:  raw bytes recv */
/*--------------------------------------------------------------------------*/

unsigned short ufRecvShortAgent(const char* agent) { /* ntohs */
  unsigned short val=0;
  int nb = ufRecvAgent(agent, (unsigned char*) &val, sizeof(val));
  if( nb < sizeof(val) )
    {
      _uflog("ufRecvShortAgent> failed to recv value");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = ntohs((const unsigned short)val);
#endif
  return val;
} /* short recvAgent */
/*--------------------------------------------------------------------------*/

int ufRecvShortsAgent(const char* agent, short* buf, int cnt) { /* ntohs */
  int nb = ufRecvAgent(agent, (unsigned char*) buf, cnt*sizeof(short));

  if( nb < cnt*sizeof(short) )
    {
      _uflog("ufRecvShortsAgent> failed to recv all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while( --cnt >= 0 ) 
    buf[cnt] = ntohs((const unsigned short)buf[cnt] );
#endif

  return (nb/sizeof(short));
} /* shorts recvAgent */
/*--------------------------------------------------------------------------*/

int ufRecvIntAgent(const char* agent) { /* ntohl */
  int val=0;
  int nb = ufRecvAgent(agent, (unsigned char*) &val, sizeof(val));
  if( nb < sizeof(val) )
    {
      _uflog("ufRecvIntAgent> failed to recv value");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (int) ntohl((const unsigned long) val);
#endif

  return val;
} /* int recvAgent */
/*--------------------------------------------------------------------------*/

int ufRecvIntsAgent( const char* agent, int* buf, int cnt )
{
  int nb = ufRecvAgent( agent, (unsigned char*) buf, cnt*sizeof(int) );

  if( nb < cnt*sizeof(int) )
    {
      _uflog("ufRecvIntsAgent> failed to recv all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while( --cnt >= 0 ) 
    buf[cnt] = ntohl((const unsigned long) buf[cnt] );
#endif

  if( nb < 0 )
  	return nb;
  else
  	return (nb/sizeof(int));
} /* ints recvAgent */
/*--------------------------------------------------------------------------*/

float ufRecvFloatAgent(const char* agent) { /* ntohl */
  float val= 0;
  int nb = ufRecvAgent(agent, (unsigned char*) &val, sizeof(val));
  if( nb < sizeof(val) )
    {
      _uflog("ufRecvFloatAgent> failed to recv value");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (int) ntohl( (unsigned long) val ) ;
#endif

  return val;
} /* float recvAgent */
/*--------------------------------------------------------------------------*/

int ufRecvFloatsAgent(const char* agent, float* buf, int cnt) { /* ntohl */
  int nb = ufRecvAgent(agent, (unsigned char*) buf, cnt*sizeof(float));

  if( nb < cnt*sizeof(float) )
    {
      _uflog("ufRecvFloatsAgent> failed to recv all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  while( --cnt >= 0 ) 
    buf[ cnt ] = (float) ntohl( (unsigned long) buf[ cnt ] ) ;
#endif

  return (nb/sizeof(float));
} /* floats recvAgent */
/*--------------------------------------------------------------------------*/

int ufSendAgent(const char* agent, const unsigned char* sndbuf, int nb)
{
  int* socFd= 0;
  int i= _UFStandardAgentList_;
  
  while( --i >= 0 ) {
    if( strcmp(agent, _theAgentNames[i]) == 0 ) {
      socFd = _theAgentSockFdVec[i];
      return ufSend(*socFd, sndbuf, nb);
    }
  }
  sprintf(_UFerrmsg,"ufSendAgent> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
} /* raw byte array send to Agent*/
/*--------------------------------------------------------------------------*/

int ufSendShortAgent(const char* agent, short val) { /* htons */
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (unsigned short) htons((const unsigned short) val);
#endif
  return ufSendAgent(agent, (unsigned char*)&val, sizeof(val));
} /* send short to Agent*/
/*--------------------------------------------------------------------------*/

int ufSendShortsAgent(const char* agent, short* buf, int cnt) { /* htons */
  int nb, nbc = cnt*sizeof(short);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while( --icnt >= 0 ) 
    buf[icnt] = htons((const unsigned short)buf[icnt] );
#endif

  nb = ufSendAgent(agent, (const unsigned char*) buf, nbc);
  if( nb < nbc )
    {
      _uflog("ufSendShortsAgent> failed to send all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while( --icnt >= 0 ) 
    buf[icnt] = ntohs((const unsigned short)buf[icnt] );
#endif
  return (nb/sizeof(short));
} /* send shorts to Agent*/
/*--------------------------------------------------------------------------*/

int ufSendIntAgent(const char* agent, int val) { /* htonl */
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (int) htonl((const unsigned long)val);
#endif
  return ufSendAgent(agent, (const unsigned char*)&val, sizeof(val));
} /* send int to Agent */
/*--------------------------------------------------------------------------*/

int ufSendIntsAgent(const char* agent, int* buf, int cnt) { /* htonl */
  int nb, nbc = cnt*sizeof(int);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while( --icnt >= 0 ) 
    buf[icnt] = htonl((const unsigned long)buf[icnt] );
#endif

  nb = ufSendAgent(agent, (const unsigned char*) buf, nbc);
  
  if( nb < nbc ) _uflog("ufSendIntsAgent> failed to send all values");

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while( --icnt >= 0 ) 
    buf[icnt] = ntohl((const unsigned long)buf[icnt] );
#endif
  return (nb/sizeof(int));
} /* send ints */
/*--------------------------------------------------------------------------*/

int ufSendFloatAgent(const char* agent, float val) { /* htonl */
#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  val = (float) htonl( (unsigned long) val ) ;
#endif
  return ufSendAgent(agent, (const unsigned char*)&val, sizeof(val));
} /* send float to Agent */
/*--------------------------------------------------------------------------*/
  
int ufSendFloatsAgent(const char* agent, float* buf, int cnt) { /* htonl */
  int nb, nbc = cnt*sizeof(int);

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  int icnt = cnt;
  while( --icnt >= 0 ) 
    buf[ icnt ] = (float) htonl( (unsigned long) buf[ icnt ] ) ;
#endif

  nb = ufSendAgent(agent, (const unsigned char*) buf, nbc);
  if( nb < nbc )
    {
      _uflog("ufSendFloatsAgent> failed to send all values");
    }

#if !defined(sparc) || defined(i386) || defined(i486) || defined(i586) || defined(i686) || defined(k6) || defined(k7)
  icnt = cnt;
  while( --icnt >= 0 ) 
    buf[ icnt ] = (float) ntohl( (unsigned long) buf[ icnt ] ) ;
#endif
  return (nb/sizeof(float));
} /* send floats to agent */
}/*--------------------------------------------------------------------------*/

/* this is old version of ufRecvObsConf which reallocs mem for obsflags: */
/* first, struct alloc support: */

static ufObsConfig* _allocObsConfig(int flagCnt) {
  int sz = sizeof(ufObsConfig) + sizeof(short)*(flagCnt - 1);
  ufObsConfig* ufoc = (ufObsConfig*) malloc(sz);
  return ufoc;
}/*--------------------------------------------------------------------------*/

int ufRecvObsConf( const char* agent, ufObsConfig** ufop )
{
  int nrecv= 0, socFd, flagCnt;
  ufProtocolHeader hdr;
  ufObsConfig* ufoc = *ufop;

  if( (socFd = ufAgentSocket( agent )) <= 0 ) {
  	sprintf(_UFerrmsg,"ufRecvObsConf> unknown agent name: %s.", agent);
 	_uflog(_UFerrmsg);
	return socFd;
  }
  
  nrecv = ufRecvHeader( socFd, &hdr );

  sprintf(_UFerrmsg, "ufRecvObsConf> elem:= %d, name= %s", hdr.elem, hdr.name);
  _uflog(_UFerrmsg);

  if( nrecv <= 0 ) {
    sprintf(_UFerrmsg, "ufRecvObsConf> failed recv hdr: nrecv=%d !", nrecv  );
    _uflog(_UFerrmsg);
    return nrecv;
  }

  if( hdr.elem <= 0 ) {
    sprintf(_UFerrmsg, "ufRecvObsConf> bad hdr: elem: %d", hdr.elem  );
    _uflog(_UFerrmsg);
    return hdr.elem;
  }

  if( ufoc > 0 )
	flagCnt = ufoc->hdr.elem;
  else 
  	flagCnt = -1;
  
  if( flagCnt >= hdr.elem ) { /* clear & re-use existing space */
    memset( ufoc->obsFlags, 0, flagCnt*sizeof(short) );
  }
  else {       /* free & reallocate space */
    free(ufoc);
    *ufop = ufoc = _allocObsConfig( hdr.elem );
  }

  ufoc->hdr = hdr;   /* set new header */
  ufoc->nodBeams = ufRecvInt(socFd);
  ufoc->chopBeams = ufRecvInt(socFd);
  ufoc->saveSets = ufRecvInt(socFd);
  ufoc->nodSets = ufRecvInt(socFd);
  
  nrecv = ufRecvShorts( socFd, (short*)&ufoc->obsFlags, ufoc->hdr.elem );
  
  if( nrecv < ufoc->hdr.elem ) {
    sprintf(_UFerrmsg, "ufRecvObsConf> nrecv: %d != elem: %d !",
                                       nrecv, ufoc->hdr.elem);
    _uflog(_UFerrmsg);
  }
  return nrecv;
}/*--------------------------------------------------------------------------*/

int ufFetchConfig(const char* agent, ufFrmConfig* uffc, ufObsConfig** ufoc) 
{
  int nb, nbr=0;
  
  nb = ufRequest( agent, "FC" );
  nbr += nb;
  if( nb <= 0 ) return nb;

  nb = ufRecvFrmConf(agent, uffc);
  nbr += nb;
  if( nb <= 0 ) return nb;

  nb = ufRequest( agent, "OC" );
  nbr += nb;
  if( nb <= 0 ) return nb;
  
  nb = ufRecvObsConf(agent, ufoc);
  nbr += nb;
  if( nb <= 0 ) return nb;

  return nbr;
}
