#if !defined(__UFClientAgent_c__)
#define __UFClientAgent_c__ "$Name:  $ $Id: ufClientAgent.c,v 0.2 2003/12/30 00:57:22 varosi beta $"
static const char rcsIdUFClientAgent[] = __UFClientAgent_c__;

#include "ufClient.h"
__UFClient_H__(ClientAgent_c);

#include "ufLog.h"
__UFLog_H__(ClientAgent_c);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 

#define  MAXNAMLEN  512

#else

#include "stdlib.h"
#include "time.h"
#include "ctype.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "inttypes.h"
#include "math.h"
#include "sys/uio.h"
#include "sys/stat.h"
#include "string.h"

#endif

static char _UFerrmsg[MAXNAMLEN + 1];

/*--------------------------------------------------------------------------*/

#define _UFmaxAgentList_ 100

/* pointers to all open sockets */
static int _theAgentSockets[_UFmaxAgentList_];

/* pointers to ports for each agent */
static int _theAgentPorts[_UFmaxAgentList_];

/* pointers to hostnames for each agent */
static char* _theHostnames[_UFmaxAgentList_];

/* pointers to all agent names */
static char* _theAgentNames[_UFmaxAgentList_];

static int _numAgents = 0;

/******************************** agent/service info *************************/

char** ufAgents( int* Nagents )
{
  *Nagents = _numAgents;
  return _theAgentNames;
}
/*--------------------------------------------------------------------------*/

int ufServices(char** infolist)
{
  *infolist = "TBD";
  return _numAgents;
}
/*--------------------------------------------------------------------------*/

int ufCloseAgent(const char* agent)
{
  int status = 0, i= _numAgents;
  
  while( --i >= 0 ) {
    if( strcmp(agent, _theAgentNames[i]) == 0 ) {
      if( _theAgentSockets[i] > 0 ) {
	sprintf(_UFerrmsg,"ufCloseAgent> closing connection to agent: %s.", agent);
	status = close( _theAgentSockets[i] );
	_theAgentSockets[i] = 0;
      }
      else
	sprintf(_UFerrmsg,"ufCloseAgent> already closed socket to agent: %s.",agent);
      _uflog(_UFerrmsg);
      return status;
    }
  }

  sprintf(_UFerrmsg,"ufCloseAgent> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);  
  return -1;
}
/*--------------------------------------------------------------------------*/

int ufConnectAgent(const char* agent, const char* host, int portNo)
{
  int socFd= 0;
  int i= _numAgents;
  char* agentName;
  char* hostname;

  while( --i >= 0 ) {
    if( strcmp( agent, _theAgentNames[i] ) == 0 ) {
      socFd = _theAgentSockets[i];
      if( socFd > 0 ) {
        sprintf(_UFerrmsg,
                "ufConnectAgent> already connected to agent: %s, on socket=%d",
                                                      agent, socFd );
        _uflog(_UFerrmsg);
        return socFd;
      }
      /* agent name is in table, but disconnected, so re-connect: */
      socFd = ufConnect( host, portNo );
      if( socFd <= 0 )
        sprintf(_UFerrmsg, "ufConnectAgent> Failed connect to agent: %s",agent);
      else {
	sprintf(_UFerrmsg,"ufConnectAgent> connected to agent: %s.",agent);
	_theAgentPorts[i] = portNo;
	_theAgentSockets[i] = socFd;
	if( strcmp( host, _theHostnames[i] ) != 0 ) {
	  free( _theHostnames[i] );
	  hostname = calloc( 1+strlen(host), sizeof(char) );
	  strcpy( hostname, host );
	  _theHostnames[i] = hostname;
	}
      }
      _uflog(_UFerrmsg);
      return socFd;
    }
  }

  /* agent name is not yet in table, so connect and add info: */

  if( _numAgents < _UFmaxAgentList_ ) {
    socFd = ufConnect( host, portNo );
    if( socFd > 0 ) {
      hostname = calloc( 1+strlen(host), sizeof(char) );
      strcpy( hostname, host );
      agentName = calloc( 1+strlen(agent), sizeof(char) );
      strcpy( agentName, agent );
      sprintf(_UFerrmsg,"ufConnectAgent> connected to agent: %s.",agentName);
      _theAgentNames[_numAgents] = agentName;
      _theHostnames[_numAgents] = hostname;
      _theAgentPorts[_numAgents] = portNo;
      _theAgentSockets[_numAgents++] = socFd;
    }
    else sprintf(_UFerrmsg, "ufConnectAgent> Failed connect to agent: %s",agent);
  }
  else sprintf(_UFerrmsg, "ufConnectAgent> exceeded max # of agents allowed in list.");

  _uflog(_UFerrmsg);
  return socFd;
}
/*--------------------------------------------------------------------------*/

int ufAgentSocket( const char* agent )      /* return socket Fd of an agent */
{
  int i= _numAgents;
  
  while( --i >= 0 ) {
    if( strcmp( agent, _theAgentNames[i] ) == 0 ) return _theAgentSockets[i];
  }

  sprintf(_UFerrmsg,"ufAgentSocket> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
}
/*-------------------------------------------------------------------------*/

int ufAgentPort( const char* agent )      /* return port number of an agent */
{
  int i= _numAgents;
  
  while( --i >= 0 ) {
    if( strcmp( agent, _theAgentNames[i] ) == 0 ) return _theAgentPorts[i];
  }

  sprintf(_UFerrmsg,"ufAgentSocket> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return -1;
}
/*-------------------------------------------------------------------------*/

char* ufAgentHost( const char* agent )      /* return hostname of an agent */
{
  int i= _numAgents;
  
  while( --i >= 0 ) {
    if( strcmp( agent, _theAgentNames[i] ) == 0 ) return _theHostnames[i];
  }

  sprintf(_UFerrmsg,"ufAgentSocket> unknown agent name= %s.", agent);
  _uflog(_UFerrmsg);
  return NULL;
}

#endif /* __UFClientAgent_c__ */
