#if !defined(__UFClient_h__)
#define __UFClient_h__ "$Name:  $ $Id: ufClient.h,v 0.9 2003/12/30 00:45:02 varosi beta $"
#define __UFClient_H__(arg) static const char arg##Client_h__rcsId[] = __UFClient_h__;

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 

#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "fcntl.h"
#include "ioLib.h"
#include "fioLib.h"
#include "in.h"
#include <sys/ioctl.h>
#include "stdio.h"
#include "time.h" 
#include "hostLib.h"
#include "math.h"
#include "inetLib.h"
#include "ctype.h"
#include "string.h" 

#else

#if defined(__APPLE__)

#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"

#else

#include "sys/types.h"
#include "arpa/inet.h"

#endif
#endif

typedef struct {
  int length, type, elem;
  short seqCnt, seqTot;
  float duration;
  char timestamp[25];
  char name[83];
} ufProtocolHeader;

typedef struct {
  ufProtocolHeader hdr;
  int w, h, d, littleEnd, dmaCnt, imageCnt, coAdds;
  int pixelSortID, frameObsSeqNo, frameObsSeqTot;
  int chopBeam, saveSet, nodBeam, nodSet, frameWriteCnt, frameSendCnt;
  int bgADUs; float bgWellpc; float sigmaFrmNoise;
  int offADUs; float offWellpc; float sigmaReadNoise;
  int frameCoadds; int chopSettleFrms; int chopCoadds; float frameTime; float savePeriod;
  int offset; int pixcnt;
  int bgADUmin; int bgADUmax; float sigmaFrmMin; float sigmaFrmMax;
  int rdADUmin; int rdADUmax; float sigmaReadMin; float sigmaReadMax;
  // now 37 elements
} ufFrameConfig;

typedef struct { 
  ufProtocolHeader hdr;
  int nodBeams, chopBeams, saveSets, nodSets, coaddsPerFrm;
  char readoutMode[12];
  char dataLabel[40];
} ufObsConfig;

/**
 * ufObsConfig struct is not sent as defined, instead the hdr is sent and then
 * the contiguous vector of 16bit flags that indicate the complete observation sequence is sent.
 * (see obsFlags in ufObsConfSend/Recv).
 */

/* basic os functions */

/** posix nanosleep */
extern void ufSleep(float time) ;

extern const char* ufHostName();
extern const char* ufHostTime(char* tz); 

/** sockets or fifos */
extern int ufAvailable(int fd);

/** named pipe/fifos */
extern int ufFifoAvailable(const char* fifoname);

/**
 * Agent (service) names, server IP addresse, default ports
 * (should be in /etc/services file)
 * return int number of list entrees, entry string looks like:
 * "AgentName@ServerHostName:nnn.nnn.nnn.nnn~PortNo"
 */
extern int ufServices(char** infolist);   /* returns max allowed # of agents */
extern char** ufAgents( int* Ngents );    /* returns list of current agents */

/* basic socket functions */

extern int ufIsIPAddress(const char* host);
extern int ufSetSocket(int socFd, struct sockaddr_in *addr, int portNo);
extern int ufConnect(const char* host, int portNo);
extern int ufClose(int socFd);

/** raw byte array output */

extern int ufSend(int socFd, const unsigned char* sndbuf, int nb);

extern int ufSendShort(int socFd, short val); /* htons */
extern int ufSendShorts(int socFd, short* vals, int cnt); /* htons */
extern int ufSendInt(int socFd, int val); /* htonl */
extern int ufSendInts(int socFd, int* vals, int cnt); /* htonl */
extern int ufSendFloat(int socFd, float val); /* htonl */
extern int ufSendFloats(int socFd, float* vals, int cnt); /* htonl */
extern int ufSendCstr(int socFd, const char* s);

/** raw byte array input */

extern int ufRecv(int socFd, unsigned char* rbuf, int len);

extern int ufSetTimeout( float timeout );  /* timeout (sec) in ufRecv */

extern short ufRecvShort(int socFd);                      /* ntohs */
extern int ufRecvShorts(int socFd, short* vals, int cnt); /* ntohs */
extern int ufRecvInt(int socFd);                          /* ntohl */
extern int ufRecvInts(int socFd, int* vals, int cnt);     /* ntohl */
extern float ufRecvFloat(int socFd);                      /* ntohl */
extern int ufRecvFloats(int socFd, float* vals, int cnt); /* ntohl */

/** reset null terminated string */
extern int ufRecvCstr(int socFd, char* rbuf, int len);

/** allocate new null terminated string */
extern int ufRecvNewCstr(int socFd, char** rbuf);


/** uf protocol object header socket i/o */

extern int ufHeaderSend(int socFd, ufProtocolHeader* hdr);
extern int ufHeaderRecv(int socFd, ufProtocolHeader* hdr);
extern int ufHeaderLength(const ufProtocolHeader* hdr);

/* ufStrings socket i/o *********/
                                  
extern int ufStringsSend( int socFd, char** strings, int Nstrings, const char* name );
extern char** ufStringsRecv( int socFd, int* Nstring, ufProtocolHeader* hdr, int* nbtot );

/*** all the func. below are by Agent name, rather than socFd */

extern int ufConnectAgent(const char* agent, const char* host, int portNo);
extern int ufCloseAgent(const char* agent);
extern int ufAgentSocket(const char* agent);
extern int ufAgentPort(const char* agent);
extern char* ufAgentHost(const char* agent);

/******** uf protocol object header i/o **********/

extern int ufHeaderSendAgent(const char* agent, ufProtocolHeader* hdr, int* socFd);
extern int ufHeaderRecvAgent(const char* agent, ufProtocolHeader* hdr, int* socFd);

/*** ufStrings i/o *********/

extern int ufStringsSendAgent( const char* agent, char** strings, int Nstrings, const char* name );
extern char** ufStringsRecvAgent( const char* agent, int* Nstring, ufProtocolHeader* hdr, int* nbtot );

/*** ufInts (32-bit) i/o: BufferName is optional input, ufProtocolHeader* hdr is output. */

extern int ufIntsSend( const char* agent, int* bufptr, int bufsiz, const char* BufferName );
extern int ufIntsRecv( const char* agent, int* bufptr, int bufsiz, ufProtocolHeader* hdr );

/*** ufFrameConfig & ufObsConfig i/o *********/

extern int ufFrameConfSend(const char* agent, ufFrameConfig* uffc);
extern int ufFrameConfRecv(const char* agent, ufFrameConfig* uffc);
extern int ufObsConfSend(const char* agent, ufObsConfig* ufoc, short* obsFlags);
extern int ufObsConfRecv(const char* agent, ufObsConfig* ufoc, short** obsFlags);

/* ufRequest:  sends a UFTimeStamp objectwith request in "name" field, 
 * server replies with UFLIB object (Hdr and data array).
 */

extern int ufRequest( const char* agent, const char* request ); 

#endif /* __UFClient_h__ */
