#!/usr/bin/perl

# shutdown.pl: Set wheels to closed positions

# Load the Trecs module
use Trecs;
my $rcsId = '$Name:  $ $Id: shutdown.pl 14 2008-06-11 01:49:45Z hon $ ';
my $status=0;

# Useful variables, initialize here to default value.  Change these to
# suit your needs...  Any non-empty values are automatically sent to
# the DB.  Please only specify values for those variables you wish to
# be sent to the DB.

### Initial Instrument Configuration ###

# Commanded camera mode
# "imaging"|"spectroscopy"
my $camera_mode = "";

# Commanded imaging mode
# "field"|"window"|"pupil"
my $imaging_mode = "" ;

# Commanded grating name
# "LowRes-10"|"HighRes-10"|"LowRes-20"
my $grating = "" ;

# Commanded central wavelength
my $central_wavelength = "" ;

# Commanded sector name
my $sector_name = "" ;

# Commanded Lyot stop name
my $lyot_stop_name = "Blank" ;

# Commanded slit width
my $slit_width = "";

# Following require overrides if manually set.
# Each override = "TRUE"|"FALSE"

# Window
my $override_window = "TRUE" ;
my $window_setting = "Blankoff" ;

# Aperture
my $override_aperture = "FALSE" ;
my $aperture_name = "" ;

# Filter
my $override_filter = "FALSE" ;
my $filter = "";

# Lens
my $override_lens = "FALSE" ;
my $lens_name = "" ;


### Initial Observation Configuration ###

# Observation data label
my $datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;

# Commanded observing mode
# "chop-nod"|"stare"|"chop"|"nod"
my $observing_mode = "" ;

# Commanded source photon collection time, minutes
my $photon_collection_time = "" ;

# Commanded secondary chop throw magnitude
my $chop_throw = "" ;

my $sky_noise = "";
my $sky_background = "";
my $airmass = "";
my $mirror_temperature = "";
my $emissivity = "";

my $inst_rot_rate = "";
my $nod_interval = "";
my $nod_settle = "";
my $readout_mode = "";

# FrameTime
my $override_frame_time = "FALSE" ;
my $frame_time = "" ;

my $override_array_temp = "FALSE" ;


##############################################################################
### Execute the first observation.  Do not change anything in this section ###
##############################################################################

# Initialize global variables
Trecs::tsprint "\n";
Trecs::tsprint "Script shutdown.pl\n";
Trecs::init_vars();

# Output configuration
Trecs::tsprint "\n" ;
Trecs::tsprint "Initializing Instrument configuration:\n" ;
Trecs::tsprint " Database:            $Trecs::epics\n" ;

### Instrument configuration ###

if( $lyot_stop_name     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.H='$lyot_stop_name`    ;}
if( $window_setting     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.K='$window_setting`    ;}
`$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.O='$override_window`   ;

# Apply the configuration
$status = apply_config();

Trecs::print_config();

