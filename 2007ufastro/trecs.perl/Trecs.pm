#!/usr/bin/perl
# Trecs.pm:  Module to support T-ReCS Scripts
# Version 2002 Dec 11 (TLH)    : Added WNR's mods.
# Version 2003 Jan    (TLH&FV) : Changed timing of monitor section near end of exec_obs.
# Version 2003 Feb 9  (TLH)    : Added abort_obs and stop_obs and script run/stop control.

package Trecs;
$Trecs::rcsId = '$Name:  $ $Id: Trecs.pm,v 0.15 2003/02/18 23:47:16 tlh beta $ ';

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

require Exporter;

@ISA = qw(Exporter Autoloader);
@EXPORT = qw(testsub apply_config exec_obs);
$VERSION = '1.0';

# directive enum from /gemini/epics/base/include/cad.h
# DO NOT CHANGE
$Trecs::CAD_ABORT  = 0;
$Trecs::CAD_MARK   = 0;
$Trecs::CAD_CLEAR  = 1;
$Trecs::CAD_PRESET = 2;
$Trecs::CAD_START  = 3;
$Trecs::CAD_STOP   = 4;
$Trecs::CAD_ACCEPT = 0;
$Trecs::CAD_REJECT = -1;

### Database Configuration ###
$Trecs::epics   = "trecs";

$Trecs::ufcaget = "";
$Trecs::ufcaput = "";
$Trecs::ufsleep = "";

### Script Control ###
$Trecs::RUN = 1;
$Trecs::STOP = 0;
$Trecs::script_status = $Trecs::RUN;
$Trecs::LOGFILE = 0;

sub openlog {
   open(LOGFILE, ">>$_[0]");
}

sub closelog {
   close(LOGFILE);
}

# Add a timestamp to STDIN and write to STDOUT and a logfile
sub tsprint {

   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

   print "$timestr $_[0]" ;
   print LOGFILE "$timestr $_[0]";

}



### Initialize global variables ###
sub init_vars {

   my $UFINSTALL = $ENV{ "UFINSTALL" } ;
   if( !defined( $UFINSTALL ) ) {
      $UFINSTALL = "/usr/local/uf" ;
      tsprint "Setting UFINSTALL to /usr/local/uf" ;
   }

   $Trecs::ufcaget = "$UFINSTALL/bin/ufcaget";
   $Trecs::ufcaput = "$UFINSTALL/bin/ufcaput";
   $Trecs::ufsleep = "$UFINSTALL/bin/ufsleep";

   if( ! -e "$Trecs::ufcaget" ) {
      tsprint "";
      tsprint "Unable to locate $Trecs::ufcaget, please ensure that this program is";
      tsprint "present in your $UFINSTALL directory path, or adjust the";
      tsprint "$UFINSTALL environment variable accordingly.";
      tsprint "" ;
      exit(-1);
   }

   if( ! -e "$Trecs::ufcaput" ) {
      tsprint "" ;
      tsprint "Unable to locate $Trecs::ufcaput, please ensure that this program is";
      tsprint "present in your $UFINSTALL directory path, or adjust the";
      tsprint "$UFINSTALL environment variable accordingly.";
      tsprint "";
      exit(-1);
   }

   if( ! -e "$Trecs::ufsleep" ) {
      tsprint "";
      tsprint "Unable to locate $Trecs::ufsleep, please ensure that this program is";
      tsprint "present in your $UFINSTALL directory path, or adjust the";
      tsprint "$UFINSTALL environment variable accordingly.";
      tsprint "";
      exit(-1);
   }

   $Trecs::script_status = $Trecs::RUN;

}


### "Clear-Init ###
### This should never be needed -- booting or rebooting should bring system
### up ready to go.  TLH  2003 Feb 12

sub clear_init {

   my $ack = 0 ;
   my $msg = "" ;
   my $cfgstat = "";


   `$Trecs::ufsleep 0.5` ;
   tsprint "\nClear - Init ...\n" ;


   `$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_CLEAR` ;

   `$Trecs::ufcaput -p $Trecs::epics':init.A='$Trecs::CAD_MARK` ;

   `$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_START` ;

   my $heart = `$Trecs::ufcaget -p $Trecs::epics':heartbeat'`;
   my $oldheart = $heart;
   while ($oldheart == $heart) {
      $heart = `$Trecs::ufcaget -p $Trecs::epics':heartbeat'`;
      tsprint "Heartbeat = $heart\n";
      sleep 1;
   }
}


# Apply the instrument configuration.  Check to see if the configuration
# is valid and, if so, wait for the re-configuration to complete.

sub apply_config {

   my $ack = 0 ;
   my $msg = "" ;
   my $cfgstat = "";

   if ($Trecs::script_status == $Trecs::STOP) {
      tsprint("Script stopped: configuration not applied.\n");
      return 0;
   }

   `$Trecs::ufsleep 0.5` ;
   tsprint "\n";
   tsprint "Applying...\n" ;

   `$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_START` ;

   # if caget times-out...
   while( $ack == 0 )   {
      `$Trecs::ufsleep 1.0` ;
      $ack = `$Trecs::ufcaget $Trecs::epics':'apply.VAL` ;
   }
   tsprint "Apply result: $ack" ;

   if( $ack == -1 ) {
      $msg = `$Trecs::ufcaget $Trecs::epics':'apply.MESS` ;
      tsprint "Apply failed/rejected':' $msg\n" ;
      return 0;
   }

 # wait for the instrument configuration to complete
   while ( $cfgstat ne "IDLE" && $cfgstat ne "ERR" ) {
      $cfgstat = `$Trecs::ufcaget $Trecs::epics':applyC'` ;
      chop $cfgstat ;
      tsprint "Configuring, status: $cfgstat\n" ;
      `$Trecs::ufsleep 2.0` ;
   }

   if( $cfgstat eq "ERR" ) {
      tsprint "Configuration status is: $cfgstat\n" ;
      $msg = `$Trecs::ufcaget $Trecs::epics':'apply.OMSS` ;
      chop $msg ;
      tsprint "Configuration failed, message: $msg\n" ;
      return 0;
   }

   return 1;
}


sub print_config {

   tsprint "\n" ;
   tsprint "Results of applying the Instrument Configuration:\n" ;

   my $camera_mode      = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.A'`	;
   my $camera_state     = `$Trecs::ufcaget $Trecs::epics':sad:cameraMode'`	;
   chomp($camera_mode);
   tsprint " Camera Mode        : $camera_mode: $camera_state";

   my $imaging_mode     = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.B'`	;
   my $imaging_state    = `$Trecs::ufcaget $Trecs::epics':sad:imagingMode'`	;
   chomp($imaging_mode);
   tsprint " Imaging Mode       : $imaging_mode: $imaging_state";

   my $wavelength       = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.F'`	;
   my $wavelength_state = `$Trecs::ufcaget $Trecs::epics':sad:wavelength'`	;
   chomp($wavelength);
   tsprint " Central wavelength : $wavelength: $wavelength_state" ;

   my $aperture_name    = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.C'`	;
   my $aperture_override= `$Trecs::ufcaget $Trecs::epics':instrumentSetup.L'`	;
   my $aperture_state   = `$Trecs::ufcaget $Trecs::epics':sad:apertureName'`    ;
   chomp($aperture_name);
   chomp($aperture_override);
   chomp($aperture_state);
   tsprint " Aperture Name      : $aperture_name: $aperture_state (override = $aperture_override)\n" ;

   my $filter_name      = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.D'`	;
   my $filter_override  = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.M'`	;
   my $filter_state     = `$Trecs::ufcaget $Trecs::epics':sad:filterName'`	;
   chomp($filter_name);
   chomp($filter_override);
   chomp($filter_state);
   tsprint " Filter Name        : $filter_name: $filter_state  (override = $filter_override)\n" ;

   my $grating_name     = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.E'`	;
   my $grating_state    = `$Trecs::ufcaget $Trecs::epics':sad:gratingName'`	;
   chomp($grating_name);
   tsprint " Grating            : $grating_name: $grating_state" ;

   my $lens_name        = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.G'`	;
   my $lens_override    = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.N'`	;
   my $lens_state       = `$Trecs::ufcaget $Trecs::epics':sad:lensName'`	;
   chomp($lens_name);
   chomp($lens_override);
   chomp($lens_state);
   tsprint " Lens Name          : $lens_name: $lens_state  (override = $lens_override)\n" ;

   my $lyot_name        = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.H'`	;
   my $lyot_state       = `$Trecs::ufcaget $Trecs::epics':sad:lyotName'`	;
   chomp($lyot_name);
   tsprint " Lyot Stop Name     : $lyot_name: $lyot_state" ;

   my $sector_name      = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.I'`	;
   my $sector_state     = `$Trecs::ufcaget $Trecs::epics':sad:sectorName'`	;
   chomp($sector_name);
   tsprint " Sector Name        : $sector_name: $sector_state" ;

   my $slit_width       = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.J'`	;
   my $slit_state       = `$Trecs::ufcaget $Trecs::epics':sad:slitName'`	;
   chomp($slit_width);
   tsprint " Slit Width         : $slit_width: $slit_state" ;

   my $window_name      = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.K'`	;
   my $window_override  = `$Trecs::ufcaget $Trecs::epics':instrumentSetup.O'`	;
   my $window_state     = `$Trecs::ufcaget $Trecs::epics':sad:windowName'`	;
   chomp($window_name);
   chomp($window_override);
   chomp($window_state);
   tsprint " Window Setting     : $window_name: $window_state  (override = $window_override)\n" ;

   tsprint "\n" ;
   tsprint "Results of applying the Observation Configuration:\n" ;

   my $observing_mode       = `$Trecs::ufcaget $Trecs::epics':observationSetup.A'`	;
   my $observing_mode_state = `$Trecs::ufcaget $Trecs::epics':sad:obsMode'`	;
   chomp($observing_mode);
   tsprint " Observing Mode     : $observing_mode: $observing_mode_state" ;

   my $readout_mode         = `$Trecs::ufcaget $Trecs::epics':observationSetup.VALL'`	;
   my $readout_mode_state   = `$Trecs::ufcaget $Trecs::epics':sad:readoutMode'`	;
   chomp($readout_mode);
   tsprint " Readout Mode       : $readout_mode: $readout_mode_state" ;

   my $frame_time           = `$Trecs::ufcaget $Trecs::epics':observationSetup.N'`	;
   my $frame_time_override  = `$Trecs::ufcaget $Trecs::epics':observationSetup.M'`	;
   my $frame_time_state     = `$Trecs::ufcaget $Trecs::epics':sad:frameTime'` ;
   chomp($frame_time);
   chomp($frame_time_override);
   chomp($frame_time_state);
   tsprint " Frame Time         : $frame_time: $frame_time_state (override = $frame_time_override)\n" ;

   my $photon_collection_time       = `$Trecs::ufcaget $Trecs::epics':observationSetup.B'`	;
   my $photon_collection_time_state = `$Trecs::ufcaget $Trecs::epics':sad:photonTime'`	;
   chomp($photon_collection_time);
   tsprint " On Source Time     : $photon_collection_time: $photon_collection_time_state" ;

   my $chop_throw           = `$Trecs::ufcaget $Trecs::epics':observationSetup.C'`	;
   my $chop_throw_state     = `$Trecs::ufcaget $Trecs::epics':sad:chopThrow'`	;
   chomp($chop_throw);
   chomp($chop_throw_state);
   tsprint " Chop Throw         : $chop_throw: $chop_throw_state\n" ;

   my $nod_interval         = `$Trecs::ufcaget $Trecs::epics':observationSetup.J'`	;
   my $nod_interval_state   = `$Trecs::ufcaget $Trecs::epics':sad:nodDwell'`	;
   chomp($nod_interval);
   tsprint " Nod Interval       : $nod_interval: $nod_interval_state" ;

   my $nod_settle           = `$Trecs::ufcaget $Trecs::epics':observationSetup.K'`	;
   my $nod_settle_state     = `$Trecs::ufcaget $Trecs::epics':sad:nodDelay'`	;
   chomp($nod_settle);
   tsprint " Nod Settle         : $nod_settle: $nod_settle_state" ;

   my $inst_rot_rate        = `$Trecs::ufcaget $Trecs::epics':observationSetup.I'`	;
   my $inst_rot_rate_state  = `$Trecs::ufcaget $Trecs::epics':sad:rotatorRate'`	;
   chomp($inst_rot_rate);
   tsprint " Inst Rot Rate      : $inst_rot_rate: $inst_rot_rate_state" ;

   my $sky_noise            = `$Trecs::ufcaget $Trecs::epics':observationSetup.D'`	;
   my $sky_noise_state      = `$Trecs::ufcaget $Trecs::epics':sad:skyNoise'`	;
   chomp($sky_noise);
   tsprint " Sky Noise          : $sky_noise: $sky_noise_state" ;

   my $sky_background       = `$Trecs::ufcaget $Trecs::epics':observationSetup.E'`	;
   my $sky_background_state = `$Trecs::ufcaget $Trecs::epics':sad:skyBackground'`	;
   chomp($sky_background);
   tsprint " Sky Background     : $sky_background: $sky_background_state" ;

   my $airmass              = `$Trecs::ufcaget $Trecs::epics':observationSetup.F'`	;
   my $airmass_state        = `$Trecs::ufcaget $Trecs::epics':sad:airMass'`	;
   chomp($airmass);
   tsprint " Airmass            : $airmass: $airmass_state" ;

   my $mirror_temperature   = `$Trecs::ufcaget $Trecs::epics':observationSetup.G'`	;
   my $mirror_temperature_state = `$Trecs::ufcaget $Trecs::epics':sad:externalTemp'` ;
   chomp($mirror_temperature);
   tsprint " Mirror Temp        : $mirror_temperature: $mirror_temperature_state" ;

   my $emissivity           = `$Trecs::ufcaget $Trecs::epics':observationSetup.H'`	;
   my $emissivity_state     = `$Trecs::ufcaget $Trecs::epics':sad:emissivity'`	;
   chomp($emissivity);
   tsprint " Emissivity         : $emissivity: $emissivity_state" ;

   # Computed Integration Time Parameters
   my $chop_coadds_state    = `$Trecs::ufcaget $Trecs::epics':sad:chopCoaddUc'` ;
   chomp($chop_coadds_state);
   tsprint " Chop Coadds        : $chop_coadds_state\n" ;

   my $frame_coadds_state   = `$Trecs::ufcaget $Trecs::epics':sad:frameCoaddUf'` ;
   tsprint " Frame Coadds       : $frame_coadds_state" ;

   my $save_sets_state      = `$Trecs::ufcaget $Trecs::epics':sad:saveSets'` ;
   tsprint " Save Sets          : $save_sets_state" ;

   my $nod_sets_state       = `$Trecs::ufcaget $Trecs::epics':sad:nodSets'` ;
   tsprint " Nod Sets           : $nod_sets_state" ;
}




# Execute an observation.

sub exec_obs {

   my ($datalabel, $abort_frame, $stop_frame) = @_;

   my $ack = 0;
   my $msg = "";
   my $CARtop = "";
   my $CARobs = "";
   my $OBSstat = "";
   my $dt0 = `date` ;
   my $framecount = 0;
   my $nodbeam = 0;
   my $nodcycle = 0;
   my $acq = 0;
   my $rdout = 0;
   my $prep = 0;

   if ($Trecs::script_status == $Trecs::STOP) {
      tsprint("Script stopped: Observation with datalabel = $datalabel not executed.\n");
      return 0;
   }

   if ($abort_frame >= 0) {
      tsprint("Observation will Abort at frame $abort_frame\n");
   }
   elsif ($stop_frame >= 0) {
      tsprint("Observation will Stop at frame $stop_frame\n");
   }

   `$Trecs::ufsleep 1.0` ;

 # Apply the data label to begin the observation
   tsprint "\n";
   tsprint "Beginning Observation with datalabel = $datalabel\n" ;
   `$Trecs::ufcaput -p $Trecs::epics':observe.A='$datalabel` ;

   my $status = "";
   $status = apply_config();
   if( $status == 0 ) { exit; }

 # wait for the observation  to complete
   $OBSstat = `$Trecs::ufcaget $Trecs::epics':sad:observationStatus'` ;
   chop $OBSstat ;
   tsprint "observationStatus: $OBSstat\n" ;

   $CARobs = "BUSY";
   while ( $CARobs eq "BUSY" or $CARobs eq "PAUSED" ) {
      `$Trecs::ufsleep 1.0` ;
      $CARobs = `$Trecs::ufcaget $Trecs::epics':observeC'` ;
      chop $CARobs ;
      $framecount = `$Trecs::ufcaget $Trecs::epics':dc:acqControlG.VALI'`;
      $nodbeam    = `$Trecs::ufcaget $Trecs::epics':sad:currentBeam'`;
      $nodcycle   = `$Trecs::ufcaget $Trecs::epics':sad:currentNodCycle'`;
      $acq        = `$Trecs::ufcaget $Trecs::epics':sad:acq'`;
      $rdout      = `$Trecs::ufcaget $Trecs::epics':sad:rdout'`;
      $prep       = `$Trecs::ufcaget $Trecs::epics':sad:prep'`;
#     $nodbeam    = `$Trecs::ufcaget $Trecs::epics':dc:acqControlG.VALI'`;
#     $nodcycle   = `$Trecs::ufcaget $Trecs::epics':dc:acqControlG.VALI'`;
      chop $framecount;
      chop $nodbeam;
      chop $nodcycle;
      chop $acq;
      chop $prep;
      chop $rdout;
      tsprint "OBS CAR: $CARobs, frame $framecount, beam $nodbeam, nod $nodcycle, acq = $acq, rdout = $rdout, prep = $prep \n" ;

     # Handle abort or stop
      if    ($framecount == $abort_frame) { abort_obs(); }
      elsif ($framecount == $stop_frame)  { stop_obs();  }
   }

   `Trecs::ufsleep 1.0`;
   if( $CARobs eq "ERR" ) {
      tsprint "$dt0: $CARobs, Observation, label: $datalabel Failed" ;
      return 0;
   }

   $OBSstat = `$Trecs::ufcaget $Trecs::epics':sad:observationStatus'` ;
   chop $OBSstat ;
   tsprint "observationStatus: $OBSstat\n" ;

   return 1;
}


# Abort the observation
sub abort_obs {

   `$Trecs::ufcaput -p $Trecs::epics':abort.DIR='$Trecs::CAD_MARK` ;
   `$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_START` ;

   tsprint "Observation Aborted\n";
   $Trecs::script_status = $Trecs::STOP;
}


# Stop the observation
sub stop_obs {

   `$Trecs::ufcaput -p $Trecs::epics':stop.DIR='$Trecs::CAD_MARK` ;
   `$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_START` ;

   tsprint "Observation Stopped\n";
   $Trecs::script_status = $Trecs::STOP;
}


# Modules must return true value to load properly.
1;
__END__
