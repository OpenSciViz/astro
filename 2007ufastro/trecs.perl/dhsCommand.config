#***********************************************************************
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#
# (c) 1997				(c) 1997
# National Research Council		Conseil national de recherches
# Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
# All rights reserved			Tous droits reserves
# 					
# NRC disclaims any warranties,	Le CNRC denie toute garantie
# expressed, implied, or statu-	enoncee, implicite ou legale,
# tory, of any kind with respect	de quelque nature que se soit,
# to the software, including		concernant le logiciel, y com-
# without limitation any war-		pris sans restriction toute
# ranty of merchantability or		garantie de valeur marchande
# fitness for a particular pur-	ou de pertinence pour un usage
# pose.  NRC shall not be liable	particulier.  Le CNRC ne
# in any event for any damages,	pourra en aucun cas etre tenu
# whether direct or indirect,		responsable de tout dommage,
# special or general, consequen-	direct ou indirect, particul-
# tial or incidental, arising		ier ou general, accessoire ou
# from the use of the software.	fortuit, resultant de l'utili-
# 					sation du logiciel.
#
#***********************************************************************
#
# FILENAME
# dhsCmd/config/dhsCmd.config
#
# PURPOSE:
# Configuration file for the command server.
#
#INDENT-OFF*
# $Log: dhsCommand.config,v $
# Revision 0.0  2002/06/03 17:44:36  hon
# initial checkin
#
# Revision 0.0  2002/02/11 16:26:46  hon
# *** empty log message ***
#
# Revision 1.9  1998/07/31 17:18:35  cockayne
# Added bdDelete command and attribute.
#
# Revision 1.8  1998/07/09 18:11:19  cockayne
# Modifications for supporting all DHS commands.
#
# Revision 1.7  1998/06/28 22:22:24  nhill
# Removed some duplicate commands.
#
# Revision 1.6  1998/06/01 22:56:33  jaeger
# Made sure that HIS is reset(init) after DTS
#
# Revision 1.5  1998/05/29 21:18:36  jaeger
# Added an ordering to the exit, init and rest commands since the command
# server now uses these as the order of execution.
#
# Revision 1.4  1998/04/01 22:01:42  jaeger
# Changed from daxka to salish.
#
# Revision 1.3  1998/03/13 22:18:13  jaeger
# added HIS, DTS and QLS subsystems.  Added subsystem list to test, reset, inicommands.  Also added dtsDatasetDelete, resetHealth, hisLogWrite.
#
# Revision 1.2  1997/09/18 17:52:49  nhill
# Changed the server from siska to daxka.
# Added the ping command.
#
# Revision 1.1  1997/04/16 21:51:34  nhill
# Initial revision
#
#INDENT-ON*
#
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#***********************************************************************
#

#
#  Application identification
#
# Keyword	application 
#
# -------	-----------
#

identity	commandServerNS


#
#  Imp information
#
# keyword       Max connects    Client retry period
#                               (seconds)
# --------      ------------    --------------
#

imp             20              30



#
#  Subsystem list
#
# keyword	subSystem	address			identity	commandLine
# -------	---------	-------			--------	-----------
#  

subsystem	CMD		dhstrecs		commandServerNS	dhsCommand
subsystem      	DTS             dhstrecs		dataServerNS    dhsData
subsystem       QLS             dhstrecs     		qlServerNS      dhsQlServer
subsystem	STA		dhstrecs		statusServerNS	dhsStatus



#
#  Command list
#
# keyword	command		abortable	subsystem	target
#		name				list		systems
#  --------	-------		---------	---------	-------
#

command		abort		yes		no
command		bdDelete	yes		no		DTS
command		cmdAbort	no		no		CMD
command		cmdSystemExit	no		no		CMD
command		continue	yes		no
command		datasetInfo	yes		no		DTS
command		debugLevel	no		yes		*
command         dtsDatasetDelete yes            no              DTS
command		endguide	yes		no
command		endverify	yes		no
command		flush		yes		yes		*
command		guide		yes		no
command		observe		yes		no
command		park		yes		no
command		pause		yes		no
command		ping		no		yes		*
command		resetHealth	yes		yes		*
command		simulateLevel	no		yes		*
command		stop		yes		no
command		test		yes		yes		*
command		verify		yes		no


#
# NOTE that these commands have a fully specified subsytem list.  This
# is because the order of execution is important for these commands.
# In the case if "reset" and "init" the Status server must be first and
# the "DTS" next to last.  In the case of exit the status server needs to
# be next to last and the data server one of the first systems.  The order
# the subsystems appear in is the order they will be executed in, except
# the command server always executes the command on itself last, so the
# Command server is always last.
#

command		exit		no		yes		DTS QLS STA CMD
command		init		no		yes		STA QLS DTS CMD
command		reset		no		yes		STA QLS DTS CMD


#
#  Command attribute list
#
# keyword	command		attribute		type		optional
#		name		name
#  --------	-------		---------		----		--------
#

attribute	bdDelete	datasetName		DHS_DT_STRING	no
attribute	cmdAbort	tag			DHS_DT_TAG	no
attribute	cmdSystemExit	disconnectTimeout	DHS_DT_INT32	yes
attribute	debugLevel	level			DHS_DT_STRING	no
attribute	debugLevel	subsystem		DHS_DT_STRING	yes
attribute	dtsDatasetDelete dataLabel		DHS_DT_STRING	no
attribute	exit		subsystem		DHS_DT_STRING	yes
attribute	flush		subsystem		DHS_DT_STRING	yes
attribute	init		subsystem		DHS_DT_STRING	yes
attribute	ping		subsystem		DHS_DT_STRING	yes
attribute	reset		subsystem		DHS_DT_STRING	yes
attribute       resetHealth     subsystem               DHS_DT_STRING   yes
attribute	simulateLevel	level			DHS_DT_STRING	no
attribute	simulateLevel	subsystem		DHS_DT_STRING	yes
attribute       test            subsystem               DHS_DT_STRING   yes
