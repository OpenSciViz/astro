#if !defined(__TRECS_C__)
#define __TRECS_C__ "RCS: $Name:  $ $Id: trecs.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufTRECSC[] = __TRECS_C__ ;


#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>


#include <car.h>
#include <genSub.h>
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>


#include <dbStaticLib.h> 
#include <dbBase.h>

#include "trecs.h"



/**********************************************************/
/**********************************************************/
/**********************************************************/
void trx_debug(char *message, char * rec_name, char *mess_level, char *debug_level) {
  static char out_mess[255] ;
  int log_it = 0 ;
  int imess_level ;
  int idebug_level ;  
  char filename[50] ;
  
  /*
  if (logFile == NULL) { 
    strcpy(filename,"./trecs.log") ; 
     if ((logFile = fopen(filename,"a+")) == NULL) { 
       printf("********************************* unable to open log file\n") ;
     } 
  } 
  */ 

  if (strcmp(debug_level,"FULL") == 0) idebug_level = 2 ;
  if (strcmp(debug_level,"MID") == 0) idebug_level = 1 ;
  if (strcmp(debug_level,"NONE") == 0) idebug_level = 0 ;

  if (strcmp(mess_level,"FULL") == 0) imess_level = 2 ;
  if (strcmp(mess_level,"MID") == 0) imess_level = 1 ;
  if (strcmp(mess_level,"NONE") == 0) imess_level = 0 ;
  
  if (imess_level <= idebug_level) log_it = 1;
  /*
  printf("\nMessage: %s\n",message) ; 
  printf("record name: %s \n",rec_name) ; 
  printf("message level %s \n",mess_level);
  printf("debug level %s \n",debug_level)
;
  */
  
  if (log_it) {
    strcpy(out_mess,"<%ld> ") ;
    strcat(out_mess,rec_name) ;
    strcat(out_mess,":") ;
    strcat(out_mess,message) ;   
    strcat(out_mess,"\n") ;
    logMsg(out_mess,(int)tickGet(),0,0,0,0,0);
    /* fwrite(out_mess,sizeof(char), strlen(out_mess), logFile) ; */
    /* fprintf(logFile,"%s",out_mess) ; */
  }
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
int init_trecs_config () {
  FILE *trxFile ;
  char in_str[80] ;
  char *temp_str ;
  char filename[40] ;
 


  strcpy(filename,"./pv/trecs_cfg.txt");
  if ((trxFile = fopen(filename,"r")) == NULL) {
    trx_debug("TReCS Configuration file could not be opened","trxConfig function","NONE","NONE") ;
    return -1 ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(ccconfig_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(mot_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(dcconfig_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(ecconfig_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(env_gs_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(env_hb_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(env_array_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(env_tempMon_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(env_press_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(temp_cont_filename,in_str) ;

    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(ch_param_filename,in_str) ;
 
    fgets(in_str,40,trxFile);
    fgets(in_str,40,trxFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(vac_param_filename,in_str) ;

    fclose(trxFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    trecs_initialized  = 1 ;
    printf("trecs Initialized ... \n") ;
  }
  /* printf("!!@ @@@@@@@@@@ !!!!!!!!!!! trecs_initialized %d\n",trecs_initialized) ; */

  return OK;
}

#endif


