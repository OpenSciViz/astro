#if !defined(__UFGEMGENSUBCOMMEC_C__)
#define __UFGEMGENSUBCOMMEC_C__ "RCS: $Name:  $ $Id: ufGemGensubCommEC.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufGEMGENSUBCOMMCEC[] = __UFGEMGENSUBCOMMEC_C__ ;

#include <stdioLib.h>
#include <string.h>
#include <stdio.h>

#include <sysLib.h>
#include <sockLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>

#include <car.h>
#include <genSub.h>
#include <genSubRecord.h>
#include <cad.h>
#include <cadRecord.h>

#include <dbStaticLib.h> 
#include <dbBase.h>
#include "memLib.h"

#include "trecs.h"
#include "ufGemComm.h"
#include "ufGemGensubComm.h"
#include "ufClient.h"
#include "ufLog.h"

static double ec_update[3] ;
static char ec_gsp[3][40] ;
static char ec_heartbeat[4][40] ;

static char ec_DEBUG_MODE[6] ;
static char ec_STATUS_DEBUG_MODE[6] ;
static char ec_STARTUP_DEBUG_MODE[6] ;
static int ec_initialized  = 0 ;
static char ec_host_ip[20] ;
static char ch_host_ip[20] ;
static char vac_host_ip[20] ;
static long ec_port_no ; /* LS 340 */
static long ch_port_no ; /* Cold Head PS 6440 */
static long vac_port_no ;/* Vacuum gage GP 354 */

/********************** EC CODING ************************/

void init_ec_config () {
  FILE *ecFile ;
  char in_str[80] ;
  char *temp_str ;
  char filename[40] ;
 
  strcpy(filename,ecconfig_filename);
  if ((ecFile = fopen(filename,"r")) == NULL) {
    trx_debug("ec Configuration file could not be opened","ecConfig function","NONE","NONE") ;
    strcpy(ec_STATUS_DEBUG_MODE,"NONE") ;
    strcpy(ec_STARTUP_DEBUG_MODE,"NONE") ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,40,ecFile);
    fgets(in_str,40,ecFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(ec_STARTUP_DEBUG_MODE,in_str) ;
    fgets(in_str,40,ecFile);
    fgets(in_str,40,ecFile);
    temp_str = strchr(in_str,'\n') ;
    if (temp_str != NULL) temp_str[0] = '\0' ;
    strcpy(ec_STATUS_DEBUG_MODE,in_str) ;
    fclose(ecFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    if ( (strcmp(ec_STARTUP_DEBUG_MODE,"NONE") != 0) ||
         (strcmp(ec_STARTUP_DEBUG_MODE,"MIN") != 0) ||
	 (strcmp(ec_STARTUP_DEBUG_MODE,"FULL") != 0) ) strcpy(ec_STARTUP_DEBUG_MODE,"NONE") ;
    if ( (strcmp(ec_STATUS_DEBUG_MODE,"NONE") != 0) ||
         (strcmp(ec_STATUS_DEBUG_MODE,"MIN") != 0) ||
	 (strcmp(ec_STATUS_DEBUG_MODE,"FULL") != 0) ) strcpy(ec_STATUS_DEBUG_MODE,"NONE") ;
    strcpy(ec_DEBUG_MODE,"NONE") ;
  }
  logMsg("EC Initialized ... \n",0,0,0,0,0,0) ;
  ec_initialized  = 1 ;
  return ;
}

/******************* INAM for EC configG ******************/
long ufecconfigGinit(genSubRecord *pgs) {
  /* Gensub Private Structure Declaration */ 
  genSubCommPrivate *pPriv;

  pPriv = (genSubCommPrivate *) malloc (sizeof(genSubCommPrivate)); 
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */


  /* set up a CALLBACK after 1.0 seconds in order to send it.
   *  set the Command state to SENDING
  */
  /* trx_debug("Setting Command State",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT ) ;

  pgs->dpvt = (void *) pPriv;
  
  return OK ;
}

/******************* SNAM for EC configG ******************/
long ufecconfigGproc(genSubRecord *pgs) {
  long status = -1;
  unsigned long line_count ;
  FILE *posFile ;
  char in_str[80] ;
  char *temp_str ;
  double temp_update ;
  char filename[50] ;
  genSubCommPrivate *pPriv;

  pPriv = (genSubCommPrivate *)pgs->dpvt;

  if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState);
  /* if (bingo) memShow(0) ; */
  /* printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
     printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
  /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ;  */
  if (!trecs_initialized)  init_trecs_config() ;
  /* printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

     printf("################### Started reading the EC config file %ld\n", tickGet()) ; */
  /*
  sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
  strcat(in_str,pgs->name) ; 
  strcat(in_str,"\n") ; 
  logMsg(in_str,0,0,0,0,0,0) ; 
  */
  if (!ec_initialized)  init_ec_config() ;
  /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */
  trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ;

  strcpy(filename,env_gs_filename);

  trx_debug("Attempting to open env_gs.txt",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;

  /* if ((posFile = fopen("./bin/svgm5/env_ai.txt","r")) == NULL) { */
  /* if ((posFile = fopen("./bin/mv167/env_ai.txt","r")) == NULL) { */
  if ((posFile = fopen(filename,"r")) == NULL) {
    trx_debug("Environment GS file could not be opened.",pgs->name,"NONE",ec_STARTUP_DEBUG_MODE) ;
    status = -1 ;
    return status ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    trx_debug("Reading env_gs.txt",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
    /* read the first line
       read the update time  */
    fscanf(posFile,"%lf",&temp_update);
    /* read the GS name */
    fgets(in_str,40,posFile);
    line_count=1 ;
    while (!feof(posFile)) {
      temp_str = strchr(in_str,'\n') ;
      if (temp_str != NULL) temp_str[0] = '\0' ;
      /* while( (in_str[0] == ' ') || (in_str[0] == '\t')) in_str = in_str + 1 ; */
      strcpy(ec_gsp[line_count-1],in_str) ;
      ec_update[line_count-1] = temp_update ;
      if (bingo) 
        printf("Line %3ld: %s\n",line_count, /*ec_update[line_count-1],*/ec_gsp[line_count-1]) ;
      /*printf("Line %3ld: %4.2f %s\n",line_count, update_envChanNames[line_count-1],envChanNames[line_count-1]) ;
	 read the update time */
      fscanf(posFile,"%lf",&temp_update);
      if (!feof(posFile)) {
        /* read the AI name */
        fgets(in_str,40,posFile);
      }
      line_count++ ;
    }

    fclose(posFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    pgs->nova = line_count-1 ;
    pgs->vala = ec_gsp[0] ;
    pgs->novb = line_count-1 ;
    pgs->valb = ec_update ;
  }

  strcpy(filename,env_hb_filename);
  if ((posFile = fopen(filename,"r")) == NULL) {
    trx_debug("Environment GS file could not be opened.",pgs->name,"NONE",ec_STARTUP_DEBUG_MODE) ;
    status = -1 ;
    return status ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    trx_debug("Reading env_hb.txt",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
    /* read the first line */
    /* read the Longin name */
    fgets(in_str,40,posFile);
    line_count=1 ;
    while (!feof(posFile)) {
      temp_str = strchr(in_str,'\n') ;
      if (temp_str != NULL) temp_str[0] = '\0' ;
      /* while( (in_str[0] == ' ') || (in_str[0] == '\t')) in_str++ ; */
      strcpy(ec_heartbeat[line_count-1],in_str) ;
      if (bingo) 
        printf("Line %3ld: %s\n",line_count, ec_heartbeat[line_count-1]) ;
      /*printf("Line %3ld: %4.2f %s\n",line_count, update_envChanNames[line_count-1],envChanNames[line_count-1]) ;
	 read the update time */
      if (!feof(posFile)) {
        /* read the AI name */
        fgets(in_str,40,posFile);
      }
      line_count++ ;
    }

    fclose(posFile) ;
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    /* *(long *)gsp->valc = (line_count-1) ; */
   
    pgs->novc = line_count-1 ;
    pgs->valc = ec_heartbeat[0] ;
  }
  trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for arrayG *******************/
long ufarrayGinit (genSubRecord *pgs) {

  /* Gensub Private Structure Declaration */
  genSubArrayPrivate *pPriv;

  /*  trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  pPriv = (genSubArrayPrivate *) malloc (sizeof(genSubArrayPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* trx_debug("Clearing input links",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ; */
  /* clear the input links */
  *(double *)pgs->a = -1.0 ;
  *(double *)pgs->b = -1.0 ;
  *(double *)pgs->c = -1.0 ;
  *(double *)pgs->d = -1.0 ;
  *(double *)pgs->e = -1.0 ;
  strcpy(pgs->j,"");
 
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  /* set up a CALLBACK after 0.5 seconds in order to send it.
   *  set the Command state to SENDING
  */

  pPriv->commandState = TRX_GS_INIT;

  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT) ;


  pgs->dpvt = (void *) pPriv;

  pgs->noj = 2 ;

  /*  trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for arrayG *******************/
long ufarrayGproc (genSubRecord *pgs) {
  long status ;
  double out_val, limit;
  char *endptr ;
  genSubArrayPrivate *pPriv;

  /* Initialization File */
  FILE *tcFile ;
  char in_str[80] ;
  double numnum ;
  char filename[50] ;

  pPriv = (genSubArrayPrivate *)pgs->dpvt ;
  /* if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* if (bingo) memShow(0) ; */
  if ((pPriv->commandState == TRX_GS_INIT) && (strcmp(pgs->j,"") == 0)) {
    /* printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
       printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
    /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ;  */
    if (!trecs_initialized)  init_trecs_config() ;
    /* printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

       printf("################### Started reading the EC config file %ld\n", tickGet()) ; */
    /* printf("######################### ec_initialized %d in %s\n",ec_initialized,pgs->name) ; */
    /*
    sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
    strcat(in_str,pgs->name) ; 
    strcat(in_str,"\n") ; 
    logMsg(in_str,0,0,0,0,0,0) ;
    */
    if (!ec_initialized)  init_ec_config() ;
    /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */
    trx_debug("Opening input parameters file.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
    /* Read some initial parameters from a file */
    pPriv->array_upper = -1.0 ;
    pPriv->array_lower = -1.0 ;
    pPriv->cold_finger_upper = -1.0 ;

    strcpy(filename,env_array_filename);
    if ((tcFile = fopen(filename,"r")) == NULL) {
      trx_debug("Array Initialization file could not be opened", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
     
    } else {
      NumFiles++ ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
      trx_debug("Reading input parameters file",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
      
      /* Read the first line Array Upper */
      fscanf(tcFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tcFile); 
      if (UFcheck_double(numnum,set_point_lo,set_point_hi) == -1) {
        trx_debug("Array upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);      
      } else { 
        pPriv->array_upper = numnum;
      }
      trx_debug("Reading 2nd line",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
      /* Read the second line Array Lower */
      fscanf(tcFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tcFile); 
      /* printf("!!!!!!!!! Set point Lo: %f set point Hi: %f and numnum is %f",set_point_lo,set_point_hi, numnum) ; */
      if (UFcheck_double(numnum,set_point_lo,set_point_hi) == -1) {
        trx_debug("Array Lower is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->array_lower = numnum;
      }
      trx_debug("Reading 3rd line",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
      /* Read the third line cold finger upper */
      fscanf(tcFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tcFile); 
      /* printf("!!!!!!!!! CF Lo: %f CF Hi: %f and numnum is %f",cold_finger_lo,cold_finger_hi, numnum) ; */
      if (UFcheck_double(numnum,cold_finger_lo,cold_finger_hi) == -1) {
        trx_debug("Cold finger is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->cold_finger_upper = numnum;
      }
      /* printf("Cold finger pPriv is %f\n",pPriv->cold_finger_upper) ; */
      fclose(tcFile) ;
      NumFiles-- ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    }
    pPriv->commandState = TRX_GS_DONE ;
    /* ********************* */
  } else {
    trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ;

    status = unpack_array(pgs) ;
    pgs->noj = 2 ;

    trx_debug("Checking the values.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
    /*Check the values unpacked against the limits */
   

    out_val = strtod(pgs->vala,&endptr) ;
    if (*(long *)pgs->b == -1) limit = pPriv->array_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->b,set_point_lo,set_point_hi) == -1) {
        trx_debug("Array upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->array_upper ;
      } else {  
        pPriv->array_upper = *(double *)pgs->b;
        *(double *)pgs->b = -1 ;
      }
    }

    if (*(long *)pgs->c == -1) limit = pPriv->array_lower ;
    else {
      if (UFcheck_double(*(double *)pgs->c,set_point_lo,set_point_hi) == -1) {
        trx_debug("Array lower is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->array_lower ;
      } else { 
        pPriv->array_lower = *(double *)pgs->c;
        limit = pPriv->array_lower ;
        *(double *)pgs->c = -1 ;
      }
    }
    /* Check to see if the array Temp is ok */
    if ( (out_val < pPriv->array_upper) && (out_val > pPriv->array_lower) && (out_val > 0)) strcpy(pgs->valc,"TRUE") ;
    else strcpy(pgs->valc,"FALSE") ;

    /*
    if ( (*endptr != '\0') || (out_val > pPriv->array_upper) || (out_val < pPriv->array_lower) || 
         ((long)pPriv->array_upper == -1) || ((long)pPriv->array_lower == -1) ) 
      strcpy(pgs->valc,"FALSE") ; 
    else strcpy(pgs->valc,"TRUE") ;
    */

    out_val = strtod(pgs->valb,&endptr) ;
    if ( (long)(*(double *)pgs->d) == -1) limit = pPriv->cold_finger_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->d,cold_finger_lo,cold_finger_hi) == -1) {
        trx_debug("Cold finger upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->cold_finger_upper ;
      } else {
        pPriv->cold_finger_upper = *(double *)pgs->d;
        limit = pPriv->cold_finger_upper ;
        *(double *)pgs->d = -1 ;
      }
    }
    /* printf("My limit is %f and my out_val is %f and my pPriv->upper is: %f. Is out_val < limit?\n",limit, out_val,pPriv->cold_finger_upper) ; */
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->vald,"TRUE") ;
    else strcpy(pgs->vald,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->vald,"FALSE") ; 
    else strcpy(pgs->vald,"TRUE") ; 
    */
    trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  }
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for tempMonG *******************/
long uftempMonGinit (genSubRecord *pgs) {

  /* Gensub Private Structure Declaration */
  genSubTempMonPrivate *pPriv;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  pPriv = (genSubTempMonPrivate *) malloc (sizeof(genSubTempMonPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* trx_debug("Clearing input parameters.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ; */
  /* clear the input links */
  *(double *)pgs->a = -1.0 ;
  *(double *)pgs->b = -1.0 ;
  *(double *)pgs->c = -1.0 ;
  *(double *)pgs->d = -1.0 ;
  *(double *)pgs->e = -1.0 ;
  *(double *)pgs->f = -1.0 ;
  *(double *)pgs->g = -1.0 ;
  *(double *)pgs->h = -1.0 ;

  strcpy(pgs->j,"");

  /* trx_debug("Opening param file.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ; */
  /* Read some initial parameters from a file */
  pPriv->cold1_upper = -1.0 ;
  pPriv->cold2_upper = -1.0 ;
  pPriv->active_upper = -1.0 ;
  pPriv->passive_upper = -1.0 ;
  pPriv->window_upper = -1.0 ;
  pPriv->strap_upper = -1.0 ;
  pPriv->edge_upper = -1.0 ;
  pPriv->middle_upper = -1.0 ;

  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  /* set up a CALLBACK after 0.5 seconds in order to send it.
   *  set the Command state to SENDING
  */

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT ) ;
 

  pgs->dpvt = (void *) pPriv;

  pgs->noj = 8 ;

  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** SNAM for tempMonG *******************/
long uftempMonGproc (genSubRecord *pgs) {
  long status ;
  double out_val, limit;
  char *endptr ;
  genSubTempMonPrivate *pPriv;

  /* Initialization File */
  FILE *tmFile ;
  char in_str[80] ;
  double numnum ;
  char filename[50] ;

  pPriv = (genSubTempMonPrivate *)pgs->dpvt ;
  /* if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* if (bingo) memShow(0) ; */
  if ((pPriv->commandState == TRX_GS_INIT) && (strcmp(pgs->j,"") == 0)) {
    /* printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
       printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
    /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ;  */
    if (!trecs_initialized)  init_trecs_config() ;
    /* printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

       printf("################### Started reading the EC config file %ld\n", tickGet()) ; */
    /* printf("######################### ec_initialized %d in %s\n",ec_initialized,pgs->name) ; */
    /*
    sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
    strcat(in_str,pgs->name) ; 
    strcat(in_str,"\n") ; 
    logMsg(in_str,0,0,0,0,0,0) ; 
    */
    if (!ec_initialized)  init_ec_config() ;
    /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */

    strcpy(filename, env_tempMon_filename);
    if ((tmFile = fopen(filename,"r")) == NULL) {
      trx_debug("Temperature Monior file could not be opened", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);   
    } else {
      NumFiles++ ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
      trx_debug("Reading param file.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
      /* Read the first line Cold 1 Upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,cold1_lo,cold1_hi) == -1) {
        trx_debug("Cold 1 upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->cold1_upper = numnum;
      }

      /* Read the second line Cold 2 upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,cold2_lo,cold2_hi) == -1) {
        trx_debug("Cold 2 Upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->cold2_upper = numnum;
      }

      /* Read the third line active upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,active_lo,active_hi) == -1) {
        trx_debug("Active sheild is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->active_upper = numnum;
      }

      /* Read the forth line passive upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,passive_lo,passive_hi) == -1) {
        trx_debug("Passive sheild upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->passive_upper = numnum;
      }

      /* Read the fifth line window upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,window_lo,window_hi) == -1) {
        trx_debug("Window upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->window_upper = numnum;
      }

      /* Read the sixth line strap upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,strap_lo,strap_hi) == -1) {
        trx_debug("Strap upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->strap_upper = numnum;
      }

      /* Read the seventh line edge upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,edge_lo,edge_hi) == -1) {
        trx_debug("Edge upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->edge_upper = numnum;
      }

      /* Read the Eigth line middle upper */
      fscanf(tmFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,tmFile); 
      if (UFcheck_double(numnum,middle_lo,middle_hi) == -1) {
        trx_debug("Middle upper is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->middle_upper = numnum;
      }
      fclose(tmFile) ;
      NumFiles-- ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    }
    pPriv->commandState = TRX_GS_DONE;    
  } else {
    trx_debug("",pgs->name,"FULL",ec_STATUS_DEBUG_MODE) ;
  
    status = unpack_array(pgs);
    pgs->noj = 8 ;

    trx_debug("Checking the values.",pgs->name,"FULL",ec_STATUS_DEBUG_MODE) ;
    /*Check the values unpacked against the limits */
 
    out_val = strtod(pgs->vala,&endptr) ;
    if (*(long *)pgs->a == -1) limit = pPriv->cold1_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->a,cold1_lo,cold1_hi) == -1) {
        trx_debug("Cold 1 upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->cold1_upper ;
      } else { 
        pPriv->cold1_upper = *(double *)pgs->a;
        limit = pPriv->cold1_upper;
        *(double *)pgs->a = -1 ;
      }
    }
    
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->vali,"TRUE") ;
    else strcpy(pgs->vali,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->vali,"FALSE") ;
    else strcpy(pgs->vali,"TRUE") ;
    */
    out_val = strtod(pgs->valb,&endptr) ;
    if (*(long *)pgs->b == -1) limit = pPriv->cold2_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->b,cold2_lo,cold2_hi) == -1) {
        trx_debug("Cold 2 upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->cold2_upper ;
      } else { 
        pPriv->cold2_upper = *(double *)pgs->b;
        limit = pPriv->cold2_upper;
        *(double *)pgs->b = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valj,"TRUE") ;
    else strcpy(pgs->valj,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valj,"FALSE") ;
    else strcpy(pgs->valj,"TRUE") ;
    */
    out_val = strtod(pgs->valc,&endptr) ;
    if (*(long *)pgs->c == -1) limit = pPriv->active_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->c,active_lo,active_hi) == -1) {
        trx_debug("Active upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->active_upper ;
      } else { 
        pPriv->active_upper = *(double *)pgs->c;
        limit = pPriv->active_upper;
        *(double *)pgs->c = -1 ;
      }
    }

    if ((out_val < limit)&& (out_val > 0)) strcpy(pgs->valk,"TRUE") ;
    else strcpy(pgs->valk,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valk,"FALSE") ;
    else strcpy(pgs->valk,"TRUE") ; 
    */

    out_val = strtod(pgs->vald,&endptr) ;
    if (*(long *)pgs->d == -1) limit = pPriv->passive_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->d,passive_lo,passive_hi) == -1) {
        trx_debug("Passive upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->passive_upper ;
      } else { 
        pPriv->passive_upper = *(double *)pgs->d;
        limit = pPriv->passive_upper ;
        *(double *)pgs->d = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->vall,"TRUE") ;
    else strcpy(pgs->vall,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->vall,"FALSE") ; 
    else strcpy(pgs->vall,"TRUE") ; 
    */
    out_val = strtod(pgs->vale,&endptr) ;
    if (*(long *)pgs->e == -1) limit = pPriv->window_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->e,window_lo,window_hi) == -1) {
        trx_debug("Window upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->window_upper ;
      } else { 
        pPriv->window_upper = *(double *)pgs->e;
        limit = pPriv->window_upper ;
        *(double *)pgs->e = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valm,"TRUE") ;
    else strcpy(pgs->valm,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valm,"FALSE") ; 
    else strcpy(pgs->valm,"TRUE") ; 
    */
    out_val = strtod(pgs->valf,&endptr) ;
    if (*(long *)pgs->f == -1) limit = pPriv->strap_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->f,strap_lo,strap_hi) == -1) {
        trx_debug("Strap upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->strap_upper ;
      } else { 
        pPriv->strap_upper = *(double *)pgs->f;
        limit = pPriv->strap_upper ;
        *(double *)pgs->f = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valn,"TRUE") ;
    else strcpy(pgs->valn,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valn,"FALSE") ; 
    else strcpy(pgs->valn,"TRUE") ; 
    */
    out_val = strtod(pgs->valg,&endptr) ;
    if (*(long *)pgs->g == -1) limit = pPriv->edge_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->g,edge_lo,edge_hi) == -1) {
        trx_debug("Edge upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->edge_upper ;
      } else { 
        pPriv->edge_upper = *(double *)pgs->g;
        limit = pPriv->edge_upper ;
        *(double *)pgs->g = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valo,"TRUE") ;
    else strcpy(pgs->valo,"FALSE") ;
    /* 
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valo,"FALSE") ; 
    else strcpy(pgs->valo,"TRUE") ; 
    */
    out_val = strtod(pgs->valh,&endptr) ;
    if (*(long *)pgs->h == -1) limit = pPriv->middle_upper ;
    else {
      if (UFcheck_double(*(double *)pgs->h,middle_lo,middle_hi) == -1) {
        trx_debug("Middle upper is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->middle_upper ;
      } else { 
        pPriv->middle_upper = *(double *)pgs->h;
        limit = pPriv->middle_upper ;
        *(double *)pgs->h = -1 ;
      }
    }
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valp,"TRUE") ;
    else strcpy(pgs->valp,"FALSE") ;
    /*
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valp,"FALSE") ; 
    else strcpy(pgs->valp,"TRUE") ; 
    */
    trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  }
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for pressMonG *******************/
long ufpressMonGinit (genSubRecord *pgs) {

  /* Gensub Private Structure Declaration */
  genSubPressurePrivate *pPriv;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  pPriv = (genSubPressurePrivate *) malloc (sizeof(genSubPressurePrivate));

  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /* trx_debug("Clearing input links.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ; */
  /* clear the input links */
  *(double *)pgs->a = -1.0 ;
  *(double *)pgs->b = -1.0 ;
  strcpy(pgs->j,"");

  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  /* set up a CALLBACK after 0.5 seconds in order to send it.
   *  set the Command state to SENDING
  */

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT) ;

  pgs->dpvt = (void *) pPriv;

  pgs->noj = 1 ;

  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ; */
  return OK ;
}

/******************** SNAM for pressMonG *******************/
long ufpressMonGproc (genSubRecord *pgs) {

  double out_val= 99999.999, limit;
  char *endptr ;
  genSubPressurePrivate *pPriv;

  /* Initialization File */
  FILE *pressFile ;
  char in_str[80] ;
  double numnum ;
  char filename[50] ;

  /*trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  pPriv = (genSubPressurePrivate *)pgs->dpvt ;
  /* if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* if (bingo) memShow(0) ; */
  if ((pPriv->commandState == TRX_GS_INIT) && (strcmp(pgs->j,"") == 0)) {
    
    /* printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
       printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
    /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ;  */
    if (!trecs_initialized)  init_trecs_config() ;
    /* printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

       printf("################### Started reading the EC config file %ld\n", tickGet()) ; */
    /* printf("######################### ec_initialized %d in %s\n",ec_initialized,pgs->name) ; */
    /*
    sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
    strcat(in_str,pgs->name) ; 
    strcat(in_str,"\n") ; 
    logMsg(in_str,0,0,0,0,0,0) ; 
    */
    if (!ec_initialized)  init_ec_config() ;
    /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */

    /* Read some initial parameters from a file */
    pPriv->press_ok_to_cool = -1.0 ;

    trx_debug("Attempting to read param file",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
    strcpy(filename,env_press_filename);
    if ((pressFile = fopen(filename,"r")) == NULL) {
      trx_debug("Pressure Initialization file could not be opened", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
    } else {
      NumFiles++ ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
      trx_debug("Reading param file",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
      /* Read the first line Pressure OK to cool */
      fscanf(pressFile,"%lf",&numnum);
      /* read till the end of the line */
      fgets(in_str,40,pressFile); 
      if (UFcheck_double(numnum,0,cryostat_ok_to_cool) == -1) {
        trx_debug("Press ok to cool is not valid", pgs->name,"MID",ec_STARTUP_DEBUG_MODE);
      } else { 
        pPriv->press_ok_to_cool = numnum;
      }

      fclose(pressFile) ;
      NumFiles-- ;
      /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ;*/
    }
    /* printf("------------------------------ cryostat_ok_to_cool is : %f\n",cryostat_ok_to_cool) ; */
    pPriv->commandState = TRX_GS_DONE;
  } else {

    unpack_array(pgs) ;
    pgs->noj = 1 ;
    trx_debug("Checking the values.",pgs->name,"FULL",ec_STARTUP_DEBUG_MODE) ;
  

    /*Check the values unpacked against the limits */
    out_val = strtod(pgs->vala,&endptr) ;
    /* if (bingo) printf("The pressure value I got is %f\n",out_val) ; */
    if (*(long *)pgs->a == -1) limit = pPriv->press_ok_to_cool ;
    else {
      if (UFcheck_double(*(double *)pgs->a,0,cryostat_ok_to_cool) == -1) {
        trx_debug("Pressure limit is not valid", pgs->name,"MID",ec_STATUS_DEBUG_MODE);
        limit = pPriv->press_ok_to_cool ;
      } else { 
        pPriv->press_ok_to_cool = *(double *)pgs->a;
        limit = pPriv->press_ok_to_cool;
        *(double *)pgs->a = -1 ;
      }
    }
    /* printf("end pointer is: (%s)\n ",endptr) ; */
    if ((out_val < limit) && (out_val > 0)) strcpy(pgs->valb,"TRUE") ;
    else strcpy(pgs->valb,"FALSE") ;
    /*
    printf("Limit is: %f , out val is: %f\n",limit, out_val) ;
    if ( (*endptr != '\0') || (out_val > limit) || ((long)limit == -1) ) strcpy(pgs->valb,"FALSE") ;  
    else strcpy(pgs->valb,"TRUE") ; 
    */
    trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  }
  return OK ;
}

/**********************************************************/
long ufECmrgCarGensubInit (genSubRecord *gsp) {
  long status = 0 ;
  char blank_str[] = "" ;
  int i ;
  int *last_command ;

  strcpy(gsp->a, blank_str) ; /* TempSetC VAL */
  strcpy(gsp->b, blank_str) ; /* TempSetC OMSS */
  strcpy(gsp->c, blank_str) ; /* chPowerC VAL */
  strcpy(gsp->d, blank_str) ; /* chPowerC OMSS */
  strcpy(gsp->e, blank_str) ; /* vacPowerC VAL */
  strcpy(gsp->f, blank_str) ; /* vacPowerC OMSS */

  /* if (bingo) 
     printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  mrgCarINIT\n") ; */
  last_command = (int *) malloc (sizeof(int)*3);
  for (i = 0; i<=2;i++) last_command[i] = CAR_IDLE ;
  strcpy(gsp->vald,"IDLE") ;
  strcpy(gsp->valc,"GOOD") ;

  gsp->dpvt = (void *) last_command ;
  return status ;
} 


/**********************************************************/
/**********************************************************/
/**********************************************************/
int status_changed (char *current, int last) {
  int changed = 0 ;
  switch (last) {
    case CAR_IDLE:
      if (strcmp(current,"IDLE") != 0) changed = 1;
      break;
    case CAR_BUSY:
      if (strcmp(current,"BUSY") != 0) changed = 1;
      break;

    case CAR_ERROR:
      if (strcmp(current,"ERR") != 0) changed = 1;
      break;
  }
  return changed ;
}

/**********************************************************/
long ufECmrgCarGensubProc (genSubRecord *gsp) {
  long status = 0 ;
  static char out_message[40];
  long action = CAR_IDLE;
  int *last_command = 0;

  memset( out_message, 0, 40 ) ;

  if ( (strcmp(gsp->a,"ERR") == 0) ||
	(strcmp(gsp->c,"ERR") == 0) ||
	(strcmp(gsp->e,"ERR") == 0) )
	{
	action = CAR_ERROR ;
	}

  if (action == CAR_IDLE) {
    if ( (strcmp(gsp->a,"BUSY") == 0) ||
	(strcmp(gsp->c,"BUSY") == 0) ||
	(strcmp(gsp->e,"BUSY") == 0)  )
	{
	action = CAR_BUSY ;
	}
  }
  
  /*Let's figure out the state of the system and its health */
  if (action == CAR_IDLE) {
    strcpy(gsp->vald,"IDLE") ;
    strcpy(gsp->valc,"GOOD") ;
  } else {
    if (action == CAR_BUSY) {
      strcpy(gsp->vald,"BUSY") ;
      strcpy(gsp->valc,"GOOD") ;
    } else {
      strcpy(gsp->vald,"ERROR") ;
      strcpy(gsp->valc,"BAD") ;
    }
  }
  /* Let's see if we can figure out the status of the last command */
  /* first let's find out which one changed */

  last_command = (int *) gsp->dpvt ;
  
  if (status_changed (gsp->a, last_command[0])) {
    if (strcmp(gsp->a,"IDLE") == 0) {
      action = CAR_IDLE ;
      last_command[0] = CAR_IDLE ;
    } else {
      if (strcmp(gsp->a,"BUSY") == 0) {
        action = CAR_BUSY ;
        last_command[0] = CAR_BUSY ;
      } else {
        action = CAR_ERROR ;
        last_command[0] = CAR_ERROR ;
        strcpy(out_message,gsp->b) ;
        /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
        if (*(long *)gsp->valb == CAR_IDLE) {

        }
      }
    }
  } else { /* Well link 'A' did not change, let keep going */
    if (status_changed (gsp->c, last_command[1])) {
      if (strcmp(gsp->c,"IDLE") == 0) {
        action = CAR_IDLE ;
        last_command[1] = CAR_IDLE ;
      } else {
        if (strcmp(gsp->c,"BUSY") == 0) {
          action = CAR_BUSY ;
          last_command[1] = CAR_BUSY ;
        } else {
          action = CAR_ERROR ;
          last_command[1] = CAR_ERROR ;
          strcpy(out_message,gsp->d) ;
          /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
          if (*(long *)gsp->valb == CAR_IDLE) {
          
          }
        }
      }
    } else {/* Well link 'C' did not change, let keep going */
      if (status_changed (gsp->e, last_command[2])) {
        if (strcmp(gsp->e,"IDLE") == 0) {
          action = CAR_IDLE ;
          last_command[2] = CAR_IDLE ;
        } else {
          if (strcmp(gsp->e,"BUSY") == 0) {
            action = CAR_BUSY ;
            last_command[2] = CAR_BUSY ;
          } else {
            action = CAR_ERROR ;
            last_command[2] = CAR_ERROR ;
            strcpy(out_message,gsp->f) ;
            /* do we need 0.5 delay here if the CAR is IDLE? or should we do a Callback?*/
            if (*(long *)gsp->valb == CAR_IDLE) {
            
            }
          }
        }
      }
    }
  }
  
  strcpy(gsp->vala,out_message); 
  *(long *)gsp->valb = action; 
 
  return status ;
} 

/******* Function to read the temperature init file *******/
long read_temp_file (char *rec_name, ufTempCurrParam *Param) {
  /* Initialization File */
  FILE *tcFile ;
  char in_str[80] ;
  double numnum ;
  char filename[50] ;
  /* char *some_str ; */
  int i;

  strcpy(filename, temp_cont_filename);
  if ((tcFile = fopen(filename,"r")) == NULL) {
    trx_debug("Temperature Controller file could not be opened", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    printf("did not open the file????????????????? \n") ;
    return -1 ;
  } else {
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ;*/
    /* Read the host IP number */
    fgets(in_str,40,tcFile);  /* comment line */
    fgets(in_str,40,tcFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    strcpy (ec_host_ip,in_str) ;
    i = 0;
    while ( ( (isdigit(ec_host_ip[i])) || (ec_host_ip[i] == '.')) && (i < 15)) i++ ;
    ec_host_ip[i] = '\0' ;

    /* Read the port number */
    fgets(in_str,40,tcFile); /* comment line */ 
   
    /* 
    fgets(in_str,40,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(tcFile,"%lf",&numnum) ;
    ec_port_no = (long) numnum ;

    /* Read the  "P VALUE" */
    fscanf(tcFile,"%lf",&numnum);
    /* read till the end of the line */
    fgets(in_str,40,tcFile); 
    if (UFcheck_double(numnum,P_value_lo,P_value_hi) == -1) {
      trx_debug("P value is not valid", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    } else { 
      Param->P = numnum;
    }

    /* Read the  "I VALUE" */
    fscanf(tcFile,"%lf",&numnum);
    /* read till the end of the line */
    fgets(in_str,40,tcFile); 
    if (UFcheck_double(numnum,I_value_lo,I_value_hi) == -1) {
      trx_debug("I value is not valid", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    } else { 
      Param->I = numnum;
    }

    /* Read the "D VALUE" */
    fscanf(tcFile,"%lf",&numnum);
    /* read till the end of the line */
    fgets(in_str,40,tcFile); 
    if (UFcheck_double(numnum,D_value_lo,D_value_hi) == -1) {
      trx_debug("D value is not valid", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    } else { 
      Param->D = numnum;
    }
   
    /* Read the "AUTO TUNE" */
    fscanf(tcFile,"%lf",&numnum);
    /* read till the end of the line */
    fgets(in_str,40,tcFile); 
    if (UFcheck_long_r((long)numnum,auto_tune_lo,auto_tune_hi) == -1) {
      trx_debug("Auto Tune value is not valid", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    } else { 
      Param->auto_tune = (long)numnum;
    }

    
    fclose(tcFile) ;  
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/******************** INAM for tempSetG *******************/
long uftempSetGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubTempPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
 
  

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubTempPrivate *) malloc (sizeof(genSubTempPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  
  pPriv->agent = 1;  /*temp agent */
  pPriv->CurrParam.set_point = -1.0 ;
  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */
  pPriv->CurrParam.heater_range = -1 ;

  /* trx_debug("Reading initial value from file", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Read some initial parameters from a file */
  pPriv->CurrParam.P = -1.0 ;
  pPriv->CurrParam.I = -1.0 ;
  pPriv->CurrParam.D = -1.0 ;
  pPriv->CurrParam.auto_tune = -1 ;
  
  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* Simulation Mode  */
  *(double *)pgs->b = -1.0 ; /* set point */
  *(long *)pgs->c = -1 ; /* heater range */
  
  /*  *(double *)pgs->d = -1.0 ; */  /* P */
  /* *(double *)pgs->e = -1.0 ; */  /* I */
  /* *(double *)pgs->f = -1.0 ; */  /* D */
  /* *(long *)pgs->g = -1 ; */  /* auto tune */

  strcpy(pgs->h,"NONE") ; /*debug mode start in NONE */
  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */

  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->temp_inp.command_mode   = -1 ;
  pPriv->temp_inp.set_point      = -1.0 ;
  pPriv->temp_inp.heater_range   = -1;
  pPriv->temp_inp.P              = -1.0;
  pPriv->temp_inp.I              = -1.0 ;
  pPriv->temp_inp.D              = -1.0 ;
  pPriv->temp_inp.auto_tune      = -1;

  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  

  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/******************** SNAM for tempSetG *******************/
long uftempSetGproc (genSubRecord *pgs) {

  genSubTempPrivate *pPriv = 0;
  long status = OK;
  char response[40] ;
  long some_num = 0;
  long ticksNow = 0;
  /* ufTempInput temp_inp ; */
  char *endptr = 0;
  int num_str = 0 ;
  static char **com ;
  int i = 0;
  char rec_name[31] ;
  double numnum = 0;
  /* char in_str[80] ; */

  strcpy(ec_DEBUG_MODE,pgs->h) ;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  pPriv = (genSubTempPrivate *)pgs->dpvt ;
  /* if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState); */
  /* if (bingo) memShow(0) ; */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /*printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
	printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (!trecs_initialized)  init_trecs_config() ;
      /*printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

	printf("################### Started reading the EC config file %ld\n", tickGet()) ; */
      /* printf("######################### ec_initialized %d in %s\n",ec_initialized,pgs->name) ; */
      /*
      sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
      strcat(in_str,pgs->name) ; 
      strcat(in_str,"\n") ; 
      logMsg(in_str,0,0,0,0,0,0) ; 
      */
      if (!ec_initialized)  init_ec_config() ;
      /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */
      /*
      read_temp_file(pgs->name,&pPriv->CurrParam) ; 
      pPriv->port_no = ec_port_no ; */ /* ZZZ */

      /* Clear outpus C-H for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      *(double *)pgs->valc = -1.0 ; /* set point */
      *(long *)pgs->vald = -1 ; /* heater range */
      *(double *)pgs->vale = pPriv->CurrParam.P ; /* P */
      *(double *)pgs->valf = pPriv->CurrParam.I ; /* I */
      *(double *)pgs->valg = pPriv->CurrParam.D ; /* D */
      *(long *)pgs->valh = pPriv->CurrParam.auto_tune ; /* auto tune */

      *(double *)pgs->d = pPriv->CurrParam.P ;   /* P */
      *(double *)pgs->e = pPriv->CurrParam.I ;  /* I */
      *(double *)pgs->f = pPriv->CurrParam.D ;  /* D */
      *(long *)pgs->g = pPriv->CurrParam.auto_tune ;  /* auto tune */
      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE);

      /* establish the connection and get the socket number */ 

      /* pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,ec_host_ip) ; */ /* ZZZ */
      pPriv->socfd = -1 ;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",ec_STARTUP_DEBUG_MODE); 
	} */ /* ZZZ */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */

      } 

      pPriv->send_init_param = 1;
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",ec_DEBUG_MODE) ; 
      trx_debug("Getting inputs A-F",pgs->name,"FULL",ec_DEBUG_MODE) ; 
      pPriv->temp_inp.command_mode   = *(long *)pgs->a ; 
      pPriv->temp_inp.set_point      = *(double *)pgs->b ; 
      pPriv->temp_inp.heater_range   = *(long *)pgs->c ; 

      if ( *(double *)pgs->d == pPriv->CurrParam.P) pPriv->temp_inp.P = -1.0 ;
      else pPriv->temp_inp.P = *(double *)pgs->d ;
      if ( *(double *)pgs->e == pPriv->CurrParam.I) pPriv->temp_inp.I = -1.0 ;
      else pPriv->temp_inp.I = *(double *)pgs->e ;
      if ( *(double *)pgs->f == pPriv->CurrParam.D)  pPriv->temp_inp.D = -1;
      else pPriv->temp_inp.D = *(double *)pgs->f ;
      if ( *(long *)pgs->g == pPriv->CurrParam.auto_tune) pPriv->temp_inp.auto_tune = -1;
      pPriv->temp_inp.auto_tune = *(long *)pgs->g;

      printf("**************My inputs are : \n") ;
      printf("Command Mode: %ld \n",pPriv->temp_inp.command_mode) ;
      printf("Set point: %f \n", pPriv->temp_inp.set_point) ;
      printf("Heater Range: %ld \n",pPriv->temp_inp.heater_range) ;

      if ( (pPriv->temp_inp.command_mode != -1) ||
           ((long)pPriv->temp_inp.set_point != -1) || 
           ((long)pPriv->temp_inp.P != -1) || 
           ((long)pPriv->temp_inp.I != -1) ||
           ((long)pPriv->temp_inp.D != -1) || 
           (pPriv->temp_inp.heater_range != -1) || 
           (pPriv->temp_inp.auto_tune != -1) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",ec_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",ec_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",ec_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-G",pgs->name,"FULL",ec_DEBUG_MODE) ;
        *(long *)pgs->a = -1; /*simulation mode */
        *(double *)pgs->b = -1.0 ; /* set point */
        *(long *)pgs->c = -1 ; /* heater range */
        *(double *)pgs->d = -1.0 ; /* P */
        *(double *)pgs->e = -1.0 ; /* I */
        *(double *)pgs->f = -1.0 ; /* D */
        *(long *)pgs->g = -1 ; /* auto tune */ 
      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        printf("??????? %s\n",response) ;
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",ec_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",ec_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
  
      /* Is it possible to get a CALLBACK processing here? */

      break;
    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",ec_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->temp_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",ec_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->temp_inp.command_mode == SIMM_NONE) ||
            (pPriv->temp_inp.command_mode == SIMM_FAST) ||
            (pPriv->temp_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufmotorGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", ec_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->temp_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          printf("My Command Mode is %ld \n",pPriv->temp_inp.command_mode) ;
          /* Read some initial parameters from a file */
          pPriv->CurrParam.P = -1.0 ;
          pPriv->CurrParam.I = -1.0 ;
          pPriv->CurrParam.D = -1.0 ;
          pPriv->CurrParam.auto_tune = -1 ;
          /* pPriv->command_mode = pPriv->temp_inp.command_mode ; */

          if (read_temp_file(pgs->name,&pPriv->CurrParam) == OK) pPriv->send_init_param = 1 ;
          pPriv->port_no = ec_port_no ;

          if (pPriv->temp_inp.command_mode != SIMM_FAST) {
	    if( pPriv->socfd < 0 ) { 
              trx_debug("Attempting to reconnect",pgs->name,"FULL",ec_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,ec_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",ec_DEBUG_MODE) ;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->temp_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            /* update the out links */
            *(long *)pgs->vald = pPriv->CurrParam.heater_range ;
            *(double *)pgs->vale = pPriv->CurrParam.P;
            *(double *)pgs->valf = pPriv->CurrParam.I;
            *(double *)pgs->valg = pPriv->CurrParam.D;
            *(long *)pgs->valh = pPriv->CurrParam.auto_tune ;

            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            } 
          }
	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        }
      } else { /* We have temperature set command */
        trx_debug("We have a tempSet Command",pgs->name,"FULL",ec_DEBUG_MODE) ;
        /* Save the current parameters */
        pPriv->OldParam.set_point = pPriv->CurrParam.set_point; 
        pPriv->OldParam.P = pPriv->CurrParam.P; 
        pPriv->OldParam.I = pPriv->CurrParam.I; 
        pPriv->OldParam.D = pPriv->CurrParam.D;
        pPriv->OldParam.heater_range = pPriv->CurrParam.heater_range; 
        pPriv->OldParam.auto_tune = pPriv->CurrParam.auto_tune;
        /* get the input into the current parameters */
        if ((long)pPriv->temp_inp.set_point != -1)  
          pPriv->CurrParam.set_point = pPriv->temp_inp.set_point  ; /* set point */
        if ((long)pPriv->temp_inp.P != -1) 
          pPriv->CurrParam.P = pPriv->temp_inp.P; /* P */
        if ((long)pPriv->temp_inp.I != -1) 
          pPriv->CurrParam.I = pPriv->temp_inp.I ; /* I */
        if ((long)pPriv->temp_inp.D != -1) 
          pPriv->CurrParam.D = pPriv->temp_inp.D ; /* D */
        if (pPriv->temp_inp.heater_range != -1) 
          pPriv->CurrParam.heater_range = pPriv->temp_inp.heater_range ; /* heater range */
        if (pPriv->temp_inp.auto_tune != -1) 
          pPriv->CurrParam.auto_tune =pPriv->temp_inp.auto_tune ; /* auto tune */
        /* see if this a command right after an init */
        if (pPriv->send_init_param) {
          pPriv->temp_inp.P = pPriv->CurrParam.P ;
          pPriv->temp_inp.I = pPriv->CurrParam.I ;
          pPriv->temp_inp.D = pPriv->CurrParam.D ;
          pPriv->temp_inp.auto_tune = pPriv->CurrParam.auto_tune ;
          pPriv->send_init_param = 0 ;
        }
        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;
        num_str = UFcheck_temp_inputs (pPriv->temp_inp,com, rec_name, &pPriv->CurrParam, 
                                             pPriv->CurrParam.command_mode) ;
        /* Clear the input structure */
        pPriv->temp_inp.command_mode   = -1 ;
        pPriv->temp_inp.set_point      = -1.0 ;
        pPriv->temp_inp.heater_range   = -1;
        pPriv->temp_inp.P              = -1.0;
        pPriv->temp_inp.I              = -1.0 ;
        pPriv->temp_inp.D              = -1.0 ;
        pPriv->temp_inp.auto_tune      = -1;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;
          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;

            *(double *)pgs->valc = pPriv->CurrParam.set_point ; /* set point */
            *(long *)pgs->vald = pPriv->CurrParam.heater_range ; /* heater range */
            *(double *)pgs->vale = pPriv->CurrParam.P ; /* P */
            *(double *)pgs->valf = pPriv->CurrParam.I ; /* I */
            *(double *)pgs->valg = pPriv->CurrParam.D ; /* D */
            *(long *)pgs->valh = pPriv->CurrParam.auto_tune ; /* auto tune */
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, 
                                 DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            else status = 0;
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
              strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_TEMPSET_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",ec_DEBUG_MODE) ;
          }
        }
      }
      

      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      printf("??????? %s\n",response) ;
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",ec_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",ec_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /*
      if (abort or stop) {
        cancelCallback (pPriv->pCallback);
        requestCallback(pPriv->pCallback,0 ) ; 
        pPriv->commandState = TRX_GS_SENDING;  
        return OK ; 
      }
      */ 
      strcpy(response,pgs->j) ;
      printf("??????? %s\n",response) ;
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_TEMPSET_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Temp Set Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",ec_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the out links */
          *(long *)pgs->vald = pPriv->CurrParam.heater_range ;
          *(double *)pgs->vale = pPriv->CurrParam.P;
          *(double *)pgs->valf = pPriv->CurrParam.I;
          *(double *)pgs->valg = pPriv->CurrParam.D;
          *(long *)pgs->valh = pPriv->CurrParam.auto_tune ;
          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else do an update if you can and exit */
          numnum = strtod(response,&endptr) ;
          if (*endptr != '\0'){ /* we do not have a number */
            cancelCallback (pPriv->pCallback);
            pPriv->commandState = TRX_GS_DONE;
            /* restore the old parameters */
            pPriv->CurrParam.P = pPriv->OldParam.P ;
            pPriv->CurrParam.I = pPriv->OldParam.I ;
            pPriv->CurrParam.D = pPriv->OldParam.D ;
            pPriv->CurrParam.heater_range = pPriv->OldParam.heater_range;
            pPriv->CurrParam.auto_tune =pPriv-> OldParam.auto_tune;

            /* set the CAR to ERR with whatever the Agent sent you */
            strcpy (pPriv->errorMessage, response);
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
          } else { /* update the out link */
            *(double *)pgs->valc = numnum ;
          }
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  return OK ;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/*********function to read the motor file *****************/
long read_ch_vac_file (char *rec_name, genSubECVacChPrivate *pPriv) {
  /* Initialization File */
  FILE *ch_vacFile ;
  char in_str[80] ;
  char filename[50] ;
  double numnum ; /*, numnum1, numnum2 ; */
  int i;
 
  if (pPriv->agent == 2) strcpy(filename,vac_param_filename);
  else strcpy(filename,ch_param_filename);

  if ((ch_vacFile = fopen(filename,"r")) == NULL) {
    trx_debug("CH/VAC file could not be opened", rec_name,"MID",ec_STARTUP_DEBUG_MODE);
    return -1 ;
  } else {
        /* Read the host IP number */
    NumFiles++ ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
    fgets(in_str,40,ch_vacFile);  /* comment line */
    fgets(in_str,40,ch_vacFile);  /* comment line */
    fgets(in_str,40,ch_vacFile);  /* comment line */
    fgets(in_str,40,ch_vacFile) ; /* IP number */
    /*
    do { 
      some_str = strrchr(in_str,' ') ; 
      if (some_str != NULL)  some_str[0] = '\0' ;       
    } while( some_str != NULL) ; 
    */ 

    if (pPriv->agent == 2) {
      strcpy (vac_host_ip,in_str) ; 
      i = 0;
      while ( ( (isdigit(vac_host_ip[i])) || (vac_host_ip[i] == '.')) && (i < 15)) i++ ;
      vac_host_ip[i] = '\0' ;
    } else {
      strcpy (ch_host_ip,in_str) ;
      i = 0;
      while ( ( (isdigit(ch_host_ip[i])) || (ch_host_ip[i] == '.')) && (i < 15)) i++ ;
      ch_host_ip[i] = '\0' ;
    }
    /* Read the port number */
    fgets(in_str,40,ch_vacFile); /* comment line */ 
   
    /* 
    fgets(in_str,40,tcFile);  
    printf("@@@@@@@ %s \n",in_str) ;  
    */ 

    fscanf(ch_vacFile,"%lf",&numnum) ;

    if (pPriv->agent == 2) vac_port_no = (long) numnum ;
    else ch_port_no = (long) numnum ;
    
    fclose(ch_vacFile) ; 
    NumFiles-- ;
    /* printf("\n#*#*#*#*#*#*#*#*#*#* The number of files is %d\n \n",NumFiles) ; */
  }
  return OK ;

}

/**********************************************************/
/**********************************************************/
/**********************************************************/
/************* INAM for chPowerG and vacPowerG ************/
long ufdevPowerGinit (genSubRecord *pgs) {
  /* Variable Declarations */

  /* Gensub Private Structure Declaration */
  genSubECVacChPrivate *pPriv;
  /* String Buffer */
  char buffer[MAX_STRING_SIZE];
  /* Status Variable */
  long status = OK;
 
  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  /*
   *  Create a private control structure for this gensub record
  */
  /* trx_debug("Creating private structure", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  pPriv = (genSubECVacChPrivate *) malloc (sizeof(genSubECVacChPrivate));
  if (pPriv == NULL) {
    trx_debug("Can't create private structure", pgs->name,"NONE","NONE");
    return -1;
  }

  /*
   *  Locate the associated CAR record fields and save their
   *  addresses in the private structure.....
  */
  
  /* trx_debug("Locating CAR Information", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  strcpy (buffer, pgs->name); 
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IVAL");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carState);
  if (status) {
    trx_debug("can't locate CAR IVAL field", pgs->name,"NONE","NONE");
    return -1;
  }

  strcpy (buffer, pgs->name);
  buffer[strlen(buffer) - 1] = '\0';
  strcat (buffer, "C.IMSS");

  status = dbNameToAddr (buffer, &pPriv->carinfo.carMessage);
  if (status) {
    trx_debug ("can't locate CAR IMSS field", pgs->name,"NONE","NONE");
    return -1;
  }
  
  /* trx_debug("Assigning initial parameters", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Assign private information needed */
  
  if (strstr(pgs->name, "vac") != NULL) pPriv->agent = 2;  /*pressure agent*/
  else 
    if (strstr(pgs->name, "ch") != NULL) pPriv->agent = 5;  /*cold head agent*/
    else pPriv->agent = -1; /* who is calling me here? */
  pPriv->CurrParam.command_mode = SIMM_NONE ; /* Simulation Mode mode (start in real mode) */
  strcpy(pPriv->CurrParam.power,"UNKNOWN") ; 

  /* trx_debug("Clearing Input links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Clear the Gensub inputs */
  *(long *)pgs->a = -1 ; /* Simulation Mode  */
  strcpy(pgs->c, "") ; /* power command */
  strcpy(pgs->j,"") ;  /* response from the agent via CA */
                       /* or the task that receives the response via TCP/IP */
  strcpy(pgs->e,"NONE") ;
  /* trx_debug("Clearing output links", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  /* Create a callback for this gensub record */
  /* trx_debug("Creating callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */
  
  pPriv->pCallback = initCallback (trecsGensubCallback);

  if (pPriv->pCallback == NULL) {
    trx_debug ("can't create a callback", pgs->name,"NONE","NONE");
    return -1;
  }

  pPriv->pCallback->pRecord = (struct dbCommon *)pgs;

  pPriv->commandState = TRX_GS_INIT;
  requestCallback(pPriv->pCallback,TRX_INIT_EC_AGENT_WAIT) ;
  /* trx_debug("Created callback", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE); */

  /* Clear the input structure */
  pPriv->ecVacCh_inp.command_mode   = -1 ;
  strcpy(pPriv->ecVacCh_inp.power,"");

  /*
  *  Save the private control structure in the record's device
  *  private field.
  */
 
  pgs->dpvt = (void *) pPriv;

  /*
  *  And do some last minute initialization stuff
  */
  

  /* trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;*/
  return OK ;
}

/********** SNAM for chPowerG and vacPowerG ***************/
long ufdevPowerGproc (genSubRecord *pgs) {

  genSubECVacChPrivate *pPriv;
  long status = OK;
  char response[40] ;
  long some_num;
  long ticksNow ; 
  /* ufTempInput temp_inp ; */
  /* char *endptr ; */
  int num_str = 0 ;
  static char **com ;
  int i; 
  char rec_name[31] ;
  /* double numnum ; */
  /* char in_str[80] ; */

  strcpy(ec_DEBUG_MODE,pgs->e) ;

  /* trx_debug("",pgs->name,"FULL",ec_DEBUG_MODE) ; */

  pPriv = (genSubECVacChPrivate *)pgs->dpvt ;
  if (bingo) printf("I am inside %s and my command state is %d\n",pgs->name, pPriv->commandState);
  /* if (bingo) memShow(0) ; */
  /* 
   *  How the record is processed depends on the current command
   *  execution state...
   */

  switch (pPriv->commandState) {

    case TRX_GS_INIT:
      /* printf("################### Started reading the TRX config file %ld\n", tickGet()) ;
	 printf("######################### trecs_initialized %d \n",trecs_initialized) ; */
      /* printf("######################### trecs_initialized %d in %s\n",trecs_initialized,pgs->name) ; */
      if (!trecs_initialized)  init_trecs_config() ;
      /* printf("################### Ended reading the TRX config file %ld\n", tickGet()) ;

	 printf("################### Started reading the EC config file %ld\n", tickGet()) ;*/
      /* printf("######################### ec_initialized %d in %s\n",ec_initialized,pgs->name) ; */
      /*
      sprintf(in_str,"######################### ec_initialized %d in ",ec_initialized) ; 
      strcat(in_str,pgs->name) ; 
      strcat(in_str,"\n") ; 
      logMsg(in_str,0,0,0,0,0,0) ; 
      */
      if (!ec_initialized)  init_ec_config() ;
      /* printf("################### Ended reading the EC config file %ld\n", tickGet()) ; */
      /* 
      if (pPriv->agent == 2) { 
        read_ch_vac_file(pgs->name,pPriv) ; 
        pPriv->port_no = vac_port_no ; 
      } else { 
        read_ch_vac_file(pgs->name,pPriv) ; 
        pPriv->port_no = ch_port_no ; 
      }
      */ /* ZZZ */
      /* Clear outpus C-H for temp Gensub */

      *(long *)pgs->vala = SIMM_NONE ; /* command mode (start in real mode) */
      *(long *)pgs->valb = -1 ; /*  socket Number */
      strcpy(pgs->valc,"UNKNOWN") ; /* power status */

      trx_debug("Attempting to establish a connection to the agent", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE);
  
      /* establish the connection and get the socket number */
      /* 
      if( pPriv->agent == 2 )
        pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,vac_host_ip) ; 
      else
        pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,ch_host_ip) ; 
      */ /* ZZZ */

      pPriv->socfd = -1;
      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */

      /* 
      if (pPriv->socfd < 0) { 
        trx_debug("There was an error connecting to the agent.", pgs->name,"MID",ec_STARTUP_DEBUG_MODE); 
      }
      */

      /* Spawn a task to be the receiver from the agent */
      if ( (pPriv->socfd > 0) && (RECV_MODE)) {
        trx_debug("Spawning a task to receive TCP/IP", pgs->name,"FULL",ec_STARTUP_DEBUG_MODE);
        /* spawn the task to receive via TCP/IP */

      } 

      /* pPriv->send_init_param = 1; */
      pPriv->commandState = TRX_GS_DONE;

      break;

    /*
     *  In idle state we wait for one of the inputs to 
     *  change indicating that a new command has arrived. Status
     *  and updating from the Agent comes during BUSY state.
     */
    case TRX_GS_DONE:
      /* Check to see if the input has changed. */ 
      trx_debug("Processing from DONE state",pgs->name,"FULL",ec_DEBUG_MODE) ;
      trx_debug("Getting inputs A-F",pgs->name,"FULL",ec_DEBUG_MODE) ;
      pPriv->ecVacCh_inp.command_mode   = *(long *)pgs->a ;
      strcpy (pPriv->ecVacCh_inp.power, pgs->c) ;
      /*
      printf("**************My inputs are : \n") ; 
      printf("Command Mode: %ld \n",pPriv->ecVacCh_inp.command_mode) ; 
      printf("Power: %s \n", pPriv->ecVacCh_inp.power) ; 
      */
      if ( (pPriv->ecVacCh_inp.command_mode != -1) ||
           (strcmp(pPriv->ecVacCh_inp.power,"") != 0) ) {
        /* A new command has arrived so set the CAR to busy */
        strcpy (pPriv->errorMessage, "");
        status = dbPutField (&pPriv->carinfo.carMessage, 
                             DBR_STRING, 
                              pPriv->errorMessage,1);
        if (status) {
          trx_debug("can't clear CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
          return OK;
        }
	trx_debug("Setting the car to BUSY.",pgs->name,"FULL",ec_DEBUG_MODE) ;
        some_num = CAR_BUSY ;
        status = dbPutField (&pPriv->carinfo.carState, 
                             DBR_LONG, &some_num,1);
        if (status) {
          trx_debug("can't set CAR to busy",pgs->name,"NONE",ec_DEBUG_MODE) ;
          return OK;
        }
        /* set up a CALLBACK after 0.5 seconds in order to send it.
        *  set the Command state to SENDING
        */
        trx_debug("Setting Command State",pgs->name,"FULL",ec_DEBUG_MODE) ;
        pPriv->commandState = TRX_GS_SENDING;
        requestCallback(pPriv->pCallback,TRX_ERROR_DELAY ) ;
        /* Clear the input links */
        trx_debug("Clearing inputs A-G",pgs->name,"FULL",ec_DEBUG_MODE) ;
        *(long *)pgs->a = -1; /*simulation mode */
        strcpy(pgs->c,"") ; /* power */
      } else {
        /*
         *  Check to see if the agent response input has changed.
         */
        strcpy(response,pgs->j) ;
        printf("??????? %s\n",response) ;
        if (strcmp(response,"") != 0) {
          /* in this state we should not expect anything in the J 
           * field from the Agent unless the agent is late with something
           */
          /* Log a message that the agent responded unexpectedly */
          trx_debug("Unexpected response from Agent:DONE",pgs->name,"NONE",ec_DEBUG_MODE) ;
        } else {
          /* OK. So somone processed the GENSUB without inputs or the inputs
           * are the same as the clear directives
           * Why would anyone do that? I know why but I won't tel.
           */
          trx_debug("Unexpected processing:DONE",pgs->name,"NONE",ec_DEBUG_MODE) ;
          /* This happens because of the CLEAR directive for the CAD's .
           * it put zero or empty strings on its out[put link but will not push
           * them out.  Because we have records that PULL those values when
           * the CAD receives a START directive, those valuses are PUSHED to the
           * GENSUB. Freaky stuff.
	   * So to fix that, the CAD's are programmed to write clear values (-1, 0 or "")
           * on their output links when they receive an empty string as input and the START
           * directive is executed.
           * It's not my fault this happens. is it?!!
	   */
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
  
      /* Is it possible to get a CALLBACK processing here? */

      break;
    case TRX_GS_SENDING:
      trx_debug("Processing from SENDING state",pgs->name,"FULL",ec_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* if so then send the abort command to the Agent */
      /* Is this a CALLBACK to send a command or do an INIT? */
      if (pPriv->ecVacCh_inp.command_mode != -1) { /* We have an init command */
        trx_debug("We have an INIT Command",pgs->name,"FULL",ec_DEBUG_MODE) ;
        /* Check first to see if we have a valid INIT directive */
        if ((pPriv->ecVacCh_inp.command_mode == SIMM_NONE) ||
            (pPriv->ecVacCh_inp.command_mode == SIMM_FAST) ||
            (pPriv->ecVacCh_inp.command_mode == SIMM_FULL) ) {
          /* need to reconnect to the agent ? -- hon */
	  if( pPriv->socfd >= 0 ) {
	    if( checkSoc( pPriv->socfd, 0.0 ) > 0 ) {
	      printf("ufmotorGproc> socket is writable, no need to reconnect...\n");
	    }
	    else {
              trx_debug("Closing curr connection",pgs->name,"FULL", ec_DEBUG_MODE) ;
              ufClose(pPriv->socfd) ;
              pPriv->socfd = -1;
              *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
	  }
          pPriv->CurrParam.command_mode = pPriv->ecVacCh_inp.command_mode; /* simulation level */
          *(long *)pgs->vala = (long)pPriv->CurrParam.command_mode ;
          /* Re-Connect if needed */
          /* printf("My Command Mode is %ld \n",pPriv->temp_inp.command_mode) ; */
          /* Read some initial parameters from a file */
          /* 
          pPriv->CurrParam.P = -1.0 ; 
          pPriv->CurrParam.I = -1.0 ; 
          pPriv->CurrParam.D = -1.0 ; 
          pPriv->CurrParam.auto_tune = -1 ;
          */
          /* pPriv->command_mode = pPriv->temp_inp.command_mode ; */
          if (pPriv->agent == 2) {
            read_ch_vac_file(pgs->name,pPriv) ;
            pPriv->port_no = vac_port_no ;
          } else {
            read_ch_vac_file(pgs->name,pPriv) ;
            pPriv->port_no = ch_port_no ;
          }
          strcpy(pPriv->CurrParam.power,"UNKNOWN") ;
	  /* if (read_temp_file(pgs->name,&pPriv->CurrParam) == OK) pPriv->send_init_param = 1 ;
	     pPriv->port_no = ec_port_no ; */

          if (pPriv->ecVacCh_inp.command_mode != SIMM_FAST) {
	    if( pPriv->socfd < 0 ) {
              trx_debug("Attempting to reconnect",pgs->name,"FULL",ec_DEBUG_MODE) ;
              pPriv->socfd = UFGetConnection(pgs->name, pPriv->port_no,ec_host_ip) ;
              trx_debug("came back from connect",pgs->name,"FULL",ec_DEBUG_MODE) ;
 	      *(long *)pgs->valb = (long)pPriv->socfd ; /* current socket number */
	    }
            if (pPriv->socfd < 0) { /* we have a bad socket connection */
              pPriv->commandState = TRX_GS_DONE;
              strcpy (pPriv->errorMessage, "Error Connecting to Agent");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
            }
          }
          /* update output links if connection is good or we are in SIMM_FAST*/
          if ( (pPriv->ecVacCh_inp.command_mode == SIMM_FAST) || (pPriv->socfd > 0) ) {
            strcpy(pgs->valc,pPriv->CurrParam.power) ;
            /* update the out links */
            /*
            *(long *)pgs->vald = pPriv->CurrParam.heater_range ; 
            *(double *)pgs->vale = pPriv->CurrParam.P; 
            *(double *)pgs->valf = pPriv->CurrParam.I; 
            *(double *)pgs->valg = pPriv->CurrParam.D; 
            *(long *)pgs->valh = pPriv->CurrParam.auto_tune ; 
            */
            /* Reset Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
            if (status) {
              trx_debug("can't set CAR to IDLE",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            } 
          }
          printf("Clearing the input structure\n") ;
          /* Clear the input structure */
          pPriv->ecVacCh_inp.command_mode   = -1 ;
          strcpy(pPriv->ecVacCh_inp.power,"") ;

	} else { /* we have an error in input of the init command */
          /* Reset Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
          strcpy (pPriv->errorMessage, "Bad INIT Directive");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        }
      } else { /* We have temperature set command */
        printf("We have a power command \n") ;
        trx_debug("We have a Power Command",pgs->name,"FULL",ec_DEBUG_MODE) ;
        /* Save the current parameters */
        strcpy(pPriv->OldParam.power, pPriv->CurrParam.power); 
        /* pPriv->OldParam.P = pPriv->CurrParam.P; 
        pPriv->OldParam.I = pPriv->CurrParam.I;  
        pPriv->OldParam.D = pPriv->CurrParam.D; 
        pPriv->OldParam.heater_range = pPriv->CurrParam.heater_range;  
        pPriv->OldParam.auto_tune = pPriv->CurrParam.auto_tune; */
        /* get the input into the current parameters */
        if (strcmp(pPriv->ecVacCh_inp.power,"") != 0)   
          strcpy(pPriv->CurrParam.power, pPriv->ecVacCh_inp.power);

        /* see if this a command right after an init */
        /*
        if (pPriv->send_init_param) { 
          pPriv->temp_inp.P = pPriv->CurrParam.P ; 
          pPriv->temp_inp.I = pPriv->CurrParam.I ; 
          pPriv->temp_inp.D = pPriv->CurrParam.D ; 
          pPriv->temp_inp.auto_tune = pPriv->CurrParam.auto_tune ; 
          pPriv->send_init_param = 0 ; 
        } 
        */
        /* formulate the command strings */
        com = malloc(50*sizeof(char *)) ;
        for (i=0;i<50;i++) com[i] = malloc(50*sizeof(char)) ;
        strcpy(rec_name,pgs->name) ;
        rec_name[strlen(rec_name)] = '\0' ;
        strcat(rec_name,".J") ;
        num_str = UFcheck_ecVacCh_inputs (pPriv->ecVacCh_inp,com, rec_name, &pPriv->CurrParam, 
                                             pPriv->CurrParam.command_mode, pPriv->agent) ;
        /* Clear the input structure */
        pPriv->ecVacCh_inp.command_mode   = -1 ;
        strcpy(pPriv->ecVacCh_inp.power,"") ;

        /* do we have commands to send? */
        if (num_str > 0) {
          /* if no Agent needed then go back to DONE */
          /* we are talking about SIMM_FAST or commands that do not need the agent */
          for (i=0;i<num_str;i++) printf("%s\n",com[i]) ;
          if ((pPriv->CurrParam.command_mode == SIMM_FAST)) { 
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;

            strcpy(pgs->valc,pPriv->CurrParam.power) ; /* power setting */
            some_num = CAR_IDLE ;
            status = dbPutField (&pPriv->carinfo.carState, 
                                 DBR_LONG, &some_num, 1);
            if (status) {
              logMsg ("can't set CAR to IDLE\n",0,0,0,0,0,0);
              return OK;
            }
          } else { /* we are in SIMM_NONE or SIMM_FULL */
            /* See if you can send the command and go to BUSY state */
            /* status = UFAgentSend(pgs->name,pPriv->socfd, num_str,  com) ; */
            if (pPriv->socfd > 0) status = ufStringsSend(pPriv->socfd, com, num_str, pgs->name) ;
            else status = 0 ;
            for (i=0;i<50;i++) free(com[i]) ;
            free(com) ;
            if (status == 0) { /* there was an error sending the commands to the agent */
              /* set Command state to DONE */
              pPriv->commandState = TRX_GS_DONE;
              /* set the CAR to ERR with appropriate message like 
               * "Bad socket"  and go to DONE state*/
              strcpy (pPriv->errorMessage, "Bad socket connection");
              /* set the CAR to ERR */
              status = dbPutField (&pPriv->carinfo.carMessage, 
                                   DBR_STRING, pPriv->errorMessage,1);
              if (status) {
                trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
              some_num = CAR_ERROR ;
              status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                             &some_num,1);
              if (status) {
                trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
                return OK;
              }
            } else { /* send successful */
              /* establish a CALLBACK */
              requestCallback(pPriv->pCallback,TRX_ECVACSET_TIMEOUT ) ;
              pPriv->startingTicks = tickGet() ;
              /* set Command state to BUSY */
              pPriv->commandState = TRX_GS_BUSY;
            }
          }
        } else {
          for (i=0;i<50;i++) free(com[i]) ;
          free(com) ;
          if (num_str < 0) {
            /* set Command state to DONE */
            pPriv->commandState = TRX_GS_DONE;
            /* we have an error in the Input */
            strcpy (pPriv->errorMessage, "Error in input");
            /* set the CAR to ERR */
            status = dbPutField (&pPriv->carinfo.carMessage, 
                                 DBR_STRING, pPriv->errorMessage,1);
            if (status) {
              trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
            some_num = CAR_ERROR ;
            status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                           &some_num,1);
            if (status) {
              trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
              return OK;
            }
          } else { /* num_str == 0 */
            /* is it possible to get here? */
            /* here we have a change in input.  The inputs though got sent
             * to be checked but no string got formulated.  */
            /* this can only happen if somehow the memory got corrupted */
            /* or the gensub is being processed by an alien. */
            /* Weird. */
            trx_debug("Unexpected processing:SENDING",pgs->name,"NONE",ec_DEBUG_MODE) ;
          }
        }
      }
      

      /*Is this a J field processing? */
      strcpy(response,pgs->j) ;
      printf("??????? %s\n",response) ;
      if (strcmp(response,"") != 0) {
        /* in this state we should not expect anything in the J 
         * field from the Agent unless the agent is late with something
         */
        /* Log a message that the agent responded unexpectedly */
        trx_debug("Unexpected response from Agent:SENDING",pgs->name,"NONE",ec_DEBUG_MODE) ;
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;

    case TRX_GS_BUSY:
      trx_debug("Processing from BUSY state",pgs->name,"FULL",ec_DEBUG_MODE) ;
      /* is this a processing with STOP or ABORT? */
      /* Then go to SENDING (CALLBACK) immediately. No need for 0.5 delay */
      /* establish a CALLBACK */
      /*
      if (abort or stop) {
        cancelCallback (pPriv->pCallback);
        requestCallback(pPriv->pCallback,0 ) ; 
        pPriv->commandState = TRX_GS_SENDING;  
        return OK ; 
      }
      */ 
      strcpy(response,pgs->j) ;
      printf("??????? %s\n",response) ;
      if (strcmp(response,"") == 0) {
        /* is this a CALLBACK timeout? or someone pushed the apply?*/
        printf("******** Is this a callback from timeout?\n") ;  
        printf("%s\n",pPriv->errorMessage) ;
        /* if CALLBACK timeout, then set CAR to ERR and go to DONE state */
        ticksNow = tickGet() ;
        if ( (ticksNow >= (pPriv->startingTicks + TRX_ECVACSET_TIMEOUT)) &&
             (strcmp(pPriv->errorMessage, "CALLBACK")==0) ) {
          /* set Command state to DONE */
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, "Cold Head or Vac Set Command Timed out");
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        } else {
          /* Quit playing with the buttons. Can't you see I am BUSY? */
          trx_debug("Unexpected processing:BUSY",pgs->name,"NONE",ec_DEBUG_MODE) ;
        }

      } else {
        /* What do we have from the Agent? */
        /* if Agent is done then 
         * cancel the call back
         * go to DONE state and
         * set CAR to IDLE */
        if (strcmp(response,"OK") == 0) {
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
          /* update the out links */
          strcpy(pgs->valc,pPriv->CurrParam.power) ;
          some_num = CAR_IDLE ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                       &some_num,1);
          if (status) {
            trx_debug("can't set CAR to IDLE",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
        } else { 
          /* else we have an error from the agent */
          cancelCallback (pPriv->pCallback);
          pPriv->commandState = TRX_GS_DONE;
	  /* The command timed out */
          strcpy (pPriv->errorMessage, response);
          /* set the CAR to ERR */
          status = dbPutField (&pPriv->carinfo.carMessage, 
                               DBR_STRING, pPriv->errorMessage,1);
          if (status) {
            trx_debug("can't set CAR message",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          some_num = CAR_ERROR ;
          status = dbPutField (&pPriv->carinfo.carState, DBR_LONG, 
                                         &some_num,1);
          if (status) {
            trx_debug("can't set CAR to ERROR",pgs->name,"NONE",ec_DEBUG_MODE) ;
            return OK;
          }
          
        }
      }
      /* Clear the J Field */
      strcpy(pgs->j,"") ;
      break;
  } /*switch (pPriv->commandState) */

  trx_debug("Exiting",pgs->name,"FULL",ec_DEBUG_MODE) ;
  return OK ;
}





#endif /* __UFGEMGENSUBCOMMEC_C__  */
