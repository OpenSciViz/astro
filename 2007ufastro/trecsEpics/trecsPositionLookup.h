
/* INDENT OFF */
/*+
 *
 * FILENAME
 * -------- 
 * trecsPositionLookup.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument position lookup
 * table module
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2001/12/22  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


#define NAME_SIZE  32


/*
 *
 *  Public function prototypes
 *
 */

long trecsInitPositionLookup (void);

long trecsReloadFilterTable (char *fileName);

long trecsReloadDeviceTable (char *fileName);

long trecsFilterLookup (char *filterName, 
                        char *filter1,
                        char *filter2, 
                        char *window);

long trecsPositionLookup (char *deviceName, 
                          char *positionName,
                          char *positionNumber,
                          float *throughput,
                          float *lambdaLow,
                          float *lambdaHigh);


