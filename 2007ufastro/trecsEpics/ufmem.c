#if !defined(__UFMEM__)
#define __UFMEM__ "$Name:  $ $Id: ufmem.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsId[] = __UFMEM__;

/* uf modification of "memShow" func. in epics 3.12Gem */

#include "vxWorks.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "timexLib.h"

#include "ellLib.h"
#include "fast_lock.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "dbBase.h"
#include "dbRecType.h"
#include "dbRecords.h"
#include "dbCommon.h"
#include "recSup.h"
#include "devSup.h"
#include "drvSup.h"
#include "choice.h"
#include "special.h"
#include "dbRecDes.h"
#include "dbStaticLib.h"
#include "dbEvent.h"
#include "ellLib.h"
#include "callback.h"
#include "memLib.h"



long ufmemShow() {
  memShow(0) ;
  return OK;
}


#endif /* __UFMEM__ */


