
/* ufGemGensubComm.h RCS: $NAME$  $ID$ */
#if !defined(__UFGEMGENSUBCOMM_H__)
#define __UFGEMGENSUBCOMM_H__ "RCS: $Name:  $ $Id: ufGemGensubComm.h,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufTUFGEMGENSUBCOMMH[] = __UFGEMGENSUBCOMM_H__ ;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <tickLib.h>
#include <msgQLib.h>
#include <taskLib.h>
#include <logLib.h>

#include <dbAccess.h>
#include <recSup.h>
#include <callback.h>
#include <wdLib.h>
#include <genSub.h>
#include <genSubRecord.h>
#include <trecs.h>

#define  TRX_CMD1    0x0001
#define  TRX_CMD2    0x0002
#define  TRX_CMD3    0x0004
#define  TRX_CMD4    0x0008
#define  TRX_CMD5    0x0010
#define  TRX_CMD6    0x0020
#define  TRX_CMD7    0x0040
#define  TRX_CMD8    0x0080
#define  TRX_CMD9    0x0100
#define  TRX_CMD10   0x0200

#define TRX_COMMAND_TIMEOUT  600   /* 10 seconds maximum busy        */
#define TRX_TEMPSET_TIMEOUT  11100  /* 3 minutes and 5 sec to set the temperature */
#define TRX_ECVACSET_TIMEOUT 900 /* 15 seconds for the agent to turn on or off the Vacuum or the cold head */
#define TRX_CC_AGENT_TIMEOUT    1800   /* 30 seconds between updates to finish moving the motor */
#define TRX_MOTOR_TIMEOUT    3600   /* a minute to finish moving the motor */
#define TRX_MOTOR_HOMING_TIMEOUT    10800   /* 3 minute to finish homing the motor */
#define TRX_DC_TIMEOUT       900   /* 15 seconds for the agents to do the calculations */
#define TRX_ERROR_DELAY      30    /* 0.5 seconds minimum busy       */
#define TRX_INIT_FILE_WAIT   1200   /* 20 seconds for Gensub that will read from files */
#define TRX_INIT_EC_AGENT_WAIT  450  /* 20 seconds for the EC */
#define TRX_INIT_DC_AGENT_WAIT  1500  /* 25  seconds for the DC */
#define TRX_INIT_CC_AGENT_WAIT  1800   /* 30 seconds for Gensub that connect to agents */
#define TRX_BT_TIMEOUT       3600   /* one minute for the agents to do the calculations */

#define TRX_GS_INIT           1    /* INITIALIZING State */
#define TRX_GS_DONE           2    /* IDLE/DONE State */
#define TRX_GS_SENDING        3    /* SENDING State */
#define TRX_GS_BUSY           4    /* BUSY State */

/* Receive information from the agent */
#define RECV_MODE          0 /* 0 for Channel Access and
                                1 for TCP/IP  */

typedef struct
{
    CALLBACK         callback;
    struct dbCommon  *pRecord;
    WDOG_ID          watchdogId;
    int              exec_state ;
} GENSUB_CALLBACK;

typedef struct 
{
    long             commandState;
    char             errorMessage[MAX_STRING_SIZE];
    struct dbAddr    carState;
    struct dbAddr    carMessage;
    GENSUB_CALLBACK  *pCallback;
} GENSUB_PRIVATE;



typedef struct {
  char carName[30] ;
  struct dbAddr carState;    /* state of the Car attached to the genSub */
  struct dbAddr carMessage ; /* message to the Car record */

} ufCarInfo;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

} genSubCommPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  char indexer ;
  long mot_num ;
  ufMotorInput mot_inp;
  ufMotorCurrParam CurrParam ;
  ufMotorCurrParam OldParam ;
} genSubMotPrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  ufTempInput temp_inp ;
  ufTempCurrParam CurrParam ;
  ufTempCurrParam OldParam;
} genSubTempPrivate ;



typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  double press_ok_to_cool ;
} genSubPressurePrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  double array_upper ;
  double array_lower ;
  double cold_finger_upper;
} genSubArrayPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  double cold1_upper;
  double cold2_upper;
  double active_upper;
  double passive_upper;
  double window_upper;
  double strap_upper;
  double edge_upper;
  double middle_upper;
} genSubTempMonPrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  char paramLevel ;
  ufdcHrdwrInput dcHwrdwr_inp;
  ufdcHrdwrCurrParam CurrParam ;
  ufdcHrdwrCurrParam OldParam ;
} genSubDCHrdwrPrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  char paramLevel ;
  ufdcPhysicalInput dcPhysical_inp;
  ufdcPhysicalCurrParam CurrParam ;
  ufdcPhysicalCurrParam OldParam ;
} genSubDCPhysicalPrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  char paramLevel ; 
  ufdcAcqContInput dcAcqCont_inp;
  ufdcAcqContCurrParam CurrParam ;
  ufdcAcqContCurrParam OldParam ;
  int observing ;
  long obs_time_out ;
  ufCarInfo ObserveCcarinfo ;
} genSubDCAcqContPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  char paramLevel ; 
  ufdcObsContInput dcObsCont_inp;
  ufdcObsContCurrParam CurrParam ;
  ufdcObsContCurrParam OldParam ;
} genSubDCObsContPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  ufecVacChInput ecVacCh_inp;
  ufecVacChCurrParam CurrParam ;
  ufecVacChCurrParam OldParam ;
} genSubECVacChPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  ufDCBiasInput bias_inp;
  ufDCBiasCurrParam CurrParam ;
  ufDCBiasCurrParam OldParam ;
} genSubDCBiasPrivate ;


typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  ufDCPreAmpInput preAmp_inp;
  ufDCPreAmpCurrParam CurrParam ;
  ufDCPreAmpCurrParam OldParam ;
} genSubDCPreAmpPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  ufBTInput boot_inp;
  ufBTCurrParam CurrParam ;
  ufBTCurrParam OldParam ;
} genSubBTPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  long time_out_value ;
  char dhs_error_message[40] ;
  ufDHSInput dhs_inp ;
  ufDHSCurrParam CurrParam ;
  ufDHSCurrParam OldParam;
} genSubDHSPrivate ;

typedef struct {
  int commandState ;
  int socfd ;
  long port_no;
  int agent ;
  ufCarInfo carinfo ;
  long startingTicks; 
  char errorMessage[MAX_STRING_SIZE];
  GENSUB_CALLBACK  *pCallback;
  int response_task_launched ;
  int send_init_param ;

  long time_out_value ;
  char qlk_error_message[40] ;
  ufQlkInput qlk_inp ;
  ufQlkCurrParam CurrParam ;
  ufQlkCurrParam OldParam;
} genSubQlkPrivate ;



#if !defined ( __UFGEMGENSUBCOMM_C__ )

extern long unpack_array (genSubRecord *pgs);
extern long unpack_any_array (genSubRecord *pgs, char *zz, int elem_cnt, char *remainder);
extern GENSUB_CALLBACK *initCallback (void (*callbackFunction)(GENSUB_CALLBACK *));
extern long requestCallback (GENSUB_CALLBACK *, long);
extern long cancelCallback (GENSUB_CALLBACK *);

extern void trecsGensubCallback (GENSUB_CALLBACK *);

#endif



#endif /* __UFGEMGENSUBCOMM_H__  */
