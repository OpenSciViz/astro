#if !defined(__UFDBL_C__)
#define __UFDBL_C__ "$Name:  $ $Id: ufdbl.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsId[] = __UFDBL_C__;

/* uf modification of "dbl" func. in epics 3.12Gem */

#include "vxWorks.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "timexLib.h"
#include "tickLib.h"
#include "memLib.h"
#include "ellLib.h"
#include "fast_lock.h"
#include "dbDefs.h"
#include "dbAccess.h"
#include "dbBase.h"
#include "dbRecType.h"
#include "dbRecords.h"
#include "dbCommon.h"
#include "recSup.h"
#include "devSup.h"
#include "drvSup.h"
#include "choice.h"
#include "special.h"
#include "dbRecDes.h"
#include "dbStaticLib.h"
#include "dbEvent.h"
#include "ellLib.h"
#include "callback.h"

typedef struct {
  DBRstatus
  DBRtime
  float value[10] ;
} MYBUFFER;

extern struct dbBase *pdbBase;

static int _verbose = 1;
static char* _agenthostIP = "000.000.000.000";
static char* _dbname = "trecs or miri";

char* ufdbName() {
  DBENTRY  dbentry;
  DBENTRY  *pdbentry= &dbentry;
  long     status= 0;
  char    *cln = ":", *recnam= 0;
  if( strcmp(_dbname, "trecs or miri") != 0 ) 
    return _dbname;

  /* init it */
  dbInitEntry(pdbBase,pdbentry);
  status = dbFirstRecdes(pdbentry);
  status = dbFirstRecord(pdbentry);
  recnam = dbGetRecordName(pdbentry);
  dbFinishEntry(pdbentry);
  if( recnam == 0 )
    return 0;

  cln = index(recnam, ':');
  if( cln == 0 ) 
    return 0;

  memset(_dbname, 0, strlen(_dbname));
  strncpy(_dbname, recnam, (cln - recnam)); 
  return _dbname;
}


long ufdbl(char *precdesname) {
  DBENTRY  dbentry;
  DBENTRY  *pdbentry= &dbentry;
  long     status;
  long     dbcnt= 0;

  dbInitEntry(pdbBase, pdbentry);
  if(!precdesname)
    status = dbFirstRecdes(pdbentry);
  else
    status = dbFindRecdes(pdbentry, precdesname);
  if(status) printf("No record description\n");
  while(!status) {
    status = dbFirstRecord(pdbentry);
    while(!status) {
      if(precdesname != 0)
        printf("%s\n", dbGetRecordName(pdbentry));
      else
        ++dbcnt;
      status = dbNextRecord(pdbentry);
    }
    if(precdesname)
      break;
    status = dbNextRecdes(pdbentry);
  }
  dbFinishEntry(pdbentry);
  return(dbcnt);
}

long ufHeartbeat () {
  DBENTRY  dbentry;
  DBENTRY  *pdbentry= &dbentry;
  long     status;
  char buffer[MAX_STRING_SIZE];
  struct dbAddr heartbeat_address;
  long heartbeat_val;
  long myoptions = 0;
  long num_req = 1;


  dbInitEntry(pdbBase, pdbentry);
  status = dbFindRecdes(pdbentry,"longout");
  while(!status) {
    status = dbFirstRecord(pdbentry);
    while(!status) {
      if (strstr(dbGetRecordName(pdbentry), "heartbeat") != 0 ) {
        /* printf("%s\n", dbGetRecordName(pdbentry)); */
        strcpy (buffer, dbGetRecordName(pdbentry) ); 
        /* strcat (buffer, ".VAL"); */
        status = dbNameToAddr (buffer, &heartbeat_address);
        if (status) {
          return -1;
        }
        status = dbGetField(&heartbeat_address,DBR_LONG,&heartbeat_val,&myoptions,&num_req,NULL);
        printf("Record %s value is: %ld\n",dbGetRecordName(pdbentry),heartbeat_val) ;
      }
      status = dbNextRecord(pdbentry);
    }
    status = dbNextRecdes(pdbentry);
  }
  dbFinishEntry(pdbentry);
  return OK ;
}

long ufuptime () {
  long tick_val ;
  long days, hours, minutes, seconds;
  char shours[3], sminutes[3],sseconds[3] ;
  tick_val = tickGet()/60 ;
  days = tick_val / (24*60*60) ; 
  tick_val = tick_val - days*24*60*60 ; 
  hours =  tick_val / (60*60) ; 
  tick_val = tick_val - hours*60*60 ; 
  minutes = tick_val / (60) ; 
  tick_val = tick_val - minutes*60 ; 
  seconds = tick_val ; 
  if (seconds < 10) sprintf(sseconds,"0%ld",seconds) ;
  else sprintf(sseconds,"%ld",seconds) ;
  if (minutes < 10) sprintf(sminutes,"0%ld",minutes) ;
  else sprintf(sminutes,"%ld",minutes) ;
  if (hours < 10) sprintf(shours,"0%ld",hours) ;
  else sprintf(shours,"%ld",hours) ;
  printf("\nIOC Uptime is %ld day(s), and %s:%s:%s\n",days,shours,sminutes,sseconds) ;
  return OK;
}
/*
long ufmemShow () {
  int status ;
  PART_ID ufPart_Id ;
  MEM_PART_STATS ufppartStats ;

  memShowInit() ;
  printf("memShow(0)\n") ;
  memShow(0) ;

  ufPart_Id = memPartCreate (RF_MEM_BASE, RF_MEM_SIZE)); 
  status = memPartInfoGet (ufPart_Id, &ufppartStats);
  printf("The status is %d\n",status) ;
  printf("Number of Free bytes is %ld\n",ufppartStats.numBytesFree) ;
  
    typedef struct
    {
    unsigned long numBytesFree,     Number of Free Bytes in Partition      
    numBlocksFree,    Number of Free Blocks in Partition      
    maxBlockSizeFree,  Maximum block size that is free.       
    numBytesAlloc,    Number of Allocated Bytes in Partition 
                numBlocksAlloc;  Number of Allocated Blocks in Partition 

        }  MEM_PART_STATS; 

  return status;

}
*/
long ufAlive () {
  printf("%ld Records loaded.\n",ufdbl(NULL)) ;
  ufHeartbeat() ;
  ufuptime ();
  return OK;
}

/* presumably name == "record.field", .i.e. "trecs:sad:agenthostIP.VAL" */
long ufdbStrGet(char* name, char str[40]) {
  struct dbAddr iAddr;
  long nel = 1, stat= 0;
  dbNameToAddr(name, &iAddr);
  stat = dbGetField(&iAddr, DBR_STRING, str, 0, &nel, 0);
  if( _verbose >  0 )
    fprintf(stderr,"ufdbStrGet> got val: %s from %s\n", str, name);

  return stat;
}

char* ufAgentHostIP() {
  char sirdbnam[40], sirdbval[40];
  char* dbnam = ufdbName();
  memset(sirdbnam, 0, sizeof(sirdbnam));
  strcat(sirdbnam, dbnam);
  strcat(sirdbnam, ":sad:agenthostIP"); 
  memset(sirdbval, 0, sizeof(sirdbval));
  ufdbStrGet(sirdbnam, sirdbval);
  strncpy(_agenthostIP, sirdbval, strlen(sirdbval));
  return _agenthostIP;
}


long ufdbStrTransfer(char* input, char* output) {
  long nel = 1, stat= 0;
  char str[40];
  struct dbAddr iAddr, oAddr;

  dbNameToAddr(input, &iAddr);
  dbNameToAddr(output, &oAddr);

  stat = dbGetField(&iAddr, DBR_STRING, str, 0, &nel, 0);
  if( _verbose >  0 )
    fprintf(stderr,"ufdbStrTransfer> got val: %s from %s\n", str, input);

  /* this will attempt to lock the record, but it is already locked...(presum. by write_so)
  stat = dbPutField(oAddr, DBR_STRING, str, &nel );
  */
  
  /* so just do a dbPut: */
  stat = dbPut(&oAddr, DBR_STRING, str, (int)nel );
  if( _verbose >  0 )
    fprintf(stderr,"ufdbStrTransfer> put val: %s to %s\n", str, output);
  return stat;
}

#endif /* __UFDBL_C__ */
