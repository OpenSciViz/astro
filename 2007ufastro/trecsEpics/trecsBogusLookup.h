
/* INDENT OFF */
/*+
 *
 * FILENAME
 * -------- 
 * trecsBogusLookup.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument dummy lookup
 * table module
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2001/01/14  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


#define NAME_SIZE  16


/*
 *
 *  Public function prototypes
 *
 */

long trecsInitBogusLookup (void);
long trecsReloadBogusLookup (void);
long trecsBogusFilterLookup (char *, char *, char *, char *);
long trecsBogusNameLookup (char *, char *, float*);


