 

/* ===================================================================== */
/* INDENT OFF */
/*+
 *
 * FILENAME
 * -------- 
 * trecsIsGensub.c
 *
 * PURPOSE
 * -------
 * Support code for the T-ReCS Instrument Sequencer gensub records
 * 
 * FUNCTION NAME(S)
 * ----------------
 * trecsCmdCombineProcess
 * trecsIsCopyGProcess
 * trecsIsInitGInit
 * trecsIsInitGProcess
 * trecsIsInsSetupCopy
 * trecsIsInsSetupTranslate
 * trecsIsNullGInit
 * trecsIsRebootGInit
 * trecsIsRebootGProcess
 * trecsIsSetWcsGProcess
 * trecsIsSubSysCombineProcess
 * trecsIsTranslateGProcess
 * trecsStateCombineGProcess
 *
 * trecsIsGensubCallback
 *
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold  (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/12  WNR  Initial coding
 * 2001/12/24  WNR  Changed initialization file handling
 */
/* INDENT ON */
/* ===================================================================== */


#include <stdioLib.h>
#include <stdlib.h>
#include <string.h>
#include <sysLib.h>

#include <logLib.h>
#include <rebootLib.h>

#include <callback.h>
#include <wdLib.h>
#include <recSup.h>
#include <dbFldTypes.h>
#include <car.h>

#include <trecsPositionLookup.h>

#include <genSubRecord.h>
#include <trecsIsGensub.h>

#define  INIT_HOLDOFF    60
#define  REBOOT_HOLDOFF  180

#define  TRX_CMD1    0x0001
#define  TRX_CMD2    0x0002
#define  TRX_CMD3    0x0004
#define  TRX_CMD4    0x0008
#define  TRX_CMD5    0x0010
#define  TRX_CMD6    0x0020
#define  TRX_CMD7    0x0040
#define  TRX_CMD8    0x0080
#define  TRX_CMD9    0x0100
#define  TRX_CMD10   0x0200

 
typedef struct
{
    CALLBACK         callback;
    struct dbCommon  *pRecord;
    WDOG_ID          watchdogId;
    int              callbackFlag;
} GENSUB_CALLBACK;


static void trecsIsGensubCallback (GENSUB_CALLBACK *);
void pvload (char *);



/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsCmdCombineGProcess
 *
 * Purpose:
 *  Combine the CAR record states and error messages from up to 10 
 *  instrument sequencer commands into a single IDLE/BUSY/ERR state.
 * 
 * Invocation:
 *  status = trecsCmdCombineGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     long    command 1 CAR state
 *      > pgs->b     string  command 1 error message
 *      > pgs->c     long    command 2 CAR state
 *      > pgs->d     string  command 2 error message
 *      > pgs->e     long    command 3 CAR state
 *      > pgs->f     string  command 3 error message
 *      > pgs->g     long    command 4 CAR state
 *      > pgs->h     string  command 4 error message
 *      > pgs->i     long    command 5 CAR state
 *      > pgs->k     string  command 5 error message
 *      > pgs->l     long    command 6 CAR state
 *      > pgs->m     string  command 6 error message
 *      > pgs->n     long    command 7 CAR state
 *      > pgs->o     string  command 7 error message
 *      > pgs->p     long    command 8 CAR state
 *      > pgs->q     string  command 8 error message
 *      > pgs->r     long    command 9 CAR state
 *      > pgs->s     string  command 9 error message
 *      > pgs->t     long    command 10 CAR state
 *      > pgs->u     string  command 10 error message
 * 
 * Parameters out:
 *      < pgs->vala  string  error message
 *      < pgs->valb  long    combined CAR state
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsCmdCombineGProcess
(
    genSubRecord *pgs           /* gensub record structure             */
)
{
    long *active = (long *) pgs->dpvt;        

    /*
     *  Remember which commands have gone busy
     */

    if (*(long *) pgs->a == CAR_BUSY) *active |= TRX_CMD1; 
    if (*(long *) pgs->c == CAR_BUSY) *active |= TRX_CMD2;
    if (*(long *) pgs->e == CAR_BUSY) *active |= TRX_CMD3;
    if (*(long *) pgs->g == CAR_BUSY) *active |= TRX_CMD4;
    if (*(long *) pgs->i == CAR_BUSY) *active |= TRX_CMD5;
    if (*(long *) pgs->k == CAR_BUSY) *active |= TRX_CMD6;
    if (*(long *) pgs->m == CAR_BUSY) *active |= TRX_CMD7;
    if (*(long *) pgs->o == CAR_BUSY) *active |= TRX_CMD8;
    if (*(long *) pgs->q == CAR_BUSY) *active |= TRX_CMD9;
    if (*(long *) pgs->s == CAR_BUSY) *active |= TRX_CMD10;


    /*
     *  If any input is BUSY then the resulting value is BUSY
     */

    if (*(long *) pgs->a == CAR_BUSY || 
        *(long *) pgs->c == CAR_BUSY ||
        *(long *) pgs->e == CAR_BUSY ||
        *(long *) pgs->g == CAR_BUSY ||
        *(long *) pgs->i == CAR_BUSY ||
        *(long *) pgs->k == CAR_BUSY ||
        *(long *) pgs->m == CAR_BUSY ||
        *(long *) pgs->o == CAR_BUSY ||
        *(long *) pgs->q == CAR_BUSY ||
        *(long *) pgs->s == CAR_BUSY)
    {
        *(long *) pgs->vala = CAR_BUSY;
        *(char *)(pgs->valb) = '\0';
    }


    /*
     *  Otherwise, check to see if any of the busy commands are now
     *  in an error state.  The first error will set the resulting 
     *  value to ERR and copy the associated message to the message output.
     */ 

    else if ((*active & TRX_CMD1) && (*(long *) pgs->a == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->b);
    }

    else if ((*active & TRX_CMD2) && (*(long *) pgs->c == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->d);
    }

    else if ((*active & TRX_CMD3) && (*(long *) pgs->e == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->f);
    }

    else if ((*active & TRX_CMD4) && (*(long *) pgs->g == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->h);
    }

    else if ((*active & TRX_CMD5) && (*(long *) pgs->i == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->j);
    }

    else if ((*active & TRX_CMD6) && (*(long *) pgs->k == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->l);
    }

    else if ((*active & TRX_CMD7) && (*(long *) pgs->m == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->n);
    }

    else if ((*active & TRX_CMD8) && (*(long *) pgs->o == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->p);
    }

    else if ((*active & TRX_CMD9) && (*(long *) pgs->q == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->r);
    }

    else if ((*active & TRX_CMD10) && (*(long *) pgs->s == CAR_ERROR))
    {
        *active = 0;
        *(long *) pgs->vala = CAR_ERROR;
        strcpy (pgs->valb, pgs->t);
    }


    /*
     *  None of the active commands had an error .... must be IDLE
     */

    else if (*(long *) pgs->vala != CAR_ERROR)
    {
        *active = 0;
        *(long *) pgs->vala = CAR_IDLE;
    }


    /*
     *  If the CAR state has not changed then set VALC to inhibit
     *  the attached CAR record updating records.  This prevents the
     *  CAR record from being processed unless there is a change.
     */

    if (*(long *) pgs->vala == *(long *) pgs->ovla)
    {
        *(long *) pgs->valc = 1;
    }
    else
    {
        *(long *) pgs->valc = 0;
    }

    return OK;

}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsCopyGProcess
 *
 * Purpose:
 *  Copy up to 21 strings from the input to the output fields
 * 
 * Invocation:
 *  status = trecsIsCopyGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     string  Input string 1 
 *      > pgs->b     string  Input string 2 
 *      > pgs->c     string  Input string 3 
 *      > pgs->d     string  Input string 4 
 *      > pgs->e     string  Input string 5 
 *      > pgs->f     string  Input string 6 
 *      > pgs->g     string  Input string 7 
 *      > pgs->h     string  Input string 8 
 *      > pgs->i     string  Input string 9 
 *      > pgs->j     string  Input string 10 
 * 
 * Parameters out:
 *      < pgs->vala  string  Output string 1
 *      < pgs->valb  string  Output string 2
 *      < pgs->valc  string  Output string 3
 *      < pgs->vald  string  Output string 4
 *      < pgs->vale  string  Output string 5
 *      < pgs->valf  string  Output string 6
 *      < pgs->valg  string  Output string 7
 *      < pgs->valh  string  Output string 8
 *      < pgs->vali  string  Output string 9
 *      < pgs->valj  string  Output string 10
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsCopyGProcess
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    long status = OK;            /* function return status           */

    if (pgs->fta == DBF_STRING && pgs->ftva == DBF_STRING) strcpy (pgs->vala, pgs->a);
    if (pgs->ftb == DBF_STRING && pgs->ftvb == DBF_STRING) strcpy (pgs->valb, pgs->b);
    if (pgs->ftc == DBF_STRING && pgs->ftvc == DBF_STRING) strcpy (pgs->valc, pgs->c);
    if (pgs->ftd == DBF_STRING && pgs->ftvd == DBF_STRING) strcpy (pgs->vald, pgs->d);
    if (pgs->fte == DBF_STRING && pgs->ftve == DBF_STRING) strcpy (pgs->vale, pgs->e);
    if (pgs->ftf == DBF_STRING && pgs->ftvf == DBF_STRING) strcpy (pgs->valf, pgs->f);
    if (pgs->ftg == DBF_STRING && pgs->ftvg == DBF_STRING) strcpy (pgs->valg, pgs->g);
    if (pgs->fth == DBF_STRING && pgs->ftvh == DBF_STRING) strcpy (pgs->valh, pgs->h);
    if (pgs->fti == DBF_STRING && pgs->ftvi == DBF_STRING) strcpy (pgs->vali, pgs->i);
    if (pgs->ftj == DBF_STRING && pgs->ftvj == DBF_STRING) strcpy (pgs->valj, pgs->j);
    if (pgs->ftk == DBF_STRING && pgs->ftvk == DBF_STRING) strcpy (pgs->valk, pgs->k);
    if (pgs->ftl == DBF_STRING && pgs->ftvl == DBF_STRING) strcpy (pgs->vall, pgs->l);
    if (pgs->ftm == DBF_STRING && pgs->ftvm == DBF_STRING) strcpy (pgs->valm, pgs->m);
    if (pgs->ftn == DBF_STRING && pgs->ftvn == DBF_STRING) strcpy (pgs->valn, pgs->n);
    if (pgs->fto == DBF_STRING && pgs->ftvo == DBF_STRING) strcpy (pgs->valo, pgs->o);
    if (pgs->ftp == DBF_STRING && pgs->ftvp == DBF_STRING) strcpy (pgs->valp, pgs->p);
    if (pgs->ftq == DBF_STRING && pgs->ftvq == DBF_STRING) strcpy (pgs->valq, pgs->q);
    if (pgs->ftr == DBF_STRING && pgs->ftvr == DBF_STRING) strcpy (pgs->valr, pgs->r);
    if (pgs->fts == DBF_STRING && pgs->ftvs == DBF_STRING) strcpy (pgs->vals, pgs->s);
    if (pgs->ftt == DBF_STRING && pgs->ftvt == DBF_STRING) strcpy (pgs->valt, pgs->t);
    if (pgs->ftu == DBF_STRING && pgs->ftvu == DBF_STRING) strcpy (pgs->valu, pgs->u);
  
    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsInitGInit
 *
 * Purpose:
 *  Create the record callback that will allow intialization
 *  to take place without holding up record processing.
 * 
 * Invocation:
 *  status = trecsIsInitGInit (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsInitGInit
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    GENSUB_CALLBACK *pCallback; /* record callback structure        */
    long status = OK;           /* function return status           */

    /*
     *  Initialize the name lookup system
     */

    status = trecsInitPositionLookup();
    if (status)
    {
        logMsg ("can't initialize lookup tables\n",0,0,0,0,0,0);
        return status;
    }

    /*
     *  Create a timed callback structure.
     */

    pCallback = (GENSUB_CALLBACK *) malloc(sizeof(GENSUB_CALLBACK));
    if ( pCallback == NULL )
    {
        logMsg ("can't create callback structure\n",0,0,0,0,0,0);
        return ERROR;
    }


    /*
     *  Register the command timeout callback with the EPICS callback system
     */

    callbackSetCallback( trecsIsGensubCallback, &pCallback->callback );


    /*
     *  Create a watchdog timer to invoke it via the EPICS watchdog task
     */

    pCallback->watchdogId = wdCreate();
    if (pCallback->watchdogId == NULL )
    {
        logMsg ("can't create watchdog timer\n",0,0,0,0,0,0);
        return ERROR;
    }


    /*
     *  Set this as a very low priority callback
     */

    callbackSetPriority( priorityLow, &pCallback->callback );

   
    /*
     *  Save the record structure in the callback structure then save
     *  the whole mess in the record's device private field.
     */

    pCallback->callbackFlag = FALSE;
    pCallback->pRecord = (struct dbCommon *) pgs;

    pgs->dpvt = (void *) pCallback;


    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsInitGProcess
 *
 * Purpose:
 *  Re-load all configuration files by triggering an initialization
 *  callback.   The CAR state will go BUSY when the gensub is proccessed
 *  to start the initialization sequence and will stay busy until the
 *  record is processed again by the initialization callback to 
 *  complete the initialization process.
 * 
 * Invocation:
 *  status = trecsIsInitGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     string  simulation state
 *      > pgs->b     string  initialization pvLoad file name
 *      > pgs->c     string  initialization filter lookup file name
 *      > pgs->d     string  initialization device lookup file name
 *
 * Parameters out:
 *      < pgs->vala  string  error message
 *      < pgs->valb  long    CAR state
 * 
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsInitGProcess
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    GENSUB_CALLBACK *pCallback;  /* callback structure              */
    long status = OK;            /* function return status          */

    pCallback = (GENSUB_CALLBACK *) pgs->dpvt;



    /*
     *  The first call to this function will invoke a callback
     *  to allow the sequncer to finish before initializing the 
     *  system.
     */

    if (pCallback->callbackFlag == FALSE)
    {

        /*
         *  Set the CAR record to BUSY
         */

        strcpy (pgs->vala, "");
        *(long *)pgs->valb = CAR_BUSY;


        /*
         *  Start the EPICS watchdog timer identified in the timeout struct.
         *  This will invoke the initCallback function in one second to 
         *  continue the initialization process.
         */

        status = wdStart(pCallback->watchdogId, 
                         INIT_HOLDOFF,
                         (FUNCPTR)callbackRequest,
                         (int)pCallback);

        if (status == ERROR )
        {
            logMsg ("can't start watchdog timer\n",0,0,0,0,0,0); 
        }
    
        return status;
    }


   /* 
     *  Initialization will now take place in the context of the callback
     *  task....
     */


    pCallback->callbackFlag = FALSE;
 

    /*
     *  Re-read the lookup tables and then use pvload to re-initiailize
     *  all the rest.....
     */

     status = trecsReloadFilterTable (pgs->c);
     if (status)
     {
        strcpy (pgs->vala, "Failed to reload filter lookup table");
     }

     status = trecsReloadDeviceTable (pgs->d);
     if (status)
     {
        strcpy (pgs->vala, "Failed to reload device lookup table");
     }

/*   pvload (pgs->b);   */


    /*
     *  Then set the CAR record back to IDLE or ERROR depending
     *  on how the initialization went.
     */

    *(long *)pgs->valb = (strlen (pgs->vala)) ? CAR_ERROR : CAR_IDLE;

    return OK;
}

  
/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsNullGInit
 *
 * Purpose:
 *  Dummy initialization function that simply creates a private
 *  long word.
 * 
 * Invocation:
 *  status = trecsIsNullGInit (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsNullGInit
(
    genSubRecord *pgs            /* cad record structure             */
)
{
    long *pLong = NULL;
    long status = OK;            /* function return status           */

    pLong = (long *) malloc (sizeof(long));
    if (pLong == NULL)
    {
        logMsg ("can't create long pointer for gensub\n",0,0,0,0,0,0);
        status = ERROR;
    }

    *pLong = 0;

    pgs->dpvt = (void *) pLong;

    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsRebootGInit
 *
 * Purpose:
 *  Create the reboot task that will do the IOC reboot stuff after a 
 *  suitable delay for the other systems to get the reboot command.
 * 
 * Invocation:
 *  status = trecsIsRebootGInit (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsRebootGInit
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    GENSUB_CALLBACK *pCallback; /* record callback structure        */
    long status = OK;           /* function return status           */


    /*
     *  Create a timed callback structure.
     */

    pCallback = (GENSUB_CALLBACK *) malloc(sizeof(GENSUB_CALLBACK));
    if ( pCallback == NULL )
    {
        logMsg ("can't create callback structure\n",0,0,0,0,0,0);
        return ERROR;
    }


    /*
     *  Register the command timeout callback with the EPICS callback system
     */

    callbackSetCallback( trecsIsGensubCallback, &pCallback->callback );


    /*
     *  Create a watchdog timer to invoke it via the EPICS watchdog task
     */

    pCallback->watchdogId = wdCreate();
    if (pCallback->watchdogId == NULL )
    {
        logMsg ("can't create watchdog timer\n",0,0,0,0,0,0);
        return ERROR;
    }


    /*
     *  Set this as a very low priority callback
     */

    callbackSetPriority( priorityLow, &pCallback->callback );

   
    /*
     *  Save the record structure in the callback structure then save
     *  the whole mess in the record's device private field.
     */

    pCallback->callbackFlag = FALSE;
    pCallback->pRecord = (struct dbCommon *)pgs;

    pgs->dpvt = (void *) pCallback;


    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsRebootGProcess
 *
 * Purpose:
 *  Start the rebooting process by triggering a reboot callback. The CAR 
 *  state will go BUSY when the gensub is proccessed to start the
 *  rebooting sequence and will stay busy until the callback re-processes
 *  the record to actually push the reset button.......
 * 
 * Invocation:
 *  status = trecsIsRebootGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 *      < pgs->vala  string  error message
 *      < pgs->valb  long    combined CAR state
 * 
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsRebootGProcess
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    GENSUB_CALLBACK *pCallback;  /* callback structure              */
    long status = OK;            /* function return status          */

    pCallback = (GENSUB_CALLBACK *) pgs->dpvt;


    /*
     *  The first call to this function will invoke a callback
     *  to allow the sequncer to finish before rebooting the 
     *  system.
     */

    if (pCallback->callbackFlag == FALSE)
    {

        /*
         *  Set the CAR record to BUSY
         */

        strcpy (pgs->vala, "");
        *(long *)pgs->valb = CAR_BUSY;


        /*
         *  Start the EPICS watchdog timer identified in the timeout struct.
         *  This will invoke the initCallback function in one second to 
         *  continue the initialization process.
         */

        status = wdStart(pCallback->watchdogId, 
                         REBOOT_HOLDOFF,
                         (FUNCPTR)callbackRequest,
                         (int)pCallback);

        if (status == ERROR )
        {
            logMsg ("can't start watchdog timer\n",0,0,0,0,0,0); 
        }
    
        return status;
    }


    /* 
     *  Rebooting will now take place in the context of the callback
     *  task....
     */

    /*
     *  Reboot the sub-systems
     */


    /* ------------------------- HOW ??? ------------------------ */ 


    /*
     *  Then reboot the VxWorks crate
     */


    reboot (BOOT_CLEAR);

    return status;

}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsSetWcsGProcess
 *
 * Purpose:
 *  Re-calculate the World Coordinate System information based on
 *  the current value of the Astrometry Context array read from the
 *  TCS.
 * 
 * Invocation:
 *  status = trecsIsSetWcsGProcess (genSubRecord *pgs);
 *      > pgs->a     double[39]  astrometry context array
 *      > pgs->b     double      central wavelength
 *
 * Parameters in:
 *
 * Parameters out:
 *      < pgs->vala  double  time at which this transform is valid
 *      < pgs->valb  double  celestial coord at x=0
 *      < pgs->valc  double  celestial coord at y=0
 *      < pgs->vald  double  affine transformation coefficient a
 *      < pgs->vale  double  affine transformation coefficient b
 *      < pgs->valf  double  affine transformation coefficient c
 *      < pgs->valg  double  affine transformation coefficient d
 *      < pgs->valh  double  affine transformation coefficient e
 *      < pgs->vali  double  affine transformation coefficient f
 *
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsIsSetWcsGProcess
(
    genSubRecord *pgs           /* cad record structure             */
)
{

/*
 *  struct WCS_CTX   trecsAstCtx;
 *  struct WCS       trecsWcs;
 *  FRAMETYPE        frame;
 *  EPOCH            equinox;
 *  double           time;
 */
    long status = OK;            /* function return status          */

    /*
     *  Convert astrometry context into world coordinate system
     *  at the time this function was called.
     */

/*
 *  trecsAstCtx = *(struct WCS_CTX *) pgs->a;
 *  frame = FK5;
 *  equinox.type = 'J';
 *  equinox.year = 2000.0;
 *
 *  status = astCtx2tr (trecsAstCtx,
 *                      frame,
 *                      equinox,
 *                      *(double *) pgs->b,
 *                      &trecsWcs,
 *                      &time);
 *
 *  if (status)
 *  {
 *      Bad Stuff....
 *
 *  }
 *
 *  *(double *) pgs->vala = time;
 *  *(double *) pgs->valb = trecsWcs.ab0[0];
 *  *(double *) pgs->valc = trecsWcs.ab0[1];
 *  *(double *) pgs->vald = trecsWcs.coeffs[0];
 *  *(double *) pgs->vale = trecsWcs.coeffs[1];
 *  *(double *) pgs->valf = trecsWcs.coeffs[2];
 *  *(double *) pgs->valg = trecsWcs.coeffs[3];
 *  *(double *) pgs->valh = trecsWcs.coeffs[4];
 *  *(double *) pgs->vali = trecsWcs.coeffs[5];
 */  


    /*
     *  generate bogus data until we are at the telescope 
     */

    *(double *) pgs->vala = *(double *) pgs->vali + 1.0;
    *(double *) pgs->valb = *(double *) pgs->vala + 1.0;
    *(double *) pgs->valc = *(double *) pgs->valb + 1.0;
    *(double *) pgs->vald = *(double *) pgs->valc + 1.0;
    *(double *) pgs->vale = *(double *) pgs->vald + 1.0;
    *(double *) pgs->valf = *(double *) pgs->vale + 1.0;
    *(double *) pgs->valg = *(double *) pgs->valf + 1.0;
    *(double *) pgs->valh = *(double *) pgs->valg + 1.0;
    *(double *) pgs->vali = *(double *) pgs->valh + 1.0;

    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsSubSysCombineProcess
 *
 * Purpose:
 *  Combine the CAR record states and error messages from the instrument
 *  sequencer, component controller and detector controller into a single
 *  IDLE/BUSY/ERR state.
 * 
 * Invocation:
 *  status = trecsIsSubSysCombineProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     string  IS CAR state
 *      > pgs->b     string  IS error message
 *      > pgs->c     string  CC CAR state
 *      > pgs->d     string  CC error message
 *      > pgs->e     string  DC CAR state
 *      > pgs->f     string  DC error message
 *      > pgs->g     string  EC CAR state 
 *      > pgs->h     string  EC error message
 * 
 * Parameters out:
 *      < pgs->vala  string  error message
 *      < pgs->valb  long    combined CAR state
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsSubSysCombineProcess
(
    genSubRecord *pgs           /* gensub record structure             */
)
{

    if (*(long *) pgs->a == CAR_BUSY || 
        *(long *) pgs->c == CAR_BUSY ||
        *(long *) pgs->e == CAR_BUSY ||
        *(long *) pgs->g == CAR_BUSY)
    {
        *(long *) pgs->valb = CAR_BUSY;
        *(char *)(pgs->vala) = '\0';

    }

    else if (*(long *) pgs->a == CAR_ERROR)
    {
        *(long *) pgs->valb = CAR_ERROR;
        strcpy (pgs->vala, pgs->b);
    }

    else if (*(long *) pgs->c == CAR_ERROR)
    {
        *(long *) pgs->valb = CAR_ERROR;
        strcpy (pgs->vala, pgs->d);
    }

    else if (*(long *) pgs->e == CAR_ERROR)
    {
        *(long *) pgs->valb = CAR_ERROR;
        strcpy (pgs->vala, pgs->f);
    }

    else if (*(long *) pgs->g == CAR_ERROR)
    {
        *(long *) pgs->valb = CAR_ERROR;
        strcpy (pgs->vala, pgs->h);
    }

    else
    {
        *(long *) pgs->valb = CAR_IDLE;
    }


    return OK;

}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsTranslateGProcess
 *
 * Purpose:
 *  Translate optical device names to wheel positions and write the
 *  results to the detector controller and component controller.
 * 
 * Invocation:
 *  status = trecsIsTranslateGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     string  aperture name
 *      > pgs->b     string  cold clamp name
 *      > pgs->c     string  filter 1 name
 *      > pgs->d     string  filter 2 name
 *      > pgs->e     string  grating name
 *      > pgs->f     string  lyot stop name
 *      > pgs->g     string  pupil name
 *      > pgs->h     string  sector name
 *      > pgs->i     string  slit name
 *      > pgs->j     string  window name
 * 
 * Parameters out:
 *      < pgs->vala  string  aperture wheel position
 *      < pgs->valb  string  cold clamp position
 *      < pgs->valc  string  filter wheel 1 position
 *      < pgs->vald  string  filter wheel 2 position
 *      < pgs->vale  string  grating wheel position
 *      < pgs->valf  string  lyot wheel position
 *      < pgs->valg  string  pupil wheel position
 *      < pgs->valh  string  sector wheel position
 *      < pgs->vali  string  slit wheel position
 *      < pgs->valj  string  window changer position
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */

long trecsIsTranslateGProcess
(
    genSubRecord *pgs           /* cad record structure             */
)
{
    float efficiency;           /* efficienct return (ignored)      */
    float lambdaMin;            /* spectral resolution return (ignored) */
    float lambdaMax;            /* spectral resolution return (ignored) */
    long status = OK;           /* function return status           */

    /*
     *  Translate the names into positions using the lookup 
     *  table for the component controller.
     */
    
    trecsPositionLookup("aprtrWhl", pgs->a, pgs->vala, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("coldClmp", pgs->b, pgs->valb, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("fltrWhl1", pgs->c, pgs->valc, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("fltrWhl2", pgs->d, pgs->vald, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("grating", pgs->e, pgs->vale, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("lyotWhl", pgs->f, pgs->valf, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("pplImg", pgs->g, pgs->valg, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("sectWhl", pgs->h, pgs->valh, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("slitWhl", pgs->i, pgs->vali, &efficiency, &lambdaMin, &lambdaMax);
    trecsPositionLookup("winChngr", pgs->j, pgs->valj, &efficiency, &lambdaMin, &lambdaMax);

    return status;
}


/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsStateCombineGInit
 *
 * Purpose:
 *  Initialize the system state outputs on startup.
 * 
 * Invocation:
 *  status = trecsStateCombineGInit (genSubRecord *pgs);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK or system error
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2001/09/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


long trecsStateCombineGInit
(
    genSubRecord *pgs           /* cad record structure             */
)
{
   strcpy (pgs->vala, "WARNING");
   strcpy (pgs->valc, "BOOTING");
   strcpy (pgs->vale, "FALSE");
   strcpy (pgs->valg, "FALSE");
   strcpy (pgs->vali, "FALSE");

   return OK;
}

/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsStateCombineGProcess
 *
 * Purpose:
 *  Combine the CAR record states into an overall system state
 *  and health.  Also publishes the global initialized, datummed 
 *  and parked flags.
 * 
 * Invocation:
 *  status = trecsStateCombineGProcess (genSubRecord *pgs);
 *
 * Parameters in:
 *      > pgs->a     long    initC state
 *      > pgs->b     long    testC state
 *      > pgs->c     long    datumC state
 *      > pgs->d     long    parkC state
 *      > pgs->e     long    debugC state
 *      > pgs->f     long    rebootC state
 *      > pgs->g     long    instrumentSetupC state
 *      > pgs->h     long    dataModeC state
 *      > pgs->i     long    setWcsC state
 *      > pgs->j     long    setDhsInfoC state
 *      > pgs->k     long    observationSetupC state
 *      > pgs->l     long    observeCmdC state
 *      > pgs->m     long    stopC state
 *      > pgs->n     long    abortC state
 *      > pgs->o     long    observeC state
 *      > pgs->p     long    cc:applyC state
 *      > pgs->q     long    dc:applyC state
 *      > pgs->r     long    ec:applyC state
 *      > pgs->s     long    heartbeat state
 * 
 * Parameters out:
 *      < pgs->vala  string  health state 
 *      < pgs->valb  long    health output enable
 *      < pgs->valc  string  state state 
 *      < pgs->vald  long    state output enable
 *      < pgs->vale  string  initialized state
 *      < pgs->valf  long    initialized output enable
 *      < pgs->valg  string  datummed state
 *      < pgs->valh  long    datummed output enable
 *      < pgs->vali  string  parked state
 *      < pgs->valj  long    parked output enable
 *
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 * 2001/09/12  WNR  Added initialized, datummed and parked outputs
 */
/* INDENT ON */
/* ===================================================================== */


long trecsStateCombineGProcess
(
    genSubRecord *pgs           /* gensub record structure             */
)
{
    char state[MAX_STRING_SIZE];
    char health[MAX_STRING_SIZE];
    char initialized[MAX_STRING_SIZE];
    char datummed[MAX_STRING_SIZE];
    char parked[MAX_STRING_SIZE];


    /*
     *  Assume the system is idle and in good health until we find 
     *  out otherwise.   Make sure that the idle state is UNINITIALIZED
     *  until the first successful init command has been completed
     */

    if (strcmp (pgs->vale, "FALSE") == 0 &&
        strcmp (pgs->valc, "INITIALIZING") != 0)
    {
        strcpy (state, "UNINITIALIZED");
        strcpy (health, "WARNING");
    }
    else
    {
        strcpy (health, "GOOD");
        strcpy (state, "IDLE");
    }

    strcpy (initialized, pgs->vale);
    strcpy (datummed, pgs->valg);
    strcpy (parked, pgs->vali);


    /*
     *  If any command or sub-systems is in an error state then the
     *  instrument state is error and the health is bad.
     */

    if (*(long *) pgs->a == CAR_ERROR || 
        *(long *) pgs->b == CAR_ERROR ||
        *(long *) pgs->c == CAR_ERROR ||
        *(long *) pgs->d == CAR_ERROR ||
        *(long *) pgs->e == CAR_ERROR ||
        *(long *) pgs->f == CAR_ERROR ||
        *(long *) pgs->g == CAR_ERROR ||
        *(long *) pgs->h == CAR_ERROR ||
        *(long *) pgs->i == CAR_ERROR ||
        *(long *) pgs->j == CAR_ERROR ||
        *(long *) pgs->k == CAR_ERROR ||
        *(long *) pgs->l == CAR_ERROR ||
        *(long *) pgs->m == CAR_ERROR ||
        *(long *) pgs->n == CAR_ERROR ||
        *(long *) pgs->o == CAR_ERROR)  
    {
        strcpy (state, "ERROR");
        strcpy (health, "BAD");
    }


    /*
     *  Otherwise see if anyone is busy and determine the state
     *  based on who is doing what.
     */ 

    else if (*(long *) pgs->o == CAR_BUSY)
    {
        strcpy (state, "EXPOSING");
    }

    else if (*(long *) pgs->a == CAR_BUSY)
    {
        strcpy(state, "INITIALIZING");
    }

    else if (*(long *) pgs->b == CAR_BUSY)
    {
        strcpy (state, "TESTING");
    }

    else if (*(long *) pgs->c == CAR_BUSY)
    {
        strcpy (state, "DATUMMING");
    }

    else if (*(long *) pgs->d == CAR_BUSY)
    {
        strcpy (state, "PARKING");
    }

    else if (*(long *) pgs->f == CAR_BUSY)
    {
        strcpy (state, "BOOTING");
    }

    else if (*(long *) pgs->e == CAR_BUSY ||
             *(long *) pgs->h == CAR_BUSY ||
             *(long *) pgs->i == CAR_BUSY ||
             *(long *) pgs->j == CAR_BUSY ||
             *(long *) pgs->k == CAR_BUSY)
    {
        strcpy (state, "CONFIGURING");
    }

    else if (*(long *) pgs->g == CAR_BUSY)
    {
        strcpy (state, "MOVING");
        strcpy (parked, "FALSE");
    }

    else if (*(long *) pgs->l == CAR_BUSY)
    {
        strcpy (state, "STARTING");
    }

    else if (*(long *) pgs->m == CAR_BUSY)
    {
        strcpy (state, "STOPPING");
    }

    else if (*(long *) pgs->n == CAR_BUSY)
    {
        strcpy (state, "ABORTING");
    }

    else if (*(long *) pgs->p == CAR_BUSY ||
             *(long *) pgs->q == CAR_BUSY ||
             *(long *) pgs->r == CAR_BUSY)
    {
        strcpy (state, "ADJUSTING");
        strcpy (health, "WARNING");
    }


    /*
     * If the heartbeat value is zero then one of the
     * sub-systems heartbeats have stopped meaning that
     * it has been disconnected....
     */

    if (*(long *) pgs->s == 0)
    {
        strcpy (state, "DISCONNECTED");
        strcpy (health, "BAD");
    }

    /*
     *  If an init command has just finished then update the state of the 
     *  initialized flag based on the result.
     */

    if (strcmp (pgs->valc, "INITIALIZING") == 0)
    {
        if (strcmp (state, "IDLE") == 0) strcpy (initialized, "TRUE");
        else if (strcmp (state, "ERROR") == 0) strcpy (initialized, "FALSE");
    }


    /*
     *  If a datum command has just finished then update the state of the 
     *  datummed flag based on the result.
     */

    if (strcmp (pgs->valc, "DATUMMING") == 0)
    {
        if (strcmp (state, "IDLE") == 0) strcpy (datummed, "TRUE");
        else if (strcmp (state, "ERROR") == 0) strcpy (datummed, "FALSE");
    }


    /*
     *  If a park command has just finished then update the state of the 
     *  parked flag based on the result.
     */

    if (strcmp (pgs->valc, "PARKING") == 0)
    {
        if (strcmp (state, "IDLE") == 0) strcpy (parked, "TRUE");
        else if (strcmp (state, "ERROR") == 0) strcpy (parked, "FALSE");
    }


    /*
     *  Now see if we have to update the state or health
     *  based on the above stuffs....  Leaving a 1 on the output
     *  enable field allows monitors to be blocked (by external 
     *  output records) if the field has not changed.
     */

    *(long *) pgs->valb = 1;
    *(long *) pgs->vald = 1;
    *(long *) pgs->valf = 1;
    *(long *) pgs->valh = 1;
    *(long *) pgs->valj = 1;

    if (strcmp (pgs->vala, health) != 0)
    {
        strcpy (pgs->vala, health);
        *(long *) pgs->valb = 0;
    }

    if (strcmp (pgs->valc, state) != 0)
    {
        strcpy(pgs->valc, state);
        *(long *)pgs->vald = 0;
    } 

    if (strcmp (pgs->vale, initialized) != 0)
    {
        strcpy(pgs->vale, initialized);
        *(long *)pgs->valf = 0;
    } 

    if (strcmp (pgs->valg, datummed) != 0)
    {
        strcpy(pgs->valg, datummed);
        *(long *)pgs->valh = 0;
    } 

    if (strcmp (pgs->vali, parked) != 0)
    {
        strcpy(pgs->vali, parked);
        *(long *)pgs->valj = 0;
    } 

    return OK;
}

  
/* ===================================================================== */
/* INDENT OFF */
/*
 * Function name:
 *  trecsIsGensubCallback
 *
 * Purpose:
 *  Dummy initialization function that does nothing for the moment.
 * 
 * Invocation:
 *  trecsIsGensubCallback (GENSUB_CALLBACK *pCallback);
 *
 * Parameters in:
 *
 * Parameters out:
 * 
 * Return value:
 *      < status    long    OK
 * 
 * Globals: 
 *  External functions:
 *  None
 * 
 *  External variables:
 *  None
 * 
 * Requirements:
 * 
 * Author:
 *  William Rambold (wrambold@gemini.edu)
 * 
 * History:
 * 2000/12/12  WNR  Initial coding
 *
 */
/* INDENT ON */
/* ===================================================================== */


static void trecsIsGensubCallback
(
    GENSUB_CALLBACK *pCallback
)
{
    genSubRecord *pRecord;
    struct rset *pRset;
    
    pRecord = (genSubRecord *) pCallback->pRecord;
    pRset = pRecord->rset;

    pCallback->callbackFlag = TRUE;

    dbScanLock (pCallback->pRecord);
    (*pRset->process)(pRecord);
    dbScanUnlock (pCallback->pRecord);

    return;
}
