

/*
 *
 *  Header file for ecAgentSim.c module.
 *
 */

#define MAX_STRING_SIZE 40

/*
 *
 *  Ec agent command structure
 *
 */

typedef struct { 
    char    commandName [MAX_STRING_SIZE];
    char    carName [MAX_STRING_SIZE];
    char    attributeA [MAX_STRING_SIZE];
    char    attributeB [MAX_STRING_SIZE];
    long    executionTime;
    } ecAgentCommand;

/*
 *
 *  Public function prototypes
 *
 */

long initEcAgentSim (MSG_Q_ID ecCommandQueue);

