#if !defined(__UFGEMCOMM_H__)
#define __UFGEMCOMM_H__ "RCS: $Name:  $ $Id: ufGemComm.h,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufUFGEMCOMMH[] = __UFGEMCOMM_H__ ;

/* #define cc_host_ip          "128.227.184.138" */
/* #define ec_port_no       52003 */
/* #define cc_port_no       52005 */

/* T Y P E   D E F I N I T I O N S */

typedef struct {
    long command_mode ;
    long test_directive ;
    double init_velocity ;
    double slew_velocity ;
    double acceleration ;
    double deceleration ;
    double drive_current ;
    double datum_speed ;
    double setting_time ;
    long jog_speed_slow ;
    long jog_speed_hi ;
    long datum_direction ;
    long datum_motor_directive ;
    double num_steps ;
    double backlash ;
    char stop_mess[40] ;
    char abort_mess[40];
    long origin_directive ;
    char home_switch[40] ;
} ufMotorInput ;

typedef struct {
    long command_mode ;
    double init_velocity ;
    double slew_velocity ;
    double acceleration ;
    double deceleration ;
    double drive_current ;
    long datum_speed ;
    double setting_time ;
    long jog_speed_slow ;
    long jog_speed_hi ;
    long datum_direction ;
    char home_switch[40] ;
    double steps_from_home ;
    double steps_to_add ;
    long homing ;
    double backlash ;
    long final_datum_speed ;
} ufMotorCurrParam ;

typedef struct {
  char name[30] ;
  double offset ;
  double throughput ;
  double lambdaLo ;
  double lambdaHi ;
} ufPosition ;

typedef struct {
  char motorName[30] ;
  int num_pos ;
  ufPosition  the_list[20] ;
} ufMotorPositions ;

typedef struct {
  double set_point ;
  double P ;
  double I ;
  double D ;
  long heater_range ;
  long auto_tune ;
  long command_mode ;
} ufTempInput ;

typedef struct {
  long command_mode ;
  double set_point ;
  double P ;
  double I ;
  double D ;
  long heater_range ;
  long auto_tune ;
} ufTempCurrParam ;

typedef struct {
  long command_mode ;
  long FrmCoadds ;
  long ChpSettleReads ;
  long ChpCoadds ;
  long Savesets ;
  long NodSettleReads ;
  long NodSets ;
  long NodSettleChops ;
  long PreValidChops ;
  long PostValidChops ;
  long pixclock ;
  char obs_mode[40] ;
  char readout_mode[40] ;
  long hFlag ;
} ufdcHrdwrInput ;

typedef struct {
  long command_mode ;
  long FrmCoadds ;
  long ChpSettleReads ;
  long ChpCoadds ;
  long Savesets ;
  long NodSettleReads ;
  long NodSets ;
  long NodSettleChops ;
  long PreValidChops ;
  long PostValidChops ;
  long pixclock ;
  char obs_mode[40] ;
  char readout_mode[40] ;
} ufdcHrdwrCurrParam ;

typedef struct {
  long command_mode ;
  double FrameTime ;
  double saveFrq ;
  double exposureTime ;
  double ChopFreq ;
  double SCSDutyCycle ;
  double nodDwelTime ;
  double nodStlTime ;
  double preValidChopTime ;
  double postValidChopTime ;
  /* double chopDutyCycle ; */
  char obs_mode[40] ;
  char readout_mode[40] ;
  long pFlag ;
} ufdcPhysicalInput ;

typedef struct {
  long command_mode ;
  double FrameTime ;
  double saveFrq ;
  double exposureTime ;
  double ChopFreq ;
  double SCSDutyCycle ;
  double nodDwelTime ;
  double nodStlTime ;
  double preValidChopTime ;
  double postValidChopTime ;
  /* double chopDutyCycle ; */
  char obs_mode[40] ;
  char readout_mode[40] ;
} ufdcPhysicalCurrParam ;

typedef struct {
  char command[40] ;
  char obs_id[40] ;
  char dhs_write[40] ;
  char qckLk_id[40] ;
  long command_mode ;
  char local_archive[40] ;
  char nod_handshake[40] ;
  /*  char archive_host[40] ; */
  char archive_path[40] ;
  char archive_file_name[40] ;
  char comment[40] ;
  char remote_archive[40] ;
  char rem_archive_host[40] ;
  char rem_archive_path[40] ;
  char rem_archive_file_name[40] ;
} ufdcAcqContInput ;

typedef struct {
  char command[40] ;
  char obs_id[40] ;
  char dhs_write[40] ;
  char qckLk_id[40] ;
  long command_mode ;
  char local_archive[40] ;
  char nod_handshake[40] ;
  /*  char archive_host[40] ; */
  char archive_path[40] ;
  char archive_file_name[40] ;
  char comment[40] ;
  char remote_archive[40] ;
  char rem_archive_host[40] ;
  char rem_archive_path[40] ;
  char rem_archive_file_name[40] ;
} ufdcAcqContCurrParam ;

typedef struct {
  double chop_freq ;
  double percentile ;
  char conditions[40] ;
} cloud_cover_element ;

typedef struct {
  double conditions;
  double percentile ;
} water_vapour_element ;

typedef struct {
  long command_mode ;
  char lambda_low[40] ;
  char lambda_hi[40] ;
  char camera_mode[40] ;
  char obs_mode[40] ;
  char on_source_time[40] ;
  char readout_mode[40] ;
  char filter_name[40] ;
  char grating_name[40] ;
  char central_wavelength[40] ;
  char preValid_chop_time[40] ;
  char lyot_name[40] ;
  char sector_name[40] ;
  char slit_name[40] ;
  char window_name[40] ;
  char throughput[40] ;
  char chopThrow[40];
  double meta_env_data[8] ;
  char sky_background[40] ;
  char air_mass[40] ;
  char sky_noise[40] ;
  char ambient_temp[40] ;
  char tel_emiss[40] ;
  char rotat_rate[40] ;
  char chop_freq[40] ;
  char fid_frame_time[40] ;
  char K_FilterFrameTime[40] ;
  char L_FilterFrameTime[40] ;
  char M_FilterFrameTime[40] ;
  char LoRes10_FilterFrameTime[40] ;
  char HiRes10_FilterFrameTime[40] ;
  char LoRes20_FilterFrameTime[40] ;

} ufdcObsContInput ;

typedef struct {
  long command_mode ;
  char lambda_low[40] ;
  char lambda_hi[40] ;
  char camera_mode[40] ;
  char obs_mode[40] ;
  char on_source_time[40] ;
  char readout_mode[40] ;
  char filter_name[40] ;
  char grating_name[40] ;
  char central_wavelength[40] ;
  char preValid_chop_time[40] ;
  char lyot_name[40] ;
  char sector_name[40] ;
  char slit_name[40] ;
  char window_name[40] ;
  char throughput[40] ;
  char chopThrow[40];
  double meta_env_data[8] ;
  char sky_background[40] ;
  char air_mass[40] ;
  char sky_noise[40] ;
  char ambient_temp[40] ;
  char tel_emiss[40] ;
  char rotat_rate[40] ;
  char chop_freq[40] ;
  char fid_frame_time[40] ;
  char K_FilterFrameTime[40] ;
  char L_FilterFrameTime[40] ;
  char M_FilterFrameTime[40] ;
  char LoRes10_FilterFrameTime[40] ;
  char HiRes10_FilterFrameTime[40] ;
  char LoRes20_FilterFrameTime[40] ;

} ufdcObsContCurrParam ;

typedef struct {
  long command_mode ;
  char power[40] ;
} ufecVacChInput ;

typedef struct {
  long command_mode ;
  char power[40] ;
} ufecVacChCurrParam ;

typedef struct {
    long command_mode ;
    long datum_directive  ;
    char power[40] ;
    long park_directive ;
    long dac_id ;
    double dac_volt ;
    long latch_dac_id ;
    long read_all_directive ;
    char vGate[40];
    char vWell[40];
    char vBias[40];
    char password[40] ;
    long DetTemp ;
    char PassEnable[40] ;
} ufDCBiasInput ;

typedef struct {
    long command_mode ;
    char power[40] ;
    double M[24] ;
    long B[24] ;
    double dac_volt_values[24];
    long dac_control_values[24] ;
    long default_values[24] ;
    long min_values[24] ;
    long max_values[24] ;
    char vGate[40];
    char vWell[40] ;
    char vBias[40] ;
} ufDCBiasCurrParam ;

typedef struct {
    long command_mode ;
    long datum_directive  ;
    char power[40] ;
    long park_directive ;
    long preAmp_id ;
    double preAmp_volt ;
    long latch_preAmp_id ;
    long read_all_directive ;
    double adjust_value ;
    double global_set ;
} ufDCPreAmpInput ;

typedef struct {
    long command_mode ;
    char power[40] ;
    long default_values[24] ;
    long preAmp_control_values[24] ;
    double adjust_value ;
    double global_set ;
} ufDCPreAmpCurrParam ;

typedef struct {
  long command_mode ;
  char all_devices[40] ;
  char annex[40] ;
  char ls_218[40] ;
  char ls_340[40] ;
  char vacuum[40] ;
  char indexors[40] ;
  char cryocooler[40] ;
  char ppcVME[40] ;
  char mce4[40] ;
  char all_agents[40] ;
  char executive[40] ;
  char ufacqframed[40] ;
  char ufgdhsd[40] ;
  char ufgls218[40] ;
  char ufgls340[40] ;
  char ufg354vacd[40] ;
  char ufgmotord[40] ;
  char ufgmce4d[40] ;
} ufBTInput ;

typedef struct {
  long command_mode ;
  char all_devices[40] ;
  char annex[40] ;
  char ls_218[40] ;
  char ls_340[40] ;
  char vacuum[40] ;
  char indexors[40] ;
  char cryocooler[40] ;
  char ppcVME[40] ;
  char mce4[40] ;
  char all_agents[40] ;
  char executive[40] ;
  char ufacqframed[40] ;
  char ufgdhsd[40] ;
  char ufgls218[40] ;
  char ufgls340[40] ;
  char ufg354vacd[40] ;
  char ufgmotord[40] ;
  char ufgmce4d[40] ;
} ufBTCurrParam ;

typedef struct {
  char data_set_name[40] ;
  long total_frames ;
  long frame_count ;
} ufDHSInput ;

typedef struct {
  char data_set_name[40] ;
  long total_frames ;
  long frame_count ;
} ufDHSCurrParam ;

typedef struct {
  char data_set_name[40] ;
  long total_frames ;
  long frame_count[14] ;
} ufQlkInput ;

typedef struct {
  char data_set_name[40] ;
  long total_frames ;
  long frame_count[14] ;
} ufQlkCurrParam ;

#if defined(__UFGEMCOMM_C__)
int bingo = 0 ;    /* 1 for debug code */
int use_near_home = 1 ;
int use_backlash = 1 ;
int near_home_margin = 10 ;
int backlash_margin = -10 ;

int  USER_INPUT = 0 ;
int ADJUSTED_INPUT = 1 ;

int cmd_mode_lo =  0 ;
int cmd_mode_hi =  3 ;

double set_point_lo =  3.0;
double set_point_hi = 400.0;

int heater_range_lo  = 0;
int heater_range_hi  = 5;

double P_value_lo = 0.0 ;
double P_value_hi = 20000.0 ;

double I_value_lo = 0.0 ;
double I_value_hi  = 20000.0 ;

double D_value_lo  = 0.0 ;
double D_value_hi  = 20000.0 ;

int auto_tune_lo = 1;
int auto_tune_hi = 6;

double cryostat_ok_to_cool = 0.0005 ;

double cold_finger_lo = 1.0 ;
double cold_finger_hi = 15.0 ;

double cold1_lo  = 5;
double cold1_hi  = 25;

double cold2_lo  = 5;
double cold2_hi  = 25;

double active_lo  = 5;
double active_hi  = 25;

double passive_lo  = 5;
double passive_hi  = 25;

double window_lo  = 5;
double window_hi  = 25;

double strap_lo  = 5; 
double strap_hi  = 25;
 
double edge_lo  = 5;
double edge_hi  = 25;

double middle_lo  = 5;
double middle_hi  = 25;

double init_velocity_lo = 20.0 ;
double init_velocity_hi = 20000.0;

double slew_velocity_lo = 20.0 ;
double slew_velocity_hi = 20000.0;

double acceleration_lo = 0.0 ;
double acceleration_hi = 255.0;

double deceleration_lo = 0.0; 
double deceleration_hi = 255.0;

double drive_current_lo = 0.1;
double drive_current_hi = 100.0 ;

double datum_speed_lo = 0.0 ;
double datum_speed_hi = 20000.0 ;

double num_steps_lo = -1000000.01 ;
double num_steps_hi = 1000000.01;

double link_clear0   = 0.00 ;
double link_clear1  = -1.00 ;

int not_valid =  -1 ;

/* Observation Modes */

int Chop_nod = 0 ;
int Chop = 1 ;
int  Nod = 2 ;
int  Stare = 3 ;

/* Read out Modes */
int readout_mode0 = 0 ;
int readout_mode1 = 1 ;
int readout_mode2 = 2 ;

ufMotorPositions namedPositionsTable[10] ;

#else
extern int bingo ;
extern int use_near_home ;
extern int use_backlash ;
extern int  USER_INPUT  ;
extern int ADJUSTED_INPUT ;
extern int backlash_margin ;

extern int cmd_mode_lo ;
extern int cmd_mode_hi ;

extern double set_point_lo;
extern double set_point_hi;

extern int heater_range_lo ;
extern int heater_range_hi ;

extern double P_value_lo ;
extern double P_value_hi ;

extern double I_value_lo;
extern double I_value_hi ;

extern double D_value_lo ;
extern double D_value_hi  ;

extern int auto_tune_lo ;
extern int auto_tune_hi ;

extern double cryostat_ok_to_cool ;

extern double cold_finger_lo ;
extern double cold_finger_hi ;

extern double cold1_lo ;
extern double cold1_hi ;

extern double cold2_lo ;
extern double cold2_hi ;

extern double active_lo ;
extern double active_hi ;

extern double passive_lo;
extern double passive_hi ;

extern double window_lo ;
extern double window_hi ;

extern double strap_lo; 
extern double strap_hi ;
 
extern double edge_lo ;
extern double edge_hi ;

extern double middle_lo ;
extern double middle_hi  ;

extern double init_velocity_lo ;
extern double init_velocity_hi;

extern double slew_velocity_lo;
extern double slew_velocity_hi ;

extern double acceleration_lo ;
extern double acceleration_hi ;

extern double deceleration_lo ; 
extern double deceleration_hi ;

extern double drive_current_lo ;
extern double drive_current_hi;

extern double datum_speed_lo ;
extern double datum_speed_hi ;

extern double num_steps_lo ;
extern double num_steps_hi;

extern double link_clear0;
extern double link_clear1;

extern int not_valid ;

/* Observation Modes */

extern int Chop_nod ;
extern int Chop ;
extern int  Nod ;
extern int  Stare ;

/* Read out Modes */
extern int readout_mode0 ;
extern int readout_mode1 ;
extern int readout_mode2 ;

extern ufMotorPositions namedPositionsTable[10] ;

#endif

/* P U B L I C   F U N C T I O N S */

int checkSoc(int socfd, float timeOut);

long UFcheck_long(long value, long val1, long val2);
long UFcheck_long_r(long value, long val1, long val2);
long UFcheck_double(double value, double loval1, double upval2) ;

int UFcheck_motor_inputs (ufMotorInput mot_inp, char **com, char indexer,
	char *rec_name, ufMotorCurrParam *CurrParam, long com_mode) ;
int UFcheck_temp_inputs (ufTempInput temp_inp, char **com, char *rec_name,
	ufTempCurrParam *CurrParam, long com_mode) ;
int UFcheck_dc_inputs (ufdcHrdwrInput dcHwrdwr_inp, char **com, char paramLevel,
	char *rec_name, ufdcHrdwrCurrParam *CurrParam, long com_mode) ;
int UFcheck_dc_Phys_inputs (ufdcPhysicalInput dcPhysical_inp, char **com,
	char paramLevel, char *rec_name, ufdcPhysicalCurrParam *CurrParam,
	long com_mode) ;
int UFcheck_dc_Acq_inputs (ufdcAcqContInput dcAcq_inp, char **com,
	char paramLevel, char *rec_name, ufdcAcqContCurrParam *CurrParam,
	long com_mode) ;
int UFcheck_dc_Obs_inputs (ufdcObsContInput dcObsCont_inp, char **com,
	char paramLevel, char *rec_name, ufdcObsContCurrParam *CurrParam,
	long com_mode) ;
int UFcheck_ecVacCh_inputs (ufecVacChInput ecVacCh_inp, char **com,
	char *rec_name, ufecVacChCurrParam *CurrParam, long com_mode, int agent) ;
int UFcheck_bias_inputs (ufDCBiasInput bias_inp, char ** com, char *rec_name,
	ufDCBiasCurrParam *CurrParam,  long com_mode) ;
long UFcheck_preAmp_inputs (ufDCPreAmpInput preAmp_inp, char **com,
	char *rec_name, ufDCPreAmpCurrParam *CurrParam, long command_mode) ;
long UFcheck_bt_inputs (ufBTInput boot_inp, char **com, char *rec_name,
	ufBTCurrParam *CurrParam, long command_mode) ;
long UFAgentSend(char *name,int socFd,int Nstrings, char ** strings);
int UFGetConnection (const char * name, int port_no, const char *host);
long UFReadPositions (FILE *posFile, int num_mot, int last_mot_num) ; 
long UFReadPositionsFile_old (char *) ;
long UFPrintPositionsTable ();
long UFGetSteps (char *motorName, int posNum, double *offset);

long UFget_obs_mode_long(char * obs_mode) ;
void UFget_obs_mode(long obs_mode_long, char *obs_mode) ; 
long UFget_readout_mode_long(char * readout_mode) ;
void UFreadout_mode(long readout_mode_long, char *readout_mode);


#endif

