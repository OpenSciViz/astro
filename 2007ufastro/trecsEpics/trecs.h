#if !defined(__TRECS_H__)
#define __TRECS_H__ "RCS: $Name:  $ $Id: trecs.h,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufTRECSH[] = __TRECS_H__ ;

#define MIN_CAR_BUSY_TIME     30 // min ticks for busy 

#define SIMM_NONE             0    /* Real Mode "raw" */
#define SIMM_FAST             2    /* Simulation w/o the Agent "sim" */
#define SIMM_FULL             3    /* Simulation with the Agent "sim" */


#define TRX_DEBUG_NONE        0   /* Just error statements */
#define TRX_DEBUG_MIN         1   /* Error + descriptions */
#define TRX_DEBUG_FULL        2   /* Everything */

#if defined(__TRECS_C__)
int trecs_initialized  = 0 ;
int NumFiles = 0 ;
int Hi_Level_Move[10] = {0,0,0,0,0,0,0,0,0,0};
#else
extern int trecs_initialized ;
extern int NumFiles ;
extern int Hi_Level_Move[10] ;
#endif

char ccconfig_filename[] = "./pv/ccconfig.txt" ;
char mot_filename[] = "./pv/mot_param.txt" ;

char dcconfig_filename[] = "./pv/dcconfig.txt" ;
char dc_param_filename[] ="./pv/dc_param.txt" ;
char dc_bias_filename[] = "./pv/dc_bias_param.txt" ;
char ecconfig_filename[] = "./pv/ecconfig.txt" ;
char env_gs_filename[] = "./pv/env_gs.txt" ;
char env_hb_filename[] = "./pv/env_hb.txt" ;
char env_array_filename[] = "./pv/env_array.txt" ;
char env_tempMon_filename[] = "./pv/env_tempMon.txt" ;
char env_press_filename[] = "./pv/env_press.txt" ;
char temp_cont_filename[] = "./pv/temp_cont.txt" ;
char ch_param_filename[] ="./pv/ch_param.txt" ;
char vac_param_filename[] ="./pv/vac_param.txt" ;
char bt_param_filename[] ="./pv/bt_param.txt" ;

/**********************************************************/
/**********************************************************/
/**********************************************************/
extern int init_trecs_config () ;

extern void trx_debug(char *message, char *rec_name, char *mess_level, char *debug_level) ;

#endif

