# Tcl Message Interface
#
#my $rcsId = q($Name:  $ $Id: orig_slow_msg.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

proc msg_debug { args } {
	global env

    catch {
	if { $env(MSGDEBUG) != 0 } {
	    puts $args
	}
    }
}

proc msg_wait { server msgid { cmd {} } } {
	msg_debug Wait: $server $msgid
	upvar #0 $server S

    set S(msgid) $msgid

    vwait ${server}($msgid);
    msg_debug Wait: $server $msgid continue
    catch { after cancel $S(${msgid}_timer) }
    catch { after cancel $S(reopen_timer) }

    set value $S($msgid)

    unset S(${msgid})
    unset S(${msgid}_timer)

    if { [lindex $value 0] == -1 } {
	error "nak [lrange $value 1 end]"
    } 
    if { [lindex $value 0] == -2 } {
        error "timeout $cmd"
    }
    if { [lindex $value 0] ==  1 } {
	return  [concat [lrange $value 1 end]]
    }
}

proc msg_timeout { server msgid } {
	msg_debug Timeout: $server $msgid
	upvar #0 $server S

    set S($msgid) -2
}

proc msg_reopen { server } {
	msg_debug Reopen: $server
	upvar #0 $server S

    set S(up) 0

    if { [catch {
	set sock [msg_setsock $server]
    }] } {
        set S(reopen_timer) [after $S(reopen) "msg_reopen $server"]
    }
}

proc msg_cmd { server cmd { timeout {} } { sync sync } { retry no } } {
	upvar #0 $server S

    set msgid $S(N)
    incr S(N) 2
    set sock $S(sock)
    set line [join [concat $msgid $cmd]]

    if { ![string compare $retry retry] } {
        set S(line) $line
    } else {
	set S(line) "$msgid ack"
    }

    msg_debug Msg: $line

    if { [catch {
	if { [catch {
	    puts $sock $line
            flush $sock
	}] } {
	    set sock [msg_setsock $server]
	    puts $sock $line
            flush $sock
	}
    }] } {
        catch { after cancel $S(reopen_timer) }
        set S(reopen_timer) [after $S(reopen) "msg_reopen $server"]
    }

    if { [string compare $timeout {}] == 0 } {
	set timeout $S(timeout)
    }

    set S($msgid) 0
    set S(${msgid}_timer) [after $timeout "msg_timeout $server $msgid"]

    if { ![string compare $sync sync] } {
	msg_wait $server $msgid $cmd
    } else {
	return $msgid
    }
}

proc msg_close { server } {
    upvar #0 $server S
	close $S(sock)
    if { [ string compare $S(logfile) NULL ] != 0 } {
	close $S(logfile)
    }
}

proc msg_sock { server } {
    upvar #0 $server S
	return $S(sock)
}


proc msg_getline { server sock } {
	upvar #0 $server S

    set len 0
    set err [catch { set len [gets $sock line] }]
    if { $len < 0 || $err == 1 } {
	if { $S(type) == 1 } { msg_debug Kil Server $server }
	if { $S(type) == 2 } { 
	    msg_debug Kil Client $S($sock)
            msg_kilclient  $server $sock 
    }

	close $sock
	set S(sock) ""

	if { $S(type) == 1 } {
	    set S(connection) Down

	    if { [catch { uplevel #0 $S(done) }] == 1 } {
		    global errorInfo

		tk_messageBox -icon error -type ok  \
		      -message "Error executing client done code: $errorInfo"
	    }

	    set line $S(line)
	    after $S(reopen)	\
		    "msg_reopen $server"
	}
	return {}
    }

    return $line
}

proc msg_handle { server sock } {
	upvar #0 $server S

    set line [msg_getline $server $sock]

    if { [string compare $line {}] == 0 } { return }

    if { [string match {[+0123456789]*} $line] == 0 } {
	set line "0 $line"
    }
    msg_debug Handle: $line
    set msgid  [lindex $line 0]
    set cmd    [lindex $line 1]
    set arg    [lindex $line 2]
    set line   [split $line]
    msg_logmsg $server "Command" "$cmd $arg"
    if { [catch { interp eval $server $cmd $server $sock $line }] } {
    	    global errorInfo

	# Select only the first line of the error
	# remove all {}s
	msg_nak $sock $msgid $errorInfo
	msg_logmsg $server "Error" "$cmd $arg"
    }
}

proc msg_set { server name value { timeout {} } { sync sync } } {
    msg_cmd $server "set $name $value" $timeout $sync
}

proc msg_get { server name { timeout {} } { sync sync } } {
    msg_cmd $server "get $name" $timeout $sync
}

proc msg_list { server P { timeout {} } } {
        upvar #0 $server S
	upvar $P p
 
    set sock $S(sock)

    msg_cmd $server "lst" $timeout
    while { 1 } {
        if { ![string compare				\
		[set line [msg_getline $server $sock]]  \
		"----LIST----"] } {
		break
	}
	lappend p "[lindex $line 0] [lindex $line 1] [list [lrange $line 2 end]]"
    }
}

proc msg_setsock { server } {
	upvar #0 $server S
	msg_debug Sock: $server $S(host) $S(port)

    set host $S(host)
    set port $S(port)

    set sock [socket $host $port]
    set S(sock) $sock

    fileevent $sock readable "msg_handle $server $sock"
    fconfigure $sock -buffering line


    if { [catch { uplevel #0 $S(init) }] == 0 } {
	if { [catch {
	    # Re-sync the variables in the message map
	    #
	    set up $S(up)
	    set $S(up) 0
	    foreach m $S(+vars) { 
		set var  [lindex $m 0]
		set name [lindex $m 1]
		set init [lindex $m 2]

		upvar #0 $var v

		if { [string compare $init Server] == 0 \
		 || ([string compare $init Up]     == 0 && $S(up) == 1) } {
		    set v [msg_get $server $name]
		}
		if { [string compare $init Client] == 0 \
		 || ([string compare $init Up]     == 0 && $S(up) == 0) } {
		    msg_set $server $name $v
		}
	    }
	    set #S(up) $up
	}] != 0 } {
    		global errorInfo

	    tk_messageBox -icon error -type ok  \
		    -message "Error syncing mapped vars with $server: $errorInfo"

	}

	if { [catch {
	    # Re-establish the subscriptions
	    #
	    foreach sub $S(+subs) {
		msg_cmd $server "sub [lindex $sub 0] [lindex $sub 3]"
	    }
	}] != 0 } {
    		global errorInfo

	    tk_messageBox -icon error -type ok  \
		    -message "Error re-establishing subscriptions with $server: $errorInfo"
	}

    } else {
    		global errorInfo

	tk_messageBox -icon error -type ok  \
	      -message "Error executing client init code: $errorInfo"
    }

    set S(up) 1
    set S(connection) Up
    return $sock
}

proc msg_subscribe { server name var { code {} } { update 0 } { timeout {} } } {
	msg_debug CSub: $server $name
	upvar #0 $server S

	set S($name) $var
	lappend S(+subs) [list $name $var $code $update]

	if { [string compare $code {}] != 0 } {
	    upvar #0 $var v
	    trace variable v w $code
	}

	if { $S(up) } { msg_cmd $server "sub $name $update" $timeout }
}

proc msg_slst { server sock msgid lst } {
	msg_debug SLst: $server $sock $msgid
	upvar #0 $server S

	msg_ack $sock $msgid

	puts $sock "server	$server	$S(name)"

	foreach { var comment } [array get S P,*] {
	    msg_debug  "published	[string range $var 2 end]	$comment"
	    puts $sock "published	[string range $var 2 end]	$comment"
	}
	foreach { cmd comment } [array get S R,*] {
	    msg_debug  "registered	[string range $cmd 2 end]	$comment"
	    puts $sock "registered	[string range $cmd 2 end]	$comment"
	}
	msg_puts $sock ----LIST----
}

proc msg_slog { server log } {
    msg_log $server $log
}

proc msg_sget { server sock msgid get name } {
	msg_debug SGet: $server $sock $msgid $name
	upvar #0 $server S

	if { [string compare $S($name) {}] == 0 } {
	    msg_nak $sock $msgid "No variable $name"
	    return
	}

	set variable $S($name)
	upvar #0 $variable var

	if { [catch { msg_ack $sock $msgid $var }] } {
	    msg_nak $sock $msgid "cannot access $name"
	}
}

proc msg_ssub { server sock msgid sub name { timeout 0 } } {
	msg_debug SSub: $server $sock $msgid $name
	upvar #0 $server S
        upvar #0 $S($name) var

        lappend S(+$sock) $name

	if { [string compare $S($name) {}] == 0 } {
	    msg_nak $sock $msgid "No variable $name"
	    return
	}
	lappend S(+$name) $sock
	msg_ack $sock $msgid
	msg_puts $sock 0 set $name $var
}

proc msg_sset { server sock msgid set args } {
	set name  [lindex $args 0]
	set value [lrange $args 1 end]

	msg_debug SSet: $server $sock $msgid $name $value
	upvar #0 $server S

	if { [string compare $S($name) {}] == 0 } {
	    msg_nak $sock $msgid "No variable $name"
	    return
	}

	set variable $S($name)
	upvar #0 $variable var

	if { [catch {
	    set var $value
	    msg_ack $sock $msgid
	}] } {
	    msg_nak $sock $msgid "cannot access $name"
	}
}

proc msg_sack { server sock msgid ack args } {
	msg_ack $sock $msgid $args
}

proc msg_cset { server sock msgid set args } {
	set name  [lindex $args 0]
	set value [join [lrange $args 1 end]]

	upvar #0 $server S
	msg_debug CSet: $server $sock $msgid $name $value ($S($name))

	global $S($name)

	set S(setting) $name

	if { [catch { set $S($name) $value }] } {
		global errorInfo
	    puts "Can't set $S($name)" : $errorInfo
	}

	set S(setting) {}
}

proc msg_cnak { server sock msgid ack args } {
	upvar #0 $server S

    set arg [join $args]
    set S($msgid) "-1 $arg"
}

proc msg_cack { server sock msgid ack args } {
	upvar #0 $server S

    set arg [join $args]
    set S($msgid) "1 $arg"
}

proc msg_nak { sock msgid args } {
    msg_debug Nak: $sock $msgid $args
    if { $msgid != 0 } {
	msg_puts $sock $msgid nak $args
    }
}

proc msg_ack { sock msgid args } {
    msg_debug Ack: $sock $msgid $args
    if { $msgid != 0 } {
	msg_puts $sock $msgid ack $args
    }
}

proc msg_security { server peer } {
	upvar #0 $server S
    puts $peer
    msg_checkhost $peer $S(hosts.allow) $S(hosts.deny)
}

proc msg_checkhost {hostname allow deny} {
    set host [string tolower $hostname]
    for {set i 0} {$i <= [expr [llength $allow] - 1]} {incr i 1} {
	if {[msg_matchone $host [lindex $allow $i]] == 1} {
	    return 1
	}
    }
    for {set j 0} {$j <= [expr [llength  $deny] - 1]} {incr j 1} {
	if {[msg_matchone $host [lindex  $deny $j]] == 1} {
	    return 0
	}
    }
    return 1
}


proc msg_matchone {hostname pattern} {
        set host [split $hostname .]
        set pat [split $pattern .] 
        regsub {\*} $pat {.*} pat
	set lenpat [llength $pat]
        set lenhost [llength $host]
    if {$lenhost < $lenpat} {
	return 0
    }
    if {$lenpat < $lenhost} {
	for {set c 0} {$c < [expr [llength $host] - $num]} {incr c 1} {
	    set pat [linsert $pat 0 .]
	}
    }	
    for {set i $lenhost} {$i > 0} {incr i -1} {
	set j [expr $i - 1]  
	set phost [lindex $host $j]
	set ppat [lindex $pat $j]
	if ![regexp ^$ppat$ $phost] {
	    if {$ppat == "."} {
		return 1
	    } else {
		return 0
	    }
	}
    }
    return 1
}




proc msg_accept { server sock addr port } {
	upvar #0 $server S

    set peer [lindex [fconfigure $sock -peername] 1]
    puts [fconfigure $sock -peername]
    set S($sock) $peer

    msg_debug New Client from $peer

    if { [msg_security $server $peer] == 1 } {
	fileevent $sock readable "msg_handle $server $sock"
	fconfigure $sock -buffering line
	msg_logmsg $server "Newclient" "$S($sock) $sock"
    } else {
	msg_debug Kil Client no permission for $peer
	msg_logmsg $server "Kilclient" "$S($sock) $sock" "permission denied" 
	close $sock
    }

    return
}

proc msg_init { server address type } {
	global env
	upvar #0 $server S


    if { [string compare $address {}] == 0 } {
	set name [string toupper $env($server)]
    } else {
	set name $address
    }

    set host [lindex [split $name : ] 0]
    if { [string compare $host "."] == 0 } {
	set host [info hostname]
    }
    set port [lindex [split $name : ] 1]

    set S(connection) Down
    set S(timeout) 	 3000
    set S(reopen)  	 1000
    set S(hosts.allow) $host
    set S(hosts.deny)  { * }
    set S(logfile)      NULL
    set S(log)          NULL
    set S(N) 		$type

    set S(type) 	$type
    set S(name) 	$name
    set S(host) 	$host
    set S(port) 	$port
    
    set S(setting)	{}

    msg_debug Init: $server name: $name host: $host port: $port

    catch { interp delete $server }
    interp create -safe $server
}

proc msg_down { server } { 
	upvar #0 $server S

    set sock $S(sock)
    close $sock

#    set clients ""
#    catch { set clients $S(+$name) }

#    foreach sock $clients {
#	close $sock
#    }
}

proc msg_up { server } { 
	msg_debug Up: $server
	upvar #0 $server S

    set port $S(port)
    set S(sock) [socket -server "msg_accept $server" $port]
}

proc msg_server { server { address {} } { log {} } } {
    upvar #0 $server S
    global logger
    msg_init $server $address 2
    msg_log $server $log
    
    interp eval $server rename set \{\}
    interp alias $server set {} msg_sset
    interp alias $server get {} msg_sget
    interp alias $server lst {} msg_slst
    interp alias $server sub {} msg_ssub
    interp alias $server ack {} msg_sack
    interp alias $server log {} msg_slog
    msg_publish $server log logger
}

proc msg_client { server { init { } } { done { } } { address {} } } {
	upvar #0 $server S

    set S(up) 0

    msg_init $server $address 1

    set S(init) $init
    set S(done) $done
    set S(sock) ""
    set S(+subs) ""
    set S(+vars) ""


    interp eval $server rename set \{\}
    interp alias $server set {} msg_cset
    interp alias $server ack {} msg_cack
    interp alias $server nak {} msg_cnak

    msg_reopen $server
}

# Server side access control
#
proc msg_allow { server allow } {
	upvar #0 $server S

    set S(hosts.allow) $allow
}
proc msg_deny { server deny } {
	upvar #0 $server S

    set S(hosts.deny)  $deny
}

# Server side bindings
#
proc msg_register { server command { comment {} } } {
	upvar #0 $server S

    set S(R,$command) $comment
    $server alias $command $server.$command 
}

proc msg_publish { server name var { code {} } { comment {} } } {
	upvar #0 $server S

    set S($name) $var
    set S(P,$name) $comment
    upvar #0 $var v
    if { [string compare $code {}] != 0 } {
	trace variable v rw $code
    }
    trace variable v w "msg_postvar $server $name"
}

proc msg_postvar { server name var index op } {
    msg_post $server $name
}

proc msg_post { server name } {
	upvar #0 $server S
	upvar #0 $S($name) var

    set clients ""
    catch { set clients $S(+$name)
        set S(+$name) {} 
    }
    foreach sock $clients {
	msg_debug Post: $sock $name $var
	if { ![catch {
	    msg_puts $sock 0 set $name $var
	}] } {
	    lappend S(+$name) $sock
	}
    }
}

# Lowest level output
#
proc msg_puts { sock args } {
	puts $sock [join $args]
}

proc msg_setvar { server name variable indx op } {
	upvar #0 $server S 
	upvar $variable value

    if { [string compare $name $S(setting)] } {
	if { $S(up) == 1 } {
	    if { $op == "r" } {
		set value [msg_get $server ${name}]
	    }
	    if { $op == "w" } {
		msg_set $server ${name} $value
	    }
	}
    }
}

proc msg_vset { name indx op } {
	    upvar #0  $name  value
	    upvar #0 _$name _value

    if { $op == "r" } {
	set value $_value
    }
    if { $op == "w" } {
	set _value $value
    }
}

proc msg_variable { server name var mode { def 0 } { init {} } { code {} } } {
	upvar #0 $server S
	upvar #0  ${var}   v
	upvar #0 _${var}  _v
	global $var _$var

	if { [info exists v] == 0 } { 
	    set $var $def
	}
	if { [info exists _v] == 0 } { 
	    set _$var $def
	}

	set S($var) $name
	set S($var,value) $v

	lappend S(+vars) [list $var $name $init]
	if { ![string compare $init Server]
	  || ![string compare $init Up] } {
	    set v [msg_get $server $name]
	}

	if { $code == {} } {
	    set code "msg_vset"
        }

	trace variable $var $mode [list msg_setvar $server $name]
	#trace variable  $var w $code
}

proc msg_mapvar { server Map } {
 foreach m $Map {
        set var  [lindex $m 0]
        set name [lindex $m 1]
        set def  [lindex $m 2]
        set init [lindex $m 3]

	if { [string compare $init {}] == 0 } {
	    set init Up
	}
 
	msg_variable $server $name $var rw $def $init
 }
}

proc msg_log {server log} {
    upvar #0 $server S
    if { [string compare $log {} ] != 0 } {
	if { [file exists $log] } {
	    set S(log) $log
	    set S(logfile) [open $log a]
	    fconfigure $S(logfile) -buffering line
	}
    }
}

proc msg_logmsg {server args} {
    upvar #0 $server S
    global logger
    set logger "[exec date] $args\n"    
    if { [string compare $S(logfile) NULL] != 0} {
	puts $S(logfile) $logger
    }
}

proc msg_kilclient { server sock } {
    upvar #0 $server S

    msg_logmsg $server "Kilclient" "$S($sock) $sock"

    foreach value $S(+$sock) {
	set ix [lsearch -exact $S(+$value) $value]
	set S(+$value) [lreplace $S(+$value) $ix $ix]
    }
    unset S(+$sock)
}

