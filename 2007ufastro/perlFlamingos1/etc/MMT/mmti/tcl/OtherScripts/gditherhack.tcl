#!/bin/sh
#\
exec tclsh "$0" "$@"

source $env(MMTITCL)/msg.tcl

msg_server GUIDSERV

msg_variable GUIDSERV image1x image1x rw
msg_variable GUIDSERV image2x image2x rw
msg_variable GUIDSERV image1y image1y rw
msg_variable GUIDSERV image2y image2y rw

msg_up GUIDSERV

msg_allow GUIDSERV flamingos1b

vwait forever

#my $rcsId = q($Name:  $ $Id: gditherhack.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);
