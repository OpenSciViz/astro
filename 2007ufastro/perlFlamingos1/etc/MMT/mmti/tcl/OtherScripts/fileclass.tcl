
set NullFileClass(newname)       NewFile
set NullFileClass(file)          {}
set NullFileClass(directory)     {}
set NullFileClass(dirslist)      {}
set NullFileClass(loadtype)      {}
set NullFileClass(pattern)        0
set NullFileClass(types) {
    { "All files"               * }
}

proc OpenFileDialog {parent fileclass} {
        FileDialog $parent $fileclass tk_getOpenFile
}
proc SaveFileDialog {parent fileclass} {
        FileDialog $parent $fileclass tk_getSaveFile
}

proc FileDialog { parent class type } {
        global FileDialogType
        global tk_strictMotif
        upvar #0 $class fileclass
        global FileNameType
 
    set blueplate 1
    set tk_strictMotif 0
 
    switch $FileDialogType {
        motif   { set tk_strictMotif 1 }
        windows { set tk_strictMotif 0 }
    }
 
    set filepath [$type                               \
                    -initialdir  $fileclass(directory)\
                    -initialfile $fileclass(file)     \
                    -filetypes   $fileclass(types)    \
                    -title       "Open File"          \
                    -blueplate   $blueplate           \
                    -dirslist    $fileclass(dirslist) \
                    -loadtype    $fileclass(loadtype) \
                    -pattern     $fileclass(pattern)  \
                    -parent $parent]
 
    if { $filepath != "" } {
        if { $FileNameType == "File pattern:" } {
            set file [lindex $filepath 0]
        } else {
            set file $filepath
        }
 
        set fileclass(path)      $file
        set fileclass(name)      [file tail $file]
        set fileclass(directory) [file dirname $file]
    }
 
    return $filepath
}

#my $rcsId = q($Name:  $ $Id: fileclass.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);
