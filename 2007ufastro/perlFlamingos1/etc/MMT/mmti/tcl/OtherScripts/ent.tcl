# Labeled Entry Thingy
#
#my $rcsId = q($Name:  $ $Id: ent.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

set EntryValue  0
set EntryWidget .


proc retcopy { name indx op } {
	    upvar $name value

    	global ${name}.buffer
	set ${name}.buffer $value
}

proc retentry { w args } { 
    set var [lindex [split $w .] end]

    global $var ${var}.buffer
 
    catch {
	upvar #0 $var value
	set ${var}.buffer $value
    }

    trace variable ${var} w { retcopy }
    
    eval entry $w -textvariable ${var}.buffer -justify right $args

    # This forces a trace on the return key
    #
    bind $w <Key-Return> {
	set [lindex [split %W .] end] [%W get]
    }
    bind $w <FocusOut> {
	upvar #0 [lindex [split %W .] end]        val
	upvar #0 [lindex [split %W .] end].buffer buf

	if { [string compare $val $buf] != 0 } {
	    %W delete 0 end
	    %W insert 0 $val
	}
    }

    return $w
}

proc msgentry { server lab w { def 0 } { init {} } { code {} } { timeout {} } { sync {} } args } {
    set var [lindex [split $w .] end]

    msg_variable $server $var $var w $def $init $code $timeout $sync

    label ${w}_l -text $lab -anchor w
    set predash ""
    set pstdash ""
    while { [lindex $args 0] == "-"  } {
	set args [lrange $args 1 end]
	set predash "$predash -"
    }
    while { [lindex $args 0] == "+"  } {
	set args [lrange $args 1 end]
	set pstdash "$pstdash -"
    }
    
    eval retentry $w $args

    return "${w}_l $predash $w $pstdash"
}

proc msgentry_async { server lab w { def 0 } { init {} } { code {} } { timeout {} } { 
sync {} } args } {
    set var [lindex [split $w .] end]
 
    msg_variable $server $var $var w $def $init $code $timeout $sync
 
    label ${w}_l -text $lab -anchor w
    set predash ""
    set pstdash ""
    while { [lindex $args 0] == "-"  } {
        set args [lrange $args 1 end]
        set predash "$predash -"
    }
    while { [lindex $args 0] == "+"  } {
        set args [lrange $args 1 end]
        set pstdash "$pstdash -"
    }
    
    eval retentry $w $args
 
    return "${w}_l $predash $w $pstdash"
}

proc labentry { lab w args } {
    set var [lindex [split $w .] end]

    global $var

    label ${w}_l -text $lab -anchor w
    set predash ""
    set pstdash ""
    while { [lindex $args 0] == "-"  } {
	set args [lrange $args 1 end]
	set predash "$predash -"
    }
    while { [lindex $args 0] == "+"  } {
	set args [lrange $args 1 end]
	set pstdash "$pstdash -"
    }
    
    eval entry $w -textvariable $var -justify right $args

    return "${w}_l $predash $w $pstdash"
}

