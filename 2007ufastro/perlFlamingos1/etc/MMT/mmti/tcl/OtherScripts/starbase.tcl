# Starbase Tables Interface
#
#my $rcsId = q($Name:  $ $Id: starbase.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

proc Starbase {} {
	global  Starbase
	return $Starbase(version)
}

set Starbase(version) "Starbase Tcl Driver 1.0"

proc starbase_nrows   { D } 	        { upvar $D data; return $data(Nrows) 	}
proc starbase_ncols   { D } 	        { upvar $D data; return $data(Ncols) 	}
proc starbase_colname { D num  }        { upvar $D data; return $data(0,$num) 	}
proc starbase_columns { D      }        { upvar $D data; return $data(Header) 	}
proc starbase_colnum  { D name }        { upvar $D data; return $data($name) 	}
proc starbase_get     { D row col }     { upvar $D data; return $data($row,$col) 	}
proc starbase_set     { D row col val } { upvar $D data; set data($row,$col) $val; 	}

proc starbase_init { t } {
	upvar t T

	set T(Nrows) 0
	set T(Ncols) 0
	set T(Header) ""
}

# Set up a starbase data array for use with ted
#
proc starbase_driver { Dr } {
	upvar $Dr driver

	set driver(nrows)	starbase_nrows
	set driver(ncols)	starbase_ncols
	set driver(get)		starbase_get
	set driver(set)		starbase_set
	set driver(colname)	starbase_colname
	set driver(colnum)	starbase_colnum
	set driver(columns)	starbase_columns
	set driver(colins)	starbase_colins
	set driver(coldel)	starbase_coldel
	set driver(colapp)	starbase_colapp
	set driver(rowins)	starbase_rowins
	set driver(rowdel)	starbase_rowdel
	set driver(rowapp)	starbase_rowapp
}
starbase_driver Starbase

proc starbase_new { t args } {
	upvar $t T

    set T(Header) $args
    set T(Ndshs)  [llength $T(Header)]
    set T(HLines) 2
    starbase_colmap T

    set T(Nrows) 0
}

proc starbase_colmap { h } {
	upvar $h H

    set c 0	
    foreach column $H(Header) {
	incr c
	set column [string trim $column]
	set H($column) $c
	set H(0,$c) $column
    }
    set H(Ncols) $c
}

proc starbase_coldel { t here } {
	upvar $t T

    set Ncols $T(Ncols)

    set T(Header) [lreplace $T(Header) [expr $here - 1] [expr $here - 1]
    starbase_colmap T

    for { set row 1 } { $row <= $T(Nrows) } { incr row } {
        for { set col $here } { $col < $Ncols } { incr col } {
	    if { [catch { set val $T($row,[expr $col + 1]) }] } {
		set T($row,$col) ""
	    } else {
	        set T($row,$col) $val
	    }
	}
    }
}

proc starbase_colins { t name here } {
	upvar $t T

    if { [info exists $T(Header)] == 0 } {
	set T(Header) $name
    } else {
        set T(Header) [linsert $T(Header) [expr $here - 1] $name]
    }
    starbase_colmap T

    for { set row 1 } { $row <= $T(Nrows) } { incr row } {
        for { set col $T(Ncol) } { $col > $here } { incr col -1 } {
	    if { [catch { set val $T($row,[expr $col - 1]) }] } {
		set T($row,$col) ""
	    } else {
	        set T($row,$col) $val
	    }
	}
    }

    for { set row 1 } { $row <= $T(Nrows) } { incr row } {
	set T($row,$here) ""
    }
}

proc starbase_header { fp h } {
	upvar $h H
	global starbase_line
	set N 1

    if { [info exists starbase_line] } {
	set line $starbase_line
	set n 1

	set H(H-$n) $line
	if { [regexp -- {^ *(-)+ *(\t *(-)+ *)*} $line] } break

	if { $n >= 2 } {
	    set l [split $H(H-[expr $n-1]) "\t"]
	    if { [llength $l] > 1 } {
		set H(H-[lindex $l 0]) [lrange $l 1 end]
		set H(N-[lindex $l 0]) [expr $n-1]
	    }
	}

	unset starbase_line
	set N 2
    }
    for { set n $N } { [set eof [gets $fp line]] != -1 } { incr n } {
	set H(H-$n) $line
	if { [regexp -- {^ *(-)+ *(\t *(-)+ *)*} $line] } break

	if { $n >= 2 } {
	    set l [split $H(H-[expr $n-1]) "\t"]
	    if { [llength $l] > 1 } {
		set H(H-[lindex $l 0]) [lrange $l 1 end]
		set H(N-[lindex $l 0]) [expr $n-1]
	    }
	}

    }

    if { $eof == -1 } {
	    error "starbase_header: eof"
    }

    set H(H-$n) $line
    set H(HLines) $n

    set H(Header) [split $H(H-[expr $n-1])  "\t"]
    set H(Dashes) [split $H(H-$n)		"\t"]
    set H(Ndshs)  [llength $H(Dashes)]

    starbase_colmap H

    return H(Header)
}

proc starbase_hdrval { h name } {
    upvar $h H

    return $H(H-$name)
}

proc starbase_hdrset { h name value } {
    upvar  #0 $h H 

    if { ![info exists H(H-$name)] } {
	set n [incr H(HLines)]

	set H(H-[expr $n-0]) $H(H-[expr $n-1])
	set H(H-[expr $n-1]) $H(H-[expr $n-2])
	set H(N-$name) [expr $n-2]
    }
    set H(H-$name) 	  $value
    set H(H-$H(N-$name)) "$name	$value"
}

proc starbase_hdrput { fp h } {
	upvar $h H

    set nl [expr $H(HLines) - 2]
    for { set l 1 } { $l <= $nl } {  incr l } {
	    puts $fp $H(H-$l)
    }

    set nc $H(Ncols)
    for { set c 1 } { $c < $nc } {  incr c } {
	puts -nonewline $fp "$H(0,$c)	"
    }
    puts $fp "$H(0,$c)"

    for { set c 1 } { $c < $nc } {  incr c } {
	puts -nonewline $fp "-	"
    }
    puts $fp "-"
}

proc starbase_readfp { fp t } {
	upvar $t T

    starbase_header $fp T

    set NCols [starbase_ncols T]

    for { set r 1 } { [gets $fp line] != -1 } { incr r } {
	if { [string index $line 0] == "\f" } {
	    global starbase_line
	    set starbase_line [string range $line 1 end]
	    break
	}
	set c 1
	foreach val [split $line "\t"] {
	    set T($r,$c) [string trim $val]
	    incr c
	}
	for { } { $c <= $NCols } { incr c } {
	    set T($r,$c) {}
	}
    }
    set T(Nrows) [expr $r-1]
}

proc starbase_read { file t } {
	upvar $t T

    set fp [open $file]
    starbase_readfp $fp T
    close $fp

    set T(filename) $file
}

proc starbase_writefp { fp t } {
	upvar $t T

    starbase_hdrput $fp T

    set nr $T(Nrows)
    set nc $T(Ncols)
    for { set r 1 } { $r <= $nr } {  incr r } {
	for { set c 1 } { $c < $nc } {  incr c } {
	    if { [catch { set val $T($r,$c) }] } {
		    set val ""
	    }
		    
	    puts -nonewline $fp "$val	"
	}
	if { [catch { set val $T($r,$c) }] } {
		set val ""
	}
	puts $fp $val
    }
}

proc starbase_write { file t } {
	upvar $t T

    set fp [open $file w]
    starbase_writefp $fp T
    close $fp
}

proc starbase_rowins { t row } {
		upvar $t T

	incr T(Nrows)

	set nr $T(Nrows)
	set nc $T(Ncols)
	for { set r $nr } { $r > $row } { set r [expr $r-1] } {
	    for { set c 1 } { $c <= $nc } { incr c } {
		if { [catch { set val $T([expr $r-1],$c) }] } {
			set val ""
		}
			
	        set T($r,$c) $val
	    }
	}

	for { set c 1 } { $c <= $nc } { incr c } {
	    set T($r,$c) ""
	}
}

proc starbase_rowdel { t row } {
	upvar $t T

    incr T(Nrows) -1

    set nr $T(Nrows)
    set nc $T(Ncols)
    for { set r $row } { $r <= $nr } { incr r } {
	for { set c 1 } { $c <= $nc } { incr c } {
	    if { [catch { set val $T([expr $r+1],$c) }] } {
		    set val ""
	    }
		    
	    set T($r,$c) $val
	}
    }

    for { set c 1 } { $c <= $nc } { incr c } {
	set T($r,$c) ""
    }
}

proc starbase_httpreader { t wait sock http } {
	global $t
	upvar #0 $wait W
	upvar #0 $t T

    set T(http) $http

    if { ![info exists ${t}(state)] || ([info exists ${t}(state)] && $T(state) == 0 ) } {
	fconfigure $sock -blocking 1

	set T(state)	1
	set T(Nrows)	0
	set T(HLines)	0
    } else {
	if { $T(state) == 1 } {
	    incr ${t}(HLines)
	    set n $T(HLines)

	    if { [gets $sock line] == -1 } {
		    error "starbase_header: eof"
	    }

	    set T(H-$n) $line
	    set l [split $line]
	    if { [llength $l] > 1 } {
		set T(H-[lindex $l 0]) [lrange $l 1 end]
	    }
	    set T(H-$n) $line

	    if { [regexp -- {^ *(-)+ *(\t *(-)+ *)*} $line] } {
		set T(Header) [split $T(H-[expr $n-1])  "\t"]
		set T(Dashes) [split $T(H-$n)		"\t"]
		set T(Ndshs)  [llength $T(Dashes)]

		starbase_colmap T

		set T(state) 2
	    }
	} else {
	    if { [gets $sock line] == -1 } {
		set T(state) 0
		set W 1
	    } else {
		incr ${t}(Nrows)
		set r $T(Nrows)

		set NCols [starbase_ncols T]
		set c 1
		foreach val [split $line "\t"] {
		    set T($r,$c) $val
		    incr c
		}
		for { } { $c <= $NCols } { incr c } {
		    set T($r,$c) {}
		}
	    }
	}
    }
}

proc starbase_cancel { t wait http } {
	upvar #0 $wait W
	upvar #0 $t T

	set T(state) 0
	set W 1
}

proc starbase_http { url t wait } {
	upvar #0 $t T

    set T(http) [http::geturl $url 				\
		-handler [list starbase_httpreader $t $wait] 	\
		-command [list starbase_cancel $t $wait]]
}

proc starbase_httpkill { t } {
	upvar #0 $t T

    http::reset $T(http)
}
