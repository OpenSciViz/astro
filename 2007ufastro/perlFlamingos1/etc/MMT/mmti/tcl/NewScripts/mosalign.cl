
procedure mosalign ( imagename, boxfile )
string imagename
string boxfile
int boxsize        {15, prompt="Centroid box size"}
real pixsize       {0.171}
real camangle      {180}
int  parity        {-1}
real rotcenterx    {1024}
real rotcentery    {1024}
real moscenterx    {1154}
real moscentery    {-1764}

begin

string im
string starfile
string configfile
string resultsfile
string bfile

int count

real starx
real stary
real boxx, boxy
real xx,yy
real instaz, instel
real angle
real offsetx, offsety, offsetangle

im=imagename
bfile = boxfile
starfile = mktemp("tmp$mosalign")


imgets (im, "INSTAZ")
instaz = real(imgets.value)
imgets (im, "INSTEL")
instel = real(imgets.value)

angle = camangle / 180.0 * 3.1415926535
xx = ( instaz * cos(angle) + instel * sin(angle)) / pixsize / parity
yy = (-instaz * sin(angle) + instel * cos(angle)) / pixsize

# Count the lines in  the box file
list=bfile
count = 0
while (fscan(list, boxx, boxy) != EOF) {
    count = count+1
}

print (count)
print (count, >>starfile)

print ("Boxes are located at:")
type (bfile)
print ("Position cursor over stars in order and hit any key")

list=bfile
for (i=0; i< count; i+=1) {
    j=fscan(list, boxx, boxy) 
    j=fscan(imcur,x,y,k,s2)
    imcntr(im, x, y, cboxsize=boxsize) | scanf("%s %s %f %s %f", s1, s1, x, s1, y)
    print(boxx, boxy, x, y, >> starfile)
}
type(starfile)

configfile=mktemp("tmp$mosalign")
print("# Rotation center\n", (rotcenterx-xx), (rotcentery-yy), >>configfile);
print("# Moswheel center\n", moscenterx,    moscentery,    >>configfile);
type(configfile)


task $rotsolve=$foreign
resultsfile=mktemp("tmp$mosalign")
type(starfile) | rotsolve (osfn(configfile), > resultsfile)

list=resultsfile
i=fscan(list, s1, offsetangle)
i=fscan(list, s1, offsetx, offsety)

type (resultsfile)

offsetangle *=-1
offsetx *= parity
xx = ( offsetx * cos(angle) - offsety * sin(angle)) * pixsize 
yy = ( offsetx * sin(angle) + offsety * cos(angle)) * pixsize 
printf("\nChange rotator PA by %5.2f\n", offsetangle)
printf("relative.offset.mmt.pl %5.2f %5.2f\n",xx,yy)

delete(resultsfile)
delete(configfile)
delete(starfile)

end


#my $rcsId = q($Name:  $ $Id: mosalign.cl,v 0.1 2003/05/22 14:57:11 raines Exp $);