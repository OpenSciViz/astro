#!/usr/local/bin/perl -w

#
#Mos Dither patterns at KPNO 4m telescopes
#With Absolute offsets wrt the pointing center
#

my $rcsId = q($Name:  $ $Id);

use strict;

use Fitsheader qw/:all/;
use Getopt::Long;
use GetYN;
use ImgTests;
use kpnoTCSinfo qw/:all/;
use KPNO_offsets     qw/:all/;
use MCE4_acquisition qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop)                       = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout)                = RW_Header_Params::get_expt_params();

my ($m_rptpat,
    $m_ndgsz,
    $m_throw,
    $use_mnudg,
    $chip_pa_on_sky_for_rot0,
    $pa_rotator,
    $pa_slit_on_chip,
    $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my ( $A_dra,
     $A_ddec,
     $B_dra,
     $B_ddec ) = compute_centered_offsets( $pa_slit_on_sky,
					   $m_throw );

my ( $Ap_dra,
     $Ap_ddec,
     $Bp_dra,
     $Bp_ddec ) = compute_nudged_offsets(  $pa_slit_on_sky,
					   $m_throw,
					   +1*$m_ndgsz );
my ( $An_dra,
     $An_ddec,
     $Bn_dra,
     $Bn_ddec ) = compute_nudged_offsets( $pa_slit_on_sky,
					  $m_throw,
					  -1*$m_ndgsz );

print_info($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
	   $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
	   $An_dra, $An_ddec, $Bn_dra, $Bn_ddec,
	   $use_mnudg);

main::prompt_setup();

#Get rid of these
my ($nudged_A_dra, $nudged_A_ddec);
my ($nudged_B_dra, $nudged_B_ddec);

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );


my $this_A_dra = $A_dra; my $this_A_ddec = $A_ddec;
my $this_B_dra = $B_dra; my $this_B_ddec = $B_ddec;

my $this_A_dra_offset  = $this_A_dra  - $A_dra;
my $this_A_ddec_offset = $this_A_ddec - $A_ddec;

my $this_B_dra_offset  = $this_B_dra  - $A_dra;
my $this_B_ddec_offset = $this_B_ddec - $A_ddec;

my $nameA = "A_centered";
my $nameB = "B_centered";

main::move_to_beam( $header,            $nameA,
		    $this_A_dra,        $this_A_ddec,
		    $this_A_dra_offset, $this_A_ddec_offset );

my $nudge_sign = 0;
for( my $i = 1; $i <= $m_rptpat; $i++ ){
  if( !$use_mnudg ){
    #not nudged
    print "\n\n";
    print "NOT Nudged.\n\n";
    $next_index = do_pattern( $header,            $lut_name,
			      $file_hint,         $next_index,
			      $net_edt_timeout,   $use_tcs,
			      $m_throw,
			      $nameA,             $nameB,
			      $this_A_dra,        $this_A_ddec,
			      $this_B_dra,        $this_B_ddec,
			      $this_A_dra_offset, $this_A_ddec_offset,
			      $this_B_dra_offset, $this_B_ddec_offset );
  }elsif( $use_mnudg ){
    $nudge_sign = select_nudge_direction( $i, $m_rptpat );

    if( $nudge_sign == 0 ){
      if( $i > 1 ){
	$nameA = "A_centered";
	$nameB = "B_centered";

	$this_A_dra = $A_dra; $this_A_ddec = $A_ddec;
	$this_B_dra = $B_dra; $this_B_ddec = $B_ddec;
	
	$this_A_dra_offset  = $this_A_dra  - $A_dra;
	$this_A_ddec_offset = $this_A_ddec - $A_ddec;
	
	$this_B_dra_offset  = $this_B_dra  - $A_dra;
	$this_B_ddec_offset = $this_B_ddec - $A_ddec;

	main::move_to_beam( $header,            $nameA,
			    $this_A_dra,        $this_A_ddec,
			    $this_A_dra_offset, $this_A_ddec_offset );
      }
      print "Not nudged ABBA pattern\n";

      $next_index = do_pattern( $header,            $lut_name,
				$file_hint,         $next_index,
				$net_edt_timeout,   $use_tcs,
				$m_throw,
				$nameA,             $nameB,
				$this_A_dra,        $this_A_ddec,
				$this_B_dra,        $this_B_ddec,
				$this_A_dra_offset, $this_A_ddec_offset,
				$this_B_dra_offset, $this_B_ddec_offset );

    }elsif( $nudge_sign == 1 ){
      $nameA = "A_positive";
      $nameB = "B_positive";

      $this_A_dra = $Ap_dra; $this_A_ddec = $Ap_ddec;
      $this_B_dra = $Bp_dra; $this_B_ddec = $Bp_ddec;

      $this_A_dra_offset  = $this_A_dra  - $A_dra;
      $this_A_ddec_offset = $this_A_ddec - $A_ddec;

      $this_B_dra_offset  = $this_B_dra  - $A_dra;
      $this_B_ddec_offset = $this_B_ddec - $A_ddec;


      main::move_to_beam( $header,            $nameA,
			  $this_A_dra,        $this_A_ddec,
			  $this_A_dra_offset, $this_A_ddec_offset );

      print "Do positive nudged ABBA pattern\n";
      $next_index = do_pattern( $header,            $lut_name,
				$file_hint,         $next_index,
				$net_edt_timeout,   $use_tcs,
				$m_throw,
				$nameA,             $nameB,
				$this_A_dra,        $this_A_ddec,
				$this_B_dra,        $this_B_ddec,
				$this_A_dra_offset, $this_A_ddec_offset,
				$this_B_dra_offset, $this_B_ddec_offset );

    }elsif( $nudge_sign == -1 ){
      $nameA = "A_negative";
      $nameB = "B_negative";

      $this_A_dra = $An_dra; $this_A_ddec = $An_ddec;
      $this_B_dra = $Bn_dra; $this_B_ddec = $Bn_ddec;

      $this_A_dra_offset  = $this_A_dra  - $A_dra;
      $this_A_ddec_offset = $this_A_ddec - $A_ddec;

      $this_B_dra_offset  = $this_B_dra  - $A_dra;
      $this_B_ddec_offset = $this_B_ddec - $A_ddec;

      main::move_to_beam( $header,            $nameA,
			  $this_A_dra,        $this_A_ddec,
			  $this_A_dra_offset, $this_A_ddec_offset );

      print "Do negative nudged ABBA pattern\n";
      $next_index = do_pattern( $header,            $lut_name,
				$file_hint,         $next_index,
				$net_edt_timeout,   $use_tcs,
				$m_throw,
				$nameA,             $nameB,
				$this_A_dra,        $this_A_ddec,
				$this_B_dra,        $this_B_ddec,
				$this_A_dra_offset, $this_A_ddec_offset,
				$this_B_dra_offset, $this_B_ddec_offset );
    }
  }#end big if

}#End main for loop


#Recenter telescope
print "++++++++++++++++++++++++++++++++++++++++++++\n";
#print "SHOULD BE: offset to origin\n";
print "offset to origin\n";
#main::my_do_offset( 0, 0 );
KPNO_offsets::do_offset( 0, 0 );

#print "SHOULD BE: doing KPNO_offsets offset_wait\n";
KPNO_offsets::offset_wait();

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
####SUBS
sub base{
  my $index = $_[0];

  my $value = 4*$index - $index + 1;

  return $value;
}#Endsub base


sub my_clear_offsets{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;
  print "SHOULD BE: EXECUTING CLEAR OFFSET COMMAND:\n";
  #print "EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  #$reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub my_clear_offsets


sub my_do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  #print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  #$reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub compute_centered_offsets{
  my ( $pa_slit_on_sky, $m_throw ) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_centered_offsets


sub compute_nudged_offsets{
  my ($pa_slit_on_sky, $beam_sep, $nudge) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $An_dra  =      (0.5 * $beam_sep + $nudge) * sin( $pa_sky_rad );
  my $An_ddec =      (0.5 * $beam_sep + $nudge) * cos( $pa_sky_rad );

  my $Bn_dra  = -1 * (0.5 * $beam_sep - $nudge) * sin( $pa_sky_rad );
  my $Bn_ddec = -1 * (0.5 * $beam_sep - $nudge) * cos( $pa_sky_rad );

  $An_dra  = sprintf '%.2f', $An_dra;
  $An_ddec = sprintf '%.2f', $An_ddec;

  $Bn_dra  = sprintf '%.2f', $Bn_dra;
  $Bn_ddec = sprintf '%.2f', $Bn_ddec;

  return( $An_dra, $An_ddec, $Bn_dra, $Bn_ddec );
}#Endsub compute_nudged_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub do_pattern{
  my ($header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw,
      $nameA, $nameB,
      $this_A_dra,        $this_A_ddec,
      $this_B_dra,        $this_B_ddec,
      $this_A_dra_offset, $this_A_ddec_offset,
      $this_B_dra_offset, $this_B_ddec_offset ) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";
  print "Take 1 Spectrum in Beam $nameA\n";
  print "PRESENT OFFSETS wrt beam A: XOFFSET = $this_A_dra_offset\n";
  print "                            YOFFSET = $this_A_ddec_offset\n";

  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_to_beam( $header,            $nameB,
		      $this_B_dra,        $this_B_ddec,
		      $this_B_dra_offset, $this_B_ddec_offset );


  print "PRESENT OFFSETS wrt beam A: XOFFSET = $this_B_dra_offset\n";
  print "                            YOFFSET = $this_B_ddec_offset\n";

  print "Take 2 Spectra in beam $nameB\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_to_beam( $header,            $nameA,
		      $this_A_dra,        $this_A_ddec,
		      $this_A_dra_offset, $this_A_ddec_offset );

  print "PRESENT OFFSETS wrt beam A: XOFFSET = $this_A_dra_offset\n";
  print "                            YOFFSET = $this_A_ddec_offset\n";

  print "Take 1 Spectrum in Beam $nameA\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  return $next_index;
}#Endsub do_pattern


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub move_to_beam{
  my ($header,          $beam_name,
      $this_dra,        $this_ddec,
      $this_dra_offset, $this_ddec_offset) = @_;

  print "\n";
  print "--------------------------\n";
  print "Moving to beam $beam_name.\n";
  main::my_do_offset( $this_dra, $this_ddec );
  #KPNO_offsets::do_offset( $this_dra, $this_ddec );

  #print "Should be waiting (KPNO_offsets  offset_wait)\n";
  KPNO_offsets::offset_wait();

  RW_Header_Params::write_rel_offset_to_header( $header,
						$this_dra_offset,
						$this_ddec_offset);
}#Endsub move_to_beam


sub print_info{
  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "----------------PRESENT PARAMETERS FOR MOS DITHER----------------\n";
  print "-------(Check that the Rotator PA and Slit PA are correct)-------\n";
  print "\n";

  print_offsets($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
		$Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
		$An_dra, $An_ddec, $Bn_dra, $Bn_ddec,
		$use_mnudg);

  print "\n";
  print "Number of times through pattern------------------------ $m_rptpat\n";
  print "Distance (arcsec) between A & B beams------------------ $m_throw\n";
  print "Distance (arcsec) to shift ABBA on repeats of pattern-- $m_ndgsz\n";
  print "\n";
  print "Rotator Position Angle (ROT_PA, deg)------------------- $pa_rotator\n";
  print "Chip PA on sky for this Rotator angle (deg)------------ ".
         "$chip_pa_on_sky_this_rotator_pa\n";
  print "\n";
  print "NET Position Angle of Slit on Sky(deg)----------------- $pa_slit_on_sky\n";

  if( $use_mnudg ){
    print "Will nudge ABBA pattern on pattern repeats.\n";
    print "Nudged pattern order---------------------------- center, positive, negative\n";
  }else{
    print "Will _NOT_ nugde pattern on repeats from center.\n";
  }

  print "-----------------------------------------------------------------\n";

}#Endsub print_info


sub print_offsets{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
      $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec,
      $use_mnudg) = @_;

  if( $use_mnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_A";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_B";
    write STDOUT;
    $~ = $old_stdout_format;

  }elsif( !$use_mnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS_NOT_NUDGED";
    write STDOUT;
    $~ = "OFFSETS_NOT_NUDGED";
    write STDOUT;
    $~ = $old_stdout_format;
  }
}#Endsub print_offsets


sub prompt_setup{
  print "\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern: \n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type CLEAR to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $clear_home = <>;chomp $clear_home;
  my $valid = 0;
  until( $valid ){
    if( $clear_home =~ m/clear/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/home/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either CLEAR or HOME ";
      $clear_home = <>; chomp $clear_home;
    }
  }

  if( $clear_home =~ m/clear/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    print "SHOULD BE: doing KPNO_offsets clear_offsets\n";
    #print "\n\nRecentering the dither pattern to the present location\n";
    #KPNO_offsets::clear_offsets();
    main::my_clear_offsets();

  }elsif( $clear_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub select_nudge_direction{
  my ($tps, $rpt_pat) = @_;

  my $center = "0";
  my $pos    = "+1";
  my $neg    = "-1";
  my $set;

  my $index = 0;
  my $index_matched = "";
  my $base_matched = "";
  until( $index >= $rpt_pat ){
    my $base = base( $index );
    #print "index = $index; tps = $tps; base = $base\n";

    if( $tps == $base ){
      $set = $center;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+1 ){
      $set = $pos;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+2 ){
      $set = $neg;
      $index = $rpt_pat + 1;

    }else{
      $index++;
    }
  }

  return $set;
}#Endsub select_nudge_direction


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;

  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    if( Fitsheader::param_value( 'DISPDS92' ) ){
      #print "SHOULD BE doing ImgTests  display_image\n";
      ImgTests::display_image( $file_hint, $this_index );
    }else{
      print "The better way to display images is off; see config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


#### FORMATS
format OFFSET_HEADINGS = 
                         Telescope Offsets wrt slit center
               Centered              Positive              Negative
Beam        dRa       dDec        dRa       dDec        dRa       dDec
---------------------------------------------------------------------------------------
.
format OFFSET_HEADINGS_NOT_NUDGED = 
      Offsets Centered wrt slit center
              Telescope
Beam        dRa       dDec
--------------------------------------
.
format OFFSETS_NOT_NUDGED =
 A    @#####.##  @#####.## (arcsec)
$A_dra, $A_ddec
 B    @#####.##  @#####.## (arcsec)
$B_dra, $B_ddec
.
format TELE_OFFSETS_BEAM_A = 
 A    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$A_dra, $A_ddec, $Ap_dra, $Ap_ddec, $An_dra, $An_ddec
.
format TELE_OFFSETS_BEAM_B = 
 B    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$B_dra, $B_ddec, $Bp_dra, $Bp_ddec, $Bn_dra, $Bn_ddec
.
