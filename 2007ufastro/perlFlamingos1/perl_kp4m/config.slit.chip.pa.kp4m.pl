#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next mos dither sequence
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Fitsheader::Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
Fitsheader::readFitsHeader($header);

print "\n\n".
      ">>>---------------------The slit PA on the Chip is".
      "---------------------<<<\n\n";

Fitsheader::printFitsHeader( "SLIT-CPA");
print "\n\n";

print_msg();
print "Change value of SLIT-CPA ";
my $reply = GetYN::get_yn();
if( !$reply ){ die "Exiting\n\n" }

my $didmod = 0;
my $redo = 0;
until( $redo ){
  my $slit_pa_on_chip = chg_slit_pa_on_chip( "SLIT-CPA" );
  if( $slit_pa_on_chip != 720 ){
    Fitsheader::setNumericParam( "SLIT-CPA", $slit_pa_on_chip  );
    $didmod += 1;
  }


  if( $didmod > 0 ){
    print "\n\nThe new value is: \n";
    Fitsheader::printFitsHeader( "SLIT-CPA");
    print "_____________________________________________________________".
      "__________________\n";
    print "\nAccept change?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){
    Fitsheader::writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    Fitsheader::writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub print_msg{
  print "This will compute the PA of the slit on the chip, ";
  print "and update the SLIT-CPA header field.\n";
  print "It assumes that you have measured the XY cordinates at each end of the slit\n\n";

}#Endsub print_msg


sub chg_slit_pa_on_chip{
  my $pa_slit_on_chip = 720;
  my $is_valid = 0;

  my $iparm = param_index( 'SLIT-CPA' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm]."\n\n";

  print "Enter the XY coordinates of the slit at the\n".
        "top of the array (larger Y values)\n";
  print "X-coordinate:  ";
  my $xtop = input_pos_real();

  print "Y-coordinate:  ";
  my $ytop = input_pos_real();

  print "\n";
  print "Enter the XY coordinates of the stlit at the\n".
         "bottom of the array (smaller Y values)\n";
  print "X-coordinate:  ";
  my $xbot = input_pos_real();

  print "Y-coordinate:  ";
  my $ybot = input_pos_real();

  if( $ybot == $ytop ){
    die "\n\nExiting\n".
        "The slits must be oriented along the Y-axis of the chip..\n\n";
  }

  print "\n";
  print "xy top = $xtop, $ytop; xy bot = $xbot, $ybot\n";

  my $dx = $xtop - $xbot; my $dy = $ytop - $ybot;
  use Math::Trig;
  my $slit_angle_on_chip_rad = atan( $dx / $dy );
  my $slit_angle_on_chip_deg = rad2deg( $slit_angle_on_chip_rad );
  $slit_angle_on_chip_deg = sprintf "%.2f", $slit_angle_on_chip_deg;

  return $slit_angle_on_chip_deg;
}#Endsub chg_slit_pa_on_chip


sub input_pos_real{
  my $response = 0;
  my $is_valid = 0;
  until( $is_valid ){
    chomp( $response = <> );

    if( $response =~ m/[a-zA-Z]/ ){
      print "Enter a positive real number:  ";
    }elsif( $response =~ m/^\.$/ ){
      print "Enter a positive real number:  ";
    }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response > 0 ){
	    #print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a value > 0\n";}
	}
      }
  }
  return $response;
}#Endsub input_pos_real


__END__

=head1 NAME

config.slit.chip.pa.kp4m.pl

=head1 Description

Tell the script what the angle of the slit
is wrt the chip.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.slit.chip.pa.kp4m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
