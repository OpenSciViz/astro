#!/usr/local/bin/perl -w

## Execute dither patterns at MMTO.
##
## MODIFIED script to use relative offsets (instreloffsets),
## instead of absolute offsets (instoffsets).
##
## Force to start at position 0 in pattern.
##
## The off source position, as given by THROW and THROW_PA
## must be given in terms of instrument coordinates.
##

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Dither_patterns qw/:all/;
use Fitsheader qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use IdleLutStatus;
use ImgTests;
use MCE4_acquisition qw/:all/;
use mmtTCSinfo qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS qw/:all/;

$|=1;#use for output piping with \r

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);

Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($dpattern, $startpos, $net_scale_factor, $d_rptpos,
    $d_rptpat, $usenudge, $nudgesiz) = RW_Header_Params::get_dither_params();

my $throw = Fitsheader::param_value( "THROW" );
my $throw_pa = Fitsheader::param_value( "THROW_PA" );

print "INPUT PARAMS\n";
print "Base pattern              = $dpattern\n";
print "Repeat each position      = $d_rptpos times\n";
print "Repeat pattern            = $d_rptpat times\n";
print "Nudge pattern on repeates = $usenudge (0=no, 1=yes)\n";
print "Size of nudge             = $nudgesiz (asec)\n";
print "\n";

print "++++++++++++++++++++++\n";
print "Forcing start pos to 0\n";
print "++++++++++++++++++++++\n\n";
$startpos = 0;

#### Load dither patterns
my @pat_abs_x        = Dither_patterns::load_pattern_x( $dpattern );
my @pat_abs_y        = Dither_patterns::load_pattern_y( $dpattern );
my @pat_rel_pt1_x    = Dither_patterns::load_pat_rel_Xorigin( $dpattern );
my @pat_rel_pt1_y    = Dither_patterns::load_pat_rel_Yorigin( $dpattern );
my @pat_rel_origin_x = Dither_patterns::load_pattern_rel_x( $dpattern );
my @pat_rel_origin_y = Dither_patterns::load_pattern_rel_y( $dpattern );

@pat_abs_x        = Dither_patterns::scale_pattern_x_or_y( \@pat_abs_x, $net_scale_factor );
@pat_abs_y        = Dither_patterns::scale_pattern_x_or_y( \@pat_abs_y, $net_scale_factor );
@pat_rel_pt1_x    = Dither_patterns::scale_pattern_x_or_y( \@pat_rel_pt1_x, $net_scale_factor );
@pat_rel_pt1_y    = Dither_patterns::scale_pattern_x_or_y( \@pat_rel_pt1_y, $net_scale_factor );
@pat_rel_origin_x = Dither_patterns::scale_pattern_x_or_y( \@pat_rel_origin_x, $net_scale_factor );
@pat_rel_origin_y = Dither_patterns::scale_pattern_x_or_y( \@pat_rel_origin_y, $net_scale_factor );

print "Absolute offsets wrt pointing center for base pattern\n";
print "___________________________________________\n";
for( my $i = 0; $i < (@pat_abs_x - 1); $i++){
  print  "$i:  ";
  printf '% 10.3f', $pat_abs_x[$i];print ", ";
  printf '% 10.3f', $pat_abs_y[$i];print ", ";
  printf "\n";
}
print "\n\n";

print "Relative offsets of base pattern from position n to position n+1:\n";
print "___________________________________________\n";
for( my $i = 0; $i < (@pat_rel_origin_x); $i++){
  print  "$i:  ";
  printf '% 10.3f', $pat_rel_origin_x[$i];print ", ";
  printf '% 10.3f', $pat_rel_origin_y[$i];print ", ";
  printf "\n";
}
print "\n\n";

print "------------SKY BEAM LOCATION---------------\n";
print "Note the sky is relative to instrument x, y\n";
print "The sky is NOT input in RA, DEC\n\n";
print "\n";
print "Location of sky beam, relative to instrument x, y\n";
print "throw            = $throw\n";
print "throw_pa         = $throw_pa\n";
my ($beamAtoB_throw_x, $beamAtoB_throw_y) = compute_throw_pos_wrt_origin( $throw, $throw_pa );
print "beamAtoB_throw_x = $beamAtoB_throw_x\n";
print "beamAtoB_throw_y = $beamAtoB_throw_y\n";
print "-------------------------------------------\n";

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#### Make socket connection & then get starting offsets
my $soc = UseTCS::make_socket_connection( $telescop, 1 );

#### Begin taking data
#### Setup mce4 only once
#print "SHOULD BE: ";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $last_pos_num  = @pat_rel_origin_x - 1;#num moves is @pat_x-1, then -1 to start count at 0
my $bool_counter = 0;
my $sign = 1;
my $next_index = $this_index;
my $X;
my $Y;
my $net_offset_x;
my $net_offset_y;
my $x_nudge = 0;
my $y_nudge = 0;

for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do abba pattern over and over
  print "\n";
  print "---------------------------------------------------\n";
  print ">>>>     Start of pattern number $rpt_pat     <<<<<\n";
  if( $rpt_pat > 0 ){
    print "\n";
    print "Move from last position in pattern to origin:\n";
    ($x_nudge, $y_nudge) = Dither_patterns::nudge_pointing($d_rptpat, $rpt_pat, $usenudge, $nudgesiz);
    print "nudge is $x_nudge, $y_nudge\n";

    $X = $pat_rel_origin_x[$last_pos_num] + $x_nudge;
    $Y = $pat_rel_origin_y[$last_pos_num] + $y_nudge;
    print "Move by: x,y = $X, $Y, in ";
    if(    $sign eq  1 ){ print "BEAM A\n";}
    elsif( $sign eq -1 ){ print "BEAM B\n";}

    mmtTCSinfo::instrel_offset( $soc, $X, $Y );
    mmtTCSinfo::offset_wait();
    print "\n";
  }

  for( my $i = $startpos; $i < $last_pos_num; $i+=1){
    print "i = $i\n";
    print "\tMove to position i = $i of base pattern\n";
    $X = $pat_rel_origin_x[$i];
    $Y = $pat_rel_origin_y[$i];
    print "\tMove by: x,y = $X, $Y\n";
    mmtTCSinfo::instrel_offset( $soc, $X, $Y );
    mmtTCSinfo::offset_wait();

    compute_and_write_net_offset( $sign, $pat_rel_pt1_x[$i], $pat_rel_pt1_y[$i],
				  $beamAtoB_throw_x, $beamAtoB_throw_y, $header,
				  $x_nudge, $y_nudge);

    print "\tTake an image at position $i, in ";
    if(    $sign eq  1 ){ print "BEAM A\n";}
    elsif( $sign eq -1 ){ print "BEAM B\n";}

    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
				 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    $sign = beam_switch( $sign, $beamAtoB_throw_x, $beamAtoB_throw_y );

    compute_and_write_net_offset( $sign, $pat_rel_pt1_x[$i], $pat_rel_pt1_y[$i],
				  $beamAtoB_throw_x, $beamAtoB_throw_y, $header,
				  $x_nudge, $y_nudge);


    print "\tTake an image at position $i, in ";
    if(    $sign eq  1 ){ print "BEAM A\n";}
    elsif( $sign eq -1 ){ print "BEAM B\n";}

    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
				 $file_hint, $next_index, $net_edt_timeout, $use_tcs, $sign);
  }#End loop over base pattern
}#End of big for loop

print "\n";
print "Move from last position in pattern to origin:\n";
$X = $pat_rel_origin_x[$last_pos_num];
$Y = $pat_rel_origin_y[$last_pos_num];
print "Move by: x,y = $X, $Y\n";
mmtTCSinfo::instrel_offset( $soc, $X, $Y );
mmtTCSinfo::offset_wait();
print "\n";
print "NOTE: the present pointing center may have been shifted\n";
print "      wrt initial pointing center by nudges\n";
print "\n";

print "\n\n";
print "SHOULD BE CLOSING SOCKET\n";
close( $soc );
print "\n\n***************************************************\n";
print     "*****          DITHER SCRIPT DONE            ******\n";
print     "***************************************************\n";
#####SUBS
sub beam_switch{
  my ( $direction, $x, $y ) = @_;

  my $this_throw_x = $sign * $x;
  my $this_throw_y = $sign * $y;

  print "\n";
  if( $sign eq 1 ){
    print "\tMove A -> B, ";
  }elsif( $sign eq -1 ){
    print "\tMove B -> A, ";
  }
  print "reloffset by $this_throw_x, $this_throw_y\n";
  mmtTCSinfo::instrel_ofset( $soc, $this_throw_x, $this_throw_y );
  mmtTCSinfo::offset_wait();

  if(    $sign eq  1 ){ $sign = -1 }
  elsif( $sign eq -1 ){ $sign = +1 }

  return $sign;
}#Endsub beam_switch


sub compute_and_write_net_offset{
  my ($sign, $rel_x, $rel_y, $throw_x, $throw_y, $header, $foo_x, $foo_y) = @_;
  my $net_x = 0;
  my $net_y = 0;

  if( $sign eq 1 ){
    $net_x = $rel_x + $foo_x;
    $net_y = $rel_y + $foo_y;
  }elsif( $sign eq -1 ){
    $net_x = $rel_x + $throw_x + $foo_x;
    $net_y = $rel_y + $throw_y + $foo_y;
  }

  print "\tSHOULD BE writing offsets $net_x, $net_y to header\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $net_x, $net_y );

}#Endsub compute_net_offset


sub compute_throw_pos_wrt_origin{
  my ($throw, $thow_pa) = @_;
  use Math::Trig;

  my $pa_rad = deg2rad( $throw_pa );
  my $throw_x = $throw * sin( $pa_rad );
  my $throw_y = $throw * cos( $pa_rad );

  $throw_x = sprintf "%.2f", $throw_x;
  $throw_y = sprintf "%.2f", $throw_y;

  return( $throw_x, $throw_y );
}#Endsub compute_nod_pos_wrt_origin


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs) = @_;

  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "\ttake image with index= $this_index\n";
    #print "SHOULD BE: ";
    print "GETTING TCSINFO\n";
    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    #print "SHOULD BE ";
    print "Calling write filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECUTING: \n$acq_cmd\n\n";
    print "acq cmd is \n$acq_cmd\n\n";
    acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    IdleLutStatus::idle_lut_status_slashr( $expt, $nreads );

    #print "SHOULD BE making unlock file, padding image, and displaying image\n";

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }
    print "\n";
    if( Fitsheader::param_value( 'PAD_DATA' ) ){
      ImgTests::pad_data( $file_hint, $this_index );
    }

    if( Fitsheader::param_value( 'DISPDS92' ) ){
      ImgTests::display_image( $file_hint, $this_index );
    }else{
      print "The better way to display images is off; see engineering.config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image

__END__

=head1 NAME

dither.object.sky.mmt.pl

=head1 Description

Executed the on source off source imaging dither pattern.


=head1 REVISION & LOCKER

$Name:  $

$Id: dither.object.sky.mmt.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
