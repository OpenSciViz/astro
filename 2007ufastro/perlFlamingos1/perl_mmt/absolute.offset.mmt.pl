#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use mmtTCSinfo qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
  print_usage();

}elsif( $cnt == 1){
  my $arg = shift;
  unless( $arg =~ m/now/i ){print_usage()};

  #Get present values
  my $get_instazoff = "1 get instazoff";
  my $get_insteloff = "1 get insteloff";

  my $sock = mmtTCSinfo::tcsops_connect();
  my $instazoff = mmtTCSinfo::tcsops( $get_instazoff, $sock, "not_quiet" );
  my $insteloff = mmtTCSinfo::tcsops( $get_insteloff, $sock, "not_quiet" );
  close( $sock );

  $instazoff = mmtTCSinfo::parse_tcs_return( $instazoff );
  $insteloff = mmtTCSinfo::parse_tcs_return( $insteloff );

  print "\n";
  print "The present offsets:  (d_x, d_y) = ($instazoff, $insteloff)\n";
  print "\n";

}elsif( $cnt == 2 ){
  my $d_x = shift;
  my $d_y = shift;

  if( $d_x =~ m/[a-zA-Z]/ or $d_y =~ m/[a-zA-Z]/ ){
    print "\n\nDid you enter a non-numerical offset by mistake?".
          "\n\nd_x = $d_x; d_y = $d_y\n\n";
    print_usage();

  }else{
    my $sock      = mmtTCSinfo::tcsops_connect();
    my $ops_reply = mmtTCSinfo::instoff_offset( $sock, $d_x, $d_y );
    close( $sock );

    mmtTCSinfo::check_tcs_returned_ack( $ops_reply );

  }
}else{
  print_usage();
}
###################
sub print_usage{
  die "\n\tUSAGE:".
      "\n\tOffset the telescope by an absolute offset wrt its catalog position.".
      "\n\tThe motion will be in RA/DEC, rotated by the present rotator position,".
      "\n\twhich effectively moves the telescope in detector coordinates, x and y.".
      "\n".
      "\n\tTo get present offsets type: absolute.offset.mmt.pl now".
      "\n\tTo   apply new offsets type: absolute.offset.mmt.pl d_x_arcsec d_y_arcsec".
      "\n".
      "\n\te.g.".
      "\n\tabsolut.offset.mmt.pl 5 10 will set the offsets +5\" in X, -10\" in Y".
      "\n".
      "\n\tThe offsets wrt the catalog center,".
      "\n\twill be 5\" W, 10\" N if Position angle = 0".
      "\n\twill be 5\" E, 10\" S if Position angle = -180.".
      "\n\n";
}#Endsub print_usage


__END__

=head1 NAME

absolute.offset.mmt.pl


=head1 Description

Make an offset in instrument x, y coordinate space
which is absolute wrt the target base position.

=head1 REVISION & LOCKER

$Name:  $

$Id: absolute.offset.mmt.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
