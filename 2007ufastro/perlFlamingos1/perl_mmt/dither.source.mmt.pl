#!/usr/local/bin/perl -w

## Execute dither patterns at MMTO.
## This script uses absolute offsets wrt the pointing center.
##
## NOTE: This script has to add in the initial offset, because
## the MMT has no way for the tcs to accumulate offsets into
## the catalog position (doing so at KPNO is called 'zero'ing 
## the offsets.

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Dither_patterns qw/:all/;
use Fitsheader qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use IdleLutStatus;
use ImgTests;
use MCE4_acquisition qw/:all/;
use mmtTCSinfo qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS qw/:all/;

$|=1;#use for output piping with \r

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);

Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($dpattern, $startpos, $net_scale_factor, $d_rptpos,
    $d_rptpat, $usenudge, $nudgesiz) = RW_Header_Params::get_dither_params();

#### Load dither patterns
my @pat_x        = Dither_patterns::load_pattern_x( $dpattern );
my @pat_y        = Dither_patterns::load_pattern_y( $dpattern );
my @rel_offset_x = Dither_patterns::load_pat_rel_x_startpos( $startpos, $dpattern );
my @rel_offset_y = Dither_patterns::load_pat_rel_y_startpos( $startpos, $dpattern );

@pat_x = Dither_patterns::scale_pattern_x_or_y( \@pat_x, $net_scale_factor );
@pat_y = Dither_patterns::scale_pattern_x_or_y( \@pat_y, $net_scale_factor );

@rel_offset_x = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_x, $net_scale_factor );
@rel_offset_y = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_y, $net_scale_factor );

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#### Make socket connection & then get starting offsets
my $soc = UseTCS::make_socket_connection( $telescop, $VIIDO );
my ( $X_present_offset, $Y_present_offset ) = get_present_offsets( $soc);
my ( $X_start_offset, $Y_start_offset )     = main::choose_initial_offsets();

add_initial_offset( \@pat_x, $X_start_offset, \@pat_y, $Y_start_offset );
print_setup();
GetYN::query_ready();

#### Begin taking data
#### Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $x_nudge = 0;
my $y_nudge = 0;
my $next_index = $this_index;

my $X;
my $Y;
my $X_origin;
my $Y_origin;
my $last_pos_num  = @pat_x - 2;#num moves is @pat_x-1, then -1 to start count at 0
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ($x_nudge, $y_nudge) = Dither_patterns::nudge_pointing($d_rptpat, $rpt_pat, $usenudge, $nudgesiz);
  $X_origin = $rel_offset_x[$startpos] + $x_nudge;
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position

    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset wrt origin by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
      mmtTCSinfo::instoff_offset( $soc, $X, $Y );
      mmtTCSinfo::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset wrt origin by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
	  print "***"; mmtTCSinfo::offset_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    $X = $pat_x[0] + $x_nudge;
    $Y = $pat_y[0] + $y_nudge;
    if( $VIIDO ){ 
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset wrt origin by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
      mmtTCSinfo::instoff_offset( $soc, $X, $Y );
      mmtTCSinfo::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
	  print "offset wrt origin by $X, $Y, back to position $startpos\n";
	  print "***"; mmtTCSinfo::offset_wait();
	}
  }

  RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

  #print "SHOULD BE: Should image $d_rptpos times here\n\n";
  print "IMAGE $d_rptpos times here\n\n";
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  my $this_pos;
  for( my $i = $startpos; $i < $last_pos_num; $i+=1){
    #-------------first position in object beam---------
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]+$x_nudge; $Y = $pat_y[$this_pos]+$y_nudge;

    $X_origin = $rel_offset_x[$this_pos] + $x_nudge;
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;

    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset wrt origin by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
      mmtTCSinfo::instoff_offset( $soc, $X, $Y );
      mmtTCSinfo::offset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "++++++++++++++++++++++++++++++++++++++++++++\n";
	   print "***offset wrt origin by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
	   print "***"; mmtTCSinfo::offset_wait();
	 }

    RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    #print "SHOULD BE: Should image $d_rptpos times here\n\n";
    print "IMAGE $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
    		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){
    print "++++++++++++++++++++++++++++++++++++++++++++\n";
    print "offset to original starting location, $X_start_offset, $Y_start_offset\n";
    mmtTCSinfo::instoff_offset( $soc, $X_start_offset, $Y_start_offset );
    mmtTCSinfo::offset_wait();
  }else{ print "***Should be doing the following:\n";
	 print "offset to original starting location, $X_start_offset, $Y_start_offset\n";
	 print "***"; mmtTCSinfo::offset_wait();
       }

close( $soc );
print "\n\n***************************************************\n";
print     "*****          DITHER SCRIPT DONE            ******\n";
print     "***************************************************\n";
####SUBS
sub choose_initial_offsets{

  print "\n\n";
  print ">------------------------------------------------------------------------------<\n";
  print "> This script will make absolute offsets of the telescope, with respect to\n";
  print "> the sum of the catalog position and the initial offsets.  Upon completion,\n";
  print "> the script will return the telescope to the initial offsets.\n";
  print ">\n";
  print "> NOTE: if the previous dither script was interupted, the present offsets may\n";
  print "> not be the ones you want.\n";

  my ($old_x, $old_y, $last_offsets_file) = get_last_offsets();

  print ">\n";
  print "> The last used offsets and the present value of the offsets are:\n";
  print "> OLD OFFSETS:  $old_x, $old_y\n";
  print "> NEW OFFSETS:  $X_present_offset, $Y_present_offset\n\n";

  my $x_choice;
  my $y_choice;

  my $choice; my $is_valid = 1;
  open FH, ">$last_offsets_file" or die "Not able to write to $last_offsets_file\n";

  while( $is_valid ){
    print "\n";
    print "Type OLD or NEW to pick which offsets to use, or type Q to quit.\n";
    print "DO NOT TYPE-C to exit script at this point, type Q instead.  ";
    $choice = <>;
    chomp $choice;

    if( $choice =~ m/old/i ){
      $x_choice = $old_x;
      $y_choice = $old_y;
      $is_valid = 0;

    }elsif( $choice =~ m/q/i ){
      print FH $old_x."\n";
      print FH $old_y."\n";
      close FH;
      die "\n\nExiting.\n\n";

    }elsif( $choice =~ m/new/i ){
      $x_choice = $X_present_offset;
      $y_choice = $Y_present_offset;
      $is_valid = 0;
    }
  }

  print "\n";
  print "You chose the $choice offsets: $x_choice, $y_choice\n";
  print FH $x_choice."\n";
  print FH $y_choice."\n";
  close FH;

  return( $x_choice, $y_choice );
}#Endsub choose_initial_offsets


sub get_last_offsets{

  my $last_offsets_file;
  if( $DEBUG ){
    $last_offsets_file = $ENV{FITS_HEADER_LUT_DEBUG}."mmt_last_start_offsets.txt";
  }else{
    $last_offsets_file = $ENV{FITS_HEADER_LUT}."mmt_last_start_offsets.txt";
  }
  open FH, $last_offsets_file or die "$last_offsets_file not found";

  my @input;
  while( defined (my $line = <FH>) ){
    chomp $line; 
    #print "input line is $line\n";
    push @input, $line;
  }

  #foreach my $item (@input){
  #  print "$item\n";
  #}

  close FH;

  my $old_x = shift @input;
  my $old_y = shift @input;

  return ($old_x, $old_y, $last_offsets_file);
}#Endsub get_last_offsets


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;

  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    #print "SHOULD BE GETTING TCSINFO\n";
    print "GETTING TCSINFO\n";
    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    #print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    print "Calling write filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = MCE4_acquisition::acquisition_cmd( $header, $lut_name, 
						     $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECUTING:  acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    IdleLutStatus::idle_lut_status_slashr( $expt, $nreads );

    #move_file_for_index_sep( $file_hint, $next_index );#NOT used
    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }
    print "\n";
    if( Fitsheader::param_value( 'PAD_DATA' ) ){
      ImgTests::pad_data( $file_hint, $this_index );
    }

    if( Fitsheader::param_value( 'DISPDS92' ) ){
      ImgTests::display_image( $file_hint, $this_index );
    }else{
      print "The better way to display images is off; see engineering.config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub get_present_offsets{
  my $sock = $_[0];

  my $get_instazoff = "1 get instazoff";
  my $get_insteloff = "1 get insteloff";

  my $instazoff = mmtTCSinfo::tcsops( $get_instazoff, $sock, "not_quiet" );
  my $insteloff = mmtTCSinfo::tcsops( $get_insteloff, $sock, "not_quiet" );

  $instazoff = mmtTCSinfo::parse_tcs_return( $instazoff );
  $insteloff = mmtTCSinfo::parse_tcs_return( $insteloff );

  return ( $instazoff, $insteloff );
}#Endsub get_start_offsets


sub add_initial_offset{
  my ($ar_pat_x, $X_start_offset, $ar_pat_y, $Y_start_offset) = @_;

  for( my $i=0; $i < (@pat_x - 1); $i++ ){
    my $cast = $$ar_pat_x[$i] + $X_start_offset;
    $cast = sprintf "%0.2f", $cast;
    $$ar_pat_x[$i] = $cast;

    $cast = $$ar_pat_y[$i] + $Y_start_offset;
    $cast = sprintf "%0.2f", $cast;
    $$ar_pat_y[$i] = $cast;
  }
}#Endsub add_initial_offset


sub print_setup{
  my $num_moves = @pat_x - 1;
  my $last_pos_num = $num_moves - 1;

  print "\n";
  print "----------------------------------------------\n\n";
  print "The default dither pattern is $dpattern.\n\n";
  Dither_patterns::scale_pattern_msg();
  print "The default pattern of absolute offsets wrt the catalog center,:\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xrel       Yrel     X_wrt_Im1    Y_wrt_Im1\n";
  my @base_pat_x = Dither_patterns::load_pattern_x( $dpattern );
  my @base_pat_y = Dither_patterns::load_pattern_y( $dpattern );

  @base_pat_x = Dither_patterns::scale_pattern_x_or_y( \@base_pat_x, $net_scale_factor );
  @base_pat_y = Dither_patterns::scale_pattern_x_or_y( \@base_pat_y, $net_scale_factor );

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $base_pat_x[$i];print ", ";
    printf '% 10.3f', $base_pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "The pattern center has the following initial offsets wrt the catalog center:\n";
  print "X_initial = $X_start_offset, Y_initial =$Y_start_offset\n\n";
  print "The absolute offsets that will be applied,\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xabs       Yabs     X_wrt_Im1    Y_wrt_Im1\n";

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.2f', $pat_x[$i];print ", ";
    printf '% 10.2f', $pat_y[$i];print ", ";
    printf '% 10.2f', $rel_offset_x[$i];print ", ";
    printf '% 10.2f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "Number of images per position--------------  $d_rptpos\n";
  print "Number of times through pattern------------  $d_rptpat\n";
  print "Start position, first time through pattern-  $startpos\n";
  print "num moves = $num_moves, lastpos= $last_pos_num\n";
  print "----------------------------------------------\n\n";

}#Endsub print_setup

__END__

=head1 NAME

dither.source.mmt.pl

=head1 Description

Execute the on source imaging dither pattern.


=head1 REVISION & LOCKER

$Name:  $

$Id: dither.source.mmt.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
