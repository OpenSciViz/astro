#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: nods9.dither.mos.mmt.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Fitsheader       qw/:all/;
use Getopt::Long;
use GetYN            qw/:all/;
use Gdr_Rpc          qw/:all/;
use Guided_Dither    qw/:all/;
use ImgTests         qw/:all/;
use MCE4_acquisition qw/:all/;
use Math::Round      qw/round/;
use mmtTCSinfo       qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS           qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop)         = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout)  = RW_Header_Params::get_expt_params();

my ($m_rptpat,
    $m_ndgsz,
    $m_throw,
    $usemnudg,
    $chip_pa_for_pa0,
    $pa_slit_on_chip) = RW_Header_Params::get_MMT_mos_dither_params();


#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";


my ($d_X, $d_Y, $recenter) = main::choose_initial_offsets();

print "Repeat pattern           = $m_rptpat times.\n";
print "Distance between beams   = $m_throw\n";
print "Nudge pattern on repeats = $m_ndgsz arcsec\n";
GetYN::query_ready();

if( $recenter ){
  main::move_to_init_offsets( $d_X, $d_Y );
}

main::move_center_to_A();

my $nudge_sign = 0;
for( my $i = 1; $i <= $m_rptpat; $i++ ){
  if( !$usemnudg ){
    #not nudged
    print "\n\n";
    do_pattern( $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw );

  }elsif( $usemnudg ){
    $nudge_sign = select_nudge_direction( $i, $m_rptpat );

    if( $nudge_sign == 0 ){
      if( $i > 1 ){
	#nudge up from previous negative beam
	my $net_nudge = $nudge_sign * $m_ndgsz;
	print "\n\n";
	print "Nudge pattern by $net_nudge.\n";
	main::nudge_beam_A( $net_nudge);
      }
      print "not nudged ABBA pattern\n";
      do_pattern( $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw );

    }elsif( $nudge_sign == 1 ){
      #shift beam A by positive nudge
      my $net_nudge = $nudge_sign * $m_ndgsz;
      print "\n\n";
      print "Nudge pattern by $net_nudge.\n";
      main::nudge_beam_A( $net_nudge);

      print "Do positive nudged ABBA pattern\n";
      do_pattern( $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw );

    }elsif( $nudge_sign == -1 ){
      #shift beam A by negative nudge
      my $net_nudge = 2 * $nudge_sign * $m_ndgsz;
      print "\n\n";
      print "Nudge pattern by $net_nudge.\n";
      main::nudge_beam_A( $net_nudge );
      print "Do negative nudged ABBA pattern\n";
      do_pattern( $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw );
    }
  }
}

print "nudge_sign = $nudge_sign\n";
main::move_A_to_center( $m_throw, $nudge_sign, $m_ndgsz );

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";
### SUBS
sub base{
  my $index = $_[0];

  my $value = 4*$index - $index + 1;

  return $value;
}#Endsub base


sub choose_initial_offsets{

  print "\n\n";
  print ">------------------------------------------------------------------------------<\n";
  print "> This script will make relative offsets of the telescope, with respect to\n";
  print "> present position given by the sum of the catalog position and the McOffsets.\n";
  print "> Upon completion the script will return the telescope to the initial offsets.\n";
  print ">\n";
  print "> NOTE:\n";
  print "> If the previous dither script was interupted, the present offsets may not be\n";
  print "> the ones you want.  Choose offsets such that the alignment stars are visible\n";
  print "> in their boxes.\n";
  print "> \n";

  my ($old_x, $old_y, $last_offsets_file) = get_last_mos_offsets();

  my $tcs_socket = mmtTCSinfo::tcsops_connect();
  my ($X_present_offset, $Y_present_offset) = get_present_offsets( $tcs_socket );

  print ">\n";
  print "> The last used offsets and the present value of the offsets are:\n";
  print "> OLD OFFSETS:  $old_x, $old_y\n";
  print "> NEW OFFSETS:  $X_present_offset, $Y_present_offset\n\n";

  my $x_choice;
  my $y_choice;

  open FH, ">$last_offsets_file" or die "Not able to write to $last_offsets_file\n";
  my $choice; my $is_valid = 1;

  while( $is_valid ){
    print "\n";
    print "Type OLD or NEW to pick which offsets to use, or type Q to quit.\n";
    print "DO NOT TYPE-C to exit script at this point, type Q instead.  ";
    $choice = <>;
    chomp $choice;

    if( $choice =~ m/old/i ){
      $x_choice = $old_x;
      $y_choice = $old_y;
      $is_valid = 0;

    }elsif( $choice =~ m/q/i ){
      print FH $old_x."\n";
      print FH $old_y."\n";
      close FH;
      die "\n\nExiting.\n\n";

    }elsif( $choice =~ m/new/i ){
      $x_choice = $X_present_offset;
      $y_choice = $Y_present_offset;
      $is_valid = 0;
    }
  }

  print "\n";
  print "You chose the $choice offsets: $x_choice, $y_choice\n";

  print FH $x_choice."\n";
  print FH $y_choice."\n";
  close FH;

  my $d_X = 0;
  my $d_Y = 0;
  my $recenter = 0;
  if( $choice =~ m/old/i ){
    $recenter = 1;
    $d_X = $X_present_offset - $old_x;
    $d_Y = $Y_present_offset - $old_y;

    if( $d_X < 0 ){ $d_X = -1*$d_X }
    if( $d_Y < 0 ){ $d_Y = -1*$d_Y }
  }

  return( $d_X, $d_Y, $recenter);
}#Endsub choose_initial_offsets


sub get_last_mos_offsets{

  my $last_offsets_file;
  if( $DEBUG ){
    $last_offsets_file = $ENV{FITS_HEADER_LUT_DEBUG}."mmt_last_mos_start_offsets.txt";
  }else{
    $last_offsets_file = $ENV{FITS_HEADER_LUT}."mmt_last_mos_start_offsets.txt";
  }
  open FH, $last_offsets_file or die "$last_offsets_file not found";

  my @input;
  while( defined (my $line = <FH>) ){
    chomp $line; 
    #print "input line is $line\n";
    push @input, $line;
  }

  #foreach my $item (@input){
  #  print "$item\n";
  #}

  close FH;

  my $old_x = shift @input;
  my $old_y = shift @input;

  return ($old_x, $old_y, $last_offsets_file);
}#Endsub get_last_mos_offsets


sub do_pattern{
  my ($header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";

  #print "SHOULD BE: Take 1 Spectrum in Beam A\n";
  print "Take 1 Spectrum in Beam $nameA\n";

  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE moving to beam B\n";
  main::move_from_beam_A_to_beam_B( $m_throw );

  #print "SHOULD BE: Take 2 Spectra in beam B\n";
  print "Take 2 Spectra in beam B\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE: moving to beam A\n";
  main::move_from_beam_B_to_beam_A( $m_throw );

  #print "SHOULD BE: Take 1 Spectrum in Beam A\n";
  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #$next_index += 4;
  return( $next_index );
}#Endsub do_pattern


sub get_present_offsets{
  my $sock = $_[0];

  my $get_instazoff = "1 get instazoff";
  my $get_insteloff = "1 get insteloff";

  #not_quiet or quiet
  my $instazoff = mmtTCSinfo::tcsops( $get_instazoff, $sock, "quiet" );
  my $insteloff = mmtTCSinfo::tcsops( $get_insteloff, $sock, "quiet" );

  $instazoff = mmtTCSinfo::parse_tcs_return( $instazoff );
  $insteloff = mmtTCSinfo::parse_tcs_return( $insteloff );

  #print "Present offsets for pointing centered between beams:\n";
  #print "instazoff = $instazoff\n";
  #print "insteloff = $insteloff\n";
  #print "\n";

  return ( $instazoff, $insteloff );
}#Endsub get_start_offsets


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub move_A_to_center{
  my( $m_throw, $nudge_sign, $m_ndgsz ) = @_;

  my $reverse_nudge = -1*$nudge_sign * $m_ndgsz;
  my $net_offset_to_A = -1*$m_throw/2 + $reverse_nudge;

  print "\n";
  print "***************************************\n";
  print "Final ABBA pattern finished.\n";
  #print "SHOULD BE moving to back to slit center\n";
  #print "SHOULD BE executing 1 ditherinst 0 ".$net_offset_to_A."\n";
  #print "SHOULD BE waiting for telescope.\n";

  #print "\n";
  print "Moving back to slit center\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, 0, $net_offset_to_A );
  mmtTCSinfo::offset_wait();
}#Endsub move_A_to_center


sub move_center_to_A{
  #print "SHOULD BE moving to beam A\n";
  #print "SHOULD BE executing 1 ditherinst 0 ".($m_throw/2)."\n";
  #print "SHOULD BE waiting for telescope.\n";

  print "Move to beam A\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, 0, ($m_throw/2) );
  mmtTCSinfo::offset_wait();
}#Endsub move_center_to_A


sub move_from_beam_B_to_beam_A{
  my ($m_throw) = @_;

  #print "SHOULD BE moving from beam B to beam A\n";
  #print "SHOULD BE executing 1 ditherinst 0 ".$m_throw."\n";
  #print "SHOULD BE waiting for telescope.\n";

  print "Move to beam A\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, 0, $m_throw );
  mmtTCSinfo::offset_wait();
}#Endsub move_to_beam_A


sub move_from_beam_A_to_beam_B{
  my $m_throw = $_[0];

  #print "SHOULD BE moving from beam A to beam B\n";
  #print "SHOULD BE executing 1 ditherinst 0 ".(-1*$m_throw)."\n";
  #print "SHOULD BE waiting for telescope.\n";

  print "Move to beam A\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, 0, (-1*$m_throw) );
  mmtTCSinfo::offset_wait();
}#Endsub move_to_beam_B


sub move_to_init_offsets{
  my ( $d_X, $d_Y) = @_;

  #print "SHOULD BE moving to \n";
  #print "SHOULD BE executing 1 ditherinst ".$d_X." ".$d_Y."\n";
  #print "SHOULD BE waiting for telescope.\n";

  print "Move to beam A\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, $d_X, $d_Y) );
  mmtTCSinfo::offset_wait();
}#Endsub move_to_slit_center


sub nudge_beam_A{
  my $net_nudge = $_[0];

  #print "SHOULD BE moving beam A by $net_nudge\n";
  #print "SHOULD BE executing 1 ditherinst 0 ".$net_nudge."\n";
  #print "SHOULD BE waiting for telescope.\n";

  print "Move to beam A\n";
  my $guider_socket = Guided_Dither::guider_connect();
  my $reply = Guided_Dither::guided_instrel_offset( $guider_socket, 0, $net_nudge );
  mmtTCSinfo::offset_wait();
}#Endsub nudge_beam_A


sub select_nudge_direction{
  my ($tps, $rpt_pat) = @_;

  my $center = "0";
  my $pos    = "+1";
  my $neg    = "-1";
  my $set;

  my $index = 0;
  my $index_matched = "";
  my $base_matched = "";
  until( $index >= $rpt_pat ){
    my $base = base( $index );
    #print "index = $index; tps = $tps; base = $base\n";

    if( $tps == $base ){
      $set = $center;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+1 ){
      $set = $pos;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+2 ){
      $set = $neg;
      $index = $rpt_pat + 1;

    }else{
      $index++;
    }
  }

  return $set;
}#Endsub select_nudge_direction


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    #print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image
