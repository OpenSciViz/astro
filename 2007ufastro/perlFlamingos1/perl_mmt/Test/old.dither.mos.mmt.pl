#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: old.dither.mos.mmt.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Fitsheader       qw/:all/;
use Getopt::Long;
use GetYN            qw/:all/;
use Gdr_Rpc          qw/:all/;
use Guided_Dither    qw/:all/;
use ImgTests         qw/:all/;
use MCE4_acquisition qw/:all/;
use Math::Round      qw/round/;
use mmtTCSinfo       qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS           qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop)         = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout)  = RW_Header_Params::get_expt_params();

my ($m_rptpat,
    $m_ndgsz,
    $m_throw,
    $usemnudg,
    $chip_pa_for_pa0,
    $pa_slit_on_chip) = RW_Header_Params::get_MMT_mos_dither_params();

#my ($pa,
#    $parangle,
#    $rotangle)        = main::get_pa();

my $pa = 0;
my $parangle = 0;
my $rotangle = 0;

my $chip_pa_this_pa   = $pa + $chip_pa_for_pa0;
my $pa_slit_on_sky    = main::compute_pa_slit_on_sky( $chip_pa_this_pa, $pa_slit_on_chip );

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec ) = compute_centered_offsets( $pa_slit_on_sky, $m_throw );
my ($Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec) = compute_nudged_offsets( $pa_slit_on_sky, $m_throw, $m_ndgsz);

my ($An_dra, $An_ddec, $Bn_dra, $Bn_ddec) = compute_nudged_offsets( $pa_slit_on_sky, $m_throw, -1*$m_ndgsz);


main::print_info( $A_dra,  $A_ddec,  $B_dra,  $B_ddec,  $Ap_dra,    $Ap_ddec,  $Bp_dra,    $Bp_ddec,
		  $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $usemnudg );

main::prompt_beam();

my $rpt_pat = $m_rptpat;
my $start  = "start";
my $center = "center";
my $pos    = "pos";
my $neg    = "neg";
my $nameA  = "A_centered";
my $nameB  = "B_centered";

my $selected_set = "start";
my $previous_set = "start";

for( my $tps = 1; $tps <= $rpt_pat; $tps++ ){
  print "\n";
  print "For loop: tps=$tps\n";
  $previous_set = $selected_set;
  $selected_set = select_pattern_set( $tps, $rpt_pat );
  #print "prev set = $previous_set; selected set = $selected_set\n";

  if( $usemnudg eq 1 ){
    if(  ($selected_set eq $center) and
	 (($previous_set eq $start) or($previous_set eq $neg))
      ){
      $nameA = "A_centered";
      $nameB = "B_centered";
      if( $previous_set eq $start ){
	print "Do centered pattern\n";
	#print "Should already be in beam A\n";

	print "Should send to beam A.\n";
	

	#print "SHOULD BE doing centered pattern\n";
	$next_index = do_pattern( $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
		                  $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);

      }elsif( $previous_set eq $neg ){
	print "SHOULD BE: Move from neg to center\n";
	$nameA = "A_centered";
	$nameB = "B_centered";
	
	print "Do centered pattern\n";
	$next_index = do_pattern( $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
				  $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);
      }

    }elsif( ($selected_set eq $pos) and ($previous_set eq $center) ){
      $nameA = "A_positive";
      $nameB = "B_positive";

      print "SHOULD BE: doing positive pattern\n";
      $next_index = do_pattern( $A_dra, $A_ddec, $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec, $nameA, $nameB,
			        $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    }elsif( ($selected_set eq $neg) and ($previous_set eq $pos) ){
      $nameA = "A_negative";
      $nameB = "B_negative";

      print "SHOULD BE: doing positive pattern\n";
      $next_index = do_pattern( $A_dra, $A_ddec, $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $nameA, $nameB,
			        $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    }
  }elsif( $usemnudg eq 0 ){
    $nameA = "A_centered";
    $nameB = "B_centered";
    $next_index = do_pattern( $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
			      $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);
  }
}#End for loop

print "\n";
print "The telescope should be in beam $nameA.\n";
print "Should have completed $rpt_pat repeats of the ABBA pattern\n";
print "Do you want to reset the pointing so the alignment stars are in their boxes\n";
print "and the objects are in the centers of the slits? ";
my $reset = GetYN::get_yn();
if( !$reset ){
  die "\n\n".
      "*******************************************************\n".
      "*****            Dither script done.              *****\n".
      "*****     Don't forget you're in beam $nameA.     *****\n".
      "*******************************************************\n";
}else{
  move_to_slit_center();
}

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";
### SUBS
sub base{
  my $index = $_[0];

  my $value = 4*$index - $index + 1;

  return $value;
}#Endsub base


sub compute_centered_offsets{
  my ($pa_slit_on_sky, $m_throw) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_centered_offsets


sub compute_nudged_offsets{
  my ($pa_slit_on_sky, $m_throw, $nudge ) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );


  my $Ap_dra  =      (0.5 * $m_throw + $nudge) * sin( $pa_sky_rad );
  my $Ap_ddec =      (0.5 * $m_throw + $nudge) * cos( $pa_sky_rad );

  my $Bp_dra  = -1 * (0.5 * $m_throw - $nudge) * sin( $pa_sky_rad );
  my $Bp_ddec = -1 * (0.5 * $m_throw - $nudge) * cos( $pa_sky_rad );

  $Ap_dra  = sprintf '%.2f', $Ap_dra;
  $Ap_ddec = sprintf '%.2f', $Ap_ddec;

  $Bp_dra  = sprintf '%.2f', $Bp_dra;
  $Bp_ddec = sprintf '%.2f', $Bp_ddec;

  return( $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec );
}#Endsub compute_nudged_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $sock = "undefined";
  print "Should be connecting to guider\n";
  #my $sock = Guided_Dither::guider_connect();

  #testing
  my $space = " ";
  my $offset_cmd = "1 ditherinst".$space.$dX.$space.$dY;
  print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd, as called by guided_instrel_offset( $sock, $dX, $dY )\n";

  #print "EXECUTING OFFSET COMMAND:\n";

  #my $reply = Guided_Dither::guided_instrel_offset( $sock, $dX, $dY );
}#Endsub do_offset


sub do_pattern{
  my ($A_dra, $A_ddec, $thisA_dra, $thisA_ddec, $thisB_dra, $thisB_ddec, $nameA, $nameB,
      $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";

  #print "SHOULD BE: Take 1 Spectrum in Beam $nameA\n";
  print "Take 1 Spectrum in Beam $nameA\n";

  #RW_Header_Params::write_rel_offset_to_header( $header,
  #						$thisA_dra  - $A_dra,
  #						$thisA_ddec - $A_ddec);

  #$next_index = take_an_image( $header, $lut_name, 1,
  #		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE moving to beam $nameB\n";
  main::move_to_beam_B( $thisB_dra, $thisB_ddec, $nameB );

  #RW_Header_Params::write_rel_offset_to_header( $header,
  #						$thisB_dra  - $A_dra,
  #						$thisB_ddec - $A_ddec);

  #print "SHOULD BE: Take 2 Spectra in beam $nameB\n";
  print "Take 2 Spectra in beam B\n";
  #$next_index = take_an_image( $header, $lut_name, 2,
  #		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE: moving to beam $nameA\n";
  main::move_to_beam_A( $thisA_dra, $thisA_ddec, $nameA );

  #RW_Header_Params::write_rel_offset_to_header( $header,
  #						$thisA_dra  - $A_dra,
  #						$thisA_ddec - $A_ddec);

  #print "SHOULD BE: Take 1 Spectrum in Beam $nameA\n";
  print "Take 1 Spectrum in Beam A\n";
  #$next_index = take_an_image( $header, $lut_name, 1,
  #		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  $next_index += 4;
  return( $next_index );
}#Endsub do_pattern


sub get_pa{
  my $bad = "3.14";

  #print "Should be getting PA\n";
  #testing
  #my ($pa, $parangle, $rotangle, $flag);
  #$flag = "success"; $pa = 0; $parangle = 0; $rotangle = 0;
  #print "test: $flag, $pa, $parangle, $rotangle\n";

  my ($pa, $parangle, $rotangle, $flag) = mmtTCSinfo::get_position_angle();
  $pa       = sprintf "%.2f", $pa;
  $parangle = sprintf "%.2f", $parangle;
  $rotangle = sprintf "%.2f", $rotangle;

  if( $flag eq "failure" ){
    print "Couldn't get tcs info.\n";
    print "Do something about it.\n";
    return ($bad, $bad, $bad);

  }elsif( $flag eq "success" ){
    return ($pa, $parangle, $rotangle);
  }
}#Endsub get_pa


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub move_to_beam_A{
  my ($thisA_dra, $thisA_ddec, $nameA) = @_;

  print "-------------------------------------\n";
  print "Move to Beam $nameA = defpos2\n";

  main::do_offset( $thisA_dra, $thisA_ddec );
  main::offset_wait();

}#Endsub move_to_beam_A


sub move_to_beam_B{
  my ($thisB_dra, $thisB_ddec, $nameB) = @_;

  print "-------------------------------------\n";
  print "Move to Beam $nameB = defpos3\n";

  main::do_offset( $thisB_dra, $thisB_ddec );
  main::offset_wait();

}#Endsub move_to_beam_B


sub move_to_slit_center{

  print "----------------------------------\n";
  print "Moving to slit center = defpos1\n";

  main::do_offset( 0, 0 );
  main::offset_wait();

}#Endsub move_to_slit_center


sub print_info{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,  $Ap_dra,    $Ap_ddec,  $Bp_dra,    $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $usemnudg ) = @_;

  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "--------------------PRESENT PARAMETERS FOR MOS DITHER----------------------------------\n";
  print "\n";
  print_offsets($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
		$Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
		$An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $usemnudg );

  print "\n";
  print "NET Position Angle of Slit on Sky(deg)---------- $pa_slit_on_sky\n";
  print "Distance between A & B beams-------------------- $m_throw  (arcsec)\n";
  print "Distance to shift ABBA on repeats of pattern---- $m_ndgsz  (guider pixels)\n";
  print "Number of times through pattern----------------- $m_rptpat\n";
  if( $usemnudg ){
    print "Will nudge ABBA pattern on pattern repeats.\n";
    print "Nudged pattern order---------------------------- center, positive, negative\n";
  }else{
    print "Will _NOT_ nugde pattern on repeats from center.\n";
  }
}#Endsub print_info


sub print_offsets{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
      $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $usemnudg ) = @_;

  if( $usemnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_A";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_B";
    write STDOUT;
    $~ = $old_stdout_format;

  }elsif( !$usemnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS_NOT_NUDGED";
    write STDOUT;
    $~ = "OFFSETS_NOT_NUDGED";
    write STDOUT;
    $~ = $old_stdout_format;
  }
}#Endsub print_offsets


sub prompt_beam{
  print "\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> The following conditions are required to start:\n";
  print "> 1) Already guiding in beam A, also called defpos1.\n";
  print "> 2) The present telescope offset matches the offset for beam A listed above.\n";
  print ">    (If they do not, have the operator zero the offsets.)\n";
  print "\n";

  GetYN::query_ready();
}#Endsub prompt_beam


sub select_pattern_set{
  my ($tps, $rpt_pat) = @_;

  my $center = "center";
  my $pos    = "pos";
  my $neg    = "neg";
  my $set;

  my $index = 0;
  my $index_matched = "";
  my $base_matched = "";
  until( $index >= $rpt_pat ){
    my $base = base( $index );
    #print "index = $index; tps = $tps; base = $base\n";

    if( $tps == $base ){
      $set = $center;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+1 ){
      $set = $pos;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+2 ){
      $set = $neg;
      $index = $rpt_pat + 1;

    }else{
      $index++;
    }
  }

  return $set;
}#Endsub select_pattern_set


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    #print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


#### FORMATS
format OFFSET_HEADINGS = 
                 Telescope and Guide Box Offsets wrt slit center
               Centered              Positive              Negative
Beam        dRa       dDec        dRa       dDec        dRa       dDec
---------------------------------------------------------------------------------------
.
format OFFSET_HEADINGS_NOT_NUDGED =
Offsets Centered wrt slit center
              Telescope
Beam        d_X       d_Y
---------------------------------
.
format OFFSETS_NOT_NUDGED =
 A    @#####.##  @#####.## (arcsec)
$A_dra, $A_ddec			
 B    @#####.##  @#####.## (arcsec)
$B_dra, $B_ddec
.
format TELE_OFFSETS_BEAM_A = 
 A    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$A_dra, $A_ddec, $Ap_dra, $Ap_ddec, $An_dra, $An_ddec
.
format TELE_OFFSETS_BEAM_B = 
 B    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$B_dra, $B_ddec, $Bp_dra, $Bp_ddec, $Bn_dra, $Bn_ddec
.
