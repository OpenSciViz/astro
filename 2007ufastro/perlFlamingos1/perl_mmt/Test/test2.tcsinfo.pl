#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: test2.tcsinfo.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

my $gdrrpc = $ENV{GDR_RPC};

use mmtTCSinfo qw/:all/;


####TCS CONNECT###
my $sock = mmtTCSinfo::tcsops_connect();
if( $sock eq $mmtTCSinfo::socket_undefined){
  print "Not getting TCS info\n";
}
###GET TCS INFO  
my $quiet = "quiet";

my $want;
#the tcsops lines had \$want, $sock, don't know why pass by ref
$want = "1 get epoch";     my $epoch     = tcsops( $want, $sock, $quiet );
$want = "1 get ra";        my $ra        = tcsops( $want, $sock, $quiet );
$want = "1 get dec";       my $dec       = tcsops( $want, $sock, $quiet );

#$want = "1 get rastr";     $ra        = tcsops( $want, $sock, $quiet );
#$want = "1 get decstr";    $dec       = tcsops( $want, $sock, $quiet );

$want = "1 get ut";        my $utime     = tcsops( $want, $sock, $quiet );
$want = "1 get lst";       my $stime     = tcsops( $want, $sock, $quiet );
$want = "1 get ha";        my $ha        = tcsops( $want, $sock, $quiet );
$want = "1 get airmass";   my $airmass   = tcsops( $want, $sock, $quiet );
$want = "1 get thetaz";    my $zen_dist  = tcsops( $want, $sock, $quiet );
$want = "1 get instazoff"; my $instazoff = tcsops( $want, $sock, $quiet );
$want = "1 get insteloff"; my $insteloff = tcsops( $want, $sock, $quiet );
$want = "1 get rot";       my $rot       = tcsops( $want, $sock, $quiet );
$want = "1 get focus";     my $telfocus  = tcsops( $want, $sock, $quiet );
$want = "1 get telname";   my $telname   = tcsops( $want, $sock, $quiet );
$want = "1 get pa     ";   my $parangle  = tcsops( $want, $sock, $quiet );
$want = "1 get mjd    ";   my $mjd       = tcsops( $want, $sock, $quiet );
$want = "1 get dateobs";   my $utcdate   = tcsops( $want, $sock, $quiet );

print "Raw returned data:\n";
print "epoch     = $epoch\n";
print "ra        = $ra\n";
print "dec       = $dec\n";
print "ut        = $utime\n";
print "lst       = $stime\n";
print "ha        = $ha\n";
print "airmass   = $airmass\n";
print "thetaz    = $zen_dist\n";
print "instazoff = $instazoff\n";
print "insteloff = $insteloff\n";
print "rot       = $rot\n";
print "focus     = $telfocus\n";
print "telname   = $telname\n";
print "parangle  = $parangle\n";
print "mjd       = $mjd\n";
print "dateobs   = $utcdate\n";


###Get rid of extraneous characters###
my @field; 
$epoch     = parse_tcs_return( $epoch     );
$ra        = parse_tcs_return( $ra        );
$dec       = parse_tcs_return( $dec       );
$utime     = parse_tcs_return( $utime     );
$stime     = parse_tcs_return( $stime     );
$ha        = parse_tcs_return( $ha        );
$airmass   = parse_tcs_return( $airmass   );
$zen_dist  = parse_tcs_return( $zen_dist  );
$instazoff = parse_tcs_return( $instazoff );
$insteloff = parse_tcs_return( $insteloff );
$rot       = parse_tcs_return( $rot       );
$telfocus  = parse_tcs_return( $telfocus  );
$telname   =~ s/^(\s*)1 ack //;  #This acutally screwed up header, so won't be written
$parangle  = parse_tcs_return( $parangle  );
$mjd       = parse_tcs_return( $mjd       );
$utcdate   = parse_tcs_return( $utcdate   );

###Trucate long number###
my $dec_loc;
$dec_loc = index($airmass, ".");
$airmass = substr $airmass, 0, ($dec_loc + 4);

$dec_loc = index($zen_dist, ".");
$zen_dist = substr $zen_dist, 0, ($dec_loc + 4);

$dec_loc = index($instazoff, ".");
$instazoff = substr $instazoff, 0, ($dec_loc + 4);

$dec_loc = index($insteloff, ".");
$insteloff = substr $insteloff, 0, ($dec_loc + 4);

$dec_loc = index($rot, ".");
$rot = substr $rot, 0, ($dec_loc + 4);

$dec_loc = index($telfocus, "."); 
$telfocus = substr $telfocus, 0, ($dec_loc + 3);

$dec_loc = index($parangle, "."); 
$parangle = substr $parangle, 0, ($dec_loc + 3);

my $pos_angle = $parangle - $rot;


print "Parsed returned data:\n";
print "epoch     = $epoch\n";
print "ra        = $ra\n";
print "dec       = $dec\n";
print "ut        = $utime\n";
print "lst       = $stime\n";
print "ha        = $ha\n";
print "airmass   = $airmass\n";
print "thetaz    = $zen_dist\n";
print "instazoff = $instazoff\n";
print "insteloff = $insteloff\n";
print "rot       = $rot\n";
print "focus     = $telfocus\n";
print "telname   = $telname\n";
print "parangle  = $parangle\n";
print "mjd       = $mjd\n";
print "dateobs   = $utcdate\n";
print "PA        = $pos_angle\n";
