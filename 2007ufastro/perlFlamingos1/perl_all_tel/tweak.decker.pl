#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use GetYN;
use AllbutMosWheel qw/:all/;

my $full_rev = $AllbutMosWheel::DECKER_REV;

my $steps_per_deg = $full_rev / 360;
my $backlash = $ENV{BACKLASH_0};
my $backlash_deg = 360 * $backlash / $full_rev;
   $backlash_deg = sprintf "%.2f", $backlash_deg; 

my $cnt = @ARGV;
if( $cnt < 1 ){ 
  #assume -help
  die "\n\tUSAGE:\n".
      "\n\ttweak.decker.pl (+|-)rotation_angle (degrees)".
      "\n\teg, tweak.decker.pl +0.5, or tweak.decker.pl -0.5\n".
      "\n\tNOTE: + rotates decker  cw wrt chip\n".
        "\t      - rotates decker ccw wrt chip\n".
	"\t        negative motions will overshoot by $backlash_deg degrees ($backlash motor units)\n".
	"\t        and then go positive by $backlash_deg degrees ($backlash motor units)\n".
	"\t        in order to remove the backlash.\n\n";
}
else{
  my $offset = shift;

  main::check_input( $offset );
  main::check_number_decimals( $offset );
  my ( $sign, $value ) = main::check_number_signs( $offset );

  my $rot = $value * $steps_per_deg;
  my $integer_offset   = main::round_input( $rot );

  my $offset_cmd = main::build_offset_cmd( $sign, $integer_offset );
  print "\n\tNOTE: + rotates decker ccw wrt chip\n".
        "\t      - rotates decker  cw wrt chip\n".
	"\t        negative motions will overshoot by $backlash_deg degrees ($backlash motor units)\n".
	"\t        and then go positive by $backlash_deg degrees ($backlash motor units)\n".
	"\t        in order to remove the backlash.\n\n";

  print "\n";

  if( $sign eq "-" ){
    print "\tThis will require two motions, in order to remove the backlash.\n";
  }else{
    print "\tThis will require one motion.\n";
  }

  print "\tAre you ready to move the wheel?  ";
  my $reply;
  until( $reply ){
    $reply = get_yn();
    if( !$reply ){ die "\nExiting\n\n";}
  }

  #Execute the motion
  #print "\n\nSHOULD BE EXECUTING\n$offset_cmd\n\n";
  print "\tMake sure you can watch activity in the motor demon xterm window.\n";
  print "\tMos wheel motion is done when motor demon activity stops.\n\n";
  print "\n\nEXECUTING\n$offset_cmd\n\n";
  system( $offset_cmd );

  if( $sign eq "-" ){
    my $tweak_mos_sleep_time = $ENV{TWEAK_MOS_SLEEP_TIME};
    my $cnt = 1;
    print "Waiting $tweak_mos_sleep_time seconds before removing backlash:\n";
    until( $cnt > $tweak_mos_sleep_time ){
      print "$cnt\n";
      sleep( 1 );
      $cnt++;
    }

    my $remove_backlash_cmd = main::build_offset_cmd( "+", $backlash );
    #print "\n\nSHOULD BE EXECUTING\n$remove_backlash_cmd\n\n";
    print "\tMake sure you can watch activity in the motor demon xterm window.\n";
    print "\tMos wheel motion is done when motor demon activity stops.\n\n";
    print "\n\EXECUTING\n$remove_backlash_cmd\n\n";
    system( $remove_backlash_cmd );
  }
}

#####SUBS
sub check_input{
  my $offset = $_[0];

  if( $offset =~ m/[a-zA-Z]/ ){
    die "\n\nPlease enter a numerical offset\n\n";
  }
}#Endsub check_input


sub check_number_decimals{
  my $offset = $_[0];

  my $first_decimal = index $offset, ".";

  if( $first_decimal >= 0 ){
    my $trunc = substr $offset, $first_decimal+1;

    my $second_decimal = index $trunc, ".";
    if( $second_decimal >= 0 ){
      die "\n\nPlease enter only one decimal point\n\n";
    }
  }
}#Endsub check_number_decimals


sub check_number_signs{
  my $input = $_[0];
  my $sign = "+";
  my $value = $input;

  my $first_plus = index $input, "+";
  my $first_neg  = index $input, "-";

  if( $first_plus > 0 ){die "\n\nEnter a leading plus sign\n\n"  }
  if( $first_neg  > 0 ){die "\n\nEnter a leading minus sign\n\n" }

  if( $first_plus == 0 ){
    my $trunc = substr $input, $first_plus+1;
    my $second_plus = index $trunc, "+";
    if( $second_plus >= 0 ){ die "\n\nEnter only one leading plus sign\n\n" }
    $sign = "+";
    $value = $trunc;
  }

  if( $first_neg == 0 ){
    my $trunc = substr $input, $first_neg+1;
    my $second_neg = index $trunc, "-";
    if( $second_neg >= 0 ){ die "\n\nEnter only one leading minus sign\n\n" }
    $sign = "-";
    $value = $trunc;
  }

  my $trunc_length = length( $input );
  if( $trunc_length == 1 ){
    if( $input =~ m/(\+|\-)/ ){ 
      die "\n\nPlease enter a value\n\n" ;
    }
  }
  
  return $sign, $value;
}#Endsub check_number_signs


sub round_input{
  my $input = $_[0];
  my $truncated_input = $input;

  #round input if there's a decimal
  my $dec_loc = index $input, ".";
  if( $dec_loc >= 0 ){
    $truncated_input = substr $input, 0, $dec_loc;
    my $rem = substr $input, $dec_loc+1, 1;

    #print "trunc = $truncated_input; rem = $rem\n";

    if( $rem ne "" ){
      my $trunc_len = length $truncated_input;
      if( $rem <= 5 ){
	if( $trunc_len == 0 ){
	  $truncated_input = 0;
	}else{
	  $truncated_input = $truncated_input + 0;
	}
      }elsif( $rem > 5 ){
	if( $trunc_len == 0 ){
	  $truncated_input = 1;
	}else{
	  $truncated_input = $truncated_input + 1;
	}
      }

    }
  }
  return $truncated_input;
}#Endsub round_input


sub build_offset_cmd{ 
  my ( $sign, $integer_offset ) = @_;

  if( $sign eq "-" ){
    $integer_offset = $backlash + $integer_offset;
  }

  my $i_v_cmd = $ENV{"I_0"}.$AllbutMosWheel::ampersand.$ENV{"V_0"};
  my $motion = $ENV{MOTOR_A}.$sign.$integer_offset;

  my $cmd = $AllbutMosWheel::cmd_head.$AllbutMosWheel::str_quote.
    $i_v_cmd.$AllbutMosWheel::ampersand.$motion.$AllbutMosWheel::str_quote;

  return $cmd;
}#Ensub build_offset_cmd


__END__

=head1 NAME

tweak.decker.pl

=head1 DESCRIPTION

Adjust the position of the Decker wheel.


=head1 REVISION & LOCKER

$Name:  $

$Id: tweak.decker.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
