#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

#my $update_time = 45; #seconds
my $update_time = $ENV{TEMP_LOG_INTERVAL}; #seconds

my $uflsc_cmd = $ENV{UFLSC}.$ENV{SPACE}.$ENV{CLIENT_QUIET}.$ENV{SPACE}."1";

my ($year, $hour, $min, $sec, $decimal_day);
my ($array_temp, $fanout_temp, $ch4_temp, $ch5_temp, $mos_temp);
my ($weekday, $month, $day);
my $cnt = 1;
$array_temp = -1.0; $fanout_temp = -2.0; $ch4_temp = -4.0; $ch5_temp = -5.0; $mos_temp = -6.0;

format TEMP_DATA =
@#####  @<<<  @####  @<<<  @##  @###.##### @###.##  @###.##  @###.##  @###.##  @###.##
$cnt, $weekday, $year, $month, $day, $decimal_day, $array_temp, $fanout_temp, $ch4_temp, $ch5_temp, $mos_temp
.
format TEMP_HEADINGS =
                                                               Worksurface
                                                              Ch. 4    Ch. 5
                                            Array    Fanout    Bottom   Top     Mos
   Num  Day    Year  Mon   Day  Decimal-Day  (K)       (K)      (K)     (K)     (K)
------------------------------------------------------------------------------------
.

my $file_name = get_next_log_file_name();
output_headings( $file_name );

my $do_forever = "";
until( $do_forever ){
  my $last_datum = `$uflsc_cmd`;
  chomp $last_datum;
  {
    local $/ = "\r"; chomp $last_datum;
  }
  chomp $last_datum;

  ($year, $hour, $min, $sec, $decimal_day )                   = format_day_num( $last_datum );
  ($array_temp, $fanout_temp, $ch4_temp, $ch5_temp, $mos_temp) = format_temps( $last_datum );
  ($weekday, $month, $day)                                    = get_weekday_month_day();
  output_data( $file_name );

  $update_time = $ENV{TEMP_LOG_INTERVAL}; #seconds
  sleep $update_time;
  $cnt++;
}


sub format_day_num{
  my $input = $_[0];
  my ($dt_stamp, $ch1, $ch2, $ch3) = split / /, $input;

  my $open_par = index $dt_stamp, "(";
  $dt_stamp = substr $dt_stamp, $open_par+1;
  my $close_par = index $dt_stamp, ")";
  $dt_stamp = substr $dt_stamp, 0, $close_par-1;

  my ($year, $day_num, $hour, $min, $sec) = split /:/, $dt_stamp;
  $sec = sprintf "%d", $sec;

  my $decimal_day = $day_num + ($hour + ($min + $sec/60)/60)/24;
  $decimal_day = sprintf "%.5f", $decimal_day;

  return ($year, $hour, $min, $sec, $decimal_day);
}#Endsub format_day_num


sub format_temps{
  my $input = $_[0];

  #print "input is $input\n";

  my $endpar = index $input, ")";
  my $data = substr $input, ($endpar+1);
  #print "data =:$data\n";

  my $ind1 = index $data, "1,";
  my $ind2 = index $data, "2,";
  my $ind4 = index $data, "4,";
  my $ind5 = index $data, "5,";
  my $ind6 = index $data, "6,";

  my $start1_val = $ind1+2;
  my $start2_val = $ind2+2;
  my $start4_val = $ind4+2;
  my $start5_val = $ind5+2;
  my $start6_val = $ind6+2;

  my $array_temp  = substr $data, $start1_val, ($ind2-$start1_val);
  my $fanout_temp = substr $data, $start2_val, ($ind4-$start2_val);
  my $ch4_temp    = substr $data, $start4_val, ($ind5-$start4_val);
  my $ch5_temp    = substr $data, $start5_val, ($ind6-$start5_val);
  my $mos_temp    = substr $data, $start6_val;

  #print "array  = $array_temp\n";
  #print "fanout = $fanout_temp\n";
  #print "ch4    = $ch4_temp\n";
  #print "ch5    = $ch5_temp\n";
  #print "mos    = $mos_temp\n";

  $array_temp  = catch_temp_err( $array_temp );
  $fanout_temp = catch_temp_err( $fanout_temp );
  $ch4_temp    = catch_temp_err( $ch4_temp );
  $ch5_temp    = catch_temp_err( $ch5_temp );
  $mos_temp    = catch_temp_err( $mos_temp );


  $array_temp  = sprintf "%.2f", $array_temp;
  $fanout_temp = sprintf "%.2f", $fanout_temp;
  $ch4_temp    = sprintf "%.2f", $ch4_temp;
  $ch5_temp    = sprintf "%.2f", $ch5_temp;
  $mos_temp    = sprintf "%.2f", $mos_temp;

  return ( $array_temp, $fanout_temp, $ch4_temp, $ch5_temp, $mos_temp );
}#Endsub format_temps


sub catch_temp_err{
  my $input = $_[0];

  #print "catch_temp_err input = $input\n";
  if( ($input eq "-----") or ($input eq "----- ") ){
    $input = -4.2;

  }elsif( $input =~ m/- /){
    $input = -4.2;

  }elsif( $input eq "" ){
    $input = -6.66;

  }elsif( $input =~ m/KKK\.K/ ){
    $input = -2.718;

  }elsif( $input =~ m/01\.\d+/ ){
    $input = -7.72;

  }elsif( $input =~ /OL/ ){
    $input = -2.16;
  }

  return $input;
}#Endsub catch_temp_err


sub get_weekday_month_day{
  my $weekday = `date '+%a'`;chomp $weekday;
  my $month   = `date '+%b'`;chomp $month;
  my $day     = `date '+%d'`;chomp $day;

  #print "$weekday, $month, $day\n";
  return ($weekday, $month, $day);
}#Endsub get_weekday_month_day


sub output_data{
  my $outfile = $_[0];

  open( TEMP_DATA, ">>$outfile") 
    || die "can't open Temp.dat\n\n";
  write TEMP_DATA;
  close TEMP_DATA;

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "TEMP_DATA";
  write STDOUT;
}#Endsub output_data


sub output_headings{
  my $outfile = $_[0];

  open( TEMP_HEADINGS, ">>$outfile") 
    || die "can't open Temp.dat\n\n";
  write TEMP_HEADINGS;
  close TEMP_HEADINGS;

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "TEMP_HEADINGS";
  write STDOUT;
  $~ = $old_stdout_format;
}#Endsub output_headings


sub get_next_log_file_name{
  use ImgTests;

  my $index_sep = ".";
  my $home_dir = $ENV{HOME}."/Temperature_Data/";
  my $file_base1 = "Temperature".$index_sep.$ENV{USER}.$index_sep;
  my $date_stamp = `date "+%Y.%b.%d"`;chomp $date_stamp;
  my $file_base = $file_base1.$date_stamp;

  my $file_ext  = ".dat";

  my $reply = ImgTests::does_dir_exist( $home_dir );

  my $last_index = 
    ImgTests::get_last_index( $home_dir, $file_base, $index_sep, $file_ext );

  my $next_index = $last_index + 1;
  my $index_str = ImgTests::parse_index( $next_index );

  my $file_name = $home_dir.$file_base.$index_sep.$index_str.$file_ext;
  print "\n\n";
  print ">>>  The new temperature data file will be\n$file_name\n";
  print ">>>  PLEASE DO NOT DELETE OLD TEMPERATURE DATA FILES!\n";
  print ">>>  PLEASE DO NOT KILL THE RECORD_TEMPS_COOLDOWN WINDOW!\n\n";

  return $file_name;
}#Endsub get_next_log_file_name


__END__

=head1 NAME

record.temperatures.pl

=head1 DESCRIPTION

Use to log the FLAMINGOS-1 temperatures.  Runs in foreground inside of
an xterm launched by initflam.pl


=head1 REVISION & LOCKER

$Name:  $

$Id: record.temperatures.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
