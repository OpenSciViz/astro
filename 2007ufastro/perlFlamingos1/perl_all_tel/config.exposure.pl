#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";

###############################################################################
#
#Configure fits header parameters for next exposure
#object, filebase, exp_time, and nreads
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use ImgTests;
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\n>>>         Present exposure parameters are:                    <<<\n";
print "_____________________________________________________________________\n";
printFitsHeader( "OBS_TYPE", "OBJECT" );
print "\n";
printFitsHeader( "FILEBASE", "ORIG_DIR" );
print "\n";
printFitsHeader( "DATE-OBS" );
print "\n";
printFitsHeader( "EXP_TIME",  "NREADS" );
print "\n";
#printFitsHeader( "USE_MST", "MSTBREAK" );
#print "\n";
printFitsHeader( "BIAS" );
print "\n";
printFitsHeader( "WEATHER1", "WEATHER2", "WIND" );


my $didmod = 0;
my $redo = 0;
my $change_bias = 0;

print "\n\nChange any parameter? ";
my $change = get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

until( $redo ){
  my $test =  modStringParams( "OBS_TYPE", "OBJECT");
  print "modstringparam returned $test\n";
  if( $test > 0 ){
    $didmod = 1 ;
  }

  $didmod += mod_FILEBASE();

  $test = mod_ORIG_DIR();
  if( $test ){
    $didmod += 1;
    my $dir_path = param_value( 'ORIG_DIR' );
    my $reply = does_dir_exist( $dir_path );
    print $reply;
  }

  $didmod += set_date();

  my $expt = set_expt();
  if( $expt > 0 ){
    setNumericParam("EXP_TIME", $expt);
    $didmod += 1;
  }

  my $nreads ;
  if( $expt > 0 ){
    $nreads = set_nreads($expt);
  }else{
    my $unchanged_expt = param_value('EXP_TIME');
    #print "Unchanged expt is $unchanged_expt\n";
    $nreads = set_nreads($unchanged_expt);
  }

  #print "set_nreads returned $nreads\n";
  if( $nreads > 0 ){
    setNumericParam("NREADS", $nreads);
    $didmod += 1;
  }

  set_CT();

  #my $chg_zero_or_one = chg_zero_or_one( "USE_MST" );
  #if( $chg_zero_or_one > -1 ){
  #  Fitsheader::setNumericParam( "USE_MST", $chg_zero_or_one );
  #  $didmod += 1;
  #}

  #my $chg_bigger_than_zero = chg_bigger_than_zero( "MSTBREAK" );
  #if( $chg_bigger_than_zero > -1 ){
  #  Fitsheader::setNumericParam( "MSTBREAK", $chg_bigger_than_zero );
  #  $didmod += 1;
  #}

  my $weather = set_weather();
  if( $weather > 0 ){
    setNumericParam("WEATHER1", $weather);
    $didmod += 1;
  }

  my $wind = set_wind();
  if( $wind > 0 ){
    setNumericParam("WIND", $wind);
    $didmod += 1;
  }

  if( $didmod > 0 ){
    print "\n\nThe new set of exposure parameters are:\n";
    printFitsHeader( "OBS_TYPE", "OBJECT" );
    print "\n";
    printFitsHeader( "FILEBASE", "ORIG_DIR" );
    print "\n";
    printFitsHeader( "DATE-OBS" );
    print "\n";
    printFitsHeader( "EXP_TIME",  "NREADS");
    print "\n";
    #printFitsHeader( "USE_MST", "MSTBREAK" );
    #print "\n";
    printFitsHeader( "BIAS" );
    print "\n";
    printFitsHeader( "WEATHER1", "WEATHER2", "WIND" );

    print "\nAccept changes (y/n)?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}


if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

    #my $dothis1 = "cp -p $fitsheader_file /data1/flamingos ";
    #my $dothis2 = "cp -p $fitsheader_file /data2/flamingos ";
    #system($dothis1);
    #print "Copying FITS header to /data1/flamingos \n";
    #system($dothis2);
    #print "Copying FITS header to /data2/flamingos \n";

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\n\nChange the detector bias ";
  $change_bias = get_yn();
  if( $change_bias ){
    Start_setbias($DEBUG);
    exit;
  }

  if( $change_bias == 0){
    print "\nNo changes made to default header.\n\n";
    exit;
  }else{
    print "\nOnly changed detector bias and header.\n\n";
    exit;
  }
}

print "\n\nChange the detector bias ";
$change_bias = get_yn();
if( $change_bias ){
  Start_setbias($DEBUG);
}

#Final print
print "\n";
###End of main

###subs
sub set_expt{
  my $iparm = param_index( 'EXP_TIME' );
  #print "exptime index is $iparm\n";
  print $fh_comments[$iparm] . "\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of seconds\n";
      }elsif( $new_value >= 2 ){
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter an exposure time >= 2 seconds\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }else{
      #print "Leaving EXP_TIME unchanged\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub set_expt


sub set_nreads{
  my $expt = $_[0];
  #print "Input expt is $expt\n";
  my $iparm = param_index( 'NREADS' );
  print $fh_comments[$iparm] . "\n";
  my $unchanged_nreads = $fh_pvalues[$iparm];
  #print "my unchanged nreads is $unchanged_nreads\n";

  my $response = 0;
  my $is_valid = 0;

  #print "expt = $expt\n";
  #print "unchanged nreads = $unchanged_nreads\n";

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    #print "input len is $input_len\n";
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of reads equal to 1 or >=3 and <= EXP_TIME/2\n";
      }elsif( $new_value <= $expt/2 ){
	if( $new_value ne 2 ){
	  #print ">expt is $expt; nreads is $new_value\n";
	  $response = $new_value;
	  $is_valid = 1;
	}elsif( $new_value eq 2 ){
	  print "NREADS = 2 is not valid\n";
	}
      }else{
	print "Please enter an integer number of reads <= EXP_TIME/2\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }elsif( $input_len == 0  ){
	#print "nreads was unchanged; check if allowed by expt\n";
	if( $unchanged_nreads > $expt/2 ){
	  print "Please enter an integer number of reads <= EXP_TIME/2\n";
	}else{
	  $is_valid = 1;
	}
    }else{
      #print "Leaving NREADS unchanged\n";
      $is_valid = 1;
    }
  }
  return $response;
}#Endsub set_nreads


sub set_CT{
  my $nreads = param_value('NREADS');
  #my $nreads = get_unchanged_param('NREADS');
  #print "nreads in set_ct is $nreads\n";
  if( $nreads == 1 ){
    setNumericParam("CYCLETYP", 40);
  }elsif( $nreads > 1 ){
    setNumericParam("CYCLETYP", 42);
  }else{
    print ">>>>> WARNING   WARNING <<<<<\n";
    print "Unable to set CYCLETYP in Fitsheader\n";
    print "nreads is not valid:  $nreads\n\n";
}

}#Endsub set_CT


sub set_date{
  my $iparm = param_index( 'DATE-OBS' );
  print $fh_comments[$iparm] . "\n";

  my $response = 0;
  my $is_valid = 0;

  until( $is_valid ){
    print $fh_params[$iparm]."= ".$fh_pvalues[$iparm]." ? ";
    my $new_value = <STDIN>;
    chomp $new_value;

    my $input_len = length $new_value;
    if( $input_len > 0 and $input_len <= 18 ){
      if( $new_value =~ m/\d{4}-\d{2}-\d{2}/ ){
	#print " $new_value\n";
	#$response = $new_value;
	setStringParam( "DATE-OBS", $new_value );
	$response = 1;
	$is_valid = 1;
      }else{
	print "Enter a numeric date in the format YYYY-MM-DD\n";
      }
    }elsif( $input_len >= 18 ){
      print "Enter a numeric date in the format YYYY-MM-DD\n";
    }else{
      #do nothing
      $is_valid = 1;
    }
    print "\n";
    return $response;
  }
}#Endsub set_date


sub Start_setbias{
  my $DEBUG = $_[0];

  my $executable = $ENV{UF_PERL_ALL_TEL_PATH}.$ENV{SET_BIAS_SCRIPT};
  #my $executable = $ENV{UF_PERL_ALL_TEL_PATH} . "setbias.pl";

  if( -e $executable ){
    print "Found $executable\n";
  }else{
    print "Cannot find $executable\n\nContinue?\n";
    my $response = get_yn();
    if($response == 0){
      die "Exiting\n";
    }
  }#end existence if

  if( $DEBUG ){
    $executable = $executable . " -debug";
  }
  system($executable);

}#Endsub Start_setbias



sub mod_FILEBASE{
    print "\n";
  print 
"__________________________________________________________________________\n";
  print "Enter template filename (Example 2001aug31 or sa57_a\n";
  print "Output filenames will be, e.g., 2001aug31".
         $ENV{DESIRED_INDEX_SEPARATOR}."####".$ENV{FITS_SUFFIX}."\n";
  print "The length must be less than 28 characters\n\n";

    
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  $iparm = param_index( 'FILEBASE' );
  
  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      
      $vlen = length( $new_pvalue );
      if( $vlen > 0 and $vlen <= 28 ){
	$fh_pvalues[$iparm] = '\'' . $new_pvalue . '\'' . (' ' x (28-$vlen));
	print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	print "\n";
	$is_valid = 1;
	$didmod = 1;
      }elsif( $vlen > 28 ){
	print "The path entered is > 28 characters long\n";
	print "Sorry, but you have to use a different filename\n";
      }elsif( $vlen <= 0 ){
	print "Not changing the current base filename\n";
	$didmod = 0;
	$is_valid = 1;
      }
    }else{
      print ">>>      WARNGING      WARNGING      <<<\n";
      print "Cannot find FILEBASE keyword in FITS header\n";
      print "NONE of the data taking scripts will work without this field\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $didmod;

} #Endsub mod_FILEBASE


sub set_weather{
  my $iparm = param_index( 'WEATHER1' );
  my $iparm2 = param_index( 'WEATHER2' );

  print "\n";
  print $fh_comments[$iparm]."\n";
  print $fh_comments[$iparm2]."\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter a weather condition flag between 1 and 6\n";
      }elsif( $new_value > 0 and $new_value < 7 ){
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter a weather condition flag between 1 and 6\n";
      }

    }elsif( $input_len >= 18 ){
      print "Please enter a weather condition flag between 1 and 6\n";
    }else{
      #print "Leaving EXP_TIME unchanged\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub set_weather


sub set_wind{
  my $iparm = param_index( 'WIND' );

  print "\n";
  print $fh_comments[$iparm]."\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter a wind condition flag between 1 and 5\n";
      }elsif( $new_value > 0 and $new_value < 6 ){
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter a wind condition flag between 1 and 5\n";
      }

    }elsif( $input_len >= 18 ){
      print "Please enter a wind condition flag between 1 and 5\n";
    }else{
      #print "Leaving EXP_TIME unchanged\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub set_weather


sub chg_zero_or_one{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter either 0 or 1:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please either 0 or 1:  ";
      }else{
	if( $response == 0 or $response == 1 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter either 0 or 1:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_zero_or_one


sub chg_bigger_than_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value > 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chng_bigger_than_zero


__END__

=head1 NAME

config.exposure.pl

=head1 DESCRIPTION

Configures the template FITS header for the next exposure.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.exposure.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
