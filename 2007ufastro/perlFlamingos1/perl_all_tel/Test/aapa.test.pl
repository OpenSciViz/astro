#!/usr/local/bin/perl -w

use strict;
use Math::Trig;

#my $throw = 1;
#my $pa =315;

my $cnt = @ARGV;
if( $cnt < 3 ){ die "aapa.test.pl throw instraa(deg) pa(deg)";}

my $throw   = shift;
my $instraa = shift;
my $pa      = shift;

print "\npie = ".pi."\n\n";

my $net_angle = $instraa + $pa;

print "net angle = $net_angle\n";

my $net_angle_rad = deg2rad($net_angle);
print  "net_anglerad = ";
printf "%.2f", $net_angle_rad;
print  "\n\n";

my $d_alpha = $throw * sin( $net_angle_rad );
my $d_dec   = $throw * cos( $net_angle_rad );

print  "d_alpha = ";
printf "%.2f", $d_alpha;
print  " d_dec = ";
printf "%.2f", $d_dec;
print  "\n\n";



#rcsId = q($Name:  $ $Id: aapa.test.pl 14 2008-06-11 01:49:45Z hon $);
