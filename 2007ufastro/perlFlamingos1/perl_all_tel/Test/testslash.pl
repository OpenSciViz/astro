#!/usr/local/bin/perl -w

use strict;

my $testdir = "net/data1/foo";

print "$testdir\n";

my $vlen = length $testdir;
print "length is $vlen\n\n";

my $lslash = index  $testdir, "/";
my $rslash = rindex $testdir, "/";

print "The  left-most slash is at $lslash\n";
print "the right-most slash at $rslash\n";
print "(starting from 0)\n\n";

if( $lslash != 0 ){
  print "test dir does not start with a slash\n";
  print "please enter an absolute path\n";
}

if( $rslash != $vlen -1 ){
  print "test dir does not end with a slash\n";
  print "please end with a slash\n";
}

#rcsId = q($Name:  $ $Id: testslash.pl 14 2008-06-11 01:49:45Z hon $);
