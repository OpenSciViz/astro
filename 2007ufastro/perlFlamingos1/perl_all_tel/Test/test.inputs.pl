#!/usr/local/bin/perl -w

use strict;

my $cnt = @ARGV;
if( $cnt < 1 ){
  die "\nPlease enter an integer.\n\n";
}

my $input = shift;

print "\n";

my $reply = check_alpha( $input );
if( !$reply ){ exit; }

#$reply = check_integer( $input );
#if( !$reply ){ exit; }

check_number_decimals( $input);
check_number_signs( $input );

print "Valid entry: $input\n\n";

####
sub check_alpha{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/[a-zA-Z]/ ){
    print "You entered $input.\n";
    print "Please enter an integer value.\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_alpha


sub check_integer{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/\.+/ ){
    print "Please enter an integer (no decimal point).\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_integer


sub check_number_decimals{
  my $offset = $_[0];

  my $first_decimal = index $offset, ".";

  my $reply = 1;
  if( $first_decimal >= 0 ){
    my $trunc = substr $offset, $first_decimal+1;

    my $second_decimal = index $trunc, ".";
    if( $second_decimal >= 0 ){
      print  "\n\nPlease enter only one decimal point\n\n";
      $reply = 0;
    }
  }
  return $reply;
}#Endsub check_number_decimals


sub check_number_signs{
  my $input = $_[0];
  my $sign = "+";
  my $value = $input;

  my $first_plus = index $input, "+";
  my $first_neg  = index $input, "-";

  my $reply = 1;
  if( $first_plus > 0 ){
    print "\n\nEnter a leading plus sign\n\n";
    $reply = 0;
  }
  if( $first_neg  > 0 ){
    print "\n\nEnter a leading minus sign\n\n";
    $reply = 0;
  }

  if( $first_plus == 0 ){
    my $trunc = substr $input, $first_plus+1;
    my $second_plus = index $trunc, "+";
    if( $second_plus >= 0 ){
      print "\n\nEnter only one leading plus sign\n\n";
      $reply = 0;
    }
    $sign = "+";
    $value = $trunc;
  }

  if( $first_neg == 0 ){
    my $trunc = substr $input, $first_neg+1;
    my $second_neg = index $trunc, "-";
    if( $second_neg >= 0 ){
      print "\n\nEnter only one leading minus sign\n\n";
      $reply = 0;
    }
    $sign = "-";
    $value = $trunc;
  }

  return $sign, $value, $reply;
}#Endsub check_number_signs

#rcsId = q($Name:  $ $Id: test.inputs.pl 14 2008-06-11 01:49:45Z hon $);
