#!/usr/local/bin/perl -w

use strict;
use Math::Trig;

#my $throw = 1;
#my $pa =315;

my $cnt = @ARGV;
if( $cnt < 2 ){ die "trig.test throw pa(deg)";}

my $throw = shift;
my $pa    = shift;

print "\npie = ".pi."\n\n";

my $pa_rad = deg2rad($pa);
print  "pa_rad = ";
printf "%.2f", $pa_rad;
print  "\n\n";

my $d_alpha = $throw * sin( $pa_rad );
my $d_dec   = $throw * cos( $pa_rad );

print  "d_alpha = ";
printf "%.2f", $d_alpha;
print  " d_dec = ";
printf "%.2f", $d_dec;
print  "\n\n";


print "test atan dx/dy = -1/5\n";
my $dx = 0;
my $dy = 5;

my $theta_rad = atan( $dx/$dy );
my $theta_deg = rad2deg( $theta_rad );

$theta_rad = sprintf "%.2f", $theta_rad;
$theta_deg = sprintf "%.2f", $theta_deg;

print "theta = $theta_rad rad\n";
print "theta = $theta_deg deg\n";

#rcsId = q($Name:  $ $Id: trig.test.pl 14 2008-06-11 01:49:45Z hon $);
