#!/usr/local/bin/perl -w

use strict;


my $rndm_num = rand;
my $rdx; my $rdy; 

if( 0 <= $rndm_num and $rndm_num < 0.25 ){
  $rdx = 0; $rdy = 1;
}elsif( 0.25 <= $rndm_num and $rndm_num < 0.5 ){
  $rdx = 1; $rdy = 0;
}elsif( 0.50 <= $rndm_num and $rndm_num < 0.75 ){
  $rdx = 0; $rdy = -1;
}elsif( 0.75 <= $rndm_num and $rndm_num <= 1.0 ){
  $rdx = -1; $rdy = 0;
}

print "rndm_num = $rndm_num ";
print "direction is $rdx, $rdy\n";

#rcsId = q($Name:  $ $Id: rand.test.pl 14 2008-06-11 01:49:45Z hon $);
