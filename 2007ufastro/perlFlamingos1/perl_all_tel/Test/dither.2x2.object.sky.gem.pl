#!/usr/local/bin/perl -w

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 0;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = param_value( 'EXP_TIME' );
my $nreads          = param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

#>>>Get dither info from header
#>>>Fix on 2x2 pattern for this script
#>>>Also insist that it starts at position 0
my $dpattern =  'pat_2x2           ';
my $startpos = 0;

#my $dpattern = param_value( 'DPATTERN' );#general pattern

my $d_scale  = param_value( 'D_SCALE'  );
my $d_rptpos = param_value( 'D_RPTPOS' );
my $d_rptpat = param_value( 'D_RPTPAT' );#$d_rptpat= 2;

my $usenudge = param_value( 'USENUDGE' );
my $nudgesiz = param_value( 'NUDGESIZ' );

#Load dither patterns
my $pat_name_len = length $dpattern;
print "pattern is $dpattern; length $pat_name_len\n";
my $ar_pat_x = load_pattern_x( $dpattern );
my $ar_pat_y = load_pattern_y( $dpattern );
my @pat_x = @$ar_pat_x;
my @pat_y = @$ar_pat_y;
my $num_moves = @pat_x - 1;
my $last_pos_num  = $num_moves - 1;
print "num moves = $num_moves, lastpos= $last_pos_num\n";

my $X; my $Y;

for( my $i = 0; $i < @pat_x; $i++){
  print "$i: $pat_x[$i], $pat_y[$i]\n";
}

#>>>Get Filename info from header
my $orig_dir  = param_value( 'ORIG_DIR' );
my $filebase  = param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = does_dir_exist( $orig_dir );
my $last_index = whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#Setup mce4 only once
print "Configuring the exposure time and cycle type on MCE4\n";
setup_mce4( $expt, $nreads );

if( $VIIDO ){
#Temporarily comment out real stuff just in case
print "Viido = $VIIDO; should be doing vii test connection";
print "Temporarily commented out\n\n";
##See if we can connect to the vii
#print "Connecting to the VII\n";
#my $soc = ufgem::viiconnect();
#if( !defined $soc || $soc eq "" ) {
#  print "?Bad socket connection to Gemini VII server\n";
#  #die "?Bad socket connection to Gemini VII server\n";
#}
}else{
  print "Viido = $VIIDO; should be doing vii test connection\n\n";
}

prompt_setup();
my $probe_name = query_probe_name();

if( $VIIDO ){
print "Viido = $VIIDO; should be setting up nod\n\n";
print "Temporarily commented out\n\n";
#setup_nod( $soc );
}else{
  print "Viido = $VIIDO; should be setting up nod\n\n";
}

#See if should get tcs info
my $use_tcs = param_value( 'USE_TCS' );
my $telescop = param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n";

my $next_index = $this_index;
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";

  ###First move is handset move from origin to first position
  #Force beam A
  if( $VIIDO ){ #nodbeam( $soc, "A" );
	      }else{ print "Should be forcing nod to beam A\n"; }

  $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
  if( $VIIDO ){ 
    print "Probe $probe_name follow on\n";
    #probe_follow_on( $soc, $probe_name );
    print "guide off\n";
    #guideOff( $soc );
    print "offset to position $startpos: $X, $Y, in beam=A\n";
    #setOffset_radec( $soc, $X, $Y );
    #offset_wait();
    print "guide on\n";
    #guideOn( $soc );
    #guide_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***Probe $probe_name follow on\n";
	 print "***guide off\n";
	 print "***offset to position $startpos: $X, $Y, in beam=A\n";
	 print "***"; offset_wait();
	 print "***guide on\n";
	 print "***"; guide_wait();
       }

  print "Should image $d_rptpos times here\n\n";
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  my $this_pos;
  for( my $i = $startpos; $i <= $last_pos_num; $i+=2 ){
    #------------------Nod to sky beam---------------
    #-------------first position in sky beam---------
    if( $VIIDO ){ 
      print "mountguide off\n";
      #mountGuideOff( $soc );
      print "guide off\n";
      #guideOff( $soc );
      print "Probe $probe_name follow off\n";
      #probe_follow_off( $soc, $probe_name );
      print "Nodding to position $i, beam=B\n";
      #nodbeam( $soc, "B" );
      #nod_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***mountguide off\n";
	   print "***guide off\n";
	   print "***Probe $probe_name follow off\n";
	   print "***Nodding to position $i, beam=B\n";
	   print "***"; nod_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #-------------second position in sky beam---------    
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];
    if( $VIIDO ){ 
      print "handset to position $this_pos: $X, $Y, beam=B\n";
      #setHandset_radec( $soc, $X, $Y );
      #handset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***handset to position $this_pos: $X, $Y, beam=B\n";
	   print "***"; handset_wait();
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #------------------Nod to object beam---------------
    #-------------first position in object beam---------
    if( $VIIDO ){
      print "Nodding to position $this_pos, beam=A\n";
      #nodbeam( $soc, "A" );
      print "Probe $probe_name follow on\n";
      #probe_follow_on( $soc, $probe_name );
      probe_wait();
      print "guide on\n";
      #guideOn( $soc );
      guide_wait();
      print "mountguide on\n";
      #mountGuideOn( $soc );
    }else{ print "***Should be doing the following:\n";
	   print "***Nodding to position $this_pos, beam=A\n";
	   print "***Probe $probe_name follow on\n";
	   print "***"; probe_wait();
	   print "***guide on\n";
	   print "***"; guide_wait();
	   print "***mountguide on\n";
	 }
    print "Should image $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    #-------------second position in object beam---------    
    $this_pos = $i+2;
    if( $this_pos <= $last_pos_num ){
      $X = $pat_x[$this_pos]; $Y = $pat_y[$this_pos];
      if( $VIIDO ){ 
	print "handset to position $this_pos: $X, $Y, beam=A\n";
	print "guide off\n";
	#guideOff( $soc );
	#setHandset_radec( $soc, $X, $Y );
	#handset_wait();
	print "guide on\n";
	#guideOn( $soc );
	#guide_wait();
      }else{ print "***Should be doing the following:\n";
	     print "***handset to position $this_pos: $X, $Y, beam=A\n";
	     print "***"; handset_wait();
	   }
      print "Should image $d_rptpos times here\n\n";
      $next_index = take_an_image( $header, $lut_name, $d_rptpos,
		     $file_hint, $next_index, $net_edt_timeout, $use_tcs);
    }
  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){ 
    print "offset to origin in beam=A\n";
    print "guide off\n";
    #guideOff( $soc );
    #setOffset_radec( $soc, 0, 0 );
    #offset_wait();
    print "guide on\n";
    #guideOn( $soc );
    #guide_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin in beam A\n";
	 print "***guide off\n";
	 print "***offset to position $startpos: $X, $Y, in beam=A\n";
	 print "***"; offset_wait();
	 print "***guide on\n";
	 print "***"; guide_wait();
       }


####SUBS
sub load_pattern_x{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_X;
    
  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    $ret_ar_ref = \@pat_3x3_rel_X;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_X;
    
  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_X;
    
  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_X;
    
  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_X;
  }
  
  return $ret_ar_ref;
}#Endsub load_pattern_x


sub load_pattern_y{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_rel_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_rel_Y;
    
  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_rel_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_rel_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_rel_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_rel_Y;
  }

  return $ret_ar_ref;

}#Endsub load_pattern_y


sub prompt_setup{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "Please have the SSA start guiding on the source pointing center\n";
  print "(no offsets) and set up in NOD BEAM=A\n\n";

  print "Are you ready?  Enter y to continue; n to exit.  ";
  my $ready = get_yn();
  if( !$ready ){
    die "\n\nExiting\n\n";
  }

}#Endsub prompt_setup


sub query_probe_name{
  print "Please tell me what guide probe you are using.\n";
  print "This code knows of the following probes:\n";
  print "Probe #\tProbe Name\n";
  print "  1    \t   p1\n";
  print "  2    \t   p2\n\n";
  print "Enter 1 or 2: ";

  my $probe_num;
  my $probe_name;
  my $is_valid = 0;
  until( $is_valid ){
    my $input = <STDIN>; chomp $input;
    $probe_num = $input;
    
    if( $probe_num == 1 or $probe_num == 2 ){
      $is_valid = 1;
      if( $probe_num == 1 ){
	$probe_name = "p1";
      }else{
	$probe_name = "p2";
      }
    }else{
      print "Please enter 1 or 2 ";
    }
  }
  print "Probe name is $probe_name\n\n";

  return $probe_name;

}#Endsub query_probe_name


sub setup_nod{
  my $soc = $_[0];
  my $throw    = param_value( 'THROW'    );
  my $throw_pa = param_value( 'THROW_PA' );

  ufgem::nodConfig( $soc, $throw, $throw_pa );

}#Endsub setup_nod


sub use_tcs{
  my ( $use_tcs, $header ) = @_;

  if( $use_tcs ){
    ####call to tcs subroutines goes here
    ####Must match all 18 characters in header
    if( $telescop eq 'Gemini-South      ' ){
      getGemTCSinfo( $header );
    }
  }else{
    print "___NOT___ getting tcs info\n\n";
  }

}#Endsub use_tcs


sub probe_follow_on{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOn( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOn( $soc );
  }

}#Endsub probe_follow_on


sub probe_follow_off{
  my ( $soc, $probe_name ) = @_;

  if( $probe_name eq "p1" ){
    p1FollowOff( $soc );

  }elsif( $probe_name eq "p2" ){
    p2FollowOff( $soc );
  }

}#Endsub probe_follow_off


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;

  #my $acq_cmd = acquisition_cmd( $header, $lut_name, 
  #				 $file_hint, $this_index, $net_edt_timeout);

  #print "acq cmd is \n$acq_cmd\n\n";
  #acquire_image( $acq_cmd );

  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    use_tcs( $use_tcs, $header );
    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image

#rcsId = q($Name:  $ $Id: dither.2x2.object.sky.gem.pl 14 2008-06-11 01:49:45Z hon $);
