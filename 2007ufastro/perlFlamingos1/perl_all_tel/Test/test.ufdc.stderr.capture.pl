#!/usr/local/bin/perl -w

use strict;
use Getopt::Long;

use Fitsheader       qw/:all/;
use MCE4_acquisition qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my $expt = 3;
my $nreads = 1;

MCE4_acquisition::setup_mce4( $expt, $nreads );


sub ver2_setup_mce4{
  my ($expt, $nreads) = @_;
  #>>>Setup MCE4 for exposure
  #>>>Read necessary header params for exposure<<<
  my $CT       = param_value( 'CYCLETYP' );
  #my $nreads_send_to_mce4 = $nreads - 1;
  #01 Oct 2001-Kevin just told me CT42 should get LDVAR 2 ($nreads - 2), 
  #because it does an extra two reads due to the way it's written
  my $nreads_send_to_mce4 = $nreads - 2;

  my $space = " ";
  my $quote = "\"";
  my $cmd_sep = $ENV{CLIENT_CMD_SEPARATOR};

  my $setup_mce4_head = $ENV{UFDC_DO_BASE} .$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                        $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                        $ENV{CLIENT_RAW}.$space;

  my $setup_mce4_body1 = $ENV{SET_EXPT_FRONT}.$expt.$ENV{SET_EXPT_BACK};#TIMER 0 EXPT S
  #my $setup_mce4_body2 = $space;
  my $setup_mce4_body2 = $space.$cmd_sep."STOP".$space;#Add MCE4 STOP command

  if( $CT == 40 ){
    #Don't reset LDVAR for nreads, as it's not used
  }elsif( $CT == 42 ){
    $setup_mce4_body2 = $setup_mce4_body2.$cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;
    #$setup_mce4_body2 = $cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;
  }else{
    die "\n\n".
        ">>>> WARNING    WARNGIN <<<<\n".
	"This script doesn't find values for the fitsheader keyword\n".
	"CYCLETYP that it understands.  It expects 40 or 42\n\n"; 
  }

  my $set_CT = $cmd_sep . $ENV{SET_CT} . $CT;

  my $net_mce4_cmd = $setup_mce4_head . $quote .
                     $setup_mce4_body1 . $setup_mce4_body2 . $set_CT .
		     $quote .
		     " 2>&1" ;

  print "\a\a\a\a\a";
  print "\n\nExecuting\n$net_mce4_cmd\n\n";
  #system( $net_mce4_cmd );
  my $reply = `$net_mce4_cmd`;
  system( "ufsleep 1.0");
}#Endsub ver2_setup_mce4

#rcsId = q($Name:  $ $Id: test.ufdc.stderr.capture.pl 14 2008-06-11 01:49:45Z hon $);
