#!/usr/local/bin/perl -w

use strict;
use MosWheel qw/:all/;


my $cnt = @ARGV;
if( $cnt < 1 ){ 
  #assume -help
  die "\n\tUSAGE:\n".
      "\n\ttweak.mos.pl (+|-)integer_offset".
      "\n\teg, tweak.mos.pl +5, or tweak.mos.pl -5\n\n";
}
else{
  my $offset = shift;

  main::check_input( $offset );
  main::check_number_decimals( $offset );
  my ( $sign, $value ) = main::check_number_signs( $offset );
  my $integer_offset   = main::round_input( $value );

  my $offset_cmd       = main::build_offset_cmd( $sign, $integer_offset );
  #Execute the motion
  print "\n\nEXECUTING\n$offset_cmd\n\n";
  system( $offset_cmd );
}


#####SUBS
sub check_input{
  my $offset = $_[0];

  if( $offset =~ m/[a-zA-Z]/ ){
    die "\n\nPlease enter a numerical offset\n\n";
  }
}#Endsub check_input


sub check_number_decimals{
  my $offset = $_[0];

  my $first_decimal = index $offset, ".";

  if( $first_decimal >= 0 ){
    my $trunc = substr $offset, $first_decimal+1;

    my $second_decimal = index $trunc, ".";
    if( $second_decimal >= 0 ){
      die "\n\nPlease enter only one decimal point\n\n";
    }
  }
}#Endsub check_number_decimals


sub check_number_signs{
  my $input = $_[0];
  my $sign = "+";
  my $value = $input;

  my $first_plus = index $input, "+";
  my $first_neg  = index $input, "-";

  if( $first_plus > 0 ){die "\n\nEnter a leading plus sign\n\n"  }
  if( $first_neg  > 0 ){die "\n\nEnter a leading minus sign\n\n" }

  if( $first_plus == 0 ){
    my $trunc = substr $input, $first_plus+1;
    my $second_plus = index $trunc, "+";
    if( $second_plus >= 0 ){ die "\n\nEnter only one leading plus sign\n\n" }
    $sign = "+";
    $value = $trunc;
  }

  if( $first_neg == 0 ){
    my $trunc = substr $input, $first_neg+1;
    my $second_neg = index $trunc, "-";
    if( $second_neg >= 0 ){ die "\n\nEnter only one leading minus sign\n\n" }
    $sign = "-";
    $value = $trunc;
  }

  return $sign, $value;
}#Endsub check_number_signs


sub round_input{
  my $input = $_[0];
  my $truncated_input = $input;

  #round input if there's a decimal
  my $dec_loc = index $input, ".";
  if( $dec_loc >= 0 ){
    $truncated_input = substr $input, 0, $dec_loc;
    my $rem = substr $input, $dec_loc+1, 1;

    #print "trunc = $truncated_input; rem = $rem\n";

    if( $rem ne "" ){
      my $trunc_len = length $truncated_input;
      if( $rem <= 5 ){
	if( $trunc_len == 0 ){
	  $truncated_input = 0;
	}else{
	  $truncated_input = $truncated_input + 0;
	}
      }elsif( $rem > 5 ){
	if( $trunc_len == 0 ){
	  $truncated_input = 1;
	}else{
	  $truncated_input = $truncated_input + 1;
	}
      }

      my $trunc_length = length( $truncated_input );
      if( $trunc_length == 1 ){
	if( $truncated_input =~ m/(\+|\-)/ ){ 
	  die "\n\nPlease enter a value >= 1\n\n" ;
	}
      }
    }
  }
  return $truncated_input;
}#Endsub round_input


sub build_offset_cmd{ 
  my ( $sign, $integer_offset ) = @_;

  my $i_v_cmd = $ENV{"I_1"}.$MosWheel::ampersand.$ENV{"V_1"};
  my $motion = $ENV{MOTOR_B}.$sign.$integer_offset;

  my $cmd = $MosWheel::cmd_head.$MosWheel::str_quote.
    $i_v_cmd.$MosWheel::ampersand.$motion.$MosWheel::str_quote;

  return $cmd;
}#Ensub build_offset_cmd

#rcsId = q($Name:  $ $Id: Oct25.tweak.mos.pl 14 2008-06-11 01:49:45Z hon $);
