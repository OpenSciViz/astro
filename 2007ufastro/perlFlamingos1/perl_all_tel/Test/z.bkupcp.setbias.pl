#!/usr/local/bin/perl -w

my $rcsID = q($Name:  $ $Id: z.bkupcp.setbias.pl 14 2008-06-11 01:49:45Z hon $);

package setbias;

use strict;
use Getopt::Long;
use GetYN;

my $DEBUG   = 0;#0 = not debugging; 1 = debugging
my $VERBOSE = 0;#0 = Quiet; 1 = verbose
GetOptions( 'debug'   => \$DEBUG );

###Set bias?
print "\n";
my $read_bias = dump_bias();
if( !$read_bias ){
  print "Continue to attempt to set bias anyways?\n";
  my $continue_query = get_yn();
  if( !$continue_query ){
    print "Quitting setbias.pl\n\n";
    exit;
  }
}
print "\n";

print "\nChange the detector bias from the above value?\n";
print "(MCE4 by default sets the bias on boot to SPECTROSCOPIC BIAS = ". 
       $ENV{SPECTRAL_BIAS} ." V)\n";
print "(IMAGING BIAS is ". $ENV{IMAGING_BIAS} ."V)\n";
my $reply = get_yn();
if($reply){
  set_bias();
  print "\nVerifying the change in bias\n";
  $read_bias = dump_bias();
  if( !$read_bias ){
    print "\a\a\a\a\a\a\a\n";
  }
}

####subs####
sub query_bias_value{
  #print ">>>Entering sub query_bias_value\n";
  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print "Enter i for imaging  mode (Vbias = ". $ENV{IMAGING_BIAS} ." V), or\n";
    print "Enter s for spectral mode (Vbias = ". $ENV{SPECTRAL_BIAS} ." V), or\n";
    print("Enter a value between 0 and 1.0\n ");
    chomp ($response = <STDIN>);

    if($response =~ m/i/i || $response =~ m/s/i){
      if($response eq 'i' || $response eq 'I'){
	print "Imaging bias value = ". $ENV{IMAGING_BIAS} ." V\n";
	$is_valid = 1;
	$response = $ENV{IMAGING_BIAS};
      }elsif($response eq 's' || $response eq 'S'){
	print "Spectra bias value = ". $ENV{SPECTRAL_BIAS} ." V\n";
	$is_valid = 1;
	$response = $ENV{SPECTRAL_BIAS};
      }
    }elsif( $response =~ m/\d{0,1}\.\d{1,2}/ || $response =~ m/\d{1}\.\d{0,2}/ ||
	    $response =~ m/0/                || $response =~ m/1/
	  ){
      #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
      print "response was $response\n";
      $is_valid = 1;
    }
  }
  print "\n";
  #print ">>>Exiting sub query_bias_value\n";
  return $response;
}#Endsub query_bias_value



sub set_bias{
  my $set_bias_to = query_bias_value();
  print "Seting bias to $set_bias_to\n\n";

  my $int_bias = 255 * $set_bias_to;
  my $idx   = index($int_bias, '.');
  if( $idx > 0 ) { 
    $int_bias = substr($int_bias, 0, index($int_bias, '.'));
  }

  my $dac_set_single_bias = "DAC_SET_SINGLE_BIAS 2 $int_bias ";
  my $dac_latch_all_bias  = "DAC_LAT_ALL_BIAS ";
  my $str_quote           = "\"";
  my $space               = " ";
  my $mce_head = $ENV{UFDC_DO_BASE}.
                 $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                 $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                 $ENV{CLIENT_RAW}.$space;

  ###Setting bias
  my $submit_list =$str_quote .
                   $dac_set_single_bias ."& $dac_latch_all_bias" .
                   $str_quote;
  my $mce_cmd = $mce_head . $submit_list;

  print ">>> Sending command to _SET_NEW_BIAS_:\n$mce_cmd\n\n";
  my $reply = `$mce_cmd`;

}#Endsub set_bias


sub dump_bias{
  my $read_bias = 1;
  my $str_quote = "\"";
  my $space     = " ";
  my $mce_head = $ENV{UFDC_DO_BASE}.
                 $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                 $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                 $ENV{CLIENT_RAW}.$space;

  ###Build string to dump bias from MCE4
  my $submit_list = $str_quote . "DAC_DUMP_BIAS " . $str_quote;
  my $mce_cmd     = $mce_head . $submit_list;
  mce_agent_sleep(2);

  print ">>> Sending command to _READ_PRESENT_BIAS_:\n$mce_cmd\n\n";
  mce_agent_sleep(2);
  my @reply = `$mce_cmd`;
  #print "@reply \n";

  if(@reply){
    my $row = $reply[3];
    #print "row three = $row\n";;
    my @row_elements = split( /\b/, $row);
    my $applied_bias = sprintf '%0.3f', $row_elements[3] / 255;
    print ">>> The present bias value from MCE4:\n";
    print ">>> MCE4 dac_dump_bias = $row_elements[3] = $applied_bias (V)\n\n";
  }else{
    print "!!!!! WARNING      WARNING !!!!";
    print "\a\a\a\a\a\a\a\a\a\a\a\a\a\a\n";
    print "!!!!! DANGER WILL ROBINSON !!!!!\n";
    print "!!!!! Could not read bias\n";
    print "!!!!! MCE4 dac_dump_bias was undef; check agent for details\n\n\n";
    $read_bias = 0;
  }

  return $read_bias;
}#Endsub dump_bias



sub mce_agent_sleep{
  my $wait_time = 5;

  if (defined $_[0]){
    $wait_time = $_[0];
  }

  my $waitcnt = 0;
  while( $waitcnt++ < $wait_time ) { 
    print"Sleep $waitcnt of $wait_time\n";
    sleep 1; 
  }
  print "\n";

}#Endsub bias_sleep


__END__


