#!/usr/bin/perl -w
# configWheels.pl : Frank Varosi, Feb.2001, interactively modify FITS header and move wheels.
my $rcsId = q($Name:  $ $Id: configWheels.pl 14 2008-06-11 01:49:45Z hon $);

use vars qw( $MOS $SLIT $FILTER $LYOT );


$CAMERA_PERL_LIBS = $ENV{"CAMERA_PERL_LIBS"};

push( @CAM_LIBS, $CAMERA_PERL_LIBS . 'LibHashTab.pl');
push( @CAM_LIBS, $CAMERA_PERL_LIBS . 'LibFitsHeader.pl');
push( @CAM_LIBS, $CAMERA_PERL_LIBS . 'LibFlamWheels.pl');
push( @CAM_LIBS, $CAMERA_PERL_LIBS . 'LibFlamMechControl.pl');

foreach $CAM_LIB (@CAM_LIBS)
  {
    if( -e $CAM_LIB and -r $CAM_LIB ) { do $CAM_LIB; }
    else {
      print "\n file $CAM_LIB does not exist or is not readable\n";
      print " shell environment variable CAMERA_PERL_LIBS = $CAMERA_PERL_LIBS , check value!\n";
    }
  }
print "\n";

my $FITSheaderFile = define_FH_file("Flamingos.fitsheader");   #prepends the CAMERA_FITS_HEADER path.

print "\n";
#Now start executing, define global vars and associative arrays (hash tables) first:

define_motor_client();
define_wheels();

readFitsHeader( $FITSheaderFile );

print "Current wheel positions in FITS header:\n\n";
printFitsHeader("obs_type",sort(keys(%MOTORS)));

my $obs_type = param_value("obs_type");
my ($new_obs_type);
my $answer = "no";

while( $answer ne "ok" )
  {
    print "\n";
    printFitsHeader("obs_type");
    print ".....Enter new value or RETURN to keep current value.\n";
    print ".....Options:   " . join(",  ",sort(keys(%OBS_TYPES))) . " ? ";
    $new_obs_type = <STDIN>;
    chomp( $new_obs_type );

    if( length( $new_obs_type ) > 0 )
      {
	if( defined $OBS_TYPES{$new_obs_type} )   # %OBS_TYPES is created by define_wheels();
	  {
	    if( $new_obs_type ne $obs_type ) { setStringParam( "obs_type", $new_obs_type ) }
	  }
	else {
	  print "\n *** invalid selection ...\n";
	  $new_obs_type = $obs_type;
	}
      }
    else { $new_obs_type = $obs_type }

    print ".....press ESCAPE and RETURN to exit,\n";
    print ".....or enter 'c' to change value:  OBS_TYPE = " . param_value("obs_type") . " ? ";
    $answer = <STDIN>;
    chomp( $answer );
    if( $answer eq "\e" ) { print "exiting...\n"; exit }
    if( $answer ne "c" ) { $answer = "ok" }
  }

$new_obs_type = param_value("obs_type");

if( ($new_obs_type eq "image-open") or ($new_obs_type eq "image-lyot") )
  {
    setStringParam( "MOS", "home" );
    setStringParam( "SLIT", "" );
  }

#------------------Now ask about moving other wheels:  mos / slit:
my (@movelist);

if( $new_obs_type eq "mos" )
  {
    push( @movelist, modWheelParam( %MOS ) );
    setStringParam( "SLIT", "" );
  }
elsif( $new_obs_type eq "slit" )
  {
    push( @movelist, modWheelParam( %SLIT ) );
    setStringParam( "MOS", "" );
  }
#------------------------------Lyot & Filter:

if( $new_obs_type ne "dark" )
  {
    if( $new_obs_type ne "image-open" )
      {
	push( @movelist, modWheelParam( %LYOT ) );
      }
    push( @movelist, modWheelParam( %FILTER ) );
  }

# Create an associative list (hash table) of other wheels and positions from FH params:

my ($position);
my ($wheel);

foreach $wheel (@movelist)
  {
    if( length($wheel) > 0 )
      {
	$position = param_value($wheel);

	if( length($position) > 0 )
	  {
	    %wheel_positions = ( %wheel_positions, $wheel=>$position );
	  }
      }
  }

#-----------Now execute motion commands, homing wheels first then moving to configuration:

my (%monitor);

if( $new_obs_type ne $obs_type )
  {
    %monitor = Config_Obs_Type( $new_obs_type, %wheel_positions );
  }
else
  {
    %monitor = Home_and_Move( %wheel_positions );
  }

#-----------make sure all FITS header params agree with new positions of wheels:
print "\n";
my ($posfh);

if( %monitor )
  {
    while( ($wheel, $position) = each(%monitor) )
      {
	if( $position ne ($posfh = param_value($wheel)) and $position ne "no_motion")
	  {
	    print " setting value of '$wheel' in FITS header from '$posfh' to '$position'\n";
	    setStringParam( $wheel, $position );
	  }
      }
  }

#--------------print results and monitor motions:
if( %monitor )
  {
    print "\nThe following wheels are moving to positions:\n\n";
    print_hash_sort_keys( %monitor );
    @movelist = sort( @MOTORS{sort(keys(%monitor))} );

    if( @movelist > 0 )
      {
	monitor_motion( @movelist );
	print "\nWriting new FITS header...\n";
	writeFitsHeader( $FITSheaderFile );

	my $dothis1 = "cp -p $FITSheaderFile /data1/flamingos";
	my $dothis2 = "cp -p $FITSheaderFile /data2/flamingos";
	system($dothis1);
	system($dothis2);

      }
    print "\n FITS header wheel params:\n\n";
    printFitsHeader("obs_type",sort(keys(%MOTORS)));
  }
else { print "\nNo changes made, nothing to move, nothing to write...\n"; }

print "\nDone\n";
exit;
#----------------End of Main-------------------------------------------------------------------

sub modWheelParam          # interactively modify a wheel parameter value of FITS header.
  {
    local ( %WHEEL ) = @_;
    my $wheel = $WHEEL{"name"};
    $wheel =~ tr/[a-z]/[A-Z]/;
    my ($new_pvalue);
    my $wposlist = get_pos_List(%WHEEL);
    my $pvalue = param_value($wheel);
    my $answer = "c";

    while( $answer eq "c" )
      {
	print "\n";
	printFitsHeader($wheel);
	print "Enter new value or RETURN to keep current value,\n";
	print "possible values:  $wposlist ? ";
	$new_pvalue = <STDIN>;
	chomp( $new_pvalue );

	if( defined $WHEEL{$new_pvalue} or (length( $new_pvalue ) == 0) )
	  {
	    if( length( $new_pvalue ) > 0 )
	      {
		if( $new_pvalue ne $pvalue ) { setStringParam( $wheel, $new_pvalue ) }
	      }
	    else { $new_pvalue = $pvalue }

	    if( defined $WHEEL{param_value($wheel)} )
	      {
		print " press RETURN to accept,\n";
		print " or enter 'c' to change value: $wheel = " . param_value($wheel) . " ? ";
		$answer = <STDIN>;
		chomp( $answer );
	      }
	    else { print "\n**** invalid parameter value ...\n"; }
 	  }
	else { print "\n**** invalid selection ...\n"; }
     }

    if( $new_pvalue ne $pvalue ) { return $WHEEL{"name"} } else { return "" }
  }
