#!/usr/local/bin/perl -w

use strict;

use ImgTests;

my $home_dir = $ENV{HOME}."/";
my $file_base1 = "Temperature.";
my $date_stamp = `date "+%Y.%b.%d"`;chomp $date_stamp;
my $file_base = $file_base1.$date_stamp;

my $index_sep = ".";
my $file_ext  = ".dat";

my $reply = ImgTests::does_dir_exist( $home_dir );

my $last_index = 
  ImgTests::get_last_index( $home_dir, $file_base, $index_sep, $file_ext );

my $next_index = $last_index + 1;
my $index_str = ImgTests::parse_index( $next_index );

my $file_name = $file_base.$index_sep.$index_str.$file_ext;
print "\n\n";
print ">>>  The new temperature data file will be $file_name\n";
print ">>>  Please do not delete old temperature data files!\n\n";

#rcsId = q($Name:  $ $Id: z.looktemp.pl 14 2008-06-11 01:49:45Z hon $);
