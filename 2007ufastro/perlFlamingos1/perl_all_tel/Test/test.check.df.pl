#!/usr/local/bin/perl -w

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my $orig_dir  = Fitsheader::param_value( 'ORIG_DIR' );

check_df( $orig_dir );

sub check_df{
  my $data_dir =$_[0];
  my $df_cmd = "df -k $data_dir";

  print "\n\n";
  print "Checking file system for space: $df_cmd\n";
  my $df_rval = `$df_cmd`;

  my $last_percent = rindex $df_rval, "%";
  my $df_val       = substr $df_rval, ($last_percent-2), 2;

  my $df_lower_limit = $ENV{DF_LOWER_LIMIT};
  my $df_upper_limit = $ENV{DF_UPPER_LIMIT};

  print "File system warning limit = $df_lower_limit% full\n";
  print "File system upper limit   = $df_upper_limit% full\n";
  if( $df_val < $df_lower_limit ){
    print "\n";
    print "The data file system is $df_val% full, below the warning limit.\n";

  }elsif( ($df_val > $df_lower_limit) and ($df_val < $df_upper_limit) ){
    print "\n";
    print "The data file system is $df_val% full.\n";
    print "You should consider moving/deleting some data.\n";
    print "Disregard and continue taking data ";
    my $reply = GetYN::get_yn();
    if( !$reply ){ die "Exiting";}

  }elsif( $df_val >= $df_upper_limit ){
    print "\n";
    print "The data file system is considerably full:\n$df_rval\n";
    die "You must move and/or delete some previous nights' data in order to continue.\n";
  }
  print "\n";
}#Endsub check_df

#rcsId = q($Name:  $ $Id: test.check.df.pl 14 2008-06-11 01:49:45Z hon $);
