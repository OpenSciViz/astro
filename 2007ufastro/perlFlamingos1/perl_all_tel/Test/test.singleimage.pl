#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: test.singleimage.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = param_value( 'EXP_TIME' );
my $nreads          = param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

my $orig_dir  = param_value( 'ORIG_DIR' );
my $filebase  = param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = does_dir_exist( $orig_dir );
my $last_index = whats_last_index($orig_dir, $filebase);
my $next_index = $last_index + 1;
print "This image's index will be $next_index\n";

set_filename_in_header( $filebase, $next_index, $header );

setup_mce4( $expt, $nreads );

#>>>>Get TCS info
my $use_tcs = param_value( 'USE_TCS' );
if( $use_tcs ){
  ####call to tcs subroutines goes here
}

#>>>Trigger system to acquire image
my $acq_cmd = acquisition_cmd( $header, $lut_name, 
                               $file_hint, $next_index, $net_edt_timeout);
acquire_image( $acq_cmd );
idle_lut_status( $expt, $nreads);

move_file_for_index_sep( $file_hint, $next_index );

exit;#That's That & That's the remark

#_______________________________________________________________________
####
####SUBS FOLLOW
####
sub whats_last_index{
  my ($orig_dir, $filebase) = @_;

  my $file_ext = $ENV{FITS_SUFFIX};

  #>>>Now figure out the next valid index
  #>>>NOTE does not look for identically named files in another directory
  my $index_sep_match_value = check_index_sep_match();
  #print "match value is $index_sep_match_value\n";

  my $last_index = 
   check_indices_for_index_sep( $index_sep_match_value, 
                                $orig_dir, $filebase, $file_ext);
  
  return $last_index;

}#Endsub whats_last_index

sub setup_mce4{
  my ($expt, $nreads) = @_;
  #>>>Setup MCE4 for exposure
  #>>>Read necessary header params for exposure<<<
  my $CT       = param_value( 'CYCLETYP' );
  my $nreads_send_to_mce4 = $nreads - 1;

  my $space = " ";
  my $quote = "\"";
  my $cmd_sep = $ENV{CLIENT_CMD_SEPARATOR};

  my $setup_mce4_head = $ENV{UFDC_DO_BASE} .$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                        $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                        $ENV{CLIENT_RAW}.$space;
	      
  my $setup_mce4_body1 = $ENV{SET_EXPT_FRONT}.$expt.$ENV{SET_EXPT_BACK};#TIMER 0 EXPT S
  my $setup_mce4_body2 = $space;
  if( $CT == 40 ){
    #Don't reset LDVAR for nreads, as it's not used
  }elsif( $CT == 42 ){
    $setup_mce4_body2 = $cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;
  }else{
    die "\n\n".
        ">>>> WARNING    WARNGIN <<<<\n".
	"This script doesn't find values for the fitsheader keyword\n".
	"CYCLETYP that it understands.  It expects 40 or 42\n\n"; 
  }

  my $set_CT = $cmd_sep . $ENV{SET_CT} . $CT;
  
  my $net_mce4_cmd = $setup_mce4_head . $quote .
                     $setup_mce4_body1 . $setup_mce4_body2 . $set_CT .
	            $quote;

  print "\a\a\a\a\a";
  print "\n\nExecuting\n$net_mce4_cmd\n\n";
  #system( $net_mce4_cmd );
  my $reply = `$net_mce4_cmd`;
  system( "ufsleep 1.0");

}#Endsub setup_mce4


sub acquisition_cmd{
  my ($header, $lutname, $file_hint, $next_index, $net_edt_timout) = @_;
  my $space = " ";
  my $quote = "\"";

  my $start    = $ENV{UFDC_ACQ_START_CMD};

  my $ds9; my $jpeg; my $png;
  my $disp_ds9 = param_value( 'DISP_DS9' );
  my $use_jpeg = param_value( 'JPEG'     );
  my $use_png  = param_value( 'PNG'      );
  #print "ds9     = $disp_ds9\n";
  #print "jpeg    = $use_jpeg\n";
  #print "use_png = $use_png\n";

  #jpeg and png have optional scale factors

  my $cnt      = " -cnt "    .param_value( 'TAKECNT' ); 
  my $lut      = " -lut "    .$lut_name;
  my $fits     = " -fits "   .$header;
  my $file     = " -file "   .$file_hint;
  my $index    = " -index "  .$next_index;
  my $timeout  = " -timeout ".$net_edt_timeout;

  my $acq_cmd_head = $ENV{UFDC}.
                     $space.$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                     $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
		     $ENV{UFDC_ACQ}.$space;

  my $acq_cmd_body = $start.$timeout.$cnt.
                     $lut.$fits.$file.$index;

  if( $disp_ds9 ){ 
    $ds9 = " -ds9";
    $acq_cmd_body = $acq_cmd_body . $ds9;
  }

  if( $use_jpeg ){ 
    $jpeg = " -jpeg";
    $acq_cmd_body = $acq_cmd_body . $jpeg;
  }

  if( $use_png ){ $png = " -png";
    $acq_cmd_body = $acq_cmd_body . $png;
  }

  my $acq_cmd      = $acq_cmd_head.
                     $quote.$acq_cmd_body.$quote;

  #print "Acquisition command is\n\n";
  #print "$acq_cmd\n\n";

  #ufdc -host flam1a -port 52008 -acq 
  #"start -ds9 -index 1 -cnt 1 -timeout 10 -lut lutfile -fits fitsfile 
  #-file filehint
  #-jpeg [scale] -png [scale]"

  return $acq_cmd;
}#Endsub acquisition_cmd


sub acquire_image{
  my $acq_cmd = $_[0];

  print "\nObservation starting.\a\a\a\a\a\n";
  print "Executing\n\n";
  print "$acq_cmd\n\n";
  print "\nIf it hangs here for more than ~6 seconds, type ufdc -q".
        " status in another $ENV{HOST} window\n\n\n";
  $reply = `$acq_cmd`;
  system("ufsleep 0.1");
  print "$reply\n";
  

}#Endsub acquire_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";
      
    }elsif( $sleep_count > $expt + $nreads + $lut_timeout ){

      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........".
            "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n";

      #stop take
   #   my $stop_take = "ufstop.pl -stop";
   #   system( $stop_take );
      #reset mce4 to ct 0
   #   system("ufidle.pl");
       exit;
       
    }#end time expired
    
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status

sub move_file_for_index_sep{
  my ( $file_hint, $this_index ) = @_;
  my $file_ext  = $ENV{FITS_SUFFIX};
  my $index_str = parse_index( $this_index );

  my $match_value = check_index_sep_match();

  #if index separators do not match
  #move final file, if it exists, to 
  #filename with proper separator
  if( $match_value == 0 ){
    my $space     = " ";
    my $uflib_index_sep   = $ENV{UFLIB_INDEX_SEPARATOR};
    my $desired_index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
    my $old_name  = $file_hint.$uflib_index_sep.$index_str.$file_ext;
    my $new_name  = $file_hint.$desired_index_sep.$index_str.$file_ext;
    
    if( -e $old_name ){
      #move to other name?
      use File::Copy;
      print "Executing\n";
      print "File::Copy call move( $old_name, $new_name )\n";
      my $success = move( $old_name, $new_name );

      if( $success == 0 ){
	print "\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\n";
	print ">>>>> WARNING   WARNING <<<<<\n";
	print ">>>>> PREVIOUS MOVE COMMAND FAILED\n";
      }elsif( $success ){
	print "Think copy succeeded\n";
      }
    }else{
      print ">>>>> WARNING  WARNING <<<<<\n";
      print ">>>>> Cannot execute   <<<<<\n";
      print "File::Copy call move( $old_name, $new_name )\n";
    }
  }

}#Endsub move_file_for_index_sep

sub parse_index{
  my $this_index = $_[0];

  my $index_str;
  if( $this_index > 0 and $this_index <= 9 ){
    $index_str = "000$this_index";
  }elsif( $this_index > 9 and $this_index <= 99 ){
    $index_str = "00$this_index";
  }elsif( $this_index > 99  and $this_index <= 999 ){
    $index_str = "0$this_index";
  }elsif( $this_index > 999 ){
    $index_str = $this_index;
  }else{
    die ">>>> WARNING    WARNING <<<<\n".
        "index, $this_index, out of bounds\n".
        "in sub parse_index\n\n";
  }

  return $index_str;
}#Endsub parse_index


sub set_filename_in_header{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};
  
  my $index_str = parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  setStringParam( "FILENAME", $filename );
  print "setting FILENAME to $filename\n";
  #writeFitsHeader( $header );

}#Endsub set_filename_in_header


