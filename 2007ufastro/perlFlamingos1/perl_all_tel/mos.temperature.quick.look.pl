#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Tk;

my $range = 10;

my $home = $ENV{'HOME'}."/";
#my $temperature_data_dir = $home."Flamingos/Temperature/Raw_Data/flamobs";
my $temperature_data_dir = $home."Temperature_Data";

#Testing
#$temperature_data_dir = "/home/raines/Flamingos/Devel";
#print $temperature_data_dir;

chdir $temperature_data_dir;

my $last_written_file = get_last_written_file( $temperature_data_dir );

#Testing
#$last_written_file = "Test.input.dat";

my $mw = MainWindow->new;
$mw -> title( "Mos Dewar Temperature Quick Look Plot Tool" );
Make_Menu();
#Make_Labels();

my $num_days        = 4;
my $ticks_per_day   = 6;
my $points_per_hour = 24;
my $points_per_day  = $ticks_per_day * $points_per_hour;
my $plot_width      = $num_days * $ticks_per_day * $points_per_hour;#=576
my $plot_height     = 300;

my $x_offset            = 20;
my $y_bottom_offset     = 20;
my $y_top_offset        = 60;
my $canvas_width_total  = 2*$x_offset + $plot_width; #20+576=596
my $canvas_height_total = $y_bottom_offset + $y_top_offset + $plot_height;#2*20+300=320

my $plot_x_left         = $x_offset;
my $plot_x_right        = $plot_x_left + $plot_width;

my $plot_y_top          = $y_top_offset;
my $plot_y_bottom       = $plot_y_top  + $plot_height;;


my $canvas = $mw->Canvas( -height => $canvas_height_total,
			  -width  => $canvas_width_total,
			  -background => 'white'
			)->pack(-side => "left", -fill => 'both',
				-expand => 1);

MainLoop;

#Subs
sub update_plot{
  my( $ar_time, $ar_array) = get_temp_data();
  my @time = @$ar_time;
  my @array = @$ar_array;

  my ($ar_scaled_array, $min) = scale_array( $ar_array, $range );
  my @scaled_array = @$ar_scaled_array;

  #plot the data, a point at a time
  for( my $i=0; $i<@array; $i++){
    $canvas->createLine($plot_x_left+$i, ($scaled_array[$i] + $y_top_offset),
		        -fill => 'red');
  }

  #plot 5 horizontal lines and label the y axis
  for( my $i=0; $i<=$range; $i++){
    my $val = ($plot_height*(1-$i/$range)) + $y_top_offset;
    $canvas->createLine($plot_x_left,$val, $plot_x_right,$val);
    $canvas->createText(10,$val, -text=>($min+$i));
  }

  #plot min and max y lines
  my $zline = $canvas->createLine($plot_x_left,$plot_y_top,    $plot_x_right,$plot_y_top);
  my $tline = $canvas->createLine($plot_x_left,$plot_y_bottom, $plot_x_right,$plot_y_bottom);

  #plot vertical lines, one per day
  for( my $i=0; $i<5; $i++){
    my $this_x = $plot_x_left + $i * $points_per_day;
    $canvas -> createLine( $this_x,$plot_y_bottom, $this_x,$plot_y_top);
    $canvas -> createText( $this_x,$plot_y_bottom+10, -text=>$i );
  }

  #plot vertical tick marks, one every 4 hours
  my $vert_tic_size = 10;
  for( my $i=0; $i<$points_per_hour; $i++){
    my $this_x = $plot_x_left + $i * $points_per_hour;
    $canvas -> createLine( $this_x,$plot_y_bottom, $this_x,($plot_y_bottom-$vert_tic_size));
    $canvas -> createLine( $this_x,$plot_y_top,   $this_x,($plot_y_top+$vert_tic_size) );
  }

  my $title = "Mos Dewar Temperature (K) vs. Time in Days";
  $canvas -> createText( 300,10, -text=>$title );
  $canvas -> createText( 300,25, -text=>$temperature_data_dir );
  $canvas -> createText( 300,40, -text=>$last_written_file );
}#Endsub draw_lines


sub get_temp_data{
  my $input_f = $last_written_file;
  my @lines;

  open( IN, $input_f ) || die "cannot open $input_f\n";
  for my $in (<IN>){
    chomp $in;
    push @lines, $in;		
  }
  close IN ;

  #strip off header info
  for( my $i=0;$i<6;$i++){
    shift @lines;
  }
  my $nlines = @lines;

  my @time;
  my @array;
  for( my $i=0;$i<$nlines;$i++){
    my @tmp = split /\s+/, $lines[$i];
    shift @tmp; #get rid of first space
    $time[$i] = $tmp[5];
    $array[$i] = $tmp[10];
  }

  $nlines = @array;
  #print "array now has $nlines lines\n";

  #Now need to throw away the data from the beginning if there's too much data
  my $three_days_points = 3* $points_per_day;
  if( $nlines >= $three_days_points ){
    my $shift_point = $nlines - $three_days_points;
    for( my $i=0; $i<$shift_point; $i++){
      shift @array;
    }
  }

  #$nlines = @array;
  #print "array now has $nlines lines\n";

  return( \@time, \@array );
}#Endsub get_temp_data


sub scale_array{
  my ($ar_in_array, $range) = @_;
  my @in_array = @$ar_in_array;
  my @scaled_array;

  my $min = find_min( \@in_array );
  print "min is $min\n";

  my $delta;
  for( my $i=0; $i<@in_array; $i++){
    $delta = $in_array[$i]-$min;
    $scaled_array[$i] = $plot_height*(1-$delta/$range);
  }

  return (\@scaled_array, $min);
}#Endsub scale_array


sub find_min{
  my $ar_in_array = $_[0];
  my @in_array = @$ar_in_array;

  my $size = @in_array;
  print "num elements = $size\n";
  #my $min = $in_array[0];
  my $min = 400;
  print "starting min = $min\n";
  for( my $i = 0; $i<@in_array; $i++){
    print "i = $i; Comparing last min, $min, to $in_array[$i]: ";
    if( ($in_array[$i] > 0) and ($in_array[$i] <= $min) ){
      print "min now = $in_array[$i]";
      $min = $in_array[$i];
    }
    print "\n";
  }

  my $integer = sprintf "%d", $min;
  #print "integer value = $integer\n";

  return $integer;
}#Endsub find_min


sub Make_Menu{
  my $menu_f = $mw -> Frame( -relief => 'ridge', -borderwidth => 2 );
  $menu_f    -> pack( -side => 'top', -anchor => 'n', -expand => 1, -fill => 'x' );

  my $quit_mb   = $menu_f -> Menubutton( -text    => "Quit",
					 -tearoff => 0,
					 -menuitems => [ ['command' => "Exit Program",
							  -command  => sub{exit;}]
						       ]
				       ) -> pack( -side => 'left' );

  my $update_mb = $menu_f -> Menubutton( -text    => "Update",
					 -tearoff => 0,
					 -menuitems => [
							[ 'command' => "Update Plot", 
							  -command  => [\&update_plot
									] ],
							]
							
				       ) -> pack( -side => 'left' );

  my $range_mb = $menu_f -> Menubutton( -text => "Temperature Range",
					-tearoff => 0,
					) -> pack( -side => 'left' );

  $range_mb -> radiobutton( -label => "5.0 K",
			    -variable => \$range,
			    -value => 5.0);

  $range_mb -> radiobutton( -label => "10.0 K",
			    -variable => \$range,
			    -value => 10.0);

  $range_mb -> radiobutton( -label => "20.0 K",
			    -variable => \$range,
			    -value => 20.0);

  $range_mb -> radiobutton( -label => "50.0 K",
			    -variable => \$range,
			    -value => 50.0);

  my $clear_mb = $menu_f -> Menubutton( -text => "Clear plot",
					-tearoff => 0,
					-menuitems => [['command' => "Clear plot",
						       -command => \&clear_canvas
						       ]]
					) -> pack( -side => 'left' );

}#Endsub Make_Menu


sub clear_canvas{

  $canvas ->addtag("newtag", "all");
  $canvas ->delete( "all" );

}#Endsub clear_canvas


sub get_last_written_file{
  my $data_dir = $_[0];

  use Time::Local;
  my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdat) = gmtime(time);
  my $elapsed_time = timegm( $sec, $min, $hour, $mday, $mon, $year );

  my $last_written_file;
  my $min_diff = 1000000;
  my $nextfile;
  my $def = 1;

  opendir( TDIR, $data_dir ) || die "Cannot open $data_dir\n";
  while( $def ){
    $nextfile = readdir( TDIR );
    if( !$nextfile ){
      $def = 0;
    }elsif( $nextfile =~ m/Temperature/ ){
      my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
	  $atime,$mtime,$ctime,$blksize,$blocks)
	= stat($nextfile);

      my $diff_sec = $elapsed_time - $mtime;
      my $diff_min = $diff_sec / 60; $diff_min = sprintf "%d", $diff_min;
      #print "nextfile = $nextfile Diff = $diff_sec sec = $diff_min min\n";
      if( $diff_min <= $min_diff ){
	$last_written_file = $nextfile;
      }
    }
  }
  closedir TDIR;

  #Testing
  #$last_written_file = "Test.input.dat";

  print "The file written to most recently was\n$data_dir/\n$last_written_file\n";

  return $last_written_file;
}#Endsub get_last_written_file


__END__

=head1 NAME

mos.temperature.quick.look.pl

=head1 DESCRIPTION

Runs a GUI for plotting the Mos dewar temperature data in the
most recent temperature log file for FLAMINGOS-1.

=head1 REVISION & LOCKER

$Name:  $

$Id: mos.temperature.quick.look.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
