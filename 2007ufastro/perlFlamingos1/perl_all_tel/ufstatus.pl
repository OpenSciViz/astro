#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


BEGIN { $| = 1; print "Initializing\n"; }
END {print "not ok 1\n" unless $loaded;}

$loaded = 1;
#print "ok 1\n";

use strict;
use Getopt::Long;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Constants      qw/:all/;
use MosWheel       qw/:all/;
use AllbutMosWheel qw/:all/;

use Fitsheader qw/:all/;
my $header = Fitsheader::select_header($DEBUG);
#$header = "/export/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
#$header = "/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);


#'Wheel'->require_version(0.01);
#print "Wheel constant is " . Wheel::PI . "\n";
#print "Wheel constant is " . PI . "\n";

#The following arrays will have 5 entries that correspond to @wheels
my @temps;
my @bias;

my $input;
my $loop = 1;

#system("clear");

my (@motors, @wheels, @actual_pos, @closest_pos, @closest_name);
my (@difference, @motion );
my ($ar1, $ar2, $ar3, $ar4, $ar5, $ar6, $ar7);
my ($mos_actual_pos, $mos_motion, $mos_closest_pos, $mos_closest_name,$mos_difference);
my $mos_motor    = qw( b );
my $mos_wheel    = qw( mos_or_slit );

my (@net_motors, @net_wheels);
my (@net_actual_pos, @net_closest_pos, @net_difference);
my (@net_closest_name, @net_motion);
my (@home_types, @net_home_types);
my $num_motors = 5;

while( $loop ){
  print "\nHit enter to go again, (x) to exit ";
  $input = <STDIN>;
  chomp( $input );

  if( $input eq 'x' ){
    print "exiting\n";
    $loop = 0;
  }else{
    #system("clear") unless $DEBUG;#equivalent to next line
    #system("clear") if !$DEBUG;
    #system("clear");

    print "\n\n";
    print "Asking for array & mos temperatures...\n\n";
    @temps = Get_Temps();
    print "\n\n";
    print "Asking for the array bias...\n\n";
    @bias = Get_Bias();

    ( $ar1, $ar2, $ar3, $ar4, 
      $ar5, $ar6, $ar7 ) =  AllbutMosWheel::get_wheel_status();
    @motors = @$ar1; @wheels = @$ar2; @actual_pos = @$ar3; @closest_pos = @$ar4;
    @closest_name = @$ar5; @difference = @$ar6; @motion = @$ar7;

    ($mos_actual_pos, $mos_motion, $mos_closest_pos, $mos_closest_name, 
     $mos_difference) = MosWheel::get_mos_status();

    make_net_vectors();

    get_home_types( \@home_types, \@net_home_types );
    
    #main::Print_Out( \@net_motors,       \@net_wheels, 
	#	     \@net_actual_pos,   \@net_closest_pos, 
	#	     \@net_closest_name, \@net_difference, \@net_motion,
	#	     \@home_types );
    main::Print_Out();
  }

}#End of while loop

#print "Out of loop\n";

##end of main code
##subs follow
sub Get_Temps{
  
  my $reply = `uflsc -q 1`;
  chomp( $reply );
  my @temps = split / /, $reply;
  shift @temps;
  $temps[0] =~ s/^1,//;
  $temps[1] =~ s/^2,//;
  $temps[2] =~ s/^6,//;
  #print "array = $temps[0], fanout = $temps[1], mos = $temps[2]\n";

  return @temps;

}#Endsub Get_Temps


sub Get_Bias{
  
  my $reply = `ufdc -q dac_dump_bias`;
  #print $reply . "\n";
  my @input_array = split /\.\.\.\./, $reply;
  my @split_input = split /=/, $input_array[1];
  my @resplit_input = split /\(/, $split_input[3];
  my $bin_bias = $resplit_input[0];
  #print( $bin_bias / 255 . "\n" );

  my @bias;
  $bias[0] = $bin_bias / 255;

  if(  $bias[0] == 1    ){ $bias[1] = "imaging" };
  if(  $bias[0] == 0.75 ){ $bias[1] = "spectroscopy" };
  if( ($bias[0] != 1) && ($bias[0] != 0.75) ){ $bias[1] = "User's choice"};

  return @bias;

}#Endsub Get_Bias


sub get_home_types{
  my ($ar_home_types, $ar_net_home_types) = @_;

  $$ar_home_types[0] = param_value( "HT_0" );#motor a = decker
  $$ar_home_types[1] = param_value( "HT_1" );#motor b = mos
  $$ar_home_types[2] = param_value( "HT_2" );#motor c = filter
  $$ar_home_types[3] = param_value( "HT_3" );#motor d = lyot
  $$ar_home_types[4] = param_value( "HT_4" );#motor e = grism

  for(my $i = 0; $i<5; $i++){
    if( $$ar_home_types[$i] == 0 ){
      $$ar_net_home_types[$i] = "Near-home";
    }elsif( $$ar_home_types[$i] == 1 ){
      $$ar_net_home_types[$i] = "Limit-switch";
    }
  }
}#Endsub get_home_types


sub make_net_vectors{
  $net_motors[0] = $motors[0];
  $net_motors[1] = $mos_motor;

  $net_wheels[0] = $wheels[0];
  $net_wheels[1] = $mos_wheel;

  $net_actual_pos[0] = $actual_pos[0];
  $net_actual_pos[1] = $mos_actual_pos;

  $net_closest_pos[0] = $closest_pos[0];
  $net_closest_pos[1] = $mos_closest_pos;

  $net_difference[0]  = $difference[0];
  $net_difference[1]  = $mos_difference;

  $net_closest_name[0] = $closest_name[0];
  $net_closest_name[1] = $mos_closest_name;

  $net_motion[0] = $motion[0];
  $net_motion[1] = $mos_motion;

  for( my $i = 2; $i <= 5; $i++ ){
    $net_motors[$i]       = $motors[$i-1];
    $net_wheels[$i]       = $wheels[$i-1];
    $net_actual_pos[$i]   = $actual_pos[$i-1];
    $net_closest_pos[$i]  = $closest_pos[$i-1];
    $net_difference[$i]   = $difference[$i-1];
    $net_closest_name[$i] = $closest_name[$i-1];
    $net_motion[$i]       = $motion[$i-1];
  }

}#Endsub make_net_vectors


sub Print_Out{
  #my ( $aref1, $aref2, $aref3, $aref4, $aref5, 
  #     $aref6, $aref7, $aref8, $aref9 ) = @_;
  #my @motors       = @$aref1;
  #my @wheels       = @$aref2;
  #my @actual_pos   = @$aref3;
  #my @closest_pos  = @$aref4;
  #my @closest_name = @$aref5;
  #my @difference   = @$aref6;
  #my @motion       = @$aref7;
  #my @temps        = @$aref8;
  #my @bias         = @$aref9;


  #system("clear");

  print "\n\n";
  print " ARRAY BIAS\n";
  print "_____________\n";
  printf("%1\.2f", $bias[0]);
  print " ($bias[1])\n";

  print "\n\n";
  print "    TEMPERATURES\n";
  print "_____________________\n";
  print "ARRAY        (Calibrated)   =  $temps[0] K\n";
  print "FANOUT MOUNT (Uncalibrated) =  $temps[1] K\n";
  print "MOS DEWAR    (Uncalibrated) =  $temps[2] K\n";

  print "\n\n";
  print "\t\t\t ACTUAL\t\tCLOSEST LIKELY\n";
  print "MOTOR\tWHEEL\t\tPOSITION\tPOSITION\  NAME   DIFFERENCE  MOTION  HOME-TYPES\n";
  print "_____\t_____\t\t________\t________________________________________________\n";

  #PRINT OUT DECKER INFO
  print " $net_motors[0]\t$net_wheels[0]\t\t$net_actual_pos[0]\t\t";
  print "$net_closest_pos[0]\t$net_closest_name[0]\t     $net_difference[0]\t";
  print "     $net_motion[0]\t$net_home_types[0]\n";

  #PRINT OUT MOS_SLIT INFO
  print " $net_motors[1]\t$net_wheels[1]\t$net_actual_pos[1]\t\t";
  print "$net_closest_pos[1]\t$net_closest_name[1]\t     $net_difference[1]\t";
  print "     $net_motion[1]\t$net_home_types[1]\n";  
 
  #PRINT OUT FILTER, LYOT, GRISM INFO
  for( my $i = 2; $i < 5; $i++ ){
    print " $net_motors[$i]\t$net_wheels[$i]\t\t$net_actual_pos[$i]\t\t";
    print "$net_closest_pos[$i]\t$net_closest_name[$i]\t     $net_difference[$i]\t";  
    print "     $net_motion[$i]\t$net_home_types[$i]\n";
  }

  print "\n\n";

}#Endsub PrintOut


__END__

=head1 NAME

ufstatus.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Does the same as the UFSTATUS_GUI, but in a text format
inside of an xterm window.


=head1 REVISION & LOCKER

$Name:  $

$Id: ufstatus.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
