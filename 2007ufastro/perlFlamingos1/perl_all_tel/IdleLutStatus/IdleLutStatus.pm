package IdleLutStatus;

use 5.006;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use IdleLutStatus ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	&idle_lut_status_before_move_movelut_orig
        &idle_lut_status_movelut_orig
	&idle_lut_status_slashn
	&idle_lut_status_slashr
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.

#Set this nonzero in order to keep print statement coming out the buffer
$| = 1;
my $lut_timeout = $ENV{LUT_TIMEOUT};
my $uffrmstatus = $ENV{UFFRMSTATUS};

sub idle_lut_status_before_move_movelut_orig{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    #print "UFFRMSTATUS: $frame_status_reply\n";
    $idle_notLutting = 
      grep( /idle: false, applyingLUT: true/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print ">>>>>>>>>>..........SHOULD BE APPLYING LUT..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status_before_move_movelut_orig


sub idle_lut_status_movelut_orig{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status_movelut_orig


sub idle_lut_status_slashn{
  my ($expt, $nreads) = @_;
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;

  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status_slashn


sub idle_lut_status_slashr{
  my ($expt, $nreads) = @_;
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = parse_framestatus_reply_orig( $frame_status_reply);

    if( ($idle_notLutting == 0) and ($sleep_count < 2) ){
      print_taking_image();

    }elsif( $idle_notLutting ==1 ){
      print "EXPOSURE DONE\n";
    }

    if( $sleep_count < $expt + $nreads ){
      if( $sleep_count < 2 ){
	print_total_time( $expt, $nreads );
      }
      print_net_sleep_time( $sleep_count, $expt, $nreads );

    }elsif( ($sleep_count >= $expt + $nreads)  ){
      if( $sleep_count eq $expt + $nreads ){ 
	print_exposure_time_elasped();
      }

      print_time_slept_past( $sleep_count, $expt, $nreads );

      if( $sleep_count > $expt + $nreads + $lut_timeout ){
	print_image_not_complete();
	exit;
      }#end time expired
    }

    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status_slashr


sub parse_framestatus_reply_orig{
  my $input = $_[0];

  my $this_status = grep( /idle: true, applyingLUT: false/, $input);

  return $this_status;
}#Endsub parse_framestatus_reply_orig


sub print_exposure_time_elasped{
  print "\a\a\a\a\a\n\n";
  print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
  print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
    "type ufstop.pl -stop -clean, then type fg to resume\n";
  print "If the ufstop.pl command hangs, hit ctrl-Z,\n".
    "and try ufstop.pl -clean only, then type fg to resume\n";
  print "Applying lut may take an additional 30 seconds\n\n";

}#Endsub print_exposure_time_elapsed


sub print_image_not_complete{
  print "\a\a\a\a\a\n\n";
  print ">>>>>>>>>>..........".
    "EXITING--IMAGE MAY NOT BE COMPLETE".
      "..........<<<<<<<<<<\n\n";
  print "IF it looks like it has stalled while applying the LUT,\n";
  print "run ufstop.pl -clean, and start over\n\n";
  print "Else if it has stalled sometime before applying the LUT,\n";
  print "run ufstop.pl -stop -clean, and start over\n\n";

}#Endsub print_image_not_complete


sub print_net_sleep_time{
  my ( $net_sleep, $this_expt, $this_nreads ) = @_;

  print "Have slept $net_sleep seconds out of an exposure time of ".
    ($this_expt + $this_nreads) ." seconds\r";
}#Endsub print_net_sleep_time


sub print_time_slept_past{
  my ( $net_sleep, $this_expt, $this_nreads ) = @_;

  print "\a\a\a\a\a";
  print "Have slept " . ($net_sleep - ($this_expt + $this_nreads)) .
    " seconds past the exposure time\r";

}#Endsub print_time_slept_past


sub print_taking_image{
  print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
}#Endsub print_taking_image


sub print_total_time{
  my ($this_expt, $this_nreads ) = @_;
  print "(Exposure time + number of reads = $this_expt + $this_nreads)\n";
}#Endsub print_total_time


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

IdleLutStatus - Perl extension for Flamingos

=head1 SYNOPSIS

  use IdleLutStatus;

=head1 DESCRIPTION
Variations on a theme for how to check whether or not the image
acquisition is finished.

=head2 EXPORT

None by default.


=head1 REVISION & LOCKER

$Name:  $

$Id: IdleLutStatus.pm,v 0.1 2003/05/22 15:19:30 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR

=head1 SEE ALSO

L<perl>.

=cut
