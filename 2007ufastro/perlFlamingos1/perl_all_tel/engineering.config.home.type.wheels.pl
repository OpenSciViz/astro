#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;
use GetYN;
use Fitsheader qw/:all/;
use Wheel qw/:all/;


my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

my @motors    = qw( a b c d e );
my @wheels    = qw( decker mos_or_slit filter lyot grism );


my @home_types_num   = get_present_home_types();
my @home_types_names = interp_types( @home_types_num );

print_home_type_msg();
print_home_types( \@motors, \@wheels, \@home_types_names );

my $redo   = 0;
my $didmod = 0;

until( $redo ){

  $didmod = change_home_type( \@motors, \@wheels, 
			      \@home_types_num, \@home_types_names );

  if( $didmod ){
    @home_types_names = interp_types( @home_types_num );
  }
  
  print "----NEW SETTINGS ARE----\n";
  print_home_types( \@motors, \@wheels, \@home_types_names );
  print "Accept the these settings? ";
  $redo = get_yn();
}

if( $didmod ){
  change_fh_vals( @home_types_num );
  writeFitsHeader( $header );
}

###SUB
sub get_present_home_types{
  my $num_mot = $ENV{NUM_MOTORS};
  my @home_types_num;
  my @home_types_names;

  for( my $i = 0; $i < $num_mot; $i++ ){
    my $num_type = param_value( ("HT_".$i) );
    $home_types_num[$i] = $num_type;
  }

  return @home_types_num;
}#Endsub get_present_home_types


sub interp_types{
  my @home_types_num = @_;
  my $num_mot = @home_types_num;
  
  my @home_types_names;
  for( my $i = 0; $i < $num_mot; $i++ ){

    if( $home_types_num[$i] == 0 ){
      $home_types_names[$i] = "Near";
    }elsif( $home_types_num[$i] == 1 ){
      $home_types_names[$i] = "Limit";
    }
  }
  
  return @home_types_names;
}#Endsub interp_types


sub change_home_type{
  my ( $ar_motors, $ar_wheels, $ar_home_types_num) = @_;
  my @motors  = @$ar_motors;
  my @wheels  = @$ar_wheels;
  my $num_mot = @motors;
  
  print "Change any home-types? ";
  my $change = get_yn();
  if( $change ){

    for( my $i = 0; $i < $num_mot; $i++){
      print "Change motor $motors[$i]? ";
      my $ch_mot = get_yn();

      if( $ch_mot ){

	if( $$ar_home_types_num[$i] == 0 ){
	  #0 = Near; 1 = Limit
	  $$ar_home_types_num[$i] = 1;

	}elsif( $$ar_home_types_num[$i] == 1){
	  #0 = Near; 1 = Limit
	  $$ar_home_types_num[$i] = 0;
	}
      }
    }
  }elsif( $change == 0 ){
    die "Exiting\n\n";
  }
  return $change;
}#Endsub change_home_type


sub print_home_type_msg{

  print "\n";
  print ">>>>>>>>>>>>>  Home Method  <<<<<<<<<<<<<<<<\n";
  print "____________________________________________\n";
  print "The wheels move to home before going to the \n";
  print "next position using one of the following two\n";
  print "techniques:\n\n";
  print "0 = Near  (Never  uses Limit Switch) and\n";
  print "1 = Limit (Always uses Limit Switch)\n\n";
  print "(If a limit switch ever fails, and you can\n";
  print " reindex that wheel--see below--, then use Near)\n\n";

  print "The near home option move the wheel backwards\n";
  print "from the present location to a position close\n";
  print "to home.  NOTE if the power on the controller\n";
  print "is cycled, it will come back up thinking that\n";
  print "all wheels are at position = 0 (irregardless\n";
  print "of the actual location) and you will have to\n";
  print "manually find home and reset the origin, and\n";
  print "then move to a positive position for near home\n";
  print "to work again.\n";

}#Endsub print_home_type_msg


sub print_home_types{
  my ( $ar_motors, $ar_wheels, $ar_home_types_names ) = @_;
  my @motors  = @$ar_motors;
  my @wheels  = @$ar_wheels;
  my @home_types_names = @$ar_home_types_names;
  my $num_mot = @motors;
  
  
  print "Motor\tWheel\t\tHome-type\n";
  print "_______________________________\n";
  
  for( my $i = 0; $i < $num_mot; $i++ ){

    if( $i == 1 ){ 
      print "$motors[$i]\t$wheels[$i]\t$home_types_names[$i]\n";
    }else{
      print "$motors[$i]\t$wheels[$i]\t\t$home_types_names[$i]\n";
    }

  }
  print "\n\n";
  
}#Endsub print_home_types


sub change_fh_vals{
  my @home_types_num = @_;
  #my $ar_home_types_num = $_[0];
  #my @home_types_num = @$ar_home_types_num;
  my $num_mot = @home_types_num;
  
  for( my $i = 0; $i < $num_mot; $i++ ){
    #print "setting HT_$i to $home_types_num[$i]\n";
    setNumericParam( ("HT_".$i), $home_types_num[$i] );
  }

}#Endsub change_fh_vals

__END__

=head1 NAME

engineering.config.home.type.wheels.pl

=head1 DESCRIPTION

Use to tell the template FITS header what manner
to home the mechanisms in FLAMINGOS-1.  The choices
are LIMIT, for use the limit switch, and NEAR HOME,
which uses David's relative stepping alorithm (really
should be called Not Home).

=head1 REVISION & LOCKER

$Name:  $

$Id: engineering.config.home.type.wheels.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
