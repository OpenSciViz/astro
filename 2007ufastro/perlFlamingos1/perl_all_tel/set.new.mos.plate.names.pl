#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use GetYN;
use ImgTests;
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header;
if( !$DEBUG ){
  $header = $ENV{FITS_HEADER_LUT}.$ENV{MOS_NAMES};
}else{
  $header = $ENV{FITS_HEADER_LUT_DEBUG}.$ENV{MOS_NAMES};
}

Find_Fitsheader($header);
readFitsHeader($header);

print "\n>>>                 Present Mos Plate Names                             <<<\n";
print "_____________________________________________________________________________\n";
printFitsHeader( "NEGNAMES", "MOS_0_K",  "MOS_2_H",  "MOS_4_F",  "MOS_6_D",  "MOS_8_B" );
print "\n";

printFitsHeader( "IMGNAMES", "IMG_APER" );
print "\n";

printFitsHeader( "POSNAMES", "MOS_12_Q", "MOS_13_P", "MOS_14_O", "MOS_15_N", "MOS_16_M", "MOS_17_L");
print "\n";

print "\n\nChange any parameter? ";
my $change = get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

my $didmod = 0;
my $redo = 0;
until( $redo ){
  $didmod += Modify_plate_name( "MOS_0_K" );
  $didmod += Modify_plate_name( "MOS_2_H" );
  $didmod += Modify_plate_name( "MOS_4_F" );
  $didmod += Modify_plate_name( "MOS_6_D" );
  $didmod += Modify_plate_name( "MOS_8_B" );

  $didmod += Modify_plate_name( "IMG_APER" );

  $didmod += Modify_plate_name( "MOS_12_Q" );
  $didmod += Modify_plate_name( "MOS_13_P" );
  $didmod += Modify_plate_name( "MOS_14_O" );
  $didmod += Modify_plate_name( "MOS_15_N" );
  $didmod += Modify_plate_name( "MOS_16_M" );
  $didmod += Modify_plate_name( "MOS_17_L" );

  if( $didmod > 0 ){
    print "\n\nThe new set of exposure parameters are:\n";
    print "_____________________________________________________________________________\n";
    printFitsHeader( "NEGNAMES", "MOS_0_K",  "MOS_2_H",  "MOS_4_F",  "MOS_6_D",  "MOS_8_B" );
    print "\n";

    printFitsHeader( "IMGNAMES", "IMG_APER" );
    print "\n";

    printFitsHeader( "POSNAMES", "MOS_12_Q", "MOS_13_P", "MOS_14_O", "MOS_15_N", "MOS_16_M", "MOS_17_L");
    print "\n";

    print "\nAccept changes (y/n)?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

print "didmod = $didmod\n";

if( $didmod > 0 ){
  if( !$DEBUG ){
    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}

#Final print
print "\n";
###End of main

###subs
sub Modify_plate_name{
  my $mos_position = $_[0];
  my $iparm = param_index( $mos_position );
  my $max_length_plate_name = 41;

  print "\n";
  print 
"__________________________________________________________________________\n";
  print "Enter a new name for the selected mos plate.\n";
  print "The length must be less than $max_length_plate_name characters.\n\n";

  my $nvals = @fh_pvalues;
  my $new_pvalue;
  my $vlen;
  my $didmod = 0;

  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );

      $vlen = length( $new_pvalue );
      if( $vlen > 0 and $vlen <= $max_length_plate_name ){
	$fh_pvalues[$iparm] = '\'' . $new_pvalue.(' ' x ($max_length_plate_name-$vlen)).'\'';
	print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	print "\n";
	$didmod = 1;
	$is_valid = 1;

      }elsif( $vlen > $max_length_plate_name ){
	print "The path entered is > $max_length_plate_name characters long.\n";
	print "Sorry, but you have to use a different mos plate name.\n";

      }elsif( $vlen <= 0 ){
	print "Not changing the current mos plate name.\n";
	$didmod = 0;
	$is_valid = 1;
      }
    }else{
      print ">>>      WARNING      WARNING      <<<\n";
      print "Cannot find $mos_position keyword in FITS header.\n";
      $is_valid = 1;
    }
  }
  return $didmod;

} #Endsub mod_FILEBASE


__END__

=head1 NAME

set.new.mos.plate.names.pl

=head1 DESCRIPTION

Run this script after loading a new set of mosplates in
FLAMINGOS-1, so that the config.mos.wheel.pl script tells
you what's loaded when it comes time to actually use one of
the mosplates.

=head1 REVISION & LOCKER

$Name:  $

$Id: set.new.mos.plate.names.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
