#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use GetYN;

my $xtp       = "xterm -fn 6x12 -geometry 112x6+0+0 -sb -sl 2000 ";

my $ls208_str = $xtp . "-n TEMP -T TEMP -e " .
                       "$ENV{UFLS208D} -chan \"1, 2, 6\" " .
                       "$ENV{DAEMON_TSHOST} $ENV{IOCOMM} " .
                       "$ENV{DAEMON_TSPORT} $ENV{UFLS208D_PORT} &";

my $xtp4      = "xterm -fn 6x12 -geometry 87x15+0+325 -sb -sl 2000 ";

my $rec_temp_str = ($xtp4 . "-n RECORD_TEMPS -T RECORD_TEMPS -e ".
		    "/usr/local/flamingos/perl_all_tel/Sh_rec_temp &" );

print "\n\tstart.temperature.demon.pl\n".
      "\tUse to initialize demon to monitor temperature\n".
      "\tfrom the temperature sensor (LakeShore 208)\n\n";

print "\tIf the demon has hung, please kill it before\n".
      "\trunning this program.\n\n";

print "\tAfter running, please turn on logging of the temperature:\n".
      "\tIn the xterm window running the demon, type Ctrl-left-mouse-button\n".
      "\tand enable 'Log to File'\n\n";

print "\tContinue? ";
my $response = get_yn();
if ($response == 0){ die "Exiting\n";}

#Make sure the demon's not running
my $pslist = `ps -ef`;
#my $look_for_ps = index($pslist,"ufls208d");
my $look_for_ps = index($pslist,$ENV{UFLS208D});

if($look_for_ps >= 0){
  print "LakeShore Temperature agent already running\n\n";
}elsif ($look_for_ps < 0){
  print "Executing LakeShore Temperature agent\n";
  system($ls208_str);
  print "sleep 5 to start agent\n";
  sleep 5;
  print "\n";
}

$look_for_ps = index($pslist, "record");
if( $look_for_ps >= 0){
  print "record.temperatures.pl already running\n";
}elsif( $look_for_ps < 0){
  print "starting temperature logging; please leave running\n";
  system( $rec_temp_str );
  print "sleep 5\n";
  sleep 5;
  print "\n";
}


__END__

=head1 NAME

start.temperature.demon.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Starts the uflib temperature daemon only.
Probably better to just use initflam.pl to get things up and
running.

=head1 REVISION & LOCKER

$Name:  $

$Id: start.temperature.demon.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
