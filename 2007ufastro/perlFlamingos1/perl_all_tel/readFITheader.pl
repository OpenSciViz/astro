#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use Getopt::Long;

my $Input_fits_file = '';
GetOptions ('file=s' => \$Input_fits_file);

if( $Input_fits_file eq '' ){
  die "\nUsage:\nreadFITheader.pl -file fitsheader_only (not an actual image)\n\n";
}

#$FITheaderFile = "Flamingos.fitsheader";
$FITheaderFile = $Input_fits_file;

print "\ninfile is $FITheaderFile\n";

open( IN, $FITheaderFile );
while( <IN> ) { $header = $_; }
close( IN );

$slen = length( $header );
print "\n # bytes in FITS header = $slen\n";

$nrec = $slen/80;
print "\n Nrecords = $nrec\n\n";

for( $i=0; $i<$nrec; $i++ )
  {
    push( @records, substr( $header, $i*80, 80 ) );
  }

$nrecs = @records;

if( $nrec ne $nrecs ) {print "number of records is not what was expected\n";}

#for( $i=0; $i<2; $i++ )
for( $i=0; $i<$nrecs-1; $i++ )
  {
    $loceq = index($records[$i],'=');
    if( $loceq == 8 ){
      $tag = substr( $records[$i], 0, $loceq );
      print "$tag= ";
      $locslash = index($records[$i],'/ ');
      $value = substr( $records[$i], $loceq+2, $locslash-$loceq-3 );
      print "$value / ";
      $comment = substr( $records[$i], $locslash+2 );
      print "$comment\n";
    }else{
      print "$records[$i]\n";
    }
  }

$tlen = length($tag);
$vlen = length($value);
$clen = length($comment);

print "$tlen\n";
print "$vlen\n";
print "$clen\n";

exit;

__END__

=head1 NAME

readFITheader.pl

=head1 DESCRIPTION

readFITheader.pl -f <template FITS header file name>

Use to look at the FLAMINGOS-1 template FITS header.


=head1 REVISION & LOCKER

$Name:  $

$Id: readFITheader.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
