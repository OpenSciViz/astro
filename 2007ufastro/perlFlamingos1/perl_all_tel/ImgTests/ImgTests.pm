package ImgTests;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use ImgTests ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
		 &check_df
		 &check_index_sep_match
		 &check_indices_for_index_sep
		 &display_image
		 &does_dir_exist
		 &get_last_index
		 &move_file_for_index_sep
		 &make_unlock_file
		 &pad_data
		 &parse_index
		 &set_filename_in_header
		 &setFilenameParam
		 &whats_last_index
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
sub check_df{
  my $data_dir =$_[0];
  my $df_cmd = "df -k $data_dir";

  print "\n\n";
  print "Checking file system for space: $df_cmd\n";
  my $df_rval = `$df_cmd`;

  my $last_percent = rindex $df_rval, "%";
  my $df_val       = substr $df_rval, ($last_percent-2), 2;

  my $df_lower_limit = $ENV{DF_LOWER_LIMIT};
  my $df_upper_limit = $ENV{DF_UPPER_LIMIT};

  print "File system warning limit = $df_lower_limit% full\n";
  print "File system upper limit   = $df_upper_limit% full\n";
  if( $df_val < $df_lower_limit ){
    print "\n";
    print "The data file system is $df_val% full, below the warning limit.\n";

  }elsif( ($df_val > $df_lower_limit) and ($df_val < $df_upper_limit) ){
    print "\n";
    print "The data file system is $df_val% full.\n";
    print "You should consider moving/deleting some data.\n";
    print "Disregard and continue taking data ";
    my $reply = GetYN::get_yn();
    if( !$reply ){ die "Exiting";}

  }elsif( $df_val >= $df_upper_limit ){
    print "\n";
    print "The data file system is considerably full:\n$df_rval\n";
    die "You must move and/or delete some previous nights' data in order to continue.\n";
  }
  print "\n";
}#Endsub check_df


sub check_index_sep_match{
  #uflib uses a . between filehint and count based on index
  #gemini desires a _ 
  #this allows us to check if they match, and maybe mv files 
  #around after the data are taken
  my $uflib_index_sep   = $ENV{UFLIB_INDEX_SEPARATOR};
  my $desired_index_sep = $ENV{DESIRED_INDEX_SEPARATOR};

  my $match_value = 0;
  if( $desired_index_sep eq $uflib_index_sep ){
    #print "they match, don't have to do anything\n";
    $match_value = 1;
  }else{
    #print "separators don't match\n";
    #print "do something about it\n";
    $match_value = 0;
  }
  return $match_value;
}#Endsub check_index_sep_match


sub check_indices_for_index_sep{
  my ($index_sep_match_value, $orig_dir, $filebase, $file_ext) = @_;
  my $index_sep;

  if( !$index_sep_match_value ){
    #Check for existence of both file types
    my $index_sep_uf = $ENV{UFLIB_INDEX_SEPARATOR};
    my $last_found_index_uf_sep  = 
      get_last_index( $orig_dir, $filebase, $index_sep_uf, $file_ext);
    print "last uf index = $last_found_index_uf_sep\n";

    my $index_sep_desired = $ENV{DESIRED_INDEX_SEPARATOR};
    my $last_found_index_desired_sep  = 
      get_last_index( $orig_dir, $filebase, $index_sep_desired, $file_ext);
    print "last desired index = $last_found_index_desired_sep\n";

    if( $last_found_index_desired_sep > $last_found_index_uf_sep ){
      return $last_found_index_desired_sep;
      print "last desired index = $last_found_index_desired_sep\n";
    }elsif( $last_found_index_desired_sep == 0 and
	    $last_found_index_uf_sep      == 0 ){
      return $last_found_index_desired_sep;
      print "last desired index = $last_found_index_desired_sep\n";
    }else{
      die "\n\n" .
	  ">>>> WARNING     WARNING <<<\n".
	  "The are more files of the form $filebase".
	  $index_sep_uf ."####$file_ext\n".
          "than there are files of the form $filebase" .
	  $index_sep_desired ."####$file_ext\n\n".
	  "Either pick a new base name, or remove the files of the form\n".
	  "$filebase$index_sep_uf####$file_ext\n\n";
    }
  }elsif( $index_sep_match_value ){
    #Only check for files with desired index separator
    $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
    my $last_found_index_desired_sep  = 
      get_last_index( $orig_dir, $filebase, $index_sep, $file_ext);
    print "last desired index = $last_found_index_desired_sep\n";
    return $last_found_index_desired_sep;
  }
}#Endsub check_indices_for_index_sep


sub display_image{
  my ($file_hint, $this_index) = @_;

  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};
  my $index_str = ImgTests::parse_index( $this_index );
  my $netfilename = $file_hint.$index_sep.$index_str.$file_ext;
  print "This will be $netfilename\n";

  my $frame;
  print "this index = $this_index\n";
  my $len = length $this_index;
  my $last = substr $this_index, ($len - 1);
  print "last char is $last\n";

  if( $last == '1' or $last == '3' or $last == '5' or $last == '7' or $last == '9'){
    $frame = 1;
    #print "odd, frame = $frame\n";
  }elsif( $last == '0' or $last == '2' or $last == '4' or $last == '6' or $last == '8' ){
    $frame = 2;
    #print "even, frame = $frame\n";
  }

  my $xpaset_frame = "xpaset -p ds9 frame $frame";
  my $xpaset_file  = "xpaset -p ds9 file $netfilename";

  #print "SHOULD BE: Executing $xpaset_frame\n";
  print "Executing $xpaset_frame\n";
  my $rep1 = `$xpaset_frame`;

  #print "SHOULD BE: Executing $xpaset_file\n";
  print "Executing $xpaset_file\n";
  my $rep2 = `$xpaset_file`;

}#Endsub display_image


sub does_dir_exist{
  my $dir_path = $_[0];
  my $od_return = opendir( DH, $dir_path ) ;
  close DH;

  if( !defined $od_return ){
    print "\n\n>>> WARNING <<<\n";
    print "Cannot find $dir_path\n\n";
    print "Continue ?";
    use GetYN;
    my $go_on = get_yn();
    if( $go_on eq 0 ){
      #print "Quiting\n";
      die "Quiting\n";;
    }
  }elsif( defined $od_return ){
    return "Found $dir_path\n";
  }

}#Endsub does_dir_exist


sub get_last_index{
  my ($orig_dir, $filebase, $index_sep, $file_ext) = @_;
  #print "orig_dir  = $orig_dir\n";
  #print "filebase  = $filebase\n";
  #print "index sep = $index_sep\n";
  #print "file ext  = $file_ext\n";

  my $filenm = $orig_dir . $filebase . $index_sep;

  print "searching for $filenm####$file_ext\n";

  my $i=1;
  my $last_found = 0;
  for($i = 0; $i <= 9999; $i += 1){
    my $matchname = $filenm;
    if( $i > 0 && $i <= 9){
      $matchname = $matchname . "000$i" . $file_ext;
      #print "searching for $matchname\n";
      if( -e $matchname ){
	$last_found = $i;
      }
    }elsif( $i > 9 && $i <= 99){
      $matchname = $matchname . "00$i" . $file_ext;
      if( -e $matchname ){
	$last_found = $i;
      }
    }elsif( $i > 99 && $i <= 999 ){
      $matchname = $matchname . "0$i" . $file_ext;
      if( -e $matchname ){
	$last_found = $i;
      }
    }elsif( $i > 999 && $i <= 9999){
      $matchname = $matchname . "$i" . $file_ext;
      if( -e $matchname ){
	$last_found = $i;
      }
    }elsif( $last_found > 999){
      die "\n\n" .
          ">>>    WARNING                     WARNING     <<<\n" .
	  "There seem to be more than 9999 files with $filenm\n" .
	  "as base name.  Pick a new basename and try again.\n\n";
    }
  }
  #print "last found is $last_found\n";
  return $last_found;

}#Endsub get_last_index


sub make_unlock_file{
  my ( $file_hint, $this_index ) = @_;
  my $index_str = parse_index( $this_index );
  my $uflib_index_sep   = $ENV{UFLIB_INDEX_SEPARATOR};

  my $unlock_filename = $file_hint.$uflib_index_sep.$index_str.".unlock";
  my $cmd = "touch $unlock_filename";
  print "Writing unlock file $unlock_filename\n";
  system( $cmd );
  my $chmod_cmd = "chmod 0777 $unlock_filename";
  print "Changing permissions on file:  $chmod_cmd\n";
  `$chmod_cmd`;
}#Endsub make_unlock_file


sub move_file_for_index_sep{
  my ( $file_hint, $this_index ) = @_;
  my $file_ext  = $ENV{FITS_SUFFIX};
  my $index_str = parse_index( $this_index );

  my $match_value = check_index_sep_match();

  #if index separators do not match
  #move final file, if it exists, to 
  #filename with proper separator
  if( $match_value == 0 ){
    my $space     = " ";
    my $uflib_index_sep   = $ENV{UFLIB_INDEX_SEPARATOR};
    my $desired_index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
    my $old_name  = $file_hint.$uflib_index_sep.$index_str.$file_ext;
    my $new_name  = $file_hint.$desired_index_sep.$index_str.$file_ext;

    if( -e $old_name ){
      #move to other name?
      use File::Copy;
      print "Executing\n";
      print "File::Copy call move( $old_name, $new_name )\n";
      my $success = move( $old_name, $new_name );

      #print "mv ( $old_name $new_name )\n";
      #my $mv_str = "mv ".$old_name." ".$new_name;
      #my $success = system( $mv_str );

      if( $success == 0 ){
	print "\a\a\a\a\a\a\a\a\a\a\a\a\a\a\a\n";
	print ">>>>> WARNING   WARNING <<<<<\n";
	print ">>>>> PREVIOUS MOVE COMMAND FAILED\n";
      }elsif( $success ){
	print "Think copy succeeded\n";
      }
    }else{
      print ">>>>> WARNING  WARNING <<<<<\n";
      print ">>>>> Cannot execute   <<<<<\n";
      print "File::Copy call move( $old_name, $new_name )\n";
    }
  }

}#Endsub move_file_for_index_sep


sub pad_data{
  my ($file_hint, $this_index) = @_;

  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};
  my $index_str = ImgTests::parse_index( $this_index );
  my $netfilename = $file_hint.$index_sep.$index_str.$file_ext;

  print "\n";
  print "Padding $netfilename so it has an integral number of 2880 byte records.\n";
  print "\n";

  my $binary_zero = pack( "L", 0 );

  open IF, ">>$netfilename" || warn "Cannot open $netfilename to pad it.\n";;
  for( my $i = 0; $i < 416; $i++ ){
    print IF $binary_zero;
  }
  close IF;

}#Endsub pad_data


sub parse_index{
  my $this_index = $_[0];

  my $index_str;
  if( $this_index > 0 and $this_index <= 9 ){
    $index_str = "000$this_index";
  }elsif( $this_index > 9 and $this_index <= 99 ){
    $index_str = "00$this_index";
  }elsif( $this_index > 99  and $this_index <= 999 ){
    $index_str = "0$this_index";
  }elsif( $this_index > 999 ){
    $index_str = $this_index;
  }else{
    die ">>>> WARNING    WARNING <<<<\n".
        "index, $this_index, out of bounds\n".
        "in sub parse_index\n\n";
  }

  return $index_str;
}#Endsub parse_index


sub set_filename_in_header{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};

  my $index_str = parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  print "setting FILENAME to $filename\n";
  setFilenameParam( $filename );
  writeFitsHeader( $header );

}#Endsub set_filename_in_header


sub setFilenameParam{
  my $new_filename = $_[0];
  my $new_pvalue = $new_filename;

  my $nvals = @fh_pvalues;
  my $iparm = param_index( 'FILENAME' );

  my $vlen = length $new_filename ;
  if( $vlen > 0 and $vlen <= 38 ){
    	$fh_pvalues[$iparm] = '\'' . $new_pvalue . '\'' . (' ' x (38-$vlen));
	print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	print "\n";

  }else{
    die "The new filename $new_filename is too long for the header\n\n";
  }

}#Endsub setFilenameParam


sub whats_last_index{
  my ($orig_dir, $filebase) = @_;

  my $file_ext = $ENV{FITS_SUFFIX};

  #>>>Now figure out the next valid index
  #>>>NOTE does not look for identically named files in another directory
  my $index_sep_match_value = check_index_sep_match();
  #print "match value is $index_sep_match_value\n";

  my $last_index = 
   check_indices_for_index_sep( $index_sep_match_value, 
                                $orig_dir, $filebase, $file_ext);

  return $last_index;

}#Endsub whats_last_index


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

ImgTests - Perl extension for Flamingos data taking

=head1 SYNOPSIS

  use ImgTests;

  requires GetYN.pm be installed
  

=head1 DESCRIPTION

Some simple routines to easily check that the observer has created the
directory that they want to write data into, and to figure out what the 
next valid image index might be.

=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: ImgTests.pm,v 0.1 2003/05/22 15:20:22 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR 21 August 2001

=head1 SEE ALSO

perl(1).

=cut
