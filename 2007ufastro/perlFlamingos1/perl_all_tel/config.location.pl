#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Copy the correct fitsheader file for the present telescope
#to /usr/local/flamingos/flamingos.headers.lut/Flamingos.fitsheader
#
##############################################################################

use strict;
use Getopt::Long;
use GetYN;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $telescope = "unknown";

GetOptions( 'debug' => \$DEBUG,
            'telescope=s' => \$telescope);

my $fitsheader_file = "";

if( $telescope =~ m/^kp2m$/i ){
  $fitsheader_file = "Flamingos.kpno.2m.fitsheader";
}elsif( $telescope =~ m/^kp4m$/i ){
  $fitsheader_file = "Flamingos.kpno.4m.fitsheader";
}elsif( $telescope =~ m/^mmt$/i ){
  $fitsheader_file = "Flamingos.mmt.fitsheader";
}elsif( $telescope =~ m/^gem$/i ){
  $fitsheader_file = "Flamingos.gemini.fitsheader";
}else{
  $telescope = "unknown";
}


if( $telescope eq "unknown"){
    usage();
    exit;
}

print "\nTelescope is $telescope\n";
print "Base template fits header for $telescope is $fitsheader_file\n";

my $fitsheader_path ="";
my $header;
if( !$DEBUG ){
  $fitsheader_path = $ENV{"FITS_HEADER_LUT"};
  $header =  $fitsheader_path . $fitsheader_file;
}elsif( $DEBUG ){
  $fitsheader_path = $ENV{"FITS_HEADER_LUT_DEBUG"};
  $header          = $fitsheader_path . $fitsheader_file;
  print "In debug mode\n";
  print "Header is $header\n";
}

if( -e $header ){
  print "\nFound $header\n\n";
}else{
  die( "\nCannot find $header:\n" . $! . "\n\n");
}

if( !$DEBUG ){
  my $cp_string = $ENV{"FITS_HEADER_LUT"} . "Flamingos.fitsheader";
  my $dothis = "cp -p $header " . $cp_string;
  print "Copying $header to $cp_string \n\n";
  system($dothis);

}elsif( $DEBUG ){
    print "\n\nShould be copying $header\n";
    print "and to /usr/local/flamingos/flamingos.fitsheader.lut/Flamingos.fitsheader\n";
    print "Instead copy to debug directories\n\n";
    my $cp_string = $ENV{"FITS_HEADER_LUT_DEBUG"} . "Flamingos.fitsheader";
    my $dothis = "cp -p $header " . $cp_string;
    print "Copying $header to $cp_string \n\n";
    system($dothis);

}#End of debug elseif

print "\n\nconfig.location.pl is done\n\n";

####End of main code

sub usage{

  my $usage = "
config.location.pl [-debug]   (optional, for testing)
                   -telescope (required choose one: kp2m, kp4m, mmt, or gem)";


  print "\nUSAGE:\n" . $usage . "\n\n";


}#endsub usage

__END__

=head1 NAME

config.location.pl

=head1 DESCRIPTION

Copies the correct template FITS header file onto the default
FITS header template file.  Be sure to do this before trying
to really observe.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.location.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
