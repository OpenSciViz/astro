package Dither_patterns;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Dither_patterns ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 
     'all' => [ 
     qw(
        @pat_5box_X    @pat_5box_Y @pat_5plus_X @pat_5plus_Y
        @pat_2x2_X     @pat_2x2_Y  @pat_3x3_X   @pat_3x3_Y
        @pat_4x4_X     @pat_4x4_Y  @pat_5x5_X   @pat_5x5_Y
	&select_from_known_patterns

        @pat_5box_rel_X    @pat_5box_rel_Y @pat_5plus_rel_X @pat_5plus_rel_Y
        @pat_2x2_rel_X     @pat_2x2_rel_Y  @pat_3x3_rel_X   @pat_3x3_rel_Y
        @pat_4x4_rel_X     @pat_4x4_rel_Y  @pat_5x5_rel_X   @pat_5x5_rel_Y

	@pat_5box_wrap_X_Y @pat_5plus_wrap_X_Y @pat_2x2_wrap_X_Y
	@pat_3x3_wrap_X_Y  @pat_4x4_wrap_X_Y   @pat_5x5_wrap_X_Y

	@pat_5box_rel_Xorigin  @pat_5box_rel_Yorigin
	@pat_5plus_rel_Xorigin @pat_5plus_rel_Yorigin
	@pat_2x2_rel_Xorigin   @pat_2x2_rel_Yorigin
	@pat_3x3_rel_Xorigin   @pat_3x3_rel_Yorigin
	@pat_4x4_rel_Xorigin   @pat_4x4_rel_Yorigin
	@pat_5x5_rel_Xorigin   @pat_5x5_rel_Yorigin

	@pattern_names         $num_pats
	@largest_start_positions  
	&get_pos_rel_start        &get_rel_pos
	&load_pattern_x           &load_pattern_y
	&load_pattern_rel_x       &load_pattern_rel_y
	&load_pat_rel_x_startpos &load_pat_rel_y_startpos
	&load_pattern_wrap
	
        &load_pat_rel_Xorigin    &load_pat_rel_Yorigin
	&scale_pattern_x_or_y    &scale_pattern_wrap
	&scale_pattern_msg
	&nudge_pointing
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.

#Absolute & Relative Offset Patterns
#Ppatterns start with origin to first position offset
#and end with last position to origin offset
our @pat_5box_X = (  0,  90, -90, -90, 90, 0 );
our @pat_5box_Y = (  0, -90,  90, -90, 90, 0 );

our @pat_5box_rel_X = ( 0,  90, -180   , 0, 180, -90 );
our @pat_5box_rel_Y = ( 0, -90,  180, -180, 180, -90 );
our @pat_5box_wrap_X_Y = ( 0, -180 );

our @pat_5box_rel_Xorigin = @pat_5box_X;
our @pat_5box_rel_Yorigin = @pat_5box_Y;
#--------------
our @pat_5plus_X = (  0, -90, 0, 90,   0, 0 );
our @pat_5plus_Y = ( 90,   0, 0,  0, -90, 0 );

our @pat_5plus_rel_X = (  0, -90, 90, 90, -90,  0 );
our @pat_5plus_rel_Y = ( 90, -90,  0,  0, -90, 90 );
our @pat_5plus_wrap_X_Y = ( 0, 180 );

our @pat_5plus_rel_Xorigin = @pat_5plus_X;
our @pat_5plus_rel_Yorigin = ( 0, -90, -90, -90, -180 );
#-------------
our @pat_2x2_X = (  90, -90, -90, 90, 0 );
our @pat_2x2_Y = ( -90,  90, -90, 90, 0 );

our @pat_2x2_rel_X = (  90, -180,    0, 180, -90 );
our @pat_2x2_rel_Y = ( -90,  180, -180, 180, -90 );
our @pat_2x2_wrap_X_Y = ( 0, -180 );

our @pat_2x2_rel_Xorigin = ( 0, -180, -180,   0 );
our @pat_2x2_rel_Yorigin = ( 0,  180,    0, 180 );
#-------------
our @count_3x3 = ( 0,   1,  2,   3,   4,   5,  6,  7,   8, 9 );
our @pat_3x3_X = ( 0, -90,  0, -90, -90,  90, 90, 90,   0, 0 );
our @pat_3x3_Y = ( 0,  90, 90, -90,   0, -90, 90,  0, -90, 0 );

our @pat_3x3_rel_X = ( 0, -90, 90,  -90,  0, 180,   0,   0, -90,  0 );
our @pat_3x3_rel_Y = ( 0,  90,  0, -180, 90, -90, 180, -90, -90, 90 );
our @pat_3x3_wrap_X_Y = ( 0, 90 );

our $ar_ref = get_rel_pos( \@pat_3x3_X );
our @pat_3x3_rel_Xorigin = @$ar_ref;
    $ar_ref = get_rel_pos( \@pat_3x3_Y );
our @pat_3x3_rel_Yorigin = @$ar_ref;
#-------------
our @count_4x4 = (   0,   1,   2,   3,   4,   5,   6,   7, 
		     8,   9,  10,  11,  12,  13,  14,  15, 16 );

our @pat_4x4_X = (  90, -45,  90, -45,  45, -45,  45,  45,
		    90, -90,  45, -90,  90, -45, -90, -90, 0 );

our @pat_4x4_Y = ( -90,  90,  90, -45, -90,  45,  45,  90,
		   -45, -90, -45, -45,  45, -90,  45,  90, 0 );

our @pat_4x4_rel_X = (   90, -135,  135, -135,   90, -90, 90,   0,   45,
		       -180,  135, -135,  180, -135, -45,  0,  90 );

our @pat_4x4_rel_Y = (  -90,  180,    0, -135,  -45, 135,  0,  45, -135,
		        -45,   45,    0,   90, -135, 135, 45, -90 );

our @pat_4x4_wrap_X_Y = ( 180, -180 );

$ar_ref = get_rel_pos( \@pat_4x4_X );
our @pat_4x4_rel_Xorigin = @$ar_ref;
$ar_ref = get_rel_pos( \@pat_4x4_Y );
our @pat_4x4_rel_Yorigin = @$ar_ref;
#-------------
our @count_5x5 = (   0,   1,   2,   3,   4,
		     5,   6,   7,   8,   9,
		    10,  11,  12,  13,  14,
		    15,  16,  17,  18,  19,
		    20,  21,  22,  23,  24, 25 );

our @pat_5x5_X = ( -45,   0,  45,  45,   0,
		   -90, -90,  90, -45,   0,
		   -45,   0,  45, -90, -45,
		   -90,  45, -90,  45, -45,
		    90,   0,  90,  90,  90, 0 );

our @pat_5x5_Y = (  45, -90,   0,  45,   0,
		     0,  90,  45,  90,  45,
		   -45, -45, -90,  45,   0,
		   -90,  90, -45, -45, -90,
		     0,  90, -45, -90,  90, 0 );

our @pat_5x5_rel_X = ( -45,   45,   45,    0, -45,
		       -90,    0,  180, -135,  45, 
		       -45,   45,   45, -135,  45, 
		       -45,  135, -135,  135, -90,
		       135,  -90,   90,    0,   0, -90 );

our @pat_5x5_rel_Y = (  45, -135,   90,   45, -45,
		         0,   90,  -45,   45, -45,
		       -90,    0,  -45,  135, -45,
		       -90,  180, -135,    0, -45,
		        90,   90, -135,  -45, 180, -90 );

our @pat_5x5_wrap_X_Y = ( -135, -45 );

$ar_ref = get_rel_pos( \@pat_5x5_X );
our @pat_5x5_rel_Xorigin = @$ar_ref;
$ar_ref = get_rel_pos( \@pat_5x5_Y );
our @pat_5x5_rel_Yorigin = @$ar_ref;
#-------------------------------------------------------
our @count_45rot_5x5 = (         0,       1,      2,       3,       4,
		                 5,       6,      7,       8,       9,
	                        10,      11,     12,      13,      14,
	                        15,      16,     17,      18,       19,
	                        20,      21,     22,      23,      24,      25 );

our @pat_5x5_45rot_X = (      0.00,  -63.64,  31.82,   63.64,    0.00,
              	            -63.64,    0.00,  95.46,   31.82,   31.82,
              	            -63.64,  -31.82, -31.82,  -31.82,  -31.82,
              	           -127.28,   95.46, -95.46,    0.00,  -95.46,
                             63.64,   63.64,  31.82,    0.00,  127.28,       0 );

our @pat_5x5_45rot_Y = (     63.64,  -63.64, -31.82,    0.00,    0.00,
	                     63.64,  127.28, -31.82,   95.46,   31.82,
                              0.00,  -31.82, -95.46,   95.46,   31.82,
                              0.00,   31.82,  31.82,  -63.64,  -31.82,
                            -63.64,   63.64, -95.46, -127.28,    0.00,       0 );

our @pat_5x5_rel_45rot_X = (  0.00,  -63.64,  95.46,   31.82,  -63.64,
                            -63.64,   63.64,-159.10,  -63.64,    0.00,
                            -95.46,   31.82,   0.00,    0.00,    0.00,
                            -95.46,  222.74,-190.92,   95.46,  -95.46,
                            159.10,    0.00, -31.82,  -31.82,  127.28, -127.28 );

our @pat_5x5_rel_45rot_Y = ( 63.64, -127.28,  31.82,   31.82,    0.00,
	                     63.64,   63.64, -95.46,   63.46,  -63.64,
                            -31.82,  -31.82, -63.64,  190.92,  -63.64,
                            -31.82,   31.82,   0.00,  -95.46,   31.82,
                            -31.82,  127.28, -159.10, -31.82,  127.28,    0.00 );

our @pat_5x5_wrap_45rot_X_Y = ( -127.28, 63.64 );

$ar_ref = get_rel_pos( \@pat_5x5_45rot_X );
our @pat_5x5_45rot_rel_Xorigin = @$ar_ref;
$ar_ref = get_rel_pos( \@pat_5x5_45rot_Y );
our @pat_5x5_45rot_rel_Yorigin = @$ar_ref;
#-------------------------------------------------------
our @pattern_names = qw( pat_2x2 pat_3x3 pat_4x4 pat_5x5
			 pat_5box pat_5plus pat_45rot_5x5);

our @array_known_patterns = qw( 2x2 3x3 4x4 5x5 5box 5plus 45rot_5x5);

our $num_pats = @array_known_patterns;
our @largest_start_positions = ( @pat_2x2_X-2,       @pat_3x3_X-2,
			         @pat_4x4_X-2,       @pat_5x5_X-2,
			         @pat_5x5_45rot_X-2, @pat_5x5_45rot_Y-2,
			         @pat_5box_X-2,      @pat_5plus_X-2
			       );

sub select_from_known_patterns {
  print "\n";
  print "The following patterns are known\n";
  print "Pattern # = pattern shape\n";
  for( my $i = 0; $i < $num_pats; $i++){
    print "Pattern $i = $array_known_patterns[$i]\n";
  }
  print "\n";
  print "5box is 2x2 and the original pointing center.\n";
  print "5plus is the 4 endpoints of a + sign and the original ".
        "pointing center.\n\n";
  print "The maximum offset is " . (90 * $ENV{DEFAULT_DITHER_SCALE}).
        " arcseconds\n";
  print "(A 'picture frame' this wide is added to \n".
        "the coverage about the pointing center)\n\n";

  print "Select a pattern by entering the pattern #: ";

  my $response = 0;
  my $is_valid = 0;
  until( $is_valid ){
    my $new_value = <STDIN>; chomp $new_value;
    print "You entered $new_value\n";

    my $input_len = length $new_value;
    if( $input_len > 0 ){

      if( $new_value =~ /\D+/ ){
	print  "Please enter an integer between 0 and ".
	       ($num_pats - 1)." ";
      }else{
	if( $new_value >= 0 and $new_value < $num_pats ){
	  $response = $pattern_names[$new_value];
	  $is_valid = 1;
	  print "The pattern selected is $response\n";
	}else{
	  print "Enter a number between 0 and ".($num_pats - 1)." ";
	}
      }
    }else{
      print "Leaving DPATTERN unchanged\n";
      $is_valid = 1;
    }
  }
  return $response;
}#Endsub select_from_known_patterns


sub get_rel_pos{
  my $ar_vector = $_[0];
  my @in_vector = @$ar_vector;
  my $first_pos = $in_vector[0];

  my @out_vector;
  for( my $i = 0; $i < @in_vector; $i++ ){
    $out_vector[$i] = $in_vector[$i] - $first_pos;
  }
  return \@out_vector;
}#Endsub get_rel_pos


sub get_pos_rel_start{
  my ( $start_pos_index, $ar_vector ) = @_;
  my @in_vector = @$ar_vector;
  my $first_pos = $in_vector[$start_pos_index];

  my @out_vector;
  for( my $i = 0; $i < @in_vector; $i++ ){
    $out_vector[$i] = $in_vector[$i] - $first_pos;
  }

  return @out_vector;
}#Endsub get_pos_rel_start


sub load_pattern_x{
  my $pat_name = $_[0];
  my @ret_array;

  #SELECT absolute offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_X;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    @ret_array = @pat_3x3_X;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_X;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_X;

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = @pat_5x5_45rot_X;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_X;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_X;
  }

  return @ret_array;
}#Endsub load_pattern_x


sub load_pattern_y{
  my $pat_name = $_[0];
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = @pat_3x3_Y;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_Y;

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = @pat_5x5_45rot_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_Y;
  }

  return @ret_array;
}#Endsub load_pattern_y


sub load_pattern_rel_x{
  my $pat_name = $_[0];
  my @ret_array;

  #SELECT absolute offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_rel_X;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    @ret_array = @pat_3x3_rel_X;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_rel_X;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_rel_X;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_rel_45rot_X;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_rel_X;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_rel_X;
  }

  return @ret_array;
}#Endsub load_pattern_rel_x


sub load_pattern_rel_y{
  my $pat_name = $_[0];
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_rel_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = @pat_3x3_rel_Y;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_rel_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_rel_Y;

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = @pat_5x5_rel_45rot_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_rel_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_rel_Y;
  }

  return @ret_array;
}#Endsub load_pattern_rel_y


sub load_pat_rel_x_startpos{
  my ( $startpos, $pat_name ) = @_;
  my @ret_array;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_2x2_X );

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_3x3_X );

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_4x4_X );

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_X );

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_45rot_X );

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5box_X );

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5plus_X );
  }

  return @ret_array;
}#Endsub load_pat_rel_x_startpos


sub load_pat_rel_y_startpos{
  my ( $startpos, $pat_name ) = @_;
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_2x2_Y );

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_3x3_Y );

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_4x4_Y );

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_Y );

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_45rot_Y );

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5box_Y );

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5plus_Y );
  }

  return @ret_array;
}#Endsub load_pat_rel_y_startpos


sub load_pattern_wrap{
  my $pat_name = $_[0];
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_wrap_X_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = @pat_3x3_wrap_X_Y;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_wrap_X_Y;

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = @pat_5x5_wrap_45rot_X_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_wrap_X_Y;
  }

  return @ret_array;
}#Endsub load_pattern_wrap


sub load_pat_rel_Xorigin{
  my $pat_name = $_[0];
  my @ret_array;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_rel_Xorigin;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    #print "found xpat of 3x3\n";
    @ret_array = @pat_3x3_rel_Xorigin;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_rel_Xorigin;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_rel_Xorigin;

  }elsif( $pat_name eq 'pat_45rot_5x5     ' ){
    @ret_array = @pat_5x5_45rot_rel_Xorigin;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_rel_Xorigin;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_rel_Xorigin;
  }

  return @ret_array;
}#Endsub load_pat_rel_Xorigin


sub load_pat_rel_Yorigin{
  my $pat_name = $_[0];
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_rel_Yorigin;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = @pat_3x3_rel_Yorigin;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_rel_Yorigin;

  }elsif( $pat_name eq 'pat_45rot_5x5           ' ){
    @ret_array = @pat_5x5_45rot_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_rel_Yorigin;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_rel_Yorigin;
  }

  return @ret_array;
}#Endsub load_pat_rel_Yorigin


sub scale_pattern_x_or_y{
  my ( $ar_x_or_y, $net_scale_factor ) = @_;
  my @x_or_y = @$ar_x_or_y;

  my $pat_len = @x_or_y;
  for( my $i = 0; $i < $pat_len; $i++ ){
    $x_or_y[$i] = $x_or_y[$i] * $net_scale_factor;
  }

  return( @x_or_y );
}#Endsub scale_pattern


sub scale_pattern_wrap{
  my ( $ar_wrap, $net_scale_factor ) = @_;
  my @wrap = @$ar_wrap;

  $wrap[0] = $$ar_wrap[0] * $net_scale_factor;
  $wrap[1] = $$ar_wrap[1] * $net_scale_factor;

  return @wrap;
}#Endsub scale_pattern_wrap


sub scale_pattern_msg{
  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $extra_dither_scale   = Fitsheader::param_value( 'D_SCALE' );
  my $net_factor = $default_dither_scale * $extra_dither_scale;

  print "The extra scale factors for $ENV{THIS_TELESCOP}:\n";
  print "Default dither scale    = $default_dither_scale\n";
  print "Extra dither scale      = $extra_dither_scale\n";
  print "Net picture frame width = ".($net_factor*90)." arcseconds\n\n";
}#Endsub scale_pattern_msg


sub nudge_pointing{
  my ( $d_rptpat, $rpt_pat, $usenudge, $nudgesiz ) = @_;
  my $rndm_num = rand;
  my $rdx = 0; my $rdy = 0; 

  if( $usenudge ){
    if( $rpt_pat >= 1 ){
      if( 0 <= $rndm_num and $rndm_num < 0.25 ){
	$rdx = 0; $rdy = 1;
      }elsif( 0.25 <= $rndm_num and $rndm_num < 0.5 ){
	$rdx = 1; $rdy = 0;
      }elsif( 0.50 <= $rndm_num and $rndm_num < 0.75 ){
	$rdx = 0; $rdy = -1;
      }elsif( 0.75 <= $rndm_num and $rndm_num <= 1.0 ){
	$rdx = -1; $rdy = 0;
      }
    }
    $rdx = $rdx * $nudgesiz;
    $rdy = $rdy * $nudgesiz;
  }
  print "\nNudge offsets by $rdx, $rdy\n\n";
  return( $rdx, $rdy );
}#Endsub nudge_pointing


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Dither_patterns - Perl extension for Flamingos

=head1 SYNOPSIS

  use Dither_patterns qw/:all/;
  

=head1 DESCRIPTION

Dither patterns in terms of absolute offset from pointing center 
for KPNO & Gemini-South, relative offsets if at MMTO. (Still need
to enter those patterns).

Scale of patterns is for KPNO 2.1m, with 90 arcsecond max offsets.
See also .login-gemini

=head2 EXPORT

@pat_5box_X    @pat_5box_Y @pat_5plus_X @pat_5plus_Y
@pat_2x2_X     @pat_2x2_Y  @pat_3x3_X   @pat_3x3_Y
@pat_4x4_X     @pat_4x4_Y  @pat_5x5_X   @pat_5x5_Y

&list_known_patterns


=head1 REVISION & LOCKER

$Name:  $

$Id: Dither_patterns.pm,v 0.1 2003/05/22 15:16:46 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR 27 Aug 2001

=head1 SEE ALSO

perl(1).

=cut
