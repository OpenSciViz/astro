#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;

my $stop_take  = 0;
my $clean_take = 0;

GetOptions ('stop' => \$stop_take, 'clean' => \$clean_take);

if( $stop_take == 0 and  $clean_take == 0){
  usage();
}

if( $stop_take){
  #print "uffrmstatus is \n";
  system("uffrmstatus");
  print "\n";
  
  print "Issuing stop command to MCE4 daemon ";
  print "and resetting uffrmstatus to idle: false, applyingLUT: false\n";
  #my $stop_cmd = 'ufdc -acq "stop"';
  my $space = " ";
  my $ufdc_cmd = $ENV{UFDC}.$space.$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                 $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
		 $ENV{CLIENT_QUIET}.$space.$ENV{UFDC_ACQ}.$space;

  my $stop_cmd = $ufdc_cmd.' "stop"';
  print "SHOULD BE EXECUTING:\n$stop_cmd\n";  
  system($stop_cmd);
  system("uffrmstatus");
  print "\n\n";

  system("ufidle.pl");

}

if( $clean_take){
  print "Cleaning take\n\n";

  print "unlinking /var/tmp/.SEMD/uf* ";
  my $unlink_rval = unlink </var/tmp/.SEMD/uf*>;
  print "... removed $unlink_rval semaphore files\n";

  print "removing directory /var/tmp/.SEMD\n";
  my $rmdir_rval = rmdir </var/tmp/.SEMD>;


  print "unlinking /var/tmp/.SEML/* ";
  $unlink_rval = unlink </var/tmp/.SEML/*>;
  print "... removed $unlink_rval semaphore files\n";

  print "removing directory /var/tmp/.SEML\n";
  $rmdir_rval = rmdir </var/tmp/.SEML>;


  my $reset_semaphores = "ufgtake -cnt 1 -timeout 1 -file /var/tmp/foo";
  system( $reset_semaphores );


}



sub usage{

  my $usage = "
ufstop.pl [-stop]  (Try this first.)
                   (Stops the current take and resets uffrmstatus.)

          [-clean] (Try this if -stop does not seem to help.
                   (Removes /var/tmp/.SEMD and /var/tmp/.SEML.)";

  print "\nUSAGE:\n" . $usage . "\n\n";

}#endsub  usage

__END__

=head1 NAME

ufstop.pl

=head1 DESCRIPTION


ufstop.pl -clean -stop

-clean resets some semaphores for the uflib MCE-4 demon.

-stop sends a stop command to MCE4.


=head1 REVISION & LOCKER

$Name:  $

$Id: ufstop.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

