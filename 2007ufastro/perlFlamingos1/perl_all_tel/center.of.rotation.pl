#!/usr/local/bin/perl -w

use strict;
use constant PI => 4 * atan2 1, 1;

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


print "\n";
print "Compute the center of rotation on the array, given the centroid postions\n";
print "of the same star taken at three diferent rotator angles.  A small range of\n";
print "angles, say 30 degrees between the first and last images should be sufficient.\n\n";
print "If at the MMT or Gemini, please take the data while the rotator is tracking---\n";
print "so use the _instrument pa_ to change the angle of the the chip on the sky.\n";
print "\n";

print "Enter the coordinates of the star at the first position:\n";
my ($x1, $y1) = input_coords();

print "Enter the coordinates of the star at the second position:\n";
my ($x2, $y2) = input_coords();

print "Enter the coordinates of the star at the third position:\n";
my ($x3, $y3) = input_coords();

my $rot = input_rotangle();
print "\n";

#print "x1, y1 = $x1, $y1\n";
#print "x2, y2 = $x2, $y2\n";
#print "x3, y3 = $x3, $y3\n";

# Location of midpoint between point 1 and 3
my $Mx = ($x3 + $x1)/2;
my $My = ($y3 + $y1)/2;

if( $rot eq 180 ){ solution_180();}

# Length from point 1 to midpoint, M
my $M1_len = 0.5 * sqrt( ($x3 - $x1)*($x3 - $x1) + ($y3 - $y1)*($y3 - $y1) );
#print "M = ($Mx, $My), len = $M1_len\n";

# Compute isosceles triangle height and radius of circle
my ($h_len, $radius) = compute_h_and_r();
#print "isosceles triangle height = $h_len\n";

#Compute angle from point 1 to 3
my $gamma = compute_gamma();
#print "angle gamma (rad) = $gamma\n";
#print "angle gamma (deg) = ".($gamma * 180 / PI)."\n";

#Compute vector of length h, parallel to line between points 1 and 3
my $hx = $h_len * cos $gamma;
my $hy = $h_len * sin $gamma;
#print "h = ($hx, $hy)\n";

#Rotate h by plus 90, for solution 1
my $hx_p90 = -1 * $h_len * sin $gamma;
my $hy_p90 =      $h_len * cos $gamma;

#Rotate h by neg 90, for solution 2
my $hx_n90 =      $h_len * sin $gamma;
my $hy_n90 = -1 * $h_len * cos $gamma;

#Translate rotate h vector by vector to point M, for the two solutions:
my $x_cen_1 = $Mx + $hx_p90; $x_cen_1 = sprintf "%0.3f", $x_cen_1;
my $y_cen_1 = $My + $hy_p90; $y_cen_1 = sprintf "%0.3f", $y_cen_1;

my $x_cen_2 = $Mx + $hx_n90; $x_cen_2 = sprintf "%0.3f", $x_cen_2;
my $y_cen_2 = $My + $hy_n90; $y_cen_2 = sprintf "%0.3f", $y_cen_2;

my ($c1_p2_len, $c2_p2_len) = suggested_center();
my $suggested_center;
if( $c1_p2_len > $c2_p2_len ){
  $suggested_center = "Solution 1";
}elsif( $c2_p2_len > $c1_p2_len ){
  $suggested_center = "Solution 2";
}

print "\n";
print "The two solutions that we come up with:\n";
print "Solution 1: ($x_cen_1, $y_cen_1)\n";
print "Solution 2: ($x_cen_2, $y_cen_2)\n";
print "\n";
print "The radius of circle that includes these points = $radius\n";
print "The distance between point 2 and solution 1 = $c1_p2_len\n";
print "The distance between point 2 and solution 2 = $c2_p2_len\n";
print "The center of rotation is most likely given by $suggested_center\n";
print "\n";

##################################
#SUBS
sub compute_gamma{
  my $delta_y = $y3 - $y1;
  my $delta_x = $x3 - $x1;
  my $gamma;

  if( $delta_x != 0 ){
    $gamma = atan2 $delta_y,  $delta_x;

  }elsif( $delta_x == 0 ){
    $gamma = 0;
  }

  return $gamma;
}#Endsub compute_gamma


sub compute_h_and_r{

  my $half_rot = 0.5 * $rot * PI / 180;
  my $term1 = sin( PI/2 - $half_rot );
  my $term2 = sin( $half_rot );

  my $h = ($term1 / $term2) * $M1_len;

  my $radius = $h / $term1; $radius = sprintf "%0.3f", $radius;

  return ($h, $radius);
}#Endsub compute_h


sub input_rotangle{
  my $rot_response = 0;

  my $is_valid = 0;
  until($is_valid){
    print "Enter the angle (in degrees) rotated between the first and last image: ";

    $rot_response = <STDIN>;
    chomp( $rot_response );

    if( $rot_response =~ m/[a-z,A-Z]/ ){
      print "Please enter a real value.\n";

    }elsif( $rot_response =~ m/^\.$/ ){
      print "Please enter a real value.\n";

    }else{
      if( $rot_response =~ m/\d{0}\.\d{1}/ || $rot_response =~ m/\d{1}/ ){
	#look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	$is_valid = 1;
      }
    }
  }

  return $rot_response;
}#Endsub input_rotangle


sub input_coords{

  my $x_response = 0;
  my $y_response = 0;

  my $is_valid = 0;
  until($is_valid){
    print "Enter the x coordinate: ";

    $x_response = <STDIN>;
    chomp( $x_response );

    if( $x_response =~ m/[a-z,A-Z]/ ){
      print "Please enter a real value.\n";

    }elsif( $x_response =~ m/^\.$/ ){
      print "Please enter a real value.\n";

    }else{
      if( $x_response =~ m/\d{0}\.\d{1}/ || $x_response =~ m/\d{1}/ ){
	#look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	$is_valid = 1;
      }
    }
  }

  $is_valid = 0;
  until($is_valid){
    print "Enter the y coordinate: ";

    $y_response = <STDIN>;
    chomp( $y_response );

    if( $y_response =~ m/[a-z,A-Z]/ ){
      print "Please enter a real value.\n";

    }elsif( $y_response =~ m/^\.$/ ){
      print "Please enter a real value.\n";

    }else{
      if( $y_response =~ m/\d{0}\.\d{1}/ || $y_response =~ m/\d{1}/ ){
	#look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	$is_valid = 1;
      }
    }
  }

  my $formated_x = sprintf "%0.3f", $x_response;
  my $formated_y = sprintf "%0.3f", $y_response;
  print "\n";
  return ($formated_x, $formated_y);
}#Endsub input_coords


sub solution_180{
  
}#Endsub solution_180


sub suggested_center{
  #Pick suggested center based on length from point 2 and radius

  my $c1_p2_len = sqrt( ($x_cen_1 - $x2)*($x_cen_1 - $x2) + ($y_cen_1 - $y2)*($y_cen_1 -$y2) );
  my $c2_p2_len = sqrt( ($x_cen_2 - $x2)*($x_cen_2 - $x2) + ($y_cen_2 - $y2)*($y_cen_2 -$y2) );

  $c1_p2_len = sprintf "%0.3f", $c1_p2_len;
  $c2_p2_len = sprintf "%0.3f", $c2_p2_len;

  #print "C1 to p2 length = $c1_p2_len\n";
  #print "C2 to p2 length = $c2_p2_len\n";

  return ( $c1_p2_len, $c2_p2_len );
}#Endsub suggested_center
