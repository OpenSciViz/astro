#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage: gem.guide.pl on|off\n\n";

}else{
  my $state = shift;

  if( $state =~ m/^on$/i) {
    print "Turniing guiding on\n";
    my $soc = ufgem::viiconnect();
    ufgem::guideOn($soc);
    close( $soc );
    
  }elsif( $state =~ m/^off$/i) {
    print "Turning guiding off\n";
    my $soc = ufgem::viiconnect();
    ufgem::guideOff($soc);
    close( $soc );
    
  }else{
    die "\n\n\tPlease enter gem.guide.pl on or gem.guide.pl off\n\n";
    
  }
  
}#End


__END__

=head1 NAME

gem.guide.pl

=head1 Description

gem.guide.pl <on | off>

Turn on or off guiding.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.guide.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
