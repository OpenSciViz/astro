#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use ufgem qw/:all/;


my $cnt = @ARGV;
my $reply;

if( $cnt < 2 ) { # assume -help
   die "\n\tusage probeGuideConfig.pl (p1|p2) (on|off)\n\n";

}
else {
  my $probe_name = shift;
  my $state = shift;
  
  if( $probe_name =~ m/^p1$/i or $probe_name =~ m/^p2$/i ){
    if( $state =~ m/^on$/i or $state =~ m/^off$/i ){
      my $soc = ufgem::viiconnect();
      ufgem::probeGuideConfig( $soc, $probe_name, $state );
      close( $soc );

    }else{
      die "\n\tYou didn't enter either on or off for ".
	  "the probe state\n\n";
    }
  }else{
      die "\n\tYou didn't enter either p1 or p2 for ".
	  "the probe name\n\n";
  }

}#End

__END__

=head1 NAME

gem.probeGuideConfig.pl

=head1 Description

Pick the guide probe to use, and turn on and off guiding.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.probeGuideConfig.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
