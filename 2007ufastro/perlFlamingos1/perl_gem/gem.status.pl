#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $val;
my $cnt = @ARGV;
if( $cnt < 1 ) {
  $val = ufgem::getStatus($soc); # get All

}else {
  my $stat = shift;
  $val = ufgem::getStatus($soc, $stat);
}

print "$val";

close($soc);


__END__

=head1 NAME

gem.status.pl

=head1 Description

Get the TCS info and print it to the screen.

=head1 REVISION & LOCKER

$Name:  $

$Id: gem.status.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
