package getGemSouthTCSinfo;

require 5.005_62;
use strict;
use warnings;

use Fitsheader qw/:all/;
use ufgem qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use getGemSouthTCSinfo ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( &get_gem_tcs_info
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
our @numeric_header_keywords =
  qw( 
     AIRMASS  TELFOCUS  HA  HUMID  INSTRAA  INSTRPA  MJD  
     DEC_OFF    RA_OFF  ROTATOR  ROTERROR
     RA_BASE  DEC_BASE  EPOCH  EQUINOX  
     USRFOC  ZD
    );

our @returned_numeric_keywords =
  qw( 
     airmass  focus   ha  humidity  instraa  instrpa  mjd  
     offsetdec  offsetra  rotator  roterror
     targetra  targetdec  targetepoch  targetequinox
     userfocus  zd
    );

#our @string_header_keywords =
#  qw( 
#     AZIMUTH  AZERROR   EL  ELERROR  FRAMEPA  LOCTIME  LST  
#     OBJECT  TARGFRM  TARGSYS  RA_TEL  DEC_TEL  UTC  
#    );
our @string_header_keywords =
  qw( 
     AZIMUTH  AZERROR   EL  ELERROR  FRAMEPA  LOCTIME  LST  
     OBJECT  TARGFRM  TARGSYS  RA_TEL  DEC_TEL  UTC  UTCDATE
    );

our @returned_string_keywords =
  qw( 
     azimuth  azerror  elevation  elerror  framepa  localTime  lst  
     targetname  targetframe  targetradecsys  telra  teldec  utc utcdate 
    );


sub get_gem_tcs_info{
  my $header = $_[0];
  print "header passed to getGemSouthTCSinfo.pm is $header\n";

  
  my $soc = ufgem::viiconnect();


  ###> The arrays with the final output from the tcs, 
  ###> formatted for the FITS header.
  my @numeric_key_vals;
  my @string_key_vals;
  
  
  get_numeric_vals( \@returned_numeric_keywords, \@numeric_key_vals, $soc );
  format_numeric_vals(\@numeric_key_vals);
  
  get_string_vals( \@returned_string_keywords, \@string_key_vals, $soc );
  
  print_numeric_vals( \@numeric_header_keywords,
		      \@returned_numeric_keywords,
		      \@numeric_key_vals );
  
  print_string_vals( \@string_header_keywords,
		     \@returned_string_keywords,
		     \@string_key_vals );

}#Endsub get_gem_tcs_info

###> Non-exported subs
sub get_numeric_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my @returned_numeric_keywords = @$aref1;
  # $aref2 will be the actual array of final values = @numeric_key_vals

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_num = @returned_numeric_keywords;
  
  for( my $i = 0; $i < $num_num; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_numeric_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_numeric_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$numeric_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }

}#Endsub get_numeric_vals


sub get_string_vals{
  my ( $aref1, $aref2, $soc ) = @_;
  my $returned_string_keywords = @$aref1;

  my $got_str = "got ";
  my $len_got_str = length $got_str;

  my $num_str = @returned_string_keywords;
  
  for( my $i = 0; $i < $num_str; $i++){
    my $reply = ufgem::getStatus( $soc, $returned_string_keywords[$i] );
    chomp $reply;
    {
      local $/ = "\r"; chomp $reply;
    }
    #get rid of 'got ', then get rid of tcs keyword
    substr $reply, 0, $len_got_str, "";
    my $len_keyword = length $returned_string_keywords[$i];
    substr $reply, 0, $len_keyword, "";
    #$string_key_vals[$i] = $reply;
    $$aref2[$i] = $reply;
  }
}#Endsub get_string_values


sub format_numeric_vals{
  my $aref = $_[0];
  my @numeric_key_vals = @$aref;
  #NUMERIC values:
  #recast output as float with same precision,
  #and right justified to 20 char long.

  my $num_num = @numeric_key_vals;
  for( my $i = 0; $i < $num_num; $i++ ){
    
    my $item = $numeric_key_vals[$i];
    my $len_val = length $item;
    if( $len_val > 20 ){
      #this one's for mjd which is a long exponential format number
      $item = sprintf "%.20f", $numeric_key_vals[$i];
    }
    my $loc_dec = index $item, ".";
    
    #print "$i: item = $item; len = $len_val; dec loc = $loc_dec\n";
    
    my $recast_format;
    if( $len_val > 20 ){
      $recast_format = "%20." . (20 - $loc_dec - 1) . "f";
    }else{
      $recast_format = "%20." . ($len_val - 1 - $loc_dec) . "f";
    }
    my $recast_val = sprintf $recast_format, $item;
    
    my $len_recast_val = length $recast_val;
    
    #if( $i == 0 ){
    #  print "---------------12345678901234567890----------\n";
    #}
    #print "$i: recast_val =$recast_val, leng = $len_recast_val\n";
    $$aref[$i] = $recast_val;
  }

}#Endsub format_numeric_vals

sub print_numeric_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @numeric_header_keywords   = @$aref1;
  my @returned_numeric_keywords = @$aref2;
  my @numeric_key_vals          = @$aref3;

  my $num_num = @returned_numeric_keywords;
  print "\n\nNumeric Values\n";
  for( my $i = 0; $i < $num_num; $i++ ){
    #  print "$i = $numeric_header_keywords[$i] =\t ".
    #    "$returned_numeric_keywords[$i] =12345678901234567890\n";
    print "$i = $numeric_header_keywords[$i] =\t ".
      "$returned_numeric_keywords[$i] =$numeric_key_vals[$i]\n";
  }
  
}#End print_numeric_vals


sub print_string_vals{
  my ( $aref1, $aref2, $aref3 ) = @_;
  my @string_header_keywords   = @$aref1;
  my @returned_string_keywords = @$aref2;
  my @string_key_vals          = @$aref3;

  my $num_str = @returned_string_keywords;
  print "\n\nString Values\n";
  for( my $i = 0; $i < $num_str; $i++ ){
    print "$i = $string_header_keywords[$i] =\t ".
      "$returned_string_keywords[$i] =$string_key_vals[$i]\n";
  }
  print "\n\n";

}#Endsub print_string_vals



#print "\n\nNumeric Values\n";
#for( my $i = 0; $i < $num_num; $i++ ){
#  print "$i = $numeric_header_keywords[$i] =\t ".
#    "$returned_numeric_keywords[$i] =$numeric_key_vals[$i]\n";
#}


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

getGemSouthTCSinfo - Perl extension for Flamingos

=head1 SYNOPSIS

  use getGemSouthTCSinfo;
 

=head1 DESCRIPTION

Requires that Fitsheader.pm and ufgem.pm be installed
queries vii for tcs info, and then sticks the data into 
the present header.

=head2 EXPORT

get_gem_tcs_info( $header );

Where $header is the absolute path and filename of the 
present header.

=head1 AUTHOR


=head1 REVISION & LOCKER

$Name:  $

$Id: getGemSouthTCSinfo.pm,v 0.1 2003/05/22 15:40:45 raines Exp $

$Locker:  $


=head1 SEE ALSO

perl(1).

=cut
