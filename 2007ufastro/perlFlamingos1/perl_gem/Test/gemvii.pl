#!/usr/bin/perl

my $rcsId = q($Name:  $ $Id: gemvii.pl 14 2008-06-11 01:49:45Z hon $);

use IO::Socket;

$host = `hostname`; chomp $host;
print "start gemvii using port 7283 on $host\n";
$servsoc = IO::Socket::INET->new( LocalAddr => $host,
				  LocalPort => 7283,
				  Proto => 'tcp',
				  Type => SOCK_STREAM,
				  Listen => 5,
				  ReuseAddr => 1,
				  ReusePort => 1 );

die "Server socket creation failed: $!" unless defined($servsoc);

while( $clntsoc = $servsoc->accept() ) {
  while( defined( $buf = <$clntsoc> ) ) {
    $date = `date`; chomp $date;
    print "Received ( $date ): $buf";
    print $clntsoc "Ok( $date ): $buf";
  }
}

close( $servsoc );
