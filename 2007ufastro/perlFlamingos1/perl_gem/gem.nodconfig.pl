#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 2 ) {
  #print help message
  die "\n\n\tUsage:  gem.nodconfig.pl throw (arcsec) pa (deg)\n\n";

}else{
  my $throw = shift;
  my $pa    = shift;

  if( $throw =~ m/[a-zA-Z]/ or $pa =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical throw or pa by mistake?".
        "\n\nthrow = $throw; pa = $pa\n\n";

  }else{
    my $soc = ufgem::viiconnect();
    ufgem::nodConfig($soc, $throw, $pa);
    close( $soc );
  }

}#End


__END__

=head1 NAME

gem.nodconfig.pl

=head1 Description

Use to setup location of the nod beam wrt
the present position in terms of an angle
and a distance, or throw.

=head1 REVISION & LOCKER

$Name:  $

$Id: gem.nodconfig.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
