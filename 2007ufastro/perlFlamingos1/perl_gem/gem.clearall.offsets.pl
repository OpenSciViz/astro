#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use GetYN;
use ufgem qw/:all/;

print "\n";
print "The offsets shown on the TSD are the net sum of two types of offsets:\n";
print "\n";
print "  1) offsets       (e.g., executed with gem.offset.pl or gem.handset.pl)\n";
print "  2) offsetAdjusts (e.g., executed with gem.offsetAdjust.instrument.pl)\n";
print "\n";
print "This will move the telescope so the both the offsets & the offsetAdjusts are (0,0)\n";
print "The telescope will move to where the net offset if (0,0)\n";
print "\n";

GetYN::query_ready();

my $soc = ufgem::viiconnect();

ufgem::clear_adjusts( $soc );
ufgem::clear_offsets( $soc );

close( $soc );

__END__

=head1 NAME

gem.clearall.offsets.pl

=head1 Description

Moves the telescope back to position where
all the possible types of offsets are at (0, 0).

=head1 REVISION & LOCKER

$Name:  $

$Id: gem.clearall.offsets.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
