#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use KPNO_offsets qw/:all/;
use Math::Round qw/ round /;

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

main::print_msg();
GetYN::query_ready();

print "Need to clear all previous lock positions,\n";
print "i.e. the A & B beams will have to be redefined.\n";
GetYN::query_ready();

Gdr_Rpc::clear_all_lock_positions( $gdrrpc );
print "--------------><-------------\n";
print "Should have cleared any and all lock positions.\n";
print "Now for the next step.  ";
GetYN::query_ready();

my ($this_x, $this_y) = Gdr_Rpc::get_current_lock_position( $gdrrpc );
sleep( 1 );
print "--------------><-------------\n";
print "Should have acquired current lock postion.\n";
print "Now for the next step.  ";
GetYN::query_ready();

my ($x_cor, $y_cor)   = Gdr_Rpc::get_guide_correction( $gdrrpc );
sleep( 1 );
print "--------------><-------------\n";
print "Should have acquired the current guide corrections.\n";
print "Now for the next step.  ";
GetYN::query_ready();

my $guide_center_x = $this_x + $x_cor;
my $guide_center_y = $this_y + $y_cor;

print "Got current box center and corrections:\n";
print "---------------------------------------\n";
print "x-position + correction = $this_x + $x_cor = $guide_center_x\n";
print "y_position + correction = $this_y + $y_cor = $guide_center_y\n";
print "\n";

print "--------------><-------------\n";
#print "Now turning off guiding in order to set lock position number 1.\n";
#Gdr_Rpc::guide_off( $gdrrpc );
#sleep 1;
#print "--------------><-------------\n";
#print "Should have disabled guiding.\n";
#print "Now to set the lock position.  ";
#GetYN::query_ready();

Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "1", $guide_center_x, $guide_center_y );
sleep 1;
print "--------------><-------------\n";
print "Should have set the lock position.\n";
print "Now to select the guide position.  ";
GetYN::query_ready();

Gdr_Rpc::select_guide_pos1( $gdrrpc );
sleep 1;
print "--------------><-------------\n";
print "Should have selected guide position number 1\n";
print "Now to start guiding at lock position number 1.  ";
GetYN::query_ready();

#Gdr_Rpc::guide_on( $gdrrpc );
#sleep 1;
#print "--------------><-------------\n";
#print "Should have started guiding in lock position number 1.\n";
print "Now to request the current guide position and then mark it.  ";
GetYN::query_ready();

my ($slit_box_x, $slit_box_y) = Gdr_Rpc::get_current_lock_position( $gdrrpc );
my $round_box_x = Math::Round::round( $slit_box_x );
my $round_box_y = Math::Round::round( $slit_box_y );
print "--------------><-------------\n";
print "Should have requested the current defined lock position.  ";
GetYN::query_ready();

my $flipped_y = Gdr_Rpc::flip_y( $round_box_y );
Gdr_Rpc::mark_current_position( $gdrrpc, $round_box_x, $flipped_y );
print "--------------><-------------\n";
print "Should have marked the current position with values $round_box_x, $flipped_y (marker coordinates).\n";
print "Now to write the guide box locations to the header.  ";
GetYN::query_ready();

print "\n";
print "Writing guide box locations to header keywords.\n";
print "GPOS1_X = $slit_box_x\n";
print "GPOS1_Y = $slit_box_y\n";
print "\n";
Fitsheader::setNumericParam( 'GPOS1_X', $slit_box_x );
Fitsheader::setNumericParam( 'GPOS1_Y', $slit_box_y );
#print "SHOULD BE WRITING FITS HEADER\n";
Fitsheader::writeFitsHeader( $header );
print "\n";

#print "SHOULDB BE clearing offset\n";
print "\n";
print "The mos dither script requires the slit center have zero offsets.\n";
print "Clear the present offsets (i.e. set to zero)?  ";
my $reply = GetYN::get_yn();
if( !$reply ){
  die "Exiting\n";
}else{
  KPNO_offsets::clear_offsets();
}


### SUBS
sub print_msg{
  print "\n\n";
  print "\tThis script will define lock position 1 at the current position,\n";
  print "\twhich should have the alignment stars well placed in their boxes,\n";
  print "\tand the Mos objects in the center of their slits.  A marker will\n";
  print "\talso be placed on the screen.\n";
  print "\n";
  print "\tThe present offsets will be zeroed.\n";
  print "\tGuiding will be turned off to define the position, and\n";
  print "\then turned on after the position is defined.\n";
  print "\n";
  print "\tThe mos dither script will use lock positions 2 & 3 for the A\n";
  print "\tand B beams.\n";
}#Endsub print_msg


__END__

=head1 NAME

guide.define.slit.center.kp2m.test.pl

=head1 Description

An alternative version.
Do not know which one is preferable to use.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.define.slit.center.kp2m.test.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
