#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";

use strict;
use Gdr_Rpc;
use GetYN;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

print "\nThis will clear the guider's memory of _ALL_ guide positions.\n";
GetYN::query_ready();

Gdr_Rpc::clear_all_lock_positions( $gdrrpc );


__END__

=head1 NAME

guide.clear.all.lock.positions.kp2m.pl

=head1 Description

Clears all known lock positions.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.clear.all.lock.positions.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
