#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use GetYN            qw/:all/;
use Gdr_Rpc          qw/:all/;
use KPNO_offssets qw/:all/;


my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

move_to_slit_center( $gdrrpc );

### SUBS
sub move_to_slit_center{
  my $gdrrpc = $_[0];

  print "----------------------------------\n";
  print "Moving to slit center = defpos1\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  sleep( 0.5 );
  Gdr_Rpc::select_guide_pos1( $gdrrpc );

  KPNO_offsets::do_offset( 0, 0 );
  KPNO_offsets::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub move_to_slit_center

__END__

=head1 NAME

guided.offset.beams.AorBto.slit.center.kp2m.pl

=head1 Description

Use to move back to the slit center from eith
beam.  Useful if recovering from a guider crash
and you want to start over.

=head1 REVISION & LOCKER

$Name:  $

$Id: guided.offset.beams.AorB.to.slit.center.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
