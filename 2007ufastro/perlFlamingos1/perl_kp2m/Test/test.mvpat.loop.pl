#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: test.mvpat.loop.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Fitsheader        qw/:all/;
use Getopt::Long;
use GetYN            qw/:all/;
use Gdr_Rpc          qw/:all/;
use ImgTests         qw/:all/;
use kpnoTCSinfo      qw/:all/;
use MCE4_acquisition qw/:all/;
use Math::Round      qw/round/;
use RW_Header_Params qw/:all/;
use UseTCS           qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($m_rptpat, $m_ndgsz, $m_throw, $usemnudg, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );
my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
my $gdr_ps = $ENV{GDR_PS};

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec ) = compute_centered_offsets( $pa_slit_on_sky, $m_throw );
my ($Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec) = compute_nudged_offsets( $pa_slit_on_sky, $m_throw,
                                                                      $m_ndgsz,        $gdr_ps);

my ($An_dra, $An_ddec, $Bn_dra, $Bn_ddec) = compute_nudged_offsets( $pa_slit_on_sky, $m_throw,
                                                                      -1*$m_ndgsz,     $gdr_ps);

my ($BoxA_dra,  $BoxA_ddec,  $BoxB_dra,  $BoxB_ddec,
    $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
    $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec) = compute_box_offsets( $pa_slit_on_sky, $m_ndgsz);


main::print_info( $A_dra,  $A_ddec,  $B_dra,  $B_ddec,  $Ap_dra,    $Ap_ddec,  $Bp_dra,    $Bp_ddec,
		  $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $BoxA_dra,  $BoxA_ddec, $BoxB_dra, $BoxB_ddec,
		  $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
                  $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec, $usemnudg);

main::prompt_beam();

my $rpt_pat = $m_rptpat;
my $start  = "start";
my $center = "center";
my $pos    = "pos";
my $neg    = "neg";
my $nameA  = "A_centered";
my $nameB  = "B_centered";

my $selected_set = "start";
my $previous_set = "start";

for( my $tps = 1; $tps <= $rpt_pat; $tps++ ){
  print "\n";
  print "For loop: tps=$tps\n";
  $previous_set = $selected_set;
  $selected_set = select_pattern_set( $tps, $rpt_pat );
  #print "prev set = $previous_set; selected set = $selected_set\n";

  if( $usemnudg eq 1 ){
    if(  ($selected_set eq $center) and
	 (($previous_set eq $start) or($previous_set eq $neg))
      ){
      $nameA = "A_centered";
      $nameB = "B_centered";
      if( $previous_set eq $start ){
	print "Do centered pattern\n";
	print "Should already be in beam A\n";
	#print "SHOULD BE doing centered pattern\n";
	$next_index = do_pattern( $gdrrpc, $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
		                  $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);
	#Offset wrt to first Beam A is zero by def.
	#RW_Header_Params::write_rel_offset_to_header( $header, 0, 0 );

      }elsif( $previous_set eq $neg ){
	print "SHOULD BE: Move from neg to center\n";
	$nameA = "A_centered";
	$nameB = "B_centered";
	
	print "Now need to move ABBA pattern from negative wrt slit center to shifted centered wrt slit center\n";
	print "Are we in beam A, of the negative ABBA pattern?\n";
	print "The offsets should be RA = $An_dra, Dec = $An_ddec.\n";
	print "If this is correct, will attempt to shift the pattern. ";
	GetYN::query_ready();
	
	shift_beam( $gdrrpc, $BoxA_dra, $BoxA_ddec, $BoxB_dra, $BoxB_ddec,
		    $nameA,  $nameB,    $A_dra,    $A_ddec);
	
	#print "SHOULD BE: Do centered pattern\n";	
	print "Do centered pattern\n";
	$next_index = do_pattern( $gdrrpc, $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
				  $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);
      }

    }elsif( ($selected_set eq $pos) and ($previous_set eq $center) ){
      $nameA = "A_positive";
      $nameB = "B_positive";

      print "Now need to move ABBA pattern from centered wrt slit center to shifted positive wrt slit center\n";
      print "Are we in beam A, of the centered ABBA pattern?\n";
      print "The offsets should be RA = $A_dra, Dec = $A_ddec.\n";
      print "If this is correct, will attempt to shift the pattern. ";
      GetYN::query_ready();

      shift_beam( $gdrrpc, $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
                  $nameA,  $nameB,     $Ap_dra,     $Ap_ddec);
      #print "SHOULD BE: doing positive pattern\n";
      $next_index = do_pattern($gdrrpc, $A_dra, $A_ddec, $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec, $nameA, $nameB,
			       $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    }elsif( ($selected_set eq $neg) and ($previous_set eq $pos) ){
      $nameA = "A_negative";
      $nameB = "B_negative";

      print "Now need to move ABBA pattern from positive wrt slit center to shifted negative wrt slit center\n";
      print "Are we in beam A, of the positive ABBA pattern?\n";
      print "The offsets should be RA = $Ap_dra, Dec = $Ap_ddec.\n";
      print "If this is correct, will attempt to shift the pattern. ";
      GetYN::query_ready();

      shift_beam( $gdrrpc, $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec,
                  $nameA,  $nameB,     $An_dra,     $An_ddec);

      #print "SHOULD BE: doing positive pattern\n";
      $next_index = do_pattern($gdrrpc, $A_dra, $A_ddec, $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $nameA, $nameB,
			       $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);

    }
  }elsif( $usemnudg eq 0 ){
    $nameA = "A_centered";
    $nameB = "B_centered";
    $next_index = do_pattern( $gdrrpc, $A_dra, $A_ddec, $A_dra, $A_ddec, $B_dra, $B_ddec, $nameA, $nameB,
			      $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs);
  }
}#End for loop

print "\n";
print "The telescope should be in beam $nameA.\n";
print "Should have completed $rpt_pat repeats of the ABBA pattern\n";
print "Do you want to reset the pointing so the alignment stars are in their boxes\n";
print "and the objects are in the centers of the slits? ";
my $reset = GetYN::get_yn();
if( !$reset ){
  die "\n\n".
      "*******************************************************\n".
      "*****            Dither script done.              *****\n".
      "*****     Don't forget you're in beam $nameA.     *****\n".
      "*******************************************************\n";
}else{
  move_to_slit_center( $gdrrpc );
}

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";
### SUBS
sub base{
  my $index = $_[0];

  my $value = 4*$index - $index + 1;

  return $value;
}#Endsub base


sub compute_box_offsets{
  my ($pa_slit_on_sky, $m_ndgsz) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  # These are in guider pixels
  my $BoxA_dra  = Fitsheader::param_value( 'GPOS2_X' );
  my $BoxA_ddec = Fitsheader::param_value( 'GPOS2_Y' );

  my $BoxB_dra  = Fitsheader::param_value( 'GPOS3_X' );
  my $BoxB_ddec = Fitsheader::param_value( 'GPOS3_Y' );

  my $BoxAp_dra  = $BoxA_dra  + $m_ndgsz * sin( $pa_sky_rad );
  my $BoxAp_ddec = $BoxA_ddec + $m_ndgsz * cos( $pa_sky_rad );

  my $BoxBp_dra  = $BoxB_dra  + $m_ndgsz * sin( $pa_sky_rad );
  my $BoxBp_ddec = $BoxB_ddec + $m_ndgsz * cos( $pa_sky_rad );

  $BoxAp_dra  = Math::Round::round( $BoxAp_dra );
  $BoxAp_ddec = Math::Round::round( $BoxAp_ddec );

  $BoxBp_dra  = Math::Round::round( $BoxBp_dra );
  $BoxBp_ddec = Math::Round::round( $BoxBp_ddec );


  my $BoxAn_dra  = $BoxA_dra  - $m_ndgsz * sin( $pa_sky_rad );
  my $BoxAn_ddec = $BoxA_ddec - $m_ndgsz * cos( $pa_sky_rad );

  my $BoxBn_dra  = $BoxB_dra  - $m_ndgsz * sin( $pa_sky_rad );
  my $BoxBn_ddec = $BoxB_ddec - $m_ndgsz * cos( $pa_sky_rad );

  $BoxAn_dra  = Math::Round::round( $BoxAn_dra );
  $BoxAn_ddec = Math::Round::round( $BoxAn_ddec );

  $BoxBn_dra  = Math::Round::round( $BoxBn_dra );
  $BoxBn_ddec = Math::Round::round( $BoxBn_ddec );

  return( $BoxA_dra,  $BoxA_ddec,  $BoxB_dra,  $BoxB_ddec,
	  $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
          $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec );
}#Endsub compute_box_offsets


sub compute_centered_offsets{
  my ($pa_slit_on_sky, $m_throw) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_centered_offsets


sub compute_nudged_offsets{
  my ($pa_slit_on_sky, $m_throw, $nudgsz_guider, $gdr_ps) = @_;

  # The nudge is sized in terms of guider pixels
  my $nudge_asec = $nudgsz_guider * $gdr_ps;
  #print "nudge = $nudgsz_guider (guider pixels) = $nudge_asec (arcsec)\n";

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );


  my $Ap_dra  =      (0.5 * $m_throw + $nudge_asec) * sin( $pa_sky_rad );
  my $Ap_ddec =      (0.5 * $m_throw + $nudge_asec) * cos( $pa_sky_rad );

  my $Bp_dra  = -1 * (0.5 * $m_throw - $nudge_asec) * sin( $pa_sky_rad );
  my $Bp_ddec = -1 * (0.5 * $m_throw - $nudge_asec) * cos( $pa_sky_rad );

  $Ap_dra  = sprintf '%.2f', $Ap_dra;
  $Ap_ddec = sprintf '%.2f', $Ap_ddec;

  $Bp_dra  = sprintf '%.2f', $Bp_dra;
  $Bp_ddec = sprintf '%.2f', $Bp_ddec;

  return( $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec );
}#Endsub compute_nudged_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub did_it_work{
  print "Did it move the boxes as expected? ";
  my $reply = GetYN::get_yn();

  if( !$reply ){
    print "Continue? ";
    my $reply2 = GetYN::get_yn();
    if( !$reply2 ){
      die "\n\nExiting\n\n";
    }
  }
}#Endsub did_it_work


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub do_pattern{
  my ($gdrrpc, $A_dra, $A_ddec, $thisA_dra, $thisA_ddec, $thisB_dra, $thisB_ddec, $nameA, $nameB,
      $header, $lut_name, $file_hint, $next_index, $net_edt_timeout, $use_tcs) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";

  #print "SHOULD BE: Take 1 Spectrum in Beam $nameA\n";
  print "Take 1 Spectrum in Beam $nameA\n";

  RW_Header_Params::write_rel_offset_to_header( $header,
  						$thisA_dra  - $A_dra,
  						$thisA_ddec - $A_ddec);

  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE moving to beam $nameB\n";
  main::move_to_beam_B( $gdrrpc, $thisB_dra, $thisB_ddec, $nameB );

  RW_Header_Params::write_rel_offset_to_header( $header,
  						$thisB_dra  - $A_dra,
  						$thisB_ddec - $A_ddec);

  #print "SHOULD BE: Take 2 Spectra in beam $nameB\n";
  print "Take 2 Spectra in beam B\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  #print "SHOULD BE: moving to beam $nameA\n";
  main::move_to_beam_A( $gdrrpc, $thisA_dra, $thisA_ddec, $nameA );

  RW_Header_Params::write_rel_offset_to_header( $header,
  						$thisA_dra  - $A_dra,
  						$thisA_ddec - $A_ddec);

  #print "SHOULD BE: Take 1 Spectrum in Beam $nameA\n";
  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  return( $next_index );
}#Endsub do_pattern


sub move_to_beam_A{
  my ($gdrrpc, $thisA_dra, $thisA_ddec, $nameA) = @_;
  #my ($header, $gdrrpc, $A_dra, $A_ddec) = @_;

  print "-------------------------------------\n";
  print "Move to Beam $nameA = defpos2\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos2( $gdrrpc );

  main::do_offset( $thisA_dra, $thisA_ddec );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub move_to_beam_A


sub move_to_beam_B{
  my ($gdrrpc, $thisB_dra, $thisB_ddec, $nameB) = @_;

  print "-------------------------------------\n";
  print "Move to Beam $nameB = defpos3\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos3( $gdrrpc );

  main::do_offset( $thisB_dra, $thisB_ddec );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub move_to_beam_B


sub move_to_slit_center{
  my $gdrrpc = $_[0];

  print "----------------------------------\n";
  print "Moving to slit center = defpos1\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos1( $gdrrpc );

  main::do_offset( 0, 0 );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub move_to_slit_center


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait


sub print_info{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,  $Ap_dra,    $Ap_ddec,  $Bp_dra,    $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec, $BoxA_dra,  $BoxA_ddec, $BoxB_dra, $BoxB_ddec,
      $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
      $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec, $usemnudg) = @_;

  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "--------------------PRESENT PARAMETERS FOR MOS DITHER----------------------------------\n";
  print "\n";
  print_offsets($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
      $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec,

      $BoxA_dra,  $BoxA_ddec,  $BoxB_dra,  $BoxB_ddec,
      $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
      $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec, $usemnudg );

  print "\n";
  print "NET Position Angle of Slit on Sky(deg)---------- $pa_slit_on_sky\n";
  print "Distance between A & B beams-------------------- $m_throw  (arcsec)\n";
  print "Distance to shift ABBA on repeats of pattern---- $m_ndgsz  (guider pixels)\n";
  print "Number of times through pattern----------------- $m_rptpat\n";
  if( $usemnudg ){
    print "Will nudge ABBA pattern on pattern repeats.\n";
    print "Nudged pattern order---------------------------- center, positive, negative\n";
  }else{
    print "Will _NOT_ nugde pattern on repeats from center.\n";
  }
}#Endsub print_info


sub print_offsets{
  my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec,
      $Ap_dra, $Ap_ddec, $Bp_dra, $Bp_ddec,
      $An_dra, $An_ddec, $Bn_dra, $Bn_ddec,

      $BoxA_dra,  $BoxA_ddec,  $BoxB_dra,  $BoxB_ddec,
      $BoxAp_dra, $BoxAp_ddec, $BoxBp_dra, $BoxBp_ddec,
      $BoxAn_dra, $BoxAn_ddec, $BoxBn_dra, $BoxBn_ddec, $usemnudg ) = @_;

  if( $usemnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_A";
    write STDOUT;
    $~ = "TELE_OFFSETS_BEAM_B";
    write STDOUT;
    $~ = "GUIDE_BOX_OFFSETS_BEAM_A";
    write STDOUT;
    $~ = "GUIDE_BOX_OFFSETS_BEAM_B";
    write STDOUT;
    $~ = $old_stdout_format;

  }elsif( !$usemnudg ){
    my $old_stdout_format = $~;
    $~ = "OFFSET_HEADINGS_NOT_NUDGED";
    write STDOUT;
    $~ = "OFFSETS_NOT_NUDGED";
    write STDOUT;
    $~ = $old_stdout_format;
  }
}#Endsub print_offsets


sub prompt_beam{
  print "\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> The following conditions are required to start:\n";
  print "> 1) Already guiding in beam A, also called defpos1.\n";
  print "> 2) The present telescope offset matches the offset for beam A listed above.\n";
  print ">    (If they do not, have the operator zero the offsets.)\n";
  print "\n";

  GetYN::query_ready();
}#Endsub prompt_beam


sub select_pattern_set{
  my ($tps, $rpt_pat) = @_;

  my $center = "center";
  my $pos    = "pos";
  my $neg    = "neg";
  my $set;

  my $index = 0;
  my $index_matched = "";
  my $base_matched = "";
  until( $index >= $rpt_pat ){
    my $base = base( $index );
    #print "index = $index; tps = $tps; base = $base\n";

    if( $tps == $base ){
      $set = $center;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+1 ){
      $set = $pos;
      $index = $rpt_pat + 1;

    }elsif( $tps == $base+2 ){
      $set = $neg;
      $index = $rpt_pat + 1;

    }else{
      $index++;
    }
  }

  return $set;
}#Endsub select_pattern_set


sub shift_beam{
  my ($gdrrpc, $thisBoxA_dra, $thisBoxA_ddec, $thisBoxB_dra, $thisBoxB_ddec,
      $nameA,  $nameB,        $thisA_dra,     $thisA_ddec) = @_;

  print "<><><><><><><><><><><><><><><><><><><><>\n";
  print "Step 1 of 5: Guiding off.\n";
  print "--------------------\n";
  Gdr_Rpc::guide_off( $gdrrpc );

  print "Step 2 of 5: Define beam $nameB on guider.\n";
  Gdr_Rpc::send_guide_and_aoi_boxes( $gdrrpc, $thisBoxB_dra, $thisBoxB_ddec );
  print "Should have moved the guide and AOI boxes to $thisBoxB_dra, $thisBoxB_ddec\n";
  main::did_it_work();

  #main::guide_wait();
  Gdr_Rpc::define_lock_position( $gdrrpc, "3" );
  Gdr_Rpc::mark_current_position( $gdrrpc );

  print "Should have set defpos3 as beam $nameB and added a marker.\n";
  main::did_it_work();

  print "Step 3 of 5: Define beam $nameA on guider.\n";
  Gdr_Rpc::send_guide_and_aoi_boxes( $gdrrpc, $thisBoxA_dra, $thisBoxA_ddec );
  print "Should have moved the guide and AOI boxes to $thisBoxA_dra, $thisBoxA_ddec\n";
  main::did_it_work();

  #main::guide_wait();
  Gdr_Rpc::define_lock_position( $gdrrpc, "2" );
  Gdr_Rpc::mark_current_position( $gdrrpc );

  print "Should have set defpos2 as beam $nameA and added a marker.\n";
  main::did_it_work();

  ### Step 4 Offset telescope to beam thisA
  print "\n";
  print "Step 4 of 5: Offset telescope to beam $nameA.\n";
  print "----------------------------------\n";
  do_offset( $thisA_dra, $thisA_ddec );
  main::offset_wait();

  ### Step 5 Start guiding in beam thisA
  print "\n";
  print "Step 5 of 5: Start guiding in beam $nameA? \n";

  my $reply = GetYN::get_yn();
  if( !$reply ){
    die "\n\nExiting.\n\n";
  }

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub shift_beam


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    #print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    print "SHOULD BE DOING idle_lut_status loop\n\n";
    #print "ENTERING idle_lut_status loop\n\n";
    #main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


#### FORMATS
format OFFSET_HEADINGS = 
                 Telescope and Guide Box Offsets wrt slit center
               Centered              Positive              Negative
Beam        dRa       dDec        dRa       dDec        dRa       dDec
---------------------------------------------------------------------------------------
.
format OFFSET_HEADINGS_NOT_NUDGED = 
               Offsets Centered wrt slit center
              Telescope               Guide Box
Beam        dRa       dDec            dRa  dDec
---------------------------------------------------------------
.
format OFFSETS_NOT_NUDGED =
 A    @#####.##  @#####.## (arcsec)  @###  @### (guider pixels)
$A_dra, $A_ddec, $BoxA_dra, $BoxA_ddec
 B    @#####.##  @#####.## (arcsec)  @###  @### (guider pixels)
$B_dra, $B_ddec, $BoxB_dra, $BoxB_ddec
.
format TELE_OFFSETS_BEAM_A = 
 A    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$A_dra, $A_ddec, $Ap_dra, $Ap_ddec, $An_dra, $An_ddec
.
format TELE_OFFSETS_BEAM_B = 
 B    @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  @#####.##  (arcsec)
$B_dra, $B_ddec, $Bp_dra, $Bp_ddec, $Bn_dra, $Bn_ddec
.
format GUIDE_BOX_OFFSETS_BEAM_A = 
 A         @###       @###       @###       @###       @###       @###  (guider pixels)
$BoxA_dra, $BoxA_ddec, $BoxAp_dra, $BoxAp_ddec, $BoxAn_dra, $BoxAn_ddec
.
format GUIDE_BOX_OFFSETS_BEAM_B = 
 B         @###       @###       @###       @###       @###       @###  (guider pixels)
$BoxB_dra, $BoxB_ddec, $BoxBp_dra, $BoxBp_ddec, $BoxBn_dra, $BoxBn_ddec
.
