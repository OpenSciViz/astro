#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: test.flip.pl 14 2008-06-11 01:49:45Z hon $);

use strict;


my $y = 176;

print "start y   = $y\n";
$y = flip_y( $y );

print "flipped y = $y\n";


sub flip_y{
  my $in_y = $_[0];

  #The marker coordinate system is flipped in y wrt the guide box coordinate system
  #The guider size is 640 x 480 (x,y)

  my $ref = 240;
  my $out_y;
  my $delta;
  if( $in_y > $ref ){
    $delta = $in_y - $ref;
    $out_y = $ref - $delta;
    print "in > out; delta = $delta; out = $out_y\n";

  }elsif( $in_y < $ref ){
    $delta = $ref - $in_y;
    $out_y = $ref + $delta;
    print "in < out; delta = $delta; out = $out_y\n";

  }elsif( $in_y == $ref ){
    $out_y = 240;
  }

  return $out_y;
}#Endsub flip_y
