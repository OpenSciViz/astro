#!/usr/local/bin/perl -w

#
#Mos Dither patterns at 2.1m telescopes
#With Absolute offsets wrt the pointing center
#

my $rcsId = q($Name:  $ $Id: dither.mos.nudge.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use RW_Header_Params qw/:all/;
use Gdr_Rpc qw/:all/;
use MCE4_acquisition qw/:all/;
use UseTCS qw/:all/;
use kpnoTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($m_rptpat, $m_ndgsz, $m_throw, $usemnudg, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );
my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

print_info();

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

my ( $A_dra, $A_ddec, $B_dra, $B_ddec ) = compute_offsets( $pa_slit_on_sky );

my ( $nudge_dra, $nudge_ddec ) = compute_nudge( $pa_slit_on_sky, $m_ndgsz );

print "nudge offsets = $nudge_dra, $nudge_ddec\n";

#print_offsets();
main::prompt_beam();
#main::prompt_setup();

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $next_index = $this_index;

#Supposed to beam in beam A at the start
for( my $rptpat = 1; $rptpat <= $m_rptpat; $rptpat++ ){
  print ">-------------------------------------<\n";
  print "do ABBA sequence # $rptpat of $m_rptpat \n";

  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  main::move_to_beam_B( $header, $gdrrpc );
  #main::move_to_beam_B( $header, $rptpat );

  print "Take 2 Spectra in beam B\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  main::move_to_beam_A( $header, $gdrrpc );
  #main::move_to_beam_A( $header, $rptpat );
  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

}#End for loop

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";
print "The telescope should be in beam A.\n";
print "Do you want to reset the pointing so the alignment stars are in their boxes\n";
print "and the objects are in the centers of the slits? ";
my $reset = GetYN::get_yn();
if( !$reset ){
  die "\n\nExiting, Dither script done.  Don't forget you're in beam A.\n\n";
}else{
  move_to_slit_center( $gdrrpc );
}

####SUBS
sub print_info{
  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "----------------PRESENT PARAMETERS FOR MOS DITHER----------------\n";
  print "-------(Check that the Rotator PA and Slit PA are correct)-------\n";
  print "\n";
  print "Number of times through pattern-------------------------------- $m_rptpat\n";
  print "Distance (arcsec) between A & B beams-------------------------- $m_throw\n";
  print "Nudge ABBA pattern on pattern repeats-------------------------- $usemnudg (0=no, 1=yes)\n";
  print "Distance in guider pixels to shift ABBA on repeats of pattern-- $m_ndgsz\n";
  print "\n";

  print "Chip PA on sky for this Rotator angle (deg)-------------------- ".
         "$chip_pa_on_sky_this_rotator_pa\n";
  print "\n";
  print "NET Position Angle of Slit on Sky(deg)------------------------- ".
         "$pa_slit_on_sky\n";
  print "-----------------------------------------------------------------\n\n";
}#Endsub print_info


sub prompt_setup{
  print "\n\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern: \n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type CLEAR to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $clear_home = <>;chomp $clear_home;
  my $valid = 0;
  until( $valid ){
    if( $clear_home =~ m/clear/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/home/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either CLEAR or HOME ";
      $clear_home = <>; chomp $clear_home;
    }
  }

  if( $clear_home =~ m/clear/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    clear_offset();
  }elsif( $clear_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub prompt_beam{
  print "\n\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> The following conditions are required to start:\n";
  print "> 1) Already guiding in beam A, also called defpos1.\n";
  print "> 2) The present telescope offset matches the offset for beam A listed below.\n";
  print "\n\n";
  main::print_offsets();

  GetYN::query_ready();
}#Endsub prompt_beam


sub clear_offset{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;
  print "EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub clear_offset


sub compute_offsets{
  my $pa_slit_on_sky = $_[0];
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_offsets


sub compute_nudge{
  my ( $pa_slit_on_sky, $m_ndgsz ) = @_;
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $nudge_dra  = $m_ndgsz * sin( $pa_sky_rad );
  my $nudge_ddec = $m_ndgsz * cos( $pa_sky_rad );

  $nudge_dra  = sprintf '%.2f', $nudge_dra;
  $nudge_ddec = sprintf '%.2f', $nudge_ddec;

  return( $nudge_dra, $nudge_ddec );
}#Endsub compute_pattern_shift


sub print_offsets{
  #print "----------------\n";
  print "Offsets to Beam A:        ";
  print "delta_RA = $A_dra, delta_DEC = $A_ddec\n";
  print "Offsets to Beam B:        ";
  print "delta_RA = $B_dra, delta_DEC = $B_ddec\n";

  #print "Nudged Offset to Beam A:  ";
  #print "delta_RA = $nudged_A_dra, delta_DEC = $nudged_A_ddec\n";
  #print "Nudged Offset to Beam B:  ";
  #print "delta_RA = $nudged_B_dra,  delta_DEC = $nudged_B_ddec\n\n";
  #print "----------------\n";
}#Endsub print_offsets


sub move_to_beam_A{
  my ($header, $gdrrpc) = @_;

  print "-------------------------------------\n";
  print "Move to Beam A = defpos2\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos2( $gdrrpc );

  main::do_offset( $A_dra, $A_ddec );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );

  #Offset wrt to first Beam A is zero by def.
  RW_Header_Params::write_rel_offset_to_header( $header, 0, 0 );

}#Endsub move_to_beam_A


sub move_to_beam_B{
  my ($header, $gdrrpc) = @_;

  print "-------------------------------------\n";
  print "Move to Beam B = defpos3\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos3( $gdrrpc );

  main::do_offset( $B_dra, $B_ddec );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );

  #Offset wrt to first Beam A is zero by def.
  RW_Header_Params::write_rel_offset_to_header( $header,
						$B_dra - $A_dra,
						$B_ddec - $A_ddec);

}#Endsub move_to_beam_B


sub move_to_slit_center{
  my $gdrrpc = $_[0];

  print "----------------------------------\n";
  print "Moving to slit center = defpos1\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  Gdr_Rpc::select_guide_pos1( $gdrrpc );

  main::do_offset( 0, 0 );
  main::offset_wait();

  Gdr_Rpc::guide_on( $gdrrpc );
}#Endsub move_to_slit_center


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    print "SHOULD BE DOING idle_lut_status loop\n\n";
    #print "ENTERING idle_lut_status loop\n\n";
    #main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky
