#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader       qw/:all/;
use Getopt::Long;
use GetYN            qw/:all/;
use Gdr_Rpc          qw/:all/;
use KPNO_offsets     qw/:all/;
use Math::Round      qw/round/;
use RW_Header_Params qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($m_rptpat, $m_ndgsz, $m_throw, $usemnudg, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );
my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
my $gdr_ps = $ENV{GDR_PS};

my ($A_dra,  $A_ddec,  $B_dra,  $B_ddec ) = main::compute_centered_offsets( $pa_slit_on_sky, $m_throw );

my $nameB  = "B_centered";

print "This will move from the slit center to beam B, at offsets = $B_dra, $B_ddec\n";
GetYN::query_ready();
main::move_to_beam_B( $gdrrpc, $B_dra, $B_ddec, $nameB );


###SUBS
sub compute_centered_offsets{
  my ($pa_slit_on_sky, $m_throw) = @_;

  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_centered_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub move_to_beam_B{
  my ($gdrrpc, $thisB_dra, $thisB_ddec, $nameB) = @_;

  print "-------------------------------------\n";
  print "Move to Beam $nameB = defpos3\n";
  Gdr_Rpc::guide_off( $gdrrpc );
  sleep( 0.5 );
  Gdr_Rpc::select_guide_pos3( $gdrrpc );
  sleep( 0.5 );

  KPNO_offsets::do_offset( $thisB_dra, $thisB_ddec );
  KPNO_offsets::offset_wait();
  Gdr_Rpc::guide_on( $gdrrpc );
  Gdr_Rpc::guide_wait();

}#Endsub move_to_beam_B


__END__

=head1 NAME

guide.offset.slit.center.to.beam.B.kp2m.pl

=head1 Description

Moves from the slit center to beam B.
The beams must already be defined.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.offset.slit.center.to.beam.B.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
