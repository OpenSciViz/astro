#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use KPNO_offsets qw/:all/;

my $DEBUG = 0;
#GetOptions( 'debug' => \$DEBUG );

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my $cnt = @ARGV;
my $gdr_ps = $ENV{GDR_PS};
if( $cnt < 2 ){
  die "\n\n".
      "USAGE:  guide.mos.offset.kp2m.pl ra_offset dec_offset\n".
      "        Offsets are relative to present position, and in arcsec.\n".
      "\n".
      "WHAT IT WILL DO:\n".
      "        1) Turn off guiding\n".
      "        2) Offset the telescope\n".
      "        3) Offset the guide and aoi boxes\n".
      "        4) Resume guiding\n\n".
      "\n".
      "        Note: The minimum size offsets that will be performed by the guider are\n".
      "        guide box: 1 guider pixels = $gdr_ps arcsec.\n".
      "        AOI   box: 2 guider pixels = ".(2*$gdr_ps)." arcsec.\n\n".
      "        The env variable GDR_PS = $gdr_ps tells this script the guider plate scale.\n\n".

      "********NEED to check signs of guider offsets match signs of ra/dec***********\n".
      "********NEED to check GDR_PS value********************************************\n\n";
}

my $ra_offset  = shift;
my $dec_offset = shift;

if( $ra_offset =~ m/[a-zA-Z]/ or $dec_offset =~ m/[a-zA-Z]/ ){
  die "\nPlease enter numerical offsets.\n\n";
}


my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n\n";

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

Gdr_Rpc::guide_off( $gdrrpc );
KPNO_offsets::do_relative_offset( $ra_offset, $dec_offset );
KPNO_offsets::offset_wait();

my ($ra_offset_pix, $dec_offset_pix) = compute_guide_offset( $gdr_ps, $ra_offset, $dec_offset );
Gdr_Rpc::move_guide_and_aoi_boxes( $gdrrpc, $ra_offset_pix, $dec_offset_pix );

Gdr_Rpc::guide_on( $gdrrpc );

print "\n";
print "Remember to wait for the star to stabilize in the guider\n\n";
#Gdr_Rpc::guide_wait();

### SUBS
sub compute_guide_offset{
  my ($gdr_ps, $ra_arcsec, $dec_arcsec) = @_;

  my $ra_offset_pix  = $ra_arcsec / $gdr_ps;
  my $dec_offset_pix = $dec_arcsec / $gdr_ps;

  $ra_offset_pix  = sprintf "%.2f", $ra_offset_pix;
  $dec_offset_pix = sprintf "%.2f", $dec_offset_pix;

  #print " ra_offset_pix = $ra_offset_pix\n".
  #      "dec_offset_pix = $dec_offset_pix\n\n";
  
  use Math::Round qw/ round /;
  $ra_offset_pix  = Math::Round::round( $ra_offset_pix  );
  $dec_offset_pix = Math::Round::round( $dec_offset_pix );

  #$ra_offset_pix  = round_input( $ra_offset_pix  );
  #$dec_offset_pix = round_input( $dec_offset_pix );

  #die " ra_offset_pix = $ra_offset_pix\n".
  #    "dec_offset_pix = $dec_offset_pix\n\n";

  ### +RA  = East  = -x for guide box
  ### +DEC = North = -y for guide box
  $ra_offset_pix  = -1*$ra_offset_pix;
  $dec_offset_pix =1*$dec_offset_pix;

  return( $ra_offset_pix, $dec_offset_pix );
}#Endsub compute_guide_offset


__END__

=head1 NAME

guide.mos.offset.kp2m.pl

=head1 Description

Use to offet the telescope when you are guiding.
Do not use the relative offset script.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.mos.offset.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
