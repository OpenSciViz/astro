#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next dither sequence
#
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);


use Fitsheader qw/:all/;
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\n\n".
      ">>>---------------------Present exposure parameters are".
      "---------------------<<<\n";

printFitsHeader( "EASTTRAN", "NORTTRAN", "NUM_TRAN" );
print "\n";
printFitsHeader( "USE_2BY2", "SIZE2BY2" );

print "_____________________________________________________________".
      "__________________\n\n";

my $didmod = 0;
my $redo = 0;

print "Change anything? ";
my $change = get_yn();
if( $change ){
  until( $redo ){

    my $chg_bigger_than_equal_to_zero = chg_bigger_than_equal_to_zero( "EASTTRAN" );
    if( $chg_bigger_than_equal_to_zero > -1 ){
      Fitsheader::setNumericParam( "EASTTRAN", $chg_bigger_than_equal_to_zero );
      $didmod += 1;
    }

    $chg_bigger_than_equal_to_zero = chg_bigger_than_equal_to_zero( "NORTTRAN" );
    if( $chg_bigger_than_equal_to_zero > -1 ){
      Fitsheader::setNumericParam( "NORTTRAN", $chg_bigger_than_equal_to_zero );
      $didmod += 1;
    }

    $chg_bigger_than_equal_to_zero = chg_bigger_than_equal_to_zero( "NUM_TRAN" );
    if( $chg_bigger_than_equal_to_zero > -1 ){
      Fitsheader::setNumericParam( "NUM_TRAN", $chg_bigger_than_equal_to_zero );
      $didmod += 1;
    }

    my $chg_zero_or_one = chg_zero_or_one( "USE_2BY2" );
    if( $chg_zero_or_one > -1 ){
      Fitsheader::setNumericParam( "USE_2BY2", $chg_zero_or_one );
      $didmod += 1;
    }

    my $chg_bigger_than_zero = chg_bigger_than_zero( "SIZE2BY2" );
    if( $chg_bigger_than_zero > -1 ){
      Fitsheader::setNumericParam( "SIZE2BY2", $chg_bigger_than_zero );
      $didmod += 1;
    }

    if( $didmod > 0 ){
      print "\n\nThe new set of dither parameters are:\n";
      printFitsHeader( "EASTTRAN", "NORTTRAN", "NUM_TRAN" );
      print "\n";
      printFitsHeader( "USE_2BY2", "SIZE2BY2" );

      print "___________________________________________________\n\n";
      print "\nAccept changes?";
      $redo = get_yn();
    }else{
      $redo = 1;
    }
  }
}elsif( !$change ){
  #do nothing
}


if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub chg_bigger_than_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value > 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number > 0:  ";
      }else{
	if( $response >0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number > 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chng_bigger_than_zero


sub chg_bigger_than_equal_to_zero{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter an integer value >= 0:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please enter an integer number >= 0:  ";
      }else{
	if( $response >=0 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter an integer number >= 0:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chng_bigger_than_equal_to_zero


sub chg_zero_or_one{
  my $input = $_[0];

  my $response = -1;
  my $is_valid = 0;

  my $iparm = Fitsheader::param_index( $input );

  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm] . "?\n";
  print"Enter either 0 or 1:  ";

  until($is_valid){
    chomp ($response = <STDIN>);
    #print "You entered $response\n";

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[a-z,A-Z]/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a number:  ";
      }elsif( $response =~ m/\./ ){
	#print "$response is not valid\n";
	print "Please either 0 or 1:  ";
      }else{
	if( $response == 0 or $response == 1 ){
	  #print "Valid response was $response\n";
	  $is_valid = 1;
	}else{
	  print "Please enter either 0 or 1:  ";
	}
      }

    }else{
      print "Leaving $input unchanged\n";
      $response = -1;
      $is_valid = 1;
    }
  }
  print "\n";

  return $response;
}#Endsub chg_zero_or_one


__END__

=head1 NAME

config.dither.strip.kpno.pl

=head1 Description

Setup a special imaging dither which takes
a series of data along a long strip on the sky.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.dither.strip.kpno.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
