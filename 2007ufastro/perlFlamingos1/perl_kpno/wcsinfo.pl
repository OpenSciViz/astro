#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Fitsheader qw/:all/;

my $tcstel = $ENV{UF_KPNO_TCS_PROG};
my $quote  = "\"";
my $space  = " ";
my $teleinfo_call = $tcstel.$space.$quote."tele info".$quote;

my $teleinfo = `$teleinfo_call`;
print "tcsinfo is:\n\n";
print $teleinfo."\n\n";

my ($ibeg);
my ($iend);
my ($tinfo);

my $RA_APR;
my $DEC_APR;
if( ($ibeg = index( $teleinfo, "appar position" )) >= 0 ){
  $iend = index( $teleinfo, "\n", $ibeg );
  $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
 ( $RA_APR, $DEC_APR ) =
    split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
  setStringParam( "RA_APR",$RA_APR);
  setStringParam("DEC_APR",$DEC_APR);
}else{
  setStringParam( "RA_APR","unkown");
  setStringParam("DEC_APR","unkown");
}


print "RA_APR  = $RA_APR;\n";
print "DEC_APR = $DEC_APR;\n\n";

my $ra_deg  = hms2deg( $RA_APR );
print "\n";
my $deg_deg = dms2deg( $DEC_APR );
print "\n";
print "ra_deg  = $ra_deg\n";
print "deg_deg = $deg_deg\n";

sub hms2deg{
  my $input = $_[0];

  my $colon = index $input, ':' ;
  my $hour  = substr $input, 0, $colon;

  my $min_sec = substr $input, $colon+1;
     $colon = index $min_sec, ':' ;
  my $min   = substr $min_sec, 0, $colon;

  my $sec   = substr $min_sec, $colon+1;
     $colon = index $sec, ':' ;
     $sec   = substr $sec, 0, $colon;

  my $deg = ( $hour + ($min + $sec/60)/60 ) * 360 / 24;

  $deg  = sprintf "%.5f", $deg;
  return $deg;
}#Endsub hms2deg


sub dms2deg{
  my $input = $_[0];

  my $colon = index $input, ':' ;
  my $deg  = substr $input, 0, $colon;

  my $min_sec = substr $input, $colon+1;
     $colon = index $min_sec, ':' ;
  my $min   = substr $min_sec, 0, $colon;

  my $sec   = substr $min_sec, $colon+1;
     $colon = index $sec, ':' ;
     $sec   = substr $sec, 0, $colon;

  my $sign = -1 if( $deg < 0 );
     $sign = +1 if( $deg >= 0);

  $deg = $sign * (abs($deg) + ( $min + $sec/60 )/ 60);
  $deg = sprintf "%.5f", $deg;
  return $deg;
}#Endsub dms2deg


__END__

=head1 NAME

wcsinfo.pl

=head1 Description

Get the present WCS related info from the TCS
and print it out.

=head1 REVISION & LOCKER

$Name:  $

$Id: wcsinfo.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
