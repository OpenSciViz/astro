package kpnoTCSinfo;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;
use WCS_Info qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use kpnoTCSinfo ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = 
  ( 'all' => [ qw(
		  &getKPNOtcsinfo
		 ) ]
  );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
sub getKPNOtcsinfo{
  my $header = $_[0];

  my $tcstel = $ENV{UF_KPNO_TCS_PROG};
  my $quote  = "\"";
  my $space  = " ";
  my $teleinfo_call = $tcstel.$space.$quote."tele info".$quote;

  my $teleinfo = `$teleinfo_call`;
  print "tcsinfo is:\n\n";
  print $teleinfo."\n\n";

  my ($ibeg);
  my ($iend);
  my ($tinfo);

  if( ($ibeg = index( $teleinfo, "mean position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA",$RA);
    setStringParam("DEC",$DEC);
    if( ($ibeg = index( $tinfo, "(" )) >= 0 ){
      $iend = rindex( $tinfo, ")" );
      my $epoch = substr( $tinfo, $ibeg+1, $iend-$ibeg-1 );
      setNumericParam("EPOCH",$epoch);
    }

    WCS_Info::set_wcs_info( $RA, $DEC, $header );

  }else{
    setStringParam( "RA","unkown");
    setStringParam("DEC","unkown");
  }

  if( ($ibeg = index( $teleinfo, "appar position" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $RA, $DEC ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam( "RA_APR",$RA);
    setStringParam("DEC_APR",$DEC);
  }else{
    setStringParam( "RA_APR","unkown");
    setStringParam("DEC_APR","unkown");
  }

  if( ($ibeg = index( $teleinfo, "ha" )) >= 0 ){
    $iend = index( $teleinfo, ",", $ibeg );
    my $ha = substr( $teleinfo, $ibeg, $iend-$ibeg );
    $ha = substr( $ha, index($ha,':')+1, length($ha) );
    setStringParam("HA",$ha);
  }else{ 
    setStringParam("HA","unkown");
  }

  if( ($ibeg = index( $teleinfo, "utime/stime" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $UT, $ST ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setStringParam("UTC",$UT);
    setStringParam("LST",' '.$ST);
  }else{
    setStringParam("UTC","unkown");
    setStringParam("LST","unkown");
  }

  if( ($ibeg = index( $teleinfo, "airmass/zen.dist" )) >= 0 ){
    $iend = index( $teleinfo, "\n", $ibeg );
    $tinfo = substr( $teleinfo, $ibeg, $iend-$ibeg );
    my ( $AM, $ZD ) =
      split( /,/, substr($tinfo,index($tinfo,':')+1,length($tinfo)), 3 );
    setNumericParam( "AIRMASS",$AM);
    setNumericParam("ZD",$ZD);
  }else{
    setNumericParam( "AIRMASS",0);
    setNumericParam("ZD",0);
  }

  my ($time, $date) = hdwe_date_time();
  #my $date = `date -u '+%Y-%m-%d'`;
  #chomp( $date );
  #setStringParam("UTCDATE",$date);
  setStringParam("DATE-OBS",$date);

  #my $time = `date -u '+%H:%M:%S'`;
  #chomp( $time );
  setStringParam("TIME-OBS",$time);

  print "Writing KPNO $ENV{THIS_TELESCOP} TCS info into header\n";
  print "$header\n\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub getKPNOtcsinfo


sub hdwe_date_time{
  my $tcstel = $ENV{UF_KPNO_TCS_PROG};
  my $quote  = "\"";
  my $space  = " ";
  my $tele_date_time_call = $tcstel.$space.$quote."hdwe utClock".$quote;
  
  my $tele_date_time = `$tele_date_time_call`;
  
  my ($word, $value)  = split( /= /, $tele_date_time);
  my ($time, $date) = split( /, /, $value);
  
  my $slash = index $date, "/";
  my $month = substr $date,0, 2;
  
  my $day   = substr $date, $slash+1;
     $slash = index $day, "/";
     $day   = substr $day, 0, 2;
  
  my $last_slash = rindex $date, "/";
  my $year       = 2000 + substr $date, $last_slash+1, 2;
  
  my $utc_date = $year."-".$month."-".$day;
  
  print "utc-time = $time\n";
  print "utc-date = $utc_date\n";
  
  return ($time, $utc_date);
}#Endsub hdwe_date_time


#sub set_wcs_info{
#  my ( $RA, $DEC, $header ) = @_;
#  
#  my $ra_deg  = hms2deg( $RA );
#  my $deg_deg = dms2deg( $DEC );
#
#
#  setNumericParam( "CRVAL1", $ra_deg  );
#  setNumericParam( "CRVAL2", $deg_deg );
#
#  my $chip_pa = param_value( 'CHIP-PA' );
#  my $rot_pa  = param_value( 'ROT_PA'  );
#
#  my $crota1  = 180 - ($rot_pa + $chip_pa);
#  setNumericParam( "CROTA1", $crota1 );
#}#Endsub set_wcs_info
#
#
#sub hms2deg{
#  my $input = $_[0];
#
#  my $colon = index $input, ':' ;
#  my $hour  = substr $input, 0, $colon;
#
#  my $min_sec = substr $input, $colon+1;
#     $colon = index $min_sec, ':' ;
#  my $min   = substr $min_sec, 0, $colon;
#
#  my $sec   = substr $min_sec, $colon+1;
#     $colon = index $sec, ':' ;
#     $sec   = substr $sec, 0, $colon;
#
#  my $deg = ( $hour + ($min + $sec/60)/60 ) * 360 / 24;
#  $deg = sprintf "%.5f", $deg;
#  return $deg;
#}#Endsub hms2deg
#
#
#sub dms2deg{
#  my $input = $_[0];
#
#  my $colon = index $input, ':' ;
#  my $deg  = substr $input, 0, $colon;
#
#  my $min_sec = substr $input, $colon+1;
#     $colon = index $min_sec, ':' ;
#  my $min   = substr $min_sec, 0, $colon;
#
#  my $sec   = substr $min_sec, $colon+1;
#     $colon = index $sec, ':' ;
#     $sec   = substr $sec, 0, $colon;
#
#  my $sign = -1 if( $deg < 0 );
#     $sign = +1 if( $deg >= 0);
#
#  $deg = $sign * (abs($deg) + ( $min + $sec/60 )/ 60);
#  $deg = sprintf "%.5f", $deg;
#  return $deg;
#}#Endsub dms2deg

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

kpnoTCSinfo - Perl extension for Flamingos at KPNO

=head1 SYNOPSIS

  use kpnoTCSinfo qw/:all/;


=head1 DESCRIPTION


=head2 EXPORT

  &getKPNOtcsinfo()

=head1 REVISION & LOCKER

$Name:  $

$Id: kpnoTCSinfo.pm,v 0.1 2003/05/22 15:51:55 raines Exp $

$Locker:  $

=head1 AUTHOR

SNR 26 Oct 2001

=head1 SEE ALSO

perl(1).

=cut
