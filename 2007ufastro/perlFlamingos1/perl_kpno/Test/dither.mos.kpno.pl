#!/usr/local/bin/perl -w

#
#Mos Dither patterns at KPNO 4m and 2.1m telescopes
#With Absolute offsets wrt the pointing center
#

my $rcsId = q($Name:  $ $Id: dither.mos.kpno.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use Dither_patterns qw/:all/; ##?really necessary?
use MCE4_acquisition qw/:all/;
use kpnoTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = Fitsheader::param_value( 'EXP_TIME' );
my $nreads          = Fitsheader::param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

#>>>Get dither info from header
my $m_rptpat = Fitsheader::param_value( 'M_RPTPAT' );
my $m_ndgsz  = Fitsheader::param_value( 'M_NDGSZ' );

my $m_throw = Fitsheader::param_value( 'M_THROW' );

my $chip_pa_on_sky_for_rot0 = Fitsheader::param_value( 'CHIP-PA' );
my $pa_rotator              = Fitsheader::param_value( 'ROT_PA' );
my $chip_pa_on_sky_this_rotator_pa = $pa_rotator + $chip_pa_on_sky_for_rot0;

my $pa_slit_on_chip = Fitsheader::param_value( "SLIT-CPA" );

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

print_info();

#>>>Get Filename info from header
my $orig_dir  = Fitsheader::param_value( 'ORIG_DIR' );
my $filebase  = Fitsheader::param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

main::prompt_setup();

#See if should get tcs info
my $use_tcs = Fitsheader::param_value( 'USE_TCS' );
my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n\n";

my $next_index = $this_index;

my ( $A_dra, $A_ddec, $B_dra, $B_ddec ) = compute_offsets();
my ( $nudged_A_dra, $nudged_A_ddec, $nudged_B_dra, $nudged_B_ddec ) = compute_nudge_offsets();
print_offsets();

main::move_to_beam_A( 1 );
for( my $rptpat = 1; $rptpat <= $m_rptpat; $rptpat++ ){
  print ">-------------------------------------<\n";
  print "do ABBA sequence # $rptpat of $m_rptpat \n";

  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  main::move_to_beam_B( $rptpat );

  print "Take 2 Spectra in beam B\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  main::move_to_beam_A( $rptpat );
  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

}#End for loop


#Recenter telescope
print "++++++++++++++++++++++++++++++++++++++++++++\n";
print "offset to origin\n";
main::do_offset( 0, 0 );
main::offset_wait();

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
####SUBS
sub print_info{
  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "----------------PRESENT PARAMETERS FOR MOS DITHER----------------\n";
  print "-------(Check that the Rotator PA and Slit PA are correct)-------\n";
  print "\n";
  print "Number of times through pattern------------------------ $m_rptpat\n";
  print "Distance (arcsec) between A & B beams------------------ $m_throw\n";
  print "Distance (arcsec) to shift ABBA on repeats of pattern-- $m_ndgsz\n";
  print "\n";
  print "Rotator Position Angle (ROT_PA, deg)------------------- $pa_rotator\n";
  print "Chip PA on sky for this Rotator angle (deg)------------ ".
         "$chip_pa_on_sky_this_rotator_pa\n";
  print "\n";
  print "NET Position Angle of Slit on Sky(deg)----------------- $pa_slit_on_sky\n";
  print "-----------------------------------------------------------------\n\n";
}#Endsub print_info


sub prompt_setup{
  print "\n\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern: \n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type CLEAR to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";
  
  my $clear_home = <>;chomp $clear_home;
  my $valid = 0;
  until( $valid ){
    if( $clear_home =~ m/clear/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/home/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either CLEAR or HOME ";
      $clear_home = <>; chomp $clear_home;
    }
  }
  
  if( $clear_home =~ m/clear/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    clear_offset();
  }elsif( $clear_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub clear_offset{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;
  print "EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub clear_offset

sub compute_offsets{
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $m_throw * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $m_throw * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_offsets


sub compute_nudge_offsets{
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $dra_nudge  = $m_ndgsz * sin( $pa_sky_rad );
  my $ddec_nudge = $m_ndgsz * cos( $pa_sky_rad );

  $dra_nudge  = sprintf '%.2f', $dra_nudge;
  $ddec_nudge = sprintf '%.2f', $ddec_nudge;

  #print "RA_nudge = $dra_nudge, DEC_nudge = $ddec_nudge\n\n";
  my $A_dra_nudged  = $A_dra  + $dra_nudge;
  my $A_ddec_nudged = $A_ddec + $ddec_nudge;

  my $B_dra_nudged  = $B_dra  + $dra_nudge;
  my $B_ddec_nudged = $B_ddec + $ddec_nudge;

  return( $A_dra_nudged, $A_ddec_nudged, $B_dra_nudged, $B_ddec_nudged );
}#Endsub compute_nudge_offsets


sub print_offsets{
  print "----------------\n";
  print "Offsets to Beam A:        ";
  print "delta_RA = $A_dra, delta_DEC = $A_ddec\n";
  print "Offsets to Beam B:        ";
  print "delta_RA = $B_dra, delta_DEC = $B_ddec\n\n";

  print "Nudged Offset to Beam A:  ";
  print "delta_RA = $nudged_A_dra, delta_DEC = $nudged_A_ddec\n";
  print "Nudged Offset to Beam B:  ";
  print "delta_RA = $nudged_B_dra,  delta_DEC = $nudged_B_ddec\n\n";
  print "----------------\n";
}#Endsub print_offsets


sub move_to_beam_A{
  my $rptpat = $_[0];

  print "-------------------------------------\n";
  print "Move to Beam A\n";
  if( $rptpat == 1 ){
    main::do_offset( $A_dra, $A_ddec );

    #Offset wrt to first Beam A is zero by def.
    main::write_rel_offset_to_header( 0, 0 );

  }elsif( $rptpat > 1 ){
    main::do_offset( $nudged_A_dra, $nudged_A_ddec );

    #Write offset wrt first Beam A
    main::write_rel_offset_to_header( $nudged_A_dra  - $A_dra,
				      $nudged_A_ddec - $A_ddec );
  }

  main::offset_wait();
}#Endsub move_to_beam_A


sub move_to_beam_B{
  my $rptpat = $_[0];

  print "-------------------------------------\n";
  print "Move to Beam B\n";
  if( $rptpat == 1 ){
    main::do_offset( $B_dra, $B_ddec );

    #Offset wrt first Beam A
    main::write_rel_offset_to_header( $B_dra - $A_dra,
				      $B_ddec - $A_ddec );
  }elsif( $rptpat > 1 ){
    main::do_offset( $nudged_B_dra, $nudged_B_ddec );
    main::write_rel_offset_to_header( $nudged_B_dra - $A_dra,
				      $nudged_B_dra - $A_ddec );
  }
  main::offset_wait();
}#Endsub move_to_beam_B

sub use_tcs{
  my ( $use_tcs, $header ) = @_;

  if( $use_tcs ){
    ####call to tcs subroutines goes here
    ####Must match all 18 characters in header
    if( $telescop eq '4m                ' ){
      kpnoTCSinfo::getKPNOtcsinfo( $header );

    }elsif( $telescop eq '2m                ' ){
      kpnoTCSinfo::getKPNOtcsinfo( $header );

    }else{
      print "\a\a\a\aSome error here.  Not at either 4m or 2m at KPNO\n\n";
    }
  }else{
    print "___NOT___ getting tcs info\n\n";
  }

}#Endsub use_tcs


sub write_rel_offset_to_header{
  my ( $xoffset, $yoffset ) = @_;

  Fitsheader::setNumericParam( 'XOFFSET', $xoffset );
  Fitsheader::setNumericParam( 'YOFFSET', $yoffset );

  #print "SHOULD BE: ";
  #print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  Fitsheader::writeFitsHeader( $header );
  print "\n\n";

}#Endsub write_rel_offset_to_header


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "<><><><><><><><><><><><><><><><><><\n\n";
    print "take image with index= $this_index\n";

    main::use_tcs( $use_tcs, $header );

    main::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    #print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    #print "ENTERING idle_lut_status loop\n\n";
    main::idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub write_filename_header_param{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};

  my $index_str = ImgTests::parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  print "setting FILENAME to $filename\n";
  ImgTests::setFilenameParam( $filename );
  Fitsheader::writeFitsHeader( $header );

}#Endsub write_filename_header_param


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  #print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky
