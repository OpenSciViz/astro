#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: dtest1.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

my $tcs  = $ENV{UF_KPNO_TCS_PROG};
my $space = $ENV{SPACE};
my $head = $ENV{TCS_OFFSET_HEAD};
my $sep  = $ENV{TCS_OFFSET_SEP};
my $tail = $ENV{TCS_OFFSET_TAIL};

my $offset_cmd = $tcs.$space.$head."dX".$sep."dY".$tail;
print "offset command is $offset_cmd\n";

#ScalePcen( 1.0 );


sub ScalePcen{
  my $scale = $_[0];
  my $factor4m = 0.5; #Scale factor to apply to table used at KPNO 2.1m for the 4#
  my $factor2m = 1.0; #Scale factor to apply to table used at KPNO 2.1m for the 2m#
  my $factor = $factor4m;

  my @pcen2x2 = ( 0,0, 90,-90, -90, 90, -90,-90,  90, 90);
  my $double_gridsize=@pcen2x2;

  #***Scale the data for the 4m***#
  my $i;
  my $tmp;
  my @scaled_pcen=();
  for($i = 0; $i < $double_gridsize; $i += 1){
    $tmp = $factor * $scale * $pcen2x2[$i];
    @scaled_pcen = (@scaled_pcen, $tmp);
    #print "$i, $pcen2x2[$i], $scaled_pcen[$i]\n";
  }

  #***Now form strings of the x-y pairs***#
  my @offset_string = ();
  for($i = 0; $i < $double_gridsize; $i += 2){
    @offset_string =  (@offset_string,"$scaled_pcen[$i], $scaled_pcen[$i+1]");
  }

  #***Diagnostic print out***#
  my $gridsize = @offset_string;
  print "gridsize = $gridsize\n";
  for($i=0; $i < $gridsize; $i += 1){
    print "$offset_string[$i]\n";
  }

  return(@offset_string);

}#End sub ScalePcen
