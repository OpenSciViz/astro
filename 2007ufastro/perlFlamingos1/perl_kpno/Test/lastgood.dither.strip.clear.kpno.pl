#!/usr/local/bin/perl -w

## Execute dither patterns at KPNO 4m and 2.1m telescopes.
## This script uses absolute offsets wrt to the pointing center.

my $rcsId = q($Name:  $ $Id: lastgood.dither.strip.clear.kpno.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Dither_patterns qw/:all/;
use Fitsheader qw/:all/;
use Getopt::Long;
use GetYN;
use ImgTests;
use KPNO_offsets qw/:all/;
use kpnoTCSinfo qw/:all/;
use MCE4_acquisition qw/:all/;
use RW_Header_Params qw/:all/;
use UseTCS qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs, $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();
my ($expt, $nreads, $extra_timeout, $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($dpattern, $startpos, $net_scale_factor, $d_rptpos,
    $d_rptpat, $usenudge, $nudgesiz) = RW_Header_Params::get_dither_params();

## Load dither patterns
my @pat_x        = Dither_patterns::load_pattern_x( $dpattern );
my @pat_y        = Dither_patterns::load_pattern_y( $dpattern );
my @rel_offset_x = Dither_patterns::load_pat_rel_x_startpos( $startpos, $dpattern );
my @rel_offset_y = Dither_patterns::load_pat_rel_y_startpos( $startpos, $dpattern );

@pat_x           = Dither_patterns::scale_pattern_x_or_y( \@pat_x, $net_scale_factor );
@pat_y           = Dither_patterns::scale_pattern_x_or_y( \@pat_y, $net_scale_factor );

@rel_offset_x    = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_x, $net_scale_factor );
@rel_offset_y    = Dither_patterns::scale_pattern_x_or_y( \@rel_offset_y, $net_scale_factor );

my $east_translation  = Fitsheader::param_value( 'EASTTRAN' );
my $north_translation = Fitsheader::param_value( 'NORTTRAN' );
my $num_translations  = Fitsheader::param_value( 'NUM_TRAN' );
my $use_2by2          = Fitsheader::param_value( 'USE_2BY2' );
my $size_2by2         = Fitsheader::param_value( 'SIZE2BY2' );

my @pat2x2_x          = Dither_patterns::load_pattern_x('pat_2x2           ');
my @pat2x2_y          = Dither_patterns::load_pattern_y('pat_2x2           ');

my $pat2x2_netscale   = sprintf "%.2f", ($size_2by2/90);
@pat2x2_x             = Dither_patterns::scale_pattern_x_or_y( \@pat2x2_x, $pat2x2_netscale );
@pat2x2_y             = Dither_patterns::scale_pattern_x_or_y( \@pat2x2_y, $pat2x2_netscale );

#for( my $i = 0; $i < @pat2x2_x; $i++){
#  print "$pat2x2_x[$i], $pat2x2_y[$i]\n";
#}
#
#for( my $i = 0; $i < @pat_x; $i++){
#  print "$pat_x[$i], $pat_y[$i], $rel_offset_x[$i], $rel_offset_y[$i]\n";
#}

#>>>Get Filename info from header
#>>>Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

main::prompt_setup();
main::print_setup();
GetYN::query_ready();

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

my $x_nudge = 0;
my $y_nudge = 0;
my $next_index = $this_index;

my $X;
my $Y;
my $X_origin;
my $Y_origin;
my $last_pos_num  = @pat_x - 2;#num moves is @pat_x-1, then -1 to start count at 0

for( my $translation = 0; $translation <= $num_translations; $translation++ ){
  if( $translation eq 0 ){
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    print "This is the 0th translation.\n";
  }elsif( ($translation > 0) and ($translation <= $num_translations) ){
    print "\n";
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
    print "Doing translation #$translation out of $num_translations translations total\n";
    #print "SHOULD BE\n";
    #print "Doing relative offset: $east_translation, $north_translation\n";
    print "Doing an absolute offset by $east_translation, $north_translation\n";
    print "\n";
    #KPNO_offsets::do_relative_offset( $east_translation, $north_translation );
    KPNO_offsets::do_offset( $east_translation, $north_translation );
    #main::my_do_relative_offset( $east_translation, $north_translation );

    print "SHOULD BE waiting\n";
    KPNO_offsets::offset_wait();
    KPNO_offsets::clear_offsets();
    #main::my_clear_offsets();
    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  }

  if( !$use_2by2 ){
    print "\nDoing dither pattern without shifting it on a 2x2 dither.\n";
    $x_nudge = 0;
    $y_nudge = 0;

    $X_origin = $rel_offset_x[$startpos] + $x_nudge;
    $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

    my $this_pos;
    for( my $this_pos = $startpos; $this_pos <= $last_pos_num; $this_pos++){
      #-------------first position in object beam---------
      $X = $pat_x[$this_pos]+$x_nudge; $X = sprintf "%.3f", $X;
      $Y = $pat_y[$this_pos]+$y_nudge; $Y = sprintf "%.3f", $Y;

      $X_origin = $rel_offset_x[$this_pos] + $x_nudge; $X_origin = sprintf "%.3f", $X_origin;
      $Y_origin = $rel_offset_y[$this_pos] + $y_nudge; $Y_origin = sprintf "%.3f", $Y_origin;

      print "--------------------------------------------------------------------\n";
      print "Offseting by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n";
      print "(within major pointing center #".($translation+1)." out of ".($num_translations+1)." major pointing centers).\n";	
      KPNO_offsets::do_offset( $X, $Y );
      #print "SHOULD BE waiting\n";
      KPNO_offsets::offset_wait();

      #print "SHOULD BE: writing_rel_offset_to_header with $X_origin, $Y_origin\n";
      #print "\n";
      RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

      #print "SHOULD BE: Should image $d_rptpos times here\n\n";
      print "IMAGE $d_rptpos times here\n\n";
      $next_index = take_an_image( $header, $lut_name, $d_rptpos,
				   $file_hint, $next_index, $net_edt_timeout, $use_tcs);	
    }
  }elsif( $use_2by2 ){
    for( my $step = 0; $step < 4; $step++ ){
      #do pattern over and over
      print "\n*******  Pointing center # ".($step+1)." of 4 through 2x2 pattern  ******\n";
      $x_nudge = $pat2x2_x[$step];
      $y_nudge = $pat2x2_y[$step];

      $X_origin = $rel_offset_x[$startpos] + $x_nudge;
      $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

      my $this_pos;
      for( my $this_pos = $startpos; $this_pos <= $last_pos_num; $this_pos++){
     	#-------------first position in object beam---------
	$X = $pat_x[$this_pos]+$x_nudge; $X = sprintf "%.3f", $X;
	$Y = $pat_y[$this_pos]+$y_nudge; $Y = sprintf "%.3f", $Y;

	$X_origin = $rel_offset_x[$this_pos] + $x_nudge; $X_origin = sprintf "%.3f", $X_origin;
	$Y_origin = $rel_offset_y[$this_pos] + $y_nudge; $Y_origin = sprintf "%.3f", $Y_origin;
	
 	print "--------------------------------------------------------------------\n";
	print "Offseting by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n";
	print "(within major pointing center #".($translation+1)." out of ".($num_translations+1)." major pointing centers).\n";
	KPNO_offsets::do_offset( $X, $Y );
	#print "SHOULD BE waiting\n";
	KPNO_offsets::offset_wait();
	
	#print "SHOULD BE: writing_rel_offset_to_header with $X_origin, $Y_origin\n";
	#print "\n";
#	RW_Header_Params::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

	#print "SHOULD BE: Should image $d_rptpos times here\n\n";
	print "IMAGE $d_rptpos times here\n\n";
	$next_index = take_an_image( $header, $lut_name, $d_rptpos,
				     $file_hint, $next_index, $net_edt_timeout, $use_tcs);	
      }
    }
  }#End ifelse use_2by2
}#End translation for loop
print "\n";
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
print "Return to original initial pointing center, a total of $num_translations translations? ";
$reply = GetYN::get_yn();
if( $reply ){
  my $X_net_offset = -1 * $num_translations * $east_translation;
  my $Y_net_offset = -1 * $num_translations * $north_translation;
  print "should be translating by $X_net_offset, $Y_net_offset\n";

  #KPNO_offsets::do_relative_offset( $X_net_offset, $Y_net_offset );
  main::my_do_relative_offset( $X_net_offset, $Y_net_offset );
  print "SHOULD BE waiting\n";
  #main::offset_wait();

  main::my_clear_offsets();
  print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
  print "\n\n***************************************************\n";
  print      "*****          DITHER SCRIPT DONE           ******\n";
  print      "***** You probably should double check the  ******\n";
  print      "***** present coordinates against your      ******\n";
  print      "***** desired position.....                 ******\n";
  print     "***************************************************\n";
}elsif( !$reply ){
  print "\n\n***************************************************\n";
  print      "*****          DITHER SCRIPT DONE           ******\n";
  print      "***** Remember, the telescope is pointed at ******\n";
  print      "***** the _END_ of the dither strip......   ******\n";
  print     "***************************************************\n";
}

####SUBS
sub prompt_setup{
  print "\n\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern: \n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type CLEAR to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $clear_home = <>;chomp $clear_home;
  my $valid = 0;
  until( $valid ){
    if( $clear_home =~ m/clear/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/home/i ){
      $valid = 1;
    }elsif( $clear_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either CLEAR or HOME ";
      $clear_home = <>; chomp $clear_home;
    }
  }

  if( $clear_home =~ m/clear/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    KPNO_offsets::clear_offsets();
  }elsif( $clear_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    #print "SHOULD BE GETTING TCSINFO\n";
    print "Getting TCS info\n";
    UseTCS::use_tcs( $use_tcs, $header, $telescop );

    #print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECUTING:  acq cmd\n";
    print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    idle_lut_status( $expt, $nreads );

    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    #print "SHOULD BE DISPLAYING IMAGE\n";
    if( Fitsheader::param_value( 'DISPDS92' ) ){
      ImgTests::display_image( $file_hint, $next_index );
    }else{
      print "The better way to display images is off; see config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub print_setup{
  my $num_moves = @pat_x - 1;
  my $last_pos_num = $num_moves - 1;

  Dither_patterns::scale_pattern_msg();

  print "The pattern center has the following initial offsets wrt the catalog center:\n";
  print "The absolute offsets that will be applied,\n";
  print "and the offsets wrt the first image are:\n\n";
  print "pos:      Xabs       Yabs     X_wrt_Im1    Y_wrt_Im1\n";

  for( my $i = 0; $i < (@pat_x - 1); $i++){
    print  "$i:  ";
    printf '% 10.3f', $pat_x[$i];print ", ";
    printf '% 10.3f', $pat_y[$i];print ", ";
    printf '% 10.3f', $rel_offset_x[$i];print ", ";
    printf '% 10.3f', $rel_offset_y[$i];
    printf "\n";
  }
  print "\n\n";

  print "Number of images per position--------------  $d_rptpos\n";
  print "Start position, first time through pattern-  $startpos\n";
  print "num moves = $num_moves, lastpos= $last_pos_num\n";
  print "\n";
  if( $use_2by2 ){
    print "Will move the base dither pattern on a 2x2 dither grid.\n";
    print "Width of 2x2 dither grid (arcsec) -------  $size_2by2\n";
  }else{
    print "Will NOT the base dither pattern on a 2x2 dither grid.\n";
  }
  print "----------------------------------------------\n\n";
}#Endsub print_setup

####stolen routines
sub my_do_relative_offset{
  my ( $dX, $dY ) = @_;
  #This will do an absolute offset wrt the pointing center
  #Syntax is like 'tcs2|4m "tele offset = d_ra, d_dec"'
  #Offsets in arcsec

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_REL_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  #print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  print "$reply\n";
}#Endsub do_relative_offset


sub my_clear_offsets{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;
  print "SHOULD BE EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  print "$reply\n";
}#Endsub clear_offsets
