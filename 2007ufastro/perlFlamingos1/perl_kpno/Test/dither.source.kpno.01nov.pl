#!/usr/local/bin/perl -w

#
#Dither patterns at KPNO 4m and 2.1m telescopes
#With Absolute offsets wrt the pointing center
#

my $rcsId = q($Name:  $ $Id: dither.source.kpno.01nov.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use ImgTests;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use kpnoTCSinfo qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

#>>>Read necessary header params for exposure      <<<
#>>>And make the necessary additions/concatonations<<<
my $expt            = Fitsheader::param_value( 'EXP_TIME' );
my $nreads          = Fitsheader::param_value( 'NREADS'   );
my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

#>>>Get dither info from header
#my $dpattern =  'pat_2x2           ';
#my $startpos = 0;

my $dpattern = Fitsheader::param_value( 'DPATTERN' );#general pattern
my $startpos = Fitsheader::param_value( 'STARTPOS' );

my $d_scale  = Fitsheader::param_value( 'D_SCALE'  );
my $d_rptpos = Fitsheader::param_value( 'D_RPTPOS' );
my $d_rptpat = Fitsheader::param_value( 'D_RPTPAT' );#$d_rptpat= 2;

my $usenudge = Fitsheader::param_value( 'USENUDGE' );
my $nudgesiz = Fitsheader::param_value( 'NUDGESIZ' );

#Load dither patterns
my $pat_name_len = length $dpattern;
print "\npattern is $dpattern; length $pat_name_len\n";
my @pat_x = main::load_pattern_x( $dpattern );
my @pat_y = main::load_pattern_y( $dpattern );
main::scale_pattern_msg();
main::scale_pattern( \@pat_x, \@pat_y );

my $num_moves = @pat_x - 1;
my $last_pos_num  = $num_moves - 1;

my @rel_offset_x = main::load_pat_rel_x_startpos( $startpos, $dpattern );
my @rel_offset_y = main::load_pat_rel_y_startpos( $startpos, $dpattern );
main::scale_pattern( \@rel_offset_x, \@rel_offset_y );

my $X; my $Y;; my $X_origin; my $Y_origin;

print "pos:      Xrel       Yrel     X_wrt_Im1    Y_wrt_Im1\n";
for( my $i = 0; $i < (@pat_x - 1); $i++){
  print  "$i:  ";
  printf '% 10.3f', $pat_x[$i];print ", ";
  printf '% 10.3f', $pat_y[$i];print ", ";
  printf '% 10.3f', $rel_offset_x[$i];print ", ";
  printf '% 10.3f', $rel_offset_y[$i];
  printf "\n";
}
print "\n\n";

print "Number of images per position--------------  $d_rptpos\n";
print "Number of times through pattern------------  $d_rptpat\n";
print "Start position, first time through pattern-  $startpos\n";
print "num moves = $num_moves, lastpos= $last_pos_num\n";
print "----------------------------------------------\n\n";

#>>>Get Filename info from header
my $orig_dir  = Fitsheader::param_value( 'ORIG_DIR' );
my $filebase  = Fitsheader::param_value( 'FILEBASE' );
my $file_hint = $orig_dir . $filebase;

#>>>Double check existence of absolute path to write data into
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
print "This image's index will be $this_index\n";

#Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

main::prompt_setup();

#See if should get tcs info
my $use_tcs = Fitsheader::param_value( 'USE_TCS' );
my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n";
print "viido = $VIIDO\n";

my $x_nudge = 0; my $y_nudge = 0;
my $next_index = $this_index;
for( my $rpt_pat = 0; $rpt_pat < $d_rptpat; $rpt_pat++ ){
  #do pattern over and over
  print 
    "\n*******  Pass # ".($rpt_pat+1)." of $d_rptpat through $dpattern  ******\n\n";
  ( $x_nudge, $y_nudge ) = nudge_pointing( $d_rptpat, $rpt_pat );
  $X_origin = $rel_offset_x[$startpos] + $x_nudge;
  $Y_origin = $rel_offset_y[$startpos] + $y_nudge;

  if( $rpt_pat == 0 ){
    ###First move is offset move from origin to first position

    $X = $pat_x[$startpos]; $Y = $pat_y[$startpos];
    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
      main::do_offset( $X, $Y );
      main::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset by $X, $Y, to position $startpos (of 0 to $last_pos_num)\n\n";
	  print "***"; #main::offset_wait();
	 }
  }elsif( $rpt_pat > 0 ){
    ###Offset move from origin to first position
    $X = $pat_x[0] + $x_nudge;
    $Y = $pat_y[0] + $y_nudge;
    if( $VIIDO ){ 
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
      main::do_offset( $X, $Y );
      main::offset_wait();
    }else{print "++++++++++++++++++++++++++++++++++++++++++++\n";
	  print "***Should be doing the following:\n";
	  print "***offset by $X, $Y, back to position $startpos (of 0 to $last_pos_num)\n\n";
	   print "offset by $X, $Y, back to position $startpos\n";
	   print "***"; #main::offset_wait();
	 }
  }

  main::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

  #print "SHOULD BE: Should image $d_rptpos times here\n\n";
  print "IMAGE $d_rptpos times here\n\n";
  $next_index = take_an_image( $header, $lut_name, $d_rptpos,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  my $this_pos;
  for( my $i = $startpos; $i < $last_pos_num; $i+=1){
    #-------------first position in object beam---------
    $this_pos = $i+1;
    $X = $pat_x[$this_pos]+$x_nudge; $Y = $pat_y[$this_pos]+$y_nudge;

    $X_origin = $rel_offset_x[$this_pos] + $x_nudge;
    $Y_origin = $rel_offset_y[$this_pos] + $y_nudge;

    if( $VIIDO ){
      print "++++++++++++++++++++++++++++++++++++++++++++\n";
      print "offset by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
      main::do_offset( $X, $Y );
      main::offset_wait();
    }else{ print "***Should be doing the following:\n";
	   print "++++++++++++++++++++++++++++++++++++++++++++\n";
	   print "***offset by $X, $Y to position $this_pos (of 0 to $last_pos_num)\n\n";
	   print "***"; #main::offset_wait();
	 }

    main::write_rel_offset_to_header( $header, $X_origin, $Y_origin );

    #print "SHOULD BE: Should image $d_rptpos times here\n\n";
    print "IMAGE $d_rptpos times here\n\n";
    $next_index = take_an_image( $header, $lut_name, $d_rptpos,
    		   $file_hint, $next_index, $net_edt_timeout, $use_tcs);
  }
  #reset startpos to 0, in case it was somewhere else
  $startpos = 0;
}

#Recenter telescope, should already be in beam A
  if( $VIIDO ){
    print "++++++++++++++++++++++++++++++++++++++++++++\n";
    print "offset to origin\n";
    main::do_offset( 0, 0 );
    main::offset_wait();
  }else{ print "***Should be doing the following:\n";
	 print "***offset to origin\n";
	 print "***offset to position $startpos: 0, 0\n";
	 print "***"; #main::offset_wait();
       }

print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
####SUBS
sub load_pattern_x{
  my $pat_name = $_[0];
  my @ret_array;

  #SELECT absolute offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_X;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    print "found xpat of 3x3\n";
    @ret_array = @pat_3x3_X;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_X;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_X;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_X;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_X;
  }

  return @ret_array;
}#Endsub load_pattern_x


sub load_pattern_y{
  my $pat_name = $_[0];
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = @pat_2x2_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = @pat_3x3_Y;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = @pat_4x4_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = @pat_5x5_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = @pat_5box_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = @pat_5plus_Y;
  }

  return @ret_array;
}#Endsub load_pattern_y


sub load_pattern_wrap{
  my $pat_name = $_[0];
  my $ret_ar_ref;

  if( $pat_name eq 'pat_2x2           ' ){
    $ret_ar_ref = \@pat_2x2_wrap_X_Y;

  }elsif( $pat_name eq 'pat_3x3           ' ){
    $ret_ar_ref = \@pat_3x3_wrap_X_Y;

  }elsif( $pat_name eq 'pat_4x4           ' ){
    $ret_ar_ref = \@pat_4x4_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5x5           ' ){
    $ret_ar_ref = \@pat_5x5_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5box          ' ){
    $ret_ar_ref = \@pat_5box_wrap_X_Y;

  }elsif( $pat_name eq 'pat_5plus         ' ){
    $ret_ar_ref = \@pat_5plus_wrap_X_Y;
  }

  return $ret_ar_ref;
}#Endsub load_pattern_wrap


sub load_pat_rel_x_startpos{
  my ( $startpos, $pat_name ) = @_;
  my @ret_array;

  #SELECT relative offset patterns
  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_2x2_X );

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_3x3_X );

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_4x4_X );

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_X );

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5box_X );

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5plus_X );
  }

  return @ret_array;
}#Endsub load_pat_rel_x_startpos


sub load_pat_rel_y_startpos{
  my ( $startpos, $pat_name ) = @_;
  my @ret_array;

  if( $pat_name eq 'pat_2x2           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_2x2_Y );

  }elsif( $pat_name eq 'pat_3x3           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_3x3_Y );

  }elsif( $pat_name eq 'pat_4x4           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_4x4_Y );

  }elsif( $pat_name eq 'pat_5x5           ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5x5_Y );

  }elsif( $pat_name eq 'pat_5box          ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5box_Y );

  }elsif( $pat_name eq 'pat_5plus         ' ){
    @ret_array = get_pos_rel_start( $startpos, \@pat_5plus_Y );
  }

  return @ret_array;
}#Endsub load_pat_rel_Yorigin


sub scale_pattern_msg{
  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $extra_dither_scale   = Fitsheader::param_value( 'D_SCALE' );
  my $net_factor = $default_dither_scale * $extra_dither_scale;

  print "\n\nApplying extra scale factors for $ENV{THIS_TELESCOP}:\n";
  print "------------------------------------\n";
  print "Default dither scale    = $default_dither_scale\n";
  print "Extra dither scale      = $extra_dither_scale\n";
  print "Net picture frame width = ".($net_factor*90)." arcseconds\n\n";
}#Endsub scale_pattern_msg


sub scale_pattern{
  my ( $ar_x, $ar_y ) = @_;

  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $extra_dither_scale   = Fitsheader::param_value( 'D_SCALE' );
  my $net_factor = $default_dither_scale * $extra_dither_scale;

  my $pat_len = @$ar_x;
  for( my $i = 0; $i < $pat_len; $i++ ){
    $$ar_x[$i] = $$ar_x[$i] * $net_factor;
    $$ar_y[$i] = $$ar_y[$i] * $net_factor;
  }
}#Endsub scale_pattern


sub prompt_setup{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "Please make sure the telescope is at the source pointing center.\n";
  print "(no offsets)\n\n";

  print "Are you ready?  Enter y to continue; n to exit.  ";
  my $ready = GetYN::get_yn();
  if( !$ready ){
    die "\n\nExiting\n\n";
  }

}#Endsub prompt_setup


sub use_tcs{
  my ( $use_tcs, $header ) = @_;

  if( $use_tcs ){
    ####call to tcs subroutines goes here
    ####Must match all 18 characters in header
    if( $telescop eq '4m                ' ){
      kpnoTCSinfo::getKPNOtcsinfo( $header );
    }else{
      print "\a\a\a\a\a\n\n\n***********Some error getting kpnoTCSinfo\n\n";
    }
  }else{
    print "___NOT___ getting tcs info\n\n";
  }

}#Endsub use_tcs


sub nudge_pointing{
  my ( $d_rptpat, $rpt_pat ) = @_;
  my $rndm_num = rand;
  my $rdx = 0; my $rdy = 0; 

  if( param_value( 'USENUDGE' ) == 1 ){
    if( $rpt_pat >= 1 ){
      if( 0 <= $rndm_num and $rndm_num < 0.25 ){
	$rdx = 0; $rdy = 1;
      }elsif( 0.25 <= $rndm_num and $rndm_num < 0.5 ){
	$rdx = 1; $rdy = 0;
      }elsif( 0.50 <= $rndm_num and $rndm_num < 0.75 ){
	$rdx = 0; $rdy = -1;
      }elsif( 0.75 <= $rndm_num and $rndm_num <= 1.0 ){
	$rdx = -1; $rdy = 0;
      }
    }
    my $nudge = param_value( 'NUDGESIZ' );
    $rdx = $rdx * $nudge;
    $rdy = $rdy * $nudge;
  }
  print "\nNudge offsets by $rdx, $rdy\n\n";
  return( $rdx, $rdy );
}#Endsub nudge_pointing


sub write_rel_offset_to_header{
  my ( $header, $xoffset, $yoffset ) = @_;

  Fitsheader::setNumericParam( 'XOFFSET', $xoffset );
  Fitsheader::setNumericParam( 'YOFFSET', $yoffset );

  #print "SHOULD BE: ";
  #print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  Fitsheader::writeFitsHeader( $header );
  print "\n\n";

}#Endsub write_rel_offset_to_header


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    print "SHOULD BE GETTING TCSINFO\n";
    main::use_tcs( $use_tcs, $header );

    print "SHOULD BE calling write filename header param with $filebase $this_index\n";
    main::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECTUING:  acq cmd is \n$acq_cmd\n\n";
    print "acq cmd is \n$acq_cmd\n\n";
    acquire_image( $acq_cmd );

    #print "SHOULD BE DOING idle_lut_status loop\n\n";
    print "ENTERING idle_lut_status loop\n\n";
    idle_lut_status( $expt, $nreads );

    #move_file_for_index_sep( $file_hint, $next_index );#NOT used
    my $unlock_param = Fitsheader::param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


sub idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = $ENV{UFFRMSTATUS};
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = $ENV{LUT_TIMEOUT};
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) . 
            " seconds past the exposure time\n";
      print "Applying lut may take an additional $lut_timeout seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	      "IMAGE MAY NOT BE COMPLETE".
              "..........<<<<<<<<<<\n\n";
	print "If it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";

	print "If it looks like it has stalled while applying the LUT,\n";
	print "consider waiting a little longer, to see if it will apply the lut ";
	print "or\n";
	print "run ufstop.pl -clean -stop, in another $ENV{HOST} window.\n";
	print "Then see if the the next image is successfully taken.\n";

	#exit;
	print "Continue with this dither script ";
	my $continue = GetYN::get_yn();
	if( !$continue ){ 
	  die "\nEXITING\n".
	      "Consider lengthening the environment variable LUT_TIMEOUT.\n";
	}else{
	  $idle_notLutting = 1;
	}
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub idle_lut_status


sub write_filename_header_param{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};

  my $index_str = ImgTests::parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  print "setting FILENAME to $filename\n";
  ImgTests::setFilenameParam( $filename );
  Fitsheader::writeFitsHeader( $header );

}#Endsub write_filename_header_param


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  #print "\n\n";
}#Endsub offset_wait


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset
