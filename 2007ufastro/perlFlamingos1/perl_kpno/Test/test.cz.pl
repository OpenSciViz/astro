#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: test.cz.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

print "\n\n";
print ">------------------------------------------------------------------------------------\n";
print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
print ">\n";
print "> You have to choose what pointing center about which to execute the dither pattern: \n";
print "> Type HOME to continue dithering about the original pointing center\n";
print ">\n";
print "> OR,\n";
print ">\n";
print "> Type CLEAR to recenter the dither pattern to your present location.\n";
print ">\n";
print "> OR,\n";
print ">\n";
print "> Type q to quit\n\n";
print "\n";
print "> Entry:  ";

my $clear_home = <>;chomp $clear_home;
my $valid = 0;
until( $valid ){
  if( $clear_home =~ m/clear/i ){
    $valid = 1;
  }elsif( $clear_home =~ m/home/i ){
    $valid = 1;
  }elsif( $clear_home =~ m/q/i ){
    die "\n\nExiting\n\n";
  }else{
    print "Please enter either CLEAR or HOME ";
    $clear_home = <>; chomp $clear_home;
  }

}

if( $clear_home =~ m/clear/i ){
  print "\n\nRecentering the dither pattern to the present location\n";
}elsif( $clear_home =~ m/home/i ){
  print "\n\nDithering about the original pointing center\n";
}
print "\n\n";
