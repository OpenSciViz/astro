#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;
use Fitsheader qw/:all/;

my $DEBUG = 0;
#GetOptions( 'debug' => \$DEBUG );

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my $cnt = @ARGV;

if( $cnt < 2 ){
  die "\n\nUSAGE:  relative.offset.kpno.pl ra_offset dec_offset\n".
      "        offsets are relative to last offset position\n\n";
}

my $ra_offset  = shift;
my $dec_offset = shift;

if( $ra_offset =~ m/[a-zA-Z]/ or $dec_offset =~ m/[a-zA-Z]/ ){
  die "\nPlease enter numerical offsets.\n\n";
}


my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n\n";

#die "die: offsets are $ra_offset, $dec_offset\n";

do_offset( $ra_offset, $dec_offset );

sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_REL_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  print "EXECUTING RELATIVE OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


__END__

=head1 NAME

relative.offset.kpno.pl

=head1 Description

Move the telescope relative to the present position.

=head1 REVISION & LOCKER

$Name:  $

$Id: relative.offset.kpno.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut
