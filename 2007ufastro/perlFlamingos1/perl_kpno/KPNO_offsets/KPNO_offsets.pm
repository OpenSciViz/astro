package KPNO_offsets;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use KPNO_offsets ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
				   &clear_offsets
				   &do_offset
				   &do_relative_offset
				   &offset_wait
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
sub clear_offsets{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;
  print "EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub clear_offsets


sub do_offset{
  my ( $dX, $dY ) = @_;
  #This will do an absolute offset wrt the pointing center
  #Syntax is like 'tcs2|4m "tele offset = d_ra, d_dec"'
  #Offsets in arcsec

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub do_relative_offset{
  my ( $dX, $dY ) = @_;
  #This will do an absolute offset wrt the pointing center
  #Syntax is like 'tcs2|4m "tele offset = d_ra, d_dec"'
  #Offsets in arcsec

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_REL_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  #print "SHOULD BE EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_relative_offset


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  #print "\n\n";
}#Endsub offset_wait


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

KPNO_offsets - Perl extension for Flamingos

=head1 SYNOPSIS

  use KPNO_offsets qw/:all/;

=head1 DESCRIPTION

  clear_offset();
  do_offset( $dra, $ddec );          #absolute offset
  do_relative_offset( $dra, $ddec ); #relative offset
  do_offset_wait();

=head2 EXPORT

=head1 REVISION & LOCKER

$Name:  $

$Id: KPNO_offsets.pm,v 0.1 2003/05/22 15:51:02 raines Exp $

$Locker:  $

=head1 AUTHOR

SNR 09 Feb 2002

=head1 SEE ALSO

perl(1).

=cut
