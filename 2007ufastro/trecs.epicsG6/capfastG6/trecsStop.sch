[schematic2]
uniq 97
[tools]
[detail]
w 176 1035 100 0 n#96 hwin.hwin#95.in 144 1024 256 1024 ecad2.ecad2#48.INPB
w 1752 651 100 0 n#94 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IMSS 1632 640 1920 640 ecars.ecars#53.IMSS
w 1752 715 100 0 n#93 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IVAL 1632 704 1920 704 ecars.ecars#53.IVAL
w 272 1451 -100 0 c#89 ecad2.ecad2#48.DIR 256 1312 192 1312 192 1440 400 1440 400 1760 288 1760 inhier.DIR.P
w 240 1483 -100 0 c#90 ecad2.ecad2#48.ICID 256 1280 160 1280 160 1472 368 1472 368 1632 288 1632 inhier.ICID.P
w 512 1451 -100 0 c#91 ecad2.ecad2#48.VAL 576 1312 640 1312 640 1440 432 1440 432 1760 576 1760 outhier.VAL.p
w 544 1483 -100 0 c#92 ecad2.ecad2#48.MESS 576 1280 672 1280 672 1472 464 1472 464 1632 576 1632 outhier.MESS.p
w 1224 619 100 0 n#76 efanouts.efanouts#13.LNK2 992 880 1152 880 1152 608 1344 608 trecsSubSysCommand.trecsSubSysCommand#57.START
w 1048 923 100 0 n#74 efanouts.efanouts#13.LNK1 992 912 1152 912 1152 1232 1280 1232 estringouts.estringouts#69.SLNK
w 1528 1227 100 0 n#64 estringouts.estringouts#69.OUT 1536 1216 1568 1216 hwout.hwout#65.outp
w 640 843 100 0 n#49 ecad2.ecad2#48.STLK 576 832 752 832 efanouts.efanouts#13.SLNK
s 2624 2032 100 1792 2000/12/05
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Initial Layout
s 2016 2032 100 1792 A
s 2512 -240 100 1792 trecsStop.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2000/12/05
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 T-ReCS stop Command
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
[cell use]
use hwin -48 983 100 0 hwin#95
xform 0 48 1024
p -208 1024 100 0 -1 val(in):$(top)state
use outhier 592 1760 100 0 VAL
xform 0 560 1760
use outhier 592 1632 100 0 MESS
xform 0 560 1632
use inhier 224 1760 100 0 DIR
xform 0 288 1760
use inhier 208 1632 100 0 ICID
xform 0 288 1632
use estringouts 1280 1159 100 0 estringouts#69
xform 0 1408 1232
p 1344 1120 100 0 0 OMSL:supervisory
p 1344 1120 100 0 1 VAL:STOP
p 1472 1152 100 1024 1 name:$(top)stopDcCmd
use hwout 1568 1175 100 0 hwout#65
xform 0 1664 1216
p 1792 1216 100 0 -1 val(outp):$(top)dc:acqControl.A
use trecsSubSysCommand 1344 423 100 0 trecsSubSysCommand#57
xform 0 1488 608
p 1344 384 100 0 1 setCommand:cmd stop
p 1344 416 100 0 1 setSystem:sys dc
use ecars 1920 423 100 0 ecars#53
xform 0 2080 592
p 2080 416 100 1024 1 name:$(top)stopC
use ecad2 256 743 100 0 ecad2#48
xform 0 416 1056
p 320 704 100 0 1 INAM:trecsIsNullInit
p 320 656 100 0 1 SNAM:trecsIsStopProcess
p 416 736 100 1024 1 name:$(top)stop
p 592 832 75 1024 -1 pproc(STLK):PP
use efanouts 752 695 100 0 efanouts#13
xform 0 872 848
p 896 688 100 1024 1 name:$(top)stopFanout
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsStop.sch,v 0.1 2003/02/15 03:02:14 trecs beta $
