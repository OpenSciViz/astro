#
# These aliases are required before running this set-up file
#

alias addenv 'if (:${\!:1}\: !~ *:\!{:2}\:*) setenv \!:1 ${\!:1}\:\!:2'
alias preenv 'if (:${\!:1}\: !~ *:\!{:2}\:*) setenv \!:1 \!:2\:${\!:1}'

setenv TARGET_ARCH svgm5

# Which OS are we using?
set uname = `uname`
if( "$uname" == "SunOS" ) then
  setenv HOST_ARCH solaris
else
  setenv  HOST_ARCH Linux
endif

#
# Important paths to:
# VxWorks PPC Tornado 2.0.x
# Capfast
# EPICS 
#
setenv VX_DIR          /gemini/tornado2.0/ppc
setenv EPICS           /gemini/epics
#
# unless we want the older stuff:
if( "$1" == "-1" ) then
#setenv VX_DIR          /gemini/windriver
setenv VX_DIR          /gemini/tornado1.0.1
#setenv EPICS           /gemini/epics3.12.2GEM6T
setenv EPICS           /gemini/epics3.13.4
endif
#
id p3 >& /dev/null
if( "$status" == "0" ) then
  setenv CAPDIR ~p3/wcs/bin
else
  setenv CAPDIR /home/p3/wcs/bin
endif

#
# Setup for VxWorks PPC Tornado
#
if( ! $?MANPATH ) setenv MANPATH /usr/share/man
addenv MANPATH         ${VX_DIR}/host/man
addenv MANPATH         ${VX_DIR}/host/sun4-solaris2/man
preenv PATH            ${VX_DIR}/host/sun4-solaris2/bin
preenv PATH            ${VX_DIR}/host/sun4-solaris2/powerpc-wrs-vxworks/bin
#addenv PATH            ${VX_DIR}/host/sun4-solaris2/m68k-wrs-vxworks/bin
#addenv PATH            ${VX_DIR}/host/sun4-solaris2/mips-wrs-vxworks/bin
setenv WIND_BASE       ${VX_DIR}
setenv WIND_HOST_TYPE  sun4-solaris2
setenv WIND_REGISTRY   antila
setenv LM_LICENSE_FILE ${VX_DIR}/.wind/license/10.lic
setenv GCC_EXEC_PREFIX ${VX_DIR}/host/${WIND_HOST_TYPE}/lib/gcc-lib/

#
# Setup for Capfast
#
if (-d ${CAPDIR}) then
    addenv PATH            ${CAPDIR}
endif

#
# Setup for epics extensions
#
preenv PATH            ${EPICS}/extensions/bin/${HOST_ARCH}
preenv PATH            ${EPICS}/base/bin/${HOST_ARCH}
preenv PATH            ${EPICS}/base/tools

#
# Setup for using Tcl/Tk Interface to Channel Access
#
setenv TCL_LIBRARY     ${EPICS}/extensions/src/tcllib/lib/tcl
setenv TK_LIBRARY      ${EPICS}/extensions/src/tcllib/lib/tk
setenv DP_LIBRARY      ${EPICS}/extensions/src/tcllib/lib/dp

