[schematic2]
uniq 80
[tools]
[detail]
w -60 1499 100 2 n#79 hwin.hwin#68.in -64 1504 -64 1504 esirs.esirs#27.INP
w 676 1083 100 2 n#72 hwin.hwin#71.in 672 1088 672 1088 esirs.esirs#33.INP
w -60 1083 100 2 n#70 hwin.hwin#69.in -64 1088 -64 1088 esirs.esirs#28.INP
w 1380 1499 100 2 n#62 hwin.hwin#61.in 1376 1504 1376 1504 esirs.esirs#39.INP
w 676 667 100 2 n#60 hwin.hwin#59.in 672 672 672 672 esirs.esirs#34.INP
w 1380 1083 100 2 n#58 hwin.hwin#57.in 1376 1088 1376 1088 esirs.esirs#38.INP
s 2624 2064 100 1792 2000/11/11
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 trecsSadObsConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2002/05/02
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 SAD Observation Configuration
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2624 2032 100 1792 2002/05/02
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Added local file and dir names
s 2016 2032 100 1792 B
[cell use]
use hwin -256 1463 100 0 hwin#68
xform 0 -160 1504
p -352 1536 100 0 -1 val(in):$(dc)hardwareG.VALM
use hwin 1184 1463 100 0 hwin#61
xform 0 1280 1504
p 1088 1536 100 0 -1 val(in):$(dc)physOutG.VALC
use hwin 480 631 100 0 hwin#59
xform 0 576 672
p 384 704 100 0 -1 val(in):$(dc)physOutG.VALB
use hwin 1184 1047 100 0 hwin#57
xform 0 1280 1088
p 1088 1120 100 0 -1 val(in):$(dc)physOutG.VALA
use hwin -256 1047 100 0 hwin#69
xform 0 -160 1088
p -352 1120 100 0 -1 val(in):$(dc)obsControlG.VALI
use hwin 480 1047 100 0 hwin#71
xform 0 576 1088
p 384 1120 100 0 -1 val(in):$(dc)obsControlG.VALJ
use esirs 2080 7 100 0 esirs#63
xform 0 2288 160
p 2144 -32 100 0 1 SCAN:Passive
p 2240 0 100 1024 -1 name:$(top)chopThrow
use esirs -64 7 100 0 esirs#48
xform 0 144 160
p 0 -32 100 0 1 SCAN:Passive
p 96 0 100 1024 -1 name:$(top)rotatorRate
use esirs 2080 423 100 0 esirs#46
xform 0 2288 576
p 2144 384 100 0 1 SCAN:Passive
p 2240 416 100 1024 -1 name:$(top)skyBackground
use esirs -64 1671 100 0 esirs#7
xform 0 144 1824
p 0 1632 100 0 1 SCAN:Passive
p 96 1664 100 1024 -1 name:$(top)externalTemp
use esirs -64 1255 100 0 esirs#27
xform 0 144 1408
p 0 1216 100 0 1 SCAN:1 second
p 96 1248 100 1024 -1 name:$(top)obsMode
use esirs -64 839 100 0 esirs#28
xform 0 144 992
p 0 800 100 0 1 SCAN:1 second
p 96 832 100 1024 -1 name:$(top)lambdaLow
use esirs -64 423 100 0 esirs#29
xform 0 144 576
p 0 384 100 0 1 SCAN:Passive
p 96 416 100 1024 -1 name:$(top)emissivity
use esirs 672 1671 100 0 esirs#31
xform 0 880 1824
p 736 1632 100 0 1 SCAN:Passive
p 832 1664 100 1024 -1 name:$(top)cameraMode
use esirs 672 1255 100 0 esirs#32
xform 0 880 1408
p 736 1216 100 0 1 SCAN:Passive
p 832 1248 100 1024 -1 name:$(top)obsType
use esirs 672 839 100 0 esirs#33
xform 0 880 992
p 736 800 100 0 1 SCAN:1 second
p 832 832 100 1024 -1 name:$(top)lambdaHigh
use esirs 672 423 100 0 esirs#34
xform 0 880 576
p 736 384 100 0 1 SCAN:1 second
p 832 416 100 1024 -1 name:$(top)saveFreq
use esirs 1376 423 100 0 esirs#37
xform 0 1584 576
p 1440 384 100 0 1 SCAN:Passive
p 1504 416 100 1024 -1 name:$(top)airMass
use esirs 1376 839 100 0 esirs#38
xform 0 1584 992
p 1440 800 100 0 1 SCAN:1 second
p 1536 832 100 1024 -1 name:$(top)frameTime
use esirs 1376 1255 100 0 esirs#39
xform 0 1584 1408
p 1440 1216 100 0 1 SCAN:1 second
p 1536 1248 100 1024 -1 name:$(top)onSourceTime
use esirs 1376 1671 100 0 esirs#40
xform 0 1584 1824
p 1440 1632 100 0 1 SCAN:Passive
p 1536 1664 100 1024 -1 name:$(top)dataMode
use esirs 2080 1655 100 0 esirs#41
xform 0 2288 1808
p 2144 1616 100 0 1 SCAN:Passive
p 2240 1648 100 1024 -1 name:$(top)obsID
use esirs 2080 1239 100 0 esirs#42
xform 0 2288 1392
p 2144 1200 100 0 1 SCAN:Passive
p 2240 1232 100 1024 -1 name:$(top)photonTime
use esirs 2080 823 100 0 esirs#43
xform 0 2288 976
p 2144 784 100 0 1 SCAN:Passive
p 2240 816 100 1024 -1 name:$(top)skyNoise
use esirs 672 7 100 0 esirs#49
xform 0 880 160
p 736 -32 100 0 1 SCAN:Passive
p 736 0 100 0 -1 name:$(top)localFileDir
use esirs 1376 7 100 0 esirs#50
xform 0 1584 160
p 1440 -32 100 0 1 SCAN:Passive
p 1440 0 100 0 -1 name:$(top)localFileName
use changeBar 1984 2023 100 0 changeBar#51
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#52
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadObsConfig.sch,v 0.0 2003/04/25 15:21:16 hon Developmental $
