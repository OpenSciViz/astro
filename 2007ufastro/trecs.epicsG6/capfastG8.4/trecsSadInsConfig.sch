[schematic2]
uniq 58
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-Recs Instrument Configuration
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2001/02/11
s 2096 -272 100 1792 Author: NWR
s 2512 -240 100 1792 trecsSadInsConfig.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2000/11/11
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Revised to match ICD changes
s 2480 2032 100 1792 NWR
s 2624 2032 100 1792 2001/02/11
[cell use]
use esirs 1280 839 100 0 esirs#55
xform 0 1488 992
p 1472 640 100 0 0 SNAM:ufSetWaveLength
p 1360 832 100 0 1 name:$(top)adjWaveLength
use esirs 1280 1255 100 0 esirs#54
xform 0 1488 1408
p 1344 1216 100 0 1 SCAN:Passive
p 1344 1248 100 0 1 name:$(top)agenthostIP
use esirs 544 423 100 0 esirs#34
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)windowName
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)slitName
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)sectorName
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)lyotName
use esirs -192 7 100 0 esirs#30
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)lensName
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)imagingMode
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)gratingName
use esirs -192 1255 100 0 esirs#27
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)filterName
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)apertureName
use esirs 544 7 100 0 esirs#48
xform 0 752 160
p 608 -32 100 0 1 SCAN:Passive
p 704 0 100 1024 -1 name:$(top)opticalEfficiency
use esirs 1280 1671 100 0 esirs#50
xform 0 1488 1824
p 1344 1632 100 0 1 SCAN:Passive
p 1472 1472 100 0 0 SNAM:ufSetWaveLength
p 1440 1664 100 1024 -1 name:$(top)wavelength
use esirs 1280 423 100 0 esirs#56
xform 0 1488 576
p 1344 416 100 0 1 name:$(top)fiducialSteps
use esirs 1280 7 100 0 esirs#57
xform 0 1488 160
p 1344 0 100 0 1 name:$(top)fiducialAngle
use changeBar 1984 2023 100 0 changeBar#51
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#52
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadInsConfig.sch,v 0.0 2003/04/25 15:21:16 hon Developmental $
