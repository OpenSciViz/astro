[schematic2]
uniq 136
[tools]
[detail]
w 1074 331 100 0 n#134 efanouts.efanouts#100.LNK2 704 320 1504 320 elongouts.elongouts#129.SLNK
w 1410 363 100 0 n#133 hwin.hwin#130.in 1312 480 1376 480 1376 352 1504 352 elongouts.elongouts#129.DOL
w 1826 299 100 0 n#132 elongouts.elongouts#129.OUT 1760 288 1952 288 hwout.hwout#131.outp
w 962 1323 100 0 n#122 efanouts.efanouts#100.LNK1 704 352 864 352 864 1312 1120 1312 eseqs.eseqs#93.SLNK
w 330 283 100 0 n#119 ecad2.ecad2#48.STLK 256 272 464 272 efanouts.efanouts#100.SLNK
w 1032 1611 100 0 n#98 eseqs.eseqs#93.DOL2 1120 1600 992 1600 992 1568 hwin.hwin#96.in
w 1032 1643 100 0 n#97 eseqs.eseqs#93.DOL1 1120 1632 992 1632 992 1664 hwin.hwin#95.in
w 1496 1611 100 0 n#94 eseqs.eseqs#93.LNK2 1440 1600 1600 1600 1600 1632 junction
w 1608 1643 100 0 n#94 eseqs.eseqs#93.LNK1 1440 1632 1824 1632 ecars.ecars#53.IVAL
w -48 891 -100 0 c#89 ecad2.ecad2#48.DIR -64 752 -128 752 -128 880 80 880 80 1200 -32 1200 inhier.DIR.P
w -80 923 -100 0 c#90 ecad2.ecad2#48.ICID -64 720 -160 720 -160 912 48 912 48 1072 -32 1072 inhier.ICID.P
w 192 891 -100 0 c#91 ecad2.ecad2#48.VAL 256 752 320 752 320 880 112 880 112 1200 256 1200 outhier.VAL.p
w 224 923 -100 0 c#92 ecad2.ecad2#48.MESS 256 720 352 720 352 912 144 912 144 1072 256 1072 outhier.MESS.p
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 Trecs continue Command
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2003/05/28
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsContinue.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2002/11/11
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Corrected nod complete link
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2003/05/28
[cell use]
use changeBar 1984 2023 100 0 changeBar#124
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#135
xform 0 2336 2032
use hwout 1952 247 100 0 hwout#131
xform 0 2048 288
p 2464 288 100 512 -1 val(outp):$(top)dc:TCSISnodComplG.J
use hwin 800 1527 100 0 hwin#96
xform 0 896 1568
p 640 1568 100 0 -1 val(in):$(CAR_IDLE)
use hwin 800 1623 100 0 hwin#95
xform 0 896 1664
p 640 1664 100 0 -1 val(in):$(CAR_BUSY)
use hwin 1120 439 100 0 hwin#130
xform 0 1216 480
p 1088 480 100 0 -1 val(in):1
use elongouts 1504 231 100 0 elongouts#129
xform 0 1632 320
p 1536 192 100 0 1 OMSL:closed_loop
p 1504 224 100 0 1 name:$(top)continueSignal
p 1760 288 75 768 -1 pproc(OUT):PP
use efanouts 464 135 100 0 efanouts#100
xform 0 584 288
p 512 128 100 0 1 name:$(top)continueFanout
p 736 352 75 1280 -1 pproc(LNK1):PP
p 736 320 75 1280 -1 pproc(LNK2):PP
use eseqs 1120 1223 100 0 eseqs#93
xform 0 1280 1472
p 1200 1184 100 0 1 DLY1:0.0e+00
p 1200 1152 100 0 1 DLY2:0.5e+00
p 1312 1216 100 1024 1 name:$(top)continueBusy
p 1088 1632 75 1280 -1 pproc(DOL1):NPP
p 1088 1600 75 1280 -1 pproc(DOL2):NPP
p 1456 1632 75 1024 -1 pproc(LNK1):PP
p 1456 1600 75 1024 -1 pproc(LNK2):PP
use outhier 272 1072 100 0 MESS
xform 0 240 1072
use outhier 272 1200 100 0 VAL
xform 0 240 1200
use inhier -112 1072 100 0 ICID
xform 0 -32 1072
use inhier -96 1200 100 0 DIR
xform 0 -32 1200
use ecars 1824 1351 100 0 ecars#53
xform 0 1984 1520
p 1984 1344 100 1024 1 name:$(top)continueC
use ecad2 -64 183 100 0 ecad2#48
xform 0 96 496
p 0 144 100 0 1 INAM:trecsIsNullInit
p 0 96 100 0 1 SNAM:trecsIsContinueProcess
p 96 176 100 1024 1 name:$(top)continue
p 272 272 75 1024 -1 pproc(STLK):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsContinue.sch,v 1.2 2003/05/29 17:28:32 hon beta $
