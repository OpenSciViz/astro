[schematic2]
uniq 276
[tools]
[detail]
w 1618 1099 100 0 n#274 egenSub.egenSub#130.FLNK 1792 288 1920 288 1920 1088 1376 1088 1376 1280 1504 1280 egenSub.egenSub#104.SLNK
w 898 331 100 0 n#273 ecad20.ecad20#227.PLNK 192 32 352 32 352 320 1504 320 egenSub.egenSub#130.SLNK
w 1938 811 100 0 n#271 egenSub.egenSub#130.OUTD 1792 800 2144 800 hwout.hwout#272.outp
w 1938 683 100 0 n#270 egenSub.egenSub#130.OUTF 1792 672 2144 672 hwout.hwout#269.outp
w 1938 747 100 0 n#267 egenSub.egenSub#130.OUTE 1792 736 2144 736 hwout.hwout#268.outp
w 818 747 100 0 n#266 ecad20.ecad20#227.VALL 192 736 1504 736 egenSub.egenSub#130.INPE
w 818 683 100 0 n#265 ecad20.ecad20#227.VALM 192 672 1504 672 egenSub.egenSub#130.INPF
w 818 811 100 0 n#264 ecad20.ecad20#227.VALK 192 800 1504 800 egenSub.egenSub#130.INPD
w 818 555 100 0 n#262 ecad20.ecad20#227.VALO 192 544 1504 544 egenSub.egenSub#130.INPH
w 818 619 100 0 n#261 ecad20.ecad20#227.VALN 192 608 1504 608 egenSub.egenSub#130.INPG
w 482 -245 100 0 n#260 ecad20.ecad20#227.VALR 192 352 288 352 288 -256 736 -256 736 -96 800 -96 trecsIsEcUpdate.trecsIsEcUpdate#194.ENABLE
w 1938 555 100 0 n#238 egenSub.egenSub#130.OUTH 1792 544 2144 544 hwout.hwout#240.outp
w 1938 619 100 0 n#237 egenSub.egenSub#130.OUTG 1792 608 2144 608 hwout.hwout#239.outp
w 962 875 100 0 n#249 ecad20.ecad20#227.VALC 192 1312 480 1312 480 864 1504 864 egenSub.egenSub#130.INPC
w 994 939 100 0 n#252 ecad20.ecad20#227.VALB 192 1376 544 1376 544 928 1504 928 egenSub.egenSub#130.INPB
w 1026 1003 100 0 n#242 ecad20.ecad20#227.VALA 192 1440 608 1440 608 992 1504 992 egenSub.egenSub#130.INPA
w 274 11 100 0 n#255 ecad20.ecad20#227.STLK 192 0 416 0 trecsSubSysCommand.trecsSubSysCommand#57.START
w 1938 875 100 0 n#212 egenSub.egenSub#130.OUTC 1792 864 2144 864 hwout.hwout#213.outp
w 1090 -21 100 0 n#202 trecsIsEcUpdate.trecsIsEcUpdate#194.FLNK 1088 -32 1152 -32 trecsSubSysCommand.trecsSubSysCommand#197.START
w 1106 203 100 0 n#201 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IMSS 704 32 768 32 768 192 1504 192 1504 0 junction
w 1490 11 100 0 n#201 trecsSubSysCommand.trecsSubSysCommand#197.CAR_IMSS 1440 0 1600 0 ecars.ecars#53.IMSS
w 1106 235 100 0 n#200 trecsSubSysCommand.trecsSubSysCommand#57.CAR_IVAL 704 96 736 96 736 224 1536 224 1536 64 junction
w 1490 75 100 0 n#200 trecsSubSysCommand.trecsSubSysCommand#197.CAR_IVAL 1440 64 1600 64 ecars.ecars#53.IVAL
w 722 -21 100 0 n#191 trecsSubSysCommand.trecsSubSysCommand#57.DONE 704 -32 800 -32 trecsIsEcUpdate.trecsIsEcUpdate#194.START
w 1938 1643 100 0 n#186 egenSub.egenSub#104.OUTF 1792 1632 2144 1632 hwout.hwout#187.outp
w 1938 1707 100 0 n#163 egenSub.egenSub#104.OUTE 1792 1696 2144 1696 hwout.hwout#166.outp
w 1938 1771 100 0 n#162 egenSub.egenSub#104.OUTD 1792 1760 2144 1760 hwout.hwout#167.outp
w 1938 1451 100 0 n#160 egenSub.egenSub#104.OUTI 1792 1440 2144 1440 hwout.hwout#165.outp
w 1314 1451 100 0 n#157 hwin.hwin#158.in 1184 1440 1504 1440 egenSub.egenSub#104.INPI
w 1314 1515 100 0 n#156 hwin.hwin#155.in 1184 1504 1504 1504 egenSub.egenSub#104.INPH
w 1314 1579 100 0 n#153 hwin.hwin#154.in 1184 1568 1504 1568 egenSub.egenSub#104.INPG
w 1314 1643 100 0 n#152 hwin.hwin#151.in 1184 1632 1504 1632 egenSub.egenSub#104.INPF
w 1314 1707 100 0 n#148 hwin.hwin#147.in 1184 1696 1504 1696 egenSub.egenSub#104.INPE
w 1314 1771 100 0 n#145 hwin.hwin#146.in 1184 1760 1504 1760 egenSub.egenSub#104.INPD
w 1314 1835 100 0 n#141 hwin.hwin#142.in 1184 1824 1504 1824 egenSub.egenSub#104.INPC
w 1314 1899 100 0 n#140 hwin.hwin#139.in 1184 1888 1504 1888 egenSub.egenSub#104.INPB
w 1314 1963 100 0 n#136 hwin.hwin#135.in 1184 1952 1504 1952 egenSub.egenSub#104.INPA
w 1938 939 100 0 n#129 egenSub.egenSub#130.OUTB 1792 928 2144 928 hwout.hwout#132.outp
w 1938 1003 100 0 n#128 egenSub.egenSub#130.OUTA 1792 992 2144 992 hwout.hwout#131.outp
w 1938 1515 100 0 n#127 egenSub.egenSub#104.OUTH 1792 1504 2144 1504 hwout.hwout#134.outp
w 1938 1579 100 0 n#126 egenSub.egenSub#104.OUTG 1792 1568 2144 1568 hwout.hwout#133.outp
w 1938 1835 100 0 n#108 egenSub.egenSub#104.OUTC 1792 1824 2144 1824 hwout.hwout#65.outp
w 1938 1899 100 0 n#107 egenSub.egenSub#104.OUTB 1792 1888 2144 1888 hwout.hwout#60.outp
w 1938 1963 100 0 n#105 egenSub.egenSub#104.OUTA 1792 1952 2144 1952 hwout.hwout#95.outp
w -112 1739 -100 0 c#89 ecad20.ecad20#227.DIR -128 1632 -192 1632 -192 1728 16 1728 16 1920 -96 1920 inhier.DIR.P
w -144 1771 -100 0 c#90 ecad20.ecad20#227.ICID -128 1600 -224 1600 -224 1760 -16 1760 -16 1856 -96 1856 inhier.ICID.P
w 136 1739 -100 0 c#91 ecad20.ecad20#227.VAL 192 1632 256 1632 256 1728 64 1728 64 1920 192 1920 outhier.VAL.p
w 168 1771 -100 0 c#92 ecad20.ecad20#227.MESS 192 1600 288 1600 288 1760 96 1760 96 1856 192 1856 outhier.MESS.p
s 2016 -80 100 1792 H
s 2240 -80 100 1792 Added save frequency
s 2480 -80 100 1792 WNR
s 2624 -80 100 1792 2003/02/18
s -448 464 100 0 override nod params
s -448 400 100 0 override save freq
s -416 656 100 0 save frequency
s 1952 816 100 0 readout mode
s 1248 816 100 0 readout mode
s 1952 624 100 0 save freq
s 1248 624 100 0 save freq
s 1952 688 100 0 nod settle time
s 1952 752 100 0 nod time
s 1248 752 100 0 nod time
s 1248 688 100 0 nod settle time
s 272 624 100 0 save freq
s 2016 16 100 1792 E
s 2240 16 100 1792 Added frame time stuff
s 2480 16 100 1792 WNR
s 2624 16 100 1792 2002/07/14
s 1952 560 100 0 frame time
s 1248 560 100 0 frame time
s -416 592 100 0 frame time
s -448 336 100 0 override frame time
s -416 976 100 0 emmissivity
s -416 1040 100 0 temperature
s -416 1104 100 0 airmass
s -416 1168 100 0 sky background
s -416 1232 100 0 sky noise
s 2624 112 100 1792 2001/09/12
s 2480 112 100 1792 WNR
s 2240 112 100 1792 added chop throw 
s 2016 112 100 1792 B
s 1952 880 100 0 chop throw
s 1248 880 100 0 chop throw
s -416 1296 100 0 chop throw
s 2624 144 100 1792 2000/12/05
s 2480 144 100 1792 WNR
s 2240 144 100 1792 Initial Layout
s 2016 144 100 1792 A
s 2512 -240 100 1792 trecsObsSetup.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2003/02/18
s 2320 -240 100 1792 Rev: H
s 2432 -192 100 256 Trecs observationSetup Command
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 1248 1008 100 0 observing mode
s 1248 944 100 0 photon time
s 1184 1968 100 0 camera mode
s 1184 1904 100 0 filter name
s 1184 1840 100 0 central wavelength
s 1184 1456 100 0 throughput
s 1184 1776 100 0 grating name
s 1184 1712 100 0 lyot name
s 1184 1648 100 0 sector name
s 1184 1584 100 0 slit name
s 1184 1520 100 0 window name
s 1952 1008 100 0 observing mode
s 1952 944 100 0 photon time
s 1952 1968 100 0 camera mode
s 1952 1904 100 0 filter name
s 1952 1840 100 0 central wavelength
s 1952 1456 100 0 throughput
s 1952 1776 100 0 grating name
s 1952 1712 100 0 lyot name
s 1952 1648 100 0 sector name
s 1952 1584 100 0 slit name
s 1952 1520 100 0 window name
s -416 1424 100 0 observing mode
s -416 1360 100 0 photon time
s 2624 80 100 1792 2001/11/12
s 2480 80 100 1792 LTF
s 2240 80 100 1792 corrected addresses of output
s 2016 80 100 1792 C
s 2016 48 100 1792 D
s 2240 48 100 1792 Re-arranged pass-through data
s 2480 48 100 1792 WNR
s 2624 48 100 1792 2001/12/26
s -416 912 100 0 rotator rate
s 272 560 100 0 frame time
s 272 1008 100 0 emmissivity
s 272 1072 100 0 temperature
s 272 1136 100 0 airmass
s 272 1200 100 0 sky background
s 272 1264 100 0 sky noise
s 272 1328 100 0 chop throw
s 272 1456 100 0 observing mode
s 272 1392 100 0 photon time
s 272 944 100 0 rotator rate
s 272 368 100 0 auto temp control
s -416 528 100 0 auto temp control
s 2016 -16 100 1792 F
s 2240 -16 100 1792 Added temperature control
s 2480 -16 100 1792 WNR
s 2624 -16 100 1792 2002/09/17
s -416 848 100 0 nod time
s -416 784 100 0 nod settle time
s -416 720 100 0 readout mode
s 272 752 100 0 nod time
s 272 688 100 0 nod settle time
s 272 816 100 0 readout mode
s 2016 -48 100 1792 G
s 2240 -48 100 1792 Added nod and readout type
s 2480 -48 100 1792 WNR
s 2624 -48 100 1792 2002/11/23
[cell use]
use changeBar 1984 -121 100 0 changeBar#275
xform 0 2336 -80
use changeBar 1984 -25 100 0 changeBar#257
xform 0 2336 16
use changeBar 1984 71 100 0 changeBar#215
xform 0 2336 112
use changeBar 1984 103 100 0 changeBar#214
xform 0 2336 144
use changeBar 1984 7 100 0 changeBar#217
xform 0 2336 48
use changeBar 1984 39 100 0 changeBar#218
xform 0 2336 80
use changeBar 1984 -57 100 0 changeBar#259
xform 0 2336 -16
use changeBar 1984 -89 100 0 changeBar#263
xform 0 2336 -48
use hwout 2144 759 100 0 hwout#272
xform 0 2240 800
p 2368 800 100 0 -1 val(outp):$(top)dc:obsControl.G
use hwout 2144 631 100 0 hwout#269
xform 0 2240 672
p 2368 672 100 0 -1 val(outp):$(top)dc:obsControl.C
use hwout 2144 695 100 0 hwout#268
xform 0 2240 736
p 2368 736 100 0 -1 val(outp):$(top)dc:obsControl.B
use hwout 2144 503 100 0 hwout#240
xform 0 2240 544
p 2368 544 100 0 -1 val(outp):$(top)dc:obsControl.S
use hwout 2144 567 100 0 hwout#239
xform 0 2240 608
p 2368 608 100 0 -1 val(outp):$(top)dc:obsControl.L
use hwout 2144 823 100 0 hwout#213
xform 0 2240 864
p 2368 864 100 0 -1 val(outp):$(top)dc:obsControl.R
use hwout 2144 1463 100 0 hwout#134
xform 0 2240 1504
p 2368 1504 100 0 -1 val(outp):$(top)dc:obsControl.P
use hwout 2144 1527 100 0 hwout#133
xform 0 2240 1568
p 2368 1568 100 0 -1 val(outp):$(top)dc:obsControl.O
use hwout 2144 887 100 0 hwout#132
xform 0 2240 928
p 2368 928 100 0 -1 val(outp):$(top)dc:obsControl.F
use hwout 2144 951 100 0 hwout#131
xform 0 2240 992
p 2368 992 100 0 -1 val(outp):$(top)dc:obsControl.E
use hwout 2144 1911 100 0 hwout#95
xform 0 2240 1952
p 2368 1952 100 0 -1 val(outp):$(top)dc:obsControl.D
use hwout 2144 1783 100 0 hwout#65
xform 0 2240 1824
p 2368 1824 100 0 -1 val(outp):$(top)dc:obsControl.J
use hwout 2144 1847 100 0 hwout#60
xform 0 2240 1888
p 2368 1888 100 0 -1 val(outp):$(top)dc:obsControl.H
use hwout 2144 1399 100 0 hwout#165
xform 0 2240 1440
p 2368 1440 100 0 -1 val(outp):$(top)dc:obsControl.Q
use hwout 2144 1655 100 0 hwout#166
xform 0 2240 1696
p 2368 1696 100 0 -1 val(outp):$(top)dc:obsControl.M
use hwout 2144 1719 100 0 hwout#167
xform 0 2240 1760
p 2368 1760 100 0 -1 val(outp):$(top)dc:obsControl.I
use hwout 2144 1591 100 0 hwout#187
xform 0 2240 1632
p 2368 1632 100 0 -1 val(outp):$(top)dc:obsControl.N
use ecad20 -128 -89 100 0 ecad20#227
xform 0 32 800
p -32 1024 100 0 0 FTVL:STRING
p -32 928 100 0 0 FTVO:STRING
p -32 832 100 0 0 FTVR:LONG
p -64 -128 100 0 1 INAM:trecsIsNullInit
p -64 -160 100 0 1 SNAM:trecsIsObsSetupProcess
p -64 -96 100 0 1 name:$(top)observationSetup
use hwin 992 1911 100 0 hwin#135
xform 0 1088 1952
p 656 1952 100 0 -1 val(in):$(top)instrumentSetup.VALA
use hwin 992 1847 100 0 hwin#139
xform 0 1088 1888
p 656 1888 100 0 -1 val(in):$(top)instrumentSetup.VALE
use hwin 992 1783 100 0 hwin#142
xform 0 1088 1824
p 656 1824 100 0 -1 val(in):$(top)instrumentSetup.VALF
use hwin 992 1719 100 0 hwin#146
xform 0 1088 1760
p 656 1760 100 0 -1 val(in):$(top)instrumentSetup.VALK
use hwin 992 1655 100 0 hwin#147
xform 0 1088 1696
p 656 1696 100 0 -1 val(in):$(top)instrumentSetup.VALL
use hwin 992 1591 100 0 hwin#151
xform 0 1088 1632
p 656 1632 100 0 -1 val(in):$(top)instrumentSetup.VALN
use hwin 992 1527 100 0 hwin#154
xform 0 1088 1568
p 656 1568 100 0 -1 val(in):$(top)instrumentSetup.VALO
use hwin 992 1463 100 0 hwin#155
xform 0 1088 1504
p 656 1504 100 0 -1 val(in):$(top)instrumentSetup.VALP
use hwin 992 1399 100 0 hwin#158
xform 0 1088 1440
p 656 1440 100 0 -1 val(in):$(top)instrumentSetup.VALQ
use trecsSubSysCommand 416 -185 100 0 trecsSubSysCommand#57
xform 0 560 0
p 416 -224 100 0 1 setCommand:cmd obsSetup
p 416 -192 100 0 1 setSystem:sys dc
use trecsSubSysCommand 1152 -217 100 0 trecsSubSysCommand#197
xform 0 1296 -32
p 1152 -256 100 0 1 setCommand:cmd obsSetup
p 1152 -224 100 0 1 setSystem:sys ec
use trecsIsEcUpdate 800 -217 100 0 trecsIsEcUpdate#194
xform 0 944 -32
use egenSub 1504 231 100 0 egenSub#130
xform 0 1648 656
p 1281 5 100 0 0 FTA:STRING
p 1281 5 100 0 0 FTB:STRING
p 1281 -27 100 0 0 FTC:STRING
p 1281 -59 100 0 0 FTD:STRING
p 1281 -91 100 0 0 FTE:STRING
p 1281 -155 100 0 0 FTF:STRING
p 1281 -155 100 0 0 FTG:STRING
p 1281 -187 100 0 0 FTH:STRING
p 1281 -219 100 0 0 FTI:STRING
p 1281 -251 100 0 0 FTJ:STRING
p 1281 5 100 0 0 FTVA:STRING
p 1281 5 100 0 0 FTVB:STRING
p 1281 -27 100 0 0 FTVC:STRING
p 1281 -59 100 0 0 FTVD:STRING
p 1281 -91 100 0 0 FTVE:STRING
p 1281 -155 100 0 0 FTVF:STRING
p 1281 -155 100 0 0 FTVG:STRING
p 1281 -187 100 0 0 FTVH:STRING
p 1281 -219 100 0 0 FTVI:STRING
p 1281 -251 100 0 0 FTVJ:STRING
p 1568 192 100 0 1 INAM:trecsIsNullGInit
p 1568 160 100 0 1 SNAM:trecsIsCopyGProcess
p 1568 224 100 768 1 name:$(top)obsSetupPreset2G
use egenSub 1504 1191 100 0 egenSub#104
xform 0 1648 1616
p 1281 965 100 0 0 FTA:STRING
p 1281 965 100 0 0 FTB:STRING
p 1281 933 100 0 0 FTC:STRING
p 1281 901 100 0 0 FTD:STRING
p 1281 869 100 0 0 FTE:STRING
p 1281 805 100 0 0 FTF:STRING
p 1281 805 100 0 0 FTG:STRING
p 1281 773 100 0 0 FTH:STRING
p 1281 741 100 0 0 FTI:STRING
p 1281 709 100 0 0 FTJ:STRING
p 1281 965 100 0 0 FTVA:STRING
p 1281 965 100 0 0 FTVB:STRING
p 1281 933 100 0 0 FTVC:STRING
p 1281 901 100 0 0 FTVD:STRING
p 1281 869 100 0 0 FTVE:STRING
p 1281 805 100 0 0 FTVF:STRING
p 1281 805 100 0 0 FTVG:STRING
p 1281 773 100 0 0 FTVH:STRING
p 1281 741 100 0 0 FTVI:STRING
p 1281 709 100 0 0 FTVJ:STRING
p 1568 1152 100 0 1 INAM:trecsIsNullGInit
p 1568 1120 100 0 1 SNAM:trecsIsCopyGProcess
p 1568 1184 100 768 1 name:$(top)obsSetupPreset1G
use outhier 208 1920 100 0 VAL
xform 0 176 1920
use outhier 208 1856 100 0 MESS
xform 0 176 1856
use inhier -160 1920 100 0 DIR
xform 0 -96 1920
use inhier -176 1856 100 0 ICID
xform 0 -96 1856
use ecars 1600 -217 100 0 ecars#53
xform 0 1760 -48
p 1760 -224 100 1024 1 name:$(top)observationSetupC
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsObsSetup.sch,v 0.6 2003/02/19 08:01:35 trecs beta $
