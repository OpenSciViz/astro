[schematic2]
uniq 48
[tools]
[detail]
s 2624 2064 100 1792 2000/11/12
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 trecsSadCcConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 T-Recs CC Configure
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2624 2032 100 1792 2001/01/25
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Removed EC records
s 2016 2032 100 1792 B
[cell use]
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#47
xform 0 2336 2032
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)aprtrWhlPos
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)fltrWhl1Pos
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)fltrWhl2Pos
use esirs -192 7 100 0 esirs#30
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)gratingPos
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)lyotWhlPos
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)pplImgPos
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)sectWhlPos
use esirs 544 423 100 0 esirs#34
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)slitWhlPos
use esirs 544 7 100 0 esirs#35
xform 0 752 160
p 608 -32 100 0 1 SCAN:Passive
p 704 0 100 1024 -1 name:$(top)winChngrPos
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadCcConfig.sch,v 0.1 2003/02/11 21:40:03 rambold beta $
