#!/usr/local/bin/perl
use GD::Graph::points;

@vac;
@detarr; # ls340[0]  x1
@coldfng; # ls340[1] x2
@cryostg1; #ls218[0] x1
@coldplt; # ls218[1] x2
@slitwhl; # ls218[2] x3
@passvsh; # ls218[3] x4
@winturr; # ls218[4] x5
@coldstrp; # ls218[5] x6
@coldedg; # ls218[6] x7
@coldctr; # ls218[7] x8

$vaccnt = 0;
@vacx;

$ls218cnt1 = 0;
@ls218x1;
$ls218cnt2 = 0;
@ls218x2;
$ls218cnt3 = 0;
@ls218x3;
$ls218cnt4 = 0;
@ls218x4;
$ls218cnt5 = 0;
@ls218x5;
$ls218cnt6 = 0;
@ls218x6;
$ls218cnt7 = 0;
@ls218x7;
$ls218cnt8 = 0;
@ls218x8;

$ls340cnt1 = 0;
@ls340x1;
$ls340cnt2 = 0;
@ls340x2;

open(LOG, "</data/trecsCryoTestJun15.log"); 

while( <LOG> ) {
  chomp;
  if( index($_,"Vacuum") >= 0 ) {
    push @vacx, "$vaccnt"; $vaccnt++;
    $v = substr($_, 1+rindex($_,' '));
    $not = rindex($v, "9.99E+09");
    if( $not < 0 ) { if( $v ne "" ) { push @vac, $v; } } 
  }
  if( index($_,"LakeShore 340") >= 0 && index($_,"kelvin") >= 0) {
    $ls340 = substr($_, index($_, '+'));
    $psc =  index($ls340, ';');
    $v = substr($ls340, 0, $psc);
    if( $v ne "" ) { push @detarr, $v; push @ls340x1, "$ls340cnt1"; $ls340cnt1++; }
    $v = substr($ls340, $psc + 1);
    if( $v ne "" ) { push @coldfng, $v; push @ls340x2, "$ls340cnt2"; $ls340cnt2++; }
  }
  if( index($_,"LakeShore 218") >= 0 ) {
     $ls218 = substr($_, index($_, '+'));
    $pc = index($ls218, ',');
    $ppc = 0;
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len);
    if( $v ne "" ) { push @cryostg1, $v; push @ls218x1, "$ls218cnt1"; $ls218cnt1++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @coldplt, $v; push @ls218x2, "$ls218cnt2"; $ls218cnt2++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v =substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @slitwhl, $v; push @ls218x3, "$ls218cnt3"; $ls218cnt3++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @passvsh, $v; push @ls218x4, "$ls218cnt4"; $ls218cnt4++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @winturr, $v; push @ls218x5, "$ls218cnt5"; $ls218cnt5++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @coldstrp, $v; push @ls218x6, "$ls218cnt6"; $ls218cnt6++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @coldedg, $v; push @ls218x7, "$ls218cnt7"; $ls218cnt7++; }

    $ppc = 1 + $pc;
    $pc = index($ls218, ',', $ppc);
    $len = $pc - $ppc;
    $v = substr($ls218, $ppc, $len); 
    if( $v ne "" ) { push @coldctr, $v; push @ls218x8, "$ls218cnt8"; $ls218cnt8++; }
  }
}

#print "@vac\n";
#print "@detarr\n"; # ls340 x1
#print "@coldfng\n"; # ls340 x2
#print "@cryostg1\n"; # ls218 x1
#print "@coldplt\n"; # ls218 x2
#print "@slitwhl\n"; # ls218 x3
#print "@passvsh\n"; # ls218 x4
#print "@winturr\n"; # ls218 x5
#print "@coldstrp\n"; # ls218 x6
#print "@coldedg\n"; # ls218 x7
#print "@coldctr\n"; # ls218 x8

$gvac = new GD::Graph::points(600, 400);
$gvac->set( x_label => 'Time',
	    y_label => 'Vacuum Pressure in Torr',
	    title => "UF TReCS CryoStat Vacuum Pressure",
	    y_max_value => 1.1*$vac[0],
	    y_label_skip => 1,
	    x_label_skip => @vac/10,
	    x_labels_vertical => 1,
	    x_label_position => 1/2,
	    markers => [ 1, 7 ],
	    marker_size => 0,
	    transparent => 1,
	    t_margin => 1, 
	    b_margin => 1, 
	    l_margin => 1, 
	    r_margin => 1,
	    two_axes => 1,
);

@data = ( [@vacx], [@vac] );
$imgvac = $gvac->plot(\@data);
open(IMG, ">/data/vac.png");
binmode IMG;
print IMG $imgvac->png;
close(IMG);

$gcoldfng = new GD::Graph::points(600, 400);
$gcoldfng->set( x_label => 'Time',
	    y_label => 'Cold Finger Temperature in Kelvin',
	    title => 'UF TReCS CryoStat Cold Finger Temperature',
	    y_max_value => 1.1*$coldfng[0],
	    y_label_skip => 1,
	    x_label_skip => @coldfng/10,
	    x_labels_vertical => 1,
	    x_label_position => 1/2,
	    markers => [ 1, 7 ],
	    marker_size => 0,
	    transparent => 1,
	    t_margin => 1, 
	    b_margin => 1, 
	    l_margin => 1, 
	    r_margin => 1,
	    two_axes => 1,
);

@data = ( [@ls340x2], [@coldfng] );
$imgcoldfng = $gcoldfng->plot(\@data);
open(IMG, ">/data/coldfng.png");
binmode IMG;
print IMG $imgcoldfng->png;
close(IMG);

$gcoldplt = new GD::Graph::points(600, 400);
$gcoldplt->set( x_label => 'Time',
	    y_label => 'Cold Plate Temperature in Kelvin',
	    title => 'UF TReCS CryoStat Cold Plate Temperature',
	    y_max_value => 1.1*$coldplt[0],
	    y_label_skip => 1,
	    x_label_skip => @coldplt/10,
	    x_labels_vertical => 1,
	    x_label_position => 1/2,
	    markers => [ 1, 7 ],
	    marker_size => 0,
	    transparent => 1,
	    t_margin => 1, 
	    b_margin => 1, 
	    l_margin => 1, 
	    r_margin => 1,
	    two_axes => 1,
);

@data = ( [@ls218x2], [@coldplt] );
$imgcoldplt = $gcoldplt->plot(\@data);
open(IMG, ">/data/coldplt.png");
binmode IMG;
print IMG $imgcoldplt->png;
close(IMG);
