package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;

public class Flam2Helper {
    JFrame mainFrame ;
    UFGraphPanel [] graphPanels;
    JTextField fileNameField;

    public Flam2Helper(String [] args) {
	mainFrame = new JFrame();
	graphPanels = new UFGraphPanel[3];
	String [] strs0 = {"CamSetpA","CamSetpB"};
	String [] strs1 = {"MosVac","CamVac"};
	String [] strs2 = {"MosCold1","MosBenc2","MosBenc3","MosBenc4","CamCold5","CamBenc6","CamBenc7","CamBenc8"};
	graphPanels[0] = new UFGraphPanel(strs0,0);
	graphPanels[1] = new UFGraphPanel(strs1,2);
	graphPanels[2] = new UFGraphPanel(strs2,4);
	JTabbedPane jtpMain = new JTabbedPane();
	JTabbedPane jtpGraph = new JTabbedPane();
	JPanel overallGraphPanel = new JPanel();
	fileNameField = new JTextField("/nfs/flam2sparc/share/data/environment/current");
	JButton saveColorsButton = new JButton("Save Colors");
	saveColorsButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			String home = UFExecCommand.getEnvVar("HOME");
			PrintWriter pw = new PrintWriter(new File(home+"/.ufflam2helper"));
			for (int i=0; i<3; i++) {
			    String [] nams = graphPanels[i].getSensorNames();
			    Color [] cols = graphPanels[i].getColors();
			    if (nams.length == cols.length) {
				for (int j=0; j<nams.length; j++) {
				    pw.println(nams[j]+" "+cols[j].getRed()+" "+cols[j].getGreen()+" "+cols[j].getBlue());
				}
			    } else {
				System.err.println("Flam2Helper> Number of sensor names != number of colors!");
			    }
			}
			pw.close();
		    } catch (Exception e) {
			System.err.println("Flam2Helper> Problem saving color prefs: "+e.toString());
		    }
		}
	    });
	jtpGraph.add(getJGraphPanel(graphPanels[0]),"CamSetPt");
	jtpGraph.add(getJGraphPanel(graphPanels[1]),"Pressure");
	jtpGraph.add(getJGraphPanel(graphPanels[2]),"Cam/Mos");
	overallGraphPanel.setLayout(new RatioLayout());	
	overallGraphPanel.add("0.01,0.01;0.99,0.95",jtpGraph);
	overallGraphPanel.add("0.125,0.96;0.20,0.04",new JLabel("Log file:"));
	overallGraphPanel.add("0.325,0.96;0.50,0.04",fileNameField);
	overallGraphPanel.add("0.90,0.96;0.10,0.04",saveColorsButton);
	jtpMain.add(new UFTail(args),"Tail");
	jtpMain.add(overallGraphPanel,"Graphs");
	mainFrame.setContentPane(jtpMain);
	mainFrame.setSize(600,600);
	mainFrame.setVisible(true);
	mainFrame.setDefaultCloseOperation(3);
	mainFrame.setTitle("Helpy Helperton");
    }

    public JPanel getJGraphPanel(final UFGraphPanel ufgp) {
	final JPanel retVal = new JPanel();
	retVal.setLayout(new RatioLayout());
	JButton paramButton = new JButton("Params");
	paramButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    Point origin = mainFrame.getLocation();
		    Dimension totalSize = mainFrame.getSize();
		    JDialog dialogBox = ufgp.getParameterDialog();
		    dialogBox.setLocation((int)(origin.getX() + totalSize.getWidth()/2),
					  (int)(origin.getY() + totalSize.getHeight()/2));
		    dialogBox.setVisible(true);
		    mainFrame.repaint();
		}
	    });
	JButton legendButton = new JButton("Legend");
	legendButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    Point origin = mainFrame.getLocation();
		    Dimension totalSize = mainFrame.getSize();
		    JDialog dialogBox = ufgp.getLegendDialog();
		    dialogBox.setLocation((int)(origin.getX() + totalSize.getWidth()/2),
					  (int)(origin.getY() + totalSize.getHeight()/2));
		    dialogBox.setVisible(true);
		    mainFrame.repaint();		    
		}
	    });
	final JCheckBox showGridBox = new JCheckBox("Show Grid",true);
	ufgp.setDrawGrid(showGridBox.isSelected());
	showGridBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    ufgp.setDrawGrid(showGridBox.isSelected());
		    mainFrame.repaint();
		}
	    });
	final JCheckBox autoScaleBox = new JCheckBox("Autoscale",true);
	ufgp.setAutoscale(autoScaleBox.isSelected());
	autoScaleBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    ufgp.setAutoscale(autoScaleBox.isSelected());
		    mainFrame.repaint();
		}
	    });
	final JButton graphButton = new JButton("Start");
	graphButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (graphButton.getText().trim().toLowerCase().equals("start")){
			if (ufgp.isThreadAlive()) ufgp.stopThread();
			ufgp.setLogFileName(fileNameField.getText());
			ufgp.startThread();
			graphButton.setText("Stop");
		    } else {
			ufgp.stopThread();
			graphButton.setText("Start");
		    }
		}
	    });
	retVal.add("0.01,0.01;0.99,0.95",ufgp);
	retVal.add("0.01,0.96;0.20,0.04",legendButton);
	retVal.add("0.25,0.96;0.20,0.04",showGridBox);
	retVal.add("0.45,0.96;0.15,0.04",autoScaleBox);
	retVal.add("0.70,0.96;0.20,0.04",paramButton);
	retVal.add("0.90,0.96;0.10,0.04",graphButton);
	return retVal;
    }


    public static void main(String [] args) {
	new Flam2Helper(args);
    }

}
