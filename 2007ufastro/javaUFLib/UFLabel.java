//Title:        UFLabel.java
//Version:      2.0
//Copyright:    Copyright (c) Frank Varosi
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Extension of JLabel class.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;

//===============================================================================
/**
 * Creates text Labels with always same prefix text and color blue.
 * Beeps and changes color to red if text "ERR" or "WARN" is displayed.
 */
public class UFLabel extends JLabel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFLabel.java,v 1.13 2005/11/29 20:20:29 drashkin Exp $";

    protected String _prefix = "";
    protected boolean _doBeep = true;
    protected boolean _needReset = false;
    protected int _minDecDigits = 1;
    protected int _maxDecDigits = 4;
    public static final Color _darkBlue = new Color(0,0,144); //default color is dark blue (almost black).
    public static final Color _darkRed =  new Color(155,0,0);
    public static final Color _darkGreen = new Color(0,77,0);
    public static final Color _darkYellow =  new Color(177,111,0);
    protected Color _color = _darkBlue;
//-------------------------------------------------------------------------------
    /**
     * Basic Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel(String prefix) {
	try  {
	    super.setText( prefix );
	    super.setForeground( _color );
	    _prefix = prefix;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, Color color ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( color );
	    _prefix = prefix;
	    _color = color;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, int minDecDigits ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( _color );
	    _prefix = prefix;
	    _minDecDigits = minDecDigits;
	    if( _minDecDigits < 0 ) _minDecDigits = 2;
	    if( _maxDecDigits < _minDecDigits ) _maxDecDigits = _minDecDigits;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, int minDecDigits, Color color ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( color );
	    _prefix = prefix;
	    _color = color;
	    _minDecDigits = minDecDigits;
	    if( _minDecDigits < 0 ) _minDecDigits = 2;
	    if( _maxDecDigits < _minDecDigits ) _maxDecDigits = _minDecDigits;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, int minDecDigits, int maxDecDigits ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( _color );
	    _prefix = prefix;
	    _minDecDigits = minDecDigits;
	    _maxDecDigits = maxDecDigits;
	    if( _minDecDigits < 0 ) _minDecDigits = 2;
	    if( _maxDecDigits < 0 ) _maxDecDigits = 3 + _minDecDigits;
	    if( _maxDecDigits < _minDecDigits ) _maxDecDigits = _minDecDigits;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, int minDecDigits, int maxDecDigits, Color color ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( color );
	    _prefix = prefix;
	    _color = color;
	    _minDecDigits = minDecDigits;
	    _maxDecDigits = maxDecDigits;
	    if( _minDecDigits < 0 ) _minDecDigits = 2;
	    if( _maxDecDigits < 0 ) _maxDecDigits = 3 + _minDecDigits;
	    if( _maxDecDigits < _minDecDigits ) _maxDecDigits = _minDecDigits;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------

    public void newPrefix( String prefix ) { _prefix = prefix; }

    public void resetColor() { _color = _darkBlue; setForeground( _color ); }

    public void newColor( Color color ) { _color = color; setForeground( _color ); }

    public void newColor( boolean error )
    {
	if( error )
	    _color = _darkRed;
	else
	    _color = _darkGreen;

	setForeground( _color );
    }

//-------------------------------------------------------------------------------
  /**
   * Override setText method to always use _prefix.
   */
    public void setText( String text )
    {
	if( text.indexOf("ERR") >= 0 || text.indexOf("WARN") >= 0 ||
	    text.indexOf("FAIL") >= 0 || text.indexOf("Fail") >= 0 )
	    {
		setForeground( _darkRed );
		_needReset = true;
		if( _doBeep ) Toolkit.getDefaultToolkit().beep();
	    }
	else if( _needReset )
	    {
		setForeground( _color );
		_needReset = false;
	    }

	super.setText( _prefix + "  " + text );
    }
//-------------------------------------------------------------------------------

    public void setText( String text, boolean noBeep )
    {
	if( noBeep ) _doBeep = false;
	setText( text );
	_doBeep = true;
    }
//-------------------------------------------------------------------------------

    public void setText( String text, Color color )
    {
	setForeground( color );
	_needReset = false;
	setText( text );
	_needReset = true;
    }
//-------------------------------------------------------------------------------

    public String getSubText()
    {
	String text = super.getText();
	if( _prefix == null ) return text;
	return text.substring( _prefix.length() ).trim();
    }
//-------------------------------------------------------------------------------

    public void setText( int value ) { setText( Integer.toString( value ) ); }

    public void setText( float value ) { setText( truncFormat( value, _minDecDigits, _maxDecDigits ) ); }

    public void setText( double value ) { setText( truncFormat( value, _minDecDigits, _maxDecDigits ) ); }

//-------------------------------------------------------------------------------

    public static String truncFormat( double value ) { return truncFormat( value, 1 ); }

    public static String truncFormat( double value, int minDecDigits ) {
	return truncFormat( value, minDecDigits, 9 );
    }

    public static String truncFormat( double value, int minDecDigits, int maxDecDigits )
    {
	String valTxt = Double.toString( value ).trim();

	if( valTxt.indexOf(".") < 0 ) return valTxt;

	int ndigits = valTxt.indexOf(".") + minDecDigits + 1;
	double vabs = Math.abs( value );
	int ndd = maxDecDigits - minDecDigits;
	if( vabs < 100 && ndd > 0 ) ++ndigits;
	if( vabs < 10  && ndd > 1 ) ++ndigits;
	if( vabs < 1   && ndd > 2 ) ++ndigits;

	if( ndigits < valTxt.length() ) {
	    String vText = valTxt.substring( 0, ndigits );
	    if( valTxt.indexOf("E") > 0 )
		return( vText + valTxt.substring( valTxt.indexOf("E"), valTxt.length() ) );
	    else
		return vText;
	}
	else return valTxt;
    }
} //end of class UFLabel

