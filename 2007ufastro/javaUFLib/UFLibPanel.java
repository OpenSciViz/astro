package javaUFLib;

//Title:        UFLibPanel
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003,4,5
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  generic extension of JPanel, used by other UF tools (e.g. JCI and JDD)

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javaUFProtocol.*;

//===============================================================================
/**
 * Generic extension of JPanel, so that certain methods/buttons can be used by UFLib components,
 * or by other UF tools such as the JCI and JDD for CanariCam.
 * @author Frank Varosi
 */

public class UFLibPanel extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFLibPanel.java,v 1.20 2005/11/29 20:20:29 drashkin Exp $";

    protected static final int DEFAULT_TIMEOUT = 9000; //milliseconds.
    protected static final int CONNECT_TIMEOUT = 7000;
    protected static final int HANDSHAKE_TIMEOUT = 8000;
    protected int socTimeout = DEFAULT_TIMEOUT;

    protected Socket _socket;
    protected String _Host = "";
    protected int _Port = 0;
    protected String serverHandshake = null;
    public String serverName = "Agent";

    protected String className = this.getClass().getName();
    protected String clientName = className;
    protected String LocalHost;

    public JPanel connectPanel = new JPanel();
    protected JButton connectButton = new JButton("Connect");
    protected UFHostPortPanel hostPortPanel = new UFHostPortPanel( _Host , _Port );

    // for agent Transaction info:
    public UFLabel statusAction   = new UFLabel("Action :");
    public UFLabel statusResponse = new UFLabel("Response :", new Color(0,99,0) ); //dark green.
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel default constructor.
     * Calls private method, createPanel(), which does all component initialization.
     */
    public UFLibPanel() {
	try {
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public UFLibPanel( String host ) {
	try {
	    setHost( host );
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public UFLibPanel( String host, int port ) {
	try {
	    setHost( host );
	    setPort( port );
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public UFLibPanel( String host, int port, String handshake ) {
	try {
	    setHost( host );
	    setPort( port );
	    serverHandshake = handshake;
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    /**
     *Component creation
     */
    private void createPanel() throws Exception
    {
	if( serverHandshake == null ) {
	    connectButton.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e) {
			if( connectToAgent() ) getNewParams();
		    }
		});
	}
	else {
	    connectButton.addActionListener(new ActionListener()
		{
		    public void actionPerformed(ActionEvent e) { connectToServer(); }
		});
	}

	connectPanel.setLayout(new GridLayout(2,1));
	connectPanel.add( connectButton );  
	connectPanel.add( hostPortPanel );
    }
//------------------------------------------------------------------------
// generic methods to be overriden.

    public void getNewParams() {}
    public void getNewParams( String callerName ) {}
    public void setNewStatus( UFStrings newStatus ) {}
    public void updateFrames( UFFrameConfig frameConfig ) {}
    public void updateObsInfo( UFStrings obsInfo ) {}

//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#Connect button action performed
     */
    public boolean connectToAgent() { return connect("connectToAgent> ", clientName); }

    public boolean connectToServer() { return connect("connectToServer> ", serverHandshake); }

    public Socket socket() { return _socket; }

//-------------------------------------------------------------------------------
    /**
     *  Connect via socket to the Agent or Server.
     */
    protected boolean connect(String cType, String handshake)
    {
	connectButton.setText("Connecting");

	if( _socket != null ) { // close connection if one exists
	    try {
		_socket.close();
		indicateConnectStatus(false);
	    }
	    catch (IOException ioe) {
		String message = cType + ioe.toString();
		statusAction.setText( message );
		System.err.println(className + "::" + message);
	    }
	}

	try {
	    _Host = getHostField();
	    _Port = getPortField();
	    String message = cType + "port=" + _Port + " @ host = " + _Host;
	    statusAction.setText( message );
	    System.out.println( className + "::" + message );
	    InetSocketAddress agentIPsoca = new InetSocketAddress( _Host, _Port );
	    _socket = new Socket();
	    _socket.connect( agentIPsoca, CONNECT_TIMEOUT );
	    _socket.setSoTimeout( HANDSHAKE_TIMEOUT );

	    //send some kind of handshake (usually client name or simple request):
	    UFTimeStamp uft = new UFTimeStamp(handshake);

	    if( uft.sendTo(_socket) <= 0 ) {
		connectError(cType,"Handshake Send");
		return false;
	    }

	    //get response from agent
	    UFProtocol ufp = null;

	    if( (ufp = UFProtocol.createFrom(_socket)) == null ) {
		connectError(cType,"Handshake Read");
		return false;
	    }

	    statusResponse.setText( ufp.name() );
	    System.out.println( className + "::" + cType + ufp.name() );
	    indicateConnectStatus(true);
	    InetAddress LocalInet = _socket.getLocalAddress();
	    LocalHost = LocalInet.getHostName();
	    //set normal timeout for socket
	    _socket.setSoTimeout( socTimeout );
	    return true;
	}
	catch (Exception x) {
	    indicateConnectStatus(false);
	    _socket = null;
	    String message = cType + x.toString();
	    statusAction.setText( message );
	    System.err.println(className + "::" + message);
	    Toolkit.getDefaultToolkit().beep();
	    return false;
	}
    }
//-------------------------------------------------------------------------------

    protected void connectError( String cType, String errmsg )
    {
	String message = cType + errmsg + " ERROR";
	statusResponse.setText( message );
	System.err.println(className + "::" + message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);
	try {
	    _socket.close();
	    _socket = null;
	}
	catch (IOException ioe) {
	    message = cType + ioe.toString();
	    statusAction.setText( message );
	    System.err.println(className + "::" + message);
	}
    }
//-------------------------------------------------------------------------------

    public void indicateConnectStatus( boolean connStatus )
    {
	if( connStatus ) {
	    connectButton.setBackground(Color.green);
	    connectButton.setForeground(Color.black);
	    connectButton.setText("Connected to " + serverName);
	}
	else { 
	    connectButton.setBackground(Color.red);
	    connectButton.setForeground(Color.white);
	    connectButton.setText("Re-Connect to " + serverName);
	}
    }
//-------------------------------------------------------------------------------

    public void setHostAndPort( String host, int port ) {
	setHost( host );
	setPort( port );
    }

    public void setHost(String host) {
	_Host = host;
	hostPortPanel.setHost( host );
    }

    public void setPort(int port) {
	_Port = port;
	hostPortPanel.setPort( port );
    }

    public String getHost() { return _Host; }
    public int getPort() { return _Port; }

    public String getHostField() {
	_Host = hostPortPanel.getHostField();
	return _Host;
    }

    public int getPortField() {
	_Port = hostPortPanel.getPortField();
	return _Port;
    }

    public void setSocTimeout() { setSocTimeout(DEFAULT_TIMEOUT); }

    public void setSocTimeout(int timeout) {
	try {
	    _socket.setSoTimeout( timeout );
	    socTimeout = timeout;
	    System.out.println( className+".setSocTimeout> = " + socTimeout );
	}
	catch (SocketException se) {
	    System.err.println( className+".setSocTimeout> " + se.toString() );
	}
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#sendRecvAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     *@param caller : String = name of calling method.
     *@param errmsg : String = error message if something bad happened.
     *@retval = UFStrings reply from DC agent.
     */
    public UFStrings sendRecvAgent( UFStrings agentRequest, String caller )
    {
	agentRequest.rename( LocalHost + ":" + agentRequest.name() );

	if( agentRequest.sendTo( _socket ) <= 0 ) {
	    String errmsg = "Send ERROR.";
	    System.err.println( caller + errmsg );
	    indicateConnectStatus(false);
	    return null;
	}

	//recv response from agent:
	UFStrings agentReply = (UFStrings)UFProtocol.createFrom( _socket );

	if( agentReply == null ) {
	    //nothing recvd but try again...
	    agentReply = (UFStrings)UFProtocol.createFrom( _socket );
	    if( agentReply == null ) {
		System.err.println( caller + "Transaction ERROR: nothing recvd, giving up.");
		indicateConnectStatus(false);
	    }
	    else {
		//send OK msg to errlog because read timeout already posted error msg:
		System.err.println("OK: " + caller + "finally got reply.");
	    }
	}

	if( socTimeout != DEFAULT_TIMEOUT ) setSocTimeout( DEFAULT_TIMEOUT );
	return agentReply;
    }
}
