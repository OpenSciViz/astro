const char rcsId[] = "$Name:  $ $Id: UFCAServ.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFCAServ.h"
#include "UFRuntime.h" // system & posix conv. funcs.

// These static members must be re-declared in this file.
gddAppFuncTable<UFPV> UFCAServ::_funcTable;
int UFPV::_currentOps;

static char* _sdefault= "0.0";
static int _idefault= 0;
static double _default= 0.0;

//static UFPVAttr _pvtest[] = { UFPVAttr("flam2:heartbeat", &_idefault), UFPVAttr("flam2:sad:DatumCnt", &_idefault) };
static UFPVAttr _pvtest[] = { UFPVAttr("flam2:heartbeat", _sdefault), UFPVAttr("flam2:sad:DatumCnt", _sdefault) };
std::map<string, UFPVAttr*> UFCAServ::_pvList;

// UFCAServ class funcs.

// mmap<string, string> dblist is:  string key==recname, string val==recname.fieldname
int UFCAServ::applysfrom(const string& file, multimap<string, string>& applylist) {
  string cmd = "cat " + file;
  cmd += " | grep -v '\\#'  | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -i apply | grep -v applyR";
  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::applysfrom> unable to open: "<<file<<endl;
    return 0;
  }

  applylist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     applylist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)applylist.size();
}

int UFCAServ::cadsfrom(const string& file, multimap<string, string>& cadlist) {
  string cmd = "cat " + file;
  cmd += " | grep -v '\\#'  | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -v apply";
  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::cadsfrom> unable to open: "<<file<<endl;
    return 0;
  }

  cadlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     cadlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)cadlist.size();
}

int UFCAServ::carsfrom(const string& file, multimap<string, string>& carlist) {
  string cmd = "cat " + file;
  cmd += " grep -i -v '\\#' | cut -d ' ' -f1 | sort -u | grep -i -v sad | grep -v R | grep -v DIR";

  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::carsfrom> unable to open: "<<file<<endl;
    return 0;
  }
  carlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     carlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)carlist.size();
}

int UFCAServ::sadsfrom(const string& file, multimap<string, string>& sadlist) {
  string cmd = "cat " + file;
  cmd += " | grep -i -v '\\#' | cut -d ' ' -f1 | sort -u | grep sad";

  FILE* fp = popen(cmd.c_str(), "r");
  if( fp == 0 ) {
    clog<<"UFCAServ::sadsfrom> unable to open: "<<file<<endl;
    return 0;
  }
  sadlist.clear();
  string recname, recfield;
  char buf[BUFSIZ];
  while( fgets(buf, BUFSIZ, fp) != 0 ) {
     recname = recfield = buf;
     size_t dot = recname.find(".");
     if( dot != string::npos )
       recname = recname.substr(0, dot);
     sadlist.insert(pair<string, string>(recname, recfield));
  }
  pclose(fp);    

  return (int)sadlist.size();
}

int UFCAServ::createPVsfrom(const multimap<string, string>& dblist, map<string, UFPVAttr*>& pvlist) {
  pvlist.clear();
  return (int)pvlist.size();
}

int UFCAServ::createPVsfrom(const string& file, map<string,  UFPVAttr*>& pvlist) {
  pvlist.clear();

  clog<<" UFCAServ::createPVsfrom> file: "<<file<<endl;

  if( file == "" ) {
    aitString aits = _pvtest[0].getName();
    string pvname = aits.string();
    pvlist[pvname] = &_pvtest[0];
    clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(int)pvlist[pvname]<<endl;
    aits = _pvtest[1].getName();
    pvname = aits.string();
    pvlist[pvname] = &_pvtest[1];
    clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(int)pvlist[pvname]<<endl;
    return pvlist.size();
  }

  FILE* fs = ::fopen(file.c_str(), "r");
  if( fs == 0 ) {
    clog<<"createPVfrom> unable to open: "<<file<<endl;
    return -1;
  }

  char pv[BUFSIZ]; int sz = sizeof(pv); memset(pv, 0, sz);
   while( fgets(pv, sz, fs) != 0 ) {
    char *tmp= pv, *pvtmp= pv;
    while( *tmp == ' ' || *tmp == '\t' || *tmp == '\n' || *tmp == '\r' ) ++tmp;
    pvtmp = tmp;
    tmp = &pv[BUFSIZ-1];
    do {
      if( *tmp == ' ' || *tmp == '\t' || *tmp == '\n' || *tmp == '\r' )
	*tmp = '\0';
    } while( --tmp != pvtmp );
    if( strlen(pvtmp) >= 0 ) {
      string pvname = pvtmp;
      UFPVAttr* pva =  new UFPVAttr(pvtmp, _sdefault);
      pvlist[pvname] = pva;
      clog<<"UFCAServ::createPVsfrom> "<<pvname<<", "<<(int)pvlist[pvname]<<endl;
    }
    memset(pv, 0, sz);
  }
  return pvlist.size(); 
}

pvExistReturn UFCAServ::pvExistTest(const casCtx& ctx, const char *pPVName) { // CA Context, pvname
  // If the PV exists, write its name to the canonical PV name object.
  const UFPVAttr *pPVAttr = UFCAServ::findPV(pPVName);
  if( pPVAttr )
    return pverExistsHere;
  
  return pverDoesNotExistHere;
}

const UFPVAttr* UFCAServ::findPV(const char *pName) {
  const UFPVAttr *pPVAttr= 0;
  string pvname = pName;
  std::map< string, UFPVAttr* >::iterator pvi = _pvList.find(pvname);
  if( pvi != _pvList.end() ) { // it exists
    pPVAttr = _pvList[pvname];
    clog<<" UFCAServ::findPV> "<<pvname<<", "<<(int)_pvList[pvname]<<endl;
  }
  else {
    clog<<" UFCAServ::findPV> "<<pvname<<" not found"<<endl;
  }
  /* 
  int nelem = NELEMENTS(UFCAServ::_pvList);
  int i= 0;
  for( pPVAttr = UFCAServ::_pvList; i < nelem; ++i, ++pPVAttr ) {
    if(strcmp(pName, pPVAttr->getName().string()) == 0 )
      return pPVAttr;
  }
  */
  return pPVAttr;
}

pvCreateReturn UFCAServ::createPV(const casCtx& ctx, const char *pPVName) {
  const UFPVAttr *pAttr = UFCAServ::findPV(pPVName);
  // If PV doesn't exist, return NULL. Otherwise, return a pointer
  // to a new UFPV object.
  if( !pAttr )
    return S_casApp_pvNotFound;

  //UFPV* pv = new (nothrow) UFPV(*this, *pAttr);
  UFPV* pv = new UFPV(*this, *pAttr);
  if( !pv )
    return S_casApp_noMemory;

  return static_cast< casPV& > (*pv);
}

UFPVAttr::UFPVAttr(const char* pName, char* val) : _highoperat(1.0), _lowoperat(0),
                                                   _high_alarm(1.10), _low_alarm(0.01),
                                                   _high_warn(1.05), _low_warn(0.05),
                                                   _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                                                   _precision(4), _name(pName),
					           _units("NoUnits"), 
						   //_numeric(FP_NAN) {
						   //_numeric(MAXFLOAT) {
						   _numeric(HUGE) {
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumString);
  _pVal = new gddScalar(gddAppType_value, aitEnumString);
  if( val == 0 ) {
    _pVal->putConvert(pName);
    return;
  }
  else {
    _pVal->putConvert(val);
  }
  // elim. whites
  while( *val == ' ' || *val == '\t' || *val == '\n' || *val == '\r' ) 
    ++val;

  if( isdigit(val[0]) ) {
    _numeric = atof(val);
  }
  else if( strlen(val) > 1 ) {
    if( isdigit(val[1]) && (val[0]=='-' || val[0]=='+') )
      _numeric = atof(val);
  }
  return;
}

UFPVAttr::UFPVAttr(const char* pName, int* val) : _highoperat(1.0), _lowoperat(0),
                                                 _high_alarm(1.10), _low_alarm(0.01),
                                                 _high_warn(1.05), _low_warn(0.05),
                                                 _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                                                 _precision(4), _name(pName),
					         _units("Normal"),
						 //_numeric(FP_NAN) {
						 //_numeric(MAXFLOAT) {
						 _numeric(HUGE) {
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumInt32);
  _pVal = new gddScalar(gddAppType_value, aitEnumInt32);
  if( val )
    _pVal->putConvert(*val);
  else
    _pVal->putConvert(0);
}

UFPVAttr::UFPVAttr(const char* pName, double* val) : _highoperat(1.0), _lowoperat(0),
                                                    _high_alarm(1.10), _low_alarm(0.01),
                                                    _high_warn(1.05), _low_warn(0.05),
                                                    _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                                                    _precision(4), _name(pName),
					            _units("Normal"),
						    //_numeric(FP_NAN) {
						    //_numeric(MAXFLOAT) {
						    _numeric(HUGE) {
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumFloat64);
  _pVal = new gddScalar(gddAppType_value, aitEnumFloat64);
  if( val ) 
    _pVal->putConvert(*val);
  else
    _pVal->putConvert(0.0);
}

UFPV::UFPV(const caServer &cas, const UFPVAttr &attr) : casPV((caServer&)cas), _attr(attr), _interest(aitFalse) {
  gdd *pValue = _attr.getVal(); // Get pointer to gdd object.
  if( !pValue )
    return;

  double value = _attr._numeric;
  aitString name = getName();
  const char* pvname = name.string();;
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString sval; pValue->getConvert(sval);
    clog<<"UFPV::> char* val: "<<sval.string()<<", pvname: "<<pvname<<endl;
    //if( !isnanf(_attr._numeric) ) 
    //if( _attr._numeric < MAXFLOAT ) 
    if( _attr._numeric < HUGE ) 
      clog<<"UFPV::> _numeric: "<<_attr._numeric<<", pvname: "<<pvname<<endl;
    else
      return;
  }
  else {
    pValue->getConvert(value);
  }
  
  if( value >= _attr.getHighAlarm() ) {
    pValue->setStat(epicsAlarmHigh);
    pValue->setSevr(epicsSevMinor);
  }
  else if (value <= _attr.getLowAlarm() ) {
    pValue->setStat(epicsAlarmLow);
    pValue->setSevr(epicsSevMinor);
  }
  else {
    pValue->setStat(epicsAlarmNone);
    pValue->setSevr(epicsSevNone);
  }

  if( primtyp == aitEnumInt32 ) {
    clog<<"UFPV::> int val: "<<value<<", pvname: "<<pvname<<endl;
    return;
  }
  clog<<"UFPV::> double val: "<<value<<", pvname: "<<pvname<<endl; 
}

caStatus UFPV::beginTransaction() {
  // Trivial implementation that informs the user of the number of
  // current IO operations in progress for the server tool. currentOps
  //  is a static member.
  _currentOps++;
  clog << "Number of current operations = " << _currentOps << "\n"; 
  return S_casApp_success;
}

void UFPV::endTransaction() {
  _currentOps--;
}

caStatus UFPV::read(const casCtx& ctx, gdd& prototype) {
  // Calls UFCAServ::read() which calls the appropriate function
  // from the application table.
  return UFCAServ::read(*this, prototype);
}

gddAppFuncTableStatus UFPV::readStatus(gdd& val) {
  gdd *pValue = _attr.getVal();
  if( pValue )
    val.putConvert(pValue->getStat());
  else
    val.putConvert(epicsAlarmUDF);

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readSeverity(gdd& val) {
  gdd *pValue = _attr.getVal();
  if( pValue )
    val.putConvert(pValue->getSevr());
  else
    val.putConvert(epicsSevNone);

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readPrecision(gdd& val) {
  val.putConvert(_attr.getPrec());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighOperation(gdd& val) {
  val.putConvert(_attr.getHighOperation());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowOperation(gdd& val) {
  val.putConvert(_attr.getLowOperation());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighAlarm(gdd& val) {
  val.putConvert(_attr.getHighAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighWarn(gdd& val) {
  val.putConvert(_attr.getHighWarn());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowWarn(gdd& val) {
  val.putConvert(_attr.getLowWarn());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowAlarm(gdd& val) {
  val.putConvert(_attr.getLowAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readHighCtrl(gdd& val) {
  val.putConvert(_attr.getHighCtrl());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readLowCtrl(gdd& val) {
  val.putConvert(_attr.getLowCtrl());
  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readVal(gdd& val) {
  // If UFPVAttr::_pVal exists, then use the gdd::get() function to 
  // assign the current value of pValue to currentVal; then use the
  // gdd::putConvert() to write the value into value.
  gdd *pValue = _attr.getVal();
  if( !pValue )
    return S_casApp_undefined;

  aitString name = getName();
  const char* pvname = name.string();

  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString curval;
    pValue->getConvert(curval);
    val.putConvert(curval);
    clog<<"UFPV::readVal> CAS name: "<<pvname<<", val: "<<curval.string()<<endl;
  }
  else if( primtyp == aitEnumInt32 ) {
    int curval= 0;
    pValue->getConvert(curval);
    val.putConvert(curval);
    clog<<"UFPV::readVal> name: "<<pvname<<", val: "<<curval<<endl;
  }
  else {
    double curval= 0;
    pValue->getConvert(curval);
    val.putConvert(curval);
    clog<<"UFPV::readVal> name: "<<pvname<<", val: "<<curval<<endl;
  }

  return S_casApp_success;
}

gddAppFuncTableStatus UFPV::readUnits(gdd& val) {
  val.put(_attr.getUnits());
  return S_casApp_success;
}

// bestExternalType() is a virtual function that can redefined to
// return the best type with which to access the PV. Called by the
// server library to respond to client request for the best type.
aitEnum UFPV::bestExternalType() {
  gdd* pValue = _attr.getVal();
  if( !pValue )
    return aitEnumInvalid;

  return pValue->primitiveType();
}

caStatus UFPV::write(const casCtx& ctx, const gdd& val) {
    clog<<"UFPV::write> enter..."<<endl;
  caServer* pServer = getCAS();

  if( !pServer ) {
    clog<<"UFPV::write> CAS service not available?"<<endl;
    return S_casApp_noSupport;
  }

  // Doesn't support writing to arrays or container objects
  // (gddAtomic or gddContainer).
  if( ! val.isScalar() ) {
    clog<<"UFPV::write> non scalar PV currently not supported..."<<endl;
    return S_casApp_noSupport;
  }

  gdd* pValue = _attr.getVal();
  if( !pValue ) {
    clog<<"UFPV::write> CAS PV not available?"<<endl;
    return S_casApp_noSupport;
  }
  aitString name = getName();
  const char* pvname = name.string();
  clog<<"UFPV::write> name: "<<pvname<<endl;

  /*
  // If pValue exists, unreference it, set the pointer to the new gdd
  // object, and reference it.
  if( pValue )
    pValue->unreference();

  pValue = &val;
  pValue->reference();
  */

  // Set the timespec structure to the current time stamp the gdd.
  struct timespec t;
  epicsTimeStamp ts = epicsTime();
  pValue->setTimeStamp(&ts);

  // Get the new value and set the severity and status according
  // to its value.
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString as; val.getConvert(as);
    clog<<"UFPV::write> pv is string, new value conversion: "<<as.string()<<endl;
    pValue->putConvert(as);
    return S_casApp_success;
  }

  double value;
  if( primtyp == aitEnumInt32 ) {
    int ival; val.getConvert(ival);
    clog<<"UFPV::write> pv is int32, new value conversion: "<<ival<<endl;
    pValue->putConvert(ival);
    value = ival;
  }
  else {
    val.getConvert(value);
    clog<<"UFPV::write> pv is double, new value conversion: "<<value<<endl;
    pValue->putConvert(value);
  }
  // all done with val, can we unreference it now?
  val.unreference();

  if( value > 1.1 * _attr.getHighAlarm() ) {
     pValue->setStat(epicsAlarmHiHi);
     pValue->setSevr(epicsSevMajor);
  }
  else if( value >= _attr.getHighAlarm() ) {
    pValue->setStat(epicsAlarmHigh);
    pValue->setSevr(epicsSevMinor);
  }
  else if( value <= _attr.getLowAlarm() ) {
    pValue->setStat(epicsAlarmLow);
    pValue->setSevr(epicsSevMinor);
  }
  else if( value < 0.9 * _attr.getLowAlarm() ) {
    pValue->setStat(epicsAlarmLoLo);
    pValue->setSevr(epicsSevMajor);
  }
  else {
    pValue->setStat(epicsAlarmNone);
    pValue->setSevr(epicsSevNone);
  }

  if( _interest == aitTrue ) {
    casEventMask select( pServer->valueEventMask() | pServer->alarmEventMask() );
    postEvent(select, *pValue);
  }

  return S_casApp_success;
}


// Constructor for UFCAServ. After passing arguments to caServer 
// constructor all of the read functions are installed in the function
// table.
//UFCAServ::UFCAServ(unsigned pvCountEstimate) : caServer(pvCountEstimate) {
UFCAServ::UFCAServ() : caServer() {
  _funcTable.installReadFunc("status", &UFPV::readStatus);
  _funcTable.installReadFunc("severity", &UFPV::readSeverity);
  _funcTable.installReadFunc("precision", &UFPV::readPrecision);
  _funcTable.installReadFunc("alarmHigh", &UFPV::readHighAlarm);
  _funcTable.installReadFunc("alarmHighWarning", &UFPV::readHighWarn);
  _funcTable.installReadFunc("alarmLowWarning", &UFPV::readLowWarn);
  _funcTable.installReadFunc("alarmLow", &UFPV::readLowAlarm);
  _funcTable.installReadFunc("value", &UFPV::readVal);
  _funcTable.installReadFunc("graphicHigh", &UFPV::readHighOperation);
  _funcTable.installReadFunc("graphicLow", &UFPV::readLowOperation);
  _funcTable.installReadFunc("controlHigh", &UFPV::readHighCtrl);
  _funcTable.installReadFunc("controlLow", &UFPV::readLowCtrl);
  _funcTable.installReadFunc("units", &UFPV::readUnits);
}


int UFCAServ::main(int argc, char** argv, char** env) {
  // Create server object.
  //UFCAServ* pCAS = new (nothrow) UFCAServ;
  UFCAServ* pCAS = new UFCAServ;
  if(!pCAS)
    return -1;

  /*
  if( argc > 1 ) { 
    if( atoi(&argv[1][1]) > 0 ) 
      pCAS->setDebugLevel(1u);
    if( atoi(&argv[1][1]) > 1 ) 
      pCAS->setDebugLevel(2u);
    if( atoi(&argv[1][1]) > 2 ) 
      pCAS->setDebugLevel(3u);
    if( atoi(&argv[1][1]) > 3 ) 
      pCAS->setDebugLevel(4u);
    if( atoi(&argv[1][1]) > 4 ) 
      pCAS->setDebugLevel(5u);
  }
  */
  string file;
  if( argc > 1 ) // assume it is the single column *.db text file name:
    file = argv[1];

  UFCAServ::createPVsfrom(file, UFCAServ::_pvList);

  // Loop forever
  double delay = 1000;
  while( aitTrue ) // fileDescriptorManager is a predeclared object found in fdMgr.h
    fileDescriptorManager.process(delay);

  return 0;
}
