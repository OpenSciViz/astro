#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "stdlib.h"
//#define __USE_ISOC99
#include "math.h"
#include "ieee754.h"
 
// stdc++
#include "cstdio"
#include "iostream"
#include "string"
#include "map"
#include "multimap.h"

// portable cas -- already included from UFPV.h
#include "UFPV.h"

const int _CAD_ABORT  = 0; ///< TBD
const int _CAD_MARK   = 0; ///< TBD
const int _CAD_CLEAR  = 1; ///< TBD
const int _CAD_PRESET = 2; ///< TBD
const int _CAD_START  = 3; ///< TBD
const int _CAD_STOP   = 4; ///< TBD
const int _CAD_ACCEPT = 0; ///< TBD
const int _CAD_REJECT = -1;///< TBD

using namespace std;

/// Brief (ONE LINE) description of class: TBD

/** The server class. Provides the pvExistTest() and createPV()
 * functions as well as the function table
 */
class UFCAServ : public caServer { 
public:
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   */
  UFCAServ();
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   */
  inline ~UFCAServ() {}

  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param argc int TBD
   * @param argv char[][] TBD
   * @param envp char[][] TBD
   * @return int TBD
   */
  static int main(int argc, char** argv, char** envp);
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param Name char* (old-stlye string) TBD
   * @return UFPVAttr pointer TBD
   * @see UFPVAttr
   */
  static const UFPVAttr *findPV(const char* Name);

  /// Brief (ONE LINE) description of method: TBD
  /**
   * the ca server library invokes the pv's read func which then makes use of this:
   * @param pv TBD
   * @param val TBD
   * @return gddAppFuncTableStatus TBD 
   */
  inline static gddAppFuncTableStatus read(UFPV& pv, gdd& val) { return UFCAServ::_funcTable.read(pv, val); }

  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param file TBD
   * @param pvlist TBD
   * @return int TBD
   */
  static int createPVsfrom(const string& file, std::map<string, UFPVAttr*>& pvlist);
  // mmap<string, string> dblist is:  string key==recname, string val==recname.fieldname
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param dblist TBD
   * @param pvlist TBD
   * @return int TBD
   */
  static int createPVsfrom(const std::multimap<string, std::string>& dblist, std::map<string, UFPVAttr*>& pvlist);
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param file TBD
   * @param applylist TBD
   * @return int TBD
   */
  static int applysfrom(const string& file, std::multimap<std::string, std::string>& applylist);
  /// Brief (ONE LINE) description of method: TBD
  /**
   * Detailed description: TBD
   * @param file TBD
   * @param cadlist TBD
   * @return int TBD
   */
  static int cadsfrom(const string& file, std::multimap<std::string, std::string>& cadlist);
  /// Brief (ONE LINE) description of method: TBD
  /** Detailed description: TBD
   * @param file TBD
   * @param carlist TBD
   * @return int TBD
   */
  static int carsfrom(const string& file, std::multimap<std::string, std::string>& carlist);
  /// Brief (ONE LINE) description of method: TBD
  /** Detailed description: TBD
   * @param file TBD
   * @param sadlist TBD
   * @return int TBD
   */
  static int sadsfrom(const string& file, std::multimap<std::string, std::string>& sadlist);
 
  /// Brief (ONE LINE) description of method: TBD
  /** Detailed description: TBD
   * @param ctx TBD
   * @param pPVName TBD
   * @return pvExistReturn
   */
  virtual pvExistReturn pvExistTest (const casCtx& ctx, const char* pPVName);
  /// Brief (ONE LINE) description of method: TBD
  /** Detailed description: TBD
   * @param ctx TBD
   * @param pPVName TBD
   * @return pvCreateReturn TBD
   */
  virtual pvCreateReturn createPV(const casCtx& ctx, const char* pPVName);

 private:
  //static const UFPVAttr _pvList[];
  static std::map<string, UFPVAttr*> _pvList; ///< TBD
    static gddAppFuncTable<UFPV> _funcTable; ///< TBD
};
