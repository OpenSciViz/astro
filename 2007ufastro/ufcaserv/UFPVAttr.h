// portable cas
#include "aitTypes.h"
#include "casdef.h"
#include "epicsTimer.h"
#include "gddApps.h"
#include "gddAppFuncTable.h"
#include "gddScalar.h"
#include "fdManager.h"

/// Brief (ONE LINE) description of class: TBD
/** This class is not part of the server interface, but is only used
 * here in order to keep track of PV values. It contains members for
 * storing and accessing all the application types needed to satisfy a
 * DBR_CTRL request. DBR_STS requests can also be satisfied. Since DM
 * makes DBR_CTRL requests for each controller or monitor initially,
 * and then makes DBR_STS requests subsequently,this class contains
 * the attributes needed to work with DM clients.
 */
class UFPVAttr {
public:
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param pName TBD
   * @param val TBD
   */ 
  UFPVAttr(const char* pName, char* val= 0);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param pName TBD
   * @param val TBD
   */ 
  UFPVAttr(const char* pName, int* val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @param pName TBD
   * @param val TBD
   */ 
  UFPVAttr(const char* pName, double* val);
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   */ 
  inline ~UFPVAttr() {}

  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return aitString TBD
   */ 
  inline const aitString &getName () const { return _name; }

  // if gdd val is a string i have no idea what this will do!
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getHighOperation () const { aitFloat64 d; _pVal->getConvert(d); return _highoperat * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getLowOperation ()  const { aitFloat64 d; _pVal->getConvert(d); return _lowoperat * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getHighAlarm () const { aitFloat64 d; _pVal->getConvert(d); return _high_alarm * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getHighWarn () const { aitFloat64 d; _pVal->getConvert(d); return _high_warn * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getLowWarn() const { aitFloat64 d; _pVal->getConvert(d); return _low_warn * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getLowAlarm () const { aitFloat64 d; _pVal->getConvert(d); return _low_alarm * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getHighCtrl () const { aitFloat64 d; _pVal->getConvert(d); return _high_ctrl_lim * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return double TBD
   */ 
  inline double getLowCtrl () const  { aitFloat64 d; _pVal->getConvert(d); return _low_ctrl_lim * d; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return short TBD
   */ 
  inline short getPrec () const { return _precision; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return gdd pointer TBD
   */ 
  inline gdd* getVal () const { return _pVal; }
  /// Brief (ONE LINE) description of method: TBD
  /** 
   * Detailed Description: TBD
   * @return aitString TBD
   */ 
  inline aitString getUnits () const { return _units; }

  double _numeric; ///< TBD

private:
  double _highoperat,///< TBD 
    _lowoperat,///< TBD 
    _high_alarm,///< TBD 
    _low_alarm,///< TBD 
    _high_warn,///< TBD 
    _low_warn,///< TBD 
    _high_ctrl_lim,///< TBD 
    _low_ctrl_lim;///< TBD
  short _precision; ///< TBD
  const aitString _name, ///< TBD 
    _units;///<TBD 
  gdd* _pVal; ///< TBD
};
