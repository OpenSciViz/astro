#if !defined(__ufcaserv_cc__)
#define __ufcaserv_cc__ "$Name:  $ $Id: ufcaserv.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufcaserv_cc__;
 
#include "UFCAServ.h"
//__UFCAServ_H__(ufcaserv_cc);
 
int main(int argc, char** argv, char** envp) {
  // just call UFCaserv main:
  return UFCAServ::main(argc, argv, envp);
}
 
#endif // __ufcaserv_cc__
