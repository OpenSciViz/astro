Flamingos-2 On-Instrument Wavefront Sensor Assembly Checkout.

THIS SCRIPT TAKES ABOUT 20 MINUTES TO COMPLETE.

Assumptions are ....

        * The IOC has been depowered and re-started cold, or the 
          IOC has been rebooted.

        * The Flamingos-2 components control database has been loaded.

        * There is nothing obstructing the OIWFS probe arm.

	If so, it should be safe to move the probe arm....
