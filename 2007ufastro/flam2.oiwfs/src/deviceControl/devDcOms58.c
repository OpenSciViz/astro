/*
************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <2005>                       (c) <2005>
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                    
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devDcOms58.c
 *
 * PURPOSE:
 * EPICS OMS VME58 Device Support code for the deviceControl record.
 *
 * FUNCTION NAME(S)
 * configureDrive       Set motor velocity, acceleration and base velocity
 * controlIndex         Create the motion ASCII command string.
 * controlMotion        Start, stop or abort a motion
 * controlPower         Enable or disable the motor power via OMS AUX bits
 * devInit              Initialize the device control record support module
 * devInitRec           Initialize an instance of deviceControl record support
 * omsScanTask          Monitor device motion and status
 * setDelay             Re-process the calling record after a given interval
 * setPosition          Load the motor position counter with given value
 *
 *INDENT-OFF*
 * Revision 1.28  2005/06/09 bmw
 * Moved DDR_OMS_SCAN_TASK_RATE definition to *.h
 *
 * Revision 1.27  2005/05/19 bmw
 * Added comments. 
 *
 * Revision 1.26  2005/04/25 bmw
 * Removed section in controlPower() that was commented out.
 * Removed section in omsScanTask() that was commented out.
 *
 * Revision 1.25  2004/11/08 bmw
 * Added comments, removed limit recovery, removed PID, made indexing modes
 * more GMOS-like, differentiated  ABORT/STOP modes and removed FLUSH, 
 * added method to retrieve model and deduce axis type and number, added
 * check for duplicate motors with same EPICS signal name.
 * Lots of changes to omsScanTask().
 *
 * Revision 1.24  2004/09/20 bmw
 * Initial F2 revision copy of HIA Altair devDevControl58.c rev 1.23.
 *
 * Revision 1.23  2002/09/13 00:37:24  angelic
 * added backlash compensation and index offset to ialgs 9 and 10
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include    <stdioLib.h>
#include    <stdlib.h>
#include    <string.h>

#include    <semLib.h>
#include    <taskLib.h>
#include    <sysLib.h>
#include    <tickLib.h>
#include    <logLib.h>
#include    <link.h>
#include    <taskLib.h>

#include    <devSup.h>
#include    <recSup.h>
#include    <ellLib.h>
#include    <devLib.h>

#include    <deviceControlRecord.h>   /* Message levels - auto-created from *.dbd */
#include    <deviceControl.h>         /* access to device ctrl private structure  */
#include    <devDcOms58.h>            /* access to device control DSET functions  */
#include    <drvOmsVme58.h>           /* VME58 hardware and software constants    */
#include    <ddrMessageLevels.h>      /* Device record message level definitions. */

/*
 *  Local Defines
 */

#define devReport                      NULL       /* No report provided           */
#define devGetIoIntInfo                NULL       /* No ioint info provided       */

#define DDR_OMS_SCAN_TASK_PRIORITY     80         /* OMS scan task priority       */
#define DDR_OMS_SCAN_TASK_STACK        0x4000     /* 16k byte stack               */
#define DDR_OMS_SCAN_TASK_OPTIONS      VX_FP_TASK /* Allow floating point         */


/*
 *  Device support function prototypes
 */

static long configureDrive (DEVICE_CONTROL_PRIVATE *);
static long controlIndex (DEVICE_CONTROL_PRIVATE * );
static long controlMotion (DEVICE_CONTROL_PRIVATE *, long);
static long controlPower (DEVICE_CONTROL_PRIVATE *, long);
static long devInit (unsigned);
static long devInitRec (DEVICE_CONTROL_RECORD *);
static long setDelay (DEVICE_CONTROL_PRIVATE *, long);
static long setPosition (DEVICE_CONTROL_PRIVATE *, long);


/*
 * Internal function prototypes
 */
 
static int omsScanTask (int, int, int, int, int, int, int, int, int, int);


/*
 *  Create the device support interface structure for the deviceControl
 *  record.
 */

DEVICE_CONTROL_DSET devDcOms58 = {
    9,
    devReport,
    devInit,
    devInitRec,
    devGetIoIntInfo,
    configureDrive,
    controlPower,
    controlMotion,
    setDelay,
    setPosition,
    };


/*
 *  Define the private control structure used to keep current information
 *  on the state of each motor. This structure is defined in devDeviceControl.h
 */

typedef struct {
    ELLNODE             node;              /* motor scan list node struct   */
    SEM_ID              mutexSem;          /* mutual exclusion semaphore    */
    DEVICE_CONTROL_PRIVATE  *pDevice;      /* calling record private struct */
    DEVICE_CONTROL_RECORD   *pRecord;      /* calling record structure      */
    int                 card;              /* interface card number         */   
    int                 type;              /* Motor type                    */
    int                 exists;            /* interface card exists         */
    int                 axis;              /* axis on the interface card    */
    int                 updateState;       /* state update request flag     */
    int                 earlyDone;         /* done interrupt before moving  */
    int                 ignoreMotion;      /* ignore motion by setPosition  */
    long                simVelocity;       /* simulated velocity            */
    long                scansLeft;         /* timeout timer oms scans left  */
    long                stoppedCntr;       /* #scans motor has been stopped */
    char                errorMessage[MAX_STRING_SIZE];  /* root message     */
    long                status;            /* motor status flag             */
    } DEV_CTL_OMS_PRIVATE;


static ELLLIST deviceControlScanList;      /* record scan list              */
static int scanTaskPeriod;                 /* system ticks between scans    */
                                           /*   (period of omsScanTask)     */

/*
 *  Define a macro to print debugging information.   If the current debug
 *  level is greater than or equal to the given debug interest level a 
 *  log message will be generated consisting of:
 *      - the value of the system tick counter
 *      - the card number generating the error
 *      - the axis number on the card generating the error
 *      - a user defined error string containing a single integer parameter.
 *        NOTE - does not support double or float parameters
 *
 * For example:
 *      DEBUG(DDR_MSG_FULL,
 *            "<%d> c:%d s:%d omsScanTask: new position: %d \n",
 *            position);
 *
 * may result in the following log message:
 *      <31265> c:1 s:3 omsScanTask: new position: 2532
 *
 */

#define DEBUG(l,FMT,V) if (l <= pDevice->debug)                    \
                                   printf ("%s: "FMT,              \
                                           taskName(0),            \
                                           tickGet(),              \
                                           pMotor->card,           \
                                           pMotor->axis,           \
                                           V);


/*
 *  Define a macro to save the first (root) error message generated
 *  since the last time the error message buffer was flushed.   This
 *  prevents the first message from being overwritten by other error
 *  messages generated as higher level functions fail due to the 
 *  original error.
 */

#define SET_ERR_MSG(MSG)                                           \
{                                                                  \
    if (!strlen (pDevice->errorMessage))                           \
        strncpy (pDevice->errorMessage, MSG, MAX_STRING_SIZE - 1); \
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * configureDrive
 *
 * INVOCATION:
 * status = configureDrive (pDevice);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *)    Record interface structure
 *
 * parameters accessed via the interface structure:
 *
 * (>) pPrivate     (DEV_CTL_OMS_PRIVATE *) motor control structure address
 * (>) simulation   (int)   current simulation mode
 * (>) baseVelocity (long)  starting / minimum velocity (steps/sec)
 * (>) velocity     (long)  maximum velocity (steps/sec)
 * (>) acceleration (long)  acceleration rate (steps/sec/sec)
 * 
 * FUNCTION VALUE:
 * (long) processing success code.
 * Return code from drvOmsVmeWriteMotor() else
 *  DRV_OMS_VME_S_CFG_ERROR
 *  DRV_OMS_VME_S_PARAM_ERR
 *
 * PURPOSE:
 * Set motor velocity, acceleration and base velocity
 *
 * DESCRIPTION:
 * In simulation mode, set the internal simulation velocity.
 * In real mode:
 *    Ensure velocity and acceleration are valid,
 *    Format an OMS command string to set the base velocity
 *      operating velocity and acceleration rate.
 *    Write the string to the given oms card/axis.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long configureDrive
(
    DEVICE_CONTROL_PRIVATE *pDevice   /* deviceControl rec interface struct */
)
{
    char    scratch[DRV_OMS_VME_MAX_MSG_LEN];        /* char scratch buffer */
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate; /* axis control struct */
    long    status = 0;                              /* function status     */
  

    DEBUG(DDR_MSG_MAX, "<%ld> c:%d s:%d configureDrive:entry%c\n", ' ');

    /*
     *  In simulation mode just set the simulation velocity.
     */

    if ( pDevice->simulation )
    {
	DEBUG(DDR_MSG_FULL,"<%ld> c:%d s:%d configureDrive:simulating%c\n",' ');
        semTake (pMotor->mutexSem, WAIT_FOREVER);
        pMotor->simVelocity = pDevice->velocity / DDR_OMS_SCAN_TASK_RATE;
        semGive (pMotor->mutexSem);
    }

    /*
     *  Otherwise set the axis the motion parameters
     */ 

    else
    {
        /*
         *  Confirm that the card and axis were found at init time.
         */

        if (!pMotor->exists)
        {
            SET_ERR_MSG("Motor hardware does not exist");
            return DRV_OMS_VME_S_CFG_ERROR;
        }
           
        /*
         *  The base velocity must be greater than zero
         */

        if ( pDevice->baseVelocity < 1 )
        {
            DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d configureDrive:velocity base cannot be less than 1: base velocity:%ld steps/sec\n", pDevice->baseVelocity);
            SET_ERR_MSG("VBAS must be greater than zero");
            return DRV_OMS_VME_S_PARAM_ERR;
        }

        /*
         *  The base velocity must be less than
         *  the programmed velocity (velocity or indexVelocity)
         */

        if ( pDevice->velocity <= pDevice->baseVelocity ||
                  pDevice->indexVelocity <= pDevice->baseVelocity )
        {
            DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d configureDrive:velocity base failure: base velocity:%ld steps/sec\n", pDevice->baseVelocity);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:velocity:%ld steps/sec\n", pDevice->velocity);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:index velocity:%ld steps/sec\n", pDevice->indexVelocity);
            SET_ERR_MSG("VBAS must be less than VELO and FIVL");
            return DRV_OMS_VME_S_PARAM_ERR;
        }

        /*
         *  The velocity must be faster than the omsScanTask rate
         *  otherwise it will think it has stopped.
         */

        if ( pDevice->velocity < DDR_OMS_SCAN_TASK_RATE ||
             pDevice->indexVelocity < DDR_OMS_SCAN_TASK_RATE)
        {
            DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d configureDrive:velocity slower than scan rate, DDR_OMS_SCAN_TASK_RATE: %d scans/sec\n", DDR_OMS_SCAN_TASK_RATE);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:velocity: %ld steps/sec\n", pDevice->velocity);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:index velocity: %ld steps/sec\n", pDevice->indexVelocity);
            SET_ERR_MSG("Velocity too slow for scan task");
            return DRV_OMS_VME_S_PARAM_ERR;
        }

        /*
         *  The acceleration must be fast enough such that the position
         *  has changed within a single omsScanTask during the ramp up 
         */
	
        if ( pDevice->acceleration < (DDR_OMS_SCAN_TASK_RATE *
                   (DDR_OMS_SCAN_TASK_RATE - pDevice->baseVelocity)) )
        {
            DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d configureDrive:acceleration too slow for scan rate, DDR_OMS_SCAN_TASK_RATE: %d scans/sec\n", DDR_OMS_SCAN_TASK_RATE);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:base velocity: %ld steps/sec\n", pDevice->baseVelocity);
            DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d configureDrive:acceleration: %ld steps/sec/sec\n", pDevice->acceleration);
            SET_ERR_MSG("Acceleration too slow for scan task");
            return DRV_OMS_VME_S_PARAM_ERR;
        }

        /*
         *  Generate an OMS command string to set the axis base velocity,
         *  operating velocity and acceleration rate.
         */

        semTake (pMotor->mutexSem, WAIT_FOREVER);

        sprintf (scratch,
                 "VB%ld VL%ld AC%ld ",
                 pDevice->baseVelocity,
                 pDevice->velocity,
                 pDevice->acceleration);


        /*
         *  Then write the string to the appropriate axis on the appropriate
         *  OMS card.
         */

        status = drvOmsVmeWriteMotor (pMotor->card,
                                      pMotor->axis,
                                      scratch);
        if (status)
        {
            drvOmsVmeGetErrorMessage (pDevice->errorMessage);
            DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d configureDrive: %s\n",
                                 pDevice->errorMessage);
        }
        semGive (pMotor->mutexSem);
    }
                                
    return ( status );                            
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * controlIndex
 *
 * INVOCATION:
 * status = controlIndex ( pDevice );
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *) Pointer to deviceControl 
 *              record structure.
 *
 * parameters accessed via the interface structure:
 *
 * (>) pPrivate     (DEV_CTL_OMS_PRIVATE *) motor control structure address
 * (>) lowLimit      (int)   state of reverse limit switch
 * (>) highLimit     (int)   state of forward limit switch
 * (>) target        (long)  target position (steps)
 * (>) index         (int)   indexing algorithm
 * (>) indexVelocity (long)  indexing velocity (steps/sec)
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 * Return code from drvOmsVmeWriteMotor() else
 *  DRV_OMS_VME_S_INDEX_ERROR
 *
 * PURPOSE:
 * Create the motion ASCII command string.
 *
 * DESCRIPTION:
 * Generate an OMS command string based on the indexing algorithm,
 * the type of OMS card used for this device and the state of
 * the motion limit switches.
 *
 * The string is then written to the OMS card for execution.  All strings
 * are terminated with "ID" to cause an interrupt when motion has finished.
 * 
 * Possible index actions are:
 *  DDR_INDEX_NONE -> Essentially just do a move.
 *  DDR_INDEX_LHSW -> Index in reverse to home switch
 *  DDR_INDEX_UHSW -> Index in forward to home switch
 *  DDR_INDEX_CHSW -> Index to center home switch
 *  DDR_INDEX_LLSW -> Index on reverse limit switch
 *  DDR_INDEX_ULSW -> Index to forward limit switch
 *  DDR_INDEX_OFLS -> Move off of lower switch
 *  DDR_INDEX_OFUS -> Move off of upper switch
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * This function is called by controlMotion()
 *
 * DEFICIENCIES:
 * 1. Assumes motors attached to encoder-capable cards have encoders 
 *    attached and that the encoders signals include an index pulse.
 * 2. UHSW and LHSW algorithms are optimized for active high home 
 *    switch signals.
 *-
 ************************************************************************
 */

static long controlIndex
(
    DEVICE_CONTROL_PRIVATE *pDevice  /* deviceControl rec interface struct */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate; /* axis control struct */
    char    scratch[DRV_OMS_VME_MAX_MSG_LEN];        /* char scratch buffer */
    long    status = 0;                              /* function status     */


    DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d controlIndex: index=<%d>\n", 
                         pDevice->index);
    switch (pDevice->index)
    {
        /*
         *  In INDEX_NONE mode (normal operation) it is not really an 
         *  index at all. The device is simply sent the target
         *  postion followed by the GO keyword to start the device 
         *  in motion.
         */

        case DDR_INDEX_NONE:
            sprintf (scratch, "MA%ld GO ID ", pDevice->target);

            break;

        /*
         *  In INDEX_LHSW mode, the device will travel in the reverse
         *  direction and index on the transition of the home switch.
         *  The OMS command string depends on both the motor type 
         *  as well as the state of the lower soft limit switch.
	 *  Closed loop systems (those using encoders) are assumed
         *  to be using encoders with an index pulse.  
         *  
         *  For LHSW type indexing, it is assumed that once the home switch  
         *  is active while moving in the reverse direction, the home switch 
         *  will remain active in the reverse direction beyond the reverse  
         *  limit position.  Therefore it can be assumed that the initial 
         *  motion direction can accurately be determined by knowing the 
         *  state of the home switch (I.e. if on the home switch move in
         *  in the foward direction and if off move in the reverse
         *  direction.  
         */


        case DDR_INDEX_LHSW:
            DEBUG(DDR_MSG_FULL,
                  "<%ld> c:%d s:%d controlIndex: INDEX_LHSW%c\n", ' ');

            /*  
             * For closed loop cards follow this procedure:
             * 
             *   1. reverse until home switch high, then ramp down,
             *   2. forward until home switch low, then ramp down, 
             *   3. reverse until home switch high, then ramp down,
             *
             *  The stage should be stopped with the home switch in the 
             *  high state (just on the switch for active high systems).
             * 
             *   4. change to a slower index velocity
             *   5. enable the index pulse
             *   6. forward until: home switch low and
             *                     encoder index pulse high and
             *                     encoder phase A high and
             *                     encoder phase B low then ramp down
             *   7. reverse back to zero position
             *  
             *  *** If starting from on the reverse limit, the motor 
             *  must start with a  move in the forward direction to 
             *  prevent a limit error.  So skip step 1. since we 
             *  already know the home switch will be high.
             *  
             *  Although it appears that the first two commands are
             *  are  unnecessary, the sequence can greatly improve 
             *  indexing speed if the distance bewteen the home
             *  switch transition and the reverse limit is significant.
             *  
             */

            if ( pMotor->type == DDR_OMS_58_ST_CL ||
                 pMotor->type == DDR_OMS_58_SV_CL )
            {
                DEBUG(DDR_MSG_FULL,
                      "<%ld> c:%d s:%d controlIndex: using index pulse%c\n",
                      ' ');

                /*
                 *  Generate an OMS command string to implement this
                 *  algorithm.
                 */

                if (pDevice->lowLimit)
                {
                    sprintf (scratch,
                             "HL HM0 HH HR0 VL%ld HE HM0 HS MA0 GO ID ",
                             pDevice->indexVelocity);
                }
                else
                {
                    sprintf (scratch,
                             "HH HR0 HL HM0 HH HR0 VL%ld HE HM0 HS MA0 GO ID ",
                             pDevice->indexVelocity);
                }
            }

            /*
             *  With open loop system, there is no index pulse, so 
             *  follow this procedure:
             * 
             *   1. reverse until home switch high, then ramp down,
             *   2. forward until home switch low, then ramp down, 
             *   3. reverse until home switch high, then ramp down,
             *
             *  The stage should be stopped with the home switch in the 
             *  high state (just on the switch for active high systems).
             * 
             *   4. change to a slower index velocity
             *   5. forward until home switch low, then ramp down, 
             *   6. reverse back to zero position
             *  
             *  *** If starting from on the reverse limit, the motor 
             *  must start with a  move in the forward direction to 
             *  prevent a limit error.  So skip step 1. since we 
             *  already know the home switch will be high.
             *  
             *  Although it appears that the first two commands are
             *  are  unnecessary, the sequence can greatly improve 
             *  indexing speed if the distance bewteen the home
             *  switch transition and the reverse limit is significant.
             *
             */

            else
            {
                /*
                 *  Generate an OMS command string to implement this
                 *  algorithm.
                 */

                if (pDevice->lowLimit)
                {
                    sprintf (scratch,
                             "HL HM0 HH HR0 VL%ld HL HM0 MA0 GO ID ",
                             pDevice->indexVelocity);
                }
                else
                {
                    sprintf (scratch,
                             "HH HR0 HL HM0 HH HR0 VL%ld HL HM0 MA0 GO ID ",
                             pDevice->indexVelocity);
                }
            }
            break;

        /*
         *  In INDEX_UHSW mode, the device will travel in the forward
         *  direction and index on the transition of the home switch. 
         *  The OMS command string depends first on the motor card 
         *  type and second on the state of the lower soft limit.  
         *  
         *  For UHSW type indexing, it is assumed that once the home switch  
         *  is active while moving in the forwar direction, the home switch 
         *  will remain active in the forward direction beyond the forward  
         *  limit position.  Therefore it can be assumed that the initial 
         *  motion direction can accurately be determined by knowing the 
         *  state of the home switch (I.e. if on the home switch move in
         *  in the reverse direction and if off move in the forward
         *  direction.  
         */

        case DDR_INDEX_UHSW:
            DEBUG(DDR_MSG_FULL,
                  "<%ld> c:%d s:%d controlIndex: INDEX_UHSW%c\n", ' ');

            /*  
             * For closed loop cards follow this procedure:
             * 
             *   1. forward until home switch high, then ramp down,
             *   2. reverse until home switch low, then ramp down, 
             *   3. forward until home switch high, then ramp down,
             *
             *  The stage should be stopped with the home switch in the 
             *  high state (just on the switch for active high systems).
             * 
             *   4. change to a slower index velocity
             *   5. enable the index pulse
             *   6. reverse until: home switch low and
             *                     encoder index pulse high and
             *                     encoder phase A high and
             *                     encoder phase B low then ramp down
             *   7. forward back to zero position
             *  
             *  *** If starting from on the forward limit, the motor 
             *  must start with a  move in the reverse direction to 
             *  prevent a limit error.  So skip step 1. since we 
             *  already know the home switch will be high.
             *  
             *  Although it appears that the first two commands are
             *  are  unnecessary, the sequence can greatly improve 
             *  indexing speed if the distance bewteen the home
             *  switch transition and the forward limit is significant.
             *  
             */

            if ( pMotor->type == DDR_OMS_58_ST_CL ||
                 pMotor->type == DDR_OMS_58_SV_CL )
            {
                DEBUG(DDR_MSG_FULL,
                      "<%ld> c:%d s:%d controlIndex: using index pulse%c\n", ' ');

                /*
                 *  Generate an OMS command string to implement this
                 *  algorithm.
                 */

                if (pDevice->highLimit)
                {
                    DEBUG(DDR_MSG_FULL,
                          "<%ld> c:%d s:%d controlIndex: hilim set%c\n",
                          ' ');
                    sprintf (scratch,
                             "HL HR0 HH HM0 VL%ld HE HR0 HS MA0 GO ID ",
                             pDevice->indexVelocity);
                }
                else
                {
                    sprintf (scratch,
                             "HH HM0 HL HR0 HH HM0 VL%ld HE HR0 HS MA0 GO ID ",
                             pDevice->indexVelocity);
                }
            }

            /*
             *  With open loop system, there is no index pulse, so 
             *  follow this procedure:
             * 
             *   1. forward until home switch high, then ramp down,
             *   2. reverse until home switch low, then ramp down, 
             *   3. forward until home switch high, then ramp down,
             *
             *  The stage should be stopped with the home switch in the 
             *  high state (just on the switch for active high systems).
             * 
             *   4. change to a slower index velocity
             *   5. reverse until home switch low, then ramp down, 
             *   6. forward back to zero position
             *  
             *  *** If starting from on the forward limit, the motor 
             *  must start with a move in the reverse direction to 
             *  prevent a limit error.  So skip step 1. since we 
             *  already know the home switch will be high.
             *  
             *  Although it appears that the first two commands are
             *  are  unnecessary, the sequence can greatly improve 
             *  indexing speed if the distance bewteen the home
             *  switch transition and the forward limit is significant.
             *
             */

            else
            {
                /*
                 *  Generate an OMS command string to implement this
                 *  algorithm.
                 */

                if (pDevice->highLimit)
                {
                    DEBUG(DDR_MSG_MAX,
                          "<%ld> c:%d s:%d controlIndex: hilim set%c\n",
                          ' ');
                    sprintf (scratch,
                             "HH HR0 HL HM0 VL%ld HH HR0 MA0 GO ID ",
                             pDevice->indexVelocity);
                }
                else
                {
                    sprintf (scratch,
                             "HL HM0 HH HR0 HL HM0 VL%ld HH HR0 MA0 GO ID ",
                             pDevice->indexVelocity);
                }           
            }

            break;
 

        /*
         *  The INDEX_CHSW mode is used for continuously rotating devices
         *  (i.e. filter wheels) that have a point contact switch trigger
         *  at some point in their travel.   The intent of this algorithm
         *  is to wind up close to the switch on the higher count side
         *  then back up slowly until the switch is triggered to define
         *  the index point. The algorithm includes extra moves designed
         *  to make the indexing work regardless of whether the centre
         *  home switch is high or low when active.
         */

        case DDR_INDEX_CHSW:
            DEBUG(DDR_MSG_FULL,
                  "<%ld> c:%d s:%d controlIndex: INDEX_CHSW%c\n", ' ');
            if ( pMotor->type == DDR_OMS_58_ST_CL ||
                 pMotor->type == DDR_OMS_58_SV_CL )
            /*
             *  With closed loop systems:
             *    1. reverse until home switch low, then ramp down
             *    2. reverse again until home switch high, then ramp down
             *    3. forward until home switch high, then ramp down
             *    4. change to slower indexing velocity
             *    5. forward until: 
             *          home switch low and
             *          encoder index pulse high and
             *          encoder phase A high and
             *          encoder phase B low then ramp down
             *    6. reverse back to zero position
             */

            {
                DEBUG(DDR_MSG_FULL,
                      "<%ld> c:%d s:%d controlIndex: using index pulse%c\n", ' ');

                sprintf (scratch,
                         "HL HR0 HH HR0 HH HM0 VL%ld HE HR0 HS MA0 GO ID ",
                         pDevice->indexVelocity);
            }

            /*
             *  With open loop systems, there is no index pulse:
             *    1. reverse until home switch low, then ramp down
             *    2. reverse again until home switch high, then ramp down
             *    3. forward until home switch high, then ramp down
             *    4. change to slower indexing velocity
             *    5. forward until home switch low, then ramp down
             *    6. reverse back to zero position
             */

            else
            {
                sprintf (scratch,
                         "HL HR0 HH HR0 HH HM0 VL%ld HL HR0 MA0 GO ID ",
                         pDevice->indexVelocity);
            }
            break;


        /*
         *  In INDEX_LLSW mode the device is instructed to move in
         *  the reverse direction at full speed until the lower
         *  limit switch is triggered, which will cause the OMS card
         *  to ramp the device down to a controlled stop.  The calling 
         *  code can then reset the position counter to zero if it 
         *  wishes.  This mode is used for devices with no home switch
         *  and is accomplished by commanding a relative move to a very 
         *  large negative position.
         */

        case DDR_INDEX_LLSW:
            DEBUG(DDR_MSG_FULL,
                  "<%ld> c:%d s:%d controlIndex: INDEX_LLSW%c\n", ' ');
            sprintf (scratch,"MR1000000000 GO ID ");
            break;


        /*
         *  In INDEX_ULSW mode the device is instructed to move in
         *  the forward direction at full speed until the upper
         *  limit switch is triggered, which will cause the OMS card
         *  to ramp the device down to a controlled stop.  The calling 
         *  code can then reset the position counter to zero if it 
         *  wishes.  This mode is used for devices with no home switch
         *  and is accomplished by commanding a relative move to a very 
         *  large positive position.
         */

        case DDR_INDEX_ULSW:
            DEBUG(DDR_MSG_FULL,
                  "<%ld> c:%d s:%d controlIndex: INDEX_ULSW%c\n", ' ');
            sprintf (scratch,"MR-1000000000 GO ID ");
            break;


        /*
         *  Undefined indexing modes are rejected here. 
         */

        default:
            SET_ERR_MSG("Invalid indexing mode requested");
            status = DRV_OMS_VME_S_INDEX_ERROR;
            break;

    }  /* End of switch (index) */

    if ( !status )
    {
        DEBUG(DDR_MSG_FULL,
              "<%ld> c:%d s:%d controlIndex: string: %s\n", 
               scratch);
	status = drvOmsVmeWriteMotor (pMotor->card,
				      pMotor->axis, 
				      scratch);
    }
    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * controlMotion
 *
 * INVOCATION:
 * status = controlMotion (pDevice, mode);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *)  record interface structure
 * (>) mode     (long)                      Motion control word.
 *
 * parameters accessed via the interface structure:
 *
 * (>) pPrivate     (DEV_CTL_OMS_PRIVATE *) motor control structure address
 * (>) simulation   (int)   current simulation mode
 * (>) position     (long)  current device position (steps)
 * (>) target       (long)  desired device position (steps)
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 * Return from controlIndex() or drvOmsVmeWriteMotor() else
 *  DRV_OMS_VME_S_INDEX_ERROR
 *  
 * PURPOSE:
 * Start, stop or abort the current motion.
 *
 * DESCRIPTION:
 * Control the motion of the selected motor based on the supplied mode.
 *
 * Possible actions are:
 *  DDR_MOVE_START -> Move to the target given in the device structure
 *  DDR_MOVE_STOP  -> Bring the motor to a controlled stop
 *  DDR_MOVE_ABORT -> Bring the motor to a VERY QUICK stop (may be out of step)
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 *-
 ************************************************************************
 */

static long controlMotion
(
    DEVICE_CONTROL_PRIVATE *pDevice,  /* deviceControl rec interface struct */
    long        mode                  /* motion control word                */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate; /* axis control struct */
    long    status = 0;                              /* function status     */


    DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d controlMotion: mode=<%ld>\n", mode);

    if (pMotor->ignoreMotion == TRUE)
    {
        DEBUG(DDR_MSG_FULL, 
                            "<%ld> c:%d s:%d controlMotion:clearing leftover ignoreMotion flag before starting%c\n",' ');
        semTake (pMotor->mutexSem, WAIT_FOREVER);
        pMotor->ignoreMotion = FALSE;
        semGive (pMotor->mutexSem);
    }

    /*
     *  In simulation mode set the target to be the current position to
     *  fake device motion when mode is GO.
     */
   
    if ( pDevice->simulation )
    {

        DEBUG(DDR_MSG_FULL, 
              "<%ld> c:%d s:%d controlMotion:simulating%c\n",' ');

        if ( mode != DDR_MOVE_GO )
        {
            semTake (pDevice->mutexSem, WAIT_FOREVER);
            pDevice->target = pDevice->position;
            semGive (pDevice->mutexSem);
        }
    }


    /*
     *  Otherwise send the appropriate motion command strings to the 
     *  OMS card.
     */

    else
    {
        /*
         *  Confirm that the requested card and axis were found at init time
         */

        if (!pMotor->exists)
        {
            SET_ERR_MSG("Motor hardware does not exist");
            return DRV_OMS_VME_S_CFG_ERROR;
        }

        /*
         *  Then generate OMS command strings according to the motion 
         *  control word and the contents of the interface structure.
         */
    
        switch ( mode )
        {

        /*
         *  In GO mode the OMS card is requested to re-position the device
         *  according to the prevailing index mode.
         */

        case DDR_MOVE_GO:
            /*
             *  Request is for move or index.
             */

            status = controlIndex( pDevice );
            DEBUG(DDR_MSG_MAX,
                  "<%ld> c:%d s:%d controlMotion: Done DDR_MOVE_GO%c\n", ' ');
            break;


        /*
         *  In STOP mode the device is commanded to stop the motion in
         *  progress. Follow with an 'ID' string to allow an interrupt upon
         *  completion.
         */

        case DDR_MOVE_STOP:
            status = drvOmsVmeWriteMotor (pMotor->card,
                                          pMotor->axis,
                                          "ST ID ");
            DEBUG(DDR_MSG_MAX,
                  "<%ld> c:%d s:%d controlMotion: Done DDR_MOVE_STOP%c\n",
                                     ' ');
            break;


        /*
         *  In ABORT mode the device is commanded to stop the motion in
         *  progress.   The difference between a STOP and an ABORT
         *  termination is that an ABORT first temporarily changes
         *  to a fast acceleration/deceleration.  This is done because
         *  there is a strong possibility that brakes will be applied
         *  at the same time.
         */

        case DDR_MOVE_ABORT:
            status = drvOmsVmeWriteMotor (pMotor->card,
                                          pMotor->axis,
                                          "AC2000000 ST ");
            DEBUG(DDR_MSG_MAX,
                  "<%ld> c:%d s:%d controlMotion: Done DDR_MOVE_ABORT%c\n",
                                     ' ');
            break;
        }
    }


    /*
     *  If the device driver code generated an error then recover the
     *  error message explaining what went wrong and save it in the
     *  interface structure.
     */

    if (status)
    {
        drvOmsVmeGetErrorMessage (pDevice->errorMessage);
        DEBUG(DDR_MSG_ERROR, 
              "<%ld> c:%d s:%d configure drive: %s\n",
              pDevice->errorMessage);
    }

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * controlPower
 *
 * INVOCATION:
 * status = controlPower (pDevice, power);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *)  Record interface structure.
 * (>) power    (long) Desired power state.
 *
 * parameters accessed via the interface structure:
 *
 * (>) pPrivate     (DEV_CTL_OMS_PRIVATE *) motor control structure address
 * (>) simulation   (int)   current simulation mode
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 * Return from drvOmsVmeWriteMotor() else 
 *  DRV_OMS_VME_S_CFG_ERROR
 *
 * PURPOSE:
 * Control motor power via OMS drive hardware auxilliary power bit
 *
 * DESCRIPTION:
 * Set the state of the OMS card auxiliary bit for the given axis by 
 * sending the ASCII string 'AF' to turn the power ON or 'AN' to turn
 * motor power OFF.
 *
 * In simulation mode simply return.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long controlPower
(
    DEVICE_CONTROL_PRIVATE *pDevice,  /* deviceControl rec interface struct */
    long power                        /* power enable/disable flag          */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate;
    long status = 0;

    DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d controlPower:<%ld>\n", power);

    /*
     *  Only write to the hardware if we are not in simulation mode
     */

    if (pDevice->simulation)
    {
	/*
	 *  Simulating, can't really do anything, just return.. 
	 */

	DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d controlPower:simulating%c\n" ,' ');
    }
    else
    {
        if (!pMotor->exists)
        {
            SET_ERR_MSG("Motor hardware does not exist");
            return DRV_OMS_VME_S_CFG_ERROR;
        }

        /*
         *  Send a command string to the OMS card to turn motor power
         *  ON (AF) or OFF (AN).  Unfortunately these work backwards.
         */

        status = drvOmsVmeWriteMotor (pMotor->card,
                                      pMotor->axis,
                                      (power) ? "AF " : "AN ");
    }


    /*
     *  If the driver code reported an error then recover the error message
     *  and copy it into the interface structure.
     */

    if (status)
    {
        drvOmsVmeGetErrorMessage (pDevice->errorMessage);
        DEBUG(DDR_MSG_ERROR, 
              "<%ld> c:%d s:%d controlPower: failed: %s\n",
              pDevice->errorMessage);
    }

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * devInit
 *
 * INVOCATION:
 * status = devInit (pass);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pass  (unsigned) Pass number (called twice during initialization)
 *
 * FUNCTION VALUE:
 * (long) initialization pass success code.
 * O if successful otherwise -1
 *
 * PURPOSE:
 * Initialize the device control record support module
 *
 * DESCRIPTION:
 * Called before and after an EPICS database has been loaded.
 * First (before) invocation:
 *    calls drvOmsVmeInit() 
 *    creates the linked list for records to be processed by the scan task, 
 *    creates the omsScanTask
 *    sets the scanning rate.
 * Second (after) invocation simply returns.
 *
 * EXTERNAL VARIABLES:
 * deviceControlScanList (ELLLIST *) scan task linked list pointer saved here.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long devInit 
(
    unsigned after                  /* System initialization pass number    */
)
{
    int deviceCtlTaskId;            /* control task ID                      */
    char errorMsg[MAX_STRING_SIZE]; /* returned error message               */
    long status = 0;                /* returned function status             */


    /*
     *  Everything is done on the first pass so simply ignore the second
     */

    if (after)
    {
        return status;
    }


    /*
     *  Initialize OMS VME drive support.  drvOmsVmeInit() returns the 
     *  number of OMS cards initialized or an error code.
     */
   
    status = drvOmsVmeInit();
    if (status < 0)
    {
        drvOmsVmeGetErrorMessage (errorMsg);
        logMsg ("\nError initializing drv: %s!\n", (int)errorMsg,0,0,0,0,0);
        return status;
    }
    else
    {
        logMsg("\n%d OMS motor card(s) initialized\n",status,0,0,0,0,0);
        status = 0;
    }

    /*
     * Create a linked list of records to be serviced every time the
     * scan task runs.
     */
    
    ellInit(&deviceControlScanList);


    /*
     * create the deviceControl record support OMS hardware scanning task
     */

    deviceCtlTaskId = taskSpawn (
        "tOmsVme",                     /* task name */
        DDR_OMS_SCAN_TASK_PRIORITY,    /* priority */
        DDR_OMS_SCAN_TASK_OPTIONS,     /* options */
        DDR_OMS_SCAN_TASK_STACK,       /* stack size */
        omsScanTask,                   /* task entry point */
        0,0,0,0,0,0,0,0,0,0);          /* invocation args 1 to 10 */

    /*
     * Spawning of task failed.
     */
 
    if (!deviceCtlTaskId)
    {
        status = -1;
        logMsg ("\nCannot create DEV CTL OMS scan task!\n",0,0,0,0,0,0);
    }


    /*
     *  Set the period for omsScanTask (number of clock ticks per scan):
     *  sysClkRateGet returns number of clock ticks per second and
     *  DDR_OMS_SCAN_TASK_RATE is the number of scans per second 
     *  we want.
     */

    scanTaskPeriod = (int)sysClkRateGet() / DDR_OMS_SCAN_TASK_RATE;
    logMsg ("\nPeriod of omsScanTask = %d clock ticks\n", scanTaskPeriod,0,0,0,0,0);

    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * devInitRec
 *
 * INVOCATION:
 * status = devInitRec (pdr);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pdr  (DEVICE_CONTROL_RECORD *) deviceControl record structure.
 *
 * FUNCTION VALUE:
 * (long) initialization success code.
 *
 * parameters accessed via the record structure:
 *
 * (>) dpvt         (DEV_CTL_PRIVATE *) interface structure address
 *
 * parameters accessed via the interface structure:
 *
 * (<) pPrivate     (DEV_CTL_OMS_PRIVATE *) motor control structure address
 *
 * PURPOSE:
 * Initialize an instance of deviceControl record support.
 *
 * DESCRIPTION:
 * Initialize each instance of this record:
 *   Create device private structure
 *   Confirm that the card exists and type is valid.
 *   Confirm that the axis type is valid and that there are no duplicates.
 *   Initialize critical device and private structure fields.
 *   Add to linked list of structure to be processed by the OMS scan task.
 *
 * EXTERNAL VARIABLES:
 * deviceControlScanList (ELLLIST *) private structure pointer
 * added to this list.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long devInitRec
(
    DEVICE_CONTROL_RECORD    *pdr    /* deviceControl record structure      */
)
{
    DEVICE_CONTROL_PRIVATE
            *pDevice = (DEVICE_CONTROL_PRIVATE *) pdr->dpvt; /* ifc struct  */
    DEV_CTL_OMS_PRIVATE *pMotor = NULL;           /*   motor control struct */
    DEV_CTL_OMS_PRIVATE *pAxis = NULL;            /*   motor control struct */
    long    status = 0;                           /* function status return */
    long cardType;                                /*    OMS VME58 card type */


    DEBUG(DDR_MSG_MAX, "<%ld> c:%d s:%d devInitRec: entry%c\n", ' ');


    /*
     *  Create a motor control structure for this record
     */

    pMotor = malloc (sizeof(DEV_CTL_OMS_PRIVATE));
    if ( pMotor == NULL )
    {
        status = -1;
        recGblRecordError(status, pdr, __FILE__ ":no room for device private");
        return status;
    }


    /*
     *  Save the motor control struct address in the record interface struct
     */
    
    pDevice->pPrivate = (void *) pMotor;


    /*
     *  Create the MUTEX semaphore to protect the record interface structure
     *  during asynchronous callback access
     */
   
    pMotor->mutexSem = NULL;
    if ((pMotor->mutexSem = semMCreate(SEM_Q_PRIORITY|SEM_INVERSION_SAFE)) ==
         NULL)
    {
        status = -1;
        recGblRecordError (status, pdr, __FILE__ ":no room for motor private");
        return status;
    }


    /*
     *  Save the address of the record structure and record interface 
     *  structure in the motor control structure.
     */ 

    semTake (pMotor->mutexSem, WAIT_FOREVER);
    
    pMotor->pDevice = pDevice;
    pMotor->pRecord = pdr;


    /*
     *  Save the card and axis for this motor in the motor control structure
     */
     
    pMotor->card = pdr->out.value.vmeio.card;
    pMotor->axis = pdr->out.value.vmeio.signal;


    /*
     *  Confirm that the interface card exists and get its type
     */

    cardType = drvOmsVmeGetCardType (pMotor->card);


    /*
     * From the card type, determine what the axis type is.
     * If it fails, the error is returned and printed above.
     */

    if (cardType == DDR_OMS_58_4E)
    {
        pMotor->type = DDR_OMS_58_ST_CL;
    }
    else if ( (cardType == DDR_OMS_58_4) ||
              (cardType == DDR_OMS_58_8) ||
              ( (cardType == DDR_OMS_58_2S2) && (pMotor->axis > 1) ) ||
              ( (cardType == DDR_OMS_58_2S6) && (pMotor->axis > 1) ) ||
              ( (cardType == DDR_OMS_58_4S4) && (pMotor->axis > 3) ) ||
              ( (cardType == DDR_OMS_58_6S2) && (pMotor->axis > 5) )  )
    {
        pMotor->type = DDR_OMS_58_ST_OL;
    }
    else if ( (cardType == DDR_OMS_58_4S) ||
              (cardType == DDR_OMS_58_8S) ||
              ( (cardType == DDR_OMS_58_2S2) && (pMotor->axis < 2) ) ||
              ( (cardType == DDR_OMS_58_2S6) && (pMotor->axis < 2) ) ||
              ( (cardType == DDR_OMS_58_4S4) && (pMotor->axis < 4) ) ||
              ( (cardType == DDR_OMS_58_6S2) && (pMotor->axis < 6) )  )
    {
        pMotor->type = DDR_OMS_58_SV_CL;
    }
    else
    {
        pMotor->type = 0;
    }

    if (pMotor->type != 0)
    {
        pMotor->exists = TRUE;
        DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d Got motor type: %d\n", pMotor->type );
    }
    else
    {
        pMotor->exists = FALSE;
        DEBUG(DDR_MSG_ERROR, "<%ld> c:%d s:%d Invalid motor type: %d (okay if simulating)\n", pMotor->type );
    }


    /*
     *  Initialize the rest of the control structure
     *
     */

    pMotor->updateState = TRUE;
    pMotor->earlyDone = FALSE;
    pMotor->ignoreMotion = FALSE;
    pMotor->simVelocity = 1;
    pMotor->scansLeft = 0;
    pMotor->stoppedCntr = 0;
    pMotor->status = 0;
    semGive (pMotor->mutexSem);


    /*
     * Check for duplicate motors on the scan list
     * Initialize with first entry in list
     */

    pAxis = (DEV_CTL_OMS_PRIVATE *) ellFirst (&deviceControlScanList);
        
    /*
     *  Compare with each non-NULL entry in list
     */

    while (pAxis)
    {  
        if ( (pAxis->card == pMotor->card) &&
             (pAxis->axis == pMotor->axis) )
        {
	   /*
            * Duplicate exists so prevent BOTH motors from getting used
            * except in simulation
            */
            pAxis->exists = FALSE;
            semTake (pMotor->mutexSem, WAIT_FOREVER);
            pMotor->exists = FALSE; 
            semGive (pMotor->mutexSem);
            DEBUG(DDR_MSG_FATAL, "\n<%ld> c:%d s:%d devInitRec: OMS motor duplicate%c\n", ' ');
        }

        /* get the next one off the list */
        pAxis = (DEV_CTL_OMS_PRIVATE *) ellNext (&pAxis->node);
       
    }   /* end of motor scanning loop */


    /*
     * And add it to the scan list
     */

    ellAdd (&deviceControlScanList, &pMotor->node);

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * omsScanTask
 *
 * INVOCATION:
 * status = omsScanTask ();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *
 * FUNCTION VALUE:
 * (int) task exit code (always -1) although should never get there.
 *
 * PURPOSE:
 * Monitor device motion and status.
 *
 * DESCRIPTION:
 *
 * The scan task executes at a pre-determined rate.   Each time it processes
 * it runs through the list of motors that have deviceControl records 
 * controlling them and manages their operation using the following algorithm:
 *
 *  DO forever:
 *  {
 *      Get the first entry on the scan list
 *      While the entry is a valid motor:
 *      {
 *          Recover the associated record and interface structures.
 *
 *          If we are in simulation mode:
 *          {
 *              Motor status is always good and encoder is always 0.
 *              Clear checkLimits flag.
 *              On 1st pass only:
 *              {
 *                  Clear home and limit switches.
 *                  Set updateState flag.
 *              }
 *              Simulate move by updating current position with the distance 
 *                the virtual motor would have gone between scans by adding or 
 *                subtracting the simulated velocity offset until the desired 
 *                target position is reached.
 *          }
 *          Else if there is hardware attached to the device:
 *          {
 *              Read current position and encoder from the OMS card. 
 *              If error as a result of position/encoder update
 *              {
 *                  Recover the status error message from the driver level
 *                    and don't bother checking for encoder errors or QA.
 *              }
 *              Else 
 *              {
 *                  If either just starting the move OR just finished
 *                    (I.e. checkLimits flag is set or moving flag is set  
 *                     and position unchanged):
 *                  {
 *                      Set updateState flag.
 *                      Read current states from OMS card:
 *                        home switch
 *                        high and low limit switches
 *                        axisDone flag 
 *                      If error as a result of motor state update recover
 *                         the status error message from the driver level.
 *                      Clear checkLimits flag.
 *                  }
 *              }
 *          }
 *          Else not simulating and there is no hardware to control
 *              Save previous position, encoder, limits & home state values.
 *
 *          Initialize local flags:
 *            Clear rescan flag.
 *            Set done flag.
 *
 *          If there is time remaining on the delay timer:
 *          {
 *              Decrement the delay timer.
 *              If the timer has reached zero set the timeout flag.
 *          }
 *
 *          If motor position & state were successfully read
 *          {
 *              If moving flag is set AND position is unchanged
 *              {
 *                  If we had started/finished in the previous scan
 *                    clear the earlyDone flag and set the done flag.
 *                  If status returned indicates either axisDone OR 
 *                    we're simulating, then we've stopped.
 *                    
 *                  {
 *                      Clear moving flag.
 *                      Set rescan flag.
 *                      Clear motion stopped scan counter 
 *                  }
 *                  Else status returned indicates either
 *                    highLimit OR lowLimit
 *                  {
 *                      If stopped and in limit for 5 scans
 *                      {
 *                          Issue "EF ID" string to set up interrupt when done
 *                      }
 *                      Else  Increment motion stopped counter
 *                  }
 *                  Else moving flag is set but not sure why
 *                      If motion has been stopped for 20 scans
 *                          Clear the moving flag
 *                          Set the rescan flag
 *              }
 *              Else if position has changed
 *              {
 *                  If moving flag not set & not new pos because of setPosition
 *                  {
 *                      Set moving flag
 *                      If already 'done', set the earlyDone flag
 *                  }
 *                  Else If position changed because of setPosition()
 *                      Clear ignoreMotion flag
 *                  Update position value.
 *                  Clear done flag.
 *                  Set rescan flag.
 *              }
 *
 *              If current encoder position has changed:
 *              {
 *                  If not moving and not simulating:
 *                  {
 *                       Indicate spontaneous motion.
 *                  }
 *                  Update encoder value.
 *                  Set rescan flag.
 *              }
 *
 *              If updateState flag set:
 *              {
 *                  If the home or either limit switch states has changed:
 *                  {
 *                      Update switch values.
 *                      Set rescan flag.
 *                  }
 *                  Clear updateState flag.
 *              }
 *          }
 * 
 *          If the motor status has changed:
 *          {
 *              Update motor status for record.
 *              Set rescan flag.
 *          }
 *
 *          If the timeout or rescan flags have been set:
 *          {
 *              Signal internal reprocessing by setting callback flag.
 *              Recover the address of the associated record's process func.
 *              Lock the record to prevent anyone else form processing it.
 *              Process the record by calling the process function.
 *              Unlock the record.
 *          }
 *
 *          Get the next entry on the scan list.
 *
 *      }
 *
 *      Calculate number of system ticks remaining until next scan starts.
 *      Put task to sleep for this number of ticks.
 *  }                          
 *                       
 *
 *
 *
 * EXTERNAL VARIABLES:
 * deviceControlScanList (ELLLIST *)   linked list of attached devices.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static int omsScanTask 
(
    int a1, int a2, int a3, int a4, int a5,     /* invocation arguments     */
    int a6, int a7, int a8, int a9, int a10     /* none of which are used   */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = NULL; /* motor control structure          */
    DEVICE_CONTROL_PRIVATE *pDevice;    /* deviceControl rec interface str  */
    DEVICE_CONTROL_RECORD *pdr;         /* deviceControl record structure   */
    struct rset *pRset;                 /* deviceControl rec function str   */
    int rescan;                         /* record must be processed flag    */
    int done;                           /* motion finished flag             */
    int axisDone;                       /* axis done flag from hardware     */
    int lowLimit;                       /* lower limit switch state         */
    int highLimit;                      /* upper limit switch state         */
    int homeSwitch;                     /* home switch state                */
    long position;                      /* motor position counter value     */
    long encoder;                       /* encoder value                    */
    int startTick;                      /* system time at start of task     */
    int taskTime;                       /* time taken to execute task       */
    long status = 0;                    /* taskDelay return status          */


    /*
     *  The scan task operates as an infinite loop...
     */

    while (TRUE)
    { 
        startTick = (int)tickGet();


        /*
         *  Get the first entry on the scan list
         */
        
        pMotor = NULL;
        pMotor = (DEV_CTL_OMS_PRIVATE *) ellFirst (&deviceControlScanList);
        
        /*
         *  Process the entry if it not NULL
         */

        while (pMotor)
        {  
            /*
             *  Recover the addresses of the record and record interface
             *  structures associated with this motor.
             */

            pDevice = pMotor->pDevice;
            pdr = pMotor->pRecord;
                            

            /*
             *  In simulation there is no real hardware to control or 
             *  query so simply make it look like things are happening
             *  normally.
             */

            if (pDevice->simulation)
            {
                /*
                 *  Motor status is always good, encoder is always 0
                 *  and no need to check the limits when simulating.
                 */

                semTake (pMotor->mutexSem, WAIT_FOREVER);
                pMotor->status = 0;
                semGive (pMotor->mutexSem);
                encoder = 0;
                semTake (pDevice->mutexSem, WAIT_FOREVER);
                pDevice->checkLimits = 0;
                semGive (pDevice->mutexSem);

                /*
                 *  If the switch states haven't been cleared then it's
                 *  the first pass while simulating so clear the local
                 *  switch variables and set the updateState flag for later.
                 */

                if ( pDevice->lowLimit   != 0 || 
                     pDevice->highLimit  != 0 || 
                     pDevice->homeSwitch != 0   )
                {
                    lowLimit = 0;
                    highLimit = 0;
                    homeSwitch = 0;

                    semTake (pMotor->mutexSem, WAIT_FOREVER);
                    pMotor->updateState = TRUE;
                    semGive (pMotor->mutexSem);
                    DEBUG(DDR_MSG_FULL, "<%ld> c:%d s:%d omsScanTask:clearing limit and home switches for simulation mode: %c\n", ' ');

                }

                /*
                 *  Fake motion by running a virtual motor.  No acceleration
                 *  or deceleration is done but the motor takes a realistic
                 *  time to get where it is going.  Move the virtual motor
                 *  the distance the real motor would have gone between
                 *  scans by adding or subtracting the simulated velocity
                 *  offset until the desired target position is reached.
                 */
                if (pDevice->simulation == DDR_SIM_FULL)
                {
                    if (pDevice->position < pDevice->target)
                    {
                        position = pDevice->position + pMotor->simVelocity;
                    }
                    else if (pDevice->position > pDevice->target)
                    {
                        position = pDevice->position - pMotor->simVelocity;
                    }
                    else
                    {
                        position = pDevice->position;
                    }
                }
                else if (pDevice->simulation == DDR_SIM_FAST)
                {
                    position = pDevice->target;
                }

                /*
                 *  Getting within one offset step of the target position
                 *  is close enough.   Make the position equal the target.
                 */

                if (abs(pDevice->target - pDevice->position) <
                    pMotor->simVelocity)
                {
                    position = pDevice->target;
                }               
            }   /* end of simulation mode */


            /*
             *  Not simulating so check to see if the card and axis were found
             *  during system initialization.  
             */

            else if (pMotor->exists)
            {
                /* 
                 *  Update the motor position and encoder value with the
                 *  current values from the OMS card.
                 */

                semTake (pMotor->mutexSem, WAIT_FOREVER);

                pMotor->status = drvOmsVmeMotorPosition (pMotor->card,
                                                         pMotor->axis,
                                                         &position,
                                                         &encoder);
                semGive (pMotor->mutexSem);

                /*
                 *  If there was a problem reading the motor position recover 
                 *  the status message that describes what went wrong and
                 *  don't bother checking for encoder errors or reading the
                 *  motor state.
                 */

                if ( pMotor->status )
                {
                    drvOmsVmeGetErrorMessage (pDevice->errorMessage);
                    DEBUG(DDR_MSG_ERROR,
                            "<%ld> c:%d s:%d Get motor position status = %ld\n",
                            pMotor->status);
                }
                else if ( pDevice->checkLimits ||
                         (pDevice->moving && (position == pDevice->position)))
       	        {
                    /*  Otherwise carry on
                     *
                     *  Update the motor state information (limits, home state
                     *  and done flag) when the motor is either just starting 
                     *  the move OR just finished (I.e. checkLimits flag is set 
                     *  or moving flag is set and position unchanged).
                     *
                     *  These updates are only done when requested to eliminate 
                     *  unnecessary communication with the OMS card.
                     */

                    
                    DEBUG(DDR_MSG_FULL,
                        "<%ld> c:%d s:%d Get motor state (checkLimits:%d)\n",
                        pDevice->checkLimits);

                    semTake (pMotor->mutexSem, WAIT_FOREVER);
                    pMotor->updateState = TRUE;

                    pMotor->status = drvOmsVmeMotorState (pMotor->card,
                                                          pMotor->axis,
                                                          &lowLimit,
                                                          &highLimit,
                                                          &homeSwitch,
                                                          &axisDone);
                    semGive (pMotor->mutexSem);

                    /*
                     *  If there was a problem reading the motor state, recover 
                     *  the status message that describes what went wrong.
                     */

                    if (pMotor->status)
                    {
                        drvOmsVmeGetErrorMessage (pDevice->errorMessage);
                        DEBUG(DDR_MSG_ERROR,
                            "<%ld> c:%d s:%d Get motor state status = %ld\n",
                            pMotor->status);
                    }

                    semTake (pDevice->mutexSem, WAIT_FOREVER);
                    pDevice->checkLimits = 0;
                    semGive (pDevice->mutexSem);               
                }

            }   /* end of things to do if the motor exists */


            /*
             *  Otherwise the motor does not exist so simply maintain
             *  the status quo.   This will prevent any action further
             *  on.
             */

            else
            {
                position = pDevice->position;
                encoder = pDevice->encoder;
                lowLimit = pDevice->lowLimit;
                highLimit = pDevice->highLimit;

                semTake (pDevice->mutexSem, WAIT_FOREVER);
                pDevice->status = pMotor->status;
                pDevice->moving = FALSE;
                semGive (pDevice->mutexSem);               
            }


            /*
             *  Control and monitoring is complete, now compare the
             *  current state to the starting state to determine if
             *  the associated record needs to be re-processed.
             */

            /*
             *  Initialize local flags
             */

            rescan = FALSE;
            done = TRUE;

            semTake (pDevice->mutexSem, WAIT_FOREVER);


            /*
             *  If there is time left on the delay timer then 
             *  decrement the counter here.  If this results in
             *  the timer reaching zero then set the timeout flag.
             *  Remember that the timer is counting the number of 
             *  times omsScanTask will run before the timeout occurs.
             */
 
            if (pMotor->scansLeft)
            {
                pDevice->timeout = (--pMotor->scansLeft <= 0);
            }


            /*
             *  Check motor status to see if motor position and state  
             *  were successfully read.
             */

            if ( !pMotor->status )
            {

                /*  
                 *  Status is okay so check to see if the motor was moving
                 *  the last time it was checked.  If the position returned by the 
                 *  card has also not changed, then we may have stopped.
                 */

                if ( pDevice->moving == TRUE       &&
                     position == pDevice->position    )
                {

                    if ( (pMotor->earlyDone == TRUE) && (axisDone == 0) )         
                    {
                        DEBUG(DDR_MSG_FULL,
                           "<%ld> c:%d s:%d omsScanTask:motion & done flag detected in the same scan%c\n", ' ');
                        axisDone = 1;
                        pMotor->earlyDone = FALSE;
                    }
                    /*  
                     *  If the axis motion is done or axis is simulated, clear the
                     *  moving flag and leave done set.  Set local rescan flag
                     *  to indicate that the record needs to see this. Then clear 
                     *  the motion stopped counter for the next move.
                     */

                    if ( axisDone || pDevice->simulation )
                    {
                        DEBUG(DDR_MSG_MIN,
                           "<%ld> c:%d s:%d omsScanTask:device motion stopped%c\n",
                           ' ');
                        pDevice->moving = FALSE;
                        rescan = TRUE;
                        semTake (pMotor->mutexSem, WAIT_FOREVER);
                        pMotor->stoppedCntr = 0;
                        semGive (pMotor->mutexSem);
                    }

                    /*
                     *  If we're in a limit then we may not yet be stopped.
                     */

                    else if ( highLimit || lowLimit )
                    {
                        /*
                         *  The moving flag is still set, we're still in a limit
                         *  and the position remains unchanged.  Let it settle
                         *  in this condition for five scans.
                         */

                        if (pMotor->stoppedCntr > 5)
                        {
                            DEBUG(DDR_MSG_MIN,"<%ld> c:%d s:%d omsScanTask:device motion stopped in limit - issuing EF ID string%c\n",' ');

                            /*  
                             *  We've stopped and given the limits time to settle.
                             *  Now we need to send an "ID" command to set up
                             *  the interrupt when done flag. 
                             */

                            semTake (pMotor->mutexSem, WAIT_FOREVER);

                            pMotor->status = drvOmsVmeWriteMotor (pMotor->card,
                                                                  pMotor->axis,
                                                                  "EF ID ");

                            semGive (pMotor->mutexSem);

                            /*
                             *  If there was a problem reading the motor state  
                             *  recover the status message that describes what 
                             *  went wrong.
                             */

                            if (pMotor->status)
                            {
                                drvOmsVmeGetErrorMessage (pDevice->errorMessage);
                                DEBUG(DDR_MSG_ERROR,
                                  "<%ld> c:%d s:%d Write motor stop status = %ld\n",
                                  pMotor->status);
                            }
                        }
                        else
                        {
                            /*
                             *  Increment motion stopped counter.
                             */

                            semTake (pMotor->mutexSem, WAIT_FOREVER);
                            pMotor->stoppedCntr++;
                            semGive (pMotor->mutexSem);
                        }
                    }
                    else
                    {
                        /*
                         *  This kludge has to stay until we figure out how to 
                         *  handle bootup initialization where the moving flag
                         *  gets set without an ID setting up the 'done' interrupt.
                         *  The position gets initialized to 0 steps but if the IOC
                         *  happens to have been left at anything non-zero (very
                         *  likely but not for sure if the IOC is rebooted but
                         *  not power cycled) then the change in position will
                         *  cause the moving flag to get set.  However there is no
                         *  'done' interrupt to cause moving to get cleared.
                         */

                        if (pMotor->stoppedCntr > 20)
                        {
 
                            DEBUG(DDR_MSG_WARNING,
                               "<%ld> c:%d s:%d omsScanTask:device motion stopped without done flag%c\n",' ');
                            pDevice->moving = FALSE;
                            rescan = TRUE;
                            semTake (pMotor->mutexSem, WAIT_FOREVER);
                            pMotor->stoppedCntr = 0;
                            semGive (pMotor->mutexSem);
                        }
                        else
                        {
                            /*
                             *  Increment motion stopped counter.
                             */

                            semTake (pMotor->mutexSem, WAIT_FOREVER);
                            pMotor->stoppedCntr++;
                            semGive (pMotor->mutexSem);
                        }
                    }
                }
                else if ( position != pDevice->position ) 
                {
                    /*  
                     *  The position has changed.  Have we just started a motion?
                     *  Check to see if axis was already moving and if not, set 
                     *  the moving flag.  But avoid getting fooled by the case 
                     *  where the position has changed because of setPosition.
                     */

                    if ( (pDevice->moving != TRUE)      &&
                         (pMotor->ignoreMotion ==  FALSE) )
                    {
                        DEBUG(DDR_MSG_MIN, 
                            "<%ld> c:%d s:%d omsScanTask:device motion started%c\n",
                            ' ');
                        pDevice->moving = TRUE;
                        DEBUG(DDR_MSG_MAX, 
                            "<%ld> c:%d s:%d omsScanTask:new position:%ld\n", 
                            position);
                        DEBUG(DDR_MSG_MAX, 
                            "<%ld> c:%d s:%d omsScanTask:old position:%ld\n", 
                            pDevice->position);

                        if (axisDone)
                        {
                            /*
                             *  The motion starting was detected in the same
                             *  scan as the done flag (short move) so set the
                             *  earlyDone flag.
                             */

                            semTake (pMotor->mutexSem, WAIT_FOREVER);
                            pMotor->earlyDone = TRUE;
                            semGive (pMotor->mutexSem);
                        }

		    }
                    else if (pMotor->ignoreMotion ==  TRUE)
                    {
                        DEBUG(DDR_MSG_FULL, 
                            "<%ld> c:%d s:%d omsScanTask:clearing ignoreMotion flag%c\n",' ');
                        semTake (pMotor->mutexSem, WAIT_FOREVER);
                        pMotor->ignoreMotion = FALSE;
                        semGive (pMotor->mutexSem);
		    }

                    /*
                     *  Update the structure position and clear the local done flag,
                     *  then set local rescan flag to indicate that the record 
                     *  needs to see this.
                     */

                    pDevice->position = position;
                    done = FALSE;
                    rescan = TRUE;
                    DEBUG(DDR_MSG_MAX, "<%ld> c:%d s:%d omsScanTask:device position change%c\n", ' ');
	        }


                /*
                 *  Update current encoder position regardless, otherwise we
                 *  get a stalled motor at startup. If the motor should not be 
                 *  moving  then you're seeing some spontaneous motion, so
                 *  update the encoder and set rescan flag.  Plus or minus a few 
                 *  counts is probably okay but make this determination at the 
                 *  rec level.
                 */

                if ( pdr->ueip && pDevice->encoder != encoder ) 
                { 
                    if ( !(pDevice->moving) && !(pDevice->simulation) )
                    {
                        DEBUG(DDR_MSG_MIN, "<%ld> c:%d s:%d omsScanTask:spontaneous encoder change:%ld counts\n", (pDevice->encoder - encoder));
                    }
                    pDevice->encoder = encoder;
                    rescan = TRUE;
                }


                /*
                 *  Look for changes in the home and limit switch states, if 
                 *  they have changed the record needs to know about it.
                 */

                if ( pMotor->updateState )
                {
                    if (lowLimit != pDevice->lowLimit)
                    {
                        DEBUG(DDR_MSG_FULL,
                          "<%ld> c:%d s:%d omsScanTask:low limit change:%d\n",
                          lowLimit);
                        pDevice->lowLimit = lowLimit;
                        rescan = TRUE;
                    }

                    if (highLimit != pDevice->highLimit)
                    {
                        DEBUG(DDR_MSG_FULL,
                          "<%ld> c:%d s:%d omsScanTask:high limit change:%d\n",
                          highLimit);
                        pDevice->highLimit = highLimit;
                        rescan = TRUE;
                    }

                    if (homeSwitch != pDevice->homeSwitch)
                    {
                        DEBUG(DDR_MSG_FULL,
                          "<%ld> c:%d s:%d omsScanTask:home switch change:%d\n",
                          homeSwitch);
                        pDevice->homeSwitch = homeSwitch;
                        rescan = TRUE;
                    }

                    semTake (pMotor->mutexSem, WAIT_FOREVER);
                    pMotor->updateState = FALSE;
                    semGive (pMotor->mutexSem);
                }
            }   /*  end of if ( !pMotor->status )... */

            /*
             *  Update the motor status if it's changed.
             */

            if (pMotor->status != pDevice->status)
            {
                DEBUG(DDR_MSG_MIN,
                 "<%ld> c:%d s:%d omsScanTask:motor status != device status%c\n",
                 ' ');
                pDevice->status = pMotor->status;
                rescan = TRUE;
            }

            semGive (pDevice->mutexSem);


            /*
             *  Check the timeout and rescan flags.   If either of them
             *  have been set then re-processes the associated record.
             */

            if (pDevice->timeout || (rescan && !pDevice->initializing))
            {
                if ( pDevice->debug == DDR_MSG_MAX )
                {
                    logMsg ("<%d> callback! <%x,%x,%ld,%ld,%d>\n",
                            (int)tickGet(),
                            (pMotor->card << 4) + pMotor->axis,
                            ((pDevice->highLimit << 12) +
                             (pDevice->lowLimit << 8) +
                             (pDevice->moving << 4) +
                             (pDevice->timeout)),
                            pDevice->position,
                            pDevice->encoder,
                            pDevice->status);
                }


                /*
                 *  Setting the callback flag tells the record processing
                 *  function that it has been called by the device support
                 */
                DEBUG(DDR_MSG_MAX, "<%ld> c:%d s:%d omsScanTask:setting callback flag%c\n", ' ');
                semTake (pDevice->mutexSem, WAIT_FOREVER);
                pDevice->callback = TRUE;
                semGive (pDevice->mutexSem);


                /*
                 *  Re-process the record by calling its process function
                 *  directly.   The processing function is recovered from the
                 *  record function structure.  Scan lock prevents anyone else
                 *  from accessing the record while it is being processed
                 *  from this task.
                 */

                pRset = (struct rset *) pdr->rset;
                dbScanLock ((struct dbCommon *) pdr);
                (*pRset->process) (pdr);
                dbScanUnlock ((struct dbCommon *) pdr);
            }
            
            
            /*
             * And move on to the next motor on the list
             */
            
            pMotor = (DEV_CTL_OMS_PRIVATE *) ellNext (&pMotor->node);
       
        }   /* end of motor scanning loop */


        /*
         * Subtract from the delay the amount of time spent processing this
         * loop.   This should allow the scan task rate to remain more stable
         * despite changes in CPU usage/speed ... AWE 07/99
         */

        taskTime = (int)tickGet() - startTick;

        if (taskTime < scanTaskPeriod)
        {
            status = taskDelay (scanTaskPeriod - taskTime);
            if (status < 0)    /* call to taskDelay failed */
            {
                SET_ERR_MSG("omsScanTask Warning: taskDelay failed ..timing off");
                DEBUG(DDR_MSG_WARNING, 
                      "<%ld> c:%d s:%d omsScanTask: taskDelay failed, so timing off - error:%ld\n", status);
            }
        }
        else
        {
            DEBUG(DDR_MSG_MIN,
                   "<%ld> c:%d s:%d omsScanTask:Slow by %d ticks!\n",taskTime);
        }

    }  /* end of main loop */

    return( -1 );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * setDelay
 *
 * INVOCATION:
 * status = setDelay (pDevice, delay);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *) Record interface structure
 * (>) delay    (long) Desired delay time in 0.1 second increments
 *
 * FUNCTION VALUE:
 * (long) Always returns 0.
 *
 * PURPOSE:
 * Re-process the calling record after a preset interval
 *
 * DESCRIPTION:
 * Load and enable the background delay timer.   Each time the scan task
 * executes the delay timer is decremented.   When this timer expires
 * the scan task will re-process the record.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * omsScanTask
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long setDelay
(
    DEVICE_CONTROL_PRIVATE *pDevice,  /* deviceControl rec interface struct */
    long        delay                 /* time to delay in 0.1 second units  */
                                      /*    before record is re-processed   */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate;


    /*
     *  Delay is time in 0.1 second units (I.e.  a value of 20 is
     *  2.0 seconds and 36 is 3.6 seconds.   Convert this time to the
     *  number of oms scans, so that the background task can count them
     *  down (scansLeft becomes a timer indicating the number of times
     *  omsScanTask will run before the specified timeout).   Setting 
     *  scansLeft to a non-zero number automatically starts the
     *  countdown timer.   Setting scansLeft to zero disables the
     *  timer. 
     */

    semTake (pMotor->mutexSem, WAIT_FOREVER);
    pMotor->scansLeft = (delay * DDR_OMS_SCAN_TASK_RATE) / 10;
    semGive (pMotor->mutexSem);

    DEBUG(DDR_MSG_MAX, 
          "<%ld> c:%d s:%d Set delay to <%ld>\n", pMotor->scansLeft);

    return( 0 );

}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * setPosition
 *
 * INVOCATION:
 * status = setPosition (pDevice, position);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pDevice  (DEVICE_CONTROL_PRIVATE *) Record interface structure.
 * (>) positon    (long) Desired position register value in steps.
 *
 * FUNCTION VALUE:
 * (long) processing success code.
 * Return from drvOmsVmeWriteMotor() else
 *  DRV_OMS_VME_S_CFG_ERROR
 *
 * PURPOSE:
 * Load the motor position counter with the specified value.
 *
 * DESCRIPTION:
 * Load the given value into the drive electronics position counter.
 * In simulation mode simply update the local position counter.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long setPosition
(
    DEVICE_CONTROL_PRIVATE *pDevice,  /* deviceControl record interface str */
    long             position         /* absolute position for this motor   */
)
{
    DEV_CTL_OMS_PRIVATE *pMotor = pDevice->pPrivate;  /* motor control str  */
    char scratch[DRV_OMS_VME_MAX_MSG_LEN];      /* message assembly buffer  */
    long status = 0;                        /* function return status       */

    DEBUG(DDR_MSG_FULL, 
          "<%ld> c:%d s:%d setPosition: define current position as:%ld \n", position);


    semTake (pMotor->mutexSem, WAIT_FOREVER);
    pMotor->ignoreMotion = TRUE;
    semGive (pMotor->mutexSem);

    /*
     *  In simulation mode we simply set the current position to the
     *  desired position.
     */

    if (pDevice->simulation)
    {
        DEBUG(DDR_MSG_FULL, 
              "<%ld> c:%d s:%d setPosition: simulating%c\n",' ');

        semTake (pDevice->mutexSem, WAIT_FOREVER);
        pDevice->position = position;
        pDevice->target = position;
        semGive (pDevice->mutexSem);
    }


    /*
     *  Otherwise send the position to the OMS card
     */

    else
    {
        /*
         *  Confirm that the card and axis were found at init time.
         */

        if (!pMotor->exists)
        {
            SET_ERR_MSG("Motor hardware does not exist");
            return DRV_OMS_VME_S_CFG_ERROR;
        }
           

        /*
         *  Use the Load Position command followed by the new
         *  position value to load.
         */

        sprintf (scratch, "ER%ld,%ld LP%ld ER1,1 ", 
                  pDevice->encoderResolution, 
                  pDevice->motorResolution, 
                  (long)(position * pDevice->encoderResolution / 
                              pDevice->motorResolution)   );

        DEBUG(DDR_MSG_FULL, 
              "<%ld> c:%d s:%d setPosition: string: %s\n",scratch);
        /*
         *  Then write the string to the appropriate axis on the appropriate
         *  OMS card.
         */

        status = drvOmsVmeWriteMotor (pMotor->card,
                                      pMotor->axis,
                                      scratch); 
        if (status)
        {
            drvOmsVmeGetErrorMessage (pDevice->errorMessage);
            DEBUG(DDR_MSG_ERROR, 
                  "<%ld> c:%d s:%d OMSscanTask: %s\n", pDevice->errorMessage);
        }
    }

    return status;
}
