/*
 *
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005.				(c) 2005
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * drvOmsVme58.h
 *
 * PURPOSE:
 * deviceControl OMS-VME58 device driver support public information
 *
 *INDENT-OFF*
 * $Log: drvOmsVme58.h,v $
 * Revision 0.1  2005/07/01 17:47:14  drashkin
 * *** empty log message ***
 *
 * Revision 1.6  2005/05/19 bmw
 * Added revision info.
 *
 * Revision 1.5  2004/09/09  bmw
 * Initial F2 revision, copy of HIA Altair rev 1.4
 * Added card sub-types.
 *
 * Revision 1.4  2001/08/10 21:32:01  angelic
 * added an additional output (done_state) to drvOmsVmeMotorPosition
 *
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef DRV_OMS58_VME_DRV_INC
#define	DRV_OMS58_VME_DRV_INC

/*  Look for the first instance of this card at this address */
#define DRV_OMS_VME_BASE             0x8000

#define DRV_OMS_VME_ADDRS_TYPE       atVMEA16 /* VME addressing type        */
#define DRV_OMS_VME_INTERRUPT_TYPE   intVME   /* Interupt source type       */
#define DRV_OMS_VME_MEM_SPACE        0x1000   /* Register address space     */


#define DRV_OMS_VME_MAX_CARDS        3  /* Maximum number of VME58 cards    */
#define DRV_OMS_VME_MAX_AXES         8  /* Maximum axes per card            */


/* 
 *   OMS VME58 card subtypes 
 *
 *   Supports subtypes: VME58-4, VME58-8, VME58-4S, VME58-8S, VME58-4E,
 *                      VME58-2S2, VME58-2S6, VME58-4S4,& VME58-6S2
 */
#define DDR_OMS_58_INVALID 0 /* Invalid or unsupported controller or axis   */
#define DDR_OMS_58_4       1 /* 4-axis stepper controller w/o encoders      */
#define DDR_OMS_58_8       2 /* 8-axis stepper controller w/o encoders      */
#define DDR_OMS_58_4E      3 /* 4 axis stepper controller w/ encoders       */
#define DDR_OMS_58_4S      4 /* 4-axis servo controller                     */
#define DDR_OMS_58_8S      5 /* 8-axis servo controller                     */
#define DDR_OMS_58_2S2     6 /* 2-axis servo plus 2-axis stepper controller */
#define DDR_OMS_58_2S6     7 /* 2-axis servo plus 6-axis stepper controller */
#define DDR_OMS_58_4S4     8 /* 4-axis servo plus 4-axis stepper controller */
#define DDR_OMS_58_6S2     9 /* 5-axis servo plus 2-axis stepper controller */

/*
 *  OMS axis types
 */
#define DDR_OMS_58_ST_OL   1 /* Open loop stepper (no encoder)              */
#define DDR_OMS_58_ST_CL   2 /* Closed loop stepper (encoder)               */
#define DDR_OMS_58_SV_CL   3 /* Closed loop servo (encoder)                 */


/*
 *  OMS indexing velocity limitations.
 *  See "Home and Initialzation Control Commands" section of VME58 user manual
 */
#define DDR_MAX_INDEX_VEL    1023.0   /* maximum final index velo., st/sec  */
#define DDR_MIN_INDEX_VEL       1.0   /* minimum final index velo., st/sec  */


/** Don't know about these 2 */
#define DRV_OMS_VME_VECTOR                  180
#define DRV_OMS_VME_INTERRUPT               5
 
#define DRV_OMS_VME_MAX_MSG_LEN             128 

#define DRV_OMS_VME_S_CMD_REJECT            -1
#define DRV_OMS_VME_S_NO_REPLY              -2
#define DRV_OMS_VME_S_SYS_ERROR             -3
#define DRV_OMS_VME_S_CFG_ERROR             -4
#define DRV_OMS_VME_S_BUFFER_FULL           -5 
#define DRV_OMS_VME_S_PARSE_ERROR           -6
#define DRV_OMS_VME_S_INDEX_ERROR           -7
#define DRV_OMS_VME_S_PARAM_ERR             -8
#define DRV_OMS_VME_S_UNSUPPORTED           -9
/*
 * Public functions
 */
 
int	drvOmsVmeGetCardType (int);
void	drvOmsVmeGetErrorMessage (char *);
long	drvOmsVmeInit();
long	drvOmsVmeMotorPosition (int, int, long *, long *);
long	drvOmsVmeMotorState (int, int, int *, int *, int *, int *);
long	drvOmsVmeReadCard (int, char * );
long	drvOmsVmeWriteCard (int, char *);  
long	drvOmsVmeWriteMotor (int, int, char *);  

void	drvOmsVmeStatRead( int );

   
#endif
