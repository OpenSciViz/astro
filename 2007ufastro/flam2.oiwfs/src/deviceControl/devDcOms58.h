/*
 *
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005.				(c) 2005
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devDcOms58.h
 *
 * PURPOSE:
 * deviceContol record OMS device support public information
 *
 *INDENT-OFF*
 * $Log: devDcOms58.h,v $
 * Revision 0.1  2005/07/01 17:47:14  drashkin
 * *** empty log message ***
 *
 * Revision 1.4  2005/06/09 bmw
 * Moved DDR_OMS_SCAN_TASK_RATE definition in from *.c
 *
 * Revision 1.3  2005/05/19 bmw
 * Added revision info.
 * 
 * Revision 1.2  2004/09/07 bmw
 * Initial F2 revision copy of HIA Altair rev 1.1.
 *
 * Revision 1.1  2000/04/12 22:11:24  dunn
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef DEVDCOMS58_H_INCLUDED
#define	DEVDCOMS58_H_INCLUDED

typedef struct {
    long            number;
    DEVSUPFUN       devReport;
    DEVSUPFUN       init;
    DEVSUPFUN       initDeviceSupport;
    DEVSUPFUN       getIointInfo;
    DEVSUPFUN       configureDrive;
    DEVSUPFUN       controlPower;
    DEVSUPFUN       controlMotion;
    DEVSUPFUN       setDelay;
    DEVSUPFUN       setPosition;
} DEVICE_CONTROL_DSET;

#endif

#define DDR_OMS_SCAN_TASK_RATE         10         /* omsScanTask rate (scans/sec) */


/* Does binary output (bo record) device support need to be added here? 
   It would appear not as the bo stuff doesn't actually get used */
