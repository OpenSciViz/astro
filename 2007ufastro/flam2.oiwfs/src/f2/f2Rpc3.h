/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005.                        (c) 2005
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * f2Rpc3.h
 *
 * PURPOSE:
 * Include file for the RPC-3 remote power bar control code
 *
 *INDENT-OFF*
 * Revision 1.7  2005/05/19  bmw
 * Added revision info.
 *
 * Revision 1.6  2005/04/28  bmw
 * Initial F2 revision, copy of HIA Altair aoRpc3.h by Saddlemyer.
 * Changed names, IP, etc.
 *
 * Revision 1.5  2002/06/28 17:47:43  saddlmyr
 * Added bit designation definitions for each channel
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef F2_RPC_3_INCLUDED
#define	F2_RPC_3_INCLUDED


/*
 * The RPC3 server name (shouldn't need the IP here).
 */

#define RPC3_HOST_NAME  "f2pb"
#define RPC3_IP "192.168.3.10"

/*
 * channel definition constants
 */

#define F2_RPC3_STATUS          0
#define F2_RPC3_EPICS_IOC       1   /* EPICS IOC VME chassis */
#define F2_RPC3_MOTOR_DRIVER    2   /* Motor and brake driver chassis */
#define F2_RPC3_OUTLET_3        3
#define F2_RPC3_OUTLET_4        4
#define F2_RPC3_OUTLET_5        5
#define F2_RPC3_OUTLET_6        6
#define F2_RPC3_OUTLET_7        7
#define F2_RPC3_OUTLET_8        8


/*
 * Status word bits definitions
 */

#define F2_RPC3_STATUS_EIOC     0x01   /* EPICS IOC VME chassis */
#define F2_RPC3_STATUS_MD       0x02   /* Motor and brake driver chassis */
#define F2_RPC3_STATUS_OUT3     0x04
#define F2_RPC3_STATUS_OUT4     0x08
#define F2_RPC3_STATUS_OUT5     0x10
#define F2_RPC3_STATUS_OUT6     0x20
#define F2_RPC3_STATUS_OUT7     0x40
#define F2_RPC3_STATUS_OUT8     0x80

/*
 * and desired states
 */

#define F2_RPC3_OFF             0
#define F2_RPC3_ON              1


/*
 * Error codes
 */

#define F2_RPC3_INVALID_ARG             -1
#define F2_RPC3_SOCKET_ERR              -2
#define F2_RPC3_HOST_ERR                -3
#define F2_RPC3_SEMAPHORE_CREATE_ERR    -4
#define F2_RPC3_TASK_SPAWN_ERR          -5
#define F2_RPC3_SEMAPHORE_TIMEOUT       -4

/*
 * external definitions
 */

#ifndef UNIX
int f2Rpc3( int channel, int state);
int f2Rpc3Init(void);
#endif /* ifndef UNIX */





#define F2_DEBUG_TEST_FOR_OUTPUT(dbgLev)                         \
    (dbgLev <= F2_LOCAL_DEBUG_LEVEL )

#ifdef DEBUG_FORMAT_CHECK
#define F2_DEBUG_CHECK_FORMAT_STRING(FMT, NUM_PARAM)             \
{                                                                \
    const char * dbg_ptr = FMT;                                  \
    int  dbg_cnt = 0;                                            \
                                                                 \
    while (*dbg_ptr != (char) 0)                                 \
    {                                                            \
        if (*dbg_ptr == '%')                                     \
        {                                                        \
            ++dbg_ptr;        /* advance to next character */    \
            if (*dbg_ptr == '%')  /* %% prints as % */           \
            {                                                    \
                ++dbg_ptr;        /* skip over the second % */   \
            }                                                    \
            else                                                 \
            {                                                    \
                ++dbg_cnt;    /* inc format specifier counter */ \
            }                                                    \
        }                                                        \
        else                                                     \
        {                                                        \
            ++dbg_ptr;        /* advance to next character */    \
        }                                                        \
    }                                                            \
    if (dbg_cnt != (NUM_PARAM+1))                                \
    {                                                            \
        (void) logMsg("<%d> : %s (%s, line %d)\n",               \
              (int) tickGet(),                                   \
              (int) "Possible format string error",              \
              (int) __FILE__, __LINE__, 0, 0);                   \
    }                                                            \
}
#else
#define F2_DEBUG_CHECK_FORMAT_STRING(FMT, NUM_PARAM)  /* do nothing */
#endif
 
#define F2_DEBUG_PRINT_NO_TEST(NUM_PARAM, FMT, V1, V2, V3, V4, V5)           \
{                                                                            \
    if (FMT != (char *) NULL)                                                \
    {                                                                        \
        F2_DEBUG_CHECK_FORMAT_STRING(FMT, NUM_PARAM)                         \
        (void) logMsg(FMT, (int)tickGet(), (int)V1, (int)V2,                 \
                           (int)V3, (int)V4, (int)V5);                       \
    }                                                                        \
    else                                                                     \
    {                                                                        \
        (void) logMsg("<%d> : %s (%s, line %d)\n",                           \
                      (int) tickGet(),                                       \
                      (int) "Invalid (NULL) format string to F2_DEBUG",      \
                      (int) __FILE__, __LINE__, 0, 0);                       \
    }                                                                        \
}


#define F2_DEBUG_PRINT(dbgLev, NUM_PARAM, FMT, V1, V2, V3, V4, V5)           \
{                                                                            \
    if (F2_DEBUG_TEST_FOR_OUTPUT(dbgLev))                                    \
    {                                                                        \
        F2_DEBUG_PRINT_NO_TEST(NUM_PARAM, FMT, V1, V2, V3, V4, V5)           \
    }                                                                        \
}


#define F2_DEBUG5(dbgLevel, FMT, V1, V2, V3, V4, V5) \
   F2_DEBUG_PRINT(dbgLevel, 5, FMT, V1, V2, V3, V4, V5)

#define F2_DEBUG4(dbgLevel, FMT, V1, V2, V3, V4)   \
   F2_DEBUG_PRINT(dbgLevel, 4, FMT, V1, V2, V3, V4, 0)

#define F2_DEBUG3(dbgLevel, FMT, V1, V2, V3)   \
   F2_DEBUG_PRINT(dbgLevel, 3, FMT, V1, V2, V3, 0, 0)

#define F2_DEBUG2(dbgLevel, FMT, V1, V2)   \
   F2_DEBUG_PRINT(dbgLevel, 2, FMT, V1, V2, 0, 0, 0)

#define F2_DEBUG1(dbgLevel, FMT, V1)   \
   F2_DEBUG_PRINT(dbgLevel, 1, FMT, V1, 0, 0, 0, 0)

#define F2_DEBUG(dbgLevel, FMT)   \
   F2_DEBUG_PRINT(dbgLevel, 0, FMT, 0, 0, 0, 0, 0)




/*
 *  F2_DEBUG_ENTER / LEAVE macros
 */

#define F2_DEBUG_ENTER  F2_DEBUG_PRINT(F2_DEBUG_FULL, 1, \
                "<%d> : Entering %s.\n", __FUNCTION__, 0, 0, 0, 0)

#define F2_DEBUG_LEAVE  F2_DEBUG_PRINT(F2_DEBUG_FULL, 1, \
                "<%d> : Leaving %s.\n", __FUNCTION__, 0, 0, 0, 0)


/*
 *  F2_DEBUG_ENTER_MAX / LEAVE_MAX macros  (trigger at F2_DEBUG_MAX)
 */

#define F2_DEBUG_ENTER_MAX  \
          F2_DEBUG1(F2_DEBUG_MAX, "<%d> : Entering %s.\n", __FUNCTION__)

#define F2_DEBUG_LEAVE_MAX  \
          F2_DEBUG1(F2_DEBUG_MAX, "<%d> : Leaving %s.\n", __FUNCTION__)


#endif /* F2_RPC_3_INCLUDED */

