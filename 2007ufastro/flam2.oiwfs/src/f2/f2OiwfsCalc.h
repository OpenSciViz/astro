/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005.                        (c) 2005
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                     
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * f2OiwfsCalc.h
 *
 * PURPOSE:
 *
 * Provides function prototypes for:
 *      f2OiwfsCalculateProbeAngles,
 *      f2OiwfsCalculateProbePosition,  
 *      f2OiwfsCalculateVerticalRange,
 *      f2OiwfsCheckTargets,
 *      f2OiwfsGetOffsets, and
 *      f2OiwfsSetOffsets.
 *
 * Provides access to global which control the behaviour of the
 * f2OiwfsCalculate* functions.
 *
 *INDENT-OFF*
 * $Log: f2OiwfsCalc.h,v $
 * Revision 0.1  2005/07/01 17:47:53  drashkin
 * *** empty log message ***
 *
 * Revision 1.4  2005/06/09  bmw
 * Changed OI_X_HI_LIM to 113.0
 *
 * Revision 1.3  2005/05/19  bmw
 * Added revision info.
 *
 * Revision 1.2  2004/10/28  bmw
 * Initial F2 revision, copy of Gemini GMOS gmOiwfsCalc.h rev 1.1
 * Changes names to F2, changed constants to as-built F2.
 *
 * Revision 1.1  2002/04/24 05:25:35  ajf
 * Added for port to epics3.13.4GEM8.4.
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef  F2_OIWFS_CALC
#define  F2_OIWFS_CALC


/*
 * The following definition of PI is required since some <math.h> header
 * files do not appear to define it.  This definition MUST APPEAR AFTER
 * the inclusion of <math.h>.
 */

#ifndef  M_PI
#define  M_PI  3.1415926535897932384626         /* Definition of PI         */
#endif

#define  F2_OIWFS_MIDPOINT_NONE        0        /* Midpoint calc hit limits */
#define  F2_OIWFS_MIDPOINT_OK          1        /* Midpoint calc successful */
#define  F2_OIWFS_MIDPOINT_EMERGENCY  -1        /* Midpoint set to midrange */


 /*
  *  Constants relating to as-built telescope and Flamingos-2 dimensions.
  */

 /*
  *  OIWFS Dewar radius (in mm) used to define reachable target zone.
  */
#define OI_DEWAR_RADIUS     139.7     /* F2 dewar radius                  */

 /*
  *  OIWFS maximum operating range.
  */
#define OI_X_HI_LIM       113.0               /* Maximum excursion in X (mm) */
#define OI_BAS_HI_LIM    (135.0 * M_PI /180)  /* Maximum base angle (rad)    */
#define OI_BAS_LO_LIM     ( 0.0 * M_PI /180)  /* Minimum base angle (rad)    */
#define OI_PKO_HI_LIM     (35.0 * M_PI /180)  /* Maximum pickoff angle (rad) */
#define OI_PKO_LO_LIM   (-150.0 * M_PI /180)  /* Minimum pickoff angle (rad) */


 /*  
  *  Distance in mm along IC frame Z axis (the optical axis) from
  *  telescope exit pupil to slit plane.
  */
#define  D_P                16400.0

 /*
  *  Offsets (in mm) to base stage origin {B} from Instrument Coordinates 
  *  origin {IC}.
  */
#define  IC_X                 252.00     /* Away from the centre of the dewar */
#define  IC_Y                   0.00     /* There should be no offset in Y    */
#define  IC_Z                 -17.58     /* GMOS number                       */

 /*
  *  X & Y offsets (in mm) to pickoff stage origin {P} from base stage 
  *  origin {B}.  .
  */
#define  B_X                   77.50     /* Length of base stage arm    */
#define  B_Y                    0.00     /* No offset in Y              */
#define  B_Z                   29.62     /* (GMOS) Height of base stage arm    */

 /*
  *  X & Y offsets to probe mirror origin {M} from pickoff stage 
  *  origin {P} (in mm).  Z offset is zero.
  */
#define  P_X                  191.00     /* Length of pickoff stage arm */
#define  P_Y                    0.00     /* No offset in Y              */
#define  P_Z                   64.03     /* (GMOS) Height of pickoff stage arm */

#define BP_X                (B_X + P_X)  /* Probe Mirror patrol radius  */





typedef  double  F2_OIWFS_RAD;                  /* Radians are double words */  
typedef  double  F2_OIWFS_MM;                   /* Millimeters are doubles  */

typedef  struct
{
    double offset;
    double xSlope;
    double ySlope;
    double tanZen2;
    double xyQuad;
    double xxQuad;
    double yyQuad;
    double xCos2zen;
} F2_OIWFS_REG_COEF;

extern int f2OiwfsSolutionId;                   /* select probe orientation */
extern int f2OiwfsStageTiltCorrection;          /* Include tilt in calcs    */
extern int f2OiwfsRefineSteps;                  /* Angle calc iterations    */




int f2OiwfsCalculateProbeAngles   (F2_OIWFS_MM xTarget, 
                                   F2_OIWFS_MM yTarget, 
                                   F2_OIWFS_RAD * baseAngle, 
                                   F2_OIWFS_RAD * pickoffAngle,
                                   F2_OIWFS_RAD * probeAngle);

int f2OiwfsCalculateProbePosition (F2_OIWFS_RAD baseAngle, 
                                   F2_OIWFS_RAD pickoffAngle,
                                   F2_OIWFS_MM  * xPosition,
                                   F2_OIWFS_MM  * yPosition,
                                   F2_OIWFS_MM  * zPosition,
                                   F2_OIWFS_RAD * probeAngle);

int f2OiwfsCalculateVerticalRange (F2_OIWFS_RAD base1, 
                                   F2_OIWFS_RAD pickoff1, 
                                   F2_OIWFS_RAD base2, 
                                   F2_OIWFS_RAD pickoff2,
                                   F2_OIWFS_MM lowerLimit, 
                                   F2_OIWFS_MM upperLimit,
                                   F2_OIWFS_MM * ymin, 
                                   F2_OIWFS_MM * ymax,
                                   F2_OIWFS_RAD * mid_base, 
                                   F2_OIWFS_RAD * mid_pickoff);

int f2OiwfsCheckTargets           (F2_OIWFS_MM xTarget, 
                                   F2_OIWFS_MM yTarget);

int f2OiwfsGetOffsets             (F2_OIWFS_RAD * base, 
                                   F2_OIWFS_RAD * pickoff, 
                                   F2_OIWFS_MM * x, 
                                   F2_OIWFS_MM * y, 
                                   F2_OIWFS_MM * z);

void f2OiwfsSetOffsets            (F2_OIWFS_RAD base, 
                                   F2_OIWFS_RAD pickoff, 
                                   F2_OIWFS_MM x, 
                                   F2_OIWFS_MM y, 
                                   F2_OIWFS_MM z);


#endif /*  F2_OIWFS_CALC  */
