.\"/*+
.\"***********************************************************************
.\"***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
.\"
.\" (c) <2005>				(c) <2005>
.\" National Research Council		Conseil national de recherches
.\" Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
.\" All rights reserved			Tous droits reserves
.\" 					
.\" NRC disclaims any warranties,	Le CNRC denie toute garantie
.\" expressed, implied, or statu-	enoncee, implicite ou legale,
.\" tory, of any kind with respect	de quelque nature que se soit,
.\" to the software, including		concernant le logiciel, y com-
.\" without limitation any war-		pris sans restriction toute
.\" ranty of merchantability or		garantie de valeur marchande
.\" fitness for a particular pur-	ou de pertinence pour un usage
.\" pose.  NRC shall not be liable	particulier.  Le CNRC ne
.\" in any event for any damages,	pourra en aucun cas etre tenu
.\" whether direct or indirect,		responsable de tout dommage,
.\" special or general, consequen-	direct ou indirect, particul-
.\" tial or incidental, arising		ier ou general, accessoire ou
.\" from the use of the software.	fortuit, resultant de l'utili-
.\" 					sation du logiciel.
.\"
.\"***********************************************************************
.\"
.\" FILENAME
.\" f2OiwfsCalibrate.1
.\"
.\" PURPOSE:
.\" Describe f2OiwfsCalibrate.
.\"
.\"
.\"INDENT-OFF*
.\" $Log: f2OiwfsCalibrate.1,v $
.\" Revision 1.1  2005/07/01 17:47:53  drashkin
.\" Initial revision
.\"
.\" Revision 1.2  2005/05/25  bmw
.\" Initial F2 version, copy of gmOiwfsCalibrate.1
.\" Changed names to reflect F2 version
.\"
.\" Revision 1.1  2000/02/24 22:43:39  msmith
.\" Initial revision
.\"
.\"
.\"INDENT-ON*
.\"
.\"***      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
.\"***********************************************************************
.\"-*/
.TH f2OiwfsCalibrate  1 May.\ 25,\ 2005 "DAO"

.SH NAME

.B f2OiwfsCalibrate
\- Computes Flamingos 2 OIWFS calibration coefficients.


.SH SYNOPSIS

.B f2OiwfsCalibrate
[ \fB-f filename\fR ]
[ \fB-h\fR ] [\fB-?\fR]
[ \fB-t\fR ]
.br
    [ \fB-b Initial_Base_Offset\fR ] [ \fB-p Initial_Pickoff_Offset\fR ]
.br
    [ \fB-x\ Initial_X_Offset\fR ] [ \fB-y Initial_Y_Offset\fR ]


.SH DESCRIPTION

.B f2OiwfsCalibrate 
determines the Flamingos 2 OIWFS calibration coefficients by performing a
non-linear least squares minimization search.  The search parameters are
the base offset angle, pickoff offset angle, X offset and Y offset.
The minimization technique used is due to Hooke and Jeeves, with modifications
due to Bell and Pike (see references).  A few additional 
modifications were made to the algorithm specified by Bell and Pike.

.SH OPERATION

The use of \fBf2OiwfsCalibrate\fR requires a file of calibration
measurements.  Each line of the calibration file corresponds to one
calibration point with values listed in the following order:

X position, Y position, Base angle and Pickoff angle.

A "#" character at the start of a line can be used to make that line 
a comment.  A sample data file is:

# X     Y     Base       Pickoff
.br
0       0    -24.9928    32.9604
.br
0       0     44.4156   -60.3228
.br
25.4    0    -44.4700    59.3136
.br
25.4    0     66.6586   -86.6800
.br
50.8    0    -60.0984    79.4932
.br
50.8    0     85.5954  -106.846
.br
76.2    0    -75.2158    97.6784
.br
76.2    0    104.875   -125.038 
.br

In order to collect this calibration data, a calibration mask can
be used.  This mask should have a hole punched at the optical axis.
When the calibration mask is loaded, and the telescope
is pointed at the "on axis star", the probe can be positioned to
several other known star positions (at which there should be mask holes).
For each known star position (calibration point), the operator will
record the known (X,Y) position on the mask, and the \fBraw\fR base 
and pickoff angles from the deviceControl Record Status engineering screens.
(This screen can be reached via the deviceControl engineering screen.)
It is important that the raw angles are used since the normally displayed 
base and pickoff angles have already had the calibration offsets applied 
to them.

The user must specify the starting value of each of the calibration
offsets.  These can be specified as command line options.  If an offset
is not given a starting value on the command line, the user will be
prompted for a value.

The allowable range for the angle offsets is -360 ... 360 degrees.

The allowable range for X and Y offsets is -20 ... 20 mm.

The search path can be observed by setting the trace (\fB-t\fR) flag.

When the final calibration offsets are reported, the angle offsets are 
always mapped to the -180 ... 180 degree range.

.SH CONFIGURING OIWFS

Once \fBf2OiwfsCalibrate\fR has determined the optimal offsets to use,
these must be recorded in the file "src/CP/pv/f2Opc.pv".
This is accomplished by replacing the values in the following four 
lines with the new offsets.

double $(top)probeBasOffset = 176.326; # Base offset 
.br
double $(top)probePkoOffset = -14.110; # Pickoff offset 
.br
double $(top)probeXOffset   =   0.236; # X offset
.br
double $(top)probeYOffset   =   0.217; # Y offset
.br

Note that these four lines may not appear exactly as above.

.SH OPTIONS

.TP
.B -b # 
.br
Sets the starting search value of the base angle offset to #.
.TP
.B -f filename 
.br
This is the name of the file containing the calibration data points.
.TP
.B -h
.br
Print a brief description of usage.
.TP
.B -help
.br
Same as \fB-h\fR.
.TP
.B -p # 
.br
Sets the starting search value of the pickoff angle offset to #.
.TP
.B -t
.br
Sets the trace flag to enable display of intermediate search positions.
.TP
.B -x # 
.br
Sets the starting search value of the X offset to #.
.TP
.B -y # 
.br
Sets the starting search value of the Y offset to #.
.TP
.B -?
.br
Same as \fB-h\fR.

.SH EXAMPLES

Invoking \fBf2OiwfsCalibrate\fR with no command line parameters results
in the program prompting the user for all necessary data.

$ \fBf2OiwfsCalibrate\fR
.br

.br
User is prompted for a calibration measurement data file and a value
for each parameter, then the minimization search is performed.


$ \fBf2OiwfsCalibrate -b 175 -p -13 -x 0 -y 0\fR
.br

.br
Set the initial base offset to 175 degrees, the initial pickoff offset to
-13 degrees, and the initial X and Y offsets to 0 mm, then perform the
minimization search.

.SH DIAGNOSTICS

\fBf2OiwfsCalibrate\fR returns the following error codes:

.TP
.B \ 0 : Success
Program successfully completed the optimization process.
.TP
.B -1 : Invalid calibration file.
Specified calibration file did not exist or did not contain any valid data. 
.TP
.B -2 : User specified "-help" option.

.TP
.B -3 : Unknown parameter.
An unknown command line parameter was specified.
.TP
.B -4 : Not enough data.
The specified calibration file did not provide a sufficient number of 
data points.
.TP
.B -5 : Invalid angle.
The specified base or pickoff angle was invalid.  Angles are specified in
degrees, and must be within the range -360 ... 360.
.TP
.B -6 : Failure to converge
The maximum number of function evaluations were performed, and convergence
was not achieved.  The calculated calibration offsets may not be correct.

.SH REFERENCES

Hooke, Robert and Jeeves, T.A.
.br
\fI"Direct Search" Solution of Numerical and Statistical Problems\fR,
.br
.B Journal of the ACM, 
Vol. \fB8\fR, No. \fB2\fR, 1961.   pp. 212-229

Kaupe, Arthur F., Jr.
.br
\fIAlgorithm 178 : Direct Search\fR,
.br
.B Communications of the ACM, 
Vol. \fB6\fR, No. 6, 1963.  pp. 313-314

Bell, M. and Pike, M.C.
.br
\fIRemark on Algorithm 178 : Direct Search\fR,
.br
.B Communications of the ACM, 
Vol \fB9\fR, No. 9, 1966.  pp. 684-685
 

.SH BUGS
\fBChanging the starting point of the search doesn't seem to change the
solution\fR.

This is \fInot\fR a bug, since the program should always find the
"best" offsets.  However, the ability to select the starting position
of the search is valuable as a check that the solution is not a local
minima.  \fBf2OiwfsCalibrate\fR cannot guarantee that it converges to 
the "true" solution, but in practice converging on local minima does not
seem to be a problem, and any starting point for the search should
result in the same solution.


.SH AUTHORS
Malcolm Smith
.br

Dominion Astrophysical Observatory
.br
Herzberg Institute of Astrophysics
.br
National Research Council
