/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005  				        (c) 2005
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		    Ottawa, Canada, K1A 0R6
 * All rights reserved			    Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					                sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * f2OiwfsCalc.c
 *
 * PURPOSE:
 * Performs transformations between the X,Y coordinate frame of the
 * Flamingos instrument and the stage angles of the OIWFS probe.
 *
 * FUNCTION NAME(S)  [ Global scope ]
 *
 * f2OiwfsCheckTargets           - Determine if X,Y targets are valid.
 *
 * f2OiwfsSetOffsets             - Initializes the calibration coefficients.
 *
 * f2OiwfsGetOffsets             - Get copies of the calibration coefficients.
 *
 * f2OiwfsCalculateProbeAngles   - Calculates the stage angles required to
 *                                 position the probe at the desired X,Y
 *                                 location in the instrument coordinate frame.
 *
 * f2OiwfsCalculateProbePosition - Calculates the X,Y position of the probe
 *                                 in the instrument coordinate frame based on
 *                                 the stage angles.
 *
 *
 * FUNCTION NAME(S)  [ Local scope ]
 *
 * CalcApproxProbeAngles         - Calculates the approximate stage angles
 *                                 given the probe's desired X,Y position 
 *                                 in the instrument. The approximation ignores
 *                                 the effects of stage tilt.
 *
 * RefineProbeAngles             - Attempts to improve the accuracy of the
 *                                 stage angles required to position the
 *                                 probe at a specified X,Y position.
 *
 * CalcApproxProbePosition       - Calculates the approximate position
 *                                 of the probe from the stage angles.
 *                                 This approximation ignores the effects
 *                                 of stage tilt therefore focus is always
 *                                 zero.
 *
 * CalcExactProbePosition        - Calculates the position from the stage
 *                                 angles (taking into account the stage
 *                                 tilt).
 *
 * MatVecMult4                   - Multiplies vector V by Matrix M and
 *                                 overwrites the original vector V with
 *                                 the result, M * V.
 *
 * InitPickoffToProbeTrans       - Constructs the transformation matrix
 *                                 which transforms coordinates in the
 *                                 pickoff arm frame of reference into 
 *                                 coordinates in the probe reference frame.
 *
 * InitBaseToPickoffTrans        - Constructs the transformation matrix
 *                                 which transforms coordinates in the
 *                                 base arm frame of reference into coordinates
 *                                 in the pickoff arm reference frame.
 *
 * InitTelescopeToBaseTrans      - Constructs the transformation matrix
 *                                 which transforms coordinates in the
 *                                 instrument frame of reference into 
 *                                 coordinates in the base arm reference frame.
 *
 * PrintMatrix                   - Prints a 4x4 matrix of doubles.  Used
 *                                 for debugging purposes only.
 *
 *
 *  There are four coordinate systems used in the calculations....
 *
 *  1)  The Instrument Coordinates frame {IC}.   This is a fixed frame  
 *      along the central axis of the telescope focal plane 
 *           X axis - points at the base stage bearing (ignoring any 
 *                    displacement in the Z axis.
 *           Y axis - 
 *           Z axis - points into the Flamingos instrument
 *                    (i.e. -Z points to the centre of the exit pupil).
 *
 *  2)  The base frame {B} is a fixed frame centred on the axis of the base 
 *      stage bearing.  From the origin of {IC} it is:
 *           - offset along X by IC_X,
 *           - offset along Z by IC_Z (this may be 0),
 *           - rotated about Z by GAMMA_ICB (180 degrees), then
 *           - rotated about Y by BETA_ICB so that -Z continues to point 
 *             to the centre of the exit pupil.
 *           X axis - points back at the origin of {IC}
 *           Z axis - defines the rotational axis of the base stage bearing
 * 
 *  3)  The pickoff frame is a moving frame centred on the axis of the pickoff 
 *      stage bearing.  It's origin is moving relative to {IC} but fixed 
 *      relative to the origin of {B}.  From the origin of {B} it is:
 *           - rotated about Z by GAMMA_BP (the current base stage angle) 
 *           - offset in X by the length of the base stage arm 
 *           - offset in Z by the height of the base stage arm
 *           - rotated about Y by BETA_BP (so that -Z continues to point 
 *             to the centre of the exit pupil).
 *           Z axis - defines the rotational axis of the pickoff stage bearing
 *
 *  4)  The probe mirror frame {M} is a moving frame centred on the optical 
 *      axis of probe optic.  Again the origin is moving relative to {IC} but 
 *      fixed relative to the origin of {P}.  From the origin of {P} it is:
 *           - rotated about Z by GAMMA_PM (the current pickoff angle)
 *           - rotated about Y by BETA_PM (so that -Z points to the centre
 *             of the exit pupil).
 *           - offset in X by the length of the pickoff stage arm
 *           - offset in Z by the height of the pickoff stage arm.
 *
 *
 *INDENT-OFF*
 * $Log: f2OiwfsCalc.c,v $
 * Revision 0.1  2005/07/01 17:47:53  drashkin
 * *** empty log message ***
 *
 * Revision 1.5  2005/06/09  bmw
 * Add debugging to f2OiwfsCheckTargets
 *
 * Revision 1.4  2005/05/19  bmw
 * Added comments & revision info.
 *
 * Revision 1.3  2004/10/01  bmw
 * Initial revision is copy of Malcolm's Gemini GMOS gmOiwfsCalc.c rev 1.2
 * Changed names to f2, remove vertical range check & best orientation, 
 * revised coordinate system description, remove ADC coefficients, add
 * f2OiwfsCheckTargets() function, revised transformations.
 *
 * Revision 1.2  2003/01/31 14:12:46  gemvx
 * Merged gmos-south
 *
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include <stdio.h>
#include <math.h>

#ifdef SOLARIS
#define logMsg(fmt,a,b,c,d,e,f)  printf(fmt, a, b, c, d, e, f)
#else
#include <logLib.h>
#endif

#include "f2OiwfsCalc.h"




#define  SQR(X)  ((X) * (X))            /* Hard definition for square funct */

#define  EMERGENCY_X  60.0              /* (GMOS) Fixed midpoint X coordinate    */
#define  EMERGENCY_Y  60.0              /* (GMOS) Filed midpoint Y coordinate   */



/*
 *  Coordinate system transformations
 *  ICB -> Instrument Coordinates to Base stage
 *  BP -> Base stage to Pickoff stage
 *  PM -> Pickoff stage to probe Mirror
 */


 /*  
  *  X/Y vector offset from origin of Instrument Coordinates {IC} 
  *  to origin of base {B} (assumes IC_Y is 0 and ignores Z offset). 
  */
#define  IC_XY   IC_X


 /*
  *  A small CW tweak of rotation in radians of base {B} Y axis so 
  *  that {B} -Z continues to point to the centre of the exit pupil.  
  *  Rotate {B} Y axis BETA_ICB radians.
  */ 
#define  BETA_ICB  -atan(IC_XY / D_P)


 /*
  *  Angle rotation in radians of {IC} Z axis such that {B} X points at 
  *  the origin of {IC} when {B} Z axis (the base stage angle) is zero.   
  *  Rotate {IC} Z axis GAMMA_ICB degrees.  Assuming IC_Y is 0, GAMMA_ICB
  *  should be 180 degrees.
  */
#define  GAMMA_ICB    M_PI


 /*
  *  A small CCW tweak of rotation in radians for pickoff {P} Y axis 
  *  so that {P} -Z continues to point to the centre of the exit pupil.
  *  Rotate {P} Y axis BETA_BP radians.
  */
#define  BETA_BP  atan(B_X / D_P)


 /*
  *  A small CCW tweak of rotation in radians for probe mirror {M} Y axis 
  *  so that {M} -Z continues to point to the centre of the exit pupil.  
  *  Rotate {M} Y axis BETA_PM radians. 
  */
#define  BETA_PM  atan(P_X / D_P)



/*
 *  These public global variables affect the calculations used to determine
 *  probe target angles.   
 *      - f2OiwfsRefineSteps specifies how many iterations of the 
 *        refinement algorithm are used to clean up the rough position
 *        generated by the inverse kinematics calculation.
 *      - f2OiwfsStageTiltCorrection enables or disables the inclusion 
 *        of stage tilt in the calculations.
 *      - f2OiwfsSolutionId specifies which of the two possible solutions
 *        generated by the position to angle algorithm to use.
 */

int f2OiwfsRefineSteps = 2;           /* Set to 2, but 1 is okay too   */
int f2OiwfsStageTiltCorrection = 1;   /* 0 to ignore stage tilt        */
int f2OiwfsSolutionId = 1;            /* Use first solution            */

int f2OiwfsCalcDebug = 0;             /* Debug flag                    */
/*
 *  Local storage for the calibration offsets
 */

static F2_OIWFS_RAD baseOffset;
static F2_OIWFS_RAD pickoffOffset;
static F2_OIWFS_MM  xOffset;
static F2_OIWFS_MM  yOffset;
static F2_OIWFS_MM  zOffset;


static F2_OIWFS_REG_COEF zRegNoADC = { -1.3, 0.0, 0.0, 0.0, 0.0,
                                       1.0 / (2 * 1910.0),    /* x^2 */
                                       1.0 / (2 * 1910.0),    /* y^2 */
                                       0.0 };

/*
 *  Transformation algorithm array definitions
 */

typedef double VECTOR[4];       /* generic four element vector array        */
typedef double MATRIX[4][4];    /* generic four by four calculation matrix  */


/*
 *  Function Prototypes (local).  Global prototypes are in f2OiwfsCalc.h.
 */


static int CalcApproxProbeAngles(F2_OIWFS_MM xTarget, F2_OIWFS_MM yTarget, 
                                 F2_OIWFS_RAD * baseAngle, 
                                 F2_OIWFS_RAD * pickoffAngle);
  
static int RefineProbeAngles(F2_OIWFS_MM xTarget, F2_OIWFS_MM yTarget,
                             F2_OIWFS_RAD * baseAngle, 
                             F2_OIWFS_RAD * pickoffAngle);

static int CalcApproxProbePosition(F2_OIWFS_RAD baseAngle, 
                                   F2_OIWFS_RAD pickoffAngle,
                                   F2_OIWFS_MM * xPos,  F2_OIWFS_MM * yPos,
                                   F2_OIWFS_MM * focus);

static int CalcExactProbePosition(F2_OIWFS_RAD baseAngle, 
                                  F2_OIWFS_RAD pickoffAngle,
                                  F2_OIWFS_MM * xPos,  F2_OIWFS_MM * yPos,  
                                  F2_OIWFS_MM * focus);

static void MatVecMult4(MATRIX T, VECTOR X);

static void InitPickoffToProbeTrans(F2_OIWFS_RAD pickoffAngle, MATRIX T);

static void InitBaseToPickoffTrans(F2_OIWFS_RAD baseAngle, MATRIX T);

static void InitTelescopeToBaseTrans(MATRIX T);

static double regEval(const F2_OIWFS_REG_COEF * coef,
                      double x, double y, double zenith);

/*
 * Print matrix debugging info if matrix printing is enabled
 */

#ifdef DEBUG_MATRIX
#define PRINT_MATRIX(S,M)  PrintMatrix(S,M)
static void PrintMatrix(char * s, MATRIX A);
#else
#define PRINT_MATRIX(S,M)  /* define PRINT_MATRIX as a NOP */
#endif


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * f2OiwfsSetOffsets
 *
 * INVOCATION:
 * f2OiwfsSetOffsets(base_offset, pickoff_offset, x_offset, y_offset);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) base (double)    Offset required to convert base angle from
 *                       encoder value to instrument reference frame (rad).
 *  (>) pickoff (double) Offset required to convert pickoff angle from
 *                       encoder value to base reference frame (rad).
 *  (>) x (double)       Offset required to convert X coordinate to
 *                       instrument reference frame (mm).
 *  (>) y (double)       Offset required to convert Y coordinate to
 *                       instrument reference frame (mm).
 *  (>) z (double)       Offset required to convert Z coordinate to
 *                       instrument reference frame (mm).
 *
 * FUNCTION VALUE:
 * (void)   Not applicable.
 *
 * PURPOSE:
 * Initialize the calibration coefficients for the probe positioning
 *
 * DESCRIPTION:
 * The calibration coefficients are simply copied from the parameters
 * into their appropriate variable.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * The calibration coefficients must have been previously calculated.
 *
 * SEE ALSO:
 *  - f2OiwfsGetOffsets
 *  - f2OiwfsCalcProbePosition
 *  - f2OiwfsCalcProbeAngles
 *  - f2OiwfsCalcVerticalRange
 *  - f2OiwfsCalcVerticalRange
 *  - CalcApproxProbeAngles
 *  - CalcApproxProbePosition
 *  - CalcExactProbePosition
 * capfast/f2OiwfsCalcPos.sch
 * CP/pv/f2Opc.pv
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

void f2OiwfsSetOffsets
(
    F2_OIWFS_RAD  base,      /* (in) baseOffset coefficient     */
    F2_OIWFS_RAD  pickoff,   /* (in) pickoffOffset coefficient  */
    F2_OIWFS_MM   x,         /* (in) xOffset coefficient        */
    F2_OIWFS_MM   y,         /* (in) yOffset coefficient        */
    F2_OIWFS_MM   z          /* (in) zOffset coefficient        */
)
{
    /*
     * Update the local offset information
     */

    baseOffset = base;
    pickoffOffset = pickoff;
    xOffset = x;
    yOffset = y;
    zOffset = z;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * f2OiwfsCheckTargets
 *
 * INVOCATION:
 * f2OiwfsCheckTargets(x_target, y_target);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) x_target (double)   X target in Instrument Coordinates frame 
 *                          of reference.
 *  (>) y_target (double)   Y target in Instrument Coordinates frame 
 *                          of reference.
 *
 * FUNCTION VALUE:
 *  (int) success code:
 *	 0 - Targets are valid.
 *	-1 - Given targets outside valid probe patrol area.
 *
 * PURPOSE:
 * Determine if X,Y targets are valid.
 *
 * DESCRIPTION:
 * Using constants derived from as-built telescope and Flamingos dimensions,
 * evaluate X,Y targets and reject all invalid targets.  Requires access to
 * these dimensions:
 *
 *     Dewar window radius
 *     Base Arm Length
 *     Pickoff Arm Length
 *     Positive X Limit
 *
 * Reject the targets if: 
 *     X is unreachable
 *     X,Y target is outside dewar window
 *     X,Y target is outside arc formed by sweeping base stage with 
 *       pickoff stage held at 0 degrees (when Y is positive)
 *     X,Y target is outside arc formed by sweeping pickoff stage with 
 *       base stage held at 0 degrees (when Y is negative)
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * None.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

int f2OiwfsCheckTargets
(
    F2_OIWFS_MM xTarget, 
    F2_OIWFS_MM yTarget
)
{
    int  status;              /* function status return */
    double bAngle;            /* Calculated angle of base stage   */
    double pAngle;            /* Calculated angle of pickoff stage*/
    double probeAngle;        /* Angle of probe head wrt detector */

    if (  xTarget > OI_X_HI_LIM ||
          (OI_DEWAR_RADIUS < sqrt(SQR(xTarget) + SQR(yTarget))) ||
          ((yTarget >= 0) && 
                (BP_X < sqrt( SQR(xTarget - IC_X) + SQR(yTarget)))) ||
          ((yTarget < 0) && 
                (P_X < sqrt( SQR(xTarget - IC_X + B_X) + SQR(yTarget)))) )
    {
        if (f2OiwfsCalcDebug)
        {
            if (xTarget > OI_X_HI_LIM)
            {
                printf("f2OiwfsCheckTargets: X is greater than %f\n", OI_X_HI_LIM);
            }
            if (OI_DEWAR_RADIUS < sqrt(SQR(xTarget) + SQR(yTarget)))
            {
                printf("f2OiwfsCheckTargets: Position is outside dewar window\n");
            }
            if ((yTarget >= 0) && 
                         (BP_X < sqrt( SQR(xTarget - IC_X) + SQR(yTarget))))
            {
                printf("f2OiwfsCheckTargets: Position out of reach due to base/pickoff arm length\n");
            }
            if ((yTarget < 0) && 
                         (P_X < sqrt( SQR(xTarget - IC_X + B_X) + SQR(yTarget))))
            {
                printf("f2OiwfsCheckTargets: Position out of reach due to pickoff arm length\n");
            }
        }
        status = -1;
    }
    else if ((status = f2OiwfsCalculateProbeAngles (xTarget,yTarget,
                                  &bAngle, &pAngle, &probeAngle)) != 0)
    {
        if (f2OiwfsCalcDebug)
        {
            printf("f2OiwfsCheckTargets: Call to f2OiwfsCalculateProbeAngles failed\n");
        }
        status = -1;
    }
    else status = 0;

    return status;

}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * f2OiwfsGetOffsets
 *
 * INVOCATION:
 * f2OiwfsGetOffsets(&base_offset, &pickoff_offset, &x_offset, &y_offset);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) base (double *)    Offset required to convert base angle from
 *                         encoder value to instrument reference frame (rad).
 *  (>) pickoff (double *) Offset required to convert pickoff angle from
 *                         encoder value to base reference frame (rad).
 *  (>) x (double *)       Offset required to convert X coordinate to
 *                         instrument reference frame (mm).
 *  (>) y (double *)       Offset required to convert Y coordinate to
 *                         instrument reference frame (mm).
 *  (>) z (double *)       Offset required to convert Z coordinate to
 *                         instrument reference frame (mm).
 *
 * FUNCTION VALUE:
 * (int)   Returns 0 if calibration offsets are properly copied into the
 *         locations specified by the parameters.  Non-zero return values
 *         indicate one or more NULL pointers as parameters.
 *
 *         1  -  NULL base    pointer.
 *         2  -  NULL pickoff pointer.
 *         4  -  NULL x       pointer.
 *         8  -  NULL y       pointer.
 *
 *         All of the four NULL pointer tests are performed, and the resulting
 *         error bits are added together, so each value 1..15 corresponds to a
 *         different set of one or more NULL pointers.
 *
 * PURPOSE:
 * Return the current calibration coefficients
 *
 * DESCRIPTION:
 * The calibration coefficients are simply copied to the appropriate parameters.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * For this to return meaningful values, the calibration coefficients must have
 * been set previously.
 *
 * SEE ALSO:
 *  - f2OiwfsSetOffsets
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


int f2OiwfsGetOffsets
(
    F2_OIWFS_RAD * base,      /* (out) Pointer to baseOffset coefficient     */
    F2_OIWFS_RAD * pickoff,   /* (out) Pointer to pickoffOffset coefficient  */
    F2_OIWFS_MM  * x,         /* (out) Pointer to xOffset coefficient        */
    F2_OIWFS_MM  * y,         /* (out) Pointer to yOffset coefficient        */
    F2_OIWFS_MM  * z          /* (out) Pointer to yOffset coefficient        */
)
{
    int error_bits = 0;

    /*
     *  If base offset return pointer is valid then return the current
     *  base offset value.
     */

    if (base == NULL)
    {
        error_bits = 1;
    }
    else
    {
        *base = baseOffset;
    }


    /*
     *  If pickoff offset return pointer is valid then return the current
     *  base offset value.
     */

    if (pickoff == NULL)
    {
        error_bits = error_bits + 2;
    }
    else
    {
        *pickoff = pickoffOffset;
    }


    /*
     *  If X offset return pointer is valid then return the current
     *  base offset value.
     */

    if (x == NULL)
    {
        error_bits = error_bits + 4;
    }
    else
    {
        *x = xOffset;
    }

    /*
     *  If Y offset return pointer is valid then return the current
     *  base offset value.
     */

    if (y == NULL)
    {
        error_bits = error_bits + 8;
    }
    else
    {
        *y = yOffset;
    }


    /*
     *  If Z offset return pointer is valid then return the current
     *  base offset value.
     */

    if (z == NULL)
    {
        error_bits = error_bits + 16;
    }
    else
    {
        *z = zOffset;
    }


    return error_bits;
}



/*
 ***********************************************************************    
 *
 *+
 * FUNCTION NAME:
 * f2OiwfsCalculateProbeAngles
 *
 * INVOCATION:
 *  status = f2OiwfsCalculateProbeAngles(xTarget, yTarget,
 *                                       &baseAngle &pickoffAngle, &probeAngle);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) xTarget (double) Desired probe X target in physical space (mm)
 *  (>) yTarget (double) Desired probe Y target in physical space (mm)
 *  (<) baseAngle (double) Calculated base stage angle (rad)
 *  (<) pickoffAngle (double) Calculated pickoff stage angle (rad)
 *  (<) probeAngle (double) Calculated probe tip angle (rad)
 *
 * FUNCTION VALUE:
 *  (int) success code:
 *	 0 - Angles calculated successfully
 *	-1 - Given target outside probe patrol area.
 *
 * PURPOSE:
 *  Convert X-Y stage targets to probe mechanism angles
 *
 * DESCRIPTION:
 *  The X,Y coordinate is first shifted by xOffset, yOffset.  Approximate
 *  base and pickoff angles are then calculated using inverse kinematics.
 *  The angles are then "improved" by performing f2OiwfsRefineSteps
 *  applications of RefineProbeAngles.  The final step ensures that the
 *  calculated angles are within the range -180...180 degrees (-PI .. PI rad).
 *
 * EXTERNAL VARIABLES:
 *  f2OiwfsRefineSteps
 *
 * PRIOR REQUIREMENTS:
 *  None.
 *
 * SEE ALSO:
 *  - CalcApproxProbeAngles
 *  - RefineProbeAngles
 *
 * DEFICIENCIES:
 *  None.
 *
 ***********************************************************************
 *
 */

int f2OiwfsCalculateProbeAngles (F2_OIWFS_MM xTarget, 
                                 F2_OIWFS_MM yTarget, 
                                 F2_OIWFS_RAD * baseAngle,
                                 F2_OIWFS_RAD * pickoffAngle,
                                 F2_OIWFS_RAD * probeAngle)
{
    int  status;              /* function status return */

  
    /* 
     *  Add calibration offsets to targets
     */

    xTarget = xTarget - xOffset;
    yTarget = -(yTarget - yOffset);


    /* 
     *  Do a inverse kinematics calculation to get an approximate value for the
     *  required base and pickoff angles
     */

    if ((status = CalcApproxProbeAngles(xTarget, yTarget,
                                   baseAngle, pickoffAngle)) != 0 )
    {
        return status;
    }

    /*
     *  If a more precise position is required then refine the base and
     *  pickoff angles.
     */
  
    if (f2OiwfsRefineSteps > 0)
    {
        if ((status = RefineProbeAngles(xTarget, yTarget, 
                                   baseAngle, pickoffAngle)) != 0 )
        {
            return status;
        }
    }

    *probeAngle = fmod(3.0 * M_PI + (*baseAngle + *pickoffAngle) , 2.0 * M_PI) - M_PI;

    /*
     *  Negate the angles (actual stage angles are opposite of the right hand rule)
     *  and add offsets.
     */

    *baseAngle    = -(baseOffset + *baseAngle);
    *pickoffAngle = -(pickoffOffset + *pickoffAngle);

    /*
     * Force both of the angles to be within the range -180 ... 180 degrees.
     * The angle is first shifted by 180 degree to make the range 0 ... 360,
     * then by an extra 360 degrees to ensure that there are no negative
     * angles due to rounding errors.
     *
     * ??? Not sure this is still necessary???
     */

    *baseAngle = fmod(3.0 * M_PI + *baseAngle, 2.0 * M_PI) - M_PI;
    *pickoffAngle = fmod(3.0 * M_PI + *pickoffAngle, 2.0 * M_PI) - M_PI;

    if ( (OI_BAS_LO_LIM > *baseAngle) || (*baseAngle > OI_BAS_HI_LIM) ||
         (OI_PKO_LO_LIM > *pickoffAngle) || (*pickoffAngle > OI_PKO_HI_LIM)   )
    {
        status = -1;
    }

    return status;
}


/*
 ***********************************************************************    
 *
 *+
 * FUNCTION NAME:
 * f2OiwfsCalculateProbePosition
 *
 * INVOCATION:
 * status = f2OiwfsCalculateProbePosition (baseAngle, 
 *                                         pickoffAngle, &x, &y, &z, &angle);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) baseAngle (double)      Current base stage angle (rad)
 *  (>) pickoffAngle (double)   Current pickoff stage angle (rad)
 *  (<) xPosition (double)      Calculated probe X position (mm)
 *  (<) yPosition (double)      Calculated probe Y position (mm)
 *  (<) zPosition (double)      Calculated probe Z position (mm)
 *  (<) probeAngle (double)     Calculated probe angle (rad)
 *
 * FUNCTION VALUE:
 *  (int) success code:
 *     0 - Angles calculated successfully
 *    -1 - Given angles can not be converted.
 *
 * PURPOSE:
 *  Convert current base and pickoff stage angles to probe positon.
 *
 * DESCRIPTION:
 *  This is a wrapper function which calls either CalcApproxProbePosition
 *  (if no correction for stage tilt is to be done) or the more accurate
 *  CalcExactProbePosition (if f2OiwfsStageTiltCorrection != 0).
 *  
 *  After computing the X and Y position of the probe, and the focus
 *  term, the X and Y positions are corrected using the X and Y offset values.
 *
 * EXTERNAL VARIABLES:
 *  f2OiwfsStageTiltCorrection
 *
 * PRIOR REQUIREMENTS:
 *  None.
 *
 * SEE ALSO:
 *  - CalcApproxProbePosition
 *  - CalcExactProbePosition
 *
 * DEFICIENCIES:
 *  None.
 *
 ***********************************************************************
 *
 */


int f2OiwfsCalculateProbePosition
(
    F2_OIWFS_RAD    baseAngle,        /* current base stage angle         */ 
    F2_OIWFS_RAD    pickoffAngle,     /* current pickoff stage angle      */
    F2_OIWFS_MM   * xPos,             /* calculated probe x coordinate    */ 
    F2_OIWFS_MM   * yPos,             /* calculated probe y coordinate    */
    F2_OIWFS_MM   * focus,            /* calculated probe z coordinate    */
    F2_OIWFS_RAD  * probeAngle        /* calculated probe angle           */
)
{
    int     status;                   /* function status return           */

    /*
     *  First, remove encoder offsets
     */
 
    baseAngle = baseAngle - baseOffset;
    pickoffAngle = pickoffAngle - pickoffOffset;


    /*
     *  Compute *xPos, *yPos and *focus using either the approximate
     *  (but faster) method or the more exact (but slower) method.
     */

    if (f2OiwfsStageTiltCorrection == 0)
    {
        status =  CalcApproxProbePosition(baseAngle, 
                                          pickoffAngle, 
                                          xPos, 
                                          yPos, 
                                          focus);
    }
    else
    {
        status =  CalcExactProbePosition(baseAngle, 
                                         pickoffAngle, 
                                         xPos, 
                                         yPos, 
                                         focus);
    }

    /*
     *  If the calculation of xPos and yPos was successful, then
     *  remove the calibration offsets.
     */

    if (status == 0)
    {

        *xPos = *xPos + xOffset;
        *yPos = *yPos + yOffset;

        *focus = *focus - zOffset;


        /*
         *  Compute probe angle in instrument reference frame
         */

        *probeAngle = fmod(3.0 * M_PI - (baseAngle + pickoffAngle) , 
                 2.0 * M_PI) - M_PI;

        /*
         *  Subtract the focal surface from the probe position.
         *  Temporarily??? use 0.0 as zenith angle.
         */

        *focus = *focus - regEval(&zRegNoADC, *xPos, *yPos, 0.0);

    }

    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * CalcApproxProbeAngles
 *
 * INVOCATION:
 * status = CalcApproxProbeAngles (x, y, &base, &pickoff, &probeA);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) xTarget (double) Desired probe X target in physical space (mm)
 *  (>) yTarget (double) Desired probe Y target in physical space (mm)
 *  (<) baseAngle (double) Calculated base stage angle (rad)
 *  (<) pickoffAngle (double) Calculated pickoff stage angle (rad)
 *
 * FUNCTION VALUE:
 * (int) success code
 *     0 - Angles calculated successfully
 *    -1 - Given target outside probe patrol area.
 *
 * PURPOSE:
 * Calculate base and pickoff stage angles required to place the probe
 * in a given X-Y position
 *
 * DESCRIPTION:
 * Use inverse kinematics to determine the two possible solutions to the
 * problem of where to move the base and pickoff stages to place the probe
 * in a given X-Y position.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static int CalcApproxProbeAngles 
(
    F2_OIWFS_MM    xTarget,        /* Desired probe X target                */
    F2_OIWFS_MM    yTarget,        /* Desired probe Y target                */
    F2_OIWFS_RAD * baseAngle,      /* Calculated base stage angle           */
    F2_OIWFS_RAD * pickoffAngle)   /* Calculated pickoff stage angle        */
{
    double   x_prime;     /* X position of target relative to base          */
    double   y_prime;     /* Y position of target relative to base          */
    double   r;           /* Vector from target to base rotation axis       */
    double   alpha;       /* Angle of vector "r" above                      */
    double   phi;         /* angle opposite the side "r" above              */
    double   theta;       /* angle opposite the pickoff arm                 */
    double   cval;        /* cosine of angle opposite vector "r"            */
    double   sval;        /* sine of angle opposite the pickoff arm         */

    /*
     * Convert from instrument coordinates to base coordinates
     * which use the axis of rotation of the base arm as the origin.
     * First subtract the {IC} to {B} origin offsets then negate since
     * both X & Y axis go in opposite directions compared to {IC}. 
     */

    x_prime = -(xTarget - IC_X);
    y_prime = -(yTarget - IC_Y);

    /*
     *  Compute distance from the target to the axis of base arm.
     */

    r = sqrt(SQR(x_prime) + SQR(y_prime));

    /*  If the target is too far from (or too close to) the axis of
     *  the base arm, then the target cannot be reached by the
     *  probe.
     */

    if (r > B_X + P_X)
    {
        return -1; /* cannot reach with both axes fully extended */
    }
    else if (r < P_X - B_X)
    {
        return -1; /* cannot reach with pickoff folded back completely */
    }


    /*
     *  Compute the angle from the base origin to the target
     *  position. (note that X and Y are in a reverse order from where 
     *  you'd expect to find them using atan2 because the X & Y are
     *  swapped).
     */

    alpha = atan2 ( y_prime, x_prime );

    /*
     *  For a triangle with sides A, B, and C, and opposing angles
     *  a, b and c, we have the following relationship:
     *
     *  cos a = (C*C + B*B - A*A) / (2 * B * C)
     *
     *  Using the base arm length as side B, pickoff arm length as side C
     *  and r as side A, we can compute the angle phi as angle a.
     */

    cval = ( SQR(B_X) + SQR(P_X) - r*r ) / (2.0 * B_X * P_X);

    phi = acos(cval);

    /*  And again for angle theta as angle c across from the pickoff arm */

    sval = ( r*r + SQR(B_X) - SQR(P_X)) / (2.0 * r * B_X);

    theta = acos(sval);

    /*
     *  Compute the stage angles and probe orientation angle based on the angle
     *  (the orientation of the "triangle") and the angles within the triangle.
     *
     *  Then return the appropriate angles.
     */

    if (-(alpha - theta) < OI_BAS_HI_LIM)
    {
        *baseAngle = alpha - theta;
        *pickoffAngle = M_PI - phi;
    }
    else
    {
        *baseAngle = alpha + theta;
        *pickoffAngle = -(M_PI - phi);
    }

    return 0;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * RefineProbeAngles
 *
 * INVOCATION:
 * status = RefineProbeAngles (xTarget,
                               yTarget, baseAngle, pickoffAngle);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 *  (>) xTarget (double) Desired probe X target in physical space (mm)
 *  (>) yTarget (double) Desired probe Y target in physical space (mm)
 *  (<) baseAngle (double) Calculated base stage angle (rad)
 *  (<) pickoffAngle (double) Calculated pickoff stage angle (rad)
 *
 * FUNCTION VALUE:
 * (int)  0 if successful.  At moment, always returns 0.
 *
 * PURPOSE:
 * Calculate a more precise (but takes longer) pair of base and pickoff
 * angles required to place the probe in a given X-Y position
 *
 * DESCRIPTION:
 * Iterate a fixed number of times to reduce the error between the
 * calculated probe position based on the base and pickoff stage
 * angles and the actual probe position desired.   The algorithm that
 * does the probe angle to position transformation is more precise than
 * the position to angle algorithm.....
 *
 * EXTERNAL VARIABLES:
 * f2OiwfsRefineSteps
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * Should do some checking to make sure refinement succeeded.
 *-
 ************************************************************************
 */


static int RefineProbeAngles
(
  F2_OIWFS_MM    xTarget,      /* X coordinate of target pos.               */
  F2_OIWFS_MM    yTarget,      /* Y coordinate of target pos.               */
  F2_OIWFS_RAD * baseAngle,    /* Original (in) improved (out) base angle   */
  F2_OIWFS_RAD * pickoffAngle /* Original (in) improved (out) pickoff angle*/
)
{
    F2_OIWFS_MM      xProbe;    /* Calculated probe X position              */
    F2_OIWFS_MM      yProbe;    /* Calculated probe Y position              */
    F2_OIWFS_MM      focus;     /* Calculated probe Z position              */
    double      xNew;           /* Refined X target position                */
    double      yNew;           /* Refined Y target position                */
    double      xError;         /* X error between calc and desired position*/
    double      yError;         /* Y error between calc and desired position*/
    int         i;              /* iteration counter                        */


    /*
     *  Make a copy of the target position.  This will change as the
     *  refinements are made, but the original xTarget and yTarget must
     *  be preserved so that it can be used to measure the error (difference
     *  between calculated probe position and target position).
     */

    xNew = xTarget;
    yNew = yTarget;
  

    /*
     *  Perform the required number of refinement steps.
     */

    for (i=0; i < f2OiwfsRefineSteps; ++i)
    {

        /*
         *  Compute the exact location of the probe if it was moved to
         *  the angles specified by *baseAngle and *pickoffAngle.
         *  This location is stored in (xProbe, yProbe).
         */

        CalcExactProbePosition(*baseAngle, 
                               *pickoffAngle, 
                               &xProbe, 
                               &yProbe,
                               &focus);
        
        /*
         *  Compute the positioning error (the difference between the target 
         *  position and the calculated probe position).
         */

        xError = xProbe - xTarget;
        yError = yProbe - yTarget;


        /*
         *  Adjust the target position so that the probe positioning error
         *  is approximately cancelled (by this change to the target position).
         */

        xNew = xNew - xError;
        yNew = yNew + yError;


        /*
         *  Get new probe angles for the adjusted target position.  These angles
         *  should correspond to an improved probe position.
         */

        CalcApproxProbeAngles(xNew, 
                              yNew, 
                              baseAngle, 
                              pickoffAngle);
    }

    return 0;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * CalcApproxProbePosition
 *
 * INVOCATION:
 * status = CalcApproxProbePosition (base, pickoff, xPos, yPos, focus);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) baseAngle    (F2_OIWFS_RAD) Current base stage angle (rad)
 * (>) pickoffAngle (F2_OIWFS_RAD) Current pickoff stage angle (rad)
 * (<) xPos         (F2_OIWFS_MM)  Calculated probe X position (mm)
 * (<) yPos         (F2_OIWFS_MM)  Calculated probe Y position (mm)
 * (<) focus        (F2_OIWFS_MM)  Calculated probe Z position (mm)
 *
 * FUNCTION VALUE:
 * (int) 0 if successful (always successful at moment).
 *
 * PURPOSE:
 * Calculate the approximate (fast) probe position based on the 
 * base and pickoff stage angles
 *
 * DESCRIPTION:
 * Perform a simple trig calculation to see where the probe should
 * be in the base frame of reference given the current base and
 * pickoff stage angles.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static int CalcApproxProbePosition
(
    F2_OIWFS_RAD     baseAngle,      /* Current base stage angle            */
    F2_OIWFS_RAD     pickoffAngle,   /* Current pickoff stage angle         */
    F2_OIWFS_MM    * xPos,           /* Calculated probe X position         */
    F2_OIWFS_MM    * yPos,           /* Calculated probe Y position         */
    F2_OIWFS_MM    * focus           /* Calculated probe Z position         */
)
{
    /*
     * Calculate the equivalent probe X-Y position.
     */

    *xPos = IC_X - 
            B_X * cos(baseAngle) - 
            P_X * cos(baseAngle+pickoffAngle);

    *yPos = IC_Y + 
            B_X * sin(baseAngle) +
            P_X * sin(baseAngle+pickoffAngle);

    *focus = zOffset;

    return 0;  /* always a success */
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * CalcExactProbePosition
 *
 * INVOCATION:
 * status = CalcExactProbePosition (baseAngle, pickoffAngle, xPos, yPos);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) baseAngle (double) Current base stage angle (rad)
 * (>) pickoffAngle (double) Current pickoff stage angle (rad)
 * (<) xPosition (double) Calculated probe position (mm)
 * (<) yPosition (double) Calculated probe position (mm)
 *
 * FUNCTION VALUE:
 * (int) calculation success code.
 *
 * PURPOSE:
 * Convert base and pickoff stage angles to probe position in the
 * probe frame of reference
 *
 * DESCRIPTION:
 * Calculate the exact probe positon by converting the current base and
 * pickoff stage angles to a probe position in the telescope fame of
 * reference then transforming that back to the probe frame.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static int CalcExactProbePosition
(
    F2_OIWFS_RAD    baseAngle,      /* Current base stage angle             */
    F2_OIWFS_RAD    pickoffAngle,   /* Current pickoff stage angle          */
    F2_OIWFS_MM   * xPos,           /* Calculated probe X position          */
    F2_OIWFS_MM   * yPos,           /* Calculated probe Y position          */
    F2_OIWFS_MM   * focus           /* Calculated probe Z position          */
)
{
    MATRIX  A;                      /* working matrix array                 */
    VECTOR  X;                      /* working vector array                 */


    /*
     * Remove encoder offsets
     */

/*    baseAngle = baseAngle - GAMMA_ICB; */
/*    pickoffAngle = pickoffAngle; */


    /* Initialize the vector X to the position of the probe in its own 
     * reference frame
     */

    X[0] = 0.0;
    X[1] = 0.0;
    X[2] = 0.0;
    X[3] = 1.0;


    /* 
     *  Create vector A using the pickoff stage to probe transformation.
     */

    InitPickoffToProbeTrans(pickoffAngle, A);

    PRINT_MATRIX("Pickoff -> Probe", A);

    /*
     *  Update vector X to pickoff frame by multiplying by vector A
     */ 
    MatVecMult4(A, X);


    /*
     *  And again using the base to pickoff stage transformation.
     */

    if (baseAngle < 0.0)
    {
        baseAngle = baseAngle + M_PI;
    } 
    else
    {
        baseAngle = baseAngle - M_PI;
    }

    /* 
     *  Create vector A using the base to pickoff stage transformation.
     */

    InitBaseToPickoffTrans(baseAngle, A);

    PRINT_MATRIX("Base -> Pickoff", A);

    /*
     *  Update vector X to base frame by multiplying by vector A
     */ 

    MatVecMult4(A, X); 


    /*
     *  This leaves the vector representation of the actual probe position
     *  in the probe frame of reference.  Since the telescope frame is
     *  simply a rotated probe frame use the telescope to base transformation
     *  to represent the probe position in the base fame of reference.
     */

    InitTelescopeToBaseTrans(A);

    PRINT_MATRIX("Telescope -> Base", A);


    MatVecMult4(A, X);

    /* 
     * Finally, de-rotate the base coordinate system to compensate for
     * using the telescope instead of probe transformation.
     */

    *xPos  = -X[0];
    *yPos  = X[1];
    *focus = X[2];

    return 0;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * InitPickoffToProbeTrans
 *
 * INVOCATION:
 * InitPickoffToProbeTrans (pickoff, matrix);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pickoff (F2_OIWFS_RAD)   current pickoff stage angle
 * (<) matrix (MATRIX)          resulting transformation matrix
 *
 * FUNCTION VALUE:
 * None.
 *
 * PURPOSE:
 * Constructs the transformation matrix which transforms coordinates in the
 * pickoff arm frame of reference into coordinates in probe reference frame.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void InitPickoffToProbeTrans
(
    F2_OIWFS_RAD   pickoffAngle,   /* Current pickoff stage angle          */
    MATRIX         T               /* Resulting transformation matrix      */
)
{
    double    Cp;      /* Cosine of pickoff angle                         */
    double    Sp;      /* Sine of pickoff angle                           */
    double    Cbpm;    /* Cosine of pickoff to probe vector               */
    double    Sbpm;    /* Sine of pickoff to probe vector                 */

    /*
     *  Initialize the trig values.
     */

    Cp   = cos(pickoffAngle);
    Sp   = sin(pickoffAngle);
    Cbpm = cos(BETA_PM);
    Sbpm = sin(BETA_PM);


    /*
     *  Now initialize the matrix.
     */

    T[0][0] =  Cp * Cbpm;
    T[0][1] = -Sp;
    T[0][2] =  Cp * Sbpm;
    T[0][3] =  Cp * Cbpm * P_X - Sp * P_Y + Cp * Sbpm * P_Z;

    T[1][0] =  Sp * Cbpm;
    T[1][1] =  Cp;
    T[1][2] =  Sp * Sbpm;
    T[1][3] =  Sp * Cbpm * P_X + Cp * P_Y + Sp * Sbpm *P_Z;

    T[2][0] =  -Sbpm;
    T[2][1] =   0;
    T[2][2] =   Cbpm;
    T[2][3] =  -Sbpm * P_X + Cbpm * P_Z;

    T[3][0] =  0;
    T[3][1] =  0;
    T[3][2] =  0;
    T[3][3] =  1;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * InitBaseToPickoffTrans
 *
 * INVOCATION:
 * InitBaseToPickoffTrans (base, matrix);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) base (F2_OIWFS_RAD)  current base stage angle
 * (!) matrix (MATRIX)      resulting transform
 *
 * FUNCTION VALUE:
 * None
 *
 * PURPOSE:
 * Constructs the transformation matrix which transforms coordinates in the
 * base arm frame of reference into coordinates in pickoff arm reference frame.
 *
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void InitBaseToPickoffTrans
(
    F2_OIWFS_RAD  baseAngle,      /* current base stage angle             */
    MATRIX        T               /* resulting transformation matrix      */
)
{
    double    Cb;      /* Cosine of base angle                            */
    double    Sb;      /* Sine of base angle                              */
    double    Cbbp;    /* Cosine of base to pickoff vector                */
    double    Sbbp;    /* Sine of base to pickoff vector                  */

    /*
     *  Initialize the trig values.
     */

    Cb   = cos(baseAngle);
    Sb   = sin(baseAngle);
    Cbbp = cos(BETA_BP);
    Sbbp = sin(BETA_BP);



    /*
     *  Now initialize the matrix.
     */

    T[0][0] =  Cb * Cbbp;
    T[0][1] = -Sb;
    T[0][2] =  Cb * Sbbp;
    T[0][3] =  Cb * B_X - Sb * B_Y;

    T[1][0] =  Sb * Cbbp;
    T[1][1] =  Cb;
    T[1][2] =  Sb * Sbbp;
    T[1][3] =  Sb * B_X + Cb * B_Y;

    T[2][0] =  -Sbbp;
    T[2][1] =   0;
    T[2][2] =   Cbbp;
    T[2][3] =   B_Z;

    T[3][0] =  0;
    T[3][1] =  0;
    T[3][2] =  0;
    T[3][3] =  1;
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * InitTelescopeToBaseTrans
 *
 * INVOCATION:
 * InitTelescopeToBaseTrans (matrix)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (<) matrix (MATRIX)    telescope to base transformation matrix
 *
 * FUNCTION VALUE:
 * None
 *
 * PURPOSE:
 * Constructs the transformation matrix which transforms coordinates in the
 * instrument frame of reference into coordinates in base arm reference frame.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void InitTelescopeToBaseTrans
(
    MATRIX      T               /* resulting matrix                     */
)
{
    double    Cg;      /* Cosine of GAMMA_ICB                               */
    double    Sg;      /* Sine of GAMMA_ICB                                 */
    double    Cb;      /* Cosine of BETA_ICB                              */
    double    Sb;      /* Sine of BETA_ICB                                */

    /*
     *  Initialize the trig values.
     */

    Cg = cos(GAMMA_ICB);
    Sg = sin(GAMMA_ICB);
    Cb = cos(BETA_ICB);
    Sb = sin(BETA_ICB);


    /*
     *  Now initialize the matrix.
     */

    T[0][0] =  Cg * Cb;
    T[0][1] =  Sg;
    T[0][2] =  Cg * Sb;
    T[0][3] =  -IC_X;

    T[1][0] =  Sg * Cb;
    T[1][1] =  Cg;
    T[1][2] =  Sg * Sb;
    T[1][3] =  IC_Y;

    T[2][0] =  -Sb;
    T[2][1] =   0;
    T[2][2] =   Cb;
    T[2][3] =   IC_Z;

    T[3][0] =  0;
    T[3][1] =  0;
    T[3][2] =  0;
    T[3][3] =  1;
}



/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * PrintMatrix
 *
 * INVOCATION:
 * PrintMatrix("Pickoff -> Probe", A);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) s (char *)  String to be printed along with matrix.
 * (>) A (MATRIX)  Matrix to be printed.
 *
 * FUNCTION VALUE:
 * (void) No return value.
 *
 * PURPOSE:
 * Prints the name and values of a 4x4 matrix
 *
 * DESCRIPTION:
 * Prints the matrix name (or some other programmer specified string)
 * and the values in the matrix.  This function is only compiled if the
 * DEBUG-MATRIX keyword is defined and should only be invoked
 * using the PRINT_MATRIX macro.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


#ifdef DEBUG_MATRIX
static void PrintMatrix
(
    char  * s,              /* String to be printed along with matrix       */
    MATRIX A                /* Matrix to be printed                         */
)
{
    int r;                  /* row counter                                  */
    int c;                  /* column counter                               */

    /*
     * Print the prefix string
     */

    printf("\n\n%s\n", s);


    /*
     * Followed by the contents of the matrix
     */

    for (r=0; r < 4; ++r)
    {
        for (c=0; c < 4; ++c)
        {
            printf("%12.6f", A[r][c]);
        }
        printf("\n");
    }
}
#endif


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * MatVecMult4
 *
 * INVOCATION:
 * MatVectMult4 (matrix, vector)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) matrix (MATRIX)      Multiplication matrix
 * (!) vector (VECTOR)      Input/output vector
 *
 * FUNCTION VALUE:
 * none
 *
 * PURPOSE:
 * Vector matrix multiplication function
 *
 * DESCRIPTION:
 * Multiply the given vector by the given matrix.  Vector is overwritten
 * by the result.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void MatVecMult4
(
    MATRIX  T,              /* Multiplication matrix                    */ 
    VECTOR  X               /* Input/output vector                      */
)
{
    double  sum;            /* summing location                         */
    VECTOR  b;              /* resulting vector                         */
    int     r;              /* Row counter                              */
    int     c;              /* Column counter                           */


    /*
     * Perform the vector multiplication
     */

    for (r=0; r < 4; ++r)
    {
        sum = 0.0;

        for (c=0; c < 4; ++c)
        {
            sum = sum + T[r][c] * X[c];
        }

        b[r] = sum;
    }


    /*
     *  Now copy result vector back into input vector.
     */

    for (r=0; r < 4; ++r)
    {
        X[r] = b[r];
    }
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * regEval
 *
 * INVOCATION:
 * xCorrected = regEval(xRegADC, x, y, zenith);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) coef   (F2_OIWFS_REG_COEF *)  Pointer to regression coefficients.
 * (>) x      (double)               X position of probe in instrument coordinates
 * (>) y      (double)               Y position of probe in instrument coordinates
 * (>) zenith (double)               ADC setting (telescope zenith angle)
 *
 * FUNCTION VALUE:
 * Returns the value of the function listed below (represented by the 
 * coefficients stored in *coef) at location x, y, zenith.
 * 
 *
 * PURPOSE:
 * Evaluates the following function:
 *
 *   coef->offset + x * coef->xSlope + y * coef->ySlope
 *   + coef->tanZen2 * tan(zenith) * tan(zenith)
 *   + coef->xyQuad * x * y + coef->xxQuad * x * x + coef->yyQuad * y * y
 *   + coef->xCos2zen * x * cos(2.0 * zenith)
 *
 * DESCRIPTION:
 * Evaluates a regression function at the given x, y and zenith values.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * The pointer coef must point to a valid set of regression coefficients.
 * The regression coefficients must have been previously computed.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static double regEval
(
    const F2_OIWFS_REG_COEF * coef,
    double x,
    double y,
    double zenith   /* zenith angle in radians */
)
{
    double sum;
    double tanZen;

    tanZen = tan(zenith);

    sum = coef->offset
        + coef->xSlope * x 
        + coef->ySlope * y
        + coef->tanZen2 * tanZen * tanZen
        + coef->xyQuad * x * y
        + coef->xxQuad * x * x
        + coef->yyQuad * y * y
        + coef->xCos2zen * x * cos(2.0 * zenith);

    return sum;
}
