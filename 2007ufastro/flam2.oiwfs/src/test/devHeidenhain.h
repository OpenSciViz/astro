/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2000.                        (c) 2000
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devHeidenhain.h
 *
 * PURPOSE:
 * Includes for the Heidenhain encoder 'reading' module
 *
 *INDENT-OFF*
 * $Log: devHeidenhain.h,v $
 * Revision 0.1  2005/07/01 17:48:40  drashkin
 * *** empty log message ***
 *
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef HEIDEN_H_INCLUDED
#define	HEIDEN_H_INCLUDED

/*
 * error codes
 */

#define	HEIDEN_INVLD_PORT	-1
#define	HEIDEN_PORT_OPEN_ERR -2
#define	HEIDEN_NOT_INIT_ERR  -3
#define	HEIDEN_SET_PORT_ERR  -4
#define	HEIDEN_SET_BAUD_ERR  -5
#define	HEIDEN_SET_OPTS_ERR  -6
#define	HEIDEN_FLUSH_ERR     -7
#define	HEIDEN_WRITE_ERR     -8
#define	HEIDEN_GET_BYTE_ERR  -9
#define	HEIDEN_READ_ERR      -10
#define	HEIDEN_TIME_ERR      -11


/*
 * function prototypes
 */

extern int heidenInit(int portNum);
extern int heidenRd(int sampleCnt, int sampleRate);
extern float heidenRdOne(int axis);
extern int heidenRdAll(double *bcTime, double *xVal, double *yVal, double *zVal);

#endif /* HEIDEN_H_INCLUDED */
