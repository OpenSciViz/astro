/* 
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <year>				(c) <year>
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ************************************************************************
 */

/*+
 * MODULE: devBc635
 *
 * FILENAME: devBc635.c
 *
 * PURPOSE:
 * Implementation of the Altair specific high-speed secondary interface
 * to the Bancomm bc635VME board
 *
 *INDENT-OFF*
 * $Log: devBc635.c,v $
 * Revision 0.1  2005/07/01 17:48:40  drashkin
 * *** empty log message ***
 *
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
-*/

/*@+quiet@*/
#include "vxWorks.h"			/* standard VxWorks include */
#include "vme.h"				/* VxWorks misc. additional */
#include "vxLib.h"				/* VxWorks misc. additional */
#include "sysLib.h"				/* memcpy() */
#include "string.h"				/* memcpy() */
#include "time.h"				/* time(), mktime etc. */
/*@-quiet@*/
#include "devBc635.h"	   /* Bancomm bc635 interface defn's and prototypes */


/*
 * now the secondary time latch and time register addresses offsets
 */

#define	BC635_LATCH_OFFSET      0x20
#define	BC635_TIME_OFFSET       0x16


/* 
 *
 * Misc constants
 */

#define	GPS_2_TAI				19.0		/* conversion, constant forever */
#define	SEC_PER_DAY				86400
#define SEC_PER_HOUR			3600
#define SEC_PER_MINUTE			60
#define	SW_TIMER_INCREMENT		0.001		/* increment for SW timer */
#define OFFSET_TEST_VALUE       123456.0    /* test value, exercising module */
#define	OFFSET_DELAY			1.0			/* 1 second to sample in test */


/*
 *  Function prototypes
 */

#ifdef SVGM5    /* a Synergy SVGM5 specific */
int sysBusToLocalAdrsDirect(int adrsSpace, char *busAdrs, char **pLocalAdrs);
#endif

/*
 *  Local Global Data Structures
 */

static double   localTimeOffset = 0;    /* local 'start of year' if local */
static double   *localTimeOffsetP = NULL;	/* where the time offset value is */
static int      localTimeSourceFlag = BC635_SOURCE_SW;	/* HW or SW timer */
static double   localSWTime = 0.0;     /* SW timer */
static void     *latchAddr;            /* local address of the latch */
static void     *timeAddr;             /* local address of the time string */



/*+
 ************************************************************************
 *+
 * EXPORTED FUNCTION: bcc635Init()
 *
 * RETURNS: int, mode of operation (HW or SW)
 *      BC635_SOURCE_SW   if the system is counting time in software 
 *      BC635_SOURCE_HW   if the system is counting time using hardware
 *      BC635_INVALID_SRC if the time flag wasn't valid
 *
 * DESCRIPTION: Initialises the BC635 time card interface.
 * Initialise internal variables (time source and offset pointer). If hardware
 * source, check for memory being accessable, if accessable, default 
 * to software timer.
 *
 ************************************************************************
-*/

int bc635Init
(
	int 		timeSourceFlag, 	/* (in) timer source, HW or SW */
	double 		*timeOffsetP		/* (in) pointer to offset for timer */
)
{
char    test_buf[2];		/* buffer for vxMemProbe return */
char    *baseAddr;          /* address of BC635 in local space */
struct  tm *gTime;          /* structure to calculate time Jan1st, 00:00:00 */
time_t  localTime;

    /*
     * check for reasonable first paramter
     */

    if ((timeSourceFlag != BC635_SOURCE_HW) &&
                (timeSourceFlag != BC635_SOURCE_SW))
    {
        return(BC635_INVALID_SRC);
    }


/* 
 * Appropriate VME space base addresses for MV2700 or SVGM5.
 * Special case for SVGM5 to use the faster direct VME interface.
 * Then determine the latch and time string addresses.
 */

#ifdef SVGM5
    if (sysBusToLocalAdrsDirect(VME_AM_USR_SHORT_IO,
                          BC635_BASE_ADDR,
                          &baseAddr) != OK)
#else
    if (sysBusToLocalAdrs(VME_AM_USR_SHORT_IO,
                          BC635_BASE_ADDR,
                          &baseAddr) != OK)
#endif
    {
        return(BC635_MEM_MAP_ERROR);
    }

    latchAddr = baseAddr + BC635_LATCH_OFFSET;
    timeAddr  = baseAddr + BC635_TIME_OFFSET;


    /* 
     * save timeOffsetP. If NULL, use local RTC to determine year and
     * set pointer to locally set time Jan 1st, 00:00:00
     * After this point no error will be returned
     */

    if (timeOffsetP != NULL)
    {
        localTimeOffsetP = timeOffsetP;
    }
    else
    {
        /*
         * Use time() to get current seconds, convert to struct tm using,
         * gmtime(), zero out all but  year then convert using mktime()
         */

        localTime = time(NULL);
        gTime = gmtime(&localTime);
        gTime->tm_sec  = 0;
        gTime->tm_min  = 0;
        gTime->tm_hour = 0;
        gTime->tm_mday = 0;
        gTime->tm_mon  = 0;
        localTimeOffset = (double)mktime(gTime);
        localTimeOffsetP = &localTimeOffset;
    }


    /*
     * if HardWare source requested, set flag and check VME bus 
     * to see if hardware is present. If so, return
     */

    if (timeSourceFlag == BC635_SOURCE_HW)
    {
        if (vxMemProbe(latchAddr,
                        VX_READ,
                        2,
                        test_buf) == OK)
        {
            localTimeSourceFlag = BC635_SOURCE_HW;
            return(BC635_SOURCE_HW);
        }
    }


    /* 
     * made it here. Either HW absent, or software source requested
     */

	localTimeSourceFlag = BC635_SOURCE_SW;
	return(BC635_SOURCE_SW);
} /* bc635Init() */





/*+
 ************************************************************************
 *
 * EXPORTED FUNCTION:  bc635RawTime()
 *
 * RETURNS: double, the time in seconds, raw, in TAI
 *         Returns the seconds, in TAI, from the time source and what
 *         value is in what localTimeOffsetP points to (if non-NULL)
 *
 * DESCRIPTION: Returns the high precision time from BC635 time card
 * Capture time, either from hardware board or software counter, convert
 * to TAI from GPS time, then optionally add in offset in what
 * localTimeOffsetP points to.
 *
 * NOTES:
 *      - bc635Init MUST be called first. Undefined behaviour if not.
 *
 ************************************************************************
-*/

double bc635RawTime(void)
{
static unsigned char	registerBuf[12];           /* copy register on BC635 */
static char	latchDummyBuf[2] = {(char)0, (char)0};   /* latch time by read */
double	seconds;                                   /* read/calculated time */


    /*
     * Is source HW timer?
     */

    if (localTimeSourceFlag == BC635_SOURCE_HW)
    {
        /*
         * write to latch location to latch, then read data
         */

        memcpy(latchAddr, latchDummyBuf, 2);
        memcpy(registerBuf, (const void *)timeAddr, 10);


        /* 
         * convert to seconds, inline to speed up.
         * Each successive nibble is BCD from most significant to least.
         * Goes from days * 100 to seconds * 0.0000001
         */

        seconds  = (double)(registerBuf[1] & 0x0f) * SEC_PER_DAY * 100.0;
        seconds += (double)(registerBuf[2] >> 4)   * SEC_PER_DAY * 10.0;
        seconds += (double)(registerBuf[2] & 0x0f) * SEC_PER_DAY;
        
        seconds += (double)(registerBuf[3] >> 4)   * SEC_PER_HOUR * 10.0;
        seconds += (double)(registerBuf[3] & 0x0f) * SEC_PER_HOUR;

        seconds += (double)(registerBuf[4] >> 4)   * SEC_PER_MINUTE * 10.0;
        seconds += (double)(registerBuf[4] & 0x0f) * SEC_PER_MINUTE;

        seconds += (double)(registerBuf[5] >> 4)   * 10.0;
        seconds += (double)(registerBuf[5] & 0x0f);

        seconds += (double)(registerBuf[6] >> 4)   * 0.1;
        seconds += (double)(registerBuf[6] & 0x0f) * 0.01;
        seconds += (double)(registerBuf[7] >> 4)   * 0.001;
        seconds += (double)(registerBuf[7] & 0x0f) * 0.0001;
        seconds += (double)(registerBuf[8] >> 4)   * 0.00001;
        seconds += (double)(registerBuf[8] & 0x0f) * 0.000001;
        seconds += (double)(registerBuf[9] >> 4)   * 0.0000001;
    } /* if localSourceFlag == BC635_SOURCE_HW */


    /*
     * localTimeSourceFlag != BC635_SOURCE_HW
     */

    else 
    {
        /*
         * increment, & use local SW 'timer'
         */

        localSWTime += SW_TIMER_INCREMENT;
        seconds = localSWTime;
     }


    /*
     * got seconds, convert to TAI from GPS
     */

    seconds += GPS_2_TAI;


    /*
     * add in offset if non-NULL pointer and return 
     */

    if (localTimeOffsetP)
    {
        seconds += *localTimeOffsetP;
    }

    return(seconds);

} /* bc635RawTime() */



/*+
 ************************************************************************
 *
 * EXPORTED FUNCTION: bc635Test()
 *
 * RETURNS: int, status of test
 *       Either:
 *      -3  BC635_INIT_ERROR     if an initialisation error detected 
 *      -2  BC635_OFFSET_ERROR   if an error was detected using the
 *                                      offset value
 *      -1  BC635_INVALID_SRC    if an invalid timeSourceFlag parameter
 *                                      was issued.
 *       0  OK                          No errors detected
 *     > 0                              Number of times a pair of successive
 *                                      samples was non-increasing.
 *
 * DESCRIPTION: Exercise and test the BC635 board.
 * This routine tests the BC635 board ONLY so far as to ensure that
 * it responds to time queries and returns increasing time values.
 * This routine behaves differently depending on the first paramter
 * If BC635_SOURCE_SW
 *      initialise without offset
 *      sample numIteration times, keep track of any non-increasing samples
 *      re-initialise with a pointer to an offset
 *      sample once. Check that offset correctly added in
 * else if BC635_SOURCE_HW
 *      initialise hardware timer and no offset
 *      sample numIterations times, keep track of any non-increasing samples
 *      re-initialise with a pointer to an offset
 *      sample once. Check that offset correctly added in
 *
 * NOTES:
 *
 ************************************************************************
-*/

int bc635Test
(
    int     timeSourceFlag,     /* (in) HW or SW timer */
    int     numIteration,       /* (in) number of iterations to perform */
    double  *averagePeriod      /* (out) the average period between samples */
)
{
double  periodAccum = 0.0;      /* timer accumulator */
double  lastTime, thisTime;     /* time values */
int     numBadSamples = 0;
int     actualSamples;
double  offsetSeconds = OFFSET_TEST_VALUE;
int		index;


    /*
     * check timeSourceFlag parameter. After this we're
     * assured that timeSourceFlag is valid and can be used
     * for initialisations
     */

    if ((timeSourceFlag != BC635_SOURCE_HW) &&
        (timeSourceFlag != BC635_SOURCE_SW))
    {
        return(BC635_INVALID_SRC);
    }


    /*
     * initialise and then sample numIteration times
     */

    actualSamples = numIteration;
    if (bc635Init(timeSourceFlag, NULL) != timeSourceFlag)
    {
        return(BC635_INIT_ERROR);
    }

    lastTime = bc635RawTime();
    for (index = 0; index < numIteration; index++)
    {
        /*
         * sample time, check for decreasing value, accumulate time
         * difference only if not decreasing. Take care of decreasing
         * total sample counts, then save lastTime
         */

        thisTime = bc635RawTime();
        if (thisTime <= lastTime)
        {
            numBadSamples += 1;
            actualSamples -= 1;
        }
        else
        {
            periodAccum += (thisTime - lastTime);
        }
        lastTime = thisTime;
    } /* for */


    /*
     * re-initialise with an offset, sample time and make sure 
     * difference is slightly larger than OFFSET_TEST_VALUE
     * Assume here that this sample is within OFFSET_DELAY seconds
     * of previous sample.
     */
    
    if (bc635Init(timeSourceFlag, &offsetSeconds) != timeSourceFlag)
    {
        return(BC635_INIT_ERROR);
    }


    /*
     * calculate the average period, fill in parameter, then return
     * error count
     */
    *averagePeriod = periodAccum / (double)actualSamples;
    return(numBadSamples);
} /* bc635Test() */
