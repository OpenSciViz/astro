[schematic2]
uniq 160
[tools]
[detail]
w 1922 1707 100 0 IncrPko eaos.EncPko.OUT 1776 1696 2128 1696 outhier.IncrPko.p
w 1922 1963 100 0 IncrBas eaos.EncBas.OUT 1776 1952 2128 1952 outhier.IncrBas.p
[cell use]
use eaos 1776 1808 100 0 EncPko
xform 0 1648 1728
p 1488 1454 100 0 0 EGU:motor units
p 1520 1632 100 0 1 OMSL:closed_loop
p 1760 1808 100 512 1 PV:$(top)$(dev)
p 1520 1600 100 0 1 SCAN:.1 second
p 1440 1760 100 512 1 def(DOL):$(top)$(dev)PkoDevice.MPOS
p 1776 1664 100 0 0 def(OUT):0.0
use eaos 1776 2064 100 0 EncBas
xform 0 1648 1984
p 1488 1710 100 0 0 EGU:motor units
p 1520 1888 100 0 1 OMSL:closed_loop
p 1568 2064 100 0 1 PV:$(top)$(dev)
p 1520 1856 100 0 1 SCAN:.1 second
p 1440 2016 100 512 1 def(DOL):$(top)$(dev)BasDevice.MPOS
p 1776 1904 100 0 0 def(OUT):0.0
use outhier 2144 1696 100 0 IncrPko
xform 0 2112 1696
use outhier 2144 1952 100 0 IncrBas
xform 0 2112 1952
[comments]
