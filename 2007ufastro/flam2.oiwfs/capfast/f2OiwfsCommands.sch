[schematic2]
uniq 400
[tools]
[detail]
w 1360 2112 100 0 FOLLOW elongouts.followState.VAL 896 2144 2208 2144 2208 2272 2304 2272 f2OiwfsCmd.f2OiwfsCmd#135.FollowState
w 1037 2172 100 1 FOLLOW f2OiwfsCmd.f2OiwfsCmd#133.FollowState 1120 2272 1024 2272 1024 2144 junction
w 1645 2172 100 1 FOLLOW f2OiwfsCmd.f2OiwfsCmd#134.FollowState 1728 2272 1632 2272 1632 2144 junction
w 450 2155 100 0 n#374 f2OiwfsFollowCad.f2OiwfsFollowCad#369.FollowState 448 2144 512 2144 junction
w 450 1099 100 0 n#374 f2OiwfsMoveCad.f2OiwfsMoveCad#370.MoveFollowState 448 1088 512 1088 512 2144 junction
w 450 1771 100 0 n#374 f2OiwfsStopCad.f2OiwfsStopCad#366.StopFollowState 448 1760 512 1760 junction
w 450 1419 100 0 n#374 f2OiwfsParkCad.f2OiwfsParkCad#367.ParkFollowState 448 1408 512 1408 junction
w 546 2147 100 0 n#374 junction 512 2144 640 2144 elongouts.followState.SLNK
w 602 619 100 0 CUR_POS inhier.CUR_POS.P 240 608 1024 608 1024 1248 1216 1248 f2OiwfsFollowA.f2OiwfsFollowA#385.cur_pos
w 2162 1259 100 0 Y_M f2OiwfsFollowA.f2OiwfsFollowA#385.y_m 1536 1248 2848 1248 outhier.Y_M.p
w 2018 1291 100 0 X_M f2OiwfsFollowA.f2OiwfsFollowA#385.x_m 1536 1280 2560 1280 2560 1408 2848 1408 outhier.X_M.p
w 1656 1355 100 0 MODE f2OiwfsFollowA.f2OiwfsFollowA#385.mode 1536 1344 1824 1344 1824 1536 junction
w 1624 1547 100 0 MODE f2OiwfsParkCad.f2OiwfsParkCad#367.ParkMODE 448 1536 2848 1536 outhier.MODE.p
w 1456 2288 100 0 MODE f2OiwfsCmd.f2OiwfsCmd#133.MODE 1440 2272 1472 2272 1472 1536 junction
w 2064 2288 100 0 MODE f2OiwfsCmd.f2OiwfsCmd#134.MODE 2048 2272 2080 2272 2080 1536 junction
w 2640 2288 100 0 MODE f2OiwfsCmd.f2OiwfsCmd#135.MODE 2624 2272 2656 2272 2656 1536 junction
w 1630 1387 100 0 DIR f2OiwfsFollowA.f2OiwfsFollowA#385.dir 1536 1376 1760 1376 1760 1824 junction
w 1618 1835 100 0 DIR f2OiwfsStopCad.f2OiwfsStopCad#366.StopDir 448 1824 2848 1824 outhier.DIR.p
w 558 1483 100 0 DIR f2OiwfsParkCad.f2OiwfsParkCad#367.ParkDIR 448 1472 704 1472 704 1824 junction
w 1504 2352 100 0 DIR f2OiwfsCmd.f2OiwfsCmd#133.DIR 1440 2336 1536 2336 1536 1824 junction
w 2112 2352 100 0 DIR f2OiwfsCmd.f2OiwfsCmd#134.DIR 2048 2336 2144 2336 2144 1824 junction
w 2704 2352 100 0 DIR f2OiwfsCmd.f2OiwfsCmd#135.DIR 2624 2336 2720 2336 2720 1824 junction
w 1084 1739 100 0 n#394 elongouts.followState.OUT 896 2112 1088 2112 1088 1376 1216 1376 f2OiwfsFollowA.f2OiwfsFollowA#385.follow
w 2626 1067 100 0 DEBUG f2OiwfsDebugCad.f2OiwfsDebugCad#382.DEBUG 2464 1056 2848 1056 outhier.DEBUG.p
w 554 843 100 0 TOLERANCE f2OiwfsFollowA.f2OiwfsFollowA#385.tolerance 1216 1280 928 1280 928 832 448 832 f2OiwfsTolCad.f2OiwfsTolCad#368.TOLERANCE
w 990 1323 100 0 Y_T f2OiwfsMoveCad.f2OiwfsMoveCad#370.Y_T 448 1152 800 1152 800 1312 1216 1312 f2OiwfsFollowA.f2OiwfsFollowA#385.y_t
w 974 1355 100 0 X_T f2OiwfsMoveCad.f2OiwfsMoveCad#370.X_T 448 1184 768 1184 768 1344 1216 1344 f2OiwfsFollowA.f2OiwfsFollowA#385.x_t
w 1410 2571 100 0 n#377 ecars.activeC.VAL 672 2560 2208 2560 2208 2336 2304 2336 f2OiwfsCmd.f2OiwfsCmd#135.ActiveState
w 1650 2347 100 0 n#377 junction 1632 2560 1632 2336 1728 2336 f2OiwfsCmd.f2OiwfsCmd#134.ActiveState
w 1042 2347 100 0 n#377 junction 1024 2560 1024 2336 1120 2336 f2OiwfsCmd.f2OiwfsCmd#133.ActiveState
s 240 2672 100 0 activeC monitors the assembly and commands
s 1872 2736 100 0 put reject messages here for testing
s 336 2640 100 0 through SNL in commandCar.stpp
[cell use]
use outhier 2816 1015 100 0 DEBUG
xform 0 2832 1056
use outhier 2816 1783 100 0 DIR
xform 0 2832 1824
use outhier 2816 1495 100 0 MODE
xform 0 2832 1536
use outhier 2816 1367 100 0 X_M
xform 0 2832 1408
use outhier 2816 1207 100 0 Y_M
xform 0 2832 1248
use elongouts 800 2224 100 0 followState
xform 0 768 2144
p 480 2286 100 0 0 EGU:0/1/2
p 704 2048 100 0 1 OMSL:supervisory
p 784 2224 100 512 -1 PV:$(top)
use inhier 224 567 100 0 CUR_POS
xform 0 240 608
use f2OiwfsFollowA 1216 1159 100 0 f2OiwfsFollowA#385
xform 0 1376 1312
use f2OiwfsDebugCad 2208 903 100 0 f2OiwfsDebugCad#382
xform 0 2336 1040
use f2OiwfsMoveCad 192 999 100 0 f2OiwfsMoveCad#370
xform 0 320 1136
use f2OiwfsFollowCad 192 1975 100 0 f2OiwfsFollowCad#369
xform 0 320 2120
use f2OiwfsTolCad 224 679 100 0 f2OiwfsTolCad#368
xform 0 320 816
use f2OiwfsParkCad 224 1335 100 0 f2OiwfsParkCad#367
xform 0 320 1472
use f2OiwfsStopCad 224 1671 100 0 f2OiwfsStopCad#366
xform 0 320 1808
use esirs 2512 2736 100 0 rejectMess
xform 0 2512 2592
p 2240 2336 100 0 0 DESC:Wavefront Sensor reject message
p 2368 2432 100 0 1 FDSC:Wavefront Sensor reject message
p 2512 2736 100 512 -1 PV:$(top)
use f2OiwfsCmd 1144 2143 100 0 f2OiwfsCmd#133
xform 0 1288 2304
p 1280 2288 150 256 -1 seta:cmd datum
use f2OiwfsCmd 1752 2143 100 0 f2OiwfsCmd#134
xform 0 1896 2304
p 1888 2288 150 256 -1 seta:cmd init
use f2OiwfsCmd 2328 2143 100 0 f2OiwfsCmd#135
xform 0 2472 2304
p 2464 2304 150 256 -1 seta:cmd test
use ecars 512 2608 100 0 activeC
xform 0 512 2448
p 512 2608 100 512 -1 PV:$(top)
use f2BorderC -144 343 100 0 f2BorderC#11
xform 0 1536 1648
p 3108 472 100 512 1 File:f2OiwfsCommands.sch
p 2516 516 150 0 1 Rev:1.0
p 2796 592 120 256 -1 Title:Receive TCS Commands
p 2836 528 100 1024 -1 author:B.Wooff
p 2836 496 100 1024 -1 date:May 31, 2005
[comments]
