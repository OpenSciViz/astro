[schematic2]
uniq 294
[tools]
[detail]
[cell use]
use f2OiwfsAssyEng 1088 1415 100 0 f2OiwfsAssyEng#293
xform 0 1200 1488
p 1088 1408 100 0 -1 seta:dev probe
use f2DeviceEng 1232 1255 100 0 f2DeviceEng#288
xform 0 1344 1328
p 1232 1248 100 0 -1 seta:dev probeBas
use f2DeviceEng 944 1255 100 0 f2DeviceEng#287
xform 0 1056 1328
p 944 1248 100 0 -1 seta:dev probePko
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 2840 -24 100 512 1 File:f2OiwfsEng.sch
p 1216 1088 100 0 0 IO:
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:OIWFS Engineering Top
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:July 22, 2004
p 1152 1056 100 0 0 model:
p 1152 1024 100 0 0 revision:
[comments]
