[schematic2]
uniq 409
[tools]
[detail]
w 914 1707 100 0 n#408 hwin.hwin#407.in 800 1696 1088 1696 ecad8.park.INPE
w 914 1771 100 0 n#403 hwin.hwin#402.in 800 1760 1088 1760 ecad8.park.INPD
w 914 1835 100 0 n#400 hwin.hwin#401.in 800 1824 1088 1824 ecad8.park.INPC
w 914 1899 100 0 n#399 hwin.hwin#398.in 800 1888 1088 1888 ecad8.park.INPB
w 914 1963 100 0 n#397 hwin.hwin#396.in 800 1952 1088 1952 ecad8.park.INPA
w 1650 2267 100 0 n#395 ecad8.park.MESS 1408 2144 1504 2144 1504 2256 1856 2256 estringouts.parkMess.DOL
w 1564 1827 100 0 n#394 ecad8.park.FLNK 1408 1440 1568 1440 1568 2224 1856 2224 estringouts.parkMess.SLNK
w 2258 1419 100 0 ParkFollowState elongouts.parkFollow.OUT 2112 1408 2464 1408 outhier.ParkFollowState.p
w 2258 1675 100 0 ParkDir elongouts.parkDir.OUT 2112 1664 2464 1664 outhier.ParkDIR.p
w 2258 1931 100 0 ParkMode elongouts.parkMode.OUT 2112 1920 2464 1920 outhier.ParkMODE.p
w 1954 1579 100 0 n#389 elongouts.parkDir.FLNK 2112 1728 2176 1728 2176 1568 1792 1568 1792 1440 1856 1440 elongouts.parkFollow.SLNK
w 1954 1835 100 0 n#388 elongouts.parkMode.FLNK 2112 1984 2176 1984 2176 1824 1792 1824 1792 1696 1856 1696 elongouts.parkDir.SLNK
w 1628 1627 100 0 n#387 ecad8.park.STLK 1408 1312 1632 1312 1632 1952 1856 1952 elongouts.parkMode.SLNK
w 1522 1867 100 0 n#386 ecad8.park.VALC 1408 1856 1696 1856 1696 1472 1856 1472 elongouts.parkFollow.DOL
w 1554 1931 100 0 n#385 ecad8.park.VALB 1408 1920 1760 1920 1760 1728 1856 1728 elongouts.parkDir.DOL
w 1602 1995 100 0 n#384 ecad8.park.VALA 1408 1984 1856 1984 elongouts.parkMode.DOL
[cell use]
use hwin 608 1655 100 0 hwin#407
xform 0 704 1696
p 611 1688 100 512 -1 val(in):$(top)followState.VAL
use hwin 608 1911 100 0 hwin#396
xform 0 704 1952
p 611 1944 100 512 -1 val(in):$(top)$(assy)Assembly.INIT
use hwin 608 1847 100 0 hwin#398
xform 0 704 1888
p 611 1880 100 512 -1 val(in):$(top)$(assy)Assembly.INDX
use hwin 608 1783 100 0 hwin#401
xform 0 704 1824
p 611 1816 100 512 -1 val(in):$(top)$(assy)Assembly.ILCK
use hwin 608 1719 100 0 hwin#402
xform 0 704 1760
p 611 1752 100 512 -1 val(in):$(top)$(assy)Assembly.ASTA
use ecad8 1280 2240 100 0 park
xform 0 1248 1728
p 1264 1984 100 0 -1 FTVA:LONG
p 1264 1920 100 0 -1 FTVB:LONG
p 1264 1856 100 0 -1 FTVC:LONG
p 1152 1200 100 0 1 INAM:
p 1264 2240 100 512 -1 PV:$(top)
p 1152 1168 100 0 1 SNAM:oiwfsParkCad
p 1408 1322 75 0 -1 pproc(STLK):PP
use estringouts 2016 2288 100 0 parkMess
xform 0 1984 2224
p 1904 2144 100 0 1 OMSL:closed_loop
p 2000 2288 100 512 -1 PV:$(top)
p 2224 2208 100 0 -1 def(OUT):$(top)rejectMess.IMSS
use outhier 2432 1623 100 0 ParkDIR
xform 0 2448 1664
use outhier 2432 1879 100 0 ParkMODE
xform 0 2448 1920
use outhier 2432 1367 100 0 ParkFollowState
xform 0 2448 1408
use elongouts 2032 1776 100 0 parkDir
xform 0 1984 1696
p 1904 1600 100 0 1 OMSL:closed_loop
p 2000 1776 100 512 -1 PV:$(top)
p 2112 1664 75 768 -1 pproc(OUT):PP
use elongouts 2032 2032 100 0 parkMode
xform 0 1984 1952
p 1904 1856 100 0 1 OMSL:closed_loop
p 2016 2032 100 512 -1 PV:$(top)
use elongouts 2032 1512 100 0 parkFollow
xform 0 1984 1440
p 1904 1344 100 0 1 OMSL:closed_loop
p 2016 1520 100 512 -1 PV:$(top)
p 2112 1408 75 768 -1 pproc(OUT):PP
use f2BorderC -144 343 100 0 f2BorderC#11
xform 0 1536 1648
p 3108 472 100 512 1 File:f2OiwfsParkCad.sch
p 2516 516 150 0 1 Rev:1.0
p 2796 592 120 256 -1 Title:Receive TCS Park Command
p 2836 528 100 1024 -1 author:Bob Wooff
p 2836 496 100 1024 -1 date:June 29, 2005
[comments]
