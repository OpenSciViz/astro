[schematic2]
uniq 363
[tools]
[detail]
w 1890 651 100 0 n#362 eaos.TrackY.FLNK 1760 640 2080 640 2080 1408 2144 1408 assemblyRecEng.assemblyRecEng#341.TRACK_TRIGGER
w 1890 875 100 0 n#362 eaos.TrackX.FLNK 1760 864 2080 864 junction
w 1154 363 100 0 n#361 ecalcs.JogTargetY.VAL 1024 352 1344 352 1344 640 1504 640 eaos.TrackY.DOL
w 1122 395 100 0 n#360 ecalcs.JogTargetY.FLNK 1024 384 1280 384 1280 608 1504 608 eaos.TrackY.SLNK
w 1154 939 100 0 n#359 ecalcs.JogTargetX.VAL 1024 928 1344 928 1344 864 1504 864 eaos.TrackX.DOL
w 1122 971 100 0 n#358 ecalcs.JogTargetX.FLNK 1024 960 1280 960 1280 832 1504 832 eaos.TrackX.SLNK
w 514 363 100 0 n#355 elongouts.JogYDir.FLNK 480 352 608 352 608 160 736 160 ecalcs.JogTargetY.SLNK
w 610 523 100 0 n#354 elongouts.JogYDir.VAL 480 320 544 320 544 512 736 512 ecalcs.JogTargetY.INPB
w 578 555 100 0 n#353 eaos.JogY.VAL 480 544 736 544 ecalcs.JogTargetY.INPA
w 514 939 100 0 n#352 elongouts.JogXDir.FLNK 480 928 608 928 608 736 736 736 ecalcs.JogTargetX.SLNK
w 610 1099 100 0 n#351 elongouts.JogXDir.VAL 480 896 544 896 544 1088 736 1088 ecalcs.JogTargetX.INPB
w 578 1131 100 0 n#350 eaos.JogX.VAL 480 1120 736 1120 ecalcs.JogTargetX.INPA
w 1602 2027 100 0 n#342 efanouts.Move1.LNK3 1344 2016 1920 2016 1920 1440 2144 1440 assemblyRecEng.assemblyRecEng#341.MOVE_TRIGGER
w 1602 1643 100 0 n#342 efanouts.Move2.LNK3 1344 1632 1920 1632 junction
w 1602 1259 100 0 n#342 efanouts.Move3.LNK3 1344 1248 1920 1248 1920 1440 junction
w 1354 1323 100 0 n#335 efanouts.Move3.LNK1 1344 1312 1424 1312 1424 1344 1504 1344 eaos.MoveX3Target.SLNK
w 1362 1291 100 0 n#334 efanouts.Move3.LNK2 1344 1280 1440 1280 1440 1152 1504 1152 eaos.MoveY3Target.SLNK
w 1354 1707 100 0 n#330 efanouts.Move2.LNK1 1344 1696 1424 1696 1424 1728 1504 1728 eaos.MoveX2Target.SLNK
w 1362 1675 100 0 n#329 efanouts.Move2.LNK2 1344 1664 1440 1664 1440 1536 1504 1536 eaos.MoveY2Target.SLNK
w 1362 2059 100 0 n#326 efanouts.Move1.LNK2 1344 2048 1440 2048 1440 1920 1504 1920 eaos.MoveY1Target.SLNK
w 1354 2091 100 0 n#325 efanouts.Move1.LNK1 1344 2080 1424 2080 1424 2112 1504 2112 eaos.MoveX1Target.SLNK
[cell use]
use eaos 1664 2192 100 0 MoveX1Target
xform 0 1632 2112
p 1632 2064 50 256 1 PREC:4
p 1664 2192 100 512 -1 PV:$(top)$(assy)
p 1696 2048 50 0 1 def(OUT):$(top)$(assy)Assembly.A
use eaos 1664 2000 100 0 MoveY1Target
xform 0 1632 1920
p 1632 1872 50 256 1 PREC:4
p 1664 2000 100 512 -1 PV:$(top)$(assy)
p 1696 1856 50 0 1 def(OUT):$(top)$(assy)Assembly.B
use eaos 1664 1808 100 0 MoveX2Target
xform 0 1632 1728
p 1632 1680 50 256 1 PREC:4
p 1664 1808 100 512 -1 PV:$(top)$(assy)
p 1696 1664 50 0 1 def(OUT):$(top)$(assy)Assembly.A
use eaos 1664 1616 100 0 MoveY2Target
xform 0 1632 1536
p 1632 1488 50 256 1 PREC:4
p 1664 1616 100 512 -1 PV:$(top)$(assy)
p 1696 1472 50 0 1 def(OUT):$(top)$(assy)Assembly.B
use eaos 1664 1424 100 0 MoveX3Target
xform 0 1632 1344
p 1632 1296 50 256 1 PREC:4
p 1664 1424 100 512 -1 PV:$(top)$(assy)
p 1696 1280 50 0 1 def(OUT):$(top)$(assy)Assembly.A
use eaos 1664 1232 100 0 MoveY3Target
xform 0 1632 1152
p 1632 1104 50 256 1 PREC:4
p 1648 1232 100 512 -1 PV:$(top)$(assy)
p 1696 1088 50 0 1 def(OUT):$(top)$(assy)Assembly.B
use eaos 384 1200 100 0 JogX
xform 0 352 1120
p 304 1072 100 0 1 PREC:4
p 368 1200 100 512 -1 PV:$(top)$(assy)
use eaos 384 624 100 0 JogY
xform 0 352 544
p 304 496 100 0 1 PREC:4
p 368 624 100 512 -1 PV:$(top)$(assy)
use eaos 1664 912 100 0 TrackX
xform 0 1632 832
p 1568 752 70 0 1 OMSL:closed_loop
p 1664 912 100 512 -1 PV:$(top)$(assy)
p 1696 784 70 0 1 def(OUT):$(top)$(assy)Assembly.A
p 1616 736 100 1024 0 name:$(top)$(I)
use eaos 1664 688 100 0 TrackY
xform 0 1632 608
p 1568 528 70 0 1 OMSL:closed_loop
p 1664 688 100 512 -1 PV:$(top)$(assy)
p 1696 560 70 0 1 def(OUT):$(top)$(assy)Assembly.B
p 1616 512 100 1024 0 name:$(top)$(I)
use ecalcs 864 1152 100 0 JogTargetX
xform 0 880 912
p 896 1088 100 0 1 CALC:(B=0)?(C+A):(C-A)
p 864 1152 100 512 -1 PV:$(top)$(assy)
p 912 1056 70 0 1 def(INPC):$(top)$(assy)Assembly.A
p 848 640 100 1024 0 name:$(top)$(I)
use ecalcs 864 576 100 0 JogTargetY
xform 0 880 336
p 896 512 100 0 1 CALC:(B=0)?(C+A):(C-A)
p 864 576 100 512 -1 PV:$(top)$(assy)
p 912 480 70 0 1 def(INPC):$(top)$(assy)Assembly.B
p 848 64 100 1024 0 name:$(top)$(I)
use elongouts 384 976 100 0 JogXDir
xform 0 352 896
p 384 976 100 512 -1 PV:$(top)$(assy)
p 336 800 100 1024 0 name:$(top)$(I)
use elongouts 384 400 100 0 JogYDir
xform 0 352 320
p 384 400 100 512 -1 PV:$(top)$(assy)
p 336 224 100 1024 0 name:$(top)$(I)
use assemblyRecEng 2144 1255 100 0 assemblyRecEng#341
xform 0 2320 1376
use f2DeviceEng 2208 1063 100 0 f2DeviceEng#339
xform 0 2320 1136
p 2240 1072 100 0 -1 seta:dev $(dev1)
use f2DeviceEng 2208 871 100 0 f2DeviceEng#340
xform 0 2320 944
p 2240 864 100 0 -1 seta:dev $(dev2)
use efanouts 1280 2160 100 0 Move1
xform 0 1224 2016
p 1264 2160 100 512 -1 PV:$(top)$(assy)
use efanouts 1280 1776 100 0 Move2
xform 0 1224 1632
p 1264 1776 100 512 -1 PV:$(top)$(assy)
use efanouts 1280 1392 100 0 Move3
xform 0 1224 1248
p 1264 1392 100 512 -1 PV:$(top)$(assy)
use f2BorderC -416 -153 100 0 gmosBorderC#91
xform 0 1264 1152
p 2836 -24 100 512 1 File:f2OiwfsAssyEng
p 1216 1088 100 0 0 IO:
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:Engineering Driver for OIWFS Assembly
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:May 31, 2005
p 1152 1056 100 0 0 model:
p 1152 1024 100 0 0 revision:
[comments]
