package ufjdd;
/**
 * Title:        Java Data Display (JDD) : FrameBuffer.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh, Frank Varosi
 * Company:      University of Florida
 * Description:  Communicate with Frame (Data) Acquisition Server (fetch frames etc.)
 */
import java.io.*;
import java.util.*;
import java.net.*;
import javaUFProtocol.*;

public class FrameBuffer {

    public static final
	String rcsID = "$Name:  $ $Id: FrameBuffer.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    protected Socket _socket = null;
    protected String bufferNames[];
    public UFFrameConfig frameConfig;

    public FrameBuffer(Socket socket) {
        _socket = socket;
    }

    public String[] getBufferNames() {
        // Send the agent a timestamp with the command BN to get "Buffer Names"
        UFTimeStamp uft = new UFTimeStamp("BN");
        
        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("FrameBuffer.getBufferNames> UFTimeStamp send ERROR");
	    return null;
        }
        
        UFStrings reply = null;

        if( (reply = (UFStrings)UFProtocol.createFrom(_socket)) == null ) {
            System.err.println("FrameBuffer.getBufferNames> UFStrings recv ERROR");
	    return null;
        }
        
        int numOfFrames = reply.numVals();
        bufferNames = new String[numOfFrames];
        
        for( int k=0; k < numOfFrames ; k++ ) bufferNames[k] = reply.valData(k);
        
        return bufferNames;
    }

    public UFFrameConfig getFrameConfig() {
        // Send the agent a timestamp with the command FC to get Frame Configuration
        UFTimeStamp uft = new UFTimeStamp("FC");
        
        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("FrameBuffer.getFrameConfig> UFTimeStamp send ERROR");
	    return null;
        }

        UFFrameConfig fc = null;

        if( (fc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
            System.err.println("FrameBuffer.getFrameConfig> UFFrameConfig recv ERROR");
	    return null;
        }

	System.out.println("FrameBuffer.getFrameConfig> (width=" + fc.width + ",  height=" + fc.height
			   + ") ---> " + fc.name());

	frameConfig = fc;
	return fc;
    }

    public synchronized ImageBuffer fetch( String buffName )
    {
        // Send the agent a timestamp with the desired Buffer Name:
        UFTimeStamp uft = new UFTimeStamp(buffName);

        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("FrameBuffer.fetch(" + buffName + ")> UFTimeStamp send ERROR");
            return null;
        }
        
        UFFrameConfig bfc = null;
        ImageBuffer imgBuffer = null;
        
        try {
            if( (bfc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
                System.err.println("FrameBuffer.fetch(" + buffName + ")> UFFrameConfig recv ERROR");
                return imgBuffer;
            }
        } catch(ClassCastException cce) {
            System.err.println("FrameBuffer.fetch(" + buffName + ")> Class Cast Exception " + cce);
            return null;
        }
        
        if( (bfc.name()).indexOf("ERROR") >= 0 ) {
            System.out.println("FrameBuffer.fetch> No data in Frame Buffer : " + buffName);
            return null;
        }
	else {
	    frameConfig = bfc;
	    UFInts ufi = null;
            
            if( (ufi =(UFInts)UFProtocol.createFrom(_socket)) == null ) {
                System.err.println("FrameBuffer.fetch(" + buffName + ")> UFInts Read ERROR");
            }

            imgBuffer = new ImageBuffer(bfc, ufi);
            return imgBuffer;
        }
    }
    
    public Vector fetchBuffers( String[] bufferList )
    {
        Vector v = new Vector(bufferList.length);
        
        for ( int i=0 ; i < bufferList.length ; i++) {
            ImageBuffer imgBuffer = fetch( bufferList[i] );
            v.add(imgBuffer);
        }

        return v;
    }
}
