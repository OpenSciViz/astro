package ufjdd;
/**
 * Title:        Java Data Display (JDD) : ColorMapDialog.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh, Frank Varosi, and Craig Warner
 * Company:      University of Florida
 * Description:  For interactively adjusting the index color mapping.
 */
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import javax.swing.event.*; 
import java.util.*;

public class ColorMapDialog extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: ColorMapDialog.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    // Contains the color map attributes
    byte[] r, b, g;
    public IndexColorModel colorModel;
    final int Ncolors = 256;
    int[] minIndexValues = new int[3];
    int[] maxIndexValues = new int[3];
    double[] gammaValues = new double[3];
    JSlider minIndexSliders[] = new JSlider[3];
    JSlider maxIndexSliders[] = new JSlider[3];
    JSlider gammaSliders[] = new JSlider[3];
    JPanel[] colorPanels = new JPanel[3];
    JComboBox restoreList;
    int[][] restoreValues;
    DataDisplayFrame jddFrame;

    public ColorMapDialog(DataDisplayFrame jddFrame) {
        this.jddFrame = jddFrame;
        this.getContentPane().setLayout(null);
        this.setSize( 875, Ncolors+40 );
        String colorNames[] = {"Red", "Green", "Blue"};

        // Create the JSliders for color map control:

        for(int i=0; i<minIndexSliders.length; i++) {
            minIndexSliders[i] = new JSlider(JSlider.VERTICAL);
            minIndexSliders[i].setName(colorNames[i] + "" + i);
            minIndexSliders[i].setBorder(BorderFactory.createTitledBorder("min"));
            minIndexSliders[i].setMinimum(0);
            minIndexSliders[i].setMaximum(Ncolors);
            minIndexSliders[i].setValue(0);
	    minIndexSliders[i].setMinorTickSpacing(Ncolors/16);
            minIndexSliders[i].setMajorTickSpacing(Ncolors/4);
            minIndexSliders[i].setPaintTicks(true);
            minIndexValues[i] = 0;
            minIndexSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    minIndexValues[index] = tempSlider.getValue();
                    updateFrameColorMap();
                }
            });
        }
        
        for(int i=0; i<maxIndexSliders.length; i++) {
            maxIndexSliders[i] = new JSlider(JSlider.VERTICAL);
            maxIndexSliders[i].setName(colorNames[i] + "" + i);
            maxIndexSliders[i].setBorder(BorderFactory.createTitledBorder("max"));
            maxIndexSliders[i].setMinimum(0);
            maxIndexSliders[i].setMaximum(Ncolors);
            maxIndexSliders[i].setValue(Ncolors);
            maxIndexSliders[i].setMinorTickSpacing(Ncolors/16);
            maxIndexSliders[i].setMajorTickSpacing(Ncolors/4);
            maxIndexSliders[i].setPaintTicks(true);
            maxIndexSliders[i].setPaintLabels(true);
            maxIndexValues[i] = Ncolors-1;
            maxIndexSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    maxIndexValues[index] = tempSlider.getValue();
                    updateFrameColorMap();
                }
            });
        }
        
        for(int i=0; i<gammaSliders.length; i++) {
            gammaSliders[i] = new JSlider(JSlider.VERTICAL);
            gammaSliders[i].setName(colorNames[i] + "" + i);
            gammaSliders[i].setBorder(BorderFactory.createTitledBorder("gamma"));
            gammaSliders[i].setMinimum(-10);
            gammaSliders[i].setMaximum(10);
            gammaSliders[i].setValue(0);
	    gammaSliders[i].setMajorTickSpacing(5);
	    gammaSliders[i].setMinorTickSpacing(1);
            gammaSliders[i].setPaintTicks(true);
            gammaSliders[i].setPaintLabels(true);
            gammaValues[i] = 1.0;
            gammaSliders[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseReleased(java.awt.event.MouseEvent e) {
                    JSlider tempSlider = (JSlider)e.getSource();
                    String sliderName = tempSlider.getName();
                    int index = Integer.parseInt(sliderName.charAt(sliderName.length()-1) + "");
                    // Scale the slider value between -1 to 1 and use as power of 10:
                    gammaValues[index] = Math.pow( 10, tempSlider.getValue()/10.0 );
                    updateFrameColorMap();
                }
            });
        }
        
        for(int i=0; i < colorPanels.length; i++) {
            colorPanels[i] = new JPanel();
            colorPanels[i].setLayout(new FlowLayout());
            colorPanels[i].setBorder(BorderFactory.createTitledBorder(colorNames[i]));
            colorPanels[i].add(minIndexSliders[i]);
            colorPanels[i].add(maxIndexSliders[i]);
            colorPanels[i].add(gammaSliders[i]);
            colorPanels[i].setBounds( i*225, 0, 215, Ncolors );
            getContentPane().add(colorPanels[i]);
        }
        
        colorModel = generateColorModel(minIndexValues, maxIndexValues, gammaValues);

	//look for file of saved color maps and put in JComboBox:
	setupRestoreList();

	restoreList.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      JComboBox tempcb = (JComboBox)ev.getSource();
	      int n = tempcb.getSelectedIndex();
	      if (n == 0) return;
              double gamma;
	      for( int j = 0; j < colorPanels.length; j++ ) {
		minIndexSliders[j].setValue(restoreValues[n-1][j*3]);
		maxIndexSliders[j].setValue(restoreValues[n-1][j*3+1]);
		gammaSliders[j].setValue(restoreValues[n-1][j*3+2]);
                minIndexValues[j] = restoreValues[n-1][j*3];
		maxIndexValues[j] = restoreValues[n-1][j*3+1];
	        gamma = restoreValues[n-1][j*3+2]/10.0;
                gammaValues[j] = Math.pow(10, gamma);
	      }
	      updateFrameColorMap();
	   }
	});

	JButton saveVals = new JButton("Save Values");
        saveVals.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      final JFrame saveFrame = new JFrame("Save Values");
	      JPanel savePanel1 = new JPanel();
              savePanel1.setSize(200,200);
	      savePanel1.setLayout(new GridLayout(3,1));
	      JLabel label = new JLabel("Enter a name:");
	      savePanel1.add(label);
	      final JTextField name = new JTextField(15);
              name.setEditable(true);
	      savePanel1.add(name);
	      JPanel savePanel2 = new JPanel();
	      savePanel2.setLayout(new FlowLayout());
	      JButton save = new JButton("Save");
	      save.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   String colorMapName = name.getText();
		   if (colorMapName != null) addColorMap(colorMapName);
		   saveFrame.dispose();
		}
	      });
	      JButton cancel = new JButton("Cancel");
	      cancel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   saveFrame.dispose();
		}
	      });
	      savePanel2.add(save);
	      savePanel2.add(cancel);
	      savePanel1.add(savePanel2);
	      saveFrame.getContentPane().add(savePanel1);
	      saveFrame.pack();
	      saveFrame.setVisible(true);
	   }
	});

        JButton delVals = new JButton("Delete Color Map");
        delVals.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              final String delItem = (String)restoreList.getSelectedItem();
              final int delIndex = restoreList.getSelectedIndex();
	      if (delItem.equals("Restore a Color Map")) return;
              final JFrame delFrame = new JFrame("Delete Color Map");
              JPanel delPanel1 = new JPanel();
              delPanel1.setSize(200,200);
              delPanel1.setLayout(new GridLayout(2,1));
              JLabel label = new JLabel("Delete: " + delItem + "?");
              delPanel1.add(label);
              JPanel delPanel2 = new JPanel();
              delPanel2.setLayout(new FlowLayout());
              JButton delete = new JButton("Delete");
              delete.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
		   delColorMap(delItem, delIndex);
                   delFrame.dispose();
                }
              });
              JButton cancel = new JButton("Cancel");
              cancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   delFrame.dispose();
                }
              });
              delPanel2.add(delete);
              delPanel2.add(cancel);
              delPanel1.add(delPanel2);
              delFrame.getContentPane().add(delPanel1);
              delFrame.pack();
              delFrame.setVisible(true);
           }
	});

        JButton ResetButton = new JButton("Reset to Gray Scale");
	JButton DoneButton = new JButton("Hide");

	ResetButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { reset(); } });

	DoneButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { done(); } });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(5,1));
        buttonPanel.setBorder(BorderFactory.createTitledBorder("Save or Restore"));
        buttonPanel.add(restoreList);
        buttonPanel.add(saveVals);
        buttonPanel.add(delVals);
	buttonPanel.add( ResetButton );
	buttonPanel.add( DoneButton );
        buttonPanel.setBounds( 675, 0, 180, Ncolors);
        getContentPane().add(buttonPanel);

        this.setLocation(5, 5);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

    private void done() { this.dispose(); }

    private void updateFrameColorMap() {
        colorModel = generateColorModel( minIndexValues, maxIndexValues, gammaValues );
        jddFrame.changeColorModel(colorModel);
    }
    
    private void reset() {
	for( int j = 0; j < colorPanels.length; j++ ) {
	    minIndexSliders[j].setValue(0);
	    maxIndexSliders[j].setValue(Ncolors);
	    gammaSliders[j].setValue(0);
	    minIndexValues[j] = 0;
	    maxIndexValues[j] = Ncolors;
	    gammaValues[j] = 1.0;
	}
	updateFrameColorMap();
	restoreList.setSelectedIndex(0);
    }

    private IndexColorModel generateColorModel(int[] minVals, int[] maxVals, double[] gamma) {
        // Generate 256-color model

	double r_min = minVals[0];
	double g_min = minVals[1];
	double b_min = minVals[2];

	double r_slope = 1.0/(maxVals[0] - minVals[0]);
        double g_slope = 1.0/(maxVals[1] - minVals[1]);
        double b_slope = 1.0/(maxVals[2] - minVals[2]);

        double r_gamma = gamma[0];
        double g_gamma = gamma[1];
        double b_gamma = gamma[2];
        
        r = new byte[Ncolors];
        g = new byte[Ncolors];
        b = new byte[Ncolors];
        
        for(int i=0; i < Ncolors; i++) {

	    if (i <= r_min) r[i] = (byte)0;
	    else if (i >= maxVals[0]) r[i] = (byte)255;
	    else r[i] = (byte) Math.floor((double)Ncolors*Math.pow(((double)i - r_min)*r_slope, r_gamma));

            if (i <= g_min) g[i] = (byte)0;
            else if (i >= maxVals[1]) g[i] = (byte)255;
            else g[i] = (byte) Math.floor((double)Ncolors*Math.pow(((double)i - g_min)*g_slope, g_gamma));

            if (i <= b_min) b[i] = (byte)0;
            else if (i >= maxVals[2]) b[i] = (byte)255;
            else b[i] = (byte) Math.floor((double)Ncolors*Math.pow(((double)i - b_min)*b_slope, b_gamma));
        }
        
        int bits = (int)Math.ceil( Math.log(Ncolors) / Math.log(2) );
        return new IndexColorModel(bits, Ncolors, r, g, b);
    }

    public IndexColorModel normColorModel(int [] normHist) {
        
        byte[] r_norm = new byte[Ncolors];
        byte[] b_norm = new byte[Ncolors];
        byte[] g_norm = new byte[Ncolors];
        
        for(int i=0; i < normHist.length; i++) {
            r_norm[i] = r[normHist[i]];
            b_norm[i] = b[normHist[i]];
            g_norm[i] = g[normHist[i]];
        }
        
        int bits = (int) Math.ceil( Math.log(Ncolors) / Math.log(2) );
        return new IndexColorModel(bits, Ncolors, r_norm, g_norm, b_norm);
    }

    public void setupRestoreList() {
       Properties props = new Properties(System.getProperties());
       String data_path = props.getProperty("jdd.data_path");
       System.out.println("jdd.data_path := " + data_path);

      File file = new File(data_path + "ColorMaps.dat");
      Vector vals = new Vector();
      String[] labels, values;
      int n = 1;
      int[] data = new int[9];
      String currLine = " ";
      Vector v = new Vector();
      if (file.exists()) {
	try {
	   BufferedReader r = new BufferedReader(new FileReader(file));
	   while (currLine != null) {
	      currLine = r.readLine();
	      if (currLine != null) {
		v.addElement(currLine);
		vals.addElement(r.readLine());
		n++;
	      }
	   }
	   r.close();
	} catch(IOException e) {
	   System.out.println("Error Reading From File.");
	}
      }
      restoreValues = new int[vals.size()][9];
      for (int j = 0; j < vals.size(); j++) {
        currLine = (String)vals.get(j);
	values = currLine.split(" ");
	for (int l = 0; l < 9; l++)
	   restoreValues[j][l] = Integer.parseInt(values[l]);
      }
      labels = new String[v.size()+1];
      labels[0] = "Restore a Color Map";
      for (int j = 1; j < n; j++) labels[j] = (String)v.get(j-1); 
      restoreList = new JComboBox(labels);
      restoreList.setMaximumRowCount(20);
      return;
   }

   public void addColorMap(String colorMapName) {
      File file = new File("jddColorMaps.dat");
      String vals = "";
      PrintWriter p;
      for (int j = 0; j < 3; j++) {
        vals += minIndexSliders[j].getValue() + " ";
        vals += maxIndexSliders[j].getValue() + " ";
        vals += gammaSliders[j].getValue();
	if (j < 2) vals += " ";
      }
      try {
	if (file.exists()) {
           p = new PrintWriter(new FileOutputStream(file, true));
	} else {
           p = new PrintWriter(new FileOutputStream(file));
	}
	p.println(colorMapName);
	p.println(vals);
	p.close();
      } catch(IOException e) {
	System.out.println("Error Writing To File.");
      }
      restoreList.addItem(colorMapName);
      int n = restoreValues.length;
      int[][] temp = new int[n+1][9];
      for (int j = 0; j < n; j++) {
	for (int l = 0; l < 9; l++) temp[j][l] = restoreValues[j][l];
      }
      for (int j = 0; j < 3; j++) {
        temp[n][j*3]=minIndexSliders[j].getValue();
        temp[n][j*3+1]=maxIndexSliders[j].getValue();
        temp[n][j*3+2]=gammaSliders[j].getValue();
      }
      restoreValues = temp;
      return;
   }

   public void delColorMap(String delItem, int delIndex) {
      File file = new File("jddColorMaps.dat");
      String label;
      String vals = "";
      PrintWriter p;
      try {
        p = new PrintWriter(new FileOutputStream(file));
	for (int j = 1; j < restoreList.getItemCount(); j++) {
	   label = (String)restoreList.getItemAt(j);
           if (!label.equals(delItem)) {
	      p.println(label);
	      vals = "";
	      for (int l = 0; l < 8; l++) {
		vals += restoreValues[j-1][l]+ " ";
	      }
	      vals += restoreValues[j-1][8];
              p.println(vals);
	   }
	}
        p.close();
      } catch(IOException e) {
        System.out.println("Error Writing To File.");
      }
      restoreList.removeItemAt(delIndex);
      int n = restoreValues.length;
      int[][] temp = new int[n-1][9];
      for (int j = 0; j < delIndex-1; j++) {
        for (int l = 0; l < 9; l++) temp[j][l] = restoreValues[j][l];
      }
      for (int j = delIndex; j < n; j++) {
        for (int l = 0; l < 9; l++) temp[j-1][l] = restoreValues[j][l];
      }
      restoreValues = temp;
      return;
   }
}
