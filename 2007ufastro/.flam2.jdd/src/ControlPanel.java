package ufjdd;
/**
 * Title:        Java Data Display (JDD): ControlPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For controlling the single image display of contents of a frame buffer.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ControlPanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ControlPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";
    
    ImageBuffer imgBuffer;
    ImageDisplayPanel imgdp;
    DataAccessPanel dataAccess;
    JRadioButton radioButtonSingle = new JRadioButton("Single");
    JRadioButton radioButtonAccum = new JRadioButton("Accum");
    ButtonGroup radioButtons = new ButtonGroup();
    JComboBox bufferSelector;
    AdjustPanel adjustPanel;
    
    public ControlPanel( ImageDisplayPanel idp,  AdjustPanel adjustPanel, DataAccessPanel dataAccess )
    {
        super(new GridLayout(1,4));
        this.imgdp = idp;
        this.dataAccess = dataAccess;
        this.adjustPanel = adjustPanel;
        
        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(imgdp.width, 50));
	setBorder(BorderFactory.createTitledBorder(""));

        int numBufs = dataAccess.bufferNames.length;
        String[] regBufferNames = new String[numBufs/2];
	int index = 0;
        
        for( int j= ((numBufs/2)); j < numBufs; j++ ) {
            regBufferNames[index++] = dataAccess.bufferNames[j];
        }

        bufferSelector = new JComboBox( regBufferNames );

        add(new JLabel("Data Buffer:"));
        add(bufferSelector);
        add(radioButtonSingle);
        add(radioButtonAccum);
  
        radioButtons.add(radioButtonSingle);
        radioButtons.add(radioButtonAccum);        
        
        ButtonModel bmod = radioButtonSingle.getModel();
        radioButtons.setSelected(bmod, true);

        // Register a listener for the radio buttons
        RadioListener myRadioListener = new RadioListener();
        radioButtonSingle.addActionListener(myRadioListener);
        radioButtonAccum.addActionListener(myRadioListener);
        
        bufferSelector.addActionListener(myRadioListener);        
    }
    
    // This method returns the selected radio button in a button group

    public static JRadioButton getSelection(ButtonGroup group) {
        for (Enumeration e=group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }
    
    public void displayBuffer( String buffname )
    {
	if( buffname.toLowerCase().indexOf("acc") >= 0 ) {
	    String basicBuff = buffname.substring( buffname.indexOf("(")+1 );
	    basicBuff = basicBuff.substring( 0, basicBuff.indexOf(")") );
	    displayBuffer( basicBuff, true );
	}
	else displayBuffer( buffname, false );
    }

    public void displayBuffer( String buffname, boolean accum )
    {
	if( accum )
	    radioButtons.setSelected( radioButtonAccum.getModel(), true);
	else
	    radioButtons.setSelected( radioButtonSingle.getModel(), true);

	bufferSelector.setSelectedItem( buffname );
	readFrameBuffer();
    }
    
    private void readFrameBuffer()
    {
        String radioSel = (String)((JRadioButton)getSelection(radioButtons)).getText();
        String selBufname = (String)bufferSelector.getSelectedItem();
        
        if( radioSel.equalsIgnoreCase("Accum") ) {
            imgdp.frmBuffName = "Accum(" + selBufname + ")";
            imgBuffer = dataAccess.fetch( imgdp.frmBuffName );
        }
        else {            
            imgdp.frmBuffName = selBufname;
            imgBuffer = dataAccess.fetch( selBufname );
        }

	if( imgBuffer != null ) {
	    imgdp.updateImage( imgBuffer );
	    adjustPanel.updateMinMaxVal(imgBuffer );
	    adjustPanel.applySetting();
	}
    }
    
    class RadioListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {                        
            readFrameBuffer();
        }
    }
}


