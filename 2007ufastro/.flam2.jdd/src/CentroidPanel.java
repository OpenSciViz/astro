package ufjdd;
/**
 * Title:        CentroidPanel.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh, Frank Varosi, Craig Warner
 * Company:      University of Florida
 * Description:  For plotting histogram of image data pixel values in a box.
 */
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javaUFLib.*;

public class CentroidPanel extends JFrame {

    public static final
	String rcsID = "$Name:  $ $Id: CentroidPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    UFLabel xLabel = new UFLabel(" Centroid X =", 2);
    UFLabel yLabel = new UFLabel(" Centroid Y =", 2); //always show at least 2 digits after decimal point.
    UFLabel fwhmValue = new UFLabel(" FWHM avg. =");
    UFLabel fwhmStdev = new UFLabel("  +/-  ");
    UFLabel fwhmX = new UFLabel(" FWHM: X cut =");
    UFLabel fwhmY = new UFLabel(" FWHM: Y cut =");
    UFLabel fwhmXY = new UFLabel(" FWHM: X-Y cut =");
    UFLabel fwhmYX = new UFLabel(" FWHM: X+Y cut =");

   public CentroidPanel() {
      super("Centroid");
      this.setSize( 230, 230 );
      Container content = getContentPane();
      content.setLayout(new GridLayout(0,1));
      content.add(xLabel);
      content.add(yLabel);
      JPanel FWHM = new JPanel();
      FWHM.setLayout(new RatioLayout());
      FWHM.add("0.0, 0.0; 0.6, 1.0", fwhmValue );
      FWHM.add("0.6, 0.0; 0.4, 1.0", fwhmStdev );
      content.add(FWHM);
      content.add(fwhmX);
      content.add(fwhmY);
      content.add(fwhmXY);
      content.add(fwhmYX);
      addWindowListener(new WindowAdapter() {
	      public void windowClosing(WindowEvent e) { e.getWindow().dispose(); } });
   }

   public void updateCentroid(float x, float y, int yMax, float[] fwhm)
   {
      if( !this.isVisible() ) this.setVisible(true);
      this.show();
      this.setState( Frame.NORMAL );
      xLabel.setText( x );
      yLabel.setText( yMax - y );
      fwhmValue.setText( fwhm[0] );
      fwhmStdev.setText( fwhm[1] );
      fwhmX.setText( fwhm[2] );
      fwhmY.setText( fwhm[3] );
      fwhmXY.setText( fwhm[4] );
      fwhmYX.setText( fwhm[5] );
   }
}
