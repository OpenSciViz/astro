package ufjdd;
/**
 * Title:        ZoomImage.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Frank Varosi
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Infrared Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

public class ZoomImage extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: ZoomImage.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    double zoomData[][];
    int xySize;
    byte pixBuffer[];
    Image pixImage = null;
    int init_x, init_y;
    int new_x, new_y;
    int xPos = 0, yPos = 0;
    double oMin, oMax;
    int newMin, newMax;
    int smoothIters = 0;
    int xStart, yStart, zoomFactor=1;
    double scaleFactor;
    boolean useScaleFactor = true;
    boolean mouseInPanel = false;
    final JPopupMenu popupMenu ;
    StatisticsPanel statPanel;
    LineCutPanel linecutPanel;
    CentroidPanel cenPanel;
    String analysisMode = "";
    float[] centrd;
    float[] fwhm = new float[6];
    boolean doCentroid = false;
    IndexColorModel colorModel;
    final HistogramBar histoBar;
    final AdjustZoomPanel adjustZoom;
    final JComboBox smoothIterSelect;
    final JLabel pixelDataVal, aperFlux;
    ImageBuffer zoomBuffer;

    public ZoomImage( int zoomSize, JComboBox smoothIterSel, IndexColorModel colorModel,
		      AdjustZoomPanel adjustZoom,  HistogramBar histoBar, JLabel pixelValue, JLabel aperFlux )
    {
	this.xySize = zoomSize;
        this.colorModel = colorModel;
        this.histoBar = histoBar;
	this.adjustZoom = adjustZoom;
	this.pixelDataVal = pixelValue;
	this.aperFlux = aperFlux;
	this.smoothIterSelect = smoothIterSel;
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        this.setMinimumSize(new Dimension(xySize, xySize));

        pixBuffer = new byte[xySize*xySize];

	// Create and add menu items
        popupMenu = new JPopupMenu();
        JMenuItem popupStats = new JMenuItem("Statisctics");
        JMenuItem popupLnCut = new JMenuItem("Line Cut");
        JMenuItem popupClear = new JMenuItem("Clear");
        final JMenuItem popupCntrd = new JMenuItem("Turn Centroid ON");
        
        popupMenu.add(popupStats);
        popupMenu.add(popupLnCut);
	popupMenu.add(popupClear);
        popupMenu.add(popupCntrd);

        SmoothListener smoothListener = new SmoothListener();
        smoothIterSelect.addActionListener( smoothListener );

        statPanel = new StatisticsPanel();
        linecutPanel = new LineCutPanel();
        cenPanel = new CentroidPanel();
                
        popupStats.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    analysisMode = "stat";
		    //System.out.println( analysisMode + " Event source " + evt.getActionCommand());
		}
	    });
        
        popupLnCut.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    analysisMode = "lncut";
		    //System.out.println( analysisMode +" Event source " + evt.getActionCommand());
		}
	    });
        
	popupCntrd.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    if (doCentroid) {
			doCentroid = false;
			popupCntrd.setText("Turn Centroid ON");
		    } else {
			doCentroid = true;
			popupCntrd.setText("Turn Centroid OFF");
			calcCentroid();
		    }
		    repaint();
		}
	    });

        popupClear.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    analysisMode = "clear";
		    repaint();
		    //System.out.println( analysisMode + " Event source " + evt.getActionCommand());
		}
	    });
        
        addMouseMotionListener(new MouseMotionListener() {

		public void mouseMoved(MouseEvent evt) { showPixelValue( evt.getX(), evt.getY() ); }
            
		public void mouseDragged(MouseEvent evt) {
		    new_x = evt.getX();
		    new_y = evt.getY();
		    repaint();
		}
	    });      

	addMouseListener(new MouseListener() {
            
		public void mousePressed(MouseEvent evt) {
                
		    if(( evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			new_x = init_x = evt.getX();
			new_y = init_y = evt.getY();
		    }
                
		    if(( evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0) {
			xPos = evt.getX();
			yPos = evt.getY();
			showPixelValue( xPos, yPos );
		    }
                
		    if(( evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			if(evt.isPopupTrigger()) {
			    popupMenu.show( evt.getComponent(), evt.getX(), evt.getY() );
			}
		    }
		};
            
		public void mouseReleased(MouseEvent evt) {

		    if(( evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
			new_x = evt.getX();
			new_y = evt.getY();
			if( analysisMode.equalsIgnoreCase("stat") ) {
			    // Pass the parameters  to plot the graph
			    calcStats();
			    repaint();
			}
			else if( analysisMode.equalsIgnoreCase("lncut") ) {
			    // Pass the parameters  to plot the graph
			    calcLineCut();
			    repaint();
			}
		    }
                
		    if(( evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0) {
			xPos = evt.getX();
			yPos = evt.getY();
			showPixelValue( xPos, yPos );
		    }

		    if(( evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
			if(evt.isPopupTrigger()) {
			    popupMenu.show( evt.getComponent(), evt.getX(), evt.getY() );
			}
		    }
		};
            
		public void mouseClicked(MouseEvent evt) {};
		public void mouseEntered(MouseEvent evt) { mouseInPanel = true; };
		public void mouseExited(MouseEvent evt) { mouseInPanel = false; };
	    });
    }

    void useScaleFactor( boolean useit ) { useScaleFactor = useit; }

    void showPixelValue() { if( !mouseInPanel ) showPixelValue( xPos, yPos ); }

    void showPixelValue( int zx, int zy )
    {
	if( zoomFactor > 0 && zoomData != null && zx >= 0 && zy >= 0 ) {
	    int xPix = zx/zoomFactor;
	    int yPix = zy/zoomFactor;
	    int xLoc = xStart + xPix;
	    int yLoc = zoomBuffer.frameConfig.height - yStart - yPix -1;
	    double zdata = zoomData[yPix][xPix];
	    if( useScaleFactor ) zdata *= scaleFactor;
	    String dataVal = UFLabel.truncFormat( zdata )
		+ "   [ " + zoomBuffer.name + " ,  cnt=" + zoomBuffer.frameConfig.frameProcCnt + " ]";
	    pixelDataVal.setText(" x = " + xLoc + ",  y = " + yLoc + ",  Data = " + dataVal );
	}
    }

    public void updateImage(ImageBuffer zoomBuff, IndexColorModel colorModel, int startX, int startY)
    {
        this.xStart = startX;
        this.yStart = startY;
	this.zoomBuffer = zoomBuff;
        this.zoomFactor = zoomBuff.zoomFactor;
        this.scaleFactor = zoomBuff.scaleFactor;

	smoothImage();
	adjustZoom.updateMinMaxVal( zoomBuffer );
	adjustZoom.applySetting();

	if( doCentroid ) calcCentroid();
	if( analysisMode.equals("stat") ) calcStats();
	if( analysisMode.equals("lncut") ) calcLineCut();
	showPixelValue();
    }
    
    public void updateColorMap(IndexColorModel colorModel) {
        this.colorModel = colorModel;
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }
    
    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        Graphics2D g2d = (Graphics2D)g;
        
        if( pixImage == null )
            g.drawString("Image not Available", 200, 200);
        else {
	    g.drawImage(pixImage, 0, 0, this);

	    if( analysisMode.equalsIgnoreCase("stat") ) {
                // Paint a rectangle with a translucent color
                g.setColor(new Color(128, 255, 128, 56));
                g.fillRect( Math.min(init_x, new_x), Math.min(init_y, new_y),
			    Math.abs(new_x - init_x), Math.abs(new_y - init_y));
                // Paint a solid black rectangular outline
                g.setColor(Color.WHITE);
                g.drawRect(Math.min(init_x, new_x), Math.min(init_y, new_y),
			   Math.abs(new_x - init_x), Math.abs(new_y - init_y));
            }
	    else if(analysisMode.equalsIgnoreCase("lncut"))  {
		g.setColor(Color.GREEN);
		g.drawLine(init_x, init_y, new_x, new_y);
	    }
	    //else g.drawImage(pixImage, 0, 0, this);

	    if (doCentroid) {
		if (centrd != null) {
		   int ax = 2, ay = 2;
		   if (fwhm[0] > 0) {
		      ay = Math.max( Math.round(fwhm[3]/2), 2 );
	              ax = Math.max( Math.round(fwhm[2]/2), 2 );
		   }
                   int x1=(int)Math.round((centrd[1]-ax)*zoomFactor);
                   int y1=(int)Math.round((centrd[0]-ay)*zoomFactor);
                   int x2=(int)Math.round((centrd[1]+ax)*zoomFactor);
                   int y2=(int)Math.round((centrd[0]+ay)*zoomFactor);

                   //g.setColor(new Color(128, 255, 128, 48));
                   //g.fillOval(x1, y1, (x2-x1), (y2-y1));
                   g.setColor(Color.BLACK);
                   g.drawLine((x1+x2)/2, y1, (x1+x2)/2, y2);
                   g.drawLine(x1, (y1+y2)/2, x2, (y1+y2)/2);
		}
	    }
	}
    }

    // magnify the array by a zoomFactor and convert from 2-D into 1-D array:

    private void zoomExpand( byte array[][] ) {

        for( int i=0; i < array.length; i++ ) {
	    int zi = i*zoomFactor;
            for( int j=0; j < array[i].length; j++ ) {
		int zj = j*zoomFactor;
		byte data = array[i][j];
                for( int k=0; k < zoomFactor; k++ ) {
		    int sindex = (k + zi) * xySize + zj;
                    for( int m=0; m < zoomFactor; m++ ) pixBuffer[sindex++] = data;
		}
	    }
	}
    }

    public void applyLinearScale() { applyLinearScale( oMin, oMax ); }
    
    public void applyLinearScale(double minv, double maxv)
    {
	if( zoomData == null ) return;
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
        double range = maxv - minv;

        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double f = ( zoomData[i][j] - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ) );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public void applyLogScale(double threshold)
    {
	if( zoomData == null ) return;
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
        double maxv = Math.log( (double)oMax );
        double range = maxv - minv;
        
        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double sd = zoomData[i][j];
		double f = 0.0;

		if( sd > threshold ) f = ( Math.log( sd ) - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ) );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    public void applyPowerScale(double threshold, double power)
    {
	if( zoomData == null ) return;
        int ny = zoomData.length;
	int nx = zoomData[0].length;
	byte[][] pixScaled = new byte[ny][nx];
	if( threshold <= 0 ) threshold = 1;
	if( power <= 0 ) power = 0.5;
	if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
        double maxv = Math.pow( (double)oMax, power );
        double range = maxv - minv;
        
        for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {

		double sd = zoomData[i][j];
		double f = 0.0;

		if( sd > threshold ) f = ( Math.pow( sd, power ) - minv )/range;

		if( f <= 0 )
		    pixScaled[i][j] = 0;
		else if( f >= 1 )
		    pixScaled[i][j] = (byte)255;
		else
		    pixScaled[i][j] = (byte)Math.round(f*255);
	    }
	}

        histoBar.updateHistogram( pixScaled, (int)Math.round( -255*minv/range ) );
	zoomExpand( pixScaled );
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }

    private void smoothImage()
    {
	zoomData = UFImageOps.smooth( zoomBuffer.image, 3, smoothIters );

	if( smoothIters > 0 ) {
	    oMin = zoomData[0][0];
	    oMax = oMin;
	    for( int i=0; i < zoomData.length; i++ ) {
		for( int j=0; j < zoomData[i].length; j++ ) {
		double zdata = zoomData[i][j];
                if( zdata < oMin ) oMin = zdata;
                if( zdata > oMax ) oMax = zdata;
		}
	    }
	    zoomBuffer.s_min = (float)oMin;
	    zoomBuffer.s_max = (float)oMax;
	}
	else {
	    oMin = zoomBuffer.min;
	    oMax = zoomBuffer.max;
	    zoomBuffer.s_min = zoomBuffer.min;
	    zoomBuffer.s_max = zoomBuffer.max;
	}
    }

    public void calcCentroid() {
	int[] zmax = UFArrayOps.whereMaxValue( zoomData );
	centrd = UFImageOps.getCentroid( zoomData, zmax[0], zmax[1], 5 );

	if (centrd[0] != -1)
	    fwhm = UFImageOps.fwhm2D( zoomData, Math.round(centrd[0]), Math.round(centrd[1]) );
	else fwhm[0] = -1;

	cenPanel.updateCentroid( centrd[1]+xStart, centrd[0]+yStart, zoomBuffer.frameConfig.height, fwhm );
    }

    public void calcStats() {
        int x1 = Math.min( init_x, new_x )/zoomFactor;
        int y1 = Math.min( init_y, new_y )/zoomFactor;
        int x2 = Math.max( init_x, new_x )/zoomFactor;
        int y2 = Math.max( init_y, new_y )/zoomFactor;
	double sf = 1.0;
	if( useScaleFactor ) sf = scaleFactor;
	statPanel.update( zoomData, x1, y1, x2, y2, sf );
    }

    public void calcLineCut() {
        int x1 = init_x/zoomFactor;
        int y1 = init_y/zoomFactor;
        int x2 = new_x/zoomFactor;
        int y2 = new_y/zoomFactor;
	double sf = 1.0;
	if( useScaleFactor ) sf = scaleFactor;
	linecutPanel.update( zoomData, x1, y1, x2, y2, sf );
    }

    class SmoothListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String selSmoothIter = (String) smoothIterSelect.getSelectedItem();
            if( selSmoothIter.length() > 0 ) {
                smoothIters =  Integer.parseInt( selSmoothIter );
		if( zoomBuffer == null ) return;
		if( zoomBuffer.image == null ) return;
		smoothImage();
		adjustZoom.updateMinMaxVal( zoomBuffer );
		adjustZoom.applySetting();
		if (doCentroid) calcCentroid();
		if (analysisMode.equals("stat")) calcStats();
		if (analysisMode.equals("lncut")) calcLineCut();
	    }
	    else smoothIters = 0;
        }
    }
}
