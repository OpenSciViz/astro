import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

public class UFTimeStamp extends UFProtocol {
  public UFTimeStamp() {
    _length=_MinLength_;
    _type=MsgTyp._TimeStamp_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // always 0 for timestamps
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _name = new String("");
  }

  public UFTimeStamp(int length) {
    _length=length;
    _type=MsgTyp._TimeStamp_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // always 0 for timestamps
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _name = new String("");
  }

  public UFTimeStamp(String name) {
    _length=_MinLength_ + name.length();
    _type=MsgTyp._TimeStamp_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // always 0 for timestamps
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _name = new String(name);
  }

  // all methods declared abstract by UFProtocal 
  // can be defined here
  public String description() { return new String("UFTimeStamp"); }
 
  // return size of the element's value (not it's name!)
  // either string length, or sizeof(float), sizeof(frame):
  public int valSize(int elemIdx) { return 0; }
 
  // write msg out to file, returns 0 on failure, num. bytes output on success
  public int writeTo(File f) {
    try {
      return _outputTo(new DataOutputStream(new FileOutputStream(f)));
    }
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    return 0;
  }

  // read/restore from file, this creates a new instance on heap, returns null on failure
  public int readData(File f) { 
    try {
      return _dataFrom(new DataInputStream(new FileInputStream(f)));
    }
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    return 0;
  }

  // write msg out to socket, returns 0 on failure, num. bytes on success
  public int sendTo(Socket soc) {
    try {
      return _outputTo(new DataOutputStream(soc.getOutputStream()));
    }
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    return 0;
  }

  // read/restore from socket,
  // this must assume that msg. length and type have already been read from socket!
  public int recvData(Socket soc) {
    try {
      return _dataFrom(new DataInputStream(soc.getInputStream()));
    }
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    return 0;
  }

  // methods required by the Externalizable Interface also
  // inherited from UFProtocol:
  // assume DataInput/OutputStreams take care of byte order issues!
  /*
  public void readExternal(DataInputStream inp) {
    Integer length=null;
    Integer typ=null;
    int readLengthAndType(inp, length, typ);
    _length = length.intValue();
    _type = typ.intValue();
    _dataFrom(inp);
  }
  public void writeExternal(DataOutputStream out) {
    _outputTo(out);
  }
  */ 
///////////////////////////// protected: /////////////////////
  // additional attributes & methods (beyond base class's):
  protected int _dataFrom(DataInputStream inp) {
    // restore everything but length & type
    int retval=0;
    try {
	//length and type have already been read
      _elem = inp.readInt(); // should be 0
      retval += 4;
      _seqCnt = inp.readShort(); // seqcnt and seqtot (both short ints)
      _seqTot = inp.readShort();
      _duration = inp.readFloat(); // read in 'duration'
      retval+=8;
      byte[] tbuf = new byte[_timestamp.length()];
      inp.readFully(tbuf, 0, _timestamp.length());
      _timestamp = new String(tbuf);
      int namelen = inp.readInt();
      retval += 4;
      byte[] namebuf = new byte[namelen];
      if( namelen > 0 ) {
	inp.readFully(namebuf, 0, namelen);
        retval +=  namelen;
	_name = new String(namebuf);
      }
    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    return retval;
  }

  protected int _outputTo(DataOutputStream out) {
    // write out all attributes
    int retval=0;
    try {
      out.writeInt(_length);
      retval += 4;
      out.writeInt(_type);
      retval += 4;
      out.writeInt(_elem);
      retval += 4;
      out.writeShort(_seqCnt);//2 shorts -- seqcnt, seqtot
      retval += 2;
      out.writeShort(_seqTot);
      retval += 2;
      out.writeFloat(_duration);//duration
      retval += 4;
      out.writeBytes(_timestamp);
      retval += _timestamp.length();
      out.writeInt(_name.length());
      retval += 4;
      if( _name.length() > 0 ) {
	out.writeBytes(_name);
	retval += _name.length();
      }
      out.flush();
    }
    catch(EOFException eof) {
      System.out.println(eof);
    } 
    catch(IOException ioe) {
      System.out.println(ioe);
    }
    catch( Exception e ) {
      System.out.println(e);
    }
    return retval;
  }

  protected void _currentTime() { 
    SimpleDateFormat df = new SimpleDateFormat("yyyy:DDD:HH:mm:ss.mmm");
    _timestamp = df.format(new Date()) + "000"; // microseconds == 000
  }  
}

