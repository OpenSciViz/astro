
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// This is exploratory as much as much as dislike programming like that.
// I am adding a piece of functionality at a time till i get a decent 
// L&F then i will add the action performed methods 
//
//
//

public class UFGuiClient extends JApplet{
    
    private boolean inAnApplet = true;
    public UFGuiClient(){
	this(true);
    }
    public UFGuiClient(boolean inAnApplet){
	this.inAnApplet =inAnApplet;
	if (inAnApplet) {
            getRootPane().putClientProperty("defeatSystemEventQueueCheck",
                                            Boolean.TRUE);
        }
    }
    public void init(){
	getContentPane().add(makePanels(), BorderLayout.NORTH);
      
    }

    public PanelSetup makePanels(){
    PanelSetup PS=new PanelSetup();
    return PS;
    }
    
    public static void main(String args[]){
	final UFGuiClient panel = new UFGuiClient(false);
	JFrame frame =new JFrame("GUI client in Progress");
	frame.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent e) {
		System.exit(0);
	    }
	    
	});
	frame.setContentPane(panel.makePanels());
	//frame.setSize(new Dimension(800,800));
	frame.pack();
	frame.setVisible(true);
    }
}



