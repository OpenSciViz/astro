import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.zip.*;

public class UFGZipFileTest {
  
    static DataInputStream _datain;
    static GZIPOutputStream _dataout;
    
    static byte[] frmbuff;
    static final int  DIMENSION = 512; // size of imagefile in bytes
    static final int EOF = -1;
    static public int w = DIMENSION;
    static public int h = DIMENSION;
    static public String sourcefile = "image.dat";
    static public String destfile = "image.dat.gz";

    public static void main(String[] argv) {
   
     int max = Integer.MIN_VALUE;
     frmbuff = new byte[w*h];
     byte val=0;
     int num=0; 

     // Reads from the sourcefile

    try {
     
      _datain = new DataInputStream(new FileInputStream(sourcefile)); 
     
      while( num < w*h) { // Read until EOF
        frmbuff[num++] = val = _datain.readByte();
	//if( val > max ) max = val;
      }
    }
    catch(FileNotFoundException nof) {
      System.err.println(sourcefile+": "+nof);
    }
    catch(EOFException eof) {
      System.err.println(sourcefile+": "+eof);
    }
    catch(IOException ioe) {
      System.err.println(sourcefile+": "+ioe);
    }
   
    finally {
      if(_datain != null) try { _datain.close(); } catch (IOException e) { ; }
    }
    num =0;

    // Writes compress data to the file "image.dat.gz"

 try {
     
     _dataout = new GZIPOutputStream(new FileOutputStream(destfile)); 
     //while( num < w*h ) { 
        _dataout.write(frmbuff,0,w*h);
	//if( val > max ) max = val;
	// }
    }
    catch(FileNotFoundException nof) {
      System.err.println(destfile+": "+nof);
    }
    catch(EOFException eof) {
      System.err.println(destfile+": "+eof);
    }
    catch(IOException ioe) {
      System.err.println(destfile+": "+ioe);
    }
   
    finally {
      try { _dataout.close(); } catch (IOException e) { ; }
    }

    
   }
}

 
