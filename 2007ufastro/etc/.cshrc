# .cshrc
if( ! $?LOGIN ) source ~/.login
set os = `/bin/uname`
if( "$os" == "SunOS" ) then
set path = (./ ~/bin /opt/EDTpdv $UFINSTALL/bin /usr/local/bin /usr/local/sbin /usr/xpg4/bin /usr/sbin /usr/bin /usr/dt/bin /usr/openwin/bin /bin /sbin /etc /usr/etc /usr/java/bin /opt/bin /usr/ccs/bin /usr/ccs/lib /usr/ucb )
else
set path = (./ ~/bin /opt/EDTpdv $UFINSTALL/bin /share/local/bin /usr/local/bin /usr/local/sbin /usr/X11R6/bin /usr/java/bin /bin /usr/bin /opt/bin /opt/netscape /sbin /etc /usr/sbin)
endif
set filec
set prompt = "%n@%m{\!}"
set history = 2000
set savehist = $history
# aliases at the end of file:
source ~/.alias
stty sane
