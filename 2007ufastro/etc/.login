setenv LOGIN
setenv TERM xterm
setenv PRINTER gatirprint
setenv EDITOR xemacs
setenv UFINSTALL /share/local/uf
setenv LD_LIBRARY_PATH $UFINSTALL/lib:/usr/local/lib:/usr/local/epics/lib:/opt/EDTpdv
# java:
#setenv CLASSPATH .:/usr/java/lib:/opt/netscape/java/classes/java40.jar
#source ~/idl_setup
