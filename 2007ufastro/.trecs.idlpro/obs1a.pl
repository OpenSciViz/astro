#!/usr/bin/perl
# obs1.pl: Stare-mode single image.
# "Std. Lab Setup": ZnSe, PAH-8.6um, Circular-a, airmass=1.0, emiss=1.0, T=18
# Run with Frametime = 50 msec or let metacon choose

# Load the Trecs module
use Trecs;

my $rcsId = '$Name:  $ $Id: obs1a.pl 14 2008-06-11 01:49:45Z hon $ ';
my $status=0;
my $scriptname = "obs1a.pl";
Trecs::openlog("obs1a.log");

# Instrument and Observation Configuration variables, change as desired.
# Any non-empty values are automatically sent to the DB.

### Initial Instrument Configuration ###

# Commanded camera mode
# "imaging"|"spectroscopy"
my $camera_mode = "imaging";

# Commanded imaging mode
# "field"|"window"|"pupil"
my $imaging_mode = "field" ;

# Commanded grating name
# "LowRes-10"|"HighRes-10"|"LowRes-20"
my $grating = "" ;

# Commanded central wavelength
my $central_wavelength = "" ;

# Commanded sector name
my $sector_name = "" ;

# Commanded Lyot stop name
my $lyot_stop_name = 'Circular-a' ;

# Commanded slit width
my $slit_width = "Clear";

# Following require overrides if manually set.
# Each override = "TRUE"|"FALSE"

# Window
my $override_window = "TRUE" ;
my $window_setting = "ZnSe" ;

# Aperture
my $override_aperture = "FALSE" ;
my $aperture_name = "" ;

# Filter
my $override_filter = "TRUE" ;
my $filter = "PAH-8.6um";

# Lens
my $override_lens = "FALSE" ;
my $lens_name = "" ;


### Initial Observation Configuration ###

# Commanded observing mode
# "chop-nod"|"stare"|"chop"|"nod"
my $observing_mode = "stare" ;

# Array readout mode
# "S1"|"S1R3"
my $readout_mode = "S1";

# FrameTime, requires override
# Actually, set frame_time to 0 for meta-frametime - 03Feb18
my $override_frame_time = "FALSE";
my $frame_time = "60" ;

# Nod params
my $nod_interval = "";
my $nod_settle = "";

# Commanded source photon collection time, minutes
my $photon_collection_time = "0.33" ;

# Commanded secondary chop throw magnitude
my $chop_throw = "0" ;

my $inst_rot_rate = "0";
my $override_array_temp = "TRUE" ;

# Environmental parameters
my $sky_noise = "20";
my $sky_background = "20";
my $airmass = "1.0";
my $mirror_temperature = "18";
my $emissivity = "1.0";


### Initialize Data label ###
my $datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;


##############################################################################
### Execute the first observation.  Do not change anything in this section ###
##############################################################################

# Initialize global variables
Trecs::tsprint "\n";
Trecs::tsprint "Script $scriptname\n";
Trecs::init_vars();

# Output configuration
Trecs::tsprint "\n" ;
Trecs::tsprint "Initializing Instrument configuration:\n" ;
Trecs::tsprint " Database:            $Trecs::epics\n" ;

# Clear Epics
`$Trecs::ufcaput -p $Trecs::epics':apply.DIR='$Trecs::CAD_CLEAR` ;

### Instrument configuration ###

if( $camera_mode        ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.A='$camera_mode`       ;}
if( $imaging_mode       ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.B='$imaging_mode`      ;}
if( $aperture_name      ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.C='$aperture_name`     ;}
if( $filter             ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.D='$filter`            ;}
if( $grating            ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.E='$grating`           ;}
if( $central_wavelength ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.F='$central_wavelength`;}
if( $lens_name          ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.G='$lens_name`         ;}
if( $lyot_stop_name     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.H='$lyot_stop_name`    ;}
if( $sector_name        ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.I='$sector_name`       ;}
if( $slit_width         ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.J='$slit_width`        ;}
if( $window_setting     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.K='$window_setting`    ;}

`$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.L='$override_aperture` ;
`$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.M='$override_filter`   ;
`$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.N='$override_lens`     ;
`$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.O='$override_window`   ;

# Note that William changed Inst.Seq. to config both instrumentSetup and observationSetup
# simultaneously, and correctly feeding instrumentSetup into observationSetup.

# Configure observationSetup.
Trecs::tsprint "\n";
Trecs::tsprint "Configuring observation. \n" ;

if( $observing_mode         ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.A='$observing_mode`; }
if( $photon_collection_time ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.B='$photon_collection_time` ;}
if( $chop_throw             ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.C='$chop_throw`;}
if( $sky_noise              ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.D='$sky_noise` ;  }
if( $sky_background         ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.E='$sky_background` ;  }
if( $airmass                ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.F='$airmass` ;   }
if( $mirror_temperature     ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.G='$mirror_temperature` ;}
if( $emissivity             ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.H='$emissivity`; }
if( $inst_rot_rate          ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.I='$inst_rot_rate`; }
if( $nod_interval           ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.J='$nod_interval`; }
if( $nod_settle             ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.K='$nod_settle`; }
if( $readout_mode           ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.L='$readout_mode`; }
if( $frame_time             ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.N='$frame_time` ;}

`$Trecs::ufcaput -p $Trecs::epics':observationSetup.M='$override_frame_time` ;
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.O='$override_array_temp` ;

`$Trecs::ufcaput -p $Trecs::epics':dataMode.A=save'` ;

#`$Trecs::ufcaput -g -p $Trecs::epics':dc:obsControlG.G=S1'`;
#`$Trecs::ufcaput -g -p $Trecs::epics':dc:obsControl.G=S1'`;


# Apply the configuration
$status = apply_config();
if( $status == 0 ) { Trecs::closelog(); exit; }

#`$Trecs::ufcaput -g -p $Trecs::epics':dc:physical.J=S1'`;
#`$Trecs::ufcaput -g -p $Trecs::epics':dc:MetaEnvG.I=S1'`;

# Configuration complete, print results
Trecs::print_config();


$status = exec_obs($datalabel, -1, -1);
if( $status == 0 ) { Trecs::closelog(); exit; }

#`$Trecs::ufcaput -g -p $Trecs::epics':dc:physical.J=S1'`;

Trecs::closelog();
