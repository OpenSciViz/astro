#!/usr/bin/perl
# Miri.pm:  Module to support T-ReCS Scripts
# Version 2002 Dec 11 (TLH)    : Added WNR's mods.
# Version 2003 Jan    (TLH&FV) : Changed timing of monitor section near end of exec_obs.
# Version 2003 Feb 9  (TLH)    : Added abort_obs and stop_obs and script run/stop control.

package Miri;
$Miri::rcsId = '$Name:  $ $Id: Miri.pm,v 0.0 2003/02/13 15:49:53 rambold beta $';

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

require Exporter;

@ISA = qw(Exporter Autoloader);
@EXPORT = qw(testsub apply_config exec_obs);
$VERSION = '1.0';

# directive enum from /gemini/epics/base/include/cad.h
# DO NOT CHANGE
$Miri::CAD_ABORT  = 0;
$Miri::CAD_MARK   = 0;
$Miri::CAD_CLEAR  = 1;
$Miri::CAD_PRESET = 2;
$Miri::CAD_START  = 3;
$Miri::CAD_STOP   = 4;
$Miri::CAD_ACCEPT = 0;
$Miri::CAD_REJECT = -1;

### Database Configuration ###
$Miri::epics   = "miri";
$Miri::sad     = "miri:sad";

$Miri::ufcaget = "";
$Miri::ufcaput = "";
$Miri::ufsleep = "";

### Script Control ###
$Miri::RUN = 1;
$Miri::STOP = 0;
$Miri::script_status = $Miri::RUN;


### Initialize global variables ###
sub init_vars {

   my $UFINSTALL = $ENV{ "UFINSTALL" } ;
   if( !defined( $UFINSTALL ) ) {
      $UFINSTALL = "/usr/local/uf" ;
      print "Setting UFINSTALL to /usr/local/uf" ;
   }

   $Miri::ufcaget = "$UFINSTALL/bin/ufcaget";
   $Miri::ufcaput = "$UFINSTALL/bin/ufcaput";
   $Miri::ufsleep = "$UFINSTALL/bin/ufsleep";

   if( ! -e "$Miri::ufcaget" ) {
      print "";
      print "Unable to locate $Miri::ufcaget, please ensure that this program is";
      print "present in your $UFINSTALL directory path, or adjust the";
      print "$UFINSTALL environment variable accordingly.";
      print "" ;
      exit(-1);
   }

   if( ! -e "$Miri::ufcaput" ) {
      print "" ;
      print "Unable to locate $Miri::ufcaput, please ensure that this program is";
      print "present in your $UFINSTALL directory path, or adjust the";
      print "$UFINSTALL environment variable accordingly.";
      print "";
      exit(-1);
   }

   if( ! -e "$Miri::ufsleep" ) {
      print "";
      print "Unable to locate $Miri::ufsleep, please ensure that this program is";
      print "present in your $UFINSTALL directory path, or adjust the";
      print "$UFINSTALL environment variable accordingly.";
      print "";
      exit(-1);
   }

   $Miri::script_status = $Miri::RUN;

}


### "Clear-Init ###
### This should never be needed -- booting or rebooting should bring system
### up ready to go.  TLH  2003 Feb 12

sub clear_init {

   my $ack = 0 ;
   my $msg = "" ;
   my $cfgstat = "";


   `$Miri::ufsleep 0.5` ;
   print "\nClear - Init ...\n" ;


   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_CLEAR` ;

   `$Miri::ufcaput -p $Miri::epics':init.A='$Miri::CAD_MARK` ;

   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_START` ;

   my $heart = `$Miri::ufcaget -p $Miri::epics':heartbeat'`;
   my $oldheart = $heart;
   while ($oldheart == $heart) {
      $heart = `$Miri::ufcaget -p $Miri::epics':heartbeat'`;
      print "Heartbeat = $heart\n";
      sleep 1;
   }
}


# Add a timestamp to STDIN and write to STDOUT and a logfile
 
sub tsprint {

   my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   my $timestr = sprintf("%02d:%02d:%02d", $hour, $min, $sec);

   print "$timestr $_[0]" ;

   open(LOGFILE, ">>mirilog.txt");
   print LOGFILE "$timestr $_[0]";
   close(LOGFILE);
}



# Apply the instrument configuration.  Check to see if the configuration
# is valid and, if so, wait for the re-configuration to complete.

sub apply_config {

   my $ack = 0 ;
   my $msg = "" ;
   my $cfgstat = "";

   if ($Miri::script_status == $Miri::STOP) {
      print("Script stopped: configuration not applied.\n");
      return 0;
   }

   `$Miri::ufsleep 0.5` ;
   print "\nApplying the configuration...\n" ;

   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_START` ;

   # if caget times-out...
   while( $ack == 0 )   {
      `$Miri::ufsleep 1.0` ;
      $ack = `$Miri::ufcaget $Miri::epics':'apply.VAL` ;
   }
   print "Apply result: $ack" ;

   if( $ack == -1 ) {
      $msg = `$Miri::ufcaget $Miri::epics':'apply.MESS` ;
      print "Instrument setup/configuration failed/rejected':' $msg\n" ;
      return 0;
   }

 # wait for the instrument configuration to complete
   while ( $cfgstat ne "IDLE" && $cfgstat ne "ERR" ) {
      $cfgstat = `$Miri::ufcaget $Miri::epics':applyC'` ;
      chop $cfgstat ;
      print "Configuring Instrument, status: $cfgstat\n" ;
      `$Miri::ufsleep 2.0` ;
   }

   if( $cfgstat eq "ERR" ) {
      print "Configuration status is: $cfgstat\n" ;
      $msg = `$Miri::ufcaget $Miri::epics':'apply.OMSS` ;
      chop $msg ;
      print "Instrument configuration failed, message: $msg\n" ;
      return 0;
   }

   return 1;
}


# Execute an observation.

sub exec_obs {

   my ($datalabel, $abort_frame, $stop_frame) = @_;

   my $ack = 0;
   my $msg = "";
   my $CARtop = "";
   my $CARobs = "";
   my $OBSstat = "";
   my $dt0 = `date` ;
   my $framecount = 0;

   if ($Miri::script_status == $Miri::STOP) {
      print("Script stopped: Observation with datalabel = $datalabel not executed.\n");
      return 0;
   }

   if ($abort_frame >= 0) {
      printf("Observation will Abort at frame $abort_frame\n");
   }
   elsif ($stop_frame >= 0) {
      printf("Observation will Stop at frame $stop_frame\n");
   }


   `$Miri::ufsleep 1.0` ;

 # Observation Data Label
   print "\nBeginning Observation with datalabel = $datalabel\n" ;
   `$Miri::ufcaput -p $Miri::epics':observe.A='$datalabel` ;

 # The above should mark the observe record -- hon.
 # MARK removed as recommended by W. Rambold.
 # `$Miri::ufcaput -p $Miri::epics':observe.DIR='$Miri::CAD_MARK` ;

 # Start the observation (exposure)
 # (2nd start):
   `$Miri::ufsleep 0.5` ;
   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_START` ;

 # if caget times-out...
   while( $ack == 0 ) {
      `$Miri::ufsleep 1.0` ;
      $ack = `$Miri::ufcaget $Miri::epics':'apply.VAL` ;
      chop $ack ;
   }

   if( $ack == -1 ) {
      $msg = `$Miri::ufcaget $Miri::epics':'apply.MESS` ;
      chop $msg ;
      print "Observation Start failed/rejected, message: $msg\n" ;
      return 0;
   }

   `$Miri::ufsleep 1.0` ;
   $CARtop = `$Miri::ufcaget $Miri::epics':applyC'` ;
   chop $CARtop ;
   print "CAR status: $CARtop\n" ;

   if( $CARtop eq "ERR" ) {
      $msg = `$Miri::ufcaget $Miri::epics':'apply.OMSS` ;
      chop $msg ;
      print "Instrument OBS start failed: $msg\n" ;
      return 0;
   }

 # wait for the observation  to complete
   $OBSstat = `$Miri::ufcaget $Miri::epics':sad:observationStatus'` ;
   chop $OBSstat ;
   print "observationStatus: $OBSstat\n" ;

   $CARobs = "BUSY";
   while ( $CARobs eq "BUSY" or $CARobs eq "PAUSED" ) {
      `$Miri::ufsleep 1.0` ;
      $CARobs = `$Miri::ufcaget $Miri::epics':observeC'` ;
      chop $CARobs ;
      $framecount = `$Miri::ufcaget $Miri::epics':dc:acqControlG.VALI'`;
      chop $framecount;
      print "OBS CAR: $CARobs, framecount = $framecount\n" ;

     # Handle abort or stop
      if    ($framecount == $abort_frame) { abort_obs(); }
      elsif ($framecount == $stop_frame)  { stop_obs();  }
   }

   if( $CARobs eq "ERR" ) {
      print "$dt0: $CARobs, Imaging Observation, label: $datalabel Failed" ;
      return 0;
   }

   $OBSstat = `$Miri::ufcaget $Miri::epics':sad:observationStatus'` ;
   chop $OBSstat ;
   print "observationStatus: $OBSstat\n" ;

   return 1;
}


# Abort the observation
sub abort_obs {

   `$Miri::ufcaput -p $Miri::epics':abort.DIR='$Miri::CAD_MARK` ;
   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_START` ;

   print "Observation Aborted\n";
   $Miri::script_status = $Miri::STOP;
}


# Stop the observation
sub stop_obs {

   `$Miri::ufcaput -p $Miri::epics':stop.DIR='$Miri::CAD_MARK` ;
   `$Miri::ufcaput -p $Miri::epics':apply.DIR='$Miri::CAD_START` ;

   print "Observation Stopped\n";
   $Miri::script_status = $Miri::STOP;
}


# Modules must return true value to load properly.
1;
__END__
