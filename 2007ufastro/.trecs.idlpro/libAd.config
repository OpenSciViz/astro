#/*+
#************************************************************************
#****  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#*
#* (c) 1996				(c) 1996.
#* National Research Council		Conseil national de recherches
#* Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
#* All rights reserved			Tous droits reserves
#* 					
#* NRC disclaims any warranties,	Le CNRC denie toute garantie
#* expressed, implied, or statu-	enoncee, implicite ou legale,
#* tory, of any kind with respect	de quelque nature que se soit,
#* to the software, including		concernant le logiciel, y com-
#* without limitation any war-		pris sans restriction toute
#* ranty of merchantability or		garantie de valeur marchande
#* fitness for a particular pur-	ou de pertinence pour un usage
#* pose.  NRC shall not be liable	particulier.  Le CNRC ne
#* in any event for any damages,	pourra en aucun cas etre tenu
#* whether direct or indirect,		responsable de tout dommage,
#* special or general, consequen-	direct ou indirect, particul-
#* tial or incidental, arising		ier ou general, accessoire ou
#* from the use of the software.	fortuit, resultant de l'utili-
#* 					sation du logiciel.
#*
#************************************************************************
#*
#*   Config file Name:	libAd.config
#*
#*   Purpose:
#*	configuration file for the archive directory library.
#*
#*   Date		: Nov 22, 1996.
#*
#*   Programmer		: Marc LeBlanc
#*
#*   Field SCCS data	: @(#)
#*	Module Name	: libAd.config
#*	Version Number	: 1.10
#*	Release Number	: 1
#*	Last Updated	: 04/02/98
#*
#*
#*   Modification History:
#*	97/01/31 JSD  :	Changed name to have capital A.
#*	97/02/17 JSD  :	Added default archive.
#*	97/03/25 SEC  :	Mod'ed format, included db server name. 
#*	97/04/04 SEC  :	Mod'ed for install at CADC.
#*	97/06/11 SEC  :	Major restructuring. All media now with medium kwd.
#*			Added default archive keyword, compression and
#*			format and remote sections. 
#*	97/08/25 SEC  :	Added site_id to remote keyword section.
#*	98/04/02 GZG  :	Added tape support and CF compression.
#*
#****  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#************************************************************************
#-*/


#
# Database information - _MUST_ be parsed first by config init routine.
#
# database	db server	database
# Keyword	name		name
# --------	---------	--------
#

database	xxx		dhsDB_NS


#
# Medium information. (local files)
#
# Medium	prior-	medium	file table	volume table
# Keyword	ity	name	name		name
# -------	------	------	----------	------------
#

medium		1	MD	mdfiles		mds


#
# Support for `Remote' files.
#
# Remote	Local		remote files	remote media	remote servers
# Keyword	Site ID		table name	table name	table name
# -------	-------		------------	------------	--------------
#

#remote		GNS		remote_files	remote_media	remote_servers


#
# Archive information.
#                               (N)o conversion of file ids
# Archive	Archive		(L)owercase or
# Keyword	Name		(U)ppercase file ids
# -------	-------		---------------------
#

archive		GEMINI		N


#
# Default Archive
#
# Default	Archive
# Keyword	Name
# -------	-------
#

default		GEMINI


#
# Compression extension information.
#
# Compression	Compression	Compression
# Keyword	Type		Extension
# -----------	-----------	-----------
#

compression	G		gz


#
# Format extension information.
#
# Format	Format		Format
# Keyword	Type		Extension
# -----------	-----------	-----------
#

format		F		fits
format		F		fit
format		S		sds
format		R		raw


# Medium        name    format
# Keyword       (type)
# -------       ------  --------

vol_mfs         CD      ISO9660
vol_mfs         DVD     ISO9660
vol_mfs         DVD     UDF
vol_mfs         MD      UFS
