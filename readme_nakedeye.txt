from: https://www.reddit.com/r/askastronomy/comments/13sv5ys/when_viewing_the_night_sky_are_the_brightest/

First off: how many stars are visible with the naked eye? Estimates vary, but the Yale Bright Star Catalogue (YBS) has about 9000 stars at magnitudes that could potentially be visible to the naked eye. Even in the best conditions, you will always see less than half of them at once.

How far away are all these visible stars? Nearly all of them are closer than 3000 light years. For comparison, the galaxy is a little less than 100k light years across, and a little less than 10k light years thick. If the galaxy was a bicycle wheel, the visible stars would all be in a ping-pong ball close to the edge of the wheel.

Here's a distribution of distances to the visible stars - most of them are closer than 1000 ly:

https://i.imgur.com/PHEzv3r.png

Now here's the actual answer to your question - the plot of distance as a function of visual magnitude. Visual magnitude is how bright the star appears to be to your eye. The horizontal axis is inverted, because large magnitude numbers mean less bright stars - but anyway, bright stars are to the right, dim stars are to the left:

https://i.imgur.com/w8sxbia.png

As you see, the brightest stars (the ones to the right) tend to be close to us (low in the graph). The brightest star, Sirius, is actually very close to us, at only 8 ly down at the bottom.

So yes, stars that look bright tend to be close to us. But the rule is not strict. Stars at magnitude 4 ... 6 (pretty dim stars) can be anywhere, at any distance. That's why I said "kind of".

This part is more technical.

Absolute magnitude is the "real" brightness of the star - not how bright it seems to us, but how bright it really is (distance is ignored). Here's a plot of absolute magnitude as a function of distance (vertical axis is inverted in terms of numbers, but stars at the top are actually the brightest, while those at the bottom are dim):

https://i.imgur.com/fNT6rck.png

What this plot says is: stars close to us can have any absolute magnitude. But stars far away need to be very bright (have very low absolute magnitude number), or else they would not be visible.

Sources:

Yale Bright Star Catalogue

https://cdsarc.u-strasbg.fr/viz-bin/Cat?V/50

The HYG Database

https://github.com/astronexus/HYG-Database


